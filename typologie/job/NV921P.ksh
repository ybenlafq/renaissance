#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NV921P.ksh                       --- VERSION DU 17/10/2016 18:33
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPNV921 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/07/15 AT 18.09.06 BY BURTEC2                      
#    STANDARDS: P  JOBSET: NV921P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  UNLOAD DE LA TABLE RTNV18                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=NV921PA
       ;;
(NV921PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=NV921PAA
       ;;
(NV921PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
# * CODIF BIDON DE LA TABLE POUR EVITER DE TOURNER AVEC STDARP                 
#    RTHV07   : NAME=RSHV07,MODE=I - DYNAM=YES                                 
       m_FileAssign -d NEW,CATLG,CATLG -r 27 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/NV921PAA.UNNV18AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV921PAA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  SORT DU FICHIER ISSU DU PGM BNV388 (PRECEDENT)                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV921PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=NV921PAD
       ;;
(NV921PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/NV921PAA.UNNV18AP
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NV921PAD.UNNV18BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_14_2 14 CH 2
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_14_2 ASCENDING,
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV921PAE
       ;;
(NV921PAE)
       m_CondExec 00,EQ,NV921PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BNV921 :MISE EN FORME DES DONNEES                                           
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV921PAG PGM=BNV921     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=NV921PAG
       ;;
(NV921PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR -g +0 FNV388 ${DATA}/PXX0/F07.NV388CCP
#                                                                              
       m_FileAssign -d SHR -g +0 FNV390 ${DATA}/PXX0/F07.NV390CCP
#                                                                              
       m_FileAssign -d SHR -g ${G_A2} FNV921 ${DATA}/PTEM/NV921PAD.UNNV18BP
# ****** FICHIER EN SORTIE LRECL 18                                            
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 FNV921PX ${DATA}/PXX0/F07.NV0921AP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 FNV921PM ${DATA}/PXX0/F07.NV0921BP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 FNV921EC ${DATA}/PXX0/F07.NV0921CP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 FNV921SR ${DATA}/PXX0/F07.NV0921DP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 FNV921MP ${DATA}/PXX0/F07.NV0921EP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 FNV921PR ${DATA}/PXX0/F07.NV0921PP
       m_ProgramExec BNV921 
#                                                                              
# ********************************************************************         
#  SORT DU FICHIER ISSU DU PGM BNV921                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV921PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=NV921PAJ
       ;;
(NV921PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/F07.NV0921AP
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PXX0/F07.NV0921BP
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PXX0/F07.NV0921CP
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PXX0/F07.NV0921DP
       m_FileAssign -d SHR -g ${G_A7} -C ${DATA}/PXX0/F07.NV0921EP
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.NV0921FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_19 1 CH 19
 /KEYS
   FLD_CH_1_19 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV921PAK
       ;;
(NV921PAK)
       m_CondExec 00,EQ,NV921PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FICHIER ISSU DU PGM BNV921                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV921PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=NV921PAM
       ;;
(NV921PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PXX0/F07.NV0921FP
       m_FileAssign -d SHR -g ${G_A9} -C ${DATA}/PXX0/F07.NV0921PP
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.NV0921XP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_19 1 CH 19
 /KEYS
   FLD_CH_1_19 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV921PAN
       ;;
(NV921PAN)
       m_CondExec 00,EQ,NV921PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP vers PCM                                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV921PAQ PGM=JVMLDM76   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=NV921PAQ
       ;;
(NV921PAQ)
       m_CondExec ${EXAAZ},NE,YES 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR -g ${G_A10} DD1 ${DATA}/PXX0/F07.NV0921XP
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FICZIP ${DATA}/PXX0/F07.V921ZIPC
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV921PAQ.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  ZIP AVEC JAVAZIP VERS DCOM                                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV921PAT PGM=JVMLDM76   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=NV921PAT
       ;;
(NV921PAT)
       m_CondExec ${EXABE},NE,YES 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR -g ${G_A11} DD1 ${DATA}/PXX0/F07.NV0921FP
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FICZIP ${DATA}/PXX0/F07.V921ZIPP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV921PAT.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTNV921P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV921PAX PGM=FTP        ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=NV921PAX
       ;;
(NV921PAX)
       m_CondExec ${EXABJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/NV921PAX.sysin
       m_UtilityExec INPUT
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTNV921P POUR PCM ( TALEND )                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV921PBA PGM=FTP        ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=NV921PBA
       ;;
(NV921PBA)
       m_CondExec ${EXABO},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/NV921PBA.sysin
       m_UtilityExec INPUT
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTNV921P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV921PBD PGM=FTP        ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=NV921PBD
       ;;
(NV921PBD)
       m_CondExec ${EXABT},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/NV921PBD.sysin
       m_UtilityExec INPUT
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTNV921P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV921PBG PGM=FTP        ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=NV921PBG
       ;;
(NV921PBG)
       m_CondExec ${EXABY},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/NV921PBG.sysin
       m_UtilityExec INPUT
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTNV921P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV921PBJ PGM=FTP        ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=NV921PBJ
       ;;
(NV921PBJ)
       m_CondExec ${EXACD},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/NV921PBJ.sysin
       m_UtilityExec INPUT
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTNV921P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV921PBM PGM=FTP        ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=NV921PBM
       ;;
(NV921PBM)
       m_CondExec ${EXACI},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/NV921PBM.sysin
       m_UtilityExec INPUT
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTNV921P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV921PBQ PGM=FTP        ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=NV921PBQ
       ;;
(NV921PBQ)
       m_CondExec ${EXACN},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/NV921PBQ.sysin
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=NV921PZA
       ;;
(NV921PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV921PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
