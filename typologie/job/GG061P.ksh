#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GG061P.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGG061 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 07/06/02 AT 11.05.40 BY BURTEC6                      
#    STANDARDS: P  JOBSET: GG061P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  CONCATENATION DU FICHIER DU JOUR ISSU DE GGS61P ET D'UN FICHIER             
#  REPRISE                                                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GG061PA
       ;;
(GG061PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GG061PAA
       ;;
(GG061PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
# ******* FIC ISSU DE GGS61P                                                   
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGP/F07.FGG061P
       m_FileAssign -d NEW,CATLG,DELETE -r 69 -g +1 SORTOUT ${DATA}/PTEM/GG061PAA.FCON01
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 69 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GG061PAB
       ;;
(GG061PAB)
       m_CondExec 00,EQ,GG061PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BCON01                                                                
#  ------------                                                                
#  MAJ DE LA TABLE RTSA00 ET CREATION DU FCON02                                
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GG061PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GG061PAD
       ;;
(GG061PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ARTICLES                                                   
#    RSSA00   : NAME=RSSA00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSSA00 /dev/null
# ******  FICHIER ISSU DU MERGE                                                
       m_FileAssign -d SHR -g ${G_A1} FCON01 ${DATA}/PTEM/GG061PAA.FCON01
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 77 -g +1 FCON02 ${DATA}/PTEM/GG061PAD.FCON02
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCON01 
       JUMP_LABEL=GG061PAE
       ;;
(GG061PAE)
       m_CondExec 04,GE,GG061PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCON01 ET CONCATENATION AVEC FICHIER ANOMALIE                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GG061PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GG061PAG
       ;;
(GG061PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GG061PAD.FCON02
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FANO01
       m_FileAssign -d NEW,CATLG,DELETE -r 77 -g +1 SORTOUT ${DATA}/PTEM/GG061PAG.BGG061P
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_10_7 10 CH 7
 /FIELDS FLD_CH_17_7 17 CH 7
 /FIELDS FLD_CH_46_7 46 CH 7
 /KEYS
   FLD_CH_10_7 ASCENDING,
   FLD_CH_17_7 ASCENDING,
   FLD_CH_46_7 ASCENDING,
   FLD_CH_1_8 DESCENDING
 /* Record Type = F  Record Length = 77 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GG061PAH
       ;;
(GG061PAH)
       m_CondExec 00,EQ,GG061PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGG061                                                                
#  CREATION DE DEUX FICHIERS POUR LES PGMS BGG062 ET BGG063                    
# ********************************************************************         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GG061PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GG061PAJ
       ;;
(GG061PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE EN LECTURE                                                     
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGF10   : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10 /dev/null
#    RSGF20   : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20 /dev/null
#    RSGG70   : NAME=RSGG70,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG70 /dev/null
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FIC ISSU DU BGG061                                                   
       m_FileAssign -d SHR -g ${G_A3} FGG61 ${DATA}/PTEM/GG061PAG.BGG061P
# ******  FIC-DES ANOMALIES                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 77 -g +1 FANO01 ${DATA}/PXX0/F07.FANO01
# ******  FIC-EXTRACTIONS                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 36 -g +1 FGG62 ${DATA}/PTEM/GG061PAJ.FGG062P
       m_FileAssign -d NEW,CATLG,DELETE -r 48 -g +1 FGG63 ${DATA}/PTEM/GG061PAJ.FGG063P
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGG061 
       JUMP_LABEL=GG061PAK
       ;;
(GG061PAK)
       m_CondExec 04,GE,GG061PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGG062                                                                
#  MAJ DB2 _A PARTIR DU FICHIER FGG062                                          
# ********************************************************************         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GG061PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GG061PAM
       ;;
(GG061PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE EN LECTURE                                                     
#    RSGG60   : NAME=RSGG60,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG60 /dev/null
#    RSSP10   : NAME=RSSP10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSSP10 /dev/null
#    RSSP20   : NAME=RSSP20,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSSP20 /dev/null
#    RSGG50   : NAME=RSGG50,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG50 /dev/null
#    RSGA67   : NAME=RSGA67,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA67 /dev/null
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FIC ISSU DE BGG061                                                   
       m_FileAssign -d SHR -g ${G_A4} FGG62 ${DATA}/PTEM/GG061PAJ.FGG062P
# ******  FIC ISSU DE BGG062 DE LA VEILLE                                      
       m_FileAssign -d SHR -g +0 FGG62V ${DATA}/PXX0/F07.FGG062V
# ******  FIC DU JOUR                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 36 -g +1 FGG62S ${DATA}/PXX0/F07.FGG062V
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGG062 
       JUMP_LABEL=GG061PAN
       ;;
(GG061PAN)
       m_CondExec 04,GE,GG061PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGG063                                                                
#  MAJ DB2 _A PARTIR DU FICHIER FGG063                                          
# ********************************************************************         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GG061PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GG061PAQ
       ;;
(GG061PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE EN LECTURE                                                     
#    RSGG60   : NAME=RSGG60,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG60 /dev/null
#    RSSP10   : NAME=RSSP10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSSP10 /dev/null
#    RSSP20   : NAME=RSSP20,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSSP20 /dev/null
#    RSGG50   : NAME=RSGG50,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG50 /dev/null
#    RSGA67   : NAME=RSGA67,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA67 /dev/null
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FIC ISSU DE BGG061                                                   
       m_FileAssign -d SHR -g ${G_A5} FGG63 ${DATA}/PTEM/GG061PAJ.FGG063P
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGG063 
       JUMP_LABEL=GG061PAR
       ;;
(GG061PAR)
       m_CondExec 04,GE,GG061PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GG061PZA
       ;;
(GG061PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GG061PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
