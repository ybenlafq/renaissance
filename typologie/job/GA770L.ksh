#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GA770L.ksh                       --- VERSION DU 08/10/2016 22:07
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGA770 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/01/17 AT 16.41.52 BY BURTECN                      
#    STANDARDS: P  JOBSET: GA770L                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# **--USER='LILLE'                                                             
# ********************************************************************         
#    IMPERATIF :  CETTE CHAINE DOIT ETRE EXECUTEE IMPERATIVEMENT               
#    -=-=-=-=-                                                                 
# ********************************************************************         
#  QUIESCE DU TABLESPACE RSGA00 AVANT MAJ DU PROG BGA770                       
#  SI PROBLEME DS LA CHAINE PRENDRE LA VALEUR DU RBA DE CE STEP                
#  ET UTILISER LA CHAINE CORTEX DB2RBL AVEC CE RBA                             
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GA770LA
       ;;
(GA770LA)
#
#GA770LAA
#GA770LAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GA770LAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       RUN=${RUN}
       JUMP_LABEL=GA770LAD
       ;;
(GA770LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE : SOUS TABLE ZPRIX                                 
#    RSGA01L  : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01L /dev/null
# ******* TABLE DES LIENS ENTRE ARTICLES                                       
#    RSGA58L  : NAME=RSGA58L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58L /dev/null
# ******* TABLE DES PRIX PAR ARTICLE                                           
#    RSGA59L  : NAME=RSGA59L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59L /dev/null
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* SOCIETE TRAITEE : 961                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
#                                                                              
# ******* MAJ DE LA DATE DE RECEPTION (D1RECEPT) DE RTGA00                     
#    RSGA00L  : NAME=RSGA00L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00L /dev/null
# ******* MAJ ARTICLES MUTES                                                   
#    RSMU15L  : NAME=RSMU15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSMU15L /dev/null
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGA770 
       JUMP_LABEL=GA770LAE
       ;;
(GA770LAE)
       m_CondExec 04,GE,GA770LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
