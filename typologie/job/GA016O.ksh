#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GA016O.ksh                       --- VERSION DU 08/10/2016 22:02
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGA016 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 10/10/27 AT 15.58.06 BY PREPA3                       
#    STANDARDS: P  JOBSET: GA016O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#    PGM=BHV080                                                                
# ********************************************************************         
#    CREATION DU FICHIER CONSO ACHAT D.A.L (FHV070) SERVANT A LOADER L         
#    TABLE RTHV70 DE D.A.L GA016O                                              
#    REPRISE:OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GA016OA
       ;;
(GA016OA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAP=${EXAAP:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GA016OAA
       ;;
(GA016OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GAMMES (HISTOR DES MVTS DE RECEPTIONS)                                
#    RSGG70O  : NAME=RSGG70O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG70O /dev/null
#  TABLE COMMANDES FOURNISSEURS                                                
#    RSGF10   : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10 /dev/null
#  TABLE GENERALISEE (S/TABLE EXACH)                                           
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
#  SORTIE (FICHIER HISTOR PAR ACHATS PAR SOC MOIS ARTICLES)                    
       m_FileAssign -d NEW,CATLG,DELETE -r 42 -g +1 FHV070 ${DATA}/PXX0/F16.BHV070AO
#  ENTREE (FICHIER DONNANT LE NUMERO DE SOCIETE A TRAITER)                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#  ENTREE (PARAMETRE FMOIS : MOIS A TRAITER)                                   
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV080 
       JUMP_LABEL=GA016OAB
       ;;
(GA016OAB)
       m_CondExec 04,GE,GA016OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    LOAD DE LA TABLE RTHV70                                                   
#    REPRISE:OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA016OAD PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GA016OAD
       ;;
(GA016OAD)
       m_CondExec ${EXAAP},NE,YES 
#  FICHIERS DE TRAVAIL                                                         
       m_FileAssign -d NEW SORTWK01 ${MT_TMP}/SORTWK01_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK02 ${MT_TMP}/SORTWK02_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK03 ${MT_TMP}/SORTWK03_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK04 ${MT_TMP}/SORTWK04_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK05 ${MT_TMP}/SORTWK05_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSHV70O  : NAME=RSHV70O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSHV70O /dev/null
       m_FileAssign -d SHR -g ${G_A1} SYSREC ${DATA}/PXX0/F16.BHV070AO
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA016OAD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA016O_GA016OAD_P916RTHV70.sysload
       m_UtilityExec
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA016OAE
       ;;
(GA016OAE)
       m_CondExec 04,GE,GA016OAD ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
