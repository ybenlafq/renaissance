#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GM070O.ksh                       --- VERSION DU 17/10/2016 18:07
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGM070 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 16/01/05 AT 10.22.03 BY BURTEC3                      
#    STANDARDS: P  JOBSET: GM070O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA59 : PRIX                              
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA75 : PRIME                             
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GM070OA
       ;;
(GM070OA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXA98=${EXA98:-0}
       FIRSTME=${FIRSTME}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A45=${G_A45:-'+1'}
       G_A46=${G_A46:-'+1'}
       G_A47=${G_A47:-'+1'}
       G_A48=${G_A48:-'+1'}
       G_A49=${G_A49:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A50=${G_A50:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       MERCRED=${MERCRED}
       RUN=${RUN}
       JUMP_LABEL=GM070OAA
       ;;
(GM070OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
#    RSGS30O  : NAME=RSGS30O,MODE=I - DYNAM=YES                                
#    RSGA59O  : NAME=RSGA59O,MODE=I - DYNAM=YES                                
#    RSGA75O  : NAME=RSGA75O,MODE=I - DYNAM=YES                                
       m_FileAssign -d NEW,CATLG,DELETE -g +1 SYSREC01 ${DATA}/PTEM/GM070OAA.BGA59AO
       m_FileAssign -d NEW,CATLG,DELETE -g +1 SYSREC02 ${DATA}/PTEM/GM070OAA.BGA75AO
       m_FileAssign -d NEW,CATLG,DELETE -g +1 SYSREC03 ${DATA}/PTEM/GM070OAA.BGS30UO
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070OAA.sysin
       
       m_DBHpuUnload -f SYSIN
# ********************************************************************         
#  SORT DES FICHIERS D'UNLOAD DE LA GA59                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GM070OAD
       ;;
(GM070OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GM070OAA.BGA59AO
       m_FileAssign -d NEW,CATLG,DELETE -g +1 SORTOUT ${DATA}/PTEM/GM070OAD.BGA59ZO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_8_2 8 CH 2
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_15_8 15 CH 8
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_8_2 ASCENDING,
   FLD_CH_15_8 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070OAE
       ;;
(GM070OAE)
       m_CondExec 00,EQ,GM070OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DES FICHIERS D'UNLOAD DE LA GA75                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GM070OAG
       ;;
(GM070OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GM070OAA.BGA75AO
       m_FileAssign -d NEW,CATLG,DELETE -g +1 SORTOUT ${DATA}/PTEM/GM070OAG.BGA75ZO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_8_2 8 CH 2
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_10_8 10 CH 8
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_8_2 ASCENDING,
   FLD_CH_10_8 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070OAH
       ;;
(GM070OAH)
       m_CondExec 00,EQ,GM070OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FIC RTGS30                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GM070OAJ
       ;;
(GM070OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GM070OAA.BGS30UO
       m_FileAssign -d NEW,CATLG,DELETE -r 16 -g +1 SORTOUT ${DATA}/PTEM/F07.BGS30FO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_3 04 CH 3
 /FIELDS FLD_CH_21_3 21 CH 3
 /FIELDS FLD_CH_18_3 18 CH 3
 /FIELDS FLD_CH_7_7 7 CH 7
 /KEYS
   FLD_CH_7_7 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_7_7,FLD_CH_4_3,FLD_CH_18_3,FLD_CH_21_3
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GM070OAK
       ;;
(GM070OAK)
       m_CondExec 00,EQ,GM070OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM068                                                                      
#  EXTRACT DES VENTES SUR 28 JOURS GLISSANTS                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GM070OAM
       ;;
(GM070OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** TABLE                                                                 
#    RSHV02O  : NAME=RSHV02O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV02O /dev/null
# ****** TABLE                                                                 
#    RSHV12   : NAME=RSHV12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV12 /dev/null
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ****** FICHIER DES VENTES SUR 28 JOURS                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 11 -g +1 FGM068 ${DATA}/PTEM/GM070OAM.BGM068AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM068 
       JUMP_LABEL=GM070OAN
       ;;
(GM070OAN)
       m_CondExec 04,GE,GM070OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CONSTITUTION DU FIC DES VENTES SOCIETES                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GM070OAQ
       ;;
(GM070OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GM070OAM.BGM068AO
       m_FileAssign -d NEW,CATLG,DELETE -r 11 -g +1 SORTOUT ${DATA}/PTEM/GM070OAQ.BGM068BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 01 CH 7
 /FIELDS FLD_PD_8_4 08 PD 4
 /KEYS
   FLD_CH_1_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_8_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070OAR
       ;;
(GM070OAR)
       m_CondExec 00,EQ,GM070OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM069                                                                      
#  EXTRACT DES CODES DESCRIPTIFS , CODE MARKETING                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GM070OAT
       ;;
(GM070OAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** TABLE                                                                 
#    RSGA00O  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00O /dev/null
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
#    RSGA14O  : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14O /dev/null
#    RSGA58O  : NAME=RSGA58O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58O /dev/null
#    RSGA12O  : NAME=RSGA12O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12O /dev/null
#  FICHIER EN ENTRE ISSU DU GM010P                                             
       m_FileAssign -d SHR -g +0 FGA053 ${DATA}/PXX0/F07.BGA053BP
#  MODIF LE FGM069 PASSE DE 273 A 436                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 436 -g +1 FGM069 ${DATA}/PTEM/GM070OAT.BGM069AO
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -g +1 FGM020 ${DATA}/PTEM/GM070OAT.BGM069BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM069 
       JUMP_LABEL=GM070OAU
       ;;
(GM070OAU)
       m_CondExec 04,GE,GM070OAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GM070OAX
       ;;
(GM070OAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GM070OAT.BGM069AO
#  CE FICHIER PASSE DE 273 A 436                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 436 -g +1 SORTOUT ${DATA}/PTEM/GM070OAX.BGM069CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 01 CH 7
 /FIELDS FLD_CH_145_5 145 CH 5
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_145_5 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070OAY
       ;;
(GM070OAY)
       m_CondExec 00,EQ,GM070OAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FGM065                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GM070OBA
       ;;
(GM070OBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PEX0/F07.BGM065BP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PEX0/F16.BGM065MO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916"
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_11_3 11 CH 3
 /CONDITION CND_1 FLD_CH_8_3 EQ CST_1_5 
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_11_3 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070OBB
       ;;
(GM070OBB)
       m_CondExec 00,EQ,GM070OBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FGM065Z                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GM070OBD
       ;;
(GM070OBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PEX0/F07.BGM065CP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PEX0/F16.BGM065ZO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "916"
 /FIELDS FLD_CH_8_3 8 CH 3
 /CONDITION CND_1 FLD_CH_8_3 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070OBE
       ;;
(GM070OBE)
       m_CondExec 00,EQ,GM070OBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM070                                                                      
#  ALIMENTATION DU CAHIER BLEU                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GM070OBG
       ;;
(GM070OBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** TABLE                                                                 
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
#    RSGA10O  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10O /dev/null
#    RSGA14O  : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14O /dev/null
#    RSGA58O  : NAME=RSGA58O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58O /dev/null
#    RSGA02   : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02 /dev/null
#    RSGA30O  : NAME=RSGA30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30O /dev/null
#    RSGA19   : NAME=RSGA19,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA19 /dev/null
#    RSGA66O  : NAME=RSGA66O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA66O /dev/null
#    RSGG20O  : NAME=RSGG20O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG20O /dev/null
#    RSGG40O  : NAME=RSGG40O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG40O /dev/null
#    RSGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA67 /dev/null
#    RSGF55   : NAME=RSGF55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF55 /dev/null
#    RSMU06   : NAME=RSMU06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSMU06 /dev/null
#    RSSP15O  : NAME=RSSP15O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSSP15O /dev/null
#    RSGA68O  : NAME=RSGA68O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA68O /dev/null
#    RSRX00O  : NAME=RSRX00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX00O /dev/null
#    RSRX55O  : NAME=RSRX55O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX55O /dev/null
#                                                                              
#    RSFL05   : NAME=RSFL05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL05 /dev/null
#    RSFL06   : NAME=RSFL06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL06 /dev/null
#    RSGQ05   : NAME=RSGQ05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGQ05 /dev/null
# **** DATE                                                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ****** FICHIER LUS                                                           
       m_FileAssign -d SHR -g ${G_A6} FGM068 ${DATA}/PTEM/GM070OAQ.BGM068BO
       m_FileAssign -d SHR -g ${G_A7} FGM069 ${DATA}/PTEM/GM070OAX.BGM069CO
       m_FileAssign -d SHR -g ${G_A8} FGM020 ${DATA}/PTEM/GM070OAT.BGM069BO
       m_FileAssign -d SHR -g +0 RTGN59 ${DATA}/PXX0/F07.BGN59FF
       m_FileAssign -d SHR -g ${G_A9} RTGA59Z ${DATA}/PTEM/GM070OAD.BGA59ZO
       m_FileAssign -d SHR -g +0 RTGN75F ${DATA}/PXX0/F07.BGN75FF
       m_FileAssign -d SHR -g ${G_A10} RTGA75Z ${DATA}/PTEM/GM070OAG.BGA75ZO
       m_FileAssign -d SHR -g ${G_A11} FGM065M ${DATA}/PEX0/F16.BGM065MO
       m_FileAssign -d SHR -g ${G_A12} FGM065Z ${DATA}/PEX0/F16.BGM065ZO
       m_FileAssign -d SHR -g ${G_A13} RTGS30 ${DATA}/PTEM/F07.BGS30FO
       m_FileAssign -d SHR -g +0 RTGS10 ${DATA}/PXX0/F07.BGS10FF
# ****** FICHIER CREES                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -g +1 RTGM01 ${DATA}/PTEM/GM070OBG.BGM070AO
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -g +1 RTGM06 ${DATA}/PTEM/GM070OBG.BGM070BO
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FGM075 ${DATA}/PTEM/GM070OBG.BGM070CO
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FGM70 ${DATA}/PTEM/GM070OBG.BGM070DO
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -g +1 FGM01 ${DATA}/PTEM/GM070OBG.BGM070EO
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -g +1 FGM06 ${DATA}/PTEM/GM070OBG.BGM070FO
#  CREATION DE 2 NOUVEAUX FICHIERS LRECL 227                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g +1 FSM025 ${DATA}/PTEM/GM070OBG.BGM070MO
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g +1 FSM026 ${DATA}/PTEM/GM070OBG.BGM070NO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM070 
       JUMP_LABEL=GM070OBH
       ;;
(GM070OBH)
       m_CondExec 04,GE,GM070OBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GM070OBJ
       ;;
(GM070OBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/GM070OBG.BGM070CO
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GM070OBJ.BGM070GO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_87_7 87 CH 7
 /FIELDS FLD_CH_10_2 10 CH 2
 /FIELDS FLD_CH_268_1 268 CH 1
 /KEYS
   FLD_CH_10_2 ASCENDING,
   FLD_CH_87_7 ASCENDING,
   FLD_CH_268_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070OBK
       ;;
(GM070OBK)
       m_CondExec 00,EQ,GM070OBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GM070OBM
       ;;
(GM070OBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/GM070OBG.BGM070DO
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GM070OBM.BGM070HO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_87_7 87 CH 7
 /FIELDS FLD_CH_268_1 268 CH 1
 /FIELDS FLD_CH_10_2 10 CH 2
 /KEYS
   FLD_CH_10_2 ASCENDING,
   FLD_CH_87_7 ASCENDING,
   FLD_CH_268_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070OBN
       ;;
(GM070OBN)
       m_CondExec 00,EQ,GM070OBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GM070OBQ
       ;;
(GM070OBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PTEM/GM070OBG.BGM070AO
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -g +1 SORTOUT ${DATA}/PTEM/GM070OBQ.BGM070IO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_229_2 229 CH 2
 /FIELDS FLD_CH_24_1 24 CH 1
 /FIELDS FLD_CH_32_7 32 CH 7
 /FIELDS FLD_CH_25_7 25 CH 7
 /KEYS
   FLD_CH_229_2 ASCENDING,
   FLD_CH_32_7 ASCENDING,
   FLD_CH_24_1 DESCENDING,
   FLD_CH_25_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070OBR
       ;;
(GM070OBR)
       m_CondExec 00,EQ,GM070OBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GM070OBT
       ;;
(GM070OBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PTEM/GM070OBG.BGM070EO
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -g +1 SORTOUT ${DATA}/PTEM/GM070OBT.BGM070JO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_24_1 24 CH 1
 /FIELDS FLD_CH_229_2 229 CH 2
 /FIELDS FLD_CH_25_7 25 CH 7
 /KEYS
   FLD_CH_229_2 ASCENDING,
   FLD_CH_25_7 ASCENDING,
   FLD_CH_24_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070OBU
       ;;
(GM070OBU)
       m_CondExec 00,EQ,GM070OBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GM070OBX
       ;;
(GM070OBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PTEM/GM070OBG.BGM070BO
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -g +1 SORTOUT ${DATA}/PTEM/GM070OBX.BGM070KO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_15_2 15 CH 2
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_22_1 22 CH 1
 /FIELDS FLD_CH_8_7 8 CH 7
 /KEYS
   FLD_CH_15_2 ASCENDING,
   FLD_CH_8_7 ASCENDING,
   FLD_CH_22_1 DESCENDING,
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070OBY
       ;;
(GM070OBY)
       m_CondExec 00,EQ,GM070OBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GM070OCA
       ;;
(GM070OCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A19} SORTIN ${DATA}/PTEM/GM070OBG.BGM070FO
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -g +1 SORTOUT ${DATA}/PTEM/GM070OCA.BGM070LO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 01 CH 7
 /FIELDS FLD_CH_15_2 15 CH 2
 /FIELDS FLD_CH_22_1 22 CH 1
 /KEYS
   FLD_CH_15_2 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_22_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070OCB
       ;;
(GM070OCB)
       m_CondExec 00,EQ,GM070OCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GM070OCD
       ;;
(GM070OCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A20} SORTIN ${DATA}/PTEM/GM070OBG.BGM070MO
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g +1 SORTOUT ${DATA}/PTEM/GM070OCD.BGM070OO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_111_1 111 CH 1
 /FIELDS FLD_CH_1_16 1 CH 16
 /KEYS
   FLD_CH_1_16 ASCENDING,
   FLD_CH_104_7 ASCENDING,
   FLD_CH_111_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070OCE
       ;;
(GM070OCE)
       m_CondExec 00,EQ,GM070OCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GM070OCG
       ;;
(GM070OCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A21} SORTIN ${DATA}/PTEM/GM070OBG.BGM070NO
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g +1 SORTOUT ${DATA}/PTEM/GM070OCG.BGM070PO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_16 1 CH 16
 /FIELDS FLD_CH_111_1 111 CH 1
 /FIELDS FLD_CH_97_7 97 CH 7
 /KEYS
   FLD_CH_1_16 ASCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_111_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070OCH
       ;;
(GM070OCH)
       m_CondExec 00,EQ,GM070OCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM075                                                                      
#  ALIMENTATION DU CAHIER BLEU                                                 
#  ACCES A LA SOUS TABLE GMPRI                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GM070OCJ
       ;;
(GM070OCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i FDATE
$FDATE
_end
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
#  2 NOUVEAUX FICHIERS RELU EN 227                                             
       m_FileAssign -d SHR -g ${G_A22} FSM025 ${DATA}/PTEM/GM070OCD.BGM070OO
       m_FileAssign -d SHR -g ${G_A23} FSM026 ${DATA}/PTEM/GM070OCG.BGM070PO
       m_FileAssign -d SHR -g ${G_A24} FGM70 ${DATA}/PTEM/GM070OBM.BGM070HO
       m_FileAssign -d SHR -g ${G_A25} FGM075 ${DATA}/PTEM/GM070OBJ.BGM070GO
       m_FileAssign -d SHR -g ${G_A26} RTGM01 ${DATA}/PTEM/GM070OBQ.BGM070IO
       m_FileAssign -d SHR -g ${G_A27} RTGM06 ${DATA}/PTEM/GM070OBX.BGM070KO
       m_FileAssign -d SHR -g ${G_A28} FGM01 ${DATA}/PTEM/GM070OBT.BGM070JO
       m_FileAssign -d SHR -g ${G_A29} FGM06 ${DATA}/PTEM/GM070OCA.BGM070LO
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IGM075 ${DATA}/PTEM/GM070OCJ.BGM075AO
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -g +1 RTGM01S ${DATA}/PTEM/GM070OCJ.BGM075BO
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -g +1 RTGM06S ${DATA}/PTEM/GM070OCJ.BGM075CO
#  2 NOVEAUX FICHIERS DE SORTIE                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g +1 FSM025S ${DATA}/PXX0/F16.BGM075DO
       m_FileAssign -d NEW,CATLG,DELETE -r 14 -g +1 FGM080 ${DATA}/PTEM/GM070OCJ.BGM075EO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM075 
       JUMP_LABEL=GM070OCK
       ;;
(GM070OCK)
       m_CondExec 04,GE,GM070OCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR LOAD RTGM01                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GM070OCM
       ;;
(GM070OCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A30} SORTIN ${DATA}/PTEM/GM070OCJ.BGM075BO
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -g +1 SORTOUT ${DATA}/PGG916/F16.RELOAD.GM01RO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_25_14 25 CH 14
 /KEYS
   FLD_CH_25_14 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070OCN
       ;;
(GM070OCN)
       m_CondExec 00,EQ,GM070OCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR LOAD RTGM06                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OCQ PGM=SORT       ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GM070OCQ
       ;;
(GM070OCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A31} SORTIN ${DATA}/PTEM/GM070OCJ.BGM075CO
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -g +1 SORTOUT ${DATA}/PGG916/F16.RELOAD.GM06RO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_16 01 CH 16
 /KEYS
   FLD_CH_1_16 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070OCR
       ;;
(GM070OCR)
       m_CondExec 00,EQ,GM070OCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR LOAD RTGM70                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GM070OCT
       ;;
(GM070OCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A32} SORTIN ${DATA}/PTEM/GM070OCJ.BGM075AO
       m_FileAssign -d NEW,CATLG,DELETE -r 402 -g +1 SORTOUT ${DATA}/PEX0/F16.RELOAD.GM70RO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_94_2 94 CH 2
 /FIELDS FLD_CH_7_401 07 CH 401
 /FIELDS FLD_PD_17_3 17 PD 3
 /FIELDS FLD_CH_7_10 07 CH 10
 /FIELDS FLD_CH_20_74 20 CH 74
 /KEYS
   FLD_CH_7_10 ASCENDING,
   FLD_PD_17_3 ASCENDING,
   FLD_CH_20_74 ASCENDING,
   FLD_BI_94_2 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_7_401
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GM070OCU
       ;;
(GM070OCU)
       m_CondExec 00,EQ,GM070OCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGM01                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OCX PGM=DSNUTILB   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=GM070OCX
       ;;
(GM070OCX)
       m_CondExec ${EXAEL},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ****** FIC DE LOAD                                                           
       m_FileAssign -d SHR -g ${G_A33} SYSREC ${DATA}/PGG916/F16.RELOAD.GM01RO
#    RSGM01O  : NAME=RSGM01O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGM01O /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070OCX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GM070O_GM070OCX_RTGM01.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GM070OCY
       ;;
(GM070OCY)
       m_CondExec 04,GE,GM070OCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGM06                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070ODA PGM=DSNUTILB   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=GM070ODA
       ;;
(GM070ODA)
       m_CondExec ${EXAEQ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ****** FIC DE LOAD                                                           
       m_FileAssign -d SHR -g ${G_A34} SYSREC ${DATA}/PGG916/F16.RELOAD.GM06RO
#    RSGM06O  : NAME=RSGM06O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGM06O /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070ODA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GM070O_GM070ODA_RTGM06.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GM070ODB
       ;;
(GM070ODB)
       m_CondExec 04,GE,GM070ODA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGM70                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070ODD PGM=DSNUTILB   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=GM070ODD
       ;;
(GM070ODD)
       m_CondExec ${EXAEV},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ****** FIC DE LOAD                                                           
       m_FileAssign -d SHR -g ${G_A35} SYSREC ${DATA}/PEX0/F16.RELOAD.GM70RO
#    RSGM70O  : NAME=RSGM70O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGM70O /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070ODD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GM070O_GM070ODD_RTGM70.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GM070ODE
       ;;
(GM070ODE)
       m_CondExec 04,GE,GM070ODD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM020                                                                      
#  EDITION CAHIER-BLEU (TRI SANS MARQUE)                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070ODG PGM=IKJEFT01   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=GM070ODG
       ;;
(GM070ODG)
       m_CondExec ${EXAFA},NE,YES 1,EQ,$[MERCRED] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE.                                                   
#    RSGA01O  : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01O /dev/null
# ******  TABLE DES CHEFS DE PRODUITS.                                         
#    RSGA02   : NAME=RSGA02,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA02 /dev/null
# ******  TABLE DES LIEUX                                                      
#    RSGA10O  : NAME=RSGA10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA10O /dev/null
# ******  TABLE DES FAMILLES.                                                  
#    RSGA14O  : NAME=RSGA14O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA14O /dev/null
# ******  TABLE VALEURS CODES MARKETING PAR ARTICLE.                           
#    RSGA26O  : NAME=RSGA26O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA26O /dev/null
# ******  TABLE CAHIER-BLEU BATCH.                                             
#    RSGM01O  : NAME=RSGM01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGM01O /dev/null
# ****** TABLE CAHIER-BLEU PAR ZONES DE PRIX                                   
#    RSGM06O  : NAME=RSGM06O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGM06O /dev/null
# ******  RELATION CODIC - SRP                                                 
       m_FileAssign -d SHR -g ${G_A36} FGM020 ${DATA}/PTEM/GM070OAT.BGM069BO
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  SOCIETE : 916000                                                     
       m_FileAssign -d SHR FSOCLIEU ${DATA}/CORTEX4.P.MTXTFIX1/R916SOC
#  NBRE DE JOURS DES EPUISES RECENTS 3C,% ECART PRIX 2C,% ECART COMM 2         
       m_FileAssign -d SHR PARAM ${DATA}/CORTEX4.P.MTXTFIX3/GM070ODG
# ******  FICHIER D'EDITION.                                                   
       m_OutputAssign -c 9 -w BGM020 IMPR
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM020 
       JUMP_LABEL=GM070ODH
       ;;
(GM070ODH)
       m_CondExec 04,GE,GM070ODG ${EXAFA},NE,YES 1,EQ,$[MERCRED] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM025    LE 1ER MERCREDI DU MOIS                                           
#  EDITION CAHIER-BLEU AVEC ARTICLES EPUISES ET EN CONTREMARQUE      *         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070ODJ PGM=IKJEFT01   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=GM070ODJ
       ;;
(GM070ODJ)
       m_CondExec ${EXAFF},NE,YES 1,EQ,$[FIRSTME] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE.                                                   
#    RSGA01O  : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01O /dev/null
# ******  TABLE DES CHEFS DE PRODUITS.                                         
#    RSGA02   : NAME=RSGA02,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA02 /dev/null
# ******  TABLE DES LIEUX                                                      
#    RSGA10O  : NAME=RSGA10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA10O /dev/null
# ******  TABLE DES FAMILLES.                                                  
#    RSGA14O  : NAME=RSGA14O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA14O /dev/null
# ******  TABLE VALEURS CODES MARKETING PAR ARTICLE.                           
#    RSGA26O  : NAME=RSGA26O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA26O /dev/null
# ******  TABLE CAHIER-BLEU BATCH.                                             
#    RSGM01O  : NAME=RSGM01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGM01O /dev/null
# ******  TABLE CAHIER-BLEU PAR ZONES DE PRIX                                  
#    RSGM06O  : NAME=RSGM06O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGM06O /dev/null
#                                                                              
# ******  CARTE DATE.                                                          
       m_FileAssign -i FDATE
$FDATE
_end
#  NBRE DE JOURS DES EPUISES RECENTS 3C,% ECART PRIX 2C,% ECART COMM 2         
       m_FileAssign -d SHR PARAM ${DATA}/CORTEX4.P.MTXTFIX1/GM070ODJ
# ******  FICHIER D'EDITION.                                                   
       m_OutputAssign -c 9 -w BGM025 IMPR
# ******  SOCIETE : 916000                                                     
       m_FileAssign -d SHR FSOCLIEU ${DATA}/CORTEX4.P.MTXTFIX1/R916SOC
#                                                                              
# ******  CARTE PARAMETRE.                                                     
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM025 
       JUMP_LABEL=GM070ODK
       ;;
(GM070ODK)
       m_CondExec 04,GE,GM070ODJ ${EXAFF},NE,YES 1,EQ,$[FIRSTME] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070ODM PGM=SORT       ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=GM070ODM
       ;;
(GM070ODM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A37} SORTIN ${DATA}/PTEM/GM070OCJ.BGM075AO
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GM070ODM.BGM080AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_270_1 270 CH 1
 /FIELDS FLD_CH_7_10 07 CH 10
 /FIELDS FLD_CH_87_7 87 CH 7
 /FIELDS FLD_PD_17_3 17 PD 3
 /FIELDS FLD_CH_20_67 20 CH 67
 /KEYS
   FLD_CH_7_10 ASCENDING,
   FLD_PD_17_3 ASCENDING,
   FLD_CH_20_67 ASCENDING,
   FLD_CH_270_1 ASCENDING,
   FLD_CH_87_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070ODN
       ;;
(GM070ODN)
       m_CondExec 00,EQ,GM070ODM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070ODQ PGM=SORT       ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=GM070ODQ
       ;;
(GM070ODQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A38} SORTIN ${DATA}/PTEM/GM070OCJ.BGM075EO
       m_FileAssign -d NEW,CATLG,DELETE -r 14 -g +1 SORTOUT ${DATA}/PTEM/GM070ODQ.BGM075FO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_7 01 CH 7
 /FIELDS FLD_BI_8_2 08 CH 2
 /KEYS
   FLD_BI_8_2 ASCENDING,
   FLD_BI_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070ODR
       ;;
(GM070ODR)
       m_CondExec 00,EQ,GM070ODQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGM080                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070ODT PGM=IKJEFT01   ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=GM070ODT
       ;;
(GM070ODT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTGA11   : NAME=RSGA11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA11 /dev/null
#  FICHIER FDATE                                                               
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER FNSOC                                                               
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#  FICHIER ENTRE                                                               
       m_FileAssign -d SHR -g ${G_A39} FGM075 ${DATA}/PTEM/GM070ODM.BGM080AO
       m_FileAssign -d SHR -g ${G_A40} FGM080 ${DATA}/PTEM/GM070ODQ.BGM075FO
#  FICHIER SORTIE                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IGM080 ${DATA}/PTEM/GM070ODT.BGM080BO
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IGM085 ${DATA}/PTEM/GM070ODT.BGM085AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM080 
       JUMP_LABEL=GM070ODU
       ;;
(GM070ODU)
       m_CondExec 04,GE,GM070ODT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER IGM080                                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070ODX PGM=SORT       ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=GM070ODX
       ;;
(GM070ODX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A41} SORTIN ${DATA}/PTEM/GM070ODT.BGM080BO
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GM070ODX.BGM080EO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_20_3 20 PD 3
 /FIELDS FLD_CH_23_60 23 CH 60
 /FIELDS FLD_PD_148_5 148 PD 5
 /FIELDS FLD_CH_7_10 7 CH 10
 /KEYS
   FLD_CH_7_10 ASCENDING,
   FLD_PD_20_3 ASCENDING,
   FLD_CH_23_60 ASCENDING,
   FLD_PD_148_5 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070ODY
       ;;
(GM070ODY)
       m_CondExec 00,EQ,GM070ODX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OEA PGM=IKJEFT01   ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=GM070OEA
       ;;
(GM070OEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A42} FEXTRAC ${DATA}/PTEM/GM070ODX.BGM080EO
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GM070OEA.BGM080FO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GM070OEB
       ;;
(GM070OEB)
       m_CondExec 04,GE,GM070OEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OED PGM=SORT       ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=GM070OED
       ;;
(GM070OED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A43} SORTIN ${DATA}/PTEM/GM070OEA.BGM080FO
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GM070OED.BGM080GO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070OEE
       ;;
(GM070OEE)
       m_CondExec 00,EQ,GM070OED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT : IGM080                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OEG PGM=IKJEFT01   ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=GM070OEG
       ;;
(GM070OEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A44} FEXTRAC ${DATA}/PTEM/GM070ODX.BGM080EO
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A45} FCUMULS ${DATA}/PTEM/GM070OEA.BGM080FO
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IGM080 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GM070OEH
       ;;
(GM070OEH)
       m_CondExec 04,GE,GM070OEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER IGM085                                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OEJ PGM=SORT       ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=GM070OEJ
       ;;
(GM070OEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A46} SORTIN ${DATA}/PTEM/GM070ODT.BGM085AO
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GM070OEJ.BGM085BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_20_3 20 PD 3
 /FIELDS FLD_CH_23_60 23 CH 60
 /FIELDS FLD_CH_7_10 7 CH 10
 /FIELDS FLD_PD_148_5 148 PD 5
 /KEYS
   FLD_CH_7_10 ASCENDING,
   FLD_PD_20_3 ASCENDING,
   FLD_CH_23_60 ASCENDING,
   FLD_PD_148_5 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070OEK
       ;;
(GM070OEK)
       m_CondExec 00,EQ,GM070OEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OEM PGM=IKJEFT01   ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=GM070OEM
       ;;
(GM070OEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A47} FEXTRAC ${DATA}/PTEM/GM070OEJ.BGM085BO
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GM070OEM.BGM085CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GM070OEN
       ;;
(GM070OEN)
       m_CondExec 04,GE,GM070OEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OEQ PGM=SORT       ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=GM070OEQ
       ;;
(GM070OEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A48} SORTIN ${DATA}/PTEM/GM070OEM.BGM085CO
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GM070OEQ.BGM085DO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070OER
       ;;
(GM070OER)
       m_CondExec 00,EQ,GM070OEQ ${EXAHD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT : IGM085                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070OET PGM=IKJEFT01   ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=GM070OET
       ;;
(GM070OET)
       m_CondExec ${EXAHI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A49} FEXTRAC ${DATA}/PTEM/GM070OEJ.BGM085BO
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A50} FCUMULS ${DATA}/PTEM/GM070OEQ.BGM085DO
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IGM085 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GM070OEU
       ;;
(GM070OEU)
       m_CondExec 04,GE,GM070OET ${EXAHI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GM070OZA
       ;;
(GM070OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
