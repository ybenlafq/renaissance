#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BGG98F.ksh                       --- VERSION DU 08/10/2016 22:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFBGG98 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/14 AT 15.09.52 BY BURTECA                      
#    STANDARDS: P  JOBSET: BGG98F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
#                                                                              
#  DELETE DES FICHERS SORTANT DES CHAINES BGG71* ET BGG70*                     
#  REPRISE OUI                                                                 
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=BGG98FA
       ;;
(BGG98FA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2015/04/14 AT 15.09.52 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: BGG98F                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'CUMULS COEFF'                          
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=BGG98FAA
       ;;
(BGG98FAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ***************************************                                      
# * DEPENDANCES POUR PLAN :             *                                      
# *   OBLIGATOIRE POUR LOGIQUE APPL     *                                      
# ***************************************                                      
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BGG98FAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=BGG98FAB
       ;;
(BGG98FAB)
       m_CondExec 16,NE,BGG98FAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP DES FICHERS BGG701* DES FILIALES                                        
# ********************************************************************         
# AAF      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -NOSYSIN                                                                     
# -ACTION(ADD)                                                                 
# -TRAN(ASCII850)                                                              
# -ARCHIVE_SPACE_PRIMARY(00002000)                                             
# -ARCHIVE_SPACE_SECONDARY(00002000)                                           
# -ARCHUNIT(TEM350)                                                            
# -ARCHIVE(PXX0.F99.BGG701AF)                                                  
# -ZIPPED_DSN(PXX0.F91.BGG701AD.COEF.DPM,BGG701AD.COEF.DPM.TXT)                
# -ZIPPED_DSN(PXX0.F61.BGG701AL.COEF.DNN,BGG701AL.COEF.DNN.TXT)                
# -ZIPPED_DSN(PXX0.F89.BGG701AM.COEF.DAL,BGG701AM.COEF.DAL.TXT)                
# -ZIPPED_DSN(PXX0.F16.BGG701AO.COEF.DOUEST,BGG701AO.COEF.DOUEST.TXT)          
# -ZIPPED_DSN(PXX0.F07.BGG701AP.COEF.DIF,BGG701AP.COEF.DIF.TXT)                
# -ZIPPED_DSN(PXX0.F08.BGG701AX.COEF.LUXEMB,BGG701AX.COEF.LUXEMB.TXT)          
# -ZIPPED_DSN(PXX0.F45.BGG701AY.COEF.DRA,BGG701AY.COEF.DRA.TXT)                
# PXX0.F91.BGG701AD.COEF.DPM                                                   
# PXX0.F61.BGG701AL.COEF.DNN                                                   
# PXX0.F89.BGG701AM.COEF.DAL                                                   
# PXX0.F16.BGG701AO.COEF.DOUEST                                                
# PXX0.F07.BGG701AP.COEF.DIF                                                   
# PXX0.F08.BGG701AX.COEF.LUXEMB                                                
# PXX0.F45.BGG701AY.COEF.DRA                                                   
#          DATAEND                                                             
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BGG98FAD PGM=JVMLDM76   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=BGG98FAD
       ;;
(BGG98FAD)
       m_CondExec ${EXAAF},NE,YES 
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 90 -t LSEQ FICZIP ${DATA}/PXX0.F99.BGG701AF
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BGG98FAD.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTBGG98F                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BGG98FAG PGM=FTP        ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=BGG98FAG
       ;;
(BGG98FAG)
       m_CondExec ${EXAAK},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/BGG98FAG.sysin
       m_UtilityExec INPUT
# ********************************************************************         
#  DELETE DES FICHERS SORTANT DES CHAINES BGG701*                              
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BGG98FAJ PGM=IDCAMS     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=BGG98FAJ
       ;;
(BGG98FAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d SHR IN4 /dev/null
       m_FileAssign -d SHR IN5 /dev/null
       m_FileAssign -d SHR IN6 /dev/null
       m_FileAssign -d SHR IN8 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BGG98FAJ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=BGG98FAK
       ;;
(BGG98FAK)
       m_CondExec 16,NE,BGG98FAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ******************************************                                   
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
