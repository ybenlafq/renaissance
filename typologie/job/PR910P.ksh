#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PR910P.ksh                       --- VERSION DU 09/10/2016 00:39
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPPR910 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/11/30 AT 13.42.29 BY BURTECN                      
#    STANDARDS: P  JOBSET: PR910P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   TRI DU FICHIER BFS052FP                                                    
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PR910PA
       ;;
(PR910PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PR910PAA
       ;;
(PR910PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGP/F07.BFS052FP
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -g +1 SORTOUT ${DATA}/PTEM/PR910PAA.BPR910AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_44_8 44 CH 08
 /KEYS
   FLD_CH_44_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR910PAB
       ;;
(PR910PAB)
       m_CondExec 00,EQ,PR910PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPR910                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR910PAD PGM=BPR910     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PR910PAD
       ;;
(PR910PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
# ************PARAMETRE MOIS                                                   
       m_FileAssign -i FMOIS
$FMOIS
_end
# **                                                                           
       m_FileAssign -d SHR -g ${G_A1} FFS052 ${DATA}/PTEM/PR910PAA.BPR910AP
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -g +1 FPR910 ${DATA}/PNCGP/F07.BFS052FP
       m_ProgramExec BPR910 
# ********************************************************************         
#   TRI DU FICHIER BFV135EP                                                    
# ********************************************************************         
# AAK      SORT                                                                
# SORTIN   FILE  NAME=BGV135EP,MODE=I                                          
# SORTOUT  FILE  NAME=BPR910BP,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(44,08,CH,A)                                                    
#          DATAEND                                                             
# ********************************************************************         
#  PGM : BPR910                                                                
# ********************************************************************         
# AAP      STEP  PGM=BPR910,LANG=UTIL                                          
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# *************************************PARAMETRE MOIS                          
# FMOIS    DATA  CLASS=VAR,PARMS=FMOISP                                        
# FFS052   FILE  NAME=BPR910BP,MODE=I                                          
# FPR910   FILE  NAME=BGV135EP,MODE=O                                          
# *******                                                                      
#  DEPENDANCE POUR PLAN                                                        
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PR910PZA
       ;;
(PR910PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PR910PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
