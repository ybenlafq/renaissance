#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GHA01O.ksh                       --- VERSION DU 17/10/2016 18:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGHA01 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/09/29 AT 14.25.36 BY BURTECN                      
#    STANDARDS: P  JOBSET: GHA01O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  CETTE CHAINE PEUT-ETRE REPORTEE APRES ACCORD D UN RESPONSABLE               
#  RISQUE DE PERTE D INFORMATION SUR LES TABLES ARTICLES                       
#  PAR RAPPORT AUX SAISIES DE PARIS                                            
# ********************************************************************         
#  BHA002 : CREATION DU FIC DE LOAD POUR LA TABLE P916.RTGA01                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GHA01OA
       ;;
(GHA01OA)
#
#GHA01OBA
#GHA01OBA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GHA01OBA
#
#
#GHA01OAJ
#GHA01OAJ Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GHA01OAJ
#
       C_A1=${C_A1:-0}
       C_A2=${C_A2:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GHA01OAA
       ;;
(GHA01OAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVEGARDES CICS            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE P916.RTGA01                                        
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#                                                                              
# ******* FIC DES SS TABLES NON MODIFIABLES                                    
       m_FileAssign -d SHR -g +0 FHA001 ${DATA}/PXX0/F07.BHA001AP
#                                                                              
# ******* FIC DE LOAD POUR P916.RTGA01                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FHA002 ${DATA}/PTEM/GHA01OAA.BHA002AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA002 
       JUMP_LABEL=GHA01OAB
       ;;
(GHA01OAB)
       m_CondExec 04,GE,GHA01OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FIC DONNEES ARTICLES AVANT LE LOAD DE P916.RTGA01                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01OAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GHA01OAD
       ;;
(GHA01OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC DONNEES ARTICLES DE P916.RTGA01                                  
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GHA01OAA.BHA002AO
# ******* FIC DONNEES ARTICLES TRI�                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PNCGO/F16.RELOAD.GA01RO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_6_15 6 CH 15
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_6_15 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GHA01OAE
       ;;
(GHA01OAE)
       m_CondExec 00,EQ,GHA01OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#           LOAD DE LA TABLE GENERALISEES OUEST P916.RTGA01                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01OAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GHA01OAG
       ;;
(GHA01OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE GENERALISEE P916.RTGA01                                        
#    RSGA01   : NAME=RSGA01O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* FIC DE LOAD POUR  P916.RTGA01                                        
       m_FileAssign -d SHR -g ${G_A2} SYSREC ${DATA}/PNCGO/F16.RELOAD.GA01RO
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01OAG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GHA01O_GHA01OAG_RTGA01.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GHA01OAH
       ;;
(GHA01OAH)
       m_CondExec 04,GE,GHA01OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REPAIR NOCOPYPEND DU TABLESPACE RSGA01O DE LA D BASE PODGA00     *         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01OAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GHA01OAM
       ;;
(GHA01OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ASSO ARTICLE/PROFIL                                            
#    RSGA73   : NAME=RSGA73O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA73 /dev/null
# ******* TABLE TEXT ETIQUETTE INFO                                            
#    RSGA74   : NAME=RSGA74O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA74 /dev/null
#                                                                              
# ******* FIC EXTRACT DES MODIFS DU JOUR SUR PDARTY.RTGA73                     
       m_FileAssign -d SHR -g +0 FHA73 ${DATA}/PXX0/F07.BHA003AP
# ******* FIC EXTRACT DES MODIFS DU JOUR SUR PDARTY.RTGA74                     
       m_FileAssign -d SHR -g +0 FHA74 ${DATA}/PXX0/F07.BHA003BP
#                                                                              
# ****** TABLE ARTICLE                                                         
#    RSGA00   : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA004 
       JUMP_LABEL=GHA01OAN
       ;;
(GHA01OAN)
       m_CondExec 04,GE,GHA01OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHA009 : MISE A JOUR DE LA TABLE P916.RTGA06                      *         
#           MISE A JOUR DE LA TABLE P916.RTFT10                      *         
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01OAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GHA01OAQ
       ;;
(GHA01OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FIC EXTRACTS ENTITES DE COMMANDES PROVENANT DE NCP                   
       m_FileAssign -d SHR -g +0 FHA005 ${DATA}/PXX0/F07.BHA005AP
#                                                                              
# ******* TABLE GENERALISEE RTGA01                                             
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE ENTITES DE COMMANDES RTGA06                                    
#    RSGA06   : NAME=RSGA06O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA06 /dev/null
# ******* TABLE DES REPERTOIRES FOURNISSEURS                                   
#    RSFT10   : NAME=RSFT10O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT10 /dev/null
# ******* TABLE FM                                                             
#    RSFM01   : NAME=RSFM01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM01 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER FSOC                                                         
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA009 
       JUMP_LABEL=GHA01OAR
       ;;
(GHA01OAR)
       m_CondExec 04,GE,GHA01OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FIC DONNEES ARTICLES AVANT LE LOAD DE P916.RTHA30                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01OAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GHA01OAT
       ;;
(GHA01OAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC DONNEES ARTICLES DE P916.RTHA30                                  
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BHA030CP
# ******* FIC DONNEES ARTICLES TRI�                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 73 -t LSEQ -g +1 SORTOUT ${DATA}/PNCGO/F16.RELOAD.HA30RO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GHA01OAU
       ;;
(GHA01OAU)
       m_CondExec 00,EQ,GHA01OAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE ARTICLES FILIALES  P916.RTHA30                   *         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01OAX PGM=DSNUTILB   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GHA01OAX
       ;;
(GHA01OAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE ARTICLES FILIALES  P916.RTHA30                                 
#    RSHA30   : NAME=RSHA30O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSHA30 /dev/null
# ******* FIC DE LOAD POUR  P916.RTHA30                                        
       m_FileAssign -d SHR -g ${G_A3} SYSREC ${DATA}/PNCGO/F16.RELOAD.HA30RO
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01OAX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GHA01O_GHA01OAX_RTHA30.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GHA01OAY
       ;;
(GHA01OAY)
       m_CondExec 04,GE,GHA01OAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REPAIR NOCOPYPEND DU TABLESPACE RSHA30R DE LA D BASE PODGA00               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01OBA PGM=DSNUTILB   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GHA01OBD
       ;;
(GHA01OBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* N� DE SOCIETE A TRAITER                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FIC EXTRACTS ARTICLES PROVENANT DE NCP                               
       m_FileAssign -d SHR -g +0 FHA00 ${DATA}/PXX0/F07.BHA050AP
# ******* FIC EXTRACTS ARTICLES PROVENANT DE NCP                               
       m_FileAssign -d SHR -g +0 FHA03 ${DATA}/PXX0/F07.BHA050IP
# ******* FIC EXTRACTS ARTICLES PROVENANT DE NCP                               
       m_FileAssign -d SHR -g +0 FHA31 ${DATA}/PXX0/F07.BHA050JP
# ******* FIC EXTRACTS ARTICLES PROVENANT DE NCP                               
       m_FileAssign -d SHR -g +0 FHA33 ${DATA}/PXX0/F07.BHA050KP
# ******* FIC EXTRACTS RELATION ARTICLE/DECLARATION DE NCP                     
       m_FileAssign -d SHR -g +0 FHA51 ${DATA}/PXX0/F07.BHA050BP
# ******* FIC EXTRACTS RELATION ARTICLE/GARANTIE DE NCP                        
       m_FileAssign -d SHR -g +0 FHA52 ${DATA}/PXX0/F07.BHA050CP
# ******* FIC EXTRACTS ARTICLE/CODE DESCRIPTIF DE NCP                          
       m_FileAssign -d SHR -g +0 FHA53 ${DATA}/PXX0/F07.BHA050DP
# ******* FIC EXTRACTS ARTICLE/ENTITE DE COMMANDE DE NCP                       
       m_FileAssign -d SHR -g +0 FHA55 ${DATA}/PXX0/F07.BHA050EP
# ******* FIC EXTRACTS ARTICLE/ARTICLE DE NCP                                  
       m_FileAssign -d SHR -g +0 FHA58 ${DATA}/PXX0/F07.BHA050FP
# ******* FILE EXTRACTS ZONE DE TRI/PRIX STANDARD                              
       m_FileAssign -d SHR -g +0 FHA59 ${DATA}/PXX0/F07.BHA050GP
# ******* FILE EXTRACTS CARACTERISTIQUES SPECIFIQUES                           
       m_FileAssign -d SHR -g +0 FHA63 ${DATA}/PXX0/F07.BHA050HP
# ******* FIC  EXTRACTS DE GHA01P SUITE MEX PCM                                
       m_FileAssign -d SHR -g +0 FHA40 ${DATA}/PXX0/F07.BHA040AP
       m_FileAssign -d SHR -g +0 FHA41 ${DATA}/PXX0/F07.BHA041AP
       m_FileAssign -d SHR -g +0 FHA42 ${DATA}/PXX0/F07.BHA042AP
#                                                                              
# **TABLES EN MAJ                                                              
#                                                                              
# ****** TABLE ARTICLE                                                         
#    RSGA00   : NAME=RSGA00O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ****** TABLE ARTICLE                                                         
#    RSGA03   : NAME=RSGA03O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA03 /dev/null
# ****** TABLE ARTICLE                                                         
#    RSGA31   : NAME=RSGA31O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA31 /dev/null
# ****** TABLE ARTICLE                                                         
#    RSGA33   : NAME=RSGA33O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA33 /dev/null
# ******* TABLE RELATION ARTICLE/DECLARATION                                   
#    RSGA51   : NAME=RSGA51O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA51 /dev/null
# ******* TABLE RELATION ARTICLE/GARANTIE                                      
#    RSGA52   : NAME=RSGA52O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA52 /dev/null
# ******* TABLE RELATION ARTICLE/CODE DESCRIPTIF                               
#    RSGA53   : NAME=RSGA53O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA53 /dev/null
# ******* TABLE RELATION ARTICLE/ENTITE DE COMMANDE                            
#    RSGA55   : NAME=RSGA55O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA55 /dev/null
# ******* TABLE RELATION ARTICLE/ARTICLE                                       
#    RSGA58   : NAME=RSGA58O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58 /dev/null
# ******* TABLE ARTICLES : ZONE DE PRIX - PRIX STANDARDS                       
#    RSGA59   : NAME=RSGA59O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
# ******* TABLE ARTICLES : CARACTERISTIQUES SPECIFIQUES                        
#    RSGA63   : NAME=RSGA63O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA63 /dev/null
# ******* TABLE ARTICLES : MODE DE DELIVRANCE                                  
#    RSGA64   : NAME=RSGA64O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA64 /dev/null
# **TABLES EN LECTURE                                                          
#                                                                              
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE FAMILLE / MODE DE DELIVRANCE                                   
#    RSGA13   : NAME=RSGA13O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA13 /dev/null
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE FAMILLES : PARAMETRES ASSOCIES AUX FAMILLES                    
#    RSGA30   : NAME=RSGA30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA051 
       JUMP_LABEL=GHA01OBE
       ;;
(GHA01OBE)
       m_CondExec 04,GE,GHA01OBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHA008 : MISE A JOUR DES CODES PARAM FAMILLE SUR LA RTGA30 A                
#           PARTIE DU FICHIER FGA30AF ISSU DE GHA01P                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01OBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GHA01OBG
       ;;
(GHA01OBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* FIC EXTRACTS ARTICLES PROVENANT DE NCP                               
       m_FileAssign -d SHR -g +0 FGA30 ${DATA}/PXX0/F99.FGA30AF
# ****** TABLE PARAM FAMILLE                                                   
#    RSGA30   : NAME=RSGA30O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA008 
       JUMP_LABEL=GHA01OBH
       ;;
(GHA01OBH)
       m_CondExec 04,GE,GHA01OBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFL001 :                                                                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01OBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GHA01OBJ
       ;;
(GHA01OBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RTGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA19   : NAME=RSGA19O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA19 /dev/null
#    RTFL05   : NAME=RSFL05O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTFL05 /dev/null
#    RTFL06   : NAME=RSFL06O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTFL06 /dev/null
#    RTFL40   : NAME=RSFL40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL40 /dev/null
#    RTFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL50 /dev/null
#    RTGQ05   : NAME=RSGQ05O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGQ05 /dev/null
# ------  TABLES EN MAJ                                                        
#    RTGA00   : NAME=RSGA00O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA65   : NAME=RSGA65O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGA65 /dev/null
#    RTGA66   : NAME=RSGA66O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGA66 /dev/null
# ------  PARAMETRE                                                            
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# -----   FICHIER REPRIS DANS LA CHA�NE FL003P                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 60 -t LSEQ -g +1 FL001 ${DATA}/PNCGO/F16.BFL001AO
# -----   FICHIER POUR EVITER DEPASSEMENT TABLEAU INTERNE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 159 -t LSEQ -g +1 FGA00 ${DATA}/PXX0/F16.BFL001CO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFL001 
       JUMP_LABEL=GHA01OBK
       ;;
(GHA01OBK)
       m_CondExec 04,GE,GHA01OBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTSP00 A PARTIR DU TS RSSP00  :JOB UNLOAD GHA01P         
#  SOUS RDAR                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01OBM PGM=DSNUTILB   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GHA01OBM
       ;;
(GHA01OBM)
       m_CondExec ${EXACI},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSSP00   : NAME=RSSP00O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSSP00 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGP/F07.UNLOAD.RSSP00
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01OBM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GHA01O_GHA01OBM_RTSP00.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GHA01OBN
       ;;
(GHA01OBN)
       m_CondExec 04,GE,GHA01OBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTSP05 A PARTIR DU TS RSSP05  :JOB UNLOAD GHA01P         
#  SOUS RDAR                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01OBQ PGM=DSNUTILB   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GHA01OBQ
       ;;
(GHA01OBQ)
       m_CondExec ${EXACN},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSSP05   : NAME=RSSP05O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSSP05 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGP/F07.UNLOAD.RSSP05
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01OBQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GHA01O_GHA01OBQ_RTSP05.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GHA01OBR
       ;;
(GHA01OBR)
       m_CondExec 04,GE,GHA01OBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTSP15 A PARTIR DU TS RSSP15  :JOB UNLOAD GHA01P         
#  SOUS RDAR                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01OBT PGM=DSNUTILB   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GHA01OBT
       ;;
(GHA01OBT)
       m_CondExec ${EXACS},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSSP15   : NAME=RSSP15O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSSP15 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGP/F07.UNLOAD.RSSP15
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01OBT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GHA01O_GHA01OBT_RTSP15.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GHA01OBU
       ;;
(GHA01OBU)
       m_CondExec 04,GE,GHA01OBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GHA01OZA
       ;;
(GHA01OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
