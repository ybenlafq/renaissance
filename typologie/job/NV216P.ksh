#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NV216P.ksh                       --- VERSION DU 08/10/2016 13:02
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPNV216 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 10/03/05 AT 10.04.43 BY BURTECA                      
#    STANDARDS: P  JOBSET: NV216P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BNV216                                                                      
#  REPRISE OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=NV216PA
       ;;
(NV216PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'0'}
       RUN=${RUN}
       JUMP_LABEL=NV216PAA
       ;;
(NV216PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#   TABLES EN LECTURE                                                          
#                                                                              
# ****** TABLE                                                                 
#    RSGF55   : NAME=RSGF55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF55 /dev/null
# ****** TABLE                                                                 
#    RSGF49   : NAME=RSGF49,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF49 /dev/null
#                                                                              
# ****** CARTE DATE.                                                           
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 268 -g +1 FNV216 ${DATA}/PTEM/NV216PAA.BNV216AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNV216 
       JUMP_LABEL=NV216PAB
       ;;
(NV216PAB)
       m_CondExec 04,GE,NV216PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FICHIER OBTENU CI DESSUS  .LRECL 268                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV216PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=NV216PAD
       ;;
(NV216PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/NV216PAA.BNV216AP
       m_FileAssign -d NEW,CATLG,DELETE -r 268 -g +1 SORTOUT ${DATA}/PXX0/F07.BNV216BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV216PAE
       ;;
(NV216PAE)
       m_CondExec 00,EQ,NV216PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BNV217 :MODULE DE COMPARAISON DU FIC DU JOUR AVEC LA VEILLE                 
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV216PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=NV216PAG
       ;;
(NV216PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ****** TABLE EN M.A.J                                                        
#    RSNV00   : NAME=RSNV00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV00 /dev/null
#                                                                              
# ****** CARTE DATE.                                                           
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#  FICHIER EN ENTREE                                                           
       m_FileAssign -d SHR -g ${G_A2} FNV217J ${DATA}/PXX0/F07.BNV216BP
#                                                                              
#  FICHIER EN ENTREE  DE LA VEILLE                                             
       m_FileAssign -d SHR -g ${G_A3} FNV217J1 ${DATA}/PXX0/F07.BNV216BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNV217 
       JUMP_LABEL=NV216PAH
       ;;
(NV216PAH)
       m_CondExec 04,GE,NV216PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=NV216PZA
       ;;
(NV216PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV216PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
