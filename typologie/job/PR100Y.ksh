#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PR100Y.ksh                       --- VERSION DU 17/10/2016 18:31
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYPR100 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/03/22 AT 15.37.37 BY BURTECO                      
#    STANDARDS: P  JOBSET: PR100Y                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BPR100 :VENTES DES PRESTATIONS PAR MAG/SOC                                  
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PR100YA
       ;;
(PR100YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+2'}
       G_A11=${G_A11:-'+2'}
       G_A12=${G_A12:-'+2'}
       G_A13=${G_A13:-'+2'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+3'}
       G_A16=${G_A16:-'+3'}
       G_A17=${G_A17:-'+3'}
       G_A18=${G_A18:-'+3'}
       G_A19=${G_A19:-'+3'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+3'}
       G_A21=${G_A21:-'+3'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+4'}
       G_A24=${G_A24:-'+4'}
       G_A25=${G_A25:-'+4'}
       G_A26=${G_A26:-'+4'}
       G_A27=${G_A27:-'+4'}
       G_A28=${G_A28:-'+4'}
       G_A29=${G_A29:-'+4'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+5'}
       G_A32=${G_A32:-'+5'}
       G_A33=${G_A33:-'+5'}
       G_A34=${G_A34:-'+5'}
       G_A35=${G_A35:-'+5'}
       G_A36=${G_A36:-'+5'}
       G_A37=${G_A37:-'+5'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+2'}
       G_A8=${G_A8:-'+2'}
       G_A9=${G_A9:-'+2'}
       RUN=${RUN}
       JUMP_LABEL=PR100YAA
       ;;
(PR100YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA02   : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02 /dev/null
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSPR00   : NAME=RSPR00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00 /dev/null
#    RSPR05   : NAME=RSPR05Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR05 /dev/null
#    RSPR11   : NAME=RSPR11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR11 /dev/null
       m_FileAssign -d SHR -g +0 FFS052 ${DATA}/PNCGY/F45.BFS052FY
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IPR101 ${DATA}/PXX0/PR100YAA.BPR100AY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IPR102 ${DATA}/PXX0/PR100YAA.BPR100BY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IPR103 ${DATA}/PXX0/PR100YAA.BPR100CY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IPR104 ${DATA}/PXX0/PR100YAA.BPR100DY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IPR105 ${DATA}/PXX0/PR100YAA.BPR100EY
       m_FileAssign -d SHR FDATEURO ${DATA}/CORTEX4.P.MTXTFIX4/EUROGCA
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPR100 
       JUMP_LABEL=PR100YAB
       ;;
(PR100YAB)
       m_CondExec 04,GE,PR100YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FICHIER POUR LE GEG                                                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR100YAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PR100YAD
       ;;
(PR100YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/PR100YAA.BPR100AY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/PR100YAD.BPR100FY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_129_4 129 PD 4
 /FIELDS FLD_PD_98_5 98 PD 5
 /FIELDS FLD_PD_113_4 113 PD 4
 /FIELDS FLD_PD_121_4 121 PD 4
 /FIELDS FLD_PD_103_5 103 PD 5
 /FIELDS FLD_PD_125_4 125 PD 4
 /FIELDS FLD_PD_117_4 117 PD 4
 /FIELDS FLD_PD_108_5 108 PD 5
 /FIELDS FLD_CH_1_32 01 CH 32
 /KEYS
   FLD_CH_1_32 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_98_5,
    TOTAL FLD_PD_103_5,
    TOTAL FLD_PD_108_5,
    TOTAL FLD_PD_113_4,
    TOTAL FLD_PD_117_4,
    TOTAL FLD_PD_121_4,
    TOTAL FLD_PD_125_4,
    TOTAL FLD_PD_129_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR100YAE
       ;;
(PR100YAE)
       m_CondExec 00,EQ,PR100YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR100YAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PR100YAG
       ;;
(PR100YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A2} FEXTRAC ${DATA}/PXX0/PR100YAD.BPR100FY
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PXX0/PR100YAG.BPR100GY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=PR100YAH
       ;;
(PR100YAH)
       m_CondExec 04,GE,PR100YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR100YAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PR100YAJ
       ;;
(PR100YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/PR100YAG.BPR100GY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/PR100YAJ.BPR100HY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR100YAK
       ;;
(PR100YAK)
       m_CondExec 00,EQ,PR100YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR100YAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PR100YAM
       ;;
(PR100YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PXX0/PR100YAD.BPR100FY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A5} FCUMULS ${DATA}/PXX0/PR100YAJ.BPR100HY
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IPR101 FEDITION
# *** FICHIER D'IMPRESSION DES MGC POUR GEG00X  FILIALES                       
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -g +1 FEG132 ${DATA}/PXX0/F45.BPR100IY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=PR100YAN
       ;;
(PR100YAN)
       m_CondExec 04,GE,PR100YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FICHIER POUR LE GEG                                                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR100YAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PR100YAQ
       ;;
(PR100YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PXX0/PR100YAA.BPR100BY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g ${G_A7} SORTOUT ${DATA}/PXX0/PR100YAD.BPR100FY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_100_5 100 PD 5
 /FIELDS FLD_PD_105_5 105 PD 5
 /FIELDS FLD_CH_1_29 01 CH 29
 /FIELDS FLD_PD_110_4 110 PD 4
 /FIELDS FLD_PD_126_4 126 PD 4
 /FIELDS FLD_PD_95_5 95 PD 5
 /FIELDS FLD_PD_118_4 118 PD 4
 /FIELDS FLD_PD_114_4 114 PD 4
 /FIELDS FLD_PD_122_4 122 PD 4
 /KEYS
   FLD_CH_1_29 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_95_5,
    TOTAL FLD_PD_100_5,
    TOTAL FLD_PD_105_5,
    TOTAL FLD_PD_110_4,
    TOTAL FLD_PD_114_4,
    TOTAL FLD_PD_118_4,
    TOTAL FLD_PD_122_4,
    TOTAL FLD_PD_126_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR100YAR
       ;;
(PR100YAR)
       m_CondExec 00,EQ,PR100YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR100YAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PR100YAT
       ;;
(PR100YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A8} FEXTRAC ${DATA}/PXX0/PR100YAD.BPR100FY
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g ${G_A9} FCUMULS ${DATA}/PXX0/PR100YAG.BPR100GY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=PR100YAU
       ;;
(PR100YAU)
       m_CondExec 04,GE,PR100YAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR100YAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=PR100YAX
       ;;
(PR100YAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PXX0/PR100YAG.BPR100GY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g ${G_A11} SORTOUT ${DATA}/PXX0/PR100YAJ.BPR100HY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR100YAY
       ;;
(PR100YAY)
       m_CondExec 00,EQ,PR100YAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR100YBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PR100YBA
       ;;
(PR100YBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A12} FEXTRAC ${DATA}/PXX0/PR100YAD.BPR100FY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A13} FCUMULS ${DATA}/PXX0/PR100YAJ.BPR100HY
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IPR102 FEDITION
# *** FICHIER D'IMPRESSION DES MGC POUR GEG00X  FILIALES                       
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -g +1 FEG132 ${DATA}/PXX0/F45.BPR100JY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=PR100YBB
       ;;
(PR100YBB)
       m_CondExec 04,GE,PR100YBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FICHIER POUR LE GEG                                                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR100YBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=PR100YBD
       ;;
(PR100YBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PXX0/PR100YAA.BPR100CY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g ${G_A15} SORTOUT ${DATA}/PXX0/PR100YAD.BPR100FY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_75_5 75 CH 5
 /FIELDS FLD_CH_97_4 97 CH 4
 /FIELDS FLD_CH_89_4 89 CH 4
 /FIELDS FLD_CH_85_4 85 CH 4
 /FIELDS FLD_CH_1_27 01 CH 27
 /FIELDS FLD_CH_101_4 101 CH 4
 /FIELDS FLD_CH_93_4 93 CH 4
 /FIELDS FLD_CH_70_5 70 CH 5
 /FIELDS FLD_CH_80_5 80 CH 5
 /KEYS
   FLD_CH_1_27 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_70_5,
    TOTAL FLD_CH_75_5,
    TOTAL FLD_CH_80_5,
    TOTAL FLD_CH_85_4,
    TOTAL FLD_CH_89_4,
    TOTAL FLD_CH_93_4,
    TOTAL FLD_CH_97_4,
    TOTAL FLD_CH_101_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR100YBE
       ;;
(PR100YBE)
       m_CondExec 00,EQ,PR100YBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR100YBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=PR100YBG
       ;;
(PR100YBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A16} FEXTRAC ${DATA}/PXX0/PR100YAD.BPR100FY
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g ${G_A17} FCUMULS ${DATA}/PXX0/PR100YAG.BPR100GY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=PR100YBH
       ;;
(PR100YBH)
       m_CondExec 04,GE,PR100YBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR100YBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=PR100YBJ
       ;;
(PR100YBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PXX0/PR100YAG.BPR100GY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g ${G_A19} SORTOUT ${DATA}/PXX0/PR100YAJ.BPR100HY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR100YBK
       ;;
(PR100YBK)
       m_CondExec 00,EQ,PR100YBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR100YBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=PR100YBM
       ;;
(PR100YBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A20} FEXTRAC ${DATA}/PXX0/PR100YAD.BPR100FY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A21} FCUMULS ${DATA}/PXX0/PR100YAJ.BPR100HY
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IPR103 FEDITION
# *** FICHIER D'IMPRESSION DES MGC POUR GEG00X  FILIALES                       
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -g +1 FEG132 ${DATA}/PXX0/F45.BPR100KY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=PR100YBN
       ;;
(PR100YBN)
       m_CondExec 04,GE,PR100YBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FICHIER POUR LE GEG                                                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR100YBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=PR100YBQ
       ;;
(PR100YBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A22} SORTIN ${DATA}/PXX0/PR100YAA.BPR100DY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g ${G_A23} SORTOUT ${DATA}/PXX0/PR100YAD.BPR100FY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_88_4 88 PD 4
 /FIELDS FLD_PD_96_4 96 PD 4
 /FIELDS FLD_PD_83_5 83 PD 5
 /FIELDS FLD_PD_100_4 100 PD 4
 /FIELDS FLD_PD_92_4 92 PD 4
 /FIELDS FLD_PD_73_5 73 PD 5
 /FIELDS FLD_PD_104_4 104 PD 4
 /FIELDS FLD_PD_78_5 78 PD 5
 /FIELDS FLD_CH_1_27 01 CH 27
 /KEYS
   FLD_CH_1_27 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_73_5,
    TOTAL FLD_PD_78_5,
    TOTAL FLD_PD_83_5,
    TOTAL FLD_PD_88_4,
    TOTAL FLD_PD_92_4,
    TOTAL FLD_PD_96_4,
    TOTAL FLD_PD_100_4,
    TOTAL FLD_PD_104_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR100YBR
       ;;
(PR100YBR)
       m_CondExec 00,EQ,PR100YBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR100YBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=PR100YBT
       ;;
(PR100YBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A24} FEXTRAC ${DATA}/PXX0/PR100YAD.BPR100FY
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g ${G_A25} FCUMULS ${DATA}/PXX0/PR100YAG.BPR100GY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=PR100YBU
       ;;
(PR100YBU)
       m_CondExec 04,GE,PR100YBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR100YBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=PR100YBX
       ;;
(PR100YBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A26} SORTIN ${DATA}/PXX0/PR100YAG.BPR100GY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g ${G_A27} SORTOUT ${DATA}/PXX0/PR100YAJ.BPR100HY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR100YBY
       ;;
(PR100YBY)
       m_CondExec 00,EQ,PR100YBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR100YCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=PR100YCA
       ;;
(PR100YCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A28} FEXTRAC ${DATA}/PXX0/PR100YAD.BPR100FY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A29} FCUMULS ${DATA}/PXX0/PR100YAJ.BPR100HY
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IPR104 FEDITION
# *** FICHIER D'IMPRESSION DES MGC POUR GEG00X  FILIALES                       
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -g +1 FEG132 ${DATA}/PXX0/F45.BPR100LY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=PR100YCB
       ;;
(PR100YCB)
       m_CondExec 04,GE,PR100YCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FICHIER POUR LE GEG                                                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR100YCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=PR100YCD
       ;;
(PR100YCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A30} SORTIN ${DATA}/PXX0/PR100YAA.BPR100EY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g ${G_A31} SORTOUT ${DATA}/PXX0/PR100YAD.BPR100FY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_1_27 01 CH 27
 /FIELDS FLD_PD_80_5 80 PD 5
 /FIELDS FLD_PD_70_5 70 PD 5
 /FIELDS FLD_PD_105_4 105 PD 4
 /FIELDS FLD_PD_75_5 75 PD 5
 /FIELDS FLD_PD_101_4 101 PD 4
 /FIELDS FLD_PD_89_4 89 PD 4
 /FIELDS FLD_PD_93_4 93 PD 4
 /FIELDS FLD_PD_97_4 97 PD 4
 /KEYS
   FLD_CH_1_27 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_70_5,
    TOTAL FLD_PD_75_5,
    TOTAL FLD_PD_80_5,
    TOTAL FLD_PD_85_4,
    TOTAL FLD_PD_89_4,
    TOTAL FLD_PD_93_4,
    TOTAL FLD_PD_97_4,
    TOTAL FLD_PD_101_4,
    TOTAL FLD_PD_105_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR100YCE
       ;;
(PR100YCE)
       m_CondExec 00,EQ,PR100YCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR100YCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=PR100YCG
       ;;
(PR100YCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A32} FEXTRAC ${DATA}/PXX0/PR100YAD.BPR100FY
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g ${G_A33} FCUMULS ${DATA}/PXX0/PR100YAG.BPR100GY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=PR100YCH
       ;;
(PR100YCH)
       m_CondExec 04,GE,PR100YCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR100YCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=PR100YCJ
       ;;
(PR100YCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A34} SORTIN ${DATA}/PXX0/PR100YAG.BPR100GY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g ${G_A35} SORTOUT ${DATA}/PXX0/PR100YAJ.BPR100HY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR100YCK
       ;;
(PR100YCK)
       m_CondExec 00,EQ,PR100YCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR100YCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=PR100YCM
       ;;
(PR100YCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A36} FEXTRAC ${DATA}/PXX0/PR100YAD.BPR100FY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A37} FCUMULS ${DATA}/PXX0/PR100YAJ.BPR100HY
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IPR105 FEDITION
# *** FICHIER D'IMPRESSION DES MGC POUR GEG00X  FILIALES                       
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -g +1 FEG132 ${DATA}/PXX0/F45.BPR100MY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=PR100YCN
       ;;
(PR100YCN)
       m_CondExec 04,GE,PR100YCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ******                                                                       
#  DEPENDANCE POUR PLAN                                                        
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PR100YZA
       ;;
(PR100YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PR100YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
