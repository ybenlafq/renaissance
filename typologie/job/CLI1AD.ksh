#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CLI1AD.ksh                       --- VERSION DU 08/10/2016 12:55
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDCLI1A -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/08/25 AT 14.32.49 BY PREPA2                       
#    STANDARDS: P  JOBSET: CLI1AD                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#    CREATION A VIDE D UNE GENERATION                                          
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=CLI1ADA
       ;;
(CLI1ADA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       LOT=${CZXSRUNB}
# ********************************************************************         
# *    GENERATED ON MONDAY    2008/08/25 AT 14.32.49 BY PREPA2                 
# *    JOBSET INFORMATION:    NAME...: CLI1AD                                  
# *                           FREQ...: 6W                                      
# *                           TITLE..: 'ALT-CLI02D'                            
# *                           APPL...: IMPMARSE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=CLI1ADAA
       ;;
(CLI1ADAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******** FIC ENTREE DUMMY                                                    
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d SHR IN4 /dev/null
       m_FileAssign -d SHR IN5 /dev/null
       m_FileAssign -d SHR IN6 /dev/null
       m_FileAssign -d SHR IN7 /dev/null
       m_FileAssign -d SHR IN8 /dev/null
# ******** RECREATION DU NOUVEAU FIC                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 350 -g +1 OUT1 ${DATA}/PEX0/F91.BBC101AD
       m_FileAssign -d NEW,CATLG,DELETE -r 108 -g +1 OUT2 ${DATA}/PEX0/F91.BBC102AD
       m_FileAssign -d NEW,CATLG,DELETE -r 157 -g +1 OUT3 ${DATA}/PEX0/F91.BBC103AD
       m_FileAssign -d NEW,CATLG,DELETE -r 86 -g +1 OUT4 ${DATA}/PEX0/F91.BBC104AD
       m_FileAssign -d NEW,CATLG,DELETE -r 750 -g +1 OUT5 ${DATA}/PEX0/F91.BBC105AD
       m_FileAssign -d NEW,CATLG,DELETE -r 108 -g +1 OUT6 ${DATA}/PEX0/F91.BBC106AD
       m_FileAssign -d NEW,CATLG,DELETE -r 157 -g +1 OUT7 ${DATA}/PEX0/F91.BBC107AD
       m_FileAssign -d NEW,CATLG,DELETE -r 86 -g +1 OUT8 ${DATA}/PEX0/F91.BBC108AD
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CLI1ADAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=CLI1ADAB
       ;;
(CLI1ADAB)
       m_CondExec 16,NE,CLI1ADAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   MISE TERMIN� DES JOBS SOUS PLAN PAR JOB UTILITAIRE DE PLAN                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CLI1ADAD PGM=CZX2PTRT   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
