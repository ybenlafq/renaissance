#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NV052P.ksh                       --- VERSION DU 17/10/2016 18:34
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPNV052 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 11/07/11 AT 12.25.40 BY BURTEC2                      
#    STANDARDS: P  JOBSET: NV052P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BNV052                                                                
#  TRAITEMENT DES DONN�ES ADDITIONNELLES 1                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=NV052PA
       ;;
(NV052PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'0'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'0'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'0'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'0'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=NV052PAA
       ;;
(NV052PAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#                                                                              
# *****   FICHIERS PARAMETRE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 1255 -g +1 FNV052 ${DATA}/PTEM/NV052PAA.BNV052AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNV052 
       JUMP_LABEL=NV052PAB
       ;;
(NV052PAB)
       m_CondExec 04,GE,NV052PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BNV054                                                                
#  DETERMINATION SITUATION DES ASSOCIATIONS A J+1                              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV052PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=NV052PAD
       ;;
(NV052PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   TABLES EN LECTURE                                                    
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA63   : NAME=RSGA63,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA63 /dev/null
#    RTBS25   : NAME=RSBS25P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBS25 /dev/null
#    RTNV14   : NAME=RSNV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTNV14 /dev/null
#                                                                              
# *****   FICHIERS PARAMETRES                                                  
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FICHIERS D'EXTRACTION                                                
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 267 -g +1 FNV054C ${DATA}/PTEM/NV052PAD.BNV054BP
       m_FileAssign -d NEW,CATLG,DELETE -r 267 -g +1 FNV054S ${DATA}/PTEM/NV052PAD.BNV054AP
       m_FileAssign -d NEW,CATLG,DELETE -r 267 -g +1 FNV054T ${DATA}/PTEM/NV052PAD.BNV054TP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNV054 
       JUMP_LABEL=NV052PAE
       ;;
(NV052PAE)
       m_CondExec 04,GE,NV052PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI  FICHIER  FNV052                                                        
#  POUR CONSTITUTION FICHIER FNV052J                                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV052PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=NV052PAG
       ;;
(NV052PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/NV052PAA.BNV052AP
# ******                                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 1255 -g +1 SORTOUT ${DATA}/PXX0/F07.BNV052BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_255 1 CH 255
 /KEYS
   FLD_CH_1_255 ASCENDING
 /* Record Type = F  Record Length = 1255 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV052PAH
       ;;
(NV052PAH)
       m_CondExec 00,EQ,NV052PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI  FICHIER  FNV054C                                                       
#  POUR CONSTITUTION FICHIER FNV054JC                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV052PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=NV052PAJ
       ;;
(NV052PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/NV052PAD.BNV054BP
# ******                                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 267 -g +1 SORTOUT ${DATA}/PXX0/F07.BNV054DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_12 1 CH 12
 /KEYS
   FLD_CH_1_12 ASCENDING
 /* Record Type = F  Record Length = 267 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV052PAK
       ;;
(NV052PAK)
       m_CondExec 00,EQ,NV052PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI  FICHIER  FNV054S                                                       
#  POUR CONSTITUTION FICHIER FNV054JS                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV052PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=NV052PAM
       ;;
(NV052PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/NV052PAD.BNV054AP
# ******                                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 267 -g +1 SORTOUT ${DATA}/PXX0/F07.BNV054CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_12 1 CH 12
 /KEYS
   FLD_CH_1_12 ASCENDING
 /* Record Type = F  Record Length = 267 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV052PAN
       ;;
(NV052PAN)
       m_CondExec 00,EQ,NV052PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI  FICHIER  FNV054T                                                       
#  POUR CONSTITUTION FICHIER FNV054JT                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV052PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=NV052PAQ
       ;;
(NV052PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/NV052PAD.BNV054TP
# ******                                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 267 -g +1 SORTOUT ${DATA}/PXX0/F07.BNV054UP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_12 1 CH 12
 /KEYS
   FLD_CH_1_12 ASCENDING
 /* Record Type = F  Record Length = 267 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV052PAR
       ;;
(NV052PAR)
       m_CondExec 00,EQ,NV052PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BNV053                                                                
#  COMPARAISON SITUATION J+1 / J POUR LES ASSOCIATIONS                         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV052PAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=NV052PAT
       ;;
(NV052PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RTNV00   : NAME=RSNV00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTNV00 /dev/null
# *****   FICHIERS PARAMETRE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FICHIERS EN ENTR�E DU JOUR                                           
       m_FileAssign -d SHR -g ${G_A5} FNV052J ${DATA}/PXX0/F07.BNV052BP
# *****   FICHIERS EN ENTR�E DE LA VEILLE                                      
       m_FileAssign -d SHR -g ${G_A6} FNV052J1 ${DATA}/PXX0/F07.BNV052BP
#                                                                              
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 1255 -g +1 FNV053 ${DATA}/PXX0/F07.BNV053AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNV053 
       JUMP_LABEL=NV052PAU
       ;;
(NV052PAU)
       m_CondExec 04,GE,NV052PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BNV055                                                                
#  COMPARAISON SITUATION J-1 / J POUR LES ASSOCIATIONS                         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV052PAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=NV052PAX
       ;;
(NV052PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RTNV00   : NAME=RSNV00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTNV00 /dev/null
# *****   FICHIERS PARAMETRE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FICHIERS EN ENTR�E DU JOUR                                           
       m_FileAssign -d SHR -g ${G_A7} FNV054JS ${DATA}/PXX0/F07.BNV054CP
# *****   FICHIERS EN ENTR�E DE LA VEILLE                                      
       m_FileAssign -d SHR -g ${G_A8} FNV054S1 ${DATA}/PXX0/F07.BNV054CP
#                                                                              
# *****   FICHIERS EN ENTR�E DU JOUR                                           
       m_FileAssign -d SHR -g ${G_A9} FNV054JC ${DATA}/PXX0/F07.BNV054DP
# *****   FICHIERS EN ENTR�E DE LA VEILLE                                      
       m_FileAssign -d SHR -g ${G_A10} FNV054C1 ${DATA}/PXX0/F07.BNV054DP
#                                                                              
# *****   FICHIERS EN ENTR�E DU JOUR                                           
       m_FileAssign -d SHR -g ${G_A11} FNV054JT ${DATA}/PXX0/F07.BNV054UP
# *****   FICHIERS EN ENTR�E DE LA VEILLE                                      
       m_FileAssign -d SHR -g ${G_A12} FNV054T1 ${DATA}/PXX0/F07.BNV054UP
#                                                                              
# *****   FICHIERS EN SORTIE VERS NV010P                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 1255 -g +1 FNV055 ${DATA}/PXX0/F07.BNV055AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNV055 
       JUMP_LABEL=NV052PAY
       ;;
(NV052PAY)
       m_CondExec 04,GE,NV052PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=NV052PZA
       ;;
(NV052PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV052PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
