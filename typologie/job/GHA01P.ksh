#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GHA01P.ksh                       --- VERSION DU 09/10/2016 05:43
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGHA01 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/09/29 AT 14.22.40 BY SYSTEM5                      
#    STANDARDS: P  JOBSET: GHA01P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='NCP'                                                               
# ********************************************************************         
#  PGM BCS623 : RECUPERATION DES TABLES DB2 NECESSAIRES A                      
#               L'ENCAISSEMENT EN EURO                                         
# ********************************************************************         
# AAA      STEP  PGM=IKJEFT01                                                  
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=H                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSABOUT REPORT SYSOUT=X                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# *                                                                            
# **** TABLES EN ENTREE                                                        
# RTFM04   FILE  DYNAM=YES,NAME=RSFM04,MODE=I                                  
# RTFM05   FILE  DYNAM=YES,NAME=RSFM05,MODE=I                                  
# *                                                                            
# **** SOUS-TABLES EN MISE A JOUR (TFM04 ET TFM05)                             
# RTGA01   FILE  DYNAM=YES,NAME=RSGA01,MODE=I                                  
# *                                                                            
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDIF                                         
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BCS623) PLAN(BCS623)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  BGA771 : MAJ DE LA DATE DE PREMIERE RECEPTION DES CODICS GROUPES            
#           EN FONCTION DES ELEMENTS DU GROUPE                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GHA01PA
       ;;
(GHA01PA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GHA01PAA
       ;;
(GHA01PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RTGA58   : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA58 /dev/null
#    RTFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL50 /dev/null
#    RTFL50   : NAME=RSFL50,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTFL50 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGA771 
       JUMP_LABEL=GHA01PAB
       ;;
(GHA01PAB)
       m_CondExec 04,GE,GHA01PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BLM050: CREE SOUS TABLE (LISTE DES MAGS DU GROUPE) POUR LISTE MARIA         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GHA01PAD
       ;;
(GHA01PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLM050 
       JUMP_LABEL=GHA01PAE
       ;;
(GHA01PAE)
       m_CondExec 04,GE,GHA01PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHA001 : EXTRACTION DES SS TABLES NON MODIFIABLES POUR LES FILIALES         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GHA01PAG
       ;;
(GHA01PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA01 /dev/null
#                                                                              
# ******* FIC D'EXTRAC EN VUE DU LOAD DE P994.RTGA01                           
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FHA001 ${DATA}/PXX0/F07.BHA001AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA001 
       JUMP_LABEL=GHA01PAH
       ;;
(GHA01PAH)
       m_CondExec 04,GE,GHA01PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHA003 : EXTRACTION DES MODIFICATIONS DE LA JOURNEE SUR RTGA73/74           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GHA01PAJ
       ;;
(GHA01PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ASSO ARTICLE/PROFIL                                            
#    RSGA73   : NAME=RSGA73,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA73 /dev/null
# ******* TABLE TEXT ETIQUETTE INFO                                            
#    RSGA74   : NAME=RSGA74,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA74 /dev/null
#                                                                              
# ******* FIC D'EXTRAC EN VUE DU LOAD DE RTGA73 FILIALES                       
       m_FileAssign -d NEW,CATLG,DELETE -r 25 -t LSEQ -g +1 FHA73 ${DATA}/PXX0/F07.BHA003AP
# ******* FIC D'EXTRAC EN VUE DU LOAD DE RTGA74 FILIALES                       
       m_FileAssign -d NEW,CATLG,DELETE -r 72 -t LSEQ -g +1 FHA74 ${DATA}/PXX0/F07.BHA003BP
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA003 
       JUMP_LABEL=GHA01PAK
       ;;
(GHA01PAK)
       m_CondExec 04,GE,GHA01PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHA005 : EXTRACTION DES NOUVELLES ENTITES DE COMMANDES SUR NCP              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GHA01PAM
       ;;
(GHA01PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ENTITE DE COMMANDE                                             
#    RSGA06   : NAME=RSGA06,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA06 /dev/null
#                                                                              
# ******* FIC DE LOAD POUR RTGA06 (FILIALES)                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 39 -t LSEQ -g +1 FHA005 ${DATA}/PXX0/F07.BHA005AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA005 
       JUMP_LABEL=GHA01PAN
       ;;
(GHA01PAN)
       m_CondExec 04,GE,GHA01PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHA030 : EXTRACTION DES MODIFICATIONS ARTICLES AYANT CHANGES DE             
#                          STATUS D'APPROVISIONNEMENT                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GHA01PAQ
       ;;
(GHA01PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA00   : NAME=RSGA00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE DES FAMILLES                                                   
#    RSGA14   : NAME=RSGA14,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA14 /dev/null
# ******* TABLE ARTICLE/ZONES DE PRIX                                          
#    RSGA59   : NAME=RSGA59,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA59 /dev/null
# ******* TABLE HISTO DES STATUS D'ARTICLES                                    
#    RSGA65   : NAME=RSGA65,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA65 /dev/null
#                                                                              
# ******* FIC D'EXTRACTION POUR FILIALES                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FBHA30 ${DATA}/PTEM/GHA01PAQ.BHA030AP
# ******* FIC DE LOAD POUR RTHA30 (FILIALES)                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 73 -t LSEQ -g +1 RTHA30 ${DATA}/PXX0/F07.BHA030CP
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  PARAMETRE SOCIETE : 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA030 
       JUMP_LABEL=GHA01PAR
       ;;
(GHA01PAR)
       m_CondExec 04,GE,GHA01PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER  DES MODIFICATIONS ARTICLES AYANT CHANGES DE STATUS          
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GHA01PAT
       ;;
(GHA01PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GHA01PAQ.BHA030AP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GHA01PAT.BHA030BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_6_3 6 CH 3
 /FIELDS FLD_CH_73_5 73 CH 5
 /FIELDS FLD_CH_36_5 36 CH 5
 /FIELDS FLD_CH_49_1 49 CH 1
 /FIELDS FLD_CH_9_7 9 CH 7
 /KEYS
   FLD_CH_49_1 ASCENDING,
   FLD_CH_6_3 ASCENDING,
   FLD_CH_36_5 ASCENDING,
   FLD_CH_73_5 ASCENDING,
   FLD_CH_9_7 ASCENDING
 /* Record Type = F  Record Length = 147 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GHA01PAU
       ;;
(GHA01PAU)
       m_CondExec 00,EQ,GHA01PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHA031 : EDITION DES MODIFICATIONS DE STATUS D'APPROVISIONNEMENT            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01PAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GHA01PAX
       ;;
(GHA01PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE HISTO DES STATUS D'ARTICLES                                    
#    RSGA65   : NAME=RSGA65,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA65 /dev/null
# ******* FICHIER EXTRAC TRIE                                                  
       m_FileAssign -d SHR -g ${G_A2} FBHA31 ${DATA}/PTEM/GHA01PAT.BHA030BP
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* EDITION ARTICLES MODIF FILIALE                                       
       m_OutputAssign -c 9 -w IHA31 IHA31
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA031 
       JUMP_LABEL=GHA01PAY
       ;;
(GHA01PAY)
       m_CondExec 04,GE,GHA01PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  MODIF PCM                                                                   
# ********************************************************************         
#  BHA053 : MAJ STATUT ET EXTRACTION PSE                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GHA01PBA
       ;;
(GHA01PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES EN MAJ                                                
#    RSGA00   : NAME=RSGA00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSGA40   : NAME=RSGA40,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA40 /dev/null
#    RSGA41   : NAME=RSGA41,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA41 /dev/null
#    RSGA42   : NAME=RSGA42,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA42 /dev/null
#    RSGA65   : NAME=RSGA65,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA65 /dev/null
#                                                                              
# ******* FIC EN SORTIE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 31 -t LSEQ -g +1 FHA40 ${DATA}/PXX0/F07.BHA040AP
# ******* FIC DE LOAD POUR RTGA00 (FILIALES)                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 FHA41 ${DATA}/PXX0/F07.BHA041AP
# ******* FIC DE LOAD POUR RTGA00 (FILIALES)                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 17 -t LSEQ -g +1 FHA42 ${DATA}/PXX0/F07.BHA042AP
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA053 
       JUMP_LABEL=GHA01PBB
       ;;
(GHA01PBB)
       m_CondExec 04,GE,GHA01PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHA050 : EXTRACTION DES MODIFICATIONS SUR LES DONNEES "GROUPE"              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01PBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GHA01PBD
       ;;
(GHA01PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA00   : NAME=RSGA00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* TABLE ARTICLES                                                       
#    RSGA03   : NAME=RSGA03,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA03 /dev/null
# ******* TABLE ARTICLES                                                       
#    RSGA31   : NAME=RSGA31,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA31 /dev/null
# ******* TABLE ARTICLES                                                       
#    RSGA33   : NAME=RSGA33,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA33 /dev/null
# ******* TABLE RELAT� ARTICLE/DECLARATION                                     
#    RSGA51   : NAME=RSGA51,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA51 /dev/null
# ******* TABLE ARTICLE/GARANTIE COMPLEMENTAIRE                                
#    RSGA52   : NAME=RSGA52,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA52 /dev/null
# ******* TABLE ARTICLE/CODE DESCRIPTIF                                        
#    RSGA53   : NAME=RSGA53,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA53 /dev/null
# ******* TABLE ARTICLE/ENTITE DE COMMANDE                                     
#    RSGA55   : NAME=RSGA55,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA55 /dev/null
# ******* TABLE RELAT� ARTICLE/ARTICLE                                         
#    RSGA58   : NAME=RSGA58,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA58 /dev/null
# ******* TABLE RELAT� ARTICLE/PRIX                                            
#    RSGA59   : NAME=RSGA59,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA59 /dev/null
# ******* TABLE RELAT� ARTICLE/                                                
#    RSGA63   : NAME=RSGA63,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA63 /dev/null
#                                                                              
# ******* FIC DE LOAD POUR RTGA00 (FILIALES)                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 493 -t LSEQ -g +1 FHA00 ${DATA}/PXX0/F07.BHA050AP
# ******* FIC DE LOAD POUR RTGA00 (FILIALES)                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 17 -t LSEQ -g +1 FHA03 ${DATA}/PXX0/F07.BHA050IP
# ******* FIC DE LOAD POUR RTGA00 (FILIALES)                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 62 -t LSEQ -g +1 FHA31 ${DATA}/PXX0/F07.BHA050JP
# ******* FIC DE LOAD POUR RTGA00 (FILIALES)                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 12 -t LSEQ -g +1 FHA33 ${DATA}/PXX0/F07.BHA050KP
# ******* FIC DE LOAD POUR RTGA51 (FILIALES)                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 12 -t LSEQ -g +1 FHA51 ${DATA}/PXX0/F07.BHA050BP
# ******* FIC DE LOAD POUR RTGA52 (FILIALES)                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 15 -t LSEQ -g +1 FHA52 ${DATA}/PXX0/F07.BHA050CP
# ******* FIC DE LOAD POUR RTGA53 (FILIALES)                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 17 -t LSEQ -g +1 FHA53 ${DATA}/PXX0/F07.BHA050DP
# ******* FIC DE LOAD POUR RTGA55 (FILIALES)                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 16 -t LSEQ -g +1 FHA55 ${DATA}/PXX0/F07.BHA050EP
# ******* FIC DE LOAD POUR RTGA58 (FILIALES)                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -t LSEQ -g +1 FHA58 ${DATA}/PXX0/F07.BHA050FP
# ******* FIC DE LOAD POUR RTGA59 (FILIALES)                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 35 -t LSEQ -g +1 FHA59 ${DATA}/PXX0/F07.BHA050GP
# ******* FIC DE LOAD POUR RTGA63 (FILIALES)                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 12 -t LSEQ -g +1 FHA63 ${DATA}/PXX0/F07.BHA050HP
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA050 
       JUMP_LABEL=GHA01PBE
       ;;
(GHA01PBE)
       m_CondExec 04,GE,GHA01PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHA052 :                                                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01PBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GHA01PBG
       ;;
(GHA01PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA13   : NAME=RSGA13,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA13 /dev/null
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA19   : NAME=RSGA19,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA19 /dev/null
#    RTGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA30 /dev/null
#                                                                              
#    RTGQ05   : NAME=RSGQ05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGQ05 /dev/null
#                                                                              
#    RTGA54   : NAME=RSGA54,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA54 /dev/null
#                                                                              
#    RTFL05   : NAME=RSFL05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL05 /dev/null
#    RTFL06   : NAME=RSFL06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL06 /dev/null
#    RTFL14   : NAME=RSFL14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL14 /dev/null
#                                                                              
#    RTFL40   : NAME=RSFL40,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL40 /dev/null
#    RTFL50   : NAME=RSFL50,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTFL50 /dev/null
#    RTFL64   : NAME=RSFL64,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL64 /dev/null
#    RTFL65   : NAME=RSFL65,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL65 /dev/null
#                                                                              
       m_FileAssign -d SHR -g ${G_A3} FHA00 ${DATA}/PXX0/F07.BHA050AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA052 
       JUMP_LABEL=GHA01PBH
       ;;
(GHA01PBH)
       m_CondExec 04,GE,GHA01PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHA007 : EXTRACTION DES DONNEES RTGA30 GROUPE                               
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01PBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GHA01PBJ
       ;;
(GHA01PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE FAMILLE                                                        
#    RSGA30   : NAME=RSGA30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA30 /dev/null
# ******* TABLE GENERALISEE COPAR                                              
#    RSGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* FIC D'EXTRACTION POUR LES FILIALES                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 FGA30 ${DATA}/PXX0/F99.FGA30AF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA007 
       JUMP_LABEL=GHA01PBK
       ;;
(GHA01PBK)
       m_CondExec 04,GE,GHA01PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFL001 :                                                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01PBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GHA01PBM
       ;;
(GHA01PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA19   : NAME=RSGA19,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA19 /dev/null
#    RTFL05   : NAME=RSFL05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL05 /dev/null
#    RTFL06   : NAME=RSFL06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL06 /dev/null
#    RTFL40   : NAME=RSFL40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL40 /dev/null
#    RTFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL50 /dev/null
#    RTGQ05   : NAME=RSGQ05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGQ05 /dev/null
# ------  TABLES EN MAJ                                                        
#    RTGA00   : NAME=RSGA00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA65   : NAME=RSGA65,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA65 /dev/null
#    RTGA66   : NAME=RSGA66,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA66 /dev/null
# ------  PARAMETRE                                                            
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# -----   FICHIER REPRIS DANS LA CHA�NE FL003P                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 60 -t LSEQ -g +1 FL001 ${DATA}/PXX0/F07.BFL001AP
# -----   FICHIER POUR EVITER DEPASSEMENT TABLEAU INTERNE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 159 -t LSEQ -g +1 FGA00 ${DATA}/PXX0/F07.BFL001CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFL001 
       JUMP_LABEL=GHA01PBN
       ;;
(GHA01PBN)
       m_CondExec 04,GE,GHA01PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFL004 :                                                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01PBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GHA01PBQ
       ;;
(GHA01PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTFL14   : NAME=RSFL14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL14 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTFL05   : NAME=RSFL05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL05 /dev/null
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGQ05   : NAME=RSGQ05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGQ05 /dev/null
# ------  TABLES EN MAJ                                                        
#    RTFL50   : NAME=RSFL50,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL50 /dev/null
#    RTFL65   : NAME=RSFL65,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL65 /dev/null
# ------  PARAMETRE                                                            
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFL004 
       JUMP_LABEL=GHA01PBR
       ;;
(GHA01PBR)
       m_CondExec 04,GE,GHA01PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTSP00                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01PBT PGM=PTLDRIVM   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GHA01PBT
       ;;
(GHA01PBT)
       m_CondExec ${EXACS},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 39 -t LSEQ -g +1 SYSREC01 ${DATA}/PNCGP/F07.UNLOAD.RSSP00
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01PBT.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD DE LA TABLE RTSP05                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01PBX PGM=PTLDRIVM   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GHA01PBX
       ;;
(GHA01PBX)
       m_CondExec ${EXACX},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 36 -t LSEQ -g +1 SYSREC01 ${DATA}/PNCGP/F07.UNLOAD.RSSP05
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01PBX.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD DE LA TABLE RTSP15                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01PCA PGM=PTLDRIVM   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GHA01PCA
       ;;
(GHA01PCA)
       m_CondExec ${EXADC},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 38 -t LSEQ -g +1 SYSREC01 ${DATA}/PNCGP/F07.UNLOAD.RSSP15
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01PCA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD DE LA TABLE RTGA10                                                   
#   POUR CHARGEMENT DE RTGA10 DE DARTY OUEST                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01PCD PGM=PTLDRIVM   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GHA01PCD
       ;;
(GHA01PCD)
       m_CondExec ${EXADH},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 322 -t LSEQ -g +1 SYSREC01 ${DATA}/PNCGP/F07.UNLOAD.RSGA10
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01PCD.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GHA01PZA
       ;;
(GHA01PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
