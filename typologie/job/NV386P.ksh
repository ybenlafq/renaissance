#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NV386P.ksh                       --- VERSION DU 08/10/2016 13:22
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPNV386 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/09/17 AT 14.31.11 BY BURTECA                      
#    STANDARDS: P  JOBSET: NV386P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BNV386 :DETERMINATION SITUATION DE L'ECO-TAXE A J +1                        
#  REPRISE OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=NV386PA
       ;;
(NV386PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=NV386PAA
       ;;
(NV386PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
#                                                                              
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#   TABLES EN LECTURE                                                          
#                                                                              
#    RSGA38   : NAME=RSGA38,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA38 /dev/null
#                                                                              
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#                                                                              
# ****** CARTE DATE.                                                           
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** FICHIER EN SORTIE LRECL 18                                            
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 FNV386 ${DATA}/PTEM/NV386PAA.NV386AAP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNV386 
       JUMP_LABEL=NV386PAB
       ;;
(NV386PAB)
       m_CondExec 04,GE,NV386PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FICHIER ISSU DU PGM BNV386 (PRECEDENT)                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV386PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=NV386PAD
       ;;
(NV386PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/NV386PAA.NV386AAP
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NV386PAD.NV386BBP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_15 01 CH 15
 /KEYS
   FLD_CH_1_15 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV386PAE
       ;;
(NV386PAE)
       m_CondExec 00,EQ,NV386PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DES DONNEES WCS                                                      
#  REPRISE .OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV386PAG PGM=PTLDRIVM   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=NV386PAG
       ;;
(NV386PAG)
       m_CondExec ${EXAAK},NE,YES 
#                                                                              
#    RSNV18   : NAME=RSNV18,MODE=I - DYNAM=YES                                 
#                                                                              
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/NV386PAG.NV386CCP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV386PAG.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  BNV387 :COMPARAISON SITUATION NCG J+1 / SITUATION WCS A J                   
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV386PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=NV386PAJ
       ;;
(NV386PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#   TABLES EN LECTURE                                                          
#                                                                              
#    RSNV18   : NAME=RSNV18,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV18 /dev/null
#                                                                              
#    RSNV00   : NAME=RSNV00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV00 /dev/null
#                                                                              
# ****** CARTE DATE.                                                           
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** FICHIER EN ENTREE                                                     
#                                                                              
       m_FileAssign -d SHR -g ${G_A2} FNV387N ${DATA}/PTEM/NV386PAD.NV386BBP
#                                                                              
       m_FileAssign -d SHR -g ${G_A3} FNV387W ${DATA}/PTEM/NV386PAG.NV386CCP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNV387 
       JUMP_LABEL=NV386PAK
       ;;
(NV386PAK)
       m_CondExec 04,GE,NV386PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=NV386PZA
       ;;
(NV386PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV386PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
