#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GM070D.ksh                       --- VERSION DU 17/10/2016 18:07
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGM070 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/04/12 AT 09.46.42 BY BURTECA                      
#    STANDARDS: P  JOBSET: GM070D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA59 : PRIX                              
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA75 : PRIME                             
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GM070DA
       ;;
(GM070DA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXAHS=${EXAHS:-0}
       EXA98=${EXA98:-0}
       FIRSTME=${FIRSTME}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A45=${G_A45:-'+1'}
       G_A46=${G_A46:-'+1'}
       G_A47=${G_A47:-'+1'}
       G_A48=${G_A48:-'+1'}
       G_A49=${G_A49:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A50=${G_A50:-'+1'}
       G_A51=${G_A51:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       JEUDI=${JEUDI}
       LUNDI=${LUNDI}
       RUN=${RUN}
       JUMP_LABEL=GM070DAA
       ;;
(GM070DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ----------------------              *                                       
# **************************************                                       
#    RSGS30   : NAME=RSGS30D,MODE=I - DYNAM=YES                                
#    RSGA59   : NAME=RSGA59D,MODE=I - DYNAM=YES                                
#    RSGA75   : NAME=RSGA75D,MODE=I - DYNAM=YES                                
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/GM070DAA.BGA59AD
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC02 ${DATA}/PTEM/GM070DAA.BGA75AD
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC03 ${DATA}/PTEM/GM070DAA.BGS30UD
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070DAA.sysin
       
       m_DBHpuUnload -f SYSIN
# ********************************************************************         
#  SORT DES FICHIERS D'UNLOAD DE LA GA59                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GM070DAD
       ;;
(GM070DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GM070DAA.BGA59AD
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070DAD.BGA59ZD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_8_2 8 CH 2
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_15_8 15 CH 8
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_8_2 ASCENDING,
   FLD_CH_15_8 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070DAE
       ;;
(GM070DAE)
       m_CondExec 00,EQ,GM070DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DES FICHIERS D'UNLOAD DE LA GA75                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GM070DAG
       ;;
(GM070DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GM070DAA.BGA75AD
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070DAG.BGA75ZD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_10_8 10 CH 8
 /FIELDS FLD_CH_8_2 8 CH 2
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_8_2 ASCENDING,
   FLD_CH_10_8 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070DAH
       ;;
(GM070DAH)
       m_CondExec 00,EQ,GM070DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FIC RTGS30                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GM070DAJ
       ;;
(GM070DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GM070DAA.BGS30UD
       m_FileAssign -d NEW,CATLG,DELETE -r 16 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/F07.BGS30FD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_18_3 18 CH 3
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_21_3 21 CH 3
 /FIELDS FLD_CH_4_3 04 CH 3
 /KEYS
   FLD_CH_7_7 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_7_7,FLD_CH_4_3,FLD_CH_18_3,FLD_CH_21_3
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GM070DAK
       ;;
(GM070DAK)
       m_CondExec 00,EQ,GM070DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM068                                                                      
#  EXTRACT DES VENTES SUR 28 JOURS GLISSANTS                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GM070DAM
       ;;
(GM070DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** TABLE                                                                 
#    RSHV02   : NAME=RSHV02D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV02 /dev/null
# ****** TABLE                                                                 
#    RSHV12   : NAME=RSHV12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV12 /dev/null
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ****** FICHIER DES VENTES SUR 28 JOURS                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 11 -t LSEQ -g +1 FGM068 ${DATA}/PTEM/GM070DAM.BGM068AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM068 
       JUMP_LABEL=GM070DAN
       ;;
(GM070DAN)
       m_CondExec 04,GE,GM070DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CONSTITUTION DU FIC DES VENTES SOCIETES                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GM070DAQ
       ;;
(GM070DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GM070DAM.BGM068AD
       m_FileAssign -d NEW,CATLG,DELETE -r 11 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070DAQ.BGM068BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_8_4 08 PD 4
 /FIELDS FLD_CH_1_7 01 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_8_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070DAR
       ;;
(GM070DAR)
       m_CondExec 00,EQ,GM070DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM069                                                                      
#  EXTRACT DES CODES DESCRIPTIFS , CODE MARKETING                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GM070DAT
       ;;
(GM070DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** TABLE                                                                 
#    RSGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA14   : NAME=RSGA14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA58   : NAME=RSGA58D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58 /dev/null
#    RSGA12   : NAME=RSGA12D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12 /dev/null
#  FICHIER EN ENTRE ISSU DU GM010P                                             
       m_FileAssign -d SHR -g +0 FGA053 ${DATA}/PXX0/F07.BGA053BP
#  MODIF LE FGM069 PASSE DE 273 A 436                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 436 -t LSEQ -g +1 FGM069 ${DATA}/PTEM/GM070DAT.BGM069AD
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 FGM020 ${DATA}/PTEM/GM070DAT.BGM069BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM069 
       JUMP_LABEL=GM070DAU
       ;;
(GM070DAU)
       m_CondExec 04,GE,GM070DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GM070DAX
       ;;
(GM070DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GM070DAT.BGM069AD
#  CE FICHIER PASSE DE 273 A 436                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 436 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070DAX.BGM069CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_145_5 145 CH 5
 /FIELDS FLD_CH_1_7 01 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_145_5 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070DAY
       ;;
(GM070DAY)
       m_CondExec 00,EQ,GM070DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FGM065                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GM070DBA
       ;;
(GM070DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PEX0/F07.BGM065BP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PEX0/F91.BGM065MD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "991"
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_1_7 1 CH 7
 /CONDITION CND_1 FLD_CH_8_3 EQ CST_1_5 
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_11_3 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070DBB
       ;;
(GM070DBB)
       m_CondExec 00,EQ,GM070DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FGM065Z                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GM070DBD
       ;;
(GM070DBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PEX0/F07.BGM065CP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PEX0/F91.BGM065ZD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "991"
 /FIELDS FLD_CH_8_3 8 CH 3
 /CONDITION CND_1 FLD_CH_8_3 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070DBE
       ;;
(GM070DBE)
       m_CondExec 00,EQ,GM070DBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM070                                                                      
#  ALIMENTATION DU CAHIER BLEU                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GM070DBG
       ;;
(GM070DBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** TABLE                                                                 
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA14   : NAME=RSGA14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA58   : NAME=RSGA58D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58 /dev/null
#    RSGA02   : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02 /dev/null
#    RSGA30   : NAME=RSGA30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSGA19   : NAME=RSGA19,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA19 /dev/null
#    RSGA66   : NAME=RSGA66D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA66 /dev/null
#    RSGG20   : NAME=RSGG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG20 /dev/null
#    RSGG40   : NAME=RSGG40D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG40 /dev/null
#    RSGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA67 /dev/null
#    RSGF55   : NAME=RSGF55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF55 /dev/null
#    RSMU06   : NAME=RSMU06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSMU06 /dev/null
#    RSSP15   : NAME=RSSP15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSSP15 /dev/null
#    RSGA68   : NAME=RSGA68D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA68 /dev/null
#    RSRX00   : NAME=RSRX00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX00 /dev/null
#    RSRX55   : NAME=RSRX55D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX55 /dev/null
#                                                                              
#    RSFL05   : NAME=RSFL05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL05 /dev/null
#    RSFL06   : NAME=RSFL06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL06 /dev/null
#    RSGQ05   : NAME=RSGQ05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGQ05 /dev/null
# **** DATE                                                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ****** FICHIER LUS                                                           
       m_FileAssign -d SHR -g ${G_A6} FGM068 ${DATA}/PTEM/GM070DAQ.BGM068BD
       m_FileAssign -d SHR -g ${G_A7} FGM069 ${DATA}/PTEM/GM070DAX.BGM069CD
       m_FileAssign -d SHR -g ${G_A8} FGM020 ${DATA}/PTEM/GM070DAT.BGM069BD
       m_FileAssign -d SHR -g +0 RTGN59 ${DATA}/PXX0/F07.BGN59FF
       m_FileAssign -d SHR -g ${G_A9} RTGA59Z ${DATA}/PTEM/GM070DAD.BGA59ZD
       m_FileAssign -d SHR -g +0 RTGN75F ${DATA}/PXX0/F07.BGN75FF
       m_FileAssign -d SHR -g ${G_A10} RTGA75Z ${DATA}/PTEM/GM070DAG.BGA75ZD
       m_FileAssign -d SHR -g ${G_A11} FGM065M ${DATA}/PEX0/F91.BGM065MD
       m_FileAssign -d SHR -g ${G_A12} FGM065Z ${DATA}/PEX0/F91.BGM065ZD
       m_FileAssign -d SHR -g ${G_A13} RTGS30 ${DATA}/PTEM/F07.BGS30FD
       m_FileAssign -d SHR -g +0 RTGS10 ${DATA}/PXX0/F07.BGS10FF
# ****** FICHIER CREES                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -t LSEQ -g +1 RTGM01 ${DATA}/PTEM/GM070DBG.BGM070AD
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -t LSEQ -g +1 RTGM06 ${DATA}/PTEM/GM070DBG.BGM070BD
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FGM075 ${DATA}/PTEM/GM070DBG.BGM070CD
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FGM70 ${DATA}/PTEM/GM070DBG.BGM070DD
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -t LSEQ -g +1 FGM01 ${DATA}/PTEM/GM070DBG.BGM070ED
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -t LSEQ -g +1 FGM06 ${DATA}/PTEM/GM070DBG.BGM070FD
#  CREATION DE 2 NOUVEAUX FICHIERS LRECL 227                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -t LSEQ -g +1 FSM025 ${DATA}/PTEM/GM070DBG.BGM070MD
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -t LSEQ -g +1 FSM026 ${DATA}/PTEM/GM070DBG.BGM070ND
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM070 
       JUMP_LABEL=GM070DBH
       ;;
(GM070DBH)
       m_CondExec 04,GE,GM070DBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GM070DBJ
       ;;
(GM070DBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/GM070DBG.BGM070CD
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070DBJ.BGM070GD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_87_7 87 CH 7
 /FIELDS FLD_CH_10_2 10 CH 2
 /FIELDS FLD_CH_268_1 268 CH 1
 /KEYS
   FLD_CH_10_2 ASCENDING,
   FLD_CH_87_7 ASCENDING,
   FLD_CH_268_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070DBK
       ;;
(GM070DBK)
       m_CondExec 00,EQ,GM070DBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GM070DBM
       ;;
(GM070DBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/GM070DBG.BGM070DD
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070DBM.BGM070HD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_268_1 268 CH 1
 /FIELDS FLD_CH_87_7 87 CH 7
 /FIELDS FLD_CH_10_2 10 CH 2
 /KEYS
   FLD_CH_10_2 ASCENDING,
   FLD_CH_87_7 ASCENDING,
   FLD_CH_268_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070DBN
       ;;
(GM070DBN)
       m_CondExec 00,EQ,GM070DBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GM070DBQ
       ;;
(GM070DBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PTEM/GM070DBG.BGM070AD
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070DBQ.BGM070ID
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_24_1 24 CH 1
 /FIELDS FLD_CH_32_7 32 CH 7
 /FIELDS FLD_CH_229_2 229 CH 2
 /FIELDS FLD_CH_25_7 25 CH 7
 /KEYS
   FLD_CH_229_2 ASCENDING,
   FLD_CH_32_7 ASCENDING,
   FLD_CH_24_1 DESCENDING,
   FLD_CH_25_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070DBR
       ;;
(GM070DBR)
       m_CondExec 00,EQ,GM070DBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GM070DBT
       ;;
(GM070DBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PTEM/GM070DBG.BGM070ED
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070DBT.BGM070JD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_24_1 24 CH 1
 /FIELDS FLD_CH_229_2 229 CH 2
 /FIELDS FLD_CH_25_7 25 CH 7
 /KEYS
   FLD_CH_229_2 ASCENDING,
   FLD_CH_25_7 ASCENDING,
   FLD_CH_24_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070DBU
       ;;
(GM070DBU)
       m_CondExec 00,EQ,GM070DBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GM070DBX
       ;;
(GM070DBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PTEM/GM070DBG.BGM070BD
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070DBX.BGM070KD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_22_1 22 CH 1
 /FIELDS FLD_CH_15_2 15 CH 2
 /FIELDS FLD_CH_8_7 8 CH 7
 /KEYS
   FLD_CH_15_2 ASCENDING,
   FLD_CH_8_7 ASCENDING,
   FLD_CH_22_1 DESCENDING,
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070DBY
       ;;
(GM070DBY)
       m_CondExec 00,EQ,GM070DBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GM070DCA
       ;;
(GM070DCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A19} SORTIN ${DATA}/PTEM/GM070DBG.BGM070FD
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070DCA.BGM070LD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 01 CH 7
 /FIELDS FLD_CH_15_2 15 CH 2
 /FIELDS FLD_CH_22_1 22 CH 1
 /KEYS
   FLD_CH_15_2 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_22_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070DCB
       ;;
(GM070DCB)
       m_CondExec 00,EQ,GM070DCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GM070DCD
       ;;
(GM070DCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A20} SORTIN ${DATA}/PTEM/GM070DBG.BGM070MD
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070DCD.BGM070OD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_1_16 1 CH 16
 /FIELDS FLD_CH_111_1 111 CH 1
 /KEYS
   FLD_CH_1_16 ASCENDING,
   FLD_CH_104_7 ASCENDING,
   FLD_CH_111_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070DCE
       ;;
(GM070DCE)
       m_CondExec 00,EQ,GM070DCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GM070DCG
       ;;
(GM070DCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A21} SORTIN ${DATA}/PTEM/GM070DBG.BGM070ND
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070DCG.BGM070PD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_111_1 111 CH 1
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_1_16 1 CH 16
 /KEYS
   FLD_CH_1_16 ASCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_111_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070DCH
       ;;
(GM070DCH)
       m_CondExec 00,EQ,GM070DCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM075                                                                      
#  ALIMENTATION DU CAHIER BLEU                                                 
#  ACCES A LA SOUS TABLE GMPRI                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GM070DCJ
       ;;
(GM070DCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i FDATE
$FDATE
_end
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#  2 NOUVEAUX FICHIERS RELU EN 227                                             
       m_FileAssign -d SHR -g ${G_A22} FSM025 ${DATA}/PTEM/GM070DCD.BGM070OD
       m_FileAssign -d SHR -g ${G_A23} FSM026 ${DATA}/PTEM/GM070DCG.BGM070PD
       m_FileAssign -d SHR -g ${G_A24} FGM70 ${DATA}/PTEM/GM070DBM.BGM070HD
       m_FileAssign -d SHR -g ${G_A25} FGM075 ${DATA}/PTEM/GM070DBJ.BGM070GD
       m_FileAssign -d SHR -g ${G_A26} RTGM01 ${DATA}/PTEM/GM070DBQ.BGM070ID
       m_FileAssign -d SHR -g ${G_A27} RTGM06 ${DATA}/PTEM/GM070DBX.BGM070KD
       m_FileAssign -d SHR -g ${G_A28} FGM01 ${DATA}/PTEM/GM070DBT.BGM070JD
       m_FileAssign -d SHR -g ${G_A29} FGM06 ${DATA}/PTEM/GM070DCA.BGM070LD
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 IGM075 ${DATA}/PTEM/GM070DCJ.BGM075AD
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -t LSEQ -g +1 RTGM01S ${DATA}/PTEM/GM070DCJ.BGM075BD
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -t LSEQ -g +1 RTGM06S ${DATA}/PTEM/GM070DCJ.BGM075CD
#  2 NOVEAUX FICHIERS DE SORTIE                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -t LSEQ -g +1 FSM025S ${DATA}/PXX0/F91.BGM075DD
       m_FileAssign -d NEW,CATLG,DELETE -r 14 -t LSEQ -g +1 FGM080 ${DATA}/PTEM/GM070DCJ.BGM075ED
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM075 
       JUMP_LABEL=GM070DCK
       ;;
(GM070DCK)
       m_CondExec 04,GE,GM070DCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR LOAD RTGM01                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GM070DCM
       ;;
(GM070DCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A30} SORTIN ${DATA}/PTEM/GM070DCJ.BGM075BD
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -t LSEQ -g +1 SORTOUT ${DATA}/PGG991/F91.RELOAD.GM01RD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_25_14 25 CH 14
 /KEYS
   FLD_CH_25_14 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070DCN
       ;;
(GM070DCN)
       m_CondExec 00,EQ,GM070DCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR LOAD RTGM06                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DCQ PGM=SORT       ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GM070DCQ
       ;;
(GM070DCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A31} SORTIN ${DATA}/PTEM/GM070DCJ.BGM075CD
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -t LSEQ -g +1 SORTOUT ${DATA}/PGG991/F91.RELOAD.GM06RD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_16 01 CH 16
 /KEYS
   FLD_CH_1_16 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070DCR
       ;;
(GM070DCR)
       m_CondExec 00,EQ,GM070DCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR LOAD RTGM70                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GM070DCT
       ;;
(GM070DCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A32} SORTIN ${DATA}/PTEM/GM070DCJ.BGM075AD
       m_FileAssign -d NEW,CATLG,DELETE -r 402 -t LSEQ -g +1 SORTOUT ${DATA}/PEX0/F91.RELOAD.GM70RD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_401 07 CH 401
 /FIELDS FLD_CH_20_74 20 CH 74
 /FIELDS FLD_CH_7_10 07 CH 10
 /FIELDS FLD_BI_94_2 94 CH 2
 /FIELDS FLD_PD_17_3 17 PD 3
 /KEYS
   FLD_CH_7_10 ASCENDING,
   FLD_PD_17_3 ASCENDING,
   FLD_CH_20_74 ASCENDING,
   FLD_BI_94_2 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_7_401
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GM070DCU
       ;;
(GM070DCU)
       m_CondExec 00,EQ,GM070DCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGM01                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DCX PGM=DSNUTILB   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=GM070DCX
       ;;
(GM070DCX)
       m_CondExec ${EXAEL},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ****** FIC DE LOAD                                                           
       m_FileAssign -d SHR -g ${G_A33} SYSREC ${DATA}/PGG991/F91.RELOAD.GM01RD
#    RSGM01   : NAME=RSGM01D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGM01 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070DCX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GM070D_GM070DCX_RTGM01.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GM070DCY
       ;;
(GM070DCY)
       m_CondExec 04,GE,GM070DCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGM06                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DDA PGM=DSNUTILB   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=GM070DDA
       ;;
(GM070DDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ****** FIC DE LOAD                                                           
       m_FileAssign -d SHR -g ${G_A34} SYSREC ${DATA}/PGG991/F91.RELOAD.GM06RD
#    RSGM06   : NAME=RSGM06D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGM06 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070DDA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GM070D_GM070DDA_RTGM06.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GM070DDB
       ;;
(GM070DDB)
       m_CondExec 04,GE,GM070DDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGM70                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DDD PGM=DSNUTILB   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=GM070DDD
       ;;
(GM070DDD)
       m_CondExec ${EXAEV},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ****** FIC DE LOAD                                                           
       m_FileAssign -d SHR -g ${G_A35} SYSREC ${DATA}/PEX0/F91.RELOAD.GM70RD
#    RSGM70   : NAME=RSGM70D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGM70 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070DDD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GM070D_GM070DDD_RTGM70.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GM070DDE
       ;;
(GM070DDE)
       m_CondExec 04,GE,GM070DDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM020                                                                      
#  EDITION CAHIER-BLEU (TRI SANS MARQUE)                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DDG PGM=IKJEFT01   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=GM070DDG
       ;;
(GM070DDG)
       m_CondExec ${EXAFA},NE,YES 1,EQ,$[LUNDI] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE.                                                   
#    RSGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLE DES CHEFS DE PRODUITS.                                         
#    RSGA02   : NAME=RSGA02,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA02 /dev/null
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  TABLE DES FAMILLES.                                                  
#    RSGA14   : NAME=RSGA14D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA14 /dev/null
# ******  TABLE VALEURS CODES MARKETING PAR ARTICLE.                           
#    RSGA26   : NAME=RSGA26D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA26 /dev/null
# ******  TABLE CAHIER-BLEU BATCH.                                             
#    RSGM01   : NAME=RSGM01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGM01 /dev/null
# ****** TABLE CAHIER-BLEU PAR ZONES DE PRIX                                   
#    RSGM06   : NAME=RSGM06D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGM06 /dev/null
# ******  RELATION CODIC - SRP                                                 
       m_FileAssign -d SHR -g ${G_A36} FGM020 ${DATA}/PTEM/GM070DAT.BGM069BD
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  SOCIETE : 991000                                                     
       m_FileAssign -d SHR FSOCLIEU ${DATA}/CORTEX4.P.MTXTFIX1/R991SOC
#  NBRE DE JOURS DES EPUISES RECENTS 3C,% ECART PRIX 2C,% ECART COMM 2         
       m_FileAssign -d SHR PARAM ${DATA}/CORTEX4.P.MTXTFIX3/GM070DDG
# ******  FICHIER D'EDITION.                                                   
       m_OutputAssign -c 9 -w BGM020 IMPR
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM020 
       JUMP_LABEL=GM070DDH
       ;;
(GM070DDH)
       m_CondExec 04,GE,GM070DDG ${EXAFA},NE,YES 1,EQ,$[LUNDI] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM020                                                                      
#  EDITION CAHIER-BLEU (TRI AVEC MARQUE)                             *         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DDJ PGM=IKJEFT01   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=GM070DDJ
       ;;
(GM070DDJ)
       m_CondExec ${EXAFF},NE,YES 1,EQ,$[JEUDI] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE.                                                   
#    RSGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLE DES CHEFS DE PRODUITS.                                         
#    RSGA02   : NAME=RSGA02,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA02 /dev/null
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  TABLE DES FAMILLES.                                                  
#    RSGA14   : NAME=RSGA14D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA14 /dev/null
# ******  TABLE VALEURS CODES MARKETING PAR ARTICLE.                           
#    RSGA26   : NAME=RSGA26D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA26 /dev/null
# ******  TABLE CAHIER-BLEU BATCH.                                             
#    RSGM01   : NAME=RSGM01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGM01 /dev/null
# ******  TABLE CAHIER-BLEU PAR ZONES DE PRIX                                  
#    RSGM06   : NAME=RSGM06D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGM06 /dev/null
# ******  RELATION CODIC - SRP                                                 
       m_FileAssign -d SHR -g ${G_A37} FGM020 ${DATA}/PTEM/GM070DAT.BGM069BD
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#  NBRE DE JOURS DES EPUISES RECENTS 3C,% ECART PRIX 2C,% ECART COMM 2         
       m_FileAssign -d SHR PARAM ${DATA}/CORTEX4.P.MTXTFIX3/GM070DDJ
# *  FICHIER D'EDITION.                                                        
       m_OutputAssign -c 9 -w BGM020V IMPR
# *  SOCIETE : 991000                                                          
       m_FileAssign -d SHR FSOCLIEU ${DATA}/CORTEX4.P.MTXTFIX1/R991SOC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM020 
       JUMP_LABEL=GM070DDK
       ;;
(GM070DDK)
       m_CondExec 04,GE,GM070DDJ ${EXAFF},NE,YES 1,EQ,$[JEUDI] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM025    LE 1ER MERCREDI DU MOIS                                           
#  EDITION CAHIER-BLEU AVEC ARTICLES EPUISES ET EN CONTREMARQUE      *         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DDM PGM=IKJEFT01   ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=GM070DDM
       ;;
(GM070DDM)
       m_CondExec ${EXAFK},NE,YES 1,EQ,$[FIRSTME] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE.                                                   
#    RSGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLE DES CHEFS DE PRODUITS.                                         
#    RSGA02   : NAME=RSGA02,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA02 /dev/null
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  TABLE DES FAMILLES.                                                  
#    RSGA14   : NAME=RSGA14D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA14 /dev/null
# ******  TABLE VALEURS CODES MARKETING PAR ARTICLE.                           
#    RSGA26   : NAME=RSGA26D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA26 /dev/null
# ******  TABLE CAHIER-BLEU BATCH.                                             
#    RSGM01   : NAME=RSGM01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGM01 /dev/null
# ******  TABLE CAHIER-BLEU PAR ZONES DE PRIX                                  
#    RSGM06   : NAME=RSGM06D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGM06 /dev/null
#                                                                              
# ******  CARTE DATE.                                                          
       m_FileAssign -i FDATE
$FDATE
_end
#  NBRE DE JOURS DES EPUISES RECENTS 3C,% ECART PRIX 2C,% ECART COMM 2         
       m_FileAssign -d SHR PARAM ${DATA}/CORTEX4.P.MTXTFIX1/GM070DDM
# ******  FICHIER D'EDITION.                                                   
       m_OutputAssign -c 9 -w BGM025 IMPR
# ******  SOCIETE : 991000                                                     
       m_FileAssign -d SHR FSOCLIEU ${DATA}/CORTEX4.P.MTXTFIX1/R991SOC
#                                                                              
# ******  CARTE PARAMETRE.                                                     
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM025 
       JUMP_LABEL=GM070DDN
       ;;
(GM070DDN)
       m_CondExec 04,GE,GM070DDM ${EXAFK},NE,YES 1,EQ,$[FIRSTME] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM030                                                                      
#  EDITION CAHIER-BLEU TOUTES ZONES MAIS IMPRESSION MONOZONE                   
#          SUIVANT LE PARAM SOUS-TABLE ZPRIX                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DDQ PGM=IKJEFT01   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=GM070DDQ
       ;;
(GM070DDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *******  TABLE GENERALISEE.                                                  
#    RSGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01 /dev/null
# *******  TABLE DES LIEUX                                                     
#    RSGA10   : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA10 /dev/null
# *******  TABLE DES CHEFS DE PRODUITS.                                        
#    RSGA02   : NAME=RSGA02,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA02 /dev/null
# *******  TABLE DES FAMILLES.                                                 
#    RSGA14   : NAME=RSGA14D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA14 /dev/null
# *******  TABLE VALEURS CODES MARKETING PAR ARTICLE.                          
#    RSGA26   : NAME=RSGA26D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA26 /dev/null
# *******  TABLE CAHIER-BLEU BATCH.                                            
#    RSGM01   : NAME=RSGM01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGM01 /dev/null
# *******  TABLE CAHIER-BLEU BATCH.                                            
#    RSGM06   : NAME=RSGM06D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGM06 /dev/null
#                                                                              
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#  NBRE DE JOURS DES EPUISES RECENTS 3C,% ECART PRIX 2C,% ECART COMM 2         
       m_FileAssign -d SHR PARAM ${DATA}/CORTEX4.P.MTXTFIX1/GM070DDQ
# ******  SOCIETE : 991000                                                     
       m_FileAssign -d SHR FSOCLIEU ${DATA}/CORTEX4.P.MTXTFIX1/R991SOC
# *******  FICHIERS D'EDITION.                                                 
       m_OutputAssign -c 9 -w IGM03001 IMPR1
       m_OutputAssign -c 9 -w IGM03002 IMPR2
       m_OutputAssign -c 9 -w IGM03003 IMPR3
       m_OutputAssign -c 9 -w IGM03004 IMPR4
       m_OutputAssign -c 9 -w IGM03005 IMPR5
       m_OutputAssign -c 9 -w IGM03006 IMPR6
       m_OutputAssign -c 9 -w IGM03007 IMPR7
       m_OutputAssign -c 9 -w IGM03008 IMPR8
       m_OutputAssign -c 9 -w IGM03009 IMPR9
       m_OutputAssign -c 9 -w IGM03010 IMPR10
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM030 
       JUMP_LABEL=GM070DDR
       ;;
(GM070DDR)
       m_CondExec 04,GE,GM070DDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DDT PGM=SORT       ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=GM070DDT
       ;;
(GM070DDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A38} SORTIN ${DATA}/PTEM/GM070DCJ.BGM075AD
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070DDT.BGM080AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_17_3 17 PD 3
 /FIELDS FLD_CH_270_1 270 CH 1
 /FIELDS FLD_CH_20_67 20 CH 67
 /FIELDS FLD_CH_87_7 87 CH 7
 /FIELDS FLD_CH_7_10 07 CH 10
 /KEYS
   FLD_CH_7_10 ASCENDING,
   FLD_PD_17_3 ASCENDING,
   FLD_CH_20_67 ASCENDING,
   FLD_CH_270_1 ASCENDING,
   FLD_CH_87_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070DDU
       ;;
(GM070DDU)
       m_CondExec 00,EQ,GM070DDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DDX PGM=SORT       ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=GM070DDX
       ;;
(GM070DDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A39} SORTIN ${DATA}/PTEM/GM070DCJ.BGM075ED
       m_FileAssign -d NEW,CATLG,DELETE -r 14 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070DDX.BGM075FD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_8_2 08 CH 2
 /FIELDS FLD_BI_1_7 01 CH 7
 /KEYS
   FLD_BI_8_2 ASCENDING,
   FLD_BI_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070DDY
       ;;
(GM070DDY)
       m_CondExec 00,EQ,GM070DDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGM080                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DEA PGM=IKJEFT01   ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=GM070DEA
       ;;
(GM070DEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTGA11   : NAME=RSGA11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA11 /dev/null
#  FICHIER FDATE                                                               
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER FNSOC                                                               
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#  FICHIER ENTRE                                                               
       m_FileAssign -d SHR -g ${G_A40} FGM075 ${DATA}/PTEM/GM070DDT.BGM080AD
       m_FileAssign -d SHR -g ${G_A41} FGM080 ${DATA}/PTEM/GM070DDX.BGM075FD
#  FICHIER SORTIE                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 IGM080 ${DATA}/PTEM/GM070DEA.BGM080BD
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 IGM085 ${DATA}/PTEM/GM070DEA.BGM085AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM080 
       JUMP_LABEL=GM070DEB
       ;;
(GM070DEB)
       m_CondExec 04,GE,GM070DEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER IGM080                                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DED PGM=SORT       ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=GM070DED
       ;;
(GM070DED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A42} SORTIN ${DATA}/PTEM/GM070DEA.BGM080BD
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070DED.BGM080ED
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_20_3 20 PD 3
 /FIELDS FLD_CH_23_60 23 CH 60
 /FIELDS FLD_PD_148_5 148 PD 5
 /FIELDS FLD_CH_7_10 7 CH 10
 /KEYS
   FLD_CH_7_10 ASCENDING,
   FLD_PD_20_3 ASCENDING,
   FLD_CH_23_60 ASCENDING,
   FLD_PD_148_5 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070DEE
       ;;
(GM070DEE)
       m_CondExec 00,EQ,GM070DED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DEG PGM=IKJEFT01   ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=GM070DEG
       ;;
(GM070DEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A43} FEXTRAC ${DATA}/PTEM/GM070DED.BGM080ED
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/GM070DEG.BGM080CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GM070DEH
       ;;
(GM070DEH)
       m_CondExec 04,GE,GM070DEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DEJ PGM=SORT       ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=GM070DEJ
       ;;
(GM070DEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A44} SORTIN ${DATA}/PTEM/GM070DEG.BGM080CD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070DEJ.BGM080DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070DEK
       ;;
(GM070DEK)
       m_CondExec 00,EQ,GM070DEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT : IGM080                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DEM PGM=IKJEFT01   ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=GM070DEM
       ;;
(GM070DEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A45} FEXTRAC ${DATA}/PTEM/GM070DED.BGM080ED
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A46} FCUMULS ${DATA}/PTEM/GM070DEJ.BGM080DD
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c Z -w IGM080 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GM070DEN
       ;;
(GM070DEN)
       m_CondExec 04,GE,GM070DEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER IGM085                                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DEQ PGM=SORT       ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=GM070DEQ
       ;;
(GM070DEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A47} SORTIN ${DATA}/PTEM/GM070DEA.BGM085AD
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070DEQ.BGM085BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_20_3 20 PD 3
 /FIELDS FLD_CH_7_10 7 CH 10
 /FIELDS FLD_PD_148_5 148 PD 5
 /FIELDS FLD_CH_23_60 23 CH 60
 /KEYS
   FLD_CH_7_10 ASCENDING,
   FLD_PD_20_3 ASCENDING,
   FLD_CH_23_60 ASCENDING,
   FLD_PD_148_5 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070DER
       ;;
(GM070DER)
       m_CondExec 00,EQ,GM070DEQ ${EXAHD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DET PGM=IKJEFT01   ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=GM070DET
       ;;
(GM070DET)
       m_CondExec ${EXAHI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A48} FEXTRAC ${DATA}/PTEM/GM070DEQ.BGM085BD
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/GM070DET.BGM085CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GM070DEU
       ;;
(GM070DEU)
       m_CondExec 04,GE,GM070DET ${EXAHI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DEX PGM=SORT       ** ID=AHN                                   
# ***********************************                                          
       JUMP_LABEL=GM070DEX
       ;;
(GM070DEX)
       m_CondExec ${EXAHN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A49} SORTIN ${DATA}/PTEM/GM070DET.BGM085CD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070DEX.BGM085DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070DEY
       ;;
(GM070DEY)
       m_CondExec 00,EQ,GM070DEX ${EXAHN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT : IGM085                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070DFA PGM=IKJEFT01   ** ID=AHS                                   
# ***********************************                                          
       JUMP_LABEL=GM070DFA
       ;;
(GM070DFA)
       m_CondExec ${EXAHS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A50} FEXTRAC ${DATA}/PTEM/GM070DEQ.BGM085BD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A51} FCUMULS ${DATA}/PTEM/GM070DEX.BGM085DD
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c Z -w IGM085 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GM070DFB
       ;;
(GM070DFB)
       m_CondExec 04,GE,GM070DFA ${EXAHS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GM070DZA
       ;;
(GM070DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
