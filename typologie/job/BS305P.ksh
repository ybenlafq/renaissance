#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BS305P.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPBS305 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 10/09/22 AT 15.01.34 BY BURTECA                      
#    STANDARDS: P  JOBSET: BS305P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BBS300 : EXTRACTION MQSERIES                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=BS305PA
       ;;
(BS305PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=BS305PAA
       ;;
(BS305PAA)
       m_CondExec ${EXAAA},NE,YES 
# *************************************                                        
# *************************************                                        
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES MUTS DU JOUR                                               
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA18   : NAME=RSGA18,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA18 /dev/null
#    RSGA27   : NAME=RSGA27,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA27 /dev/null
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******                                                                       
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/BS305PAA
# ******                                                                       
       m_FileAssign -d SHR FENS ${DATA}/CORTEX4.P.MTXTFIX3/BS305PAA
# ******  FICHIER                                                              
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 10040 -g +1 FLIEN ${DATA}/PTEM/BS305PAA.BBS300AP
#                                                                              
# ******  FICHIER                                                              
# FLIENNC  FILE  NAME=BBS300BP,MODE=O                                          
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS300 
       JUMP_LABEL=BS305PAB
       ;;
(BS305PAB)
       m_CondExec 04,GE,BS305PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRO DU FICHIER BBS300AP                                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS305PAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=BS305PAD
       ;;
(BS305PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g ${G_A1} INPUT ${DATA}/PTEM/BS305PAA.BBS300AP
       m_FileAssign -d NEW,CATLG,DELETE -r 10040 -g +1 OUTPUT ${DATA}/PXX0/F07.BBS300HP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BS305PAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=BS305PAE
       ;;
(BS305PAE)
       m_CondExec 16,NE,BS305PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BBS301 : MAJ DES TABLES DES LIENS                                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS305PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=BS305PAG
       ;;
(BS305PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN ENTREE                                                      
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSBS02   : NAME=RSBS02P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS02 /dev/null
#    RSBS03   : NAME=RSBS03P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS03 /dev/null
#    RSBS04   : NAME=RSBS04P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS04 /dev/null
#    RSBS05   : NAME=RSBS05P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS05 /dev/null
#    RSBS06   : NAME=RSBS06P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS06 /dev/null
# ******  TABLE EN MAJ                                                         
#    RSBS02   : NAME=RSBS02P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBS02 /dev/null
#    RSBS03   : NAME=RSBS03P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBS03 /dev/null
#    RSBS04   : NAME=RSBS04P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBS04 /dev/null
#    RSBS05   : NAME=RSBS05P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBS05 /dev/null
#    RSBS06   : NAME=RSBS06P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBS06 /dev/null
# ******  FIC EN LECTURE                                                       
       m_FileAssign -d SHR -g +0 FBS301E ${DATA}/PXX0/F07.BBS301AP
# ******  FIC EN LECTURE                                                       
       m_FileAssign -d SHR -g ${G_A2} FLIEN ${DATA}/PTEM/BS305PAA.BBS300AP
# ******  EDITION DE TRACE                                                     
       m_OutputAssign -c 9 -w BBS301 FTRACE
# ******  EDITION DE TRACE                                                     
       m_OutputAssign -c 9 -w BBS301H FTRACEH
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******                                                                       
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/BS305PAG
#                                                                              
# ******  FIC EN SORTIE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 132 -g +1 FBS301S ${DATA}/PXX0/F07.BBS301AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS301 
       JUMP_LABEL=BS305PAH
       ;;
(BS305PAH)
       m_CondExec 04,GE,BS305PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BBS302 : TRAITEMENT D ACTIVATION EN MASSE DES LIENS                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS305PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=BS305PAJ
       ;;
(BS305PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN ENTREE                                                      
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
# ******  TABLE EN MAJ                                                         
#    RSBS05   : NAME=RSBS05P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBS05 /dev/null
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS302 
       JUMP_LABEL=BS305PAK
       ;;
(BS305PAK)
       m_CondExec 04,GE,BS305PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DES DONNEES DES TABLES RTGA00 , RTGS36                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS305PAM PGM=PTLDRIVM   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=BS305PAM
       ;;
(BS305PAM)
       m_CondExec ${EXAAU},NE,YES 
# **************************************                                       
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
#    RTGS36   : NAME=RSGS36,MODE=I - DYNAM=YES                                 
# **************************************                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 7 -g +1 SYSREC01 ${DATA}/PTEM/BS305PAM.UNGA00GP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BS305PAM.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# *******************************************************************          
#  TRI DE L'UNLOAD SYSREC01                                                    
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP BS305PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=BS305PAQ
       ;;
(BS305PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/BS305PAM.UNGA00GP
       m_FileAssign -d NEW,CATLG,DELETE -r 7 -g +1 SORTOUT ${DATA}/PTEM/BS305PAQ.UNGA00HP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_7 1 CH 7
 /KEYS
   FLD_BI_1_7 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 7 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BS305PAR
       ;;
(BS305PAR)
       m_CondExec 00,EQ,BS305PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BBS305 : ALIMENTATION DES LISTES GENERIQUES                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS305PAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=BS305PAT
       ;;
(BS305PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES MUTS DU JOUR                                               
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA53   : NAME=RSGA53,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA53 /dev/null
#    RSBS02   : NAME=RSBS02P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS02 /dev/null
#    RSBS03   : NAME=RSBS03P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS03 /dev/null
#    RSBS04   : NAME=RSBS04P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS04 /dev/null
#    RSPR00   : NAME=RSPR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR00 /dev/null
#    RSPR53   : NAME=RSPR53P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR53 /dev/null
# ******  TABLE DETAIL DE MUT                                                  
#    RSBS03   : NAME=RSBS03P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBS03 /dev/null
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLE ENTREE                                                         
       m_FileAssign -d SHR -g ${G_A4} FCODIC ${DATA}/PTEM/BS305PAQ.UNGA00HP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS305 
       JUMP_LABEL=BS305PAU
       ;;
(BS305PAU)
       m_CondExec 04,GE,BS305PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BBS306 : TRI PAR PRIX DES LISTES GENERIQUES                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS305PAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=BS305PAX
       ;;
(BS305PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES MUTS DU JOUR                                               
#    RSBS02   : NAME=RSBS02P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS02 /dev/null
#    RSBS03   : NAME=RSBS03P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS03 /dev/null
#    RSPR10   : NAME=RSPR10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR10 /dev/null
#    RSGN59   : NAME=RSGN59,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGN59 /dev/null
# ******  TABLE DETAIL DE MUT                                                  
#    RSBS03   : NAME=RSBS03P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBS03 /dev/null
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS306 
       JUMP_LABEL=BS305PAY
       ;;
(BS305PAY)
       m_CondExec 04,GE,BS305PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BBS307 : TRAITEMENT DES LIENS GENERIQUES                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS305PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=BS305PBA
       ;;
(BS305PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN ENTREE                                                      
#    RSBS02   : NAME=RSBS02P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS02 /dev/null
#    RSBS03   : NAME=RSBS03P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS03 /dev/null
#    RSBS05   : NAME=RSBS05P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS05 /dev/null
# ******  TABLE EN MAJ                                                         
#    RSBS05   : NAME=RSBS05P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBS05 /dev/null
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS307 
       JUMP_LABEL=BS305PBB
       ;;
(BS305PBB)
       m_CondExec 04,GE,BS305PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BBS308 : TRAITEMENT DU TRI D AFFICHAGE                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS305PBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=BS305PBD
       ;;
(BS305PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN ENTREE                                                      
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGN59   : NAME=RSGN59,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGN59 /dev/null
#    RSBS02   : NAME=RSBS02P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS02 /dev/null
#    RSBS05   : NAME=RSBS05P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS05 /dev/null
# ******  TABLE EN MAJ                                                         
#    RSBS05   : NAME=RSBS05P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBS05 /dev/null
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS308 
       JUMP_LABEL=BS305PBE
       ;;
(BS305PBE)
       m_CondExec 04,GE,BS305PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BBS310 : ALIMENTATION DE LA TABLE RTBS01                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS305PBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=BS305PBG
       ;;
(BS305PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES MUTS DU JOUR                                               
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSGA52   : NAME=RSGA52,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA52 /dev/null
#    RSBS01   : NAME=RSBS01P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS01 /dev/null
#    RSBS02   : NAME=RSBS02P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS02 /dev/null
#    RSBS03   : NAME=RSBS03P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS03 /dev/null
#    RSBS05   : NAME=RSBS05P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS05 /dev/null
#    RSPR00   : NAME=RSPR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR00 /dev/null
# ******  TABLE DETAIL DE MUT                                                  
#    RSBS01   : NAME=RSBS01P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBS01 /dev/null
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS310 
       JUMP_LABEL=BS305PBH
       ;;
(BS305PBH)
       m_CondExec 04,GE,BS305PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=BS305PZA
       ;;
(BS305PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BS305PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
