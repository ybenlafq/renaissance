      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      **********************************************************                
      *   COPY DE LA TABLE RVGI1400                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGI1400                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGI1400.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGI1400.                                                            
      *}                                                                        
           02  GI14-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GI14-CTABINTCA                                                   
               PIC X(0005).                                                     
           02  GI14-CA                                                          
               PIC S9(6) COMP-3.                                                
           02  GI14-FM                                                          
               PIC S9(3) COMP-3.                                                
           02  GI14-TYPPA                                                       
               PIC X.                                                           
           02  GI14-MONTANT                                                     
               PIC S9(3)V99 COMP-3.                                             
           02  GI14-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGI1400                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGI1400-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGI1400-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI14-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI14-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI14-CTABINTCA-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI14-CTABINTCA-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI14-CA-F                                                        
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI14-CA-F                                                        
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI14-FM-F                                                        
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI14-FM-F                                                        
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI14-TYPPA-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI14-TYPPA-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI14-MONTANT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI14-MONTANT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI14-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI14-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
