      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
          02 COMM-GA13-APPLI REDEFINES COMM-GA00-APPLI.                         
             05 COMM-GA13-FONCT               PIC X(3).                         
             05 COMM-GA13-CETAT               PIC X(10).                        
             05 COMM-GA13-NO-PAGE             PIC S9(3)  COMP-3.                
             05 COMM-GA13-TAB.                                                  
                10 COMM-GA13-TABLE     OCCURS  99.                              
                   15 COMM-GA13-TAS           PIC  X.                           
                   15 COMM-GA13-OPER          PIC  X.                           
                   15 COMM-GA13-SELEC         PIC  X.                           
                   15 COMM-GA13-CRFAM         PIC  X(5).                        
                   15 COMM-GA13-CMARK         PIC  X(5).                        
                   15 COMM-GA13-LMARK         PIC  X(20).                       
                   15 COMM-GA13-WSAUT         PIC  X(1).                        
                   15 COMM-GA13-WSEQ          PIC  9(2).                        
                   15 COMM-GA13-WIMBR         PIC  9.                           
                10 COMM-GA13-WFLAGMGI         PIC  X(01).                       
                10 COMM-GA13-FILLER           PIC  X(45).                       
                                                                                
