      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRX200 AU 04/02/1999  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                           12,04,BI,A,                          *        
      *                           16,05,BI,A,                          *        
      *                           21,05,BI,A,                          *        
      *                           26,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRX200.                                                        
            05 NOMETAT-IRX200           PIC X(6) VALUE 'IRX200'.                
            05 RUPTURES-IRX200.                                                 
           10 IRX200-NSOCIETE           PIC X(03).                      007  003
           10 IRX200-NZONPRIX           PIC X(02).                      010  002
           10 IRX200-NCONC              PIC X(04).                      012  004
           10 IRX200-RAYON              PIC X(05).                      016  005
           10 IRX200-CFAM               PIC X(05).                      021  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRX200-SEQUENCE           PIC S9(04) COMP.                026  002
      *--                                                                       
           10 IRX200-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRX200.                                                   
           10 IRX200-LEMPCONC           PIC X(15).                      028  015
           10 IRX200-LENSCONC           PIC X(15).                      043  015
           10 IRX200-LZONPRIX           PIC X(20).                      058  020
           10 IRX200-ALIGNPART          PIC S9(05)      COMP-3.         078  003
           10 IRX200-ALIGNTOT           PIC S9(05)      COMP-3.         081  003
           10 IRX200-INFSRP             PIC S9(03)      COMP-3.         084  002
           10 IRX200-NBREFCONC          PIC S9(05)      COMP-3.         086  003
           10 IRX200-NBREFEXPO          PIC S9(05)      COMP-3.         089  003
           10 IRX200-PVINF              PIC S9(05)      COMP-3.         092  003
           10 IRX200-TRI                PIC S9(09)      COMP-3.         095  005
           10 IRX200-DSAISIE-MAX        PIC X(08).                      100  008
           10 IRX200-DSAISIE-MIN        PIC X(08).                      108  008
            05 FILLER                      PIC X(397).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IRX200-LONG           PIC S9(4)   COMP  VALUE +115.           
      *                                                                         
      *--                                                                       
        01  DSECT-IRX200-LONG           PIC S9(4) COMP-5  VALUE +115.           
                                                                                
      *}                                                                        
