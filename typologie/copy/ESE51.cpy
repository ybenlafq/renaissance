      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESE01   ESE01                                              00000020
      ***************************************************************** 00000030
       01   ESE51I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFAUTL  COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MDEFAUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEFAUTF  PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MDEFAUTI  PIC X(21).                                      00000190
      * FONCTION                                                        00000200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000210
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000220
           02 FILLER    PIC X(4).                                       00000230
           02 MWFONCI   PIC X(3).                                       00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCREDEBL      COMP PIC S9(4).                            00000250
      *--                                                                       
           02 MDCREDEBL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDCREDEBF      PIC X.                                     00000260
           02 FILLER    PIC X(4).                                       00000270
           02 MDCREDEBI      PIC X(8).                                  00000280
      * CODE SERVICE                                                    00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCSERVL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNCSERVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCSERVF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNCSERVI  PIC X(5).                                       00000330
      * LIBELLE LONG CODE SERVICE                                       00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCSERVLL      COMP PIC S9(4).                            00000350
      *--                                                                       
           02 MLCSERVLL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLCSERVLF      PIC X.                                     00000360
           02 FILLER    PIC X(4).                                       00000370
           02 MLCSERVLI      PIC X(20).                                 00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNORDREL  COMP PIC S9(4).                                 00000390
      *--                                                                       
           02 MNORDREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNORDREF  PIC X.                                          00000400
           02 FILLER    PIC X(4).                                       00000410
           02 MNORDREI  PIC X(3).                                       00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCSERVCL      COMP PIC S9(4).                            00000430
      *--                                                                       
           02 MLCSERVCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLCSERVCF      PIC X.                                     00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MLCSERVCI      PIC X(5).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMAJL    COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MDMAJL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDMAJF    PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MDMAJI    PIC X(8).                                       00000500
      * CODE OPERATEUR                                                  00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPERL   COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MCOPERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOPERF   PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MCOPERI   PIC X(5).                                       00000550
      * LIBELLE CODE OPERATEUR                                          00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLOPERL   COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MLOPERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLOPERF   PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MLOPERI   PIC X(20).                                      00000600
      * CODE FAMILLE                                                    00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCFAMI    PIC X(5).                                       00000650
      * LIBELLE CODE FAMILLE                                            00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLFAMI    PIC X(20).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTVAL    COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCTVAL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCTVAF    PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCTVAI    PIC X.                                          00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTVAL    COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MLTVAL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLTVAF    PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MLTVAI    PIC X(17).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCASSORTCL     COMP PIC S9(4).                            00000790
      *--                                                                       
           02 MCASSORTCL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCASSORTCF     PIC X.                                     00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCASSORTCI     PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATECL   COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MDATECL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATECF   PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MDATECI   PIC X(8).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHEFL    COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCHEFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCHEFF    PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCHEFI    PIC X(5).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCHEFL   COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MLCHEFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLCHEFF   PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MLCHEFI   PIC X(20).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCASSORTFL     COMP PIC S9(4).                            00000950
      *--                                                                       
           02 MCASSORTFL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCASSORTFF     PIC X.                                     00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCASSORTFI     PIC X(5).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEFL   COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MDATEFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATEFF   PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MDATEFI   PIC X(8).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMAJEURL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MMAJEURL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMAJEURF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MMAJEURI  PIC X(21).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSERVICEL      COMP PIC S9(4).                            00001070
      *--                                                                       
           02 MSERVICEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSERVICEF      PIC X.                                     00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MSERVICEI      PIC X(20).                                 00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPSERL      COMP PIC S9(4).                            00001110
      *--                                                                       
           02 MCTYPSERL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTYPSERF      PIC X.                                     00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCTYPSERI      PIC X.                                     00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOPTIONL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MOPTIONL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MOPTIONF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MOPTIONI  PIC X(21).                                      00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPINNL      COMP PIC S9(4).                            00001190
      *--                                                                       
           02 MCTYPINNL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTYPINNF      PIC X.                                     00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MCTYPINNI      PIC X.                                     00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODEFACIL     COMP PIC S9(4).                            00001230
      *--                                                                       
           02 MCODEFACIL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCODEFACIF     PIC X.                                     00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MCODEFACII     PIC X(20).                                 00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODEFACL      COMP PIC S9(4).                            00001270
      *--                                                                       
           02 MCODEFACL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCODEFACF      PIC X.                                     00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MCODEFACI      PIC X(21).                                 00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENTRDIVL      COMP PIC S9(4).                            00001310
      *--                                                                       
           02 MENTRDIVL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MENTRDIVF      PIC X.                                     00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MENTRDIVI      PIC X(20).                                 00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPL    COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MCTYPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCTYPF    PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MCTYPI    PIC X(5).                                       00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPL    COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MLTYPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLTYPF    PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MLTYPI    PIC X(20).                                      00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MPAGEI    PIC X(3).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00001470
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MPAGEMAXI      PIC X(3).                                  00001500
           02 MTABLEI OCCURS   5 TIMES .                                00001510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLGESTL      COMP PIC S9(4).                            00001520
      *--                                                                       
             03 MLGESTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLGESTF      PIC X.                                     00001530
             03 FILLER  PIC X(4).                                       00001540
             03 MLGESTI      PIC X(36).                                 00001550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFGESTL      COMP PIC S9(4).                            00001560
      *--                                                                       
             03 MFGESTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MFGESTF      PIC X.                                     00001570
             03 FILLER  PIC X(4).                                       00001580
             03 MFGESTI      PIC X.                                     00001590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVGESTL      COMP PIC S9(4).                            00001600
      *--                                                                       
             03 MVGESTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MVGESTF      PIC X.                                     00001610
             03 FILLER  PIC X(4).                                       00001620
             03 MVGESTI      PIC X(20).                                 00001630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001640
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001650
           02 FILLER    PIC X(4).                                       00001660
           02 MZONCMDI  PIC X(15).                                      00001670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001680
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001690
           02 FILLER    PIC X(4).                                       00001700
           02 MLIBERRI  PIC X(58).                                      00001710
      * CODE TRANSACTION                                                00001720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001730
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001740
           02 FILLER    PIC X(4).                                       00001750
           02 MCODTRAI  PIC X(4).                                       00001760
      * CICS DE TRAVAIL                                                 00001770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001780
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001790
           02 FILLER    PIC X(4).                                       00001800
           02 MCICSI    PIC X(5).                                       00001810
      * NETNAME                                                         00001820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001830
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001840
           02 FILLER    PIC X(4).                                       00001850
           02 MNETNAMI  PIC X(8).                                       00001860
      * CODE TERMINAL                                                   00001870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001880
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001890
           02 FILLER    PIC X(4).                                       00001900
           02 MSCREENI  PIC X(5).                                       00001910
      ***************************************************************** 00001920
      * SDF: ESE01   ESE01                                              00001930
      ***************************************************************** 00001940
       01   ESE51O REDEFINES ESE51I.                                    00001950
           02 FILLER    PIC X(12).                                      00001960
      * DATE DU JOUR                                                    00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MDATJOUA  PIC X.                                          00001990
           02 MDATJOUC  PIC X.                                          00002000
           02 MDATJOUP  PIC X.                                          00002010
           02 MDATJOUH  PIC X.                                          00002020
           02 MDATJOUV  PIC X.                                          00002030
           02 MDATJOUO  PIC X(10).                                      00002040
      * HEURE                                                           00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MTIMJOUA  PIC X.                                          00002070
           02 MTIMJOUC  PIC X.                                          00002080
           02 MTIMJOUP  PIC X.                                          00002090
           02 MTIMJOUH  PIC X.                                          00002100
           02 MTIMJOUV  PIC X.                                          00002110
           02 MTIMJOUO  PIC X(5).                                       00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MDEFAUTA  PIC X.                                          00002140
           02 MDEFAUTC  PIC X.                                          00002150
           02 MDEFAUTP  PIC X.                                          00002160
           02 MDEFAUTH  PIC X.                                          00002170
           02 MDEFAUTV  PIC X.                                          00002180
           02 MDEFAUTO  PIC X(21).                                      00002190
      * FONCTION                                                        00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MWFONCA   PIC X.                                          00002220
           02 MWFONCC   PIC X.                                          00002230
           02 MWFONCP   PIC X.                                          00002240
           02 MWFONCH   PIC X.                                          00002250
           02 MWFONCV   PIC X.                                          00002260
           02 MWFONCO   PIC X(3).                                       00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MDCREDEBA      PIC X.                                     00002290
           02 MDCREDEBC PIC X.                                          00002300
           02 MDCREDEBP PIC X.                                          00002310
           02 MDCREDEBH PIC X.                                          00002320
           02 MDCREDEBV PIC X.                                          00002330
           02 MDCREDEBO      PIC X(8).                                  00002340
      * CODE SERVICE                                                    00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MNCSERVA  PIC X.                                          00002370
           02 MNCSERVC  PIC X.                                          00002380
           02 MNCSERVP  PIC X.                                          00002390
           02 MNCSERVH  PIC X.                                          00002400
           02 MNCSERVV  PIC X.                                          00002410
           02 MNCSERVO  PIC X(5).                                       00002420
      * LIBELLE LONG CODE SERVICE                                       00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MLCSERVLA      PIC X.                                     00002450
           02 MLCSERVLC PIC X.                                          00002460
           02 MLCSERVLP PIC X.                                          00002470
           02 MLCSERVLH PIC X.                                          00002480
           02 MLCSERVLV PIC X.                                          00002490
           02 MLCSERVLO      PIC X(20).                                 00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MNORDREA  PIC X.                                          00002520
           02 MNORDREC  PIC X.                                          00002530
           02 MNORDREP  PIC X.                                          00002540
           02 MNORDREH  PIC X.                                          00002550
           02 MNORDREV  PIC X.                                          00002560
           02 MNORDREO  PIC X(3).                                       00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MLCSERVCA      PIC X.                                     00002590
           02 MLCSERVCC PIC X.                                          00002600
           02 MLCSERVCP PIC X.                                          00002610
           02 MLCSERVCH PIC X.                                          00002620
           02 MLCSERVCV PIC X.                                          00002630
           02 MLCSERVCO      PIC X(5).                                  00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MDMAJA    PIC X.                                          00002660
           02 MDMAJC    PIC X.                                          00002670
           02 MDMAJP    PIC X.                                          00002680
           02 MDMAJH    PIC X.                                          00002690
           02 MDMAJV    PIC X.                                          00002700
           02 MDMAJO    PIC X(8).                                       00002710
      * CODE OPERATEUR                                                  00002720
           02 FILLER    PIC X(2).                                       00002730
           02 MCOPERA   PIC X.                                          00002740
           02 MCOPERC   PIC X.                                          00002750
           02 MCOPERP   PIC X.                                          00002760
           02 MCOPERH   PIC X.                                          00002770
           02 MCOPERV   PIC X.                                          00002780
           02 MCOPERO   PIC X(5).                                       00002790
      * LIBELLE CODE OPERATEUR                                          00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MLOPERA   PIC X.                                          00002820
           02 MLOPERC   PIC X.                                          00002830
           02 MLOPERP   PIC X.                                          00002840
           02 MLOPERH   PIC X.                                          00002850
           02 MLOPERV   PIC X.                                          00002860
           02 MLOPERO   PIC X(20).                                      00002870
      * CODE FAMILLE                                                    00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MCFAMA    PIC X.                                          00002900
           02 MCFAMC    PIC X.                                          00002910
           02 MCFAMP    PIC X.                                          00002920
           02 MCFAMH    PIC X.                                          00002930
           02 MCFAMV    PIC X.                                          00002940
           02 MCFAMO    PIC X(5).                                       00002950
      * LIBELLE CODE FAMILLE                                            00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MLFAMA    PIC X.                                          00002980
           02 MLFAMC    PIC X.                                          00002990
           02 MLFAMP    PIC X.                                          00003000
           02 MLFAMH    PIC X.                                          00003010
           02 MLFAMV    PIC X.                                          00003020
           02 MLFAMO    PIC X(20).                                      00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MCTVAA    PIC X.                                          00003050
           02 MCTVAC    PIC X.                                          00003060
           02 MCTVAP    PIC X.                                          00003070
           02 MCTVAH    PIC X.                                          00003080
           02 MCTVAV    PIC X.                                          00003090
           02 MCTVAO    PIC X.                                          00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MLTVAA    PIC X.                                          00003120
           02 MLTVAC    PIC X.                                          00003130
           02 MLTVAP    PIC X.                                          00003140
           02 MLTVAH    PIC X.                                          00003150
           02 MLTVAV    PIC X.                                          00003160
           02 MLTVAO    PIC X(17).                                      00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MCASSORTCA     PIC X.                                     00003190
           02 MCASSORTCC     PIC X.                                     00003200
           02 MCASSORTCP     PIC X.                                     00003210
           02 MCASSORTCH     PIC X.                                     00003220
           02 MCASSORTCV     PIC X.                                     00003230
           02 MCASSORTCO     PIC X(5).                                  00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MDATECA   PIC X.                                          00003260
           02 MDATECC   PIC X.                                          00003270
           02 MDATECP   PIC X.                                          00003280
           02 MDATECH   PIC X.                                          00003290
           02 MDATECV   PIC X.                                          00003300
           02 MDATECO   PIC X(8).                                       00003310
           02 FILLER    PIC X(2).                                       00003320
           02 MCHEFA    PIC X.                                          00003330
           02 MCHEFC    PIC X.                                          00003340
           02 MCHEFP    PIC X.                                          00003350
           02 MCHEFH    PIC X.                                          00003360
           02 MCHEFV    PIC X.                                          00003370
           02 MCHEFO    PIC X(5).                                       00003380
           02 FILLER    PIC X(2).                                       00003390
           02 MLCHEFA   PIC X.                                          00003400
           02 MLCHEFC   PIC X.                                          00003410
           02 MLCHEFP   PIC X.                                          00003420
           02 MLCHEFH   PIC X.                                          00003430
           02 MLCHEFV   PIC X.                                          00003440
           02 MLCHEFO   PIC X(20).                                      00003450
           02 FILLER    PIC X(2).                                       00003460
           02 MCASSORTFA     PIC X.                                     00003470
           02 MCASSORTFC     PIC X.                                     00003480
           02 MCASSORTFP     PIC X.                                     00003490
           02 MCASSORTFH     PIC X.                                     00003500
           02 MCASSORTFV     PIC X.                                     00003510
           02 MCASSORTFO     PIC X(5).                                  00003520
           02 FILLER    PIC X(2).                                       00003530
           02 MDATEFA   PIC X.                                          00003540
           02 MDATEFC   PIC X.                                          00003550
           02 MDATEFP   PIC X.                                          00003560
           02 MDATEFH   PIC X.                                          00003570
           02 MDATEFV   PIC X.                                          00003580
           02 MDATEFO   PIC X(8).                                       00003590
           02 FILLER    PIC X(2).                                       00003600
           02 MMAJEURA  PIC X.                                          00003610
           02 MMAJEURC  PIC X.                                          00003620
           02 MMAJEURP  PIC X.                                          00003630
           02 MMAJEURH  PIC X.                                          00003640
           02 MMAJEURV  PIC X.                                          00003650
           02 MMAJEURO  PIC X(21).                                      00003660
           02 FILLER    PIC X(2).                                       00003670
           02 MSERVICEA      PIC X.                                     00003680
           02 MSERVICEC PIC X.                                          00003690
           02 MSERVICEP PIC X.                                          00003700
           02 MSERVICEH PIC X.                                          00003710
           02 MSERVICEV PIC X.                                          00003720
           02 MSERVICEO      PIC X(20).                                 00003730
           02 FILLER    PIC X(2).                                       00003740
           02 MCTYPSERA      PIC X.                                     00003750
           02 MCTYPSERC PIC X.                                          00003760
           02 MCTYPSERP PIC X.                                          00003770
           02 MCTYPSERH PIC X.                                          00003780
           02 MCTYPSERV PIC X.                                          00003790
           02 MCTYPSERO      PIC X.                                     00003800
           02 FILLER    PIC X(2).                                       00003810
           02 MOPTIONA  PIC X.                                          00003820
           02 MOPTIONC  PIC X.                                          00003830
           02 MOPTIONP  PIC X.                                          00003840
           02 MOPTIONH  PIC X.                                          00003850
           02 MOPTIONV  PIC X.                                          00003860
           02 MOPTIONO  PIC X(21).                                      00003870
           02 FILLER    PIC X(2).                                       00003880
           02 MCTYPINNA      PIC X.                                     00003890
           02 MCTYPINNC PIC X.                                          00003900
           02 MCTYPINNP PIC X.                                          00003910
           02 MCTYPINNH PIC X.                                          00003920
           02 MCTYPINNV PIC X.                                          00003930
           02 MCTYPINNO      PIC X.                                     00003940
           02 FILLER    PIC X(2).                                       00003950
           02 MCODEFACIA     PIC X.                                     00003960
           02 MCODEFACIC     PIC X.                                     00003970
           02 MCODEFACIP     PIC X.                                     00003980
           02 MCODEFACIH     PIC X.                                     00003990
           02 MCODEFACIV     PIC X.                                     00004000
           02 MCODEFACIO     PIC X(20).                                 00004010
           02 FILLER    PIC X(2).                                       00004020
           02 MCODEFACA      PIC X.                                     00004030
           02 MCODEFACC PIC X.                                          00004040
           02 MCODEFACP PIC X.                                          00004050
           02 MCODEFACH PIC X.                                          00004060
           02 MCODEFACV PIC X.                                          00004070
           02 MCODEFACO      PIC X(21).                                 00004080
           02 FILLER    PIC X(2).                                       00004090
           02 MENTRDIVA      PIC X.                                     00004100
           02 MENTRDIVC PIC X.                                          00004110
           02 MENTRDIVP PIC X.                                          00004120
           02 MENTRDIVH PIC X.                                          00004130
           02 MENTRDIVV PIC X.                                          00004140
           02 MENTRDIVO      PIC X(20).                                 00004150
           02 FILLER    PIC X(2).                                       00004160
           02 MCTYPA    PIC X.                                          00004170
           02 MCTYPC    PIC X.                                          00004180
           02 MCTYPP    PIC X.                                          00004190
           02 MCTYPH    PIC X.                                          00004200
           02 MCTYPV    PIC X.                                          00004210
           02 MCTYPO    PIC X(5).                                       00004220
           02 FILLER    PIC X(2).                                       00004230
           02 MLTYPA    PIC X.                                          00004240
           02 MLTYPC    PIC X.                                          00004250
           02 MLTYPP    PIC X.                                          00004260
           02 MLTYPH    PIC X.                                          00004270
           02 MLTYPV    PIC X.                                          00004280
           02 MLTYPO    PIC X(20).                                      00004290
           02 FILLER    PIC X(2).                                       00004300
           02 MPAGEA    PIC X.                                          00004310
           02 MPAGEC    PIC X.                                          00004320
           02 MPAGEP    PIC X.                                          00004330
           02 MPAGEH    PIC X.                                          00004340
           02 MPAGEV    PIC X.                                          00004350
           02 MPAGEO    PIC X(3).                                       00004360
           02 FILLER    PIC X(2).                                       00004370
           02 MPAGEMAXA      PIC X.                                     00004380
           02 MPAGEMAXC PIC X.                                          00004390
           02 MPAGEMAXP PIC X.                                          00004400
           02 MPAGEMAXH PIC X.                                          00004410
           02 MPAGEMAXV PIC X.                                          00004420
           02 MPAGEMAXO      PIC X(3).                                  00004430
           02 MTABLEO OCCURS   5 TIMES .                                00004440
             03 FILLER       PIC X(2).                                  00004450
             03 MLGESTA      PIC X.                                     00004460
             03 MLGESTC PIC X.                                          00004470
             03 MLGESTP PIC X.                                          00004480
             03 MLGESTH PIC X.                                          00004490
             03 MLGESTV PIC X.                                          00004500
             03 MLGESTO      PIC X(36).                                 00004510
             03 FILLER       PIC X(2).                                  00004520
             03 MFGESTA      PIC X.                                     00004530
             03 MFGESTC PIC X.                                          00004540
             03 MFGESTP PIC X.                                          00004550
             03 MFGESTH PIC X.                                          00004560
             03 MFGESTV PIC X.                                          00004570
             03 MFGESTO      PIC X.                                     00004580
             03 FILLER       PIC X(2).                                  00004590
             03 MVGESTA      PIC X.                                     00004600
             03 MVGESTC PIC X.                                          00004610
             03 MVGESTP PIC X.                                          00004620
             03 MVGESTH PIC X.                                          00004630
             03 MVGESTV PIC X.                                          00004640
             03 MVGESTO      PIC X(20).                                 00004650
           02 FILLER    PIC X(2).                                       00004660
           02 MZONCMDA  PIC X.                                          00004670
           02 MZONCMDC  PIC X.                                          00004680
           02 MZONCMDP  PIC X.                                          00004690
           02 MZONCMDH  PIC X.                                          00004700
           02 MZONCMDV  PIC X.                                          00004710
           02 MZONCMDO  PIC X(15).                                      00004720
           02 FILLER    PIC X(2).                                       00004730
           02 MLIBERRA  PIC X.                                          00004740
           02 MLIBERRC  PIC X.                                          00004750
           02 MLIBERRP  PIC X.                                          00004760
           02 MLIBERRH  PIC X.                                          00004770
           02 MLIBERRV  PIC X.                                          00004780
           02 MLIBERRO  PIC X(58).                                      00004790
      * CODE TRANSACTION                                                00004800
           02 FILLER    PIC X(2).                                       00004810
           02 MCODTRAA  PIC X.                                          00004820
           02 MCODTRAC  PIC X.                                          00004830
           02 MCODTRAP  PIC X.                                          00004840
           02 MCODTRAH  PIC X.                                          00004850
           02 MCODTRAV  PIC X.                                          00004860
           02 MCODTRAO  PIC X(4).                                       00004870
      * CICS DE TRAVAIL                                                 00004880
           02 FILLER    PIC X(2).                                       00004890
           02 MCICSA    PIC X.                                          00004900
           02 MCICSC    PIC X.                                          00004910
           02 MCICSP    PIC X.                                          00004920
           02 MCICSH    PIC X.                                          00004930
           02 MCICSV    PIC X.                                          00004940
           02 MCICSO    PIC X(5).                                       00004950
      * NETNAME                                                         00004960
           02 FILLER    PIC X(2).                                       00004970
           02 MNETNAMA  PIC X.                                          00004980
           02 MNETNAMC  PIC X.                                          00004990
           02 MNETNAMP  PIC X.                                          00005000
           02 MNETNAMH  PIC X.                                          00005010
           02 MNETNAMV  PIC X.                                          00005020
           02 MNETNAMO  PIC X(8).                                       00005030
      * CODE TERMINAL                                                   00005040
           02 FILLER    PIC X(2).                                       00005050
           02 MSCREENA  PIC X.                                          00005060
           02 MSCREENC  PIC X.                                          00005070
           02 MSCREENP  PIC X.                                          00005080
           02 MSCREENH  PIC X.                                          00005090
           02 MSCREENV  PIC X.                                          00005100
           02 MSCREENO  PIC X(5).                                       00005110
                                                                                
