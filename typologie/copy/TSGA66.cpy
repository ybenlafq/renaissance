      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : IDENTIFICATION ARTICLE - CODES DESCRIPTIFS             *         
      *****************************************************************         
      *                                                               *         
       01  TS-GA66.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GA66-LONG                 PIC S9(5) COMP-3                     
                                           VALUE +110.                          
           02 TS-GA66-DONNEES.                                                  
      *------------------------------TABLE TYPE DE DECLARATION                  
              03 TS-GA66-PARTIE1.                                               
                 04 TS-GA66-NCODIC           PIC X(7).                          
                 04 TS-GA66-NTITRE           PIC X(5).                          
                 04 TS-GA66-LTEXTE           PIC X(40).                         
                 04 TS-GA66-LGMAX            PIC 9(2).                          
                 04 TS-GA66-LIBELLE.                                            
                    05 TS-GA66-LRUBRIQ          PIC X(10).                      
                    05 TS-GA66-FILLER           PIC X(25).                      
                 04 TS-GA66-COULEUR.                                            
                    05 TS-GA66-CCOULTEXTE       PIC X(5).                       
                    05 TS-GA66-NDEBTEXTE        PIC X(2).                       
                    05 TS-GA66-NFINTEXTE        PIC X(2).                       
                    05 TS-GA66-CCOULFOND        PIC X(5).                       
                    05 TS-GA66-NDEBFOND         PIC X(2).                       
                    05 TS-GA66-LGRFOND          PIC X(4).                       
                 04 TS-GA66-CMAJ             PIC X(1).                          
      *                                                                         
                                                                                
