      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * MAJ DES PRIX ARTICLES ET PRIMES                                 00000020
      ***************************************************************** 00000030
       01   EGN10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNCODICI  PIC X(7).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLREFI    PIC X(20).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCFAMI    PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCMARQI   PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLMARQI   PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICSL      COMP PIC S9(4).                                    
      *--                                                                       
           02 MNCODICSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODICSF      PIC X.                                             
           02 FILLER    PIC X(4).                                               
           02 MNCODICSI      PIC X(7).                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCODICSL      COMP PIC S9(4).                                    
      *--                                                                       
           02 MLCODICSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLCODICSF      PIC X.                                             
           02 FILLER    PIC X(4).                                               
           02 MLCODICSI      PIC X(20).                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSL      COMP PIC S9(4).                                         
      *--                                                                       
           02 MCSL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MCSF      PIC X.                                                  
           02 FILLER    PIC X(4).                                               
           02 MCSI      PIC X(5).                                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNSOCI    PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBSUBSL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLIBSUBSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBSUBSF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLIBSUBSI      PIC X(9).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPROL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAPPROF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCAPPROI  PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRMPL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MPRMPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPRMPF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MPRMPI    PIC X(8).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIOAL     COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MIOAL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MIOAF     PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MIOAI     PIC X.                                          00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIDEBL    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MIDEBL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MIDEBF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MIDEBI    PIC X(8).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MICODSUL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MICODSUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MICODSUF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MICODSUI  PIC X(7).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSSAPPL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MSSAPPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSSAPPF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MSSAPPI   PIC X.                                          00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPOL   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXPOF   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCEXPOI   PIC X(5).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSRPL     COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MSRPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSRPF     PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MSRPI     PIC X(8).                                       00000730
           02 MACTD OCCURS   3 TIMES .                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTL   COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MACTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MACTF   PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MACTI   PIC X.                                          00000780
           02 MOAD OCCURS   3 TIMES .                                   00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MOAL    COMP PIC S9(4).                                 00000800
      *--                                                                       
             03 MOAL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MOAF    PIC X.                                          00000810
             03 FILLER  PIC X(4).                                       00000820
             03 MOAI    PIC X.                                          00000830
           02 MDEBOAD OCCURS   3 TIMES .                                00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEBOAL      COMP PIC S9(4).                            00000850
      *--                                                                       
             03 MDEBOAL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDEBOAF      PIC X.                                     00000860
             03 FILLER  PIC X(4).                                       00000870
             03 MDEBOAI      PIC X(8).                                  00000880
           02 MCODSUBD OCCURS   3 TIMES .                               00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODSUBL     COMP PIC S9(4).                            00000900
      *--                                                                       
             03 MCODSUBL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCODSUBF     PIC X.                                     00000910
             03 FILLER  PIC X(4).                                       00000920
             03 MCODSUBI     PIC X(7).                                  00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMPACL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MCOMPACL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCOMPACF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MCOMPACI  PIC X(3).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRAL     COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MPRAL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MPRAF     PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MPRAI     PIC X(8).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTKNATL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MSTKNATL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTKNATF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MSTKNATI  PIC X(7).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCDEPL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MSOCDEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCDEPF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MSOCDEPI  PIC X(3).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPOTL   COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MDEPOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEPOTF   PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MDEPOTI   PIC X(3).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTEDEPL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MQTEDEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQTEDEPF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MQTEDEPI  PIC X(6).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATCDEL  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MDATCDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATCDEF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MDATCDEI  PIC X(10).                                      00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTECDEL  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MQTECDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQTECDEF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MQTECDEI  PIC X(6).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBMBL   COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MLIBMBL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIBMBF   PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MLIBMBI   PIC X(3).                                       00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBVAR1L      COMP PIC S9(4).                            00001300
      *--                                                                       
           02 MLIBVAR1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBVAR1F      PIC X.                                     00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MLIBVAR1I      PIC X(11).                                 00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOPSPEL   COMP PIC S9(4).                                         
      *--                                                                       
           02 MOPSPEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MOPSPEF   PIC X.                                                  
           02 FILLER    PIC X(4).                                               
           02 MOPSPEI   PIC X(19).                                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIPVRL    COMP PIC S9(4).                                 00001340
      *--                                                                       
           02 MIPVRL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MIPVRF    PIC X.                                          00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MIPVRI    PIC X(8).                                       00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MICIMPL   COMP PIC S9(4).                                 00001380
      *--                                                                       
           02 MICIMPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MICIMPF   PIC X.                                          00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MICIMPI   PIC X.                                          00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIDPVRL   COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MIDPVRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MIDPVRF   PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MIDPVRI   PIC X(8).                                       00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MICOMML   COMP PIC S9(4).                                 00001460
      *--                                                                       
           02 MICOMML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MICOMMF   PIC X.                                          00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MICOMMI   PIC X(10).                                      00001490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MITMBL    COMP PIC S9(4).                                 00001500
      *--                                                                       
           02 MITMBL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MITMBF    PIC X.                                          00001510
           02 FILLER    PIC X(4).                                       00001520
           02 MITMBI    PIC X(7).                                       00001530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIPRRL    COMP PIC S9(4).                                 00001540
      *--                                                                       
           02 MIPRRL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MIPRRF    PIC X.                                          00001550
           02 FILLER    PIC X(4).                                       00001560
           02 MIPRRI    PIC X(6).                                       00001570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIDPRRL   COMP PIC S9(4).                                 00001580
      *--                                                                       
           02 MIDPRRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MIDPRRF   PIC X.                                          00001590
           02 FILLER    PIC X(4).                                       00001600
           02 MIDPRRI   PIC X(8).                                       00001610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MICPRRL   COMP PIC S9(4).                                 00001620
      *--                                                                       
           02 MICPRRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MICPRRF   PIC X.                                          00001630
           02 FILLER    PIC X(4).                                       00001640
           02 MICPRRI   PIC X(10).                                      00001650
           02 MACT2D OCCURS   4 TIMES .                                 00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACT2L  COMP PIC S9(4).                                 00001670
      *--                                                                       
             03 MACT2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MACT2F  PIC X.                                          00001680
             03 FILLER  PIC X(4).                                       00001690
             03 MACT2I  PIC X.                                          00001700
           02 MPVRD OCCURS   4 TIMES .                                  00001710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPVRL   COMP PIC S9(4).                                 00001720
      *--                                                                       
             03 MPVRL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MPVRF   PIC X.                                          00001730
             03 FILLER  PIC X(4).                                       00001740
             03 MPVRI   PIC X(8).                                       00001750
           02 MCIMPERD OCCURS   4 TIMES .                               00001760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCIMPERL     COMP PIC S9(4).                            00001770
      *--                                                                       
             03 MCIMPERL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCIMPERF     PIC X.                                     00001780
             03 FILLER  PIC X(4).                                       00001790
             03 MCIMPERI     PIC X.                                     00001800
           02 MTMBD OCCURS   4 TIMES .                                  00001810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTMBL   COMP PIC S9(4).                                 00001820
      *--                                                                       
             03 MTMBL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MTMBF   PIC X.                                          00001830
             03 FILLER  PIC X(4).                                       00001840
             03 MTMBI   PIC X(7).                                       00001850
           02 MACT4D OCCURS   9 TIMES .                                 00001860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACT4L  COMP PIC S9(4).                                 00001870
      *--                                                                       
             03 MACT4L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MACT4F  PIC X.                                          00001880
             03 FILLER  PIC X(4).                                       00001890
             03 MACT4I  PIC X.                                          00001900
           02 MTYPCOMMD OCCURS   9 TIMES .                              00001910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPCOMML    COMP PIC S9(4).                            00001920
      *--                                                                       
             03 MTYPCOMML COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MTYPCOMMF    PIC X.                                     00001930
             03 FILLER  PIC X(4).                                       00001940
             03 MTYPCOMMI    PIC X.                                     00001950
           02 MPRRD OCCURS   9 TIMES .                                  00001960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRRL   COMP PIC S9(4).                                 00001970
      *--                                                                       
             03 MPRRL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MPRRF   PIC X.                                          00001980
             03 FILLER  PIC X(4).                                       00001990
             03 MPRRI   PIC X(6).                                       00002000
           02 MDPRRD OCCURS   9 TIMES .                                 00002010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDPRRL  COMP PIC S9(4).                                 00002020
      *--                                                                       
             03 MDPRRL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDPRRF  PIC X.                                          00002030
             03 FILLER  PIC X(4).                                       00002040
             03 MDPRRI  PIC X(8).                                       00002050
           02 MWIMPERD OCCURS   4 TIMES .                               00002060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWIMPERL     COMP PIC S9(4).                            00002070
      *--                                                                       
             03 MWIMPERL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWIMPERF     PIC X.                                     00002080
             03 FILLER  PIC X(4).                                       00002090
             03 MWIMPERI     PIC X.                                     00002100
           02 MDPVRD OCCURS   4 TIMES .                                 00002110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDPVRL  COMP PIC S9(4).                                 00002120
      *--                                                                       
             03 MDPVRL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDPVRF  PIC X.                                          00002130
             03 FILLER  PIC X(4).                                       00002140
             03 MDPVRI  PIC X(8).                                       00002150
           02 MCOMMD OCCURS   4 TIMES .                                 00002160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMML  COMP PIC S9(4).                                 00002170
      *--                                                                       
             03 MCOMML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCOMMF  PIC X.                                          00002180
             03 FILLER  PIC X(4).                                       00002190
             03 MCOMMI  PIC X(10).                                      00002200
           02 MCPRRD OCCURS   9 TIMES .                                 00002210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPRRL  COMP PIC S9(4).                                 00002220
      *--                                                                       
             03 MCPRRL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCPRRF  PIC X.                                          00002230
             03 FILLER  PIC X(4).                                       00002240
             03 MCPRRI  PIC X(10).                                      00002250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMAJCGAL  COMP PIC S9(4).                                         
      *--                                                                       
           02 MMAJCGAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMAJCGAF  PIC X.                                                  
           02 FILLER    PIC X(4).                                       00002280
           02 MMAJCGAI  PIC X.                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCROSSL   COMP PIC S9(4).                                         
      *--                                                                       
           02 MCROSSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCROSSF   PIC X.                                                  
           02 FILLER    PIC X(4).                                       00002320
           02 MCROSSI   PIC X.                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MB2BOCL   COMP PIC S9(4).                                         
      *--                                                                       
           02 MB2BOCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MB2BOCF   PIC X.                                                  
           02 FILLER    PIC X(4).                                       00002360
           02 MB2BOCI   PIC X.                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MB2BDL    COMP PIC S9(4).                                         
      *--                                                                       
           02 MB2BDL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MB2BDF    PIC X.                                                  
           02 FILLER    PIC X(4).                                       00002400
           02 MB2BDI    PIC X(8).                                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MB2BLL    COMP PIC S9(4).                                         
      *--                                                                       
           02 MB2BLL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MB2BLF    PIC X.                                                  
           02 FILLER    PIC X(4).                                       00002440
           02 MB2BLI    PIC X(10).                                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MB2BOCHL  COMP PIC S9(4).                                         
      *--                                                                       
           02 MB2BOCHL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MB2BOCHF  PIC X.                                                  
           02 FILLER    PIC X(4).                                       00002480
           02 MB2BOCHI  PIC X.                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MB2BDHL   COMP PIC S9(4).                                         
      *--                                                                       
           02 MB2BDHL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MB2BDHF   PIC X.                                                  
           02 FILLER    PIC X(4).                                               
           02 MB2BDHI   PIC X(8).                                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MB2BLHL   COMP PIC S9(4).                                         
      *--                                                                       
           02 MB2BLHL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MB2BLHF   PIC X.                                                  
           02 FILLER    PIC X(4).                                               
           02 MB2BLHI   PIC X(10).                                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00002800
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00002810
           02 FILLER    PIC X(4).                                       00002820
           02 MLIBERRI  PIC X(78).                                      00002830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00002840
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00002850
           02 FILLER    PIC X(4).                                       00002860
           02 MCODTRAI  PIC X(4).                                       00002870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00002880
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00002890
           02 FILLER    PIC X(4).                                       00002900
           02 MCICSI    PIC X(5).                                       00002910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00002920
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00002930
           02 FILLER    PIC X(4).                                       00002940
           02 MNETNAMI  PIC X(8).                                       00002950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00002960
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00002970
           02 FILLER    PIC X(4).                                       00002980
           02 MSCREENI  PIC X(4).                                       00002990
      ***************************************************************** 00003000
      * MAJ DES PRIX ARTICLES ET PRIMES                                 00003010
      ***************************************************************** 00003020
       01   EGN10O REDEFINES EGN10I.                                    00003030
           02 FILLER    PIC X(12).                                      00003040
           02 FILLER    PIC X(2).                                       00003050
           02 MDATJOUA  PIC X.                                          00003060
           02 MDATJOUC  PIC X.                                          00003070
           02 MDATJOUP  PIC X.                                          00003080
           02 MDATJOUH  PIC X.                                          00003090
           02 MDATJOUV  PIC X.                                          00003100
           02 MDATJOUO  PIC X(10).                                      00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MTIMJOUA  PIC X.                                          00003130
           02 MTIMJOUC  PIC X.                                          00003140
           02 MTIMJOUP  PIC X.                                          00003150
           02 MTIMJOUH  PIC X.                                          00003160
           02 MTIMJOUV  PIC X.                                          00003170
           02 MTIMJOUO  PIC X(5).                                       00003180
           02 FILLER    PIC X(2).                                       00003190
           02 MNCODICA  PIC X.                                          00003200
           02 MNCODICC  PIC X.                                          00003210
           02 MNCODICP  PIC X.                                          00003220
           02 MNCODICH  PIC X.                                          00003230
           02 MNCODICV  PIC X.                                          00003240
           02 MNCODICO  PIC X(7).                                       00003250
           02 FILLER    PIC X(2).                                       00003260
           02 MLREFA    PIC X.                                          00003270
           02 MLREFC    PIC X.                                          00003280
           02 MLREFP    PIC X.                                          00003290
           02 MLREFH    PIC X.                                          00003300
           02 MLREFV    PIC X.                                          00003310
           02 MLREFO    PIC X(20).                                      00003320
           02 FILLER    PIC X(2).                                       00003330
           02 MCFAMA    PIC X.                                          00003340
           02 MCFAMC    PIC X.                                          00003350
           02 MCFAMP    PIC X.                                          00003360
           02 MCFAMH    PIC X.                                          00003370
           02 MCFAMV    PIC X.                                          00003380
           02 MCFAMO    PIC X(5).                                       00003390
           02 FILLER    PIC X(2).                                       00003400
           02 MCMARQA   PIC X.                                          00003410
           02 MCMARQC   PIC X.                                          00003420
           02 MCMARQP   PIC X.                                          00003430
           02 MCMARQH   PIC X.                                          00003440
           02 MCMARQV   PIC X.                                          00003450
           02 MCMARQO   PIC X(5).                                       00003460
           02 FILLER    PIC X(2).                                       00003470
           02 MLMARQA   PIC X.                                          00003480
           02 MLMARQC   PIC X.                                          00003490
           02 MLMARQP   PIC X.                                          00003500
           02 MLMARQH   PIC X.                                          00003510
           02 MLMARQV   PIC X.                                          00003520
           02 MLMARQO   PIC X(20).                                      00003530
           02 FILLER    PIC X(2).                                       00003540
           02 MNCODICSA      PIC X.                                             
           02 MNCODICSC PIC X.                                                  
           02 MNCODICSP PIC X.                                                  
           02 MNCODICSH PIC X.                                                  
           02 MNCODICSV PIC X.                                                  
           02 MNCODICSO      PIC X(7).                                          
           02 FILLER    PIC X(2).                                               
           02 MLCODICSA      PIC X.                                             
           02 MLCODICSC PIC X.                                                  
           02 MLCODICSP PIC X.                                                  
           02 MLCODICSH PIC X.                                                  
           02 MLCODICSV PIC X.                                                  
           02 MLCODICSO      PIC X(20).                                         
           02 FILLER    PIC X(2).                                               
           02 MCSA      PIC X.                                                  
           02 MCSC      PIC X.                                                  
           02 MCSP      PIC X.                                                  
           02 MCSH      PIC X.                                                  
           02 MCSV      PIC X.                                                  
           02 MCSO      PIC X(5).                                               
           02 FILLER    PIC X(2).                                               
           02 MNSOCA    PIC X.                                          00003550
           02 MNSOCC    PIC X.                                          00003560
           02 MNSOCP    PIC X.                                          00003570
           02 MNSOCH    PIC X.                                          00003580
           02 MNSOCV    PIC X.                                          00003590
           02 MNSOCO    PIC X(3).                                       00003600
           02 FILLER    PIC X(2).                                       00003610
           02 MLIBSUBSA      PIC X.                                     00003620
           02 MLIBSUBSC PIC X.                                          00003630
           02 MLIBSUBSP PIC X.                                          00003640
           02 MLIBSUBSH PIC X.                                          00003650
           02 MLIBSUBSV PIC X.                                          00003660
           02 MLIBSUBSO      PIC X(9).                                  00003670
           02 FILLER    PIC X(2).                                       00003680
           02 MCAPPROA  PIC X.                                          00003690
           02 MCAPPROC  PIC X.                                          00003700
           02 MCAPPROP  PIC X.                                          00003710
           02 MCAPPROH  PIC X.                                          00003720
           02 MCAPPROV  PIC X.                                          00003730
           02 MCAPPROO  PIC X(5).                                       00003740
           02 FILLER    PIC X(2).                                       00003750
           02 MPRMPA    PIC X.                                          00003760
           02 MPRMPC    PIC X.                                          00003770
           02 MPRMPP    PIC X.                                          00003780
           02 MPRMPH    PIC X.                                          00003790
           02 MPRMPV    PIC X.                                          00003800
           02 MPRMPO    PIC X(8).                                       00003810
           02 FILLER    PIC X(2).                                       00003820
           02 MIOAA     PIC X.                                          00003830
           02 MIOAC     PIC X.                                          00003840
           02 MIOAP     PIC X.                                          00003850
           02 MIOAH     PIC X.                                          00003860
           02 MIOAV     PIC X.                                          00003870
           02 MIOAO     PIC X.                                          00003880
           02 FILLER    PIC X(2).                                       00003890
           02 MIDEBA    PIC X.                                          00003900
           02 MIDEBC    PIC X.                                          00003910
           02 MIDEBP    PIC X.                                          00003920
           02 MIDEBH    PIC X.                                          00003930
           02 MIDEBV    PIC X.                                          00003940
           02 MIDEBO    PIC X(8).                                       00003950
           02 FILLER    PIC X(2).                                       00003960
           02 MICODSUA  PIC X.                                          00003970
           02 MICODSUC  PIC X.                                          00003980
           02 MICODSUP  PIC X.                                          00003990
           02 MICODSUH  PIC X.                                          00004000
           02 MICODSUV  PIC X.                                          00004010
           02 MICODSUO  PIC X(7).                                       00004020
           02 FILLER    PIC X(2).                                       00004030
           02 MSSAPPA   PIC X.                                          00004040
           02 MSSAPPC   PIC X.                                          00004050
           02 MSSAPPP   PIC X.                                          00004060
           02 MSSAPPH   PIC X.                                          00004070
           02 MSSAPPV   PIC X.                                          00004080
           02 MSSAPPO   PIC X.                                          00004090
           02 FILLER    PIC X(2).                                       00004100
           02 MCEXPOA   PIC X.                                          00004110
           02 MCEXPOC   PIC X.                                          00004120
           02 MCEXPOP   PIC X.                                          00004130
           02 MCEXPOH   PIC X.                                          00004140
           02 MCEXPOV   PIC X.                                          00004150
           02 MCEXPOO   PIC X(5).                                       00004160
           02 FILLER    PIC X(2).                                       00004170
           02 MSRPA     PIC X.                                          00004180
           02 MSRPC     PIC X.                                          00004190
           02 MSRPP     PIC X.                                          00004200
           02 MSRPH     PIC X.                                          00004210
           02 MSRPV     PIC X.                                          00004220
           02 MSRPO     PIC X(8).                                       00004230
           02 DFHMS1 OCCURS   3 TIMES .                                 00004240
             03 FILLER       PIC X(2).                                  00004250
             03 MACTA   PIC X.                                          00004260
             03 MACTC   PIC X.                                          00004270
             03 MACTP   PIC X.                                          00004280
             03 MACTH   PIC X.                                          00004290
             03 MACTV   PIC X.                                          00004300
             03 MACTO   PIC X.                                          00004310
           02 DFHMS2 OCCURS   3 TIMES .                                 00004320
             03 FILLER       PIC X(2).                                  00004330
             03 MOAA    PIC X.                                          00004340
             03 MOAC    PIC X.                                          00004350
             03 MOAP    PIC X.                                          00004360
             03 MOAH    PIC X.                                          00004370
             03 MOAV    PIC X.                                          00004380
             03 MOAO    PIC X.                                          00004390
           02 DFHMS3 OCCURS   3 TIMES .                                 00004400
             03 FILLER       PIC X(2).                                  00004410
             03 MDEBOAA      PIC X.                                     00004420
             03 MDEBOAC PIC X.                                          00004430
             03 MDEBOAP PIC X.                                          00004440
             03 MDEBOAH PIC X.                                          00004450
             03 MDEBOAV PIC X.                                          00004460
             03 MDEBOAO      PIC X(8).                                  00004470
           02 DFHMS4 OCCURS   3 TIMES .                                 00004480
             03 FILLER       PIC X(2).                                  00004490
             03 MCODSUBA     PIC X.                                     00004500
             03 MCODSUBC     PIC X.                                     00004510
             03 MCODSUBP     PIC X.                                     00004520
             03 MCODSUBH     PIC X.                                     00004530
             03 MCODSUBV     PIC X.                                     00004540
             03 MCODSUBO     PIC X(7).                                  00004550
           02 FILLER    PIC X(2).                                       00004560
           02 MCOMPACA  PIC X.                                          00004570
           02 MCOMPACC  PIC X.                                          00004580
           02 MCOMPACP  PIC X.                                          00004590
           02 MCOMPACH  PIC X.                                          00004600
           02 MCOMPACV  PIC X.                                          00004610
           02 MCOMPACO  PIC X(3).                                       00004620
           02 FILLER    PIC X(2).                                       00004630
           02 MPRAA     PIC X.                                          00004640
           02 MPRAC     PIC X.                                          00004650
           02 MPRAP     PIC X.                                          00004660
           02 MPRAH     PIC X.                                          00004670
           02 MPRAV     PIC X.                                          00004680
           02 MPRAO     PIC X(8).                                       00004690
           02 FILLER    PIC X(2).                                       00004700
           02 MSTKNATA  PIC X.                                          00004710
           02 MSTKNATC  PIC X.                                          00004720
           02 MSTKNATP  PIC X.                                          00004730
           02 MSTKNATH  PIC X.                                          00004740
           02 MSTKNATV  PIC X.                                          00004750
           02 MSTKNATO  PIC X(7).                                       00004760
           02 FILLER    PIC X(2).                                       00004770
           02 MSOCDEPA  PIC X.                                          00004780
           02 MSOCDEPC  PIC X.                                          00004790
           02 MSOCDEPP  PIC X.                                          00004800
           02 MSOCDEPH  PIC X.                                          00004810
           02 MSOCDEPV  PIC X.                                          00004820
           02 MSOCDEPO  PIC X(3).                                       00004830
           02 FILLER    PIC X(2).                                       00004840
           02 MDEPOTA   PIC X.                                          00004850
           02 MDEPOTC   PIC X.                                          00004860
           02 MDEPOTP   PIC X.                                          00004870
           02 MDEPOTH   PIC X.                                          00004880
           02 MDEPOTV   PIC X.                                          00004890
           02 MDEPOTO   PIC X(3).                                       00004900
           02 FILLER    PIC X(2).                                       00004910
           02 MQTEDEPA  PIC X.                                          00004920
           02 MQTEDEPC  PIC X.                                          00004930
           02 MQTEDEPP  PIC X.                                          00004940
           02 MQTEDEPH  PIC X.                                          00004950
           02 MQTEDEPV  PIC X.                                          00004960
           02 MQTEDEPO  PIC X(6).                                       00004970
           02 FILLER    PIC X(2).                                       00004980
           02 MDATCDEA  PIC X.                                          00004990
           02 MDATCDEC  PIC X.                                          00005000
           02 MDATCDEP  PIC X.                                          00005010
           02 MDATCDEH  PIC X.                                          00005020
           02 MDATCDEV  PIC X.                                          00005030
           02 MDATCDEO  PIC X(10).                                      00005040
           02 FILLER    PIC X(2).                                       00005050
           02 MQTECDEA  PIC X.                                          00005060
           02 MQTECDEC  PIC X.                                          00005070
           02 MQTECDEP  PIC X.                                          00005080
           02 MQTECDEH  PIC X.                                          00005090
           02 MQTECDEV  PIC X.                                          00005100
           02 MQTECDEO  PIC X(6).                                       00005110
           02 FILLER    PIC X(2).                                       00005120
           02 MLIBMBA   PIC X.                                          00005130
           02 MLIBMBC   PIC X.                                          00005140
           02 MLIBMBP   PIC X.                                          00005150
           02 MLIBMBH   PIC X.                                          00005160
           02 MLIBMBV   PIC X.                                          00005170
           02 MLIBMBO   PIC X(3).                                       00005180
           02 FILLER    PIC X(2).                                       00005190
           02 MLIBVAR1A      PIC X.                                     00005200
           02 MLIBVAR1C PIC X.                                          00005210
           02 MLIBVAR1P PIC X.                                          00005220
           02 MLIBVAR1H PIC X.                                          00005230
           02 MLIBVAR1V PIC X.                                          00005240
           02 MLIBVAR1O      PIC X(11).                                 00005250
           02 FILLER    PIC X(2).                                       00005260
           02 MOPSPEA   PIC X.                                                  
           02 MOPSPEC   PIC X.                                                  
           02 MOPSPEP   PIC X.                                                  
           02 MOPSPEH   PIC X.                                                  
           02 MOPSPEV   PIC X.                                                  
           02 MOPSPEO   PIC X(19).                                              
           02 FILLER    PIC X(2).                                               
           02 MIPVRA    PIC X.                                          00005270
           02 MIPVRC    PIC X.                                          00005280
           02 MIPVRP    PIC X.                                          00005290
           02 MIPVRH    PIC X.                                          00005300
           02 MIPVRV    PIC X.                                          00005310
           02 MIPVRO    PIC X(8).                                       00005320
           02 FILLER    PIC X(2).                                       00005330
           02 MICIMPA   PIC X.                                          00005340
           02 MICIMPC   PIC X.                                          00005350
           02 MICIMPP   PIC X.                                          00005360
           02 MICIMPH   PIC X.                                          00005370
           02 MICIMPV   PIC X.                                          00005380
           02 MICIMPO   PIC X.                                          00005390
           02 FILLER    PIC X(2).                                       00005400
           02 MIDPVRA   PIC X.                                          00005410
           02 MIDPVRC   PIC X.                                          00005420
           02 MIDPVRP   PIC X.                                          00005430
           02 MIDPVRH   PIC X.                                          00005440
           02 MIDPVRV   PIC X.                                          00005450
           02 MIDPVRO   PIC X(8).                                       00005460
           02 FILLER    PIC X(2).                                       00005470
           02 MICOMMA   PIC X.                                          00005480
           02 MICOMMC   PIC X.                                          00005490
           02 MICOMMP   PIC X.                                          00005500
           02 MICOMMH   PIC X.                                          00005510
           02 MICOMMV   PIC X.                                          00005520
           02 MICOMMO   PIC X(10).                                      00005530
           02 FILLER    PIC X(2).                                       00005540
           02 MITMBA    PIC X.                                          00005550
           02 MITMBC    PIC X.                                          00005560
           02 MITMBP    PIC X.                                          00005570
           02 MITMBH    PIC X.                                          00005580
           02 MITMBV    PIC X.                                          00005590
           02 MITMBO    PIC X(7).                                       00005600
           02 FILLER    PIC X(2).                                       00005610
           02 MIPRRA    PIC X.                                          00005620
           02 MIPRRC    PIC X.                                          00005630
           02 MIPRRP    PIC X.                                          00005640
           02 MIPRRH    PIC X.                                          00005650
           02 MIPRRV    PIC X.                                          00005660
           02 MIPRRO    PIC X(6).                                       00005670
           02 FILLER    PIC X(2).                                       00005680
           02 MIDPRRA   PIC X.                                          00005690
           02 MIDPRRC   PIC X.                                          00005700
           02 MIDPRRP   PIC X.                                          00005710
           02 MIDPRRH   PIC X.                                          00005720
           02 MIDPRRV   PIC X.                                          00005730
           02 MIDPRRO   PIC X(8).                                       00005740
           02 FILLER    PIC X(2).                                       00005750
           02 MICPRRA   PIC X.                                          00005760
           02 MICPRRC   PIC X.                                          00005770
           02 MICPRRP   PIC X.                                          00005780
           02 MICPRRH   PIC X.                                          00005790
           02 MICPRRV   PIC X.                                          00005800
           02 MICPRRO   PIC X(10).                                      00005810
           02 DFHMS5 OCCURS   4 TIMES .                                 00005820
             03 FILLER       PIC X(2).                                  00005830
             03 MACT2A  PIC X.                                          00005840
             03 MACT2C  PIC X.                                          00005850
             03 MACT2P  PIC X.                                          00005860
             03 MACT2H  PIC X.                                          00005870
             03 MACT2V  PIC X.                                          00005880
             03 MACT2O  PIC X.                                          00005890
           02 DFHMS6 OCCURS   4 TIMES .                                 00005900
             03 FILLER       PIC X(2).                                  00005910
             03 MPVRA   PIC X.                                          00005920
             03 MPVRC   PIC X.                                          00005930
             03 MPVRP   PIC X.                                          00005940
             03 MPVRH   PIC X.                                          00005950
             03 MPVRV   PIC X.                                          00005960
             03 MPVRO   PIC X(8).                                       00005970
           02 DFHMS7 OCCURS   4 TIMES .                                 00005980
             03 FILLER       PIC X(2).                                  00005990
             03 MCIMPERA     PIC X.                                     00006000
             03 MCIMPERC     PIC X.                                     00006010
             03 MCIMPERP     PIC X.                                     00006020
             03 MCIMPERH     PIC X.                                     00006030
             03 MCIMPERV     PIC X.                                     00006040
             03 MCIMPERO     PIC X.                                     00006050
           02 DFHMS8 OCCURS   4 TIMES .                                 00006060
             03 FILLER       PIC X(2).                                  00006070
             03 MTMBA   PIC X.                                          00006080
             03 MTMBC   PIC X.                                          00006090
             03 MTMBP   PIC X.                                          00006100
             03 MTMBH   PIC X.                                          00006110
             03 MTMBV   PIC X.                                          00006120
             03 MTMBO   PIC X(7).                                       00006130
           02 DFHMS9 OCCURS   9 TIMES .                                 00006140
             03 FILLER       PIC X(2).                                  00006150
             03 MACT4A  PIC X.                                          00006160
             03 MACT4C  PIC X.                                          00006170
             03 MACT4P  PIC X.                                          00006180
             03 MACT4H  PIC X.                                          00006190
             03 MACT4V  PIC X.                                          00006200
             03 MACT4O  PIC X.                                          00006210
           02 DFHMS10 OCCURS   9 TIMES .                                00006220
             03 FILLER       PIC X(2).                                  00006230
             03 MTYPCOMMA    PIC X.                                     00006240
             03 MTYPCOMMC    PIC X.                                     00006250
             03 MTYPCOMMP    PIC X.                                     00006260
             03 MTYPCOMMH    PIC X.                                     00006270
             03 MTYPCOMMV    PIC X.                                     00006280
             03 MTYPCOMMO    PIC X.                                     00006290
           02 DFHMS11 OCCURS   9 TIMES .                                00006300
             03 FILLER       PIC X(2).                                  00006310
             03 MPRRA   PIC X.                                          00006320
             03 MPRRC   PIC X.                                          00006330
             03 MPRRP   PIC X.                                          00006340
             03 MPRRH   PIC X.                                          00006350
             03 MPRRV   PIC X.                                          00006360
             03 MPRRO   PIC X(6).                                       00006370
           02 DFHMS12 OCCURS   9 TIMES .                                00006380
             03 FILLER       PIC X(2).                                  00006390
             03 MDPRRA  PIC X.                                          00006400
             03 MDPRRC  PIC X.                                          00006410
             03 MDPRRP  PIC X.                                          00006420
             03 MDPRRH  PIC X.                                          00006430
             03 MDPRRV  PIC X.                                          00006440
             03 MDPRRO  PIC X(8).                                       00006450
           02 DFHMS13 OCCURS   4 TIMES .                                00006460
             03 FILLER       PIC X(2).                                  00006470
             03 MWIMPERA     PIC X.                                     00006480
             03 MWIMPERC     PIC X.                                     00006490
             03 MWIMPERP     PIC X.                                     00006500
             03 MWIMPERH     PIC X.                                     00006510
             03 MWIMPERV     PIC X.                                     00006520
             03 MWIMPERO     PIC X.                                     00006530
           02 DFHMS14 OCCURS   4 TIMES .                                00006540
             03 FILLER       PIC X(2).                                  00006550
             03 MDPVRA  PIC X.                                          00006560
             03 MDPVRC  PIC X.                                          00006570
             03 MDPVRP  PIC X.                                          00006580
             03 MDPVRH  PIC X.                                          00006590
             03 MDPVRV  PIC X.                                          00006600
             03 MDPVRO  PIC X(8).                                       00006610
           02 DFHMS15 OCCURS   4 TIMES .                                00006620
             03 FILLER       PIC X(2).                                  00006630
             03 MCOMMA  PIC X.                                          00006640
             03 MCOMMC  PIC X.                                          00006650
             03 MCOMMP  PIC X.                                          00006660
             03 MCOMMH  PIC X.                                          00006670
             03 MCOMMV  PIC X.                                          00006680
             03 MCOMMO  PIC X(10).                                      00006690
           02 DFHMS16 OCCURS   9 TIMES .                                00006700
             03 FILLER       PIC X(2).                                  00006710
             03 MCPRRA  PIC X.                                          00006720
             03 MCPRRC  PIC X.                                          00006730
             03 MCPRRP  PIC X.                                          00006740
             03 MCPRRH  PIC X.                                          00006750
             03 MCPRRV  PIC X.                                          00006760
             03 MCPRRO  PIC X(10).                                      00006770
           02 FILLER    PIC X(2).                                       00006780
           02 MMAJCGAA  PIC X.                                                  
           02 MMAJCGAC  PIC X.                                                  
           02 MMAJCGAP  PIC X.                                                  
           02 MMAJCGAH  PIC X.                                                  
           02 MMAJCGAV  PIC X.                                                  
           02 MMAJCGAO  PIC X.                                                  
           02 FILLER    PIC X(2).                                       00006850
           02 MCROSSA   PIC X.                                                  
           02 MCROSSC   PIC X.                                                  
           02 MCROSSP   PIC X.                                                  
           02 MCROSSH   PIC X.                                                  
           02 MCROSSV   PIC X.                                                  
           02 MCROSSO   PIC X.                                                  
           02 FILLER    PIC X(2).                                       00006920
           02 MB2BOCA   PIC X.                                                  
           02 MB2BOCC   PIC X.                                                  
           02 MB2BOCP   PIC X.                                                  
           02 MB2BOCH   PIC X.                                                  
           02 MB2BOCV   PIC X.                                                  
           02 MB2BOCO   PIC X.                                                  
           02 FILLER    PIC X(2).                                       00006990
           02 MB2BDA    PIC X.                                                  
           02 MB2BDC    PIC X.                                                  
           02 MB2BDP    PIC X.                                                  
           02 MB2BDH    PIC X.                                                  
           02 MB2BDV    PIC X.                                                  
           02 MB2BDO    PIC X(8).                                               
           02 FILLER    PIC X(2).                                       00007060
           02 MB2BLA    PIC X.                                                  
           02 MB2BLC    PIC X.                                                  
           02 MB2BLP    PIC X.                                                  
           02 MB2BLH    PIC X.                                                  
           02 MB2BLV    PIC X.                                                  
           02 MB2BLO    PIC X(10).                                              
           02 FILLER    PIC X(2).                                       00007130
           02 MB2BOCHA  PIC X.                                                  
           02 MB2BOCHC  PIC X.                                                  
           02 MB2BOCHP  PIC X.                                                  
           02 MB2BOCHH  PIC X.                                                  
           02 MB2BOCHV  PIC X.                                                  
           02 MB2BOCHO  PIC X.                                                  
           02 FILLER    PIC X(2).                                       00007680
           02 MB2BDHA   PIC X.                                                  
           02 MB2BDHC   PIC X.                                                  
           02 MB2BDHP   PIC X.                                                  
           02 MB2BDHH   PIC X.                                                  
           02 MB2BDHV   PIC X.                                                  
           02 MB2BDHO   PIC X(8).                                               
           02 FILLER    PIC X(2).                                               
           02 MB2BLHA   PIC X.                                                  
           02 MB2BLHC   PIC X.                                                  
           02 MB2BLHP   PIC X.                                                  
           02 MB2BLHH   PIC X.                                                  
           02 MB2BLHV   PIC X.                                                  
           02 MB2BLHO   PIC X(10).                                              
           02 FILLER    PIC X(2).                                               
           02 MLIBERRA  PIC X.                                          00007690
           02 MLIBERRC  PIC X.                                          00007700
           02 MLIBERRP  PIC X.                                          00007710
           02 MLIBERRH  PIC X.                                          00007720
           02 MLIBERRV  PIC X.                                          00007730
           02 MLIBERRO  PIC X(78).                                      00007740
           02 FILLER    PIC X(2).                                       00007750
           02 MCODTRAA  PIC X.                                          00007760
           02 MCODTRAC  PIC X.                                          00007770
           02 MCODTRAP  PIC X.                                          00007780
           02 MCODTRAH  PIC X.                                          00007790
           02 MCODTRAV  PIC X.                                          00007800
           02 MCODTRAO  PIC X(4).                                       00007810
           02 FILLER    PIC X(2).                                       00007820
           02 MCICSA    PIC X.                                          00007830
           02 MCICSC    PIC X.                                          00007840
           02 MCICSP    PIC X.                                          00007850
           02 MCICSH    PIC X.                                          00007860
           02 MCICSV    PIC X.                                          00007870
           02 MCICSO    PIC X(5).                                       00007880
           02 FILLER    PIC X(2).                                       00007890
           02 MNETNAMA  PIC X.                                          00007900
           02 MNETNAMC  PIC X.                                          00007910
           02 MNETNAMP  PIC X.                                          00007920
           02 MNETNAMH  PIC X.                                          00007930
           02 MNETNAMV  PIC X.                                          00007940
           02 MNETNAMO  PIC X(8).                                       00007950
           02 FILLER    PIC X(2).                                       00007960
           02 MSCREENA  PIC X.                                          00007970
           02 MSCREENC  PIC X.                                          00007980
           02 MSCREENP  PIC X.                                          00007990
           02 MSCREENH  PIC X.                                          00008000
           02 MSCREENV  PIC X.                                          00008010
           02 MSCREENO  PIC X(4).                                       00008020
                                                                                
