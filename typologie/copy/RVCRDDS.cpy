      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CRDDS CONTROLES DU DOSSIER             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCRDDS.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCRDDS.                                                             
      *}                                                                        
           05  CRDDS-CTABLEG2    PIC X(15).                                     
           05  CRDDS-CTABLEG2-REDEF REDEFINES CRDDS-CTABLEG2.                   
               10  CRDDS-CDOSSIER        PIC X(05).                             
               10  CRDDS-NSEQ            PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  CRDDS-NSEQ-N         REDEFINES CRDDS-NSEQ                    
                                         PIC 9(02).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  CRDDS-WTABLEG     PIC X(80).                                     
           05  CRDDS-WTABLEG-REDEF  REDEFINES CRDDS-WTABLEG.                    
               10  CRDDS-WACTIF          PIC X(01).                             
               10  CRDDS-CCTRL           PIC X(05).                             
               10  CRDDS-CLEINT          PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  CRDDS-CLEINT-N       REDEFINES CRDDS-CLEINT                  
                                         PIC 9(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCRDDS-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCRDDS-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CRDDS-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CRDDS-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CRDDS-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CRDDS-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
