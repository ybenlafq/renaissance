      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA71   EGA71                                              00000020
      ***************************************************************** 00000030
       01   EGA71I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * FONCTION                                                        00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCTL   COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MFONCTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MFONCTF   PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MFONCTI   PIC X(3).                                       00000200
      * LIBELLE FONCTION                                                00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFONCTL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLFONCTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLFONCTF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLFONCTI  PIC X(13).                                      00000250
      * NOM DE LA TABLE                                                 00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTABLEL  COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MCTABLEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTABLEF  PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MCTABLEI  PIC X(6).                                       00000300
      * NOM DE LA SOUS TABLE                                            00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSTABLEL      COMP PIC S9(4).                            00000320
      *--                                                                       
           02 MCSTABLEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCSTABLEF      PIC X.                                     00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MCSTABLEI      PIC X(5).                                  00000350
      * DATE DE DERNIERE MODIF                                          00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMAJL    COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MDMAJL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDMAJF    PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MDMAJI    PIC X(8).                                       00000400
      * LIBELLE DESCRIPTIF                                              00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTABLEL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLTABLEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTABLEF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLTABLEI  PIC X(30).                                      00000450
      * SERVICE RESPONSABLE                                             00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRESPL   COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MCRESPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCRESPF   PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MCRESPI   PIC X(3).                                       00000500
      * DATE DE MISE EN UTILISATION                                     00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEBUTL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MDDEBUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDDEBUTF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MDDEBUTI  PIC X(8).                                       00000550
      * DATE DE FIN D UTILISATION                                       00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFINL    COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MDFINL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDFINF    PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MDFINI    PIC X(8).                                       00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOMVUEL      COMP PIC S9(4).                            00000610
      *--                                                                       
           02 MLNOMVUEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLNOMVUEF      PIC X.                                     00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MLNOMVUEI      PIC X(13).                                 00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOMVUEL  COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MNOMVUEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNOMVUEF  PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MNOMVUEI  PIC X(8).                                       00000680
      * ZONE CMD AIDA                                                   00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MZONCMDI  PIC X(15).                                      00000730
      * MESSAGE ERREUR                                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MLIBERRI  PIC X(58).                                      00000780
      * CODE TRANSACTION                                                00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MCODTRAI  PIC X(4).                                       00000830
      * CICS DE TRAVAIL                                                 00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000850
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000860
           02 FILLER    PIC X(4).                                       00000870
           02 MCICSI    PIC X(5).                                       00000880
      * NETNAME                                                         00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MNETNAMI  PIC X(8).                                       00000930
      * CODE TERMINAL                                                   00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MSCREENI  PIC X(5).                                       00000980
      ***************************************************************** 00000990
      * SDF: EGA71   EGA71                                              00001000
      ***************************************************************** 00001010
       01   EGA71O REDEFINES EGA71I.                                    00001020
           02 FILLER    PIC X(12).                                      00001030
      * DATE DU JOUR                                                    00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MDATJOUA  PIC X.                                          00001060
           02 MDATJOUC  PIC X.                                          00001070
           02 MDATJOUP  PIC X.                                          00001080
           02 MDATJOUH  PIC X.                                          00001090
           02 MDATJOUV  PIC X.                                          00001100
           02 MDATJOUO  PIC X(10).                                      00001110
      * HEURE                                                           00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MTIMJOUA  PIC X.                                          00001140
           02 MTIMJOUC  PIC X.                                          00001150
           02 MTIMJOUP  PIC X.                                          00001160
           02 MTIMJOUH  PIC X.                                          00001170
           02 MTIMJOUV  PIC X.                                          00001180
           02 MTIMJOUO  PIC X(5).                                       00001190
      * FONCTION                                                        00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MFONCTA   PIC X.                                          00001220
           02 MFONCTC   PIC X.                                          00001230
           02 MFONCTP   PIC X.                                          00001240
           02 MFONCTH   PIC X.                                          00001250
           02 MFONCTV   PIC X.                                          00001260
           02 MFONCTO   PIC X(3).                                       00001270
      * LIBELLE FONCTION                                                00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MLFONCTA  PIC X.                                          00001300
           02 MLFONCTC  PIC X.                                          00001310
           02 MLFONCTP  PIC X.                                          00001320
           02 MLFONCTH  PIC X.                                          00001330
           02 MLFONCTV  PIC X.                                          00001340
           02 MLFONCTO  PIC X(13).                                      00001350
      * NOM DE LA TABLE                                                 00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MCTABLEA  PIC X.                                          00001380
           02 MCTABLEC  PIC X.                                          00001390
           02 MCTABLEP  PIC X.                                          00001400
           02 MCTABLEH  PIC X.                                          00001410
           02 MCTABLEV  PIC X.                                          00001420
           02 MCTABLEO  PIC X(6).                                       00001430
      * NOM DE LA SOUS TABLE                                            00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MCSTABLEA      PIC X.                                     00001460
           02 MCSTABLEC PIC X.                                          00001470
           02 MCSTABLEP PIC X.                                          00001480
           02 MCSTABLEH PIC X.                                          00001490
           02 MCSTABLEV PIC X.                                          00001500
           02 MCSTABLEO      PIC X(5).                                  00001510
      * DATE DE DERNIERE MODIF                                          00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MDMAJA    PIC X.                                          00001540
           02 MDMAJC    PIC X.                                          00001550
           02 MDMAJP    PIC X.                                          00001560
           02 MDMAJH    PIC X.                                          00001570
           02 MDMAJV    PIC X.                                          00001580
           02 MDMAJO    PIC X(8).                                       00001590
      * LIBELLE DESCRIPTIF                                              00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MLTABLEA  PIC X.                                          00001620
           02 MLTABLEC  PIC X.                                          00001630
           02 MLTABLEP  PIC X.                                          00001640
           02 MLTABLEH  PIC X.                                          00001650
           02 MLTABLEV  PIC X.                                          00001660
           02 MLTABLEO  PIC X(30).                                      00001670
      * SERVICE RESPONSABLE                                             00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MCRESPA   PIC X.                                          00001700
           02 MCRESPC   PIC X.                                          00001710
           02 MCRESPP   PIC X.                                          00001720
           02 MCRESPH   PIC X.                                          00001730
           02 MCRESPV   PIC X.                                          00001740
           02 MCRESPO   PIC X(3).                                       00001750
      * DATE DE MISE EN UTILISATION                                     00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MDDEBUTA  PIC X.                                          00001780
           02 MDDEBUTC  PIC X.                                          00001790
           02 MDDEBUTP  PIC X.                                          00001800
           02 MDDEBUTH  PIC X.                                          00001810
           02 MDDEBUTV  PIC X.                                          00001820
           02 MDDEBUTO  PIC X(8).                                       00001830
      * DATE DE FIN D UTILISATION                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MDFINA    PIC X.                                          00001860
           02 MDFINC    PIC X.                                          00001870
           02 MDFINP    PIC X.                                          00001880
           02 MDFINH    PIC X.                                          00001890
           02 MDFINV    PIC X.                                          00001900
           02 MDFINO    PIC X(8).                                       00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MLNOMVUEA      PIC X.                                     00001930
           02 MLNOMVUEC PIC X.                                          00001940
           02 MLNOMVUEP PIC X.                                          00001950
           02 MLNOMVUEH PIC X.                                          00001960
           02 MLNOMVUEV PIC X.                                          00001970
           02 MLNOMVUEO      PIC X(13).                                 00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MNOMVUEA  PIC X.                                          00002000
           02 MNOMVUEC  PIC X.                                          00002010
           02 MNOMVUEP  PIC X.                                          00002020
           02 MNOMVUEH  PIC X.                                          00002030
           02 MNOMVUEV  PIC X.                                          00002040
           02 MNOMVUEO  PIC X(8).                                       00002050
      * ZONE CMD AIDA                                                   00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MZONCMDA  PIC X.                                          00002080
           02 MZONCMDC  PIC X.                                          00002090
           02 MZONCMDP  PIC X.                                          00002100
           02 MZONCMDH  PIC X.                                          00002110
           02 MZONCMDV  PIC X.                                          00002120
           02 MZONCMDO  PIC X(15).                                      00002130
      * MESSAGE ERREUR                                                  00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MLIBERRA  PIC X.                                          00002160
           02 MLIBERRC  PIC X.                                          00002170
           02 MLIBERRP  PIC X.                                          00002180
           02 MLIBERRH  PIC X.                                          00002190
           02 MLIBERRV  PIC X.                                          00002200
           02 MLIBERRO  PIC X(58).                                      00002210
      * CODE TRANSACTION                                                00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MCODTRAA  PIC X.                                          00002240
           02 MCODTRAC  PIC X.                                          00002250
           02 MCODTRAP  PIC X.                                          00002260
           02 MCODTRAH  PIC X.                                          00002270
           02 MCODTRAV  PIC X.                                          00002280
           02 MCODTRAO  PIC X(4).                                       00002290
      * CICS DE TRAVAIL                                                 00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MCICSA    PIC X.                                          00002320
           02 MCICSC    PIC X.                                          00002330
           02 MCICSP    PIC X.                                          00002340
           02 MCICSH    PIC X.                                          00002350
           02 MCICSV    PIC X.                                          00002360
           02 MCICSO    PIC X(5).                                       00002370
      * NETNAME                                                         00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MNETNAMA  PIC X.                                          00002400
           02 MNETNAMC  PIC X.                                          00002410
           02 MNETNAMP  PIC X.                                          00002420
           02 MNETNAMH  PIC X.                                          00002430
           02 MNETNAMV  PIC X.                                          00002440
           02 MNETNAMO  PIC X(8).                                       00002450
      * CODE TERMINAL                                                   00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MSCREENA  PIC X.                                          00002480
           02 MSCREENC  PIC X.                                          00002490
           02 MSCREENP  PIC X.                                          00002500
           02 MSCREENH  PIC X.                                          00002510
           02 MSCREENV  PIC X.                                          00002520
           02 MSCREENO  PIC X(5).                                       00002530
                                                                                
