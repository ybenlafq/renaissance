      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE BSGES MODE DE GESTION RTBS31           *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBSGES.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBSGES.                                                             
      *}                                                                        
           05  BSGES-CTABLEG2    PIC X(15).                                     
           05  BSGES-CTABLEG2-REDEF REDEFINES BSGES-CTABLEG2.                   
               10  BSGES-CGEST           PIC X(07).                             
           05  BSGES-WTABLEG     PIC X(80).                                     
           05  BSGES-WTABLEG-REDEF  REDEFINES BSGES-WTABLEG.                    
               10  BSGES-LIBELLE         PIC X(20).                             
               10  BSGES-LGCGEST         PIC X(05).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBSGES-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBSGES-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BSGES-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  BSGES-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BSGES-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  BSGES-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
