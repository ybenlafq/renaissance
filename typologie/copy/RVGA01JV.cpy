      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE BQMAG CORRESPONDANCE MAGASIN/BANQUE    *        
      *----------------------------------------------------------------*        
       01  RVGA01JV.                                                            
           05  BQMAG-CTABLEG2    PIC X(15).                                     
           05  BQMAG-CTABLEG2-REDEF REDEFINES BQMAG-CTABLEG2.                   
               10  BQMAG-NOMAG           PIC X(03).                             
               10  BQMAG-CMOPAI          PIC X(05).                             
           05  BQMAG-WTABLEG     PIC X(80).                                     
           05  BQMAG-WTABLEG-REDEF  REDEFINES BQMAG-WTABLEG.                    
               10  BQMAG-CBANQ           PIC X(03).                             
               10  BQMAG-LIBANQ          PIC X(25).                             
               10  BQMAG-CTYPOPER        PIC X(05).                             
               10  BQMAG-LIBEL           PIC X(10).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01JV-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BQMAG-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  BQMAG-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BQMAG-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  BQMAG-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
