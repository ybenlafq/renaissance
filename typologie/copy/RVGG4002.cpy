      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      **********************************************************                
      *   COPY DE LA TABLE RVGG4002                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGG4002                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGG4002.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGG4002.                                                            
      *}                                                                        
           02  GG40-NCODIC                                                      
               PIC X(0007).                                                     
           02  GG40-NCONC                                                       
               PIC X(0004).                                                     
           02  GG40-PEXPCOMMART                                                 
               PIC S9(5)V9(0002) COMP-3.                                        
           02  GG40-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GG40-NLIEU                                                       
               PIC X(0003).                                                     
           02  GG40-DEFFET                                                      
               PIC X(0008).                                                     
           02  GG40-DFINEFFET                                                   
               PIC X(0008).                                                     
           02  GG40-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GG40-WFINEFFET                                                   
               PIC X(0001).                                                     
           02  GG40-CORIG                                                       
               PIC X(0001).                                                     
           02  GG40-PCOMMADP                                                    
               PIC S9(5)V9(0002) COMP-3.                                        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGG4001                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGG4002-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGG4002-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG40-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG40-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG40-NCONC-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG40-NCONC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG40-PEXPCOMMART-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG40-PEXPCOMMART-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG40-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG40-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG40-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG40-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG40-DEFFET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG40-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG40-DFINEFFET-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG40-DFINEFFET-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG40-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG40-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG40-WFINEFFET-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG40-WFINEFFET-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG40-CORIG-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG40-CORIG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG40-PCOMMADP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG40-PCOMMADP-F                                                  
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
