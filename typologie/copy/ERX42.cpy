      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERX42   ERX42                                              00000020
      ***************************************************************** 00000030
       01   ERX42I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRELEVL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNRELEVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNRELEVF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNRELEVI  PIC X(7).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCONCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCONCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCONCI   PIC X(4).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCONCL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLCONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLCONCF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLCONCI   PIC X(15).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNMAGI    PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCVENDL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCVENDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCVENDF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCVENDI   PIC X(6).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVENDL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLVENDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLVENDF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLVENDI   PIC X(15).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDERREFL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDERREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDERREFF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDERREFI  PIC X(20).                                      00000450
           02 MCFAMD OCCURS   13 TIMES .                                00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MCFAMI  PIC X(5).                                       00000500
           02 MCMARQD OCCURS   13 TIMES .                               00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000520
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000530
             03 FILLER  PIC X(4).                                       00000540
             03 MCMARQI      PIC X(5).                                  00000550
           02 MREFFOUD OCCURS   13 TIMES .                              00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREFFOUL     COMP PIC S9(4).                            00000570
      *--                                                                       
             03 MREFFOUL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MREFFOUF     PIC X.                                     00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MREFFOUI     PIC X(21).                                 00000600
           02 MPRELEVD OCCURS   13 TIMES .                              00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRELEVL     COMP PIC S9(4).                            00000620
      *--                                                                       
             03 MPRELEVL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPRELEVF     PIC X.                                     00000630
             03 FILLER  PIC X(4).                                       00000640
             03 MPRELEVI     PIC X(9).                                  00000650
           02 MQCOLD OCCURS   13 TIMES .                                00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQCOLL  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MQCOLL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQCOLF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MQCOLI  PIC X(3).                                       00000700
           02 MLCOMMD OCCURS   13 TIMES .                               00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOMML      COMP PIC S9(4).                            00000720
      *--                                                                       
             03 MLCOMML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLCOMMF      PIC X.                                     00000730
             03 FILLER  PIC X(4).                                       00000740
             03 MLCOMMI      PIC X(20).                                 00000750
           02 MNCODICD OCCURS   13 TIMES .                              00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000770
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MNCODICI     PIC X(7).                                  00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000810
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000820
           02 FILLER    PIC X(4).                                       00000830
           02 MLIBERRI  PIC X(77).                                      00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000850
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000860
           02 FILLER    PIC X(4).                                       00000870
           02 MCODTRAI  PIC X(4).                                       00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000890
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000900
           02 FILLER    PIC X(4).                                       00000910
           02 MCICSI    PIC X(5).                                       00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000930
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000940
           02 FILLER    PIC X(4).                                       00000950
           02 MNETNAMI  PIC X(8).                                       00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MSCREENI  PIC X(4).                                       00001000
      ***************************************************************** 00001010
      * SDF: ERX42   ERX42                                              00001020
      ***************************************************************** 00001030
       01   ERX42O REDEFINES ERX42I.                                    00001040
           02 FILLER    PIC X(12).                                      00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MDATJOUA  PIC X.                                          00001070
           02 MDATJOUC  PIC X.                                          00001080
           02 MDATJOUP  PIC X.                                          00001090
           02 MDATJOUH  PIC X.                                          00001100
           02 MDATJOUV  PIC X.                                          00001110
           02 MDATJOUO  PIC X(10).                                      00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MTIMJOUA  PIC X.                                          00001140
           02 MTIMJOUC  PIC X.                                          00001150
           02 MTIMJOUP  PIC X.                                          00001160
           02 MTIMJOUH  PIC X.                                          00001170
           02 MTIMJOUV  PIC X.                                          00001180
           02 MTIMJOUO  PIC X(5).                                       00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MNRELEVA  PIC X.                                          00001210
           02 MNRELEVC  PIC X.                                          00001220
           02 MNRELEVP  PIC X.                                          00001230
           02 MNRELEVH  PIC X.                                          00001240
           02 MNRELEVV  PIC X.                                          00001250
           02 MNRELEVO  PIC X(7).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MNSOCA    PIC X.                                          00001280
           02 MNSOCC    PIC X.                                          00001290
           02 MNSOCP    PIC X.                                          00001300
           02 MNSOCH    PIC X.                                          00001310
           02 MNSOCV    PIC X.                                          00001320
           02 MNSOCO    PIC X(3).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MNCONCA   PIC X.                                          00001350
           02 MNCONCC   PIC X.                                          00001360
           02 MNCONCP   PIC X.                                          00001370
           02 MNCONCH   PIC X.                                          00001380
           02 MNCONCV   PIC X.                                          00001390
           02 MNCONCO   PIC X(4).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MLCONCA   PIC X.                                          00001420
           02 MLCONCC   PIC X.                                          00001430
           02 MLCONCP   PIC X.                                          00001440
           02 MLCONCH   PIC X.                                          00001450
           02 MLCONCV   PIC X.                                          00001460
           02 MLCONCO   PIC X(15).                                      00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MNMAGA    PIC X.                                          00001490
           02 MNMAGC    PIC X.                                          00001500
           02 MNMAGP    PIC X.                                          00001510
           02 MNMAGH    PIC X.                                          00001520
           02 MNMAGV    PIC X.                                          00001530
           02 MNMAGO    PIC X(3).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MCVENDA   PIC X.                                          00001560
           02 MCVENDC   PIC X.                                          00001570
           02 MCVENDP   PIC X.                                          00001580
           02 MCVENDH   PIC X.                                          00001590
           02 MCVENDV   PIC X.                                          00001600
           02 MCVENDO   PIC X(6).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MLVENDA   PIC X.                                          00001630
           02 MLVENDC   PIC X.                                          00001640
           02 MLVENDP   PIC X.                                          00001650
           02 MLVENDH   PIC X.                                          00001660
           02 MLVENDV   PIC X.                                          00001670
           02 MLVENDO   PIC X(15).                                      00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MDERREFA  PIC X.                                          00001700
           02 MDERREFC  PIC X.                                          00001710
           02 MDERREFP  PIC X.                                          00001720
           02 MDERREFH  PIC X.                                          00001730
           02 MDERREFV  PIC X.                                          00001740
           02 MDERREFO  PIC X(20).                                      00001750
           02 DFHMS1 OCCURS   13 TIMES .                                00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MCFAMA  PIC X.                                          00001780
             03 MCFAMC  PIC X.                                          00001790
             03 MCFAMP  PIC X.                                          00001800
             03 MCFAMH  PIC X.                                          00001810
             03 MCFAMV  PIC X.                                          00001820
             03 MCFAMO  PIC X(5).                                       00001830
           02 DFHMS2 OCCURS   13 TIMES .                                00001840
             03 FILLER       PIC X(2).                                  00001850
             03 MCMARQA      PIC X.                                     00001860
             03 MCMARQC PIC X.                                          00001870
             03 MCMARQP PIC X.                                          00001880
             03 MCMARQH PIC X.                                          00001890
             03 MCMARQV PIC X.                                          00001900
             03 MCMARQO      PIC X(5).                                  00001910
           02 DFHMS3 OCCURS   13 TIMES .                                00001920
             03 FILLER       PIC X(2).                                  00001930
             03 MREFFOUA     PIC X.                                     00001940
             03 MREFFOUC     PIC X.                                     00001950
             03 MREFFOUP     PIC X.                                     00001960
             03 MREFFOUH     PIC X.                                     00001970
             03 MREFFOUV     PIC X.                                     00001980
             03 MREFFOUO     PIC X(21).                                 00001990
           02 DFHMS4 OCCURS   13 TIMES .                                00002000
             03 FILLER       PIC X(2).                                  00002010
             03 MPRELEVA     PIC X.                                     00002020
             03 MPRELEVC     PIC X.                                     00002030
             03 MPRELEVP     PIC X.                                     00002040
             03 MPRELEVH     PIC X.                                     00002050
             03 MPRELEVV     PIC X.                                     00002060
             03 MPRELEVO     PIC X(9).                                  00002070
           02 DFHMS5 OCCURS   13 TIMES .                                00002080
             03 FILLER       PIC X(2).                                  00002090
             03 MQCOLA  PIC X.                                          00002100
             03 MQCOLC  PIC X.                                          00002110
             03 MQCOLP  PIC X.                                          00002120
             03 MQCOLH  PIC X.                                          00002130
             03 MQCOLV  PIC X.                                          00002140
             03 MQCOLO  PIC X(3).                                       00002150
           02 DFHMS6 OCCURS   13 TIMES .                                00002160
             03 FILLER       PIC X(2).                                  00002170
             03 MLCOMMA      PIC X.                                     00002180
             03 MLCOMMC PIC X.                                          00002190
             03 MLCOMMP PIC X.                                          00002200
             03 MLCOMMH PIC X.                                          00002210
             03 MLCOMMV PIC X.                                          00002220
             03 MLCOMMO      PIC X(20).                                 00002230
           02 DFHMS7 OCCURS   13 TIMES .                                00002240
             03 FILLER       PIC X(2).                                  00002250
             03 MNCODICA     PIC X.                                     00002260
             03 MNCODICC     PIC X.                                     00002270
             03 MNCODICP     PIC X.                                     00002280
             03 MNCODICH     PIC X.                                     00002290
             03 MNCODICV     PIC X.                                     00002300
             03 MNCODICO     PIC X(7).                                  00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MLIBERRA  PIC X.                                          00002330
           02 MLIBERRC  PIC X.                                          00002340
           02 MLIBERRP  PIC X.                                          00002350
           02 MLIBERRH  PIC X.                                          00002360
           02 MLIBERRV  PIC X.                                          00002370
           02 MLIBERRO  PIC X(77).                                      00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MCODTRAA  PIC X.                                          00002400
           02 MCODTRAC  PIC X.                                          00002410
           02 MCODTRAP  PIC X.                                          00002420
           02 MCODTRAH  PIC X.                                          00002430
           02 MCODTRAV  PIC X.                                          00002440
           02 MCODTRAO  PIC X(4).                                       00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MCICSA    PIC X.                                          00002470
           02 MCICSC    PIC X.                                          00002480
           02 MCICSP    PIC X.                                          00002490
           02 MCICSH    PIC X.                                          00002500
           02 MCICSV    PIC X.                                          00002510
           02 MCICSO    PIC X(5).                                       00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MNETNAMA  PIC X.                                          00002540
           02 MNETNAMC  PIC X.                                          00002550
           02 MNETNAMP  PIC X.                                          00002560
           02 MNETNAMH  PIC X.                                          00002570
           02 MNETNAMV  PIC X.                                          00002580
           02 MNETNAMO  PIC X(8).                                       00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MSCREENA  PIC X.                                          00002610
           02 MSCREENC  PIC X.                                          00002620
           02 MSCREENP  PIC X.                                          00002630
           02 MSCREENH  PIC X.                                          00002640
           02 MSCREENV  PIC X.                                          00002650
           02 MSCREENO  PIC X(4).                                       00002660
                                                                                
