      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************            
      * TS GA50 DEPOT LIES AUX SOCIETES                                         
       01 TS-DEP-SOC.                                                           
           05 TS-DEP-SOC-SOCIETE.                                               
               10 TS-DEP-SOC-NSOC PIC X(3).                                     
               10 TS-DEP-SOC-NLIEU PIC X(3).                                    
           05 TS-DEP-SOC-DEPOT.                                                 
               10 TS-DEP-SOC-NDEPSOC PIC X(3).                                  
               10 TS-DEP-SOC-NDEPLIEU PIC X(3).                                 
           05 TS-DEP-SOC-NPRIORITE PIC 9(3).                                    
           05 TS-DEP-SOC-INDEX PIC S9(3) COMP-3.                                
                                                                                
