      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE ZPFIL PARAM. FILIALES / ZONE DE PRIX   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01ZP.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01ZP.                                                            
      *}                                                                        
           05  ZPFIL-CTABLEG2    PIC X(15).                                     
           05  ZPFIL-CTABLEG2-REDEF REDEFINES ZPFIL-CTABLEG2.                   
               10  ZPFIL-SOCZP           PIC X(05).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  ZPFIL-SOCZP-N        REDEFINES ZPFIL-SOCZP                   
                                         PIC 9(05).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  ZPFIL-WTABLEG     PIC X(80).                                     
           05  ZPFIL-WTABLEG-REDEF  REDEFINES ZPFIL-WTABLEG.                    
               10  ZPFIL-CPARAM1         PIC X(10).                             
               10  ZPFIL-CPARAM2         PIC X(10).                             
               10  ZPFIL-CPARAM3         PIC X(10).                             
               10  ZPFIL-CPARAM4         PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01ZP-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01ZP-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ZPFIL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  ZPFIL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ZPFIL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  ZPFIL-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
