      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA37   EGA37                                              00000020
      ***************************************************************** 00000030
       01   EGA37I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCFAMI    PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCECOL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCECOL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCECOF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCECOI    PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCECOL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLCECOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLCECOF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLCECOI   PIC X(10).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWACHVENL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MWACHVENL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWACHVENF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MWACHVENI      PIC X.                                     00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFFETL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDEFFETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEFFETF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDEFFETI  PIC X(8).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPAYSL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCPAYSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCPAYSF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCPAYSI   PIC X(2).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMONTANTL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MMONTANTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MMONTANTF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MMONTANTI      PIC X(8).                                  00000490
           02 MTABLEI OCCURS   14 TIMES .                               00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCACTIONL    COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MCACTIONL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCACTIONF    PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCACTIONI    PIC X.                                     00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTCFAML      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MTCFAML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MTCFAMF      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MTCFAMI      PIC X(5).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTCECOL      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MTCECOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MTCECOF      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MTCECOI      PIC X(3).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTLCECOL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MTLCECOL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTLCECOF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MTLCECOI     PIC X(10).                                 00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTWACVEL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MTWACVEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTWACVEF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MTWACVEI     PIC X.                                     00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTDEFFL      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MTDEFFL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MTDEFFF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MTDEFFI      PIC X(8).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTCPAYSL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MTCPAYSL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTCPAYSF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MTCPAYSI     PIC X(2).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTMONTANTL   COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MTMONTANTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MTMONTANTF   PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MTMONTANTI   PIC X(8).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MLIBERRI  PIC X(78).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCODTRAI  PIC X(4).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCICSI    PIC X(5).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MNETNAMI  PIC X(8).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MSCREENI  PIC X(4).                                       00001020
      ***************************************************************** 00001030
      * SDF: EGA37   EGA37                                              00001040
      ***************************************************************** 00001050
       01   EGA37O REDEFINES EGA37I.                                    00001060
           02 FILLER    PIC X(12).                                      00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MDATJOUA  PIC X.                                          00001090
           02 MDATJOUC  PIC X.                                          00001100
           02 MDATJOUP  PIC X.                                          00001110
           02 MDATJOUH  PIC X.                                          00001120
           02 MDATJOUV  PIC X.                                          00001130
           02 MDATJOUO  PIC X(10).                                      00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MTIMJOUA  PIC X.                                          00001160
           02 MTIMJOUC  PIC X.                                          00001170
           02 MTIMJOUP  PIC X.                                          00001180
           02 MTIMJOUH  PIC X.                                          00001190
           02 MTIMJOUV  PIC X.                                          00001200
           02 MTIMJOUO  PIC X(5).                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MPAGEA    PIC X.                                          00001230
           02 MPAGEC    PIC X.                                          00001240
           02 MPAGEP    PIC X.                                          00001250
           02 MPAGEH    PIC X.                                          00001260
           02 MPAGEV    PIC X.                                          00001270
           02 MPAGEO    PIC X(3).                                       00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MPAGEMAXA      PIC X.                                     00001300
           02 MPAGEMAXC PIC X.                                          00001310
           02 MPAGEMAXP PIC X.                                          00001320
           02 MPAGEMAXH PIC X.                                          00001330
           02 MPAGEMAXV PIC X.                                          00001340
           02 MPAGEMAXO      PIC X(3).                                  00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MCFAMA    PIC X.                                          00001370
           02 MCFAMC    PIC X.                                          00001380
           02 MCFAMP    PIC X.                                          00001390
           02 MCFAMH    PIC X.                                          00001400
           02 MCFAMV    PIC X.                                          00001410
           02 MCFAMO    PIC X(5).                                       00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MCECOA    PIC X.                                          00001440
           02 MCECOC    PIC X.                                          00001450
           02 MCECOP    PIC X.                                          00001460
           02 MCECOH    PIC X.                                          00001470
           02 MCECOV    PIC X.                                          00001480
           02 MCECOO    PIC X(3).                                       00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MLCECOA   PIC X.                                          00001510
           02 MLCECOC   PIC X.                                          00001520
           02 MLCECOP   PIC X.                                          00001530
           02 MLCECOH   PIC X.                                          00001540
           02 MLCECOV   PIC X.                                          00001550
           02 MLCECOO   PIC X(10).                                      00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MWACHVENA      PIC X.                                     00001580
           02 MWACHVENC PIC X.                                          00001590
           02 MWACHVENP PIC X.                                          00001600
           02 MWACHVENH PIC X.                                          00001610
           02 MWACHVENV PIC X.                                          00001620
           02 MWACHVENO      PIC X.                                     00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MDEFFETA  PIC X.                                          00001650
           02 MDEFFETC  PIC X.                                          00001660
           02 MDEFFETP  PIC X.                                          00001670
           02 MDEFFETH  PIC X.                                          00001680
           02 MDEFFETV  PIC X.                                          00001690
           02 MDEFFETO  PIC X(8).                                       00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MCPAYSA   PIC X.                                          00001720
           02 MCPAYSC   PIC X.                                          00001730
           02 MCPAYSP   PIC X.                                          00001740
           02 MCPAYSH   PIC X.                                          00001750
           02 MCPAYSV   PIC X.                                          00001760
           02 MCPAYSO   PIC X(2).                                       00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MMONTANTA      PIC X.                                     00001790
           02 MMONTANTC PIC X.                                          00001800
           02 MMONTANTP PIC X.                                          00001810
           02 MMONTANTH PIC X.                                          00001820
           02 MMONTANTV PIC X.                                          00001830
           02 MMONTANTO      PIC X(8).                                  00001840
           02 MTABLEO OCCURS   14 TIMES .                               00001850
             03 FILLER       PIC X(2).                                  00001860
             03 MCACTIONA    PIC X.                                     00001870
             03 MCACTIONC    PIC X.                                     00001880
             03 MCACTIONP    PIC X.                                     00001890
             03 MCACTIONH    PIC X.                                     00001900
             03 MCACTIONV    PIC X.                                     00001910
             03 MCACTIONO    PIC X.                                     00001920
             03 FILLER       PIC X(2).                                  00001930
             03 MTCFAMA      PIC X.                                     00001940
             03 MTCFAMC PIC X.                                          00001950
             03 MTCFAMP PIC X.                                          00001960
             03 MTCFAMH PIC X.                                          00001970
             03 MTCFAMV PIC X.                                          00001980
             03 MTCFAMO      PIC X(5).                                  00001990
             03 FILLER       PIC X(2).                                  00002000
             03 MTCECOA      PIC X.                                     00002010
             03 MTCECOC PIC X.                                          00002020
             03 MTCECOP PIC X.                                          00002030
             03 MTCECOH PIC X.                                          00002040
             03 MTCECOV PIC X.                                          00002050
             03 MTCECOO      PIC X(3).                                  00002060
             03 FILLER       PIC X(2).                                  00002070
             03 MTLCECOA     PIC X.                                     00002080
             03 MTLCECOC     PIC X.                                     00002090
             03 MTLCECOP     PIC X.                                     00002100
             03 MTLCECOH     PIC X.                                     00002110
             03 MTLCECOV     PIC X.                                     00002120
             03 MTLCECOO     PIC X(10).                                 00002130
             03 FILLER       PIC X(2).                                  00002140
             03 MTWACVEA     PIC X.                                     00002150
             03 MTWACVEC     PIC X.                                     00002160
             03 MTWACVEP     PIC X.                                     00002170
             03 MTWACVEH     PIC X.                                     00002180
             03 MTWACVEV     PIC X.                                     00002190
             03 MTWACVEO     PIC X.                                     00002200
             03 FILLER       PIC X(2).                                  00002210
             03 MTDEFFA      PIC X.                                     00002220
             03 MTDEFFC PIC X.                                          00002230
             03 MTDEFFP PIC X.                                          00002240
             03 MTDEFFH PIC X.                                          00002250
             03 MTDEFFV PIC X.                                          00002260
             03 MTDEFFO      PIC X(8).                                  00002270
             03 FILLER       PIC X(2).                                  00002280
             03 MTCPAYSA     PIC X.                                     00002290
             03 MTCPAYSC     PIC X.                                     00002300
             03 MTCPAYSP     PIC X.                                     00002310
             03 MTCPAYSH     PIC X.                                     00002320
             03 MTCPAYSV     PIC X.                                     00002330
             03 MTCPAYSO     PIC X(2).                                  00002340
             03 FILLER       PIC X(2).                                  00002350
             03 MTMONTANTA   PIC X.                                     00002360
             03 MTMONTANTC   PIC X.                                     00002370
             03 MTMONTANTP   PIC X.                                     00002380
             03 MTMONTANTH   PIC X.                                     00002390
             03 MTMONTANTV   PIC X.                                     00002400
             03 MTMONTANTO   PIC X(8).                                  00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MLIBERRA  PIC X.                                          00002430
           02 MLIBERRC  PIC X.                                          00002440
           02 MLIBERRP  PIC X.                                          00002450
           02 MLIBERRH  PIC X.                                          00002460
           02 MLIBERRV  PIC X.                                          00002470
           02 MLIBERRO  PIC X(78).                                      00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MCODTRAA  PIC X.                                          00002500
           02 MCODTRAC  PIC X.                                          00002510
           02 MCODTRAP  PIC X.                                          00002520
           02 MCODTRAH  PIC X.                                          00002530
           02 MCODTRAV  PIC X.                                          00002540
           02 MCODTRAO  PIC X(4).                                       00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MCICSA    PIC X.                                          00002570
           02 MCICSC    PIC X.                                          00002580
           02 MCICSP    PIC X.                                          00002590
           02 MCICSH    PIC X.                                          00002600
           02 MCICSV    PIC X.                                          00002610
           02 MCICSO    PIC X(5).                                       00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MNETNAMA  PIC X.                                          00002640
           02 MNETNAMC  PIC X.                                          00002650
           02 MNETNAMP  PIC X.                                          00002660
           02 MNETNAMH  PIC X.                                          00002670
           02 MNETNAMV  PIC X.                                          00002680
           02 MNETNAMO  PIC X(8).                                       00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MSCREENA  PIC X.                                          00002710
           02 MSCREENC  PIC X.                                          00002720
           02 MSCREENP  PIC X.                                          00002730
           02 MSCREENH  PIC X.                                          00002740
           02 MSCREENV  PIC X.                                          00002750
           02 MSCREENO  PIC X(4).                                       00002760
                                                                                
