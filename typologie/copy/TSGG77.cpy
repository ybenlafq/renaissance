      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : CONSULTATION COMPOSITION DE GROUPE / ELEMENTS          *         
      *****************************************************************         
      *                                                               *         
       01  TS-GG77.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GG77-LONG                 PIC S9(03) COMP-3                    
                                           VALUE +117.                          
      *----------------------------------  DONNEES  TS                          
           02 TS-GG77-DONNEES.                                                  
      *----------------------------------  CODE FAM CODIC GROUPE                
              03 TS-GG77-CFAMG             PIC X(05).                           
      *----------------------------------  CODIC GROUPE                         
              03 TS-GG77-NCODICG           PIC X(07).                           
      *----------------------------------  PRIX CODIC GROUPE                    
              03 TS-GG77-PRIXG             PIC S9(05)V99 COMP-3.                
      *----------------------------------  DATE EFFET PRIX CODIC GROUPE         
              03 TS-GG77-DEFFETG           PIC X(08).                           
      *----------------------------------  STATUT CODIC GROUPE                  
              03 TS-GG77-STATCPEG          PIC X(03).                           
      *----------------------------------  REFERENCE CODIC GROUPE               
              03 TS-GG77-LREFFOUG          PIC X(20).                           
      *----------------------------------  CODIC ELEMENT                        
              03 TS-GG77-NCODICE           PIC X(07).                           
      *----------------------------------  CODE FAM CODIC ELEMENT               
              03 TS-GG77-CFAME             PIC X(05).                           
      *----------------------------------  PRIX CODIC ELEMENT                   
              03 TS-GG77-PRIXE             PIC S9(05)V99 COMP-3.                
      *----------------------------------  DATE EFFET PRIX CODIC ELEMENT        
              03 TS-GG77-DEFFETE           PIC X(08).                           
      *----------------------------------  STATUT CODIC ELEMENT                 
              03 TS-GG77-STATCPEE          PIC X(03).                           
      *----------------------------------  REFERENCE CODIC ELEMENT              
              03 TS-GG77-LREFFOUE          PIC X(20).                           
      *----------------------------------  QTE ARTICLES LIEES                   
              03 TS-GG77-QTYPLIEN          PIC 9.                               
      *----------------------------------  VALEUR DEGRE DE LIBERTE              
              03 TS-GG77-DEGRELIB          PIC X(05).                           
      *----------------------------------  CODIC GROUPE                         
              03 TS-GG77-CODICGRP          PIC X(07).                           
      *----------------------------------  CONCURRENT DU CODICE SI PVEX         
              03 TS-GG77-NCONCE            PIC X(05).                           
      *----------------------------------  CONCURRENT DU CODICG SI PVEX         
              03 TS-GG77-NCONCG            PIC X(05).                           
                                                                                
