      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGA2800                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA2800                         
      **********************************************************                
       01  RVGA2800.                                                            
           02  GA28-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GA28-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GA28-DVALIDITE                                                   
               PIC X(0008).                                                     
           02  GA28-QSOLA1                                                      
               PIC S9(3)V9(0002) COMP-3.                                        
           02  GA28-QSOLA2                                                      
               PIC S9(3)V9(0002) COMP-3.                                        
           02  GA28-QRACKB1                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
           02  GA28-QRACKB2                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
           02  GA28-QRACKB3                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
           02  GA28-QRACKB4                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
           02  GA28-QVRACC1                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGA2800                                  
      **********************************************************                
       01  RVGA2800-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA28-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA28-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA28-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA28-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA28-DVALIDITE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA28-DVALIDITE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA28-QSOLA1-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA28-QSOLA1-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA28-QSOLA2-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA28-QSOLA2-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA28-QRACKB1-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA28-QRACKB1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA28-QRACKB2-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA28-QRACKB2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA28-QRACKB3-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA28-QRACKB3-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA28-QRACKB4-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA28-QRACKB4-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA28-QVRACC1-F                                                   
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GA28-QVRACC1-F                                                   
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
      *}                                                                        
