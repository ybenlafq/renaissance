      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE CMTD00.RTGA31                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVGA3100.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVGA3100.                                                             
      *}                                                                        
           10 GA31-NCODIC          PIC X(07).                                   
           10 GA31-NEAN            PIC X(13).                                   
           10 GA31-WACTIF          PIC X(1).                                    
           10 GA31-LNEANPROD       PIC X(15).                                   
           10 GA31-WAUTHENT        PIC X(01).                                   
           10 GA31-LIDENTIFIANT    PIC X(10).                                   
           10 GA31-DACTIF          PIC X(08).                                   
           10 GA31-DSYST           PIC S9(13) COMP-3.                           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVGA3100-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVGA3100-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA31-NCODIC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA31-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA31-NEAN-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 GA31-NEAN-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA31-WACTIF-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA31-WACTIF-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA31-LNEANPROD-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GA31-LNEANPROD-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA31-WAUTHENT-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA31-WAUTHENT-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA31-LIDENTIFIANT-F  PIC S9(4) COMP.                              
      *--                                                                       
           10 GA31-LIDENTIFIANT-F  PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA31-DACTIF-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA31-DACTIF-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA31-DSYST-F         PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 GA31-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
