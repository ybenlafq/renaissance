      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GVDEL DELAI DE MISE A DISPO LIVRAISO   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01W6.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01W6.                                                            
      *}                                                                        
           05  GVDEL-CTABLEG2    PIC X(15).                                     
           05  GVDEL-CTABLEG2-REDEF REDEFINES GVDEL-CTABLEG2.                   
               10  GVDEL-NSOCENTR        PIC X(06).                             
               10  GVDEL-LDEPLIV         PIC X(06).                             
               10  GVDEL-CMODDEL         PIC X(03).                             
           05  GVDEL-WTABLEG     PIC X(80).                                     
           05  GVDEL-WTABLEG-REDEF  REDEFINES GVDEL-WTABLEG.                    
               10  GVDEL-WACTIF          PIC X(01).                             
               10  GVDEL-QDELAI          PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  GVDEL-QDELAI-N       REDEFINES GVDEL-QDELAI                  
                                         PIC 9(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01W6-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01W6-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GVDEL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GVDEL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GVDEL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GVDEL-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
