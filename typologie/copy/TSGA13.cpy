      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *===============================================================* 00011001
      *                                                               * 00012001
      *      TS ASSOCIE A LA TRANSACTION GA13                         * 00013001
      *      CONTIENT LES CODES MARKETINGS ASSOCIES A UN CODE FAMILLE * 00014001
      *                                                               * 00015001
      *      TR : GA13  TABLE D' EDITION DES ETATS                    * 00016001
      *                (TABLE MARKETING INTEGREE)                     * 00018001
      *      NOM: 'GA13' + EIBTRMID                                   * 00019001
      *      LG : 407 C                                               * 00019102
      *           A UN ITEM CORRESPOND UNE PAGE                       * 00019201
      *                                                               * 00019301
      *===============================================================* 00019401
                                                                        00019501
       01  TS-GA13.                                                     00060001
           05 TS-GA13-LONG              PIC S9(5) COMP-3     VALUE +407.00064002
           05 TS-GA13-DONNEES.                                          00066001
              10 TS-GA13-TABLE          OCCURS  11.                     00320001
                 15 TS-GA13-TAS         PIC  X.                         00330001
                 15 TS-GA13-OPER        PIC  X.                         00340001
                 15 TS-GA13-SELEC       PIC  X.                         00350001
                 15 TS-GA13-CRFAM       PIC  X(5).                      00360001
                 15 TS-GA13-CMARK       PIC  X(5).                      00370001
                 15 TS-GA13-LMARK       PIC  X(20).                     00380001
                 15 TS-GA13-WSAUT       PIC  X(1).                      00390001
                 15 TS-GA13-WSEQ        PIC  9(2).                      00400001
                 15 TS-GA13-WIMBR       PIC  9.                         00410001
                                                                        00410101
      *===============================================================* 00410201
                                                                                
