      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TFM05 TAUX DE CONVERSION DEVISES       *        
      *----------------------------------------------------------------*        
       01  RVGA01TW.                                                            
           05  TFM05-CTABLEG2    PIC X(15).                                     
           05  TFM05-CTABLEG2-REDEF REDEFINES TFM05-CTABLEG2.                   
               10  TFM05-CLE             PIC X(12).                             
           05  TFM05-WTABLEG     PIC X(80).                                     
           05  TFM05-WTABLEG-REDEF  REDEFINES TFM05-WTABLEG.                    
               10  TFM05-WACTIF          PIC X(01).                             
               10  TFM05-DEFFET          PIC X(08).                             
               10  TFM05-OPERATOR        PIC X(01).                             
               10  TFM05-TAUX            PIC S9(04)V9(05)     COMP-3.           
               10  TFM05-WZONES          PIC X(25).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01TW-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TFM05-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TFM05-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TFM05-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TFM05-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
