      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE BBTE  BRUN BLANC TLM ELA               *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01AL.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01AL.                                                            
      *}                                                                        
           05  BBTE-CTABLEG2     PIC X(15).                                     
           05  BBTE-CTABLEG2-REDEF  REDEFINES BBTE-CTABLEG2.                    
               10  BBTE-SOCIETE          PIC X(03).                             
               10  BBTE-NLIEU            PIC X(03).                             
           05  BBTE-WTABLEG      PIC X(80).                                     
           05  BBTE-WTABLEG-REDEF   REDEFINES BBTE-WTABLEG.                     
               10  BBTE-FLAG             PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01AL-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01AL-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BBTE-CTABLEG2-F   PIC S9(4)  COMP.                               
      *--                                                                       
           05  BBTE-CTABLEG2-F   PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BBTE-WTABLEG-F    PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  BBTE-WTABLEG-F    PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
