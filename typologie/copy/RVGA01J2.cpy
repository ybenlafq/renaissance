      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GGENT FOURNISSEURS PARTICULIERS NCG    *        
      *----------------------------------------------------------------*        
       01  RVGA01J2.                                                            
           05  GGENT-CTABLEG2    PIC X(15).                                     
           05  GGENT-CTABLEG2-REDEF REDEFINES GGENT-CTABLEG2.                   
               10  GGENT-NENTCDE         PIC X(05).                             
           05  GGENT-WTABLEG     PIC X(80).                                     
           05  GGENT-WTABLEG-REDEF  REDEFINES GGENT-WTABLEG.                    
               10  GGENT-FLAG            PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01J2-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GGENT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GGENT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GGENT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GGENT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
