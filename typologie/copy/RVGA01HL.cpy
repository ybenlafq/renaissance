      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TPLPR ASSOCIATION PLAN PROFIL TOURNE   *        
      *----------------------------------------------------------------*        
       01  RVGA01HL.                                                            
           05  TPLPR-CTABLEG2    PIC X(15).                                     
           05  TPLPR-CTABLEG2-REDEF REDEFINES TPLPR-CTABLEG2.                   
               10  TPLPR-NSOCDEP         PIC S9(07)       COMP-3.               
               10  TPLPR-CPLAN           PIC X(05).                             
               10  TPLPR-CPROFIL         PIC X(05).                             
           05  TPLPR-WTABLEG     PIC X(80).                                     
           05  TPLPR-WTABLEG-REDEF  REDEFINES TPLPR-WTABLEG.                    
               10  TPLPR-WTOURREC        PIC X(02).                             
               10  TPLPR-LADRTOUR        PIC X(18).                             
               10  TPLPR-WCOMMUT         PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01HL-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TPLPR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TPLPR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TPLPR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TPLPR-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
