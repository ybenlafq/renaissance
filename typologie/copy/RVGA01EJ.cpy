      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE LIENT DEGRE DE LIBERTE                 *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01EJ.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01EJ.                                                            
      *}                                                                        
           05  LIENT-CTABLEG2    PIC X(15).                                     
           05  LIENT-CTABLEG2-REDEF REDEFINES LIENT-CTABLEG2.                   
               10  LIENT-LIEN            PIC X(05).                             
               10  LIENT-DEGRE           PIC X(05).                             
           05  LIENT-WTABLEG     PIC X(80).                                     
           05  LIENT-WTABLEG-REDEF  REDEFINES LIENT-WTABLEG.                    
               10  LIENT-TOP             PIC X(01).                             
               10  LIENT-LIBELLE         PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01EJ-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01EJ-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LIENT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  LIENT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LIENT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  LIENT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
