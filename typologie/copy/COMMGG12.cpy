      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
           03  COMMGG12 REDEFINES COMM-GG00-FILLER.                             
             05 COMM-GG12-TABLE-DEMANDE-ALIG.                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       10 COMM-GG12-NBR-NDEMALI    PIC S9(4)     COMP.                   
      *--                                                                       
              10 COMM-GG12-NBR-NDEMALI    PIC S9(4) COMP-5.                     
      *}                                                                        
              10 COMM-GG12-TAB-NDEMALI    PIC 9(7) OCCURS 100.                  
             05 COMM-GG12-MAP.                                                  
              10 COMM-GG12-LFAM           PIC X(20).                            
              10 COMM-GG12-PGM            PIC X(05).                            
              10 COMM-GG12-LMARQ          PIC X(20).                            
              10 COMM-GG12-PEXPTTC        PIC S9(7)V99  COMP-3.                 
              10 COMM-GG12-PCF            PIC S9(7)V99  COMP-3.                 
              10 COMM-GG12-DATDEB         PIC 9(8).                             
              10 COMM-GG12-DATSUIV        PIC 9(8).                             
              10 COMM-GG12-DATFIN         PIC 9(8).                             
              10 COMM-GG12-WMULTIFAM      PIC X.                                
              10 COMM-GG12-MAGASIN.                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          15 COMM-GG12-NBR-MAG     PIC S9(4)     COMP.                   
      *--                                                                       
                 15 COMM-GG12-NBR-MAG     PIC S9(4) COMP-5.                     
      *}                                                                        
                 15 COMM-GG12-NMAGASIN    OCCURS 8.                             
                    20 COMM-GG12-NMAG     PIC 9(3).                             
                    20 COMM-GG12-LMAG     PIC X(20).                            
              10 COMM-GG12-PRMP           PIC S9(7)V99  COMP-3.                 
              10 COMM-GG12-PMBU           PIC S9(7)V99  COMP-3.                 
              10 COMM-GG12-QTMB           PIC S9(3)V9(4)   COMP-3.              
             05 COMM-GG12-PRIX.                                                 
              10 COMM-GG12-QTAUXTVA       PIC S9V9(4)  COMP-3.                  
                                                                                
