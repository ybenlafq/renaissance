      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SRP34 MAGASIN EN DEROGATION SRP        *        
      *----------------------------------------------------------------*        
       01  RVGA01RR.                                                            
           05  SRP34-CTABLEG2    PIC X(15).                                     
           05  SRP34-CTABLEG2-REDEF REDEFINES SRP34-CTABLEG2.                   
               10  SRP34-SSF             PIC X(02).                             
               10  SRP34-NSOCIETE        PIC X(03).                             
               10  SRP34-NLIEU           PIC X(03).                             
           05  SRP34-WTABLEG     PIC X(80).                                     
           05  SRP34-WTABLEG-REDEF  REDEFINES SRP34-WTABLEG.                    
               10  SRP34-WACTIF          PIC X(01).                             
               10  SRP34-WDEROG          PIC X(01).                             
               10  SRP34-WDEROG-N       REDEFINES SRP34-WDEROG                  
                                         PIC 9(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01RR-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SRP34-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SRP34-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SRP34-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SRP34-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
