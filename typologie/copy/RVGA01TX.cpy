      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TEURO DATES D EFFET DES DEVISES        *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01TX.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01TX.                                                            
      *}                                                                        
           05  TEURO-CTABLEG2    PIC X(15).                                     
           05  TEURO-CTABLEG2-REDEF REDEFINES TEURO-CTABLEG2.                   
               10  TEURO-CLE             PIC X(12).                             
           05  TEURO-WTABLEG     PIC X(80).                                     
           05  TEURO-WTABLEG-REDEF  REDEFINES TEURO-WTABLEG.                    
               10  TEURO-WACTIF          PIC X(01).                             
               10  TEURO-DEFFET          PIC X(08).                             
               10  TEURO-DFEFFET         PIC X(08).                             
               10  TEURO-WMONNAIE        PIC X(01).                             
               10  TEURO-WZONES          PIC X(30).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01TX-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01TX-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TEURO-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TEURO-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TEURO-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TEURO-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
