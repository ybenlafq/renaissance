      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGA7401                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA7401                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA7401.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA7401.                                                            
      *}                                                                        
           02  GA74-NCODIC                                                      
               PIC X(0007).                                                     
           02  GA74-NTITRE                                                      
               PIC X(0005).                                                     
           02  GA74-LTEXTE                                                      
               PIC X(0040).                                                     
           02  GA74-CCOULTEXTE                                                  
               PIC X(0005).                                                     
           02  GA74-NDEBTEXTE                                                   
               PIC X(0002).                                                     
           02  GA74-NFINTEXTE                                                   
               PIC X(0002).                                                     
           02  GA74-CCOULFOND                                                   
               PIC X(0005).                                                     
           02  GA74-NDEBFOND                                                    
               PIC X(0002).                                                     
           02  GA74-LGRFOND                                                     
               PIC X(0004).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGA7401                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA7401-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA7401-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA74-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA74-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA74-NTITRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA74-NTITRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA74-LTEXTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA74-LTEXTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA74-CCOULTEXTE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA74-CCOULTEXTE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA74-NDEBTEXTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA74-NDEBTEXTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA74-NFINTEXTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA74-NFINTEXTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA74-CCOULFOND-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA74-CCOULFOND-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA74-NDEBFOND-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA74-NDEBFOND-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA74-LGRFOND-F                                                   
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GA74-LGRFOND-F                                                   
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
