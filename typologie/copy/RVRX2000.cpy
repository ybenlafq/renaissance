      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVRX2000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRX2000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRX2000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRX2000.                                                            
      *}                                                                        
           02  RX20-NRELEVE                                                     
               PIC X(0007).                                                     
           02  RX20-NCONC                                                       
               PIC X(0004).                                                     
           02  RX20-NMAG                                                        
               PIC X(0003).                                                     
           02  RX20-WTYPREL                                                     
               PIC X(0001).                                                     
           02  RX20-DSUPPORT                                                    
               PIC X(0008).                                                     
           02  RX20-DRELEVE                                                     
               PIC X(0008).                                                     
           02  RX20-DHRELEVE                                                    
               PIC X(0002).                                                     
           02  RX20-D1SAIREL                                                    
               PIC X(0008).                                                     
           02  RX20-DCLOREL                                                     
               PIC X(0008).                                                     
           02  RX20-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRX2000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRX2000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRX2000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX20-NRELEVE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX20-NRELEVE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX20-NCONC-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX20-NCONC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX20-NMAG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX20-NMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX20-WTYPREL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX20-WTYPREL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX20-DSUPPORT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX20-DSUPPORT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX20-DRELEVE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX20-DRELEVE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX20-DHRELEVE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX20-DHRELEVE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX20-D1SAIREL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX20-D1SAIREL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX20-DCLOREL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX20-DCLOREL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX20-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX20-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
