      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *===============================================================* 00000010
      *                                                               * 00000020
      *      TS ASSOCIE A LA TRANSACTION GG18                         * 00000030
      *      CONTIENT TOUTES LES FAMILLES DE LA GA14                  * 00000040
      *                                                               * 00000050
      *                                                               * 00000060
      *      NOM: 'GG18' + EIBTRMID                                   * 00000070
      *                                                               * 00000080
      *===============================================================* 00000090
                                                                        00000100
       01  TS-GG18.                                                     00000110
           02 TS-GG18-NOM.                                              00000120
              04 TS-GG18-GG18           PIC X(4)         VALUE 'GG18'.  00000130
              04 TS-GG18-TRMID          PIC X(4).                       00000140
           02 TS-GG18-LONG              PIC S9(5) COMP-3 VALUE +990.    00000150
           02 TS-GG18-DONNEES.                                          00000160
              04 TS-GG18-LIGNE          OCCURS 15.                      00000170
      *                                                                 00000180
                 06 TS-GG18-CFAM        PIC X(05).                      00000190
                 06 TS-GG18-LFAM        PIC X(20).                      00000200
                 06 TS-GG18-WMULTIFAM   PIC X(01).                      00000210
                 06 TS-GG18-LMARK1      PIC X(20).                      00000220
                 06 TS-GG18-LMARK2      PIC X(20).                      00000230
      *                                                                 00000240
      *===============================================================* 00000250
                                                                                
