      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE VARAY SEQUENCE EDITION RAYON           *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01IE.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01IE.                                                            
      *}                                                                        
           05  VARAY-CTABLEG2    PIC X(15).                                     
           05  VARAY-CTABLEG2-REDEF REDEFINES VARAY-CTABLEG2.                   
               10  VARAY-CETAT           PIC X(06).                             
               10  VARAY-NSEQEDIT        PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  VARAY-NSEQEDIT-N     REDEFINES VARAY-NSEQEDIT                
                                         PIC 9(02).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  VARAY-WTABLEG     PIC X(80).                                     
           05  VARAY-WTABLEG-REDEF  REDEFINES VARAY-WTABLEG.                    
               10  VARAY-CRAYON          PIC X(05).                             
               10  VARAY-TYPTRI          PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  VARAY-TYPTRI-N       REDEFINES VARAY-TYPTRI                  
                                         PIC 9(02).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  VARAY-WACTIF          PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01IE-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01IE-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  VARAY-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  VARAY-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  VARAY-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  VARAY-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
