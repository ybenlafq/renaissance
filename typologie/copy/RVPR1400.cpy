      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVPR1400                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVPR1400                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPR1400.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPR1400.                                                            
      *}                                                                        
           02  PR14-CPRESTATION                                                 
               PIC X(0005).                                                     
           02  PR14-NZONPRIX                                                    
               PIC X(0002).                                                     
           02  PR14-NCODIC                                                      
               PIC X(0007).                                                     
           02  PR14-MTPREST                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  PR14-DEFFET                                                      
               PIC X(0008).                                                     
           02  PR14-DFEFFET                                                     
               PIC X(0008).                                                     
           02  PR14-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVPR1400                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPR1400-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPR1400-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR14-CPRESTATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR14-CPRESTATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR14-NZONPRIX-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR14-NZONPRIX-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR14-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR14-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR14-MTPREST-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR14-MTPREST-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR14-DEFFET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR14-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR14-DFEFFET-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR14-DFEFFET-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR14-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR14-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
