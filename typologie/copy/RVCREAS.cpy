      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CREAS CODE RAISON                      *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCREAS.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCREAS.                                                             
      *}                                                                        
           05  CREAS-CTABLEG2    PIC X(15).                                     
           05  CREAS-CTABLEG2-REDEF REDEFINES CREAS-CTABLEG2.                   
               10  CREAS-CREASON         PIC X(05).                             
           05  CREAS-WTABLEG     PIC X(80).                                     
           05  CREAS-WTABLEG-REDEF  REDEFINES CREAS-WTABLEG.                    
               10  CREAS-LREASON         PIC X(57).                             
               10  CREAS-WACTIF          PIC X(01).                             
               10  CREAS-WNSEQ           PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  CREAS-WNSEQ-N        REDEFINES CREAS-WNSEQ                   
                                         PIC 9(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCREAS-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCREAS-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CREAS-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CREAS-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CREAS-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CREAS-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
