      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESE00   ESE00                                              00000020
      ***************************************************************** 00000030
       01   ETH13I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFONCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFONCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCFONCI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPCOL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCOPCOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOPCOF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCOPCOI   PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLOPCOL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLOPCOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLOPCOF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLOPCOI   PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNCODICI  PIC X(7).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLREFFOURNI    PIC X(20).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCFAMI    PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLFAMI    PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MHISTOL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MHISTOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MHISTOF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MHISTOI   PIC X.                                          00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCMARQI   PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLMARQI   PIC X(20).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEL    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MDATEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDATEF    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MDATEI    PIC X(10).                                      00000650
           02 MSD OCCURS   10 TIMES .                                   00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSL     COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MSL COMP-5 PIC S9(4).                                           
      *}                                                                        
             03 MSF     PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MSI     PIC X.                                          00000700
           02 MLCOPCOD OCCURS   10 TIMES .                              00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOPCOL     COMP PIC S9(4).                            00000720
      *--                                                                       
             03 MLCOPCOL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLCOPCOF     PIC X.                                     00000730
             03 FILLER  PIC X(4).                                       00000740
             03 MLCOPCOI     PIC X(3).                                  00000750
           02 MDEFFETD OCCURS   10 TIMES .                              00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00000770
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MDEFFETI     PIC X(10).                                 00000800
           02 MPVPD OCCURS   10 TIMES .                                 00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPVPL   COMP PIC S9(4).                                 00000820
      *--                                                                       
             03 MPVPL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MPVPF   PIC X.                                          00000830
             03 FILLER  PIC X(4).                                       00000840
             03 MPVPI   PIC X(14).                                      00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MZONCMDI  PIC X(15).                                      00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MLIBERRI  PIC X(58).                                      00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MCODTRAI  PIC X(4).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MCICSI    PIC X(5).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MNETNAMI  PIC X(8).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MSCREENI  PIC X(4).                                       00001090
      ***************************************************************** 00001100
      * SDF: ESE00   ESE00                                              00001110
      ***************************************************************** 00001120
       01   ETH13O REDEFINES ETH13I.                                    00001130
           02 FILLER    PIC X(12).                                      00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MDATJOUA  PIC X.                                          00001160
           02 MDATJOUC  PIC X.                                          00001170
           02 MDATJOUP  PIC X.                                          00001180
           02 MDATJOUH  PIC X.                                          00001190
           02 MDATJOUV  PIC X.                                          00001200
           02 MDATJOUO  PIC X(10).                                      00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MTIMJOUA  PIC X.                                          00001230
           02 MTIMJOUC  PIC X.                                          00001240
           02 MTIMJOUP  PIC X.                                          00001250
           02 MTIMJOUH  PIC X.                                          00001260
           02 MTIMJOUV  PIC X.                                          00001270
           02 MTIMJOUO  PIC X(5).                                       00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MPAGEA    PIC X.                                          00001300
           02 MPAGEC    PIC X.                                          00001310
           02 MPAGEP    PIC X.                                          00001320
           02 MPAGEH    PIC X.                                          00001330
           02 MPAGEV    PIC X.                                          00001340
           02 MPAGEO    PIC X(3).                                       00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MPAGEMAXA      PIC X.                                     00001370
           02 MPAGEMAXC PIC X.                                          00001380
           02 MPAGEMAXP PIC X.                                          00001390
           02 MPAGEMAXH PIC X.                                          00001400
           02 MPAGEMAXV PIC X.                                          00001410
           02 MPAGEMAXO      PIC X(3).                                  00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MCFONCA   PIC X.                                          00001440
           02 MCFONCC   PIC X.                                          00001450
           02 MCFONCP   PIC X.                                          00001460
           02 MCFONCH   PIC X.                                          00001470
           02 MCFONCV   PIC X.                                          00001480
           02 MCFONCO   PIC X(3).                                       00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MCOPCOA   PIC X.                                          00001510
           02 MCOPCOC   PIC X.                                          00001520
           02 MCOPCOP   PIC X.                                          00001530
           02 MCOPCOH   PIC X.                                          00001540
           02 MCOPCOV   PIC X.                                          00001550
           02 MCOPCOO   PIC X(3).                                       00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MLOPCOA   PIC X.                                          00001580
           02 MLOPCOC   PIC X.                                          00001590
           02 MLOPCOP   PIC X.                                          00001600
           02 MLOPCOH   PIC X.                                          00001610
           02 MLOPCOV   PIC X.                                          00001620
           02 MLOPCOO   PIC X(20).                                      00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MNCODICA  PIC X.                                          00001650
           02 MNCODICC  PIC X.                                          00001660
           02 MNCODICP  PIC X.                                          00001670
           02 MNCODICH  PIC X.                                          00001680
           02 MNCODICV  PIC X.                                          00001690
           02 MNCODICO  PIC X(7).                                       00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MLREFFOURNA    PIC X.                                     00001720
           02 MLREFFOURNC    PIC X.                                     00001730
           02 MLREFFOURNP    PIC X.                                     00001740
           02 MLREFFOURNH    PIC X.                                     00001750
           02 MLREFFOURNV    PIC X.                                     00001760
           02 MLREFFOURNO    PIC X(20).                                 00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MCFAMA    PIC X.                                          00001790
           02 MCFAMC    PIC X.                                          00001800
           02 MCFAMP    PIC X.                                          00001810
           02 MCFAMH    PIC X.                                          00001820
           02 MCFAMV    PIC X.                                          00001830
           02 MCFAMO    PIC X(5).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MLFAMA    PIC X.                                          00001860
           02 MLFAMC    PIC X.                                          00001870
           02 MLFAMP    PIC X.                                          00001880
           02 MLFAMH    PIC X.                                          00001890
           02 MLFAMV    PIC X.                                          00001900
           02 MLFAMO    PIC X(20).                                      00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MHISTOA   PIC X.                                          00001930
           02 MHISTOC   PIC X.                                          00001940
           02 MHISTOP   PIC X.                                          00001950
           02 MHISTOH   PIC X.                                          00001960
           02 MHISTOV   PIC X.                                          00001970
           02 MHISTOO   PIC X.                                          00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MCMARQA   PIC X.                                          00002000
           02 MCMARQC   PIC X.                                          00002010
           02 MCMARQP   PIC X.                                          00002020
           02 MCMARQH   PIC X.                                          00002030
           02 MCMARQV   PIC X.                                          00002040
           02 MCMARQO   PIC X(5).                                       00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MLMARQA   PIC X.                                          00002070
           02 MLMARQC   PIC X.                                          00002080
           02 MLMARQP   PIC X.                                          00002090
           02 MLMARQH   PIC X.                                          00002100
           02 MLMARQV   PIC X.                                          00002110
           02 MLMARQO   PIC X(20).                                      00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MDATEA    PIC X.                                          00002140
           02 MDATEC    PIC X.                                          00002150
           02 MDATEP    PIC X.                                          00002160
           02 MDATEH    PIC X.                                          00002170
           02 MDATEV    PIC X.                                          00002180
           02 MDATEO    PIC X(10).                                      00002190
           02 DFHMS1 OCCURS   10 TIMES .                                00002200
             03 FILLER       PIC X(2).                                  00002210
             03 MSA     PIC X.                                          00002220
             03 MSC     PIC X.                                          00002230
             03 MSP     PIC X.                                          00002240
             03 MSH     PIC X.                                          00002250
             03 MSV     PIC X.                                          00002260
             03 MSO     PIC X.                                          00002270
           02 DFHMS2 OCCURS   10 TIMES .                                00002280
             03 FILLER       PIC X(2).                                  00002290
             03 MLCOPCOA     PIC X.                                     00002300
             03 MLCOPCOC     PIC X.                                     00002310
             03 MLCOPCOP     PIC X.                                     00002320
             03 MLCOPCOH     PIC X.                                     00002330
             03 MLCOPCOV     PIC X.                                     00002340
             03 MLCOPCOO     PIC X(3).                                  00002350
           02 DFHMS3 OCCURS   10 TIMES .                                00002360
             03 FILLER       PIC X(2).                                  00002370
             03 MDEFFETA     PIC X.                                     00002380
             03 MDEFFETC     PIC X.                                     00002390
             03 MDEFFETP     PIC X.                                     00002400
             03 MDEFFETH     PIC X.                                     00002410
             03 MDEFFETV     PIC X.                                     00002420
             03 MDEFFETO     PIC X(10).                                 00002430
           02 DFHMS4 OCCURS   10 TIMES .                                00002440
             03 FILLER       PIC X(2).                                  00002450
             03 MPVPA   PIC X.                                          00002460
             03 MPVPC   PIC X.                                          00002470
             03 MPVPP   PIC X.                                          00002480
             03 MPVPH   PIC X.                                          00002490
             03 MPVPV   PIC X.                                          00002500
             03 MPVPO   PIC X(14).                                      00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MZONCMDA  PIC X.                                          00002530
           02 MZONCMDC  PIC X.                                          00002540
           02 MZONCMDP  PIC X.                                          00002550
           02 MZONCMDH  PIC X.                                          00002560
           02 MZONCMDV  PIC X.                                          00002570
           02 MZONCMDO  PIC X(15).                                      00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MLIBERRA  PIC X.                                          00002600
           02 MLIBERRC  PIC X.                                          00002610
           02 MLIBERRP  PIC X.                                          00002620
           02 MLIBERRH  PIC X.                                          00002630
           02 MLIBERRV  PIC X.                                          00002640
           02 MLIBERRO  PIC X(58).                                      00002650
           02 FILLER    PIC X(2).                                       00002660
           02 MCODTRAA  PIC X.                                          00002670
           02 MCODTRAC  PIC X.                                          00002680
           02 MCODTRAP  PIC X.                                          00002690
           02 MCODTRAH  PIC X.                                          00002700
           02 MCODTRAV  PIC X.                                          00002710
           02 MCODTRAO  PIC X(4).                                       00002720
           02 FILLER    PIC X(2).                                       00002730
           02 MCICSA    PIC X.                                          00002740
           02 MCICSC    PIC X.                                          00002750
           02 MCICSP    PIC X.                                          00002760
           02 MCICSH    PIC X.                                          00002770
           02 MCICSV    PIC X.                                          00002780
           02 MCICSO    PIC X(5).                                       00002790
           02 FILLER    PIC X(2).                                       00002800
           02 MNETNAMA  PIC X.                                          00002810
           02 MNETNAMC  PIC X.                                          00002820
           02 MNETNAMP  PIC X.                                          00002830
           02 MNETNAMH  PIC X.                                          00002840
           02 MNETNAMV  PIC X.                                          00002850
           02 MNETNAMO  PIC X(8).                                       00002860
           02 FILLER    PIC X(2).                                       00002870
           02 MSCREENA  PIC X.                                          00002880
           02 MSCREENC  PIC X.                                          00002890
           02 MSCREENP  PIC X.                                          00002900
           02 MSCREENH  PIC X.                                          00002910
           02 MSCREENV  PIC X.                                          00002920
           02 MSCREENO  PIC X(4).                                       00002930
                                                                                
