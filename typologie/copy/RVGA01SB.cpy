      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE AUTO1 REGROUPEMENTS PAR APPLICATION    *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01SB.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01SB.                                                            
      *}                                                                        
           05  AUTO1-CTABLEG2    PIC X(15).                                     
           05  AUTO1-CTABLEG2-REDEF REDEFINES AUTO1-CTABLEG2.                   
               10  AUTO1-CDOMAPPL        PIC X(07).                             
               10  AUTO1-CTABLE          PIC X(06).                             
           05  AUTO1-WTABLEG     PIC X(80).                                     
           05  AUTO1-WTABLEG-REDEF  REDEFINES AUTO1-WTABLEG.                    
               10  AUTO1-WACTIF          PIC X(01).                             
               10  AUTO1-TYPEAUTO        PIC X(03).                             
               10  AUTO1-LCOMMENT        PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01SB-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01SB-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AUTO1-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  AUTO1-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AUTO1-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  AUTO1-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
