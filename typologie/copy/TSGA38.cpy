      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *===============================================================* 00010000
      *                                                               * 00020000
      *      TS ASSOCIE A LA TRANSACTION GA38                         * 00030000
      *            AJOUT DE L'ECOTAXE AU PRODUIT                      * 00040000
      *                                                               * 00050000
      *===============================================================* 00060000
                                                                        00070000
       01 TS-GA38.                                                      00080000
          05 TS-GA38-LONG              PIC S9(5) COMP-3     VALUE +117. 00090014
          05 TS-GA38-DONNEES.                                           00100000
             10 TS-GA38-NCODIC         PIC  X(7).                       00111000
             10 TS-GA38-CPAYS          PIC  X(2).                       00113000
             10 TS-GA38-WIMPORTE       PIC  X(1).                       00116004
             10 TS-GA38-NSEQ           PIC  S9(9)    COMP-3.            00117012
             10 TS-GA38-DONNEES-O.                                      00120000
                15 TS-GA38-CFOURN-O    PIC  X(5).                       00140000
                15 TS-GA38-LFOURN-O    PIC  X(20).                      00150006
      *--                                                    SSAAMMJJ   00160000
                15 TS-GA38-DEFFETA-O   PIC  X(8).                       00170001
                15 TS-GA38-DEFFETV-O   PIC  X(8).                       00180001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         15 TS-GA38-MONTANTA-O  PIC  S9(5)V99 COMP.              00190001
      *--                                                                       
                15 TS-GA38-MONTANTA-O  PIC  S9(5)V99 COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         15 TS-GA38-MONTANTV-O  PIC  S9(5)V99 COMP.              00190101
      *--                                                                       
                15 TS-GA38-MONTANTV-O  PIC  S9(5)V99 COMP-5.                    
      *}                                                                        
                15 TS-GA38-WTFOUR-O    PIC  X(01).                      00191001
      * DONNEES DE L'ECRAN MISE A JOUR                                  00200000
             10 TS-GA38-DONNEES-N.                                      00210000
                15 TS-GA38-CFOURN-N    PIC  X(5).                       00230000
                15 TS-GA38-LFOURN-N    PIC  X(20).                      00240006
      *--                                                    SSAAMMJJ   00250000
                15 TS-GA38-DEFFETA-N   PIC  X(8).                       00260001
                15 TS-GA38-DEFFETV-N   PIC  X(8).                       00270001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         15 TS-GA38-MONTANTA-N  PIC  S9(5)V99 COMP .             00280001
      *--                                                                       
                15 TS-GA38-MONTANTA-N  PIC  S9(5)V99 COMP-5 .                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         15 TS-GA38-MONTANTV-N  PIC  S9(5)V99 COMP .             00280101
      *--                                                                       
                15 TS-GA38-MONTANTV-N  PIC  S9(5)V99 COMP-5 .                   
      *}                                                                        
                15 TS-GA38-WTFOUR-N    PIC  X(01).                      00281000
      * FLAG                                                            00282010
             10 TS-GA38-FLAG-ACTION    PIC  X(01) VALUE ' '.            00283010
                88 TS-GA38-NORMAL                 VALUE ' '.            00284010
                88 TS-GA38-CREATION               VALUE 'C'.            00285010
                88 TS-GA38-MAJ                    VALUE 'M'.            00286010
                88 TS-GA38-SUPPRESSION            VALUE 'S'.            00287011
                88 TS-GA38-SUPP-CRE               VALUE 'X'.            00288011
             10 TS-GA38-DONNEES-GA55   PIC  X(01).                      00289013
      *===============================================================* 00320000
                                                                                
