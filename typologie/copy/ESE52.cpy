      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESE52   ESE52                                              00000020
      ***************************************************************** 00000030
       01   ESE52I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCSERVL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNCSERVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCSERVF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNCSERVI  PIC X(5).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCSERVL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLCSERVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCSERVF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLCSERVI  PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPERL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCOPERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOPERF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCOPERI   PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLOPERL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLOPERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLOPERF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLOPERI   PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCFAMI    PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLFAMI    PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MPAGEI    PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MPAGEMAXI      PIC X(3).                                  00000490
           02 MWTABLEI OCCURS   11 TIMES .                              00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCCDESCL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MCCDESCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCCDESCF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCCDESCI     PIC X(5).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCDESCL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MLCDESCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLCDESCF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MLCDESCI     PIC X(20).                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCVDESCL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MCVDESCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCVDESCF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCVDESCI     PIC X(5).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLVDESCL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MLVDESCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLVDESCF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MLVDESCI     PIC X(20).                                 00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MZONCMDI  PIC X(15).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MLIBERRI  PIC X(58).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCODTRAI  PIC X(4).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCICSI    PIC X(5).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MNETNAMI  PIC X(8).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MSCREENI  PIC X(4).                                       00000900
      ***************************************************************** 00000910
      * SDF: ESE52   ESE52                                              00000920
      ***************************************************************** 00000930
       01   ESE52O REDEFINES ESE52I.                                    00000940
           02 FILLER    PIC X(12).                                      00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MDATJOUA  PIC X.                                          00000970
           02 MDATJOUC  PIC X.                                          00000980
           02 MDATJOUP  PIC X.                                          00000990
           02 MDATJOUH  PIC X.                                          00001000
           02 MDATJOUV  PIC X.                                          00001010
           02 MDATJOUO  PIC X(10).                                      00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MTIMJOUA  PIC X.                                          00001040
           02 MTIMJOUC  PIC X.                                          00001050
           02 MTIMJOUP  PIC X.                                          00001060
           02 MTIMJOUH  PIC X.                                          00001070
           02 MTIMJOUV  PIC X.                                          00001080
           02 MTIMJOUO  PIC X(5).                                       00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MWFONCA   PIC X.                                          00001110
           02 MWFONCC   PIC X.                                          00001120
           02 MWFONCP   PIC X.                                          00001130
           02 MWFONCH   PIC X.                                          00001140
           02 MWFONCV   PIC X.                                          00001150
           02 MWFONCO   PIC X(3).                                       00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MNCSERVA  PIC X.                                          00001180
           02 MNCSERVC  PIC X.                                          00001190
           02 MNCSERVP  PIC X.                                          00001200
           02 MNCSERVH  PIC X.                                          00001210
           02 MNCSERVV  PIC X.                                          00001220
           02 MNCSERVO  PIC X(5).                                       00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MLCSERVA  PIC X.                                          00001250
           02 MLCSERVC  PIC X.                                          00001260
           02 MLCSERVP  PIC X.                                          00001270
           02 MLCSERVH  PIC X.                                          00001280
           02 MLCSERVV  PIC X.                                          00001290
           02 MLCSERVO  PIC X(20).                                      00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MCOPERA   PIC X.                                          00001320
           02 MCOPERC   PIC X.                                          00001330
           02 MCOPERP   PIC X.                                          00001340
           02 MCOPERH   PIC X.                                          00001350
           02 MCOPERV   PIC X.                                          00001360
           02 MCOPERO   PIC X(5).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MLOPERA   PIC X.                                          00001390
           02 MLOPERC   PIC X.                                          00001400
           02 MLOPERP   PIC X.                                          00001410
           02 MLOPERH   PIC X.                                          00001420
           02 MLOPERV   PIC X.                                          00001430
           02 MLOPERO   PIC X(20).                                      00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MCFAMA    PIC X.                                          00001460
           02 MCFAMC    PIC X.                                          00001470
           02 MCFAMP    PIC X.                                          00001480
           02 MCFAMH    PIC X.                                          00001490
           02 MCFAMV    PIC X.                                          00001500
           02 MCFAMO    PIC X(5).                                       00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MLFAMA    PIC X.                                          00001530
           02 MLFAMC    PIC X.                                          00001540
           02 MLFAMP    PIC X.                                          00001550
           02 MLFAMH    PIC X.                                          00001560
           02 MLFAMV    PIC X.                                          00001570
           02 MLFAMO    PIC X(20).                                      00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MPAGEA    PIC X.                                          00001600
           02 MPAGEC    PIC X.                                          00001610
           02 MPAGEP    PIC X.                                          00001620
           02 MPAGEH    PIC X.                                          00001630
           02 MPAGEV    PIC X.                                          00001640
           02 MPAGEO    PIC X(3).                                       00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MPAGEMAXA      PIC X.                                     00001670
           02 MPAGEMAXC PIC X.                                          00001680
           02 MPAGEMAXP PIC X.                                          00001690
           02 MPAGEMAXH PIC X.                                          00001700
           02 MPAGEMAXV PIC X.                                          00001710
           02 MPAGEMAXO      PIC X(3).                                  00001720
           02 MWTABLEO OCCURS   11 TIMES .                              00001730
             03 FILLER       PIC X(2).                                  00001740
             03 MCCDESCA     PIC X.                                     00001750
             03 MCCDESCC     PIC X.                                     00001760
             03 MCCDESCP     PIC X.                                     00001770
             03 MCCDESCH     PIC X.                                     00001780
             03 MCCDESCV     PIC X.                                     00001790
             03 MCCDESCO     PIC X(5).                                  00001800
             03 FILLER       PIC X(2).                                  00001810
             03 MLCDESCA     PIC X.                                     00001820
             03 MLCDESCC     PIC X.                                     00001830
             03 MLCDESCP     PIC X.                                     00001840
             03 MLCDESCH     PIC X.                                     00001850
             03 MLCDESCV     PIC X.                                     00001860
             03 MLCDESCO     PIC X(20).                                 00001870
             03 FILLER       PIC X(2).                                  00001880
             03 MCVDESCA     PIC X.                                     00001890
             03 MCVDESCC     PIC X.                                     00001900
             03 MCVDESCP     PIC X.                                     00001910
             03 MCVDESCH     PIC X.                                     00001920
             03 MCVDESCV     PIC X.                                     00001930
             03 MCVDESCO     PIC X(5).                                  00001940
             03 FILLER       PIC X(2).                                  00001950
             03 MLVDESCA     PIC X.                                     00001960
             03 MLVDESCC     PIC X.                                     00001970
             03 MLVDESCP     PIC X.                                     00001980
             03 MLVDESCH     PIC X.                                     00001990
             03 MLVDESCV     PIC X.                                     00002000
             03 MLVDESCO     PIC X(20).                                 00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MZONCMDA  PIC X.                                          00002030
           02 MZONCMDC  PIC X.                                          00002040
           02 MZONCMDP  PIC X.                                          00002050
           02 MZONCMDH  PIC X.                                          00002060
           02 MZONCMDV  PIC X.                                          00002070
           02 MZONCMDO  PIC X(15).                                      00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MLIBERRA  PIC X.                                          00002100
           02 MLIBERRC  PIC X.                                          00002110
           02 MLIBERRP  PIC X.                                          00002120
           02 MLIBERRH  PIC X.                                          00002130
           02 MLIBERRV  PIC X.                                          00002140
           02 MLIBERRO  PIC X(58).                                      00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCODTRAA  PIC X.                                          00002170
           02 MCODTRAC  PIC X.                                          00002180
           02 MCODTRAP  PIC X.                                          00002190
           02 MCODTRAH  PIC X.                                          00002200
           02 MCODTRAV  PIC X.                                          00002210
           02 MCODTRAO  PIC X(4).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MCICSA    PIC X.                                          00002240
           02 MCICSC    PIC X.                                          00002250
           02 MCICSP    PIC X.                                          00002260
           02 MCICSH    PIC X.                                          00002270
           02 MCICSV    PIC X.                                          00002280
           02 MCICSO    PIC X(5).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MNETNAMA  PIC X.                                          00002310
           02 MNETNAMC  PIC X.                                          00002320
           02 MNETNAMP  PIC X.                                          00002330
           02 MNETNAMH  PIC X.                                          00002340
           02 MNETNAMV  PIC X.                                          00002350
           02 MNETNAMO  PIC X(8).                                       00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MSCREENA  PIC X.                                          00002380
           02 MSCREENC  PIC X.                                          00002390
           02 MSCREENP  PIC X.                                          00002400
           02 MSCREENH  PIC X.                                          00002410
           02 MSCREENV  PIC X.                                          00002420
           02 MSCREENO  PIC X(4).                                       00002430
                                                                                
