      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CLASS CLASSES DE ROTATION ABC          *        
      *----------------------------------------------------------------*        
       01  RVGA01Y.                                                             
           05  CLASS-CTABLEG2    PIC X(15).                                     
           05  CLASS-CTABLEG2-REDEF REDEFINES CLASS-CTABLEG2.                   
               10  CLASS-CLASSE          PIC X(01).                             
           05  CLASS-WTABLEG     PIC X(80).                                     
           05  CLASS-WTABLEG-REDEF  REDEFINES CLASS-WTABLEG.                    
               10  CLASS-WPOURCEN        PIC X(03).                             
               10  CLASS-WPOURCEN-N     REDEFINES CLASS-WPOURCEN                
                                         PIC 9(03).                             
               10  CLASS-WCLASSE         PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01Y-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CLASS-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CLASS-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CLASS-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CLASS-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
