      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGI2200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGI2200                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGI2200.                                                            
           02  GI22-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GI22-NZONPRIX                                                    
               PIC X(0002).                                                     
           02  GI22-NCODIC                                                      
               PIC X(0007).                                                     
           02  GI22-DEFFET                                                      
               PIC X(0008).                                                     
           02  GI22-DFINEFFET                                                   
               PIC X(0008).                                                     
           02  GI22-PCOMMFOR                                                    
               PIC S9(5)V9(0002) COMP-3.                                        
           02  GI22-PCOMMART                                                    
               PIC S9(5)V9(0002) COMP-3.                                        
           02  GI22-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGI2200                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGI2200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI22-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI22-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI22-NZONPRIX-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI22-NZONPRIX-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI22-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI22-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI22-DEFFET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI22-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI22-DFINEFFET-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI22-DFINEFFET-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI22-PCOMMFOR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI22-PCOMMFOR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI22-PCOMMART-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI22-PCOMMART-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI22-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI22-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
