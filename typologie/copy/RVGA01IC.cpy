      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE VAGLI GROUPE DE LIEUX PAR ETAT VALO    *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01IC.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01IC.                                                            
      *}                                                                        
           05  VAGLI-CTABLEG2    PIC X(15).                                     
           05  VAGLI-CTABLEG2-REDEF REDEFINES VAGLI-CTABLEG2.                   
               10  VAGLI-CETAT           PIC X(06).                             
               10  VAGLI-NSEQ            PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  VAGLI-NSEQ-N         REDEFINES VAGLI-NSEQ                    
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  VAGLI-WTABLEG     PIC X(80).                                     
           05  VAGLI-WTABLEG-REDEF  REDEFINES VAGLI-WTABLEG.                    
               10  VAGLI-CTYPLIEU        PIC X(01).                             
               10  VAGLI-NSOCLIEU        PIC X(06).                             
               10  VAGLI-CCONSO          PIC X(06).                             
               10  VAGLI-WACTIF          PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01IC-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01IC-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  VAGLI-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  VAGLI-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  VAGLI-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  VAGLI-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
