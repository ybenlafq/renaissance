      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA17   EGA17                                              00000020
      ***************************************************************** 00000030
       01   EGA75I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEML   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEMF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFONCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFONCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCFONCI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPRODL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNBPRODL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBPRODF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNBPRODI  PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCMARQI   PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLMARQI   PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFOL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLREFOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLREFOF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLREFOI   PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCREFL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCREFF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCREFI    PIC X(20).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCFAMI    PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLFAMI    PIC X(20).                                      00000530
           02 OCCURSI OCCURS   12 TIMES .                               00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODICKL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MCODICKL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCODICKF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCODICKI     PIC X(7).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAMKL      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MCFAMKL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCFAMKF      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCFAMKI      PIC X(5).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCREFKL      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MCREFKL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCREFKF      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCREFKI      PIC X(20).                                 00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNPRODL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MNPRODL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNPRODF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNPRODI      PIC X(15).                                 00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCCOLORL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MCCOLORL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCCOLORF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MCCOLORI     PIC X(5).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOPCOL      COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MCOPCOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCOPCOF      PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCOPCOI      PIC X(3).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATCREL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MDATCREL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDATCREF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MDATCREI     PIC X(8).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MZONCMDI  PIC X(15).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MLIBERRI  PIC X(58).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCODTRAI  PIC X(4).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCICSI    PIC X(5).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MNETNAMI  PIC X(8).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MSCREENI  PIC X(4).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRFAML    COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MRFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MRFAMF    PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MRFAMI    PIC X.                                          00001100
      ***************************************************************** 00001110
      * SDF: EGA17   EGA17                                              00001120
      ***************************************************************** 00001130
       01   EGA75O REDEFINES EGA75I.                                    00001140
           02 FILLER    PIC X(12).                                      00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MDATJOUA  PIC X.                                          00001170
           02 MDATJOUC  PIC X.                                          00001180
           02 MDATJOUP  PIC X.                                          00001190
           02 MDATJOUH  PIC X.                                          00001200
           02 MDATJOUV  PIC X.                                          00001210
           02 MDATJOUO  PIC X(10).                                      00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MTIMJOUA  PIC X.                                          00001240
           02 MTIMJOUC  PIC X.                                          00001250
           02 MTIMJOUP  PIC X.                                          00001260
           02 MTIMJOUH  PIC X.                                          00001270
           02 MTIMJOUV  PIC X.                                          00001280
           02 MTIMJOUO  PIC X(5).                                       00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MPAGEA    PIC X.                                          00001310
           02 MPAGEC    PIC X.                                          00001320
           02 MPAGEP    PIC X.                                          00001330
           02 MPAGEH    PIC X.                                          00001340
           02 MPAGEV    PIC X.                                          00001350
           02 MPAGEO    PIC X(3).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MPAGEMA   PIC X.                                          00001380
           02 MPAGEMC   PIC X.                                          00001390
           02 MPAGEMP   PIC X.                                          00001400
           02 MPAGEMH   PIC X.                                          00001410
           02 MPAGEMV   PIC X.                                          00001420
           02 MPAGEMO   PIC X(3).                                       00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MCFONCA   PIC X.                                          00001450
           02 MCFONCC   PIC X.                                          00001460
           02 MCFONCP   PIC X.                                          00001470
           02 MCFONCH   PIC X.                                          00001480
           02 MCFONCV   PIC X.                                          00001490
           02 MCFONCO   PIC X(3).                                       00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MNBPRODA  PIC X.                                          00001520
           02 MNBPRODC  PIC X.                                          00001530
           02 MNBPRODP  PIC X.                                          00001540
           02 MNBPRODH  PIC X.                                          00001550
           02 MNBPRODV  PIC X.                                          00001560
           02 MNBPRODO  PIC X(20).                                      00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MCMARQA   PIC X.                                          00001590
           02 MCMARQC   PIC X.                                          00001600
           02 MCMARQP   PIC X.                                          00001610
           02 MCMARQH   PIC X.                                          00001620
           02 MCMARQV   PIC X.                                          00001630
           02 MCMARQO   PIC X(5).                                       00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MLMARQA   PIC X.                                          00001660
           02 MLMARQC   PIC X.                                          00001670
           02 MLMARQP   PIC X.                                          00001680
           02 MLMARQH   PIC X.                                          00001690
           02 MLMARQV   PIC X.                                          00001700
           02 MLMARQO   PIC X(20).                                      00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MLREFOA   PIC X.                                          00001730
           02 MLREFOC   PIC X.                                          00001740
           02 MLREFOP   PIC X.                                          00001750
           02 MLREFOH   PIC X.                                          00001760
           02 MLREFOV   PIC X.                                          00001770
           02 MLREFOO   PIC X(20).                                      00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MCREFA    PIC X.                                          00001800
           02 MCREFC    PIC X.                                          00001810
           02 MCREFP    PIC X.                                          00001820
           02 MCREFH    PIC X.                                          00001830
           02 MCREFV    PIC X.                                          00001840
           02 MCREFO    PIC X(20).                                      00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MCFAMA    PIC X.                                          00001870
           02 MCFAMC    PIC X.                                          00001880
           02 MCFAMP    PIC X.                                          00001890
           02 MCFAMH    PIC X.                                          00001900
           02 MCFAMV    PIC X.                                          00001910
           02 MCFAMO    PIC X(5).                                       00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MLFAMA    PIC X.                                          00001940
           02 MLFAMC    PIC X.                                          00001950
           02 MLFAMP    PIC X.                                          00001960
           02 MLFAMH    PIC X.                                          00001970
           02 MLFAMV    PIC X.                                          00001980
           02 MLFAMO    PIC X(20).                                      00001990
           02 OCCURSO OCCURS   12 TIMES .                               00002000
             03 FILLER       PIC X(2).                                  00002010
             03 MCODICKA     PIC X.                                     00002020
             03 MCODICKC     PIC X.                                     00002030
             03 MCODICKP     PIC X.                                     00002040
             03 MCODICKH     PIC X.                                     00002050
             03 MCODICKV     PIC X.                                     00002060
             03 MCODICKO     PIC X(7).                                  00002070
             03 FILLER       PIC X(2).                                  00002080
             03 MCFAMKA      PIC X.                                     00002090
             03 MCFAMKC PIC X.                                          00002100
             03 MCFAMKP PIC X.                                          00002110
             03 MCFAMKH PIC X.                                          00002120
             03 MCFAMKV PIC X.                                          00002130
             03 MCFAMKO      PIC X(5).                                  00002140
             03 FILLER       PIC X(2).                                  00002150
             03 MCREFKA      PIC X.                                     00002160
             03 MCREFKC PIC X.                                          00002170
             03 MCREFKP PIC X.                                          00002180
             03 MCREFKH PIC X.                                          00002190
             03 MCREFKV PIC X.                                          00002200
             03 MCREFKO      PIC X(20).                                 00002210
             03 FILLER       PIC X(2).                                  00002220
             03 MNPRODA      PIC X.                                     00002230
             03 MNPRODC PIC X.                                          00002240
             03 MNPRODP PIC X.                                          00002250
             03 MNPRODH PIC X.                                          00002260
             03 MNPRODV PIC X.                                          00002270
             03 MNPRODO      PIC X(15).                                 00002280
             03 FILLER       PIC X(2).                                  00002290
             03 MCCOLORA     PIC X.                                     00002300
             03 MCCOLORC     PIC X.                                     00002310
             03 MCCOLORP     PIC X.                                     00002320
             03 MCCOLORH     PIC X.                                     00002330
             03 MCCOLORV     PIC X.                                     00002340
             03 MCCOLORO     PIC X(5).                                  00002350
             03 FILLER       PIC X(2).                                  00002360
             03 MCOPCOA      PIC X.                                     00002370
             03 MCOPCOC PIC X.                                          00002380
             03 MCOPCOP PIC X.                                          00002390
             03 MCOPCOH PIC X.                                          00002400
             03 MCOPCOV PIC X.                                          00002410
             03 MCOPCOO      PIC X(3).                                  00002420
             03 FILLER       PIC X(2).                                  00002430
             03 MDATCREA     PIC X.                                     00002440
             03 MDATCREC     PIC X.                                     00002450
             03 MDATCREP     PIC X.                                     00002460
             03 MDATCREH     PIC X.                                     00002470
             03 MDATCREV     PIC X.                                     00002480
             03 MDATCREO     PIC X(8).                                  00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MZONCMDA  PIC X.                                          00002510
           02 MZONCMDC  PIC X.                                          00002520
           02 MZONCMDP  PIC X.                                          00002530
           02 MZONCMDH  PIC X.                                          00002540
           02 MZONCMDV  PIC X.                                          00002550
           02 MZONCMDO  PIC X(15).                                      00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MLIBERRA  PIC X.                                          00002580
           02 MLIBERRC  PIC X.                                          00002590
           02 MLIBERRP  PIC X.                                          00002600
           02 MLIBERRH  PIC X.                                          00002610
           02 MLIBERRV  PIC X.                                          00002620
           02 MLIBERRO  PIC X(58).                                      00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MCODTRAA  PIC X.                                          00002650
           02 MCODTRAC  PIC X.                                          00002660
           02 MCODTRAP  PIC X.                                          00002670
           02 MCODTRAH  PIC X.                                          00002680
           02 MCODTRAV  PIC X.                                          00002690
           02 MCODTRAO  PIC X(4).                                       00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MCICSA    PIC X.                                          00002720
           02 MCICSC    PIC X.                                          00002730
           02 MCICSP    PIC X.                                          00002740
           02 MCICSH    PIC X.                                          00002750
           02 MCICSV    PIC X.                                          00002760
           02 MCICSO    PIC X(5).                                       00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MNETNAMA  PIC X.                                          00002790
           02 MNETNAMC  PIC X.                                          00002800
           02 MNETNAMP  PIC X.                                          00002810
           02 MNETNAMH  PIC X.                                          00002820
           02 MNETNAMV  PIC X.                                          00002830
           02 MNETNAMO  PIC X(8).                                       00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MSCREENA  PIC X.                                          00002860
           02 MSCREENC  PIC X.                                          00002870
           02 MSCREENP  PIC X.                                          00002880
           02 MSCREENH  PIC X.                                          00002890
           02 MSCREENV  PIC X.                                          00002900
           02 MSCREENO  PIC X(4).                                       00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MRFAMA    PIC X.                                          00002930
           02 MRFAMC    PIC X.                                          00002940
           02 MRFAMP    PIC X.                                          00002950
           02 MRFAMH    PIC X.                                          00002960
           02 MRFAMV    PIC X.                                          00002970
           02 MRFAMO    PIC X.                                          00002980
                                                                                
