      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE ESTKM VERSIONS ETAT STOCK MAGASIN      *        
      *----------------------------------------------------------------*        
       01  RVGA01CU.                                                            
           05  ESTKM-CTABLEG2    PIC X(15).                                     
           05  ESTKM-CTABLEG2-REDEF REDEFINES ESTKM-CTABLEG2.                   
               10  ESTKM-CVERSION        PIC X(10).                             
           05  ESTKM-WTABLEG     PIC X(80).                                     
           05  ESTKM-WTABLEG-REDEF  REDEFINES ESTKM-WTABLEG.                    
               10  ESTKM-SFAM            PIC X(05).                             
               10  ESTKM-STATUT          PIC X(04).                             
               10  ESTKM-FLAG            PIC X(06).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01CU-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ESTKM-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  ESTKM-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ESTKM-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  ESTKM-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
