      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ******************************************************************        
CT10  * 10/10/02 DSA056 REMPLACEMENT CUSINE PAR CUSINEVRAI                      
MAINT * 10/12/02 DSA057 REFONTE INTERFACE                                       
        01  TS-GA75.                                                            
      * ---------------------------------------  LONGUEUR TS      1851          
MAINT *     02 TS-GA75-LONG    PIC S9(5) COMP-3    VALUE +1851.                 
MAINT       02 TS-GA75-LONG PIC S9(5) COMP-3 VALUE +1972.                       
      * ----------------------------------  DONNEES                             
            02 TS-GA75-DONNEES.                                                 
      * DONNEES SECTEURS                                                        
               03 TS-GA75-DONNEES-SECTEURS.                                     
      * DONNEES SECTEURS DIDENTIFICATION                                        
                  04 TS-GA75-IDENT.                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
MAINT *              05 TS-GA75-STATUS PIC 9(3) COMP.                           
      *--                                                                       
                     05 TS-GA75-STATUS PIC 9(3) COMP-5.                         
      *}                                                                        
                     05 TS-GA75-NCODICK       PIC    X(07).                     
                     05 TS-GA75-NCODIC        PIC    X(07).                     
                     05 TS-GA75-CMARQ         PIC    X(05).                     
                     05 TS-GA75-LREFO         PIC    X(20).                     
                     05 TS-GA75-NEAN          PIC    X(15).                     
                     05 TS-GA75-NPROD         PIC    X(15).                     
                     05 TS-GA75-CCOLOR        PIC    X(05).                     
      * ----------------------------------------  LONGUEUR          74          
                  04 TS-GA75-GENERALES.                                         
                     05 TS-GA75-DCREATION     PIC    X(08).                     
                     05 TS-GA75-WMULTISOC     PIC    X(01).                     
                     05 TS-GA75-VERSION       PIC    X(01).                     
CT10  *              05 TS-GA75-CUSINE        PIC    X(05).                     
CT10                 05 TS-GA75-CUSINEVRAI    PIC    X(05).                     
                     05 TS-GA75-CORIGPROD     PIC    X(05).                     
      *              05 TS-GA75-CDESTPROD     PIC    X(05).                     
                     05 TS-GA75-CLIGPROD      PIC    X(05).                     
                     05 TS-GA75-LMODBASE      PIC    X(20).                     
                     05 TS-GA75-CFAM          PIC    X(05).                     
                     05 TS-GA75-LFAM          PIC    X(20).                     
                     05 TS-GA75-LREFFOURN     PIC    X(20).                     
                     05 TS-GA75-COPCO         PIC    X(03).                     
                     05 TS-GA75-LOPCO         PIC    X(10).                     
      * ----------------------------------------  LONGUEUR         127          
                  04 TS-GA75-CDESC.                                             
MAINT *              05 TS-GA75-CDESCRIP-LONG     PIC S9(5) COMP-3              
MAINT *                                           VALUE +1500.                  
MAINT                05 TS-GA75-CDESCRIP-LONG PIC S9(5) COMP-3                  
MAINT                                         VALUE +1560.                      
                     05 TS-GA75-CDESCRIP-DONNEES.                               
                        06 TS-GA75-GA53-TAB       OCCURS 30.                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
MAINT *                    10 TS-GA75-CDESTATUS PIC 9(3) COMP.                  
      *--                                                                       
                           10 TS-GA75-CDESTATUS PIC 9(3) COMP-5.                
      *}                                                                        
                           10 TS-GA75-CDESCRIPTIF PIC X(05).                    
                           10 TS-GA75-CVDESCRIPT  PIC X(05).                    
                           10 TS-GA75-LDESCRIPTIF PIC X(20).                    
                           10 TS-GA75-LVDESCRIPT  PIC X(20).                    
      * ----------------------------------------  LONGUEUR        1500          
CRO               04 TS-GA75-CDESTINATION.                                      
MAINT *              05 TS-GA75-CDEST-LONG     PIC S9(5) COMP-3                 
MAINT *                                           VALUE +150.                   
MAINT                05 TS-GA75-CDEST-LONG PIC S9(5) COMP-3 VALUE +210.         
                     05 TS-GA75-CDESCRIP-DONNEES.                               
                        06 TS-GA75-GA53-TAB       OCCURS 30.                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
MAINT *                    10 TS-GA75-DESSTATUS PIC 9(3) COMP.                  
      *--                                                                       
                           10 TS-GA75-DESSTATUS PIC 9(3) COMP-5.                
      *}                                                                        
                           10 TS-GA75-CDEST       PIC X(05).                    
      * ----------------------------------------  LONGUEUR        150           
                                                                                
