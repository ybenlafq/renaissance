      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * COMMAREA SPECIFIQUE PROG TGA37                             *            
      *       TR :  GA37 GESTION DE L'ECOTAXE                      *            
      *       PG : TGA37 GESTION DE L'ECOTAXE                      *            
      **************************************************************            
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-GA37-LONG-COMMAREA            PIC S9(4) COMP VALUE +4096.        
      *--                                                                       
       01  COM-GA37-LONG-COMMAREA            PIC S9(4) COMP-5 VALUE             
                                                                  +4096.        
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA ----------------------------------- 100          
          02 FILLER-COM-AIDA                  PIC X(100).                       
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020          
          02 COMM-CICS-APPLID                 PIC X(8).                         
          02 COMM-CICS-NETNAM                 PIC X(8).                         
          02 COMM-CICS-TRANSA                 PIC X(4).                         
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100          
          02 COMM-DATE-SIECLE                 PIC XX.                           
          02 COMM-DATE-ANNEE                  PIC XX.                           
          02 COMM-DATE-MOIS                   PIC XX.                           
          02 COMM-DATE-JOUR                   PIC XX.                           
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
          02 COMM-DATE-QNTA                   PIC 999.                          
          02 COMM-DATE-QNT0                   PIC 99999.                        
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
          02 COMM-DATE-BISX                   PIC 9.                            
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                   
          02 COMM-DATE-JSM                    PIC 9.                            
      *   LIBELLES DU JOUR COURT - LONG                                         
          02 COMM-DATE-JSM-LC                 PIC XXX.                          
          02 COMM-DATE-JSM-LL                 PIC XXXXXXXX.                     
      *   LIBELLES DU MOIS COURT - LONG                                         
          02 COMM-DATE-MOIS-LC                PIC XXX.                          
          02 COMM-DATE-MOIS-LL                PIC XXXXXXXX.                     
      *   DIFFERENTES FORMES DE DATE                                            
          02 COMM-DATE-SSAAMMJJ               PIC X(8).                         
          02 COMM-DATE-AAMMJJ                 PIC X(6).                         
          02 COMM-DATE-JJMMSSAA               PIC X(8).                         
          02 COMM-DATE-JJMMAA                 PIC X(6).                         
          02 COMM-DATE-JJ-MM-AA               PIC X(8).                         
          02 COMM-DATE-JJ-MM-SSAA             PIC X(10).                        
      *   DIFFERENTES FORMES DE DATE                                            
          02 COMM-DATE-FILLER                 PIC X(14).                        
      *                                                                         
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 322          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS                   PIC S9(4) COMP VALUE -1.          
      *--                                                                       
          02 COMM-SWAP-CURS                   PIC S9(4) COMP-5 VALUE -1.        
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 370        PIC X(1).                         
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3574          
      *                                                                         
          02 COMM-GA37-APPLI.                                                   
      *                                                                         
             03 COMM-GA37-SAV-DONNEES.                                          
                04 COMM-GA37-CFAM             PIC X(5).                         
                04 COMM-GA37-CECO             PIC X(3).                         
                04 COMM-GA37-LCECO            PIC X(10).                        
                04 COMM-GA37-WACHVEN          PIC X.                            
                04 COMM-GA37-DEFFET           PIC X(8).                         
                04 COMM-GA37-CPAYS            PIC X(2).                         
                04 COMM-GA37-MONTANT          PIC X(8).                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GA37-NB-PAGES            PIC S9(3)  COMP.                  
      *--                                                                       
             03 COMM-GA37-NB-PAGES            PIC S9(3) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GA37-NUM-PAGE            PIC S9(3)  COMP.                  
      *--                                                                       
             03 COMM-GA37-NUM-PAGE            PIC S9(3) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GA37-TS-RANG             PIC S9(7)  COMP OCCURS 15.        
      *--                                                                       
             03 COMM-GA37-TS-RANG             PIC S9(7) COMP-5 OCCURS           
                                                                     15.        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GA37-TS-NBLIGNES         PIC S9(7)  COMP.                  
      *--                                                                       
             03 COMM-GA37-TS-NBLIGNES         PIC S9(7) COMP-5.                 
      *}                                                                        
             03 COMM-GA37-FLAG.                                                 
      *------------------------------ FLAG SUR LA LIGNE DE SAISIE               
                04 COMM-GA37-FLAG-TYPE-TMT    PIC 9.                            
                   88 COMM-GA37-VIDE                     VALUE 0.               
                   88 COMM-GA37-PAGINATION               VALUE 1.               
                   88 COMM-GA37-FILTRE                   VALUE 2.               
                   88 COMM-GA37-CREATION                 VALUE 3.               
                   88 COMM-GA37-LS-MAJ                   VALUE 4.               
      *------------------------------ FLAG SUR LE CODE ACTION                   
                04 COMM-GA37-FLAG-ACTION      PIC 9.                            
                   88 COMM-GA37-NORMAL                   VALUE 0.               
                   88 COMM-GA37-HISTORIQUE               VALUE 1.               
                   88 COMM-GA37-SUPPRESSION              VALUE 2.               
      *------------------------------ FLAG MISE A JOUR DONNEES ECRAN            
                04 COMM-GA37-FLAG-MAJ         PIC 9.                            
                   88 COMM-GA37-PAS-MAJ                  VALUE 0.               
                   88 COMM-GA37-MAJ                      VALUE 1.               
      *------------------------------ ZONE LIBRE                                
             03 COMM-GA37-LIBRE               PIC X(3394).                      
      *                                                                         
      *****************************************************************         
                                                                                
