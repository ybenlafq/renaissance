      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : MISE A JOUR DU REFERENTIEL NATIONAL    (GN12)          *         
      *        PAGINATION DES PRIX DE VENTE DE REFERENCE              *         
      ******************************************************************        
      * DE02005 23/06/08 SUPPORT MAINTENANCE                                    
      *                  ZONES MARGES TROP PETITES                              
      ******************************************************************        
       01  TS-GN12.                                                             
      *----------------------------------  LONGUEUR TS                          
      *    02 TS-GN12-LONG                 PIC S9(03) COMP-3 VALUE +89.         
           02 TS-GN12-LONG PIC S9(03) COMP-3 VALUE +93.                         
      *----------------------------------  DONNEES  TS                          
           02 TS-GN12-DONNEES.                                                  
              03 TS-GN12-ACT2                PIC X(1).                          
              03 TS-GN12-PVR                 PIC S9(5)V99.                      
              03 TS-GN12-WIMPER              PIC X.                             
              03 TS-GN12-CIMPER              PIC X.                             
              03 TS-GN12-DPVR                PIC X(8).                          
              03 TS-GN12-DPVR-AV             PIC X(8).                          
              03 TS-GN12-COMM                PIC X(10).                         
              03 TS-GN12-TMB                 PIC S9(2)V9.                       
      *       03 TS-GN12-MB                  PIC S9(3)V99.                      
              03 TS-GN12-MB                  PIC S9(5)V99.                      
              03 TS-GN12-ACT3                PIC X(1).                          
              03 TS-GN12-PVR2                PIC S9(5)V99.                      
              03 TS-GN12-CBLOQ               PIC X.                             
              03 TS-GN12-DPVR2               PIC X(8).                          
              03 TS-GN12-DPVR2-AV            PIC X(8).                          
              03 TS-GN12-COMM2               PIC X(10).                         
              03 TS-GN12-TMB2                PIC S9(2)V9.                       
      *       03 TS-GN12-MB2                 PIC S9(3)V99.                      
              03 TS-GN12-MB2                 PIC S9(5)V99.                      
      *----------------------------------  BOOLEEN  DE MODIF                    
      *----------------------------------  '1' MODIFIER, '0' NON MODIF          
      *----------------------------------  '2' WIMPER MODIFIER � OUI            
              03 TS-GN12-MODIF-REF1          PIC X(1).                          
      *----------------------------------  BOOLEEN  DE MODIF                    
      *----------------------------------  '1' MODIFIER, '0' NON MODIF          
      *----------------------------------  '2' WBLOQ  MODIFIER � OUI            
              03 TS-GN12-MODIF-REF2          PIC X(1).                          
                                                                                
