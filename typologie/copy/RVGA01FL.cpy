      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE OCRED CODES ORGANISMES DE CREDIT       *        
      *----------------------------------------------------------------*        
       01  RVGA01FL.                                                            
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  OCRED-CTABLEG2    PIC X(15).                                     
           05  OCRED-CTABLEG2-REDEF REDEFINES OCRED-CTABLEG2.                   
               10  OCRED-CCRED           PIC X(05).                             
           05  OCRED-WTABLEG     PIC X(80).                                     
           05  OCRED-WTABLEG-REDEF  REDEFINES OCRED-WTABLEG.                    
               10  OCRED-LIBCRED         PIC X(20).                             
               10  OCRED-DARTY           PIC X(01).                             
               10  OCRED-CODE36          PIC X(01).                             
               10  OCRED-RG              PIC X(02).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  OCRED-RG-N           REDEFINES OCRED-RG                      
                                         PIC 9(02).                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  OCRED-W               PIC X(01).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01FL-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  OCRED-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  OCRED-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  OCRED-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  OCRED-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
