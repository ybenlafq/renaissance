      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE BAERR ERREURS D ENCAISSEMENT DES BA    *        
      *----------------------------------------------------------------*        
       01  RVGA01K9.                                                            
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  BAERR-CTABLEG2    PIC X(15).                                     
           05  BAERR-CTABLEG2-REDEF REDEFINES BAERR-CTABLEG2.                   
               10  BAERR-NECART          PIC X(02).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  BAERR-NECART-N       REDEFINES BAERR-NECART                  
                                         PIC 9(02).                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  BAERR-WTABLEG     PIC X(80).                                     
           05  BAERR-WTABLEG-REDEF  REDEFINES BAERR-WTABLEG.                    
               10  BAERR-WACTIF          PIC X(01).                             
               10  BAERR-LECART          PIC X(20).                             
               10  BAERR-WMODRGLT        PIC X(01).                             
               10  BAERR-WANCBA          PIC X(01).                             
               10  BAERR-WECS            PIC X(02).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01K9-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BAERR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  BAERR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BAERR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  BAERR-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
