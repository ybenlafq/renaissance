      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HEGAR MODES DE GARANTIE GHE            *        
      *----------------------------------------------------------------*        
       01  RVGA01KC.                                                            
           05  HEGAR-CTABLEG2    PIC X(15).                                     
           05  HEGAR-CTABLEG2-REDEF REDEFINES HEGAR-CTABLEG2.                   
               10  HEGAR-CGARANTI        PIC X(05).                             
           05  HEGAR-WTABLEG     PIC X(80).                                     
           05  HEGAR-WTABLEG-REDEF  REDEFINES HEGAR-WTABLEG.                    
               10  HEGAR-WACTIF          PIC X(01).                             
               10  HEGAR-LGARANTI        PIC X(20).                             
               10  HEGAR-CMOTIF          PIC X(10).                             
               10  HEGAR-WQTEMUL         PIC X(01).                             
               10  HEGAR-WNHSMUL         PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01KC-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HEGAR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HEGAR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HEGAR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HEGAR-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
