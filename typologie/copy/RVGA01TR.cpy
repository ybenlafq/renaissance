      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EUROP MONNAIES EN COURS                *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01TR.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01TR.                                                            
      *}                                                                        
           05  EUROP-CTABLEG2    PIC X(15).                                     
           05  EUROP-CTABLEG2-REDEF REDEFINES EUROP-CTABLEG2.                   
               10  EUROP-CDEVISE         PIC X(03).                             
               10  EUROP-WMONNAIE        PIC X(01).                             
           05  EUROP-WTABLEG     PIC X(80).                                     
           05  EUROP-WTABLEG-REDEF  REDEFINES EUROP-WTABLEG.                    
               10  EUROP-DDEBUT          PIC X(08).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  EUROP-DDEBUT-N       REDEFINES EUROP-DDEBUT                  
                                         PIC 9(08).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  EUROP-DFIN            PIC X(08).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  EUROP-DFIN-N         REDEFINES EUROP-DFIN                    
                                         PIC 9(08).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01TR-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01TR-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EUROP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EUROP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EUROP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EUROP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
