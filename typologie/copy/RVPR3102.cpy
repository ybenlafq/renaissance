      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVPR3102                      *        
      ******************************************************************        
       01  RVPR3102.                                                            
           10 PR31-NCHRONO         PIC X(7).                                    
           10 PR31-NLIGNE          PIC X(3).                                    
           10 PR31-NVENTE          PIC X(7).                                    
           10 PR31-CTITRENOM       PIC X(5).                                    
           10 PR31-LNOM            PIC X(25).                                   
           10 PR31-LPRENOM         PIC X(15).                                   
           10 PR31-MTPRIME         PIC S9(7)V9(2) USAGE COMP-3.                 
           10 PR31-GSM             PIC X(11).                                   
           10 PR31-IMEI            PIC X(15).                                   
           10 PR31-ICCIDNSCE       PIC X(20).                                   
           10 PR31-INITIALES       PIC X(2).                                    
           10 PR31-WDOSSIER        PIC X(1).                                    
           10 PR31-WBONCDE         PIC X(1).                                    
           10 PR31-WPIDENT         PIC X(1).                                    
           10 PR31-WAR             PIC X(1).                                    
           10 PR31-DAR             PIC X(8).                                    
           10 PR31-MTRGLT          PIC S9(7)V9(2) USAGE COMP-3.                 
           10 PR31-DRGLT           PIC X(8).                                    
           10 PR31-DSYST           PIC S9(13)V USAGE COMP-3.                    
           10 PR31-DVENTE          PIC X(8).                                    
           10 PR31-DARI            PIC X(8).                                    
           10 PR31-DCREATION       PIC X(8).                                    
           10 PR31-DMODIF          PIC X(8).                                    
           10 PR31-NUMABO          PIC X(10).                                   
           10 PR31-WREFUS          PIC X(2).                                    
           10 PR31-WCONTROLE       PIC X(1).                                    
           10 PR31-WSUP            PIC X(1).                                    
           10 PR31-MTRGLA         PIC S9(7)V9(2) USAGE COMP-3.                  
           10 PR31-MTRGLB         PIC S9(7)V9(2) USAGE COMP-3.                  
           10 PR31-MTRGLC         PIC S9(7)V9(2) USAGE COMP-3.                  
           10 PR31-MTRGLD         PIC S9(7)V9(2) USAGE COMP-3.                  
           10 PR31-MTRGLE         PIC S9(7)V9(2) USAGE COMP-3.                  
           10 PR31-MTRGLF         PIC S9(7)V9(2) USAGE COMP-3.                  
           10 PR31-MTRGLG         PIC S9(7)V9(2) USAGE COMP-3.                  
           10 PR31-MTRGLH         PIC S9(7)V9(2) USAGE COMP-3.                  
           10 PR31-MTRGLI         PIC S9(7)V9(2) USAGE COMP-3.                  
           10 PR31-MTRGLJ         PIC S9(7)V9(2) USAGE COMP-3.                  
           10 PR31-NSEQNQ         PIC S9(5) USAGE COMP-3.                       
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 26      *        
      ******************************************************************        
                                                                                
