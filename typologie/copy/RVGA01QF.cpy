      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE QCEXA ENTITES D EXCEPTION D ANALYSE    *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01QF.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01QF.                                                            
      *}                                                                        
           05  QCEXA-CTABLEG2    PIC X(15).                                     
           05  QCEXA-CTABLEG2-REDEF REDEFINES QCEXA-CTABLEG2.                   
               10  QCEXA-CENTITE         PIC X(05).                             
               10  QCEXA-NSEQ            PIC X(02).                             
           05  QCEXA-WTABLEG     PIC X(80).                                     
           05  QCEXA-WTABLEG-REDEF  REDEFINES QCEXA-WTABLEG.                    
               10  QCEXA-WACTIF          PIC X(01).                             
               10  QCEXA-NCISOC          PIC X(03).                             
               10  QCEXA-NSOCLIEU        PIC X(06).                             
               10  QCEXA-CTCARTE         PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01QF-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01QF-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QCEXA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  QCEXA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QCEXA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  QCEXA-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
