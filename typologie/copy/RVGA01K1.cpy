      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HEREF CODES REFUS GHE                  *        
      *----------------------------------------------------------------*        
       01  RVGA01K1.                                                            
           05  HEREF-CTABLEG2    PIC X(15).                                     
           05  HEREF-CTABLEG2-REDEF REDEFINES HEREF-CTABLEG2.                   
               10  HEREF-CREFUS          PIC X(05).                             
           05  HEREF-WTABLEG     PIC X(80).                                     
           05  HEREF-WTABLEG-REDEF  REDEFINES HEREF-WTABLEG.                    
               10  HEREF-WACTIF          PIC X(01).                             
               10  HEREF-LREFUS          PIC X(35).                             
               10  HEREF-WCHCODIC        PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01K1-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HEREF-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HEREF-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HEREF-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HEREF-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
