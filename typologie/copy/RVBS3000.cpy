      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVBS3000                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVBS3000                 00000060
      *   CLE UNIQUE : CPROFASS                                         00000070
      *---------------------------------------------------------        00000080
      *                                                                 00000090
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBS3000.                                                    00000100
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBS3000.                                                            
      *}                                                                        
           10 BS30-CPROFASS        PIC X(5).                            00000110
           10 BS30-LPROFASS        PIC X(20).                           00000120
           10 BS30-LGCTYPENT       PIC X(1).                            00000130
           10 BS30-LVCTYPENT       PIC X(2).                            00000140
           10 BS30-LGWREMAUT       PIC X(1).                            00000150
           10 BS30-LVWREMAUT       PIC X(1).                            00000160
           10 BS30-LGCCALCUL       PIC X(1).                            00000170
           10 BS30-LVCCALCUL       PIC X(5).                            00000180
           10 BS30-WACTIF          PIC X(1).                            00000190
           10 BS30-NSEQ            PIC S9(3) COMP-3.                    00000200
           10 BS30-CHEFPROD        PIC X(5).                            00000210
           10 BS30-DSYST           PIC S9(13) COMP-3.                   00000220
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00000230
      *---------------------------------------------------------        00000240
      *   LISTE DES FLAGS DE LA TABLE RVBS3000                          00000250
      *---------------------------------------------------------        00000260
      *                                                                 00000270
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBS3000-FLAGS.                                              00000280
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBS3000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS30-CPROFASS-F      PIC S9(4) COMP.                      00000290
      *--                                                                       
           10 BS30-CPROFASS-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS30-LPROFASS-F      PIC S9(4) COMP.                      00000300
      *--                                                                       
           10 BS30-LPROFASS-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS30-LGCTYPENT-F     PIC S9(4) COMP.                      00000310
      *--                                                                       
           10 BS30-LGCTYPENT-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS30-LVCTYPENT-F     PIC S9(4) COMP.                      00000320
      *--                                                                       
           10 BS30-LVCTYPENT-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS30-LGWREMAUT-F     PIC S9(4) COMP.                      00000330
      *--                                                                       
           10 BS30-LGWREMAUT-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS30-LVWREMAUT-F     PIC S9(4) COMP.                      00000340
      *--                                                                       
           10 BS30-LVWREMAUT-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS30-LGCCALCUL-F     PIC S9(4) COMP.                      00000350
      *--                                                                       
           10 BS30-LGCCALCUL-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS30-LVCCALCUL-F     PIC S9(4) COMP.                      00000360
      *--                                                                       
           10 BS30-LVCCALCUL-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS30-WACTIF-F        PIC S9(4) COMP.                      00000370
      *--                                                                       
           10 BS30-WACTIF-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS30-NSEQ-F          PIC S9(4) COMP.                      00000380
      *--                                                                       
           10 BS30-NSEQ-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS30-CHEFPROD-F      PIC S9(4) COMP.                      00000390
      *--                                                                       
           10 BS30-CHEFPROD-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS30-DSYST-F         PIC S9(4) COMP.                      00000400
      *                                                                         
      *--                                                                       
           10 BS30-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
