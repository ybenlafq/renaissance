      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00010001
      *   COPY MESSAGE MQ REMONTEE ENCAISSEMENT                         00020001
      *   VERSION AVEC ZONES NMD POUR MAGASIN B&S                       00030001
      ***************************************************************** 00040001
      *  MODIFICATION                                                 * 00050001
      *  DSA008     : DA04  26/03/2008                                * 00060001
      *               AJOUT DES ZONES POUR RTNV20 + VERSION           * 00070001
      ***************************************************************** 00080001
      *                                                                 00090001
       01  WS-MQ10.                                                     00100001
         02 WS-QUEUE.                                                   00110001
               10   MQ10-MSGID     PIC    X(24).                        00120001
               10   MQ10-CORRELID  PIC    X(24).                        00130001
         02 WS-CODRET              PIC    XX.                           00140001
         02  MESSAGE45.                                                 00150001
           03 MESSAGE45-ENTETE.                                         00160001
               05   MES45-TYPE     PIC    X(3).                         00170001
               05   MES45-NSOCMSG  PIC    X(3).                         00180001
               05   MES45-NLIEUMSG PIC    X(3).                         00190001
               05   MES45-NSOCDST  PIC    X(3).                         00200001
               05   MES45-NLIEUDST PIC    X(3).                         00210001
               05   MES45-NORD     PIC    9(8).                         00220001
               05   MES45-LPROG    PIC    X(10).                        00230001
               05   MES45-DJOUR    PIC    X(8).                         00240001
               05   MES45-WSID     PIC    X(10).                        00250001
               05   MES45-USER     PIC    X(10).                        00260001
               05   MES45-CHRONO   PIC    9(7).                         00270001
               05   MES45-NBRMSG   PIC    9(7).                         00280001
DA04  *        05   MES45-FILLER   PIC    X(30).                        00290001
DA04           05   MES45-NBROCC   PIC    X(05).                        00300001
DA04           05   MES45-LGNOCC   PIC    X(05).                        00310001
DA04           05   MES45-VERSION  PIC    X(02).                        00320001
               05   MES45-FILLER   PIC    X(18).                        00330001
                                                                        00340001
         02 MESSAGE45-DATA.                                             00350001
           05  MES45-NSOCVTE       PIC    X(3).                         00360001
           05  MES45-NLIEUVTE      PIC    X(3).                         00370001
           05  MES45-NVENTE        PIC    X(7).                         00380001
           05  MES45-TYPVTE        PIC    X(1).                         00390001
           05  MES45-NB-GV10       PIC    9(3).                         00400001
           05  MES45-NB-GV02       PIC    9(3).                         00410001
           05  MES45-NB-GV11       PIC    9(3).                         00420001
           05  MES45-NB-GV05       PIC    9(3).                         00430001
           05  MES45-NB-GV06       PIC    9(3).                         00440001
           05  MES45-NB-GV08       PIC    9(3).                         00450001
           05  MES45-NB-GV14       PIC    9(3).                         00460001
NMD        05  MES45-WINDMOD       PIC    X(1).                         00470001
           05  MES45-WDGRAD        PIC    X(1).                         00480001
           05  MES-TABLES.                                              00490001
      *-- MES-TABLES-DATAS CONTIENT TOUS LES ENREGS SUIVANTS :          00500001
      *-- LONGUEUR DES TABLES = LG + 4 CAR POUR LE NOM SOIT  :          00510001
      *-->RTGV10 : 1 OCCURENCE = 426 CAR                ==>  426        00520001
      *-->RTGV02 : 1 OCCURENCE = 453 CAR MAXI * 4 OCC   ==> 1812        00530001
      *-->RTGV11 : 1 OCCURENCE = 366 CAR MAXI *80 OCC   ==>29280        00540001
      *-->RTGV05 : 1 OCCURENCE =  68 CAR MAXI * 5 OCC   ==>  340        00550001
      *-->RTGV06 : 1 OCCURENCE =  31 CAR MAXI *10 OCC   ==>  310        00560001
      *-->RTGV08 : 1 OCCURENCE = 103 CAR MAXI *20 OCC   ==> 2060        00570001
DA04  *-->RTNV20 : 1 OCCURENCE =  92 CAR MAXI *80 OCC   ==> 7360        00580001
      *-->RTGV14 : 1 OCCURENCE =  79 CAR MAXI *100 OCC  ==> 7900        00590001
      *-->TOTAL TABLES RTGV*                            ==>49488        00600001
DA04  *-- FILLER DE RESERVE  ANCIEN                     ==> 7713        00610001
      *-- FILLER DE RESERVE  NOUVEAU                    ==>  353        00620001
      *-->TOTAL GENERALE TABLES + FILLER                ==>49841        00630001
      *                                                                 00640001
             10 MES-TABLES-DATAS  PIC    X(49841).                      00650001
      *----------------FIN MESSAGE---LONGUEUR TOTALE = 50000 CAR-----*  00660001
      *------DESCRIPTION DES TABLES-DU MESSAGE-----------------------*  00670001
       01 MSG.                                                          00680001
      *------DESCRIPTION TABLE RVGV1005 OU RVVE1000--LG=426 CAR------*  00690001
          02 GV10.                                                      00700001
             05 MGV10-NTAB    PIC X(04).                                00710001
             05 MGV10-NSOC    PIC X(03).                                00720001
             05 MGV10-NLIE    PIC X(03).                                00730001
             05 MGV10-NVTE    PIC X(07).                                00740001
             05 MGV10-NORD    PIC X(05).                                00750001
             05 MGV10-NCLI    PIC X(09).                                00760001
             05 MGV10-DVTE    PIC X(08).                                00770001
             05 MGV10-DHVT    PIC X(02).                                00780001
             05 MGV10-DMVT    PIC X(02).                                00790001
             05 MGV10-PTTV    PIC S9(7)V99 COMP-3.                      00800001
             05 MGV10-PVER    PIC S9(7)V99 COMP-3.                      00810001
             05 MGV10-PCOM    PIC S9(7)V99 COMP-3.                      00820001
             05 MGV10-PLIV    PIC S9(7)V99 COMP-3.                      00830001
             05 MGV10-PDIF    PIC S9(7)V99 COMP-3.                      00840001
             05 MGV10-PFAC    PIC S9(7)V99 COMP-3.                      00850001
             05 MGV10-CREV    PIC X(05).                                00860001
             05 MGV10-PREM    PIC S9(7)V99 COMP-3.                      00870001
             05 MGV10-LCV1    PIC X(30).                                00880001
             05 MGV10-LCV2    PIC X(30).                                00890001
             05 MGV10-LCV3    PIC X(30).                                00900001
             05 MGV10-LCV4    PIC X(30).                                00910001
             05 MGV10-DMOD    PIC X(08).                                00920001
             05 MGV10-WFAC    PIC X(01).                                00930001
             05 MGV10-WEXP    PIC X(01).                                00940001
             05 MGV10-WDET    PIC X(01).                                00950001
             05 MGV10-WDXH    PIC X(01).                                00960001
             05 MGV10-CORG    PIC X(05).                                00970001
             05 MGV10-CMPA    PIC X(05).                                00980001
             05 MGV10-LDE1    PIC X(30).                                00990001
             05 MGV10-LDE2    PIC X(30).                                01000001
             05 MGV10-DLIV    PIC X(08).                                01010001
             05 MGV10-NFOL    PIC X(03).                                01020001
             05 MGV10-LAUT    PIC X(05).                                01030001
             05 MGV10-NAUT    PIC X(05).                                01040001
             05 MGV10-DSYST   PIC S9(13) COMP-3.                        01050001
             05 MGV10-DSTAT   PIC X(04).                                01060001
             05 MGV10-DFACT   PIC X(08).                                01070001
             05 MGV10-CACID   PIC X(08).                                01080001
             05 MGV10-NSOCM   PIC X(03).                                01090001
             05 MGV10-NLIEM   PIC X(03).                                01100001
             05 MGV10-NSOCP   PIC X(03).                                01110001
             05 MGV10-NLIEP   PIC X(03).                                01120001
             05 MGV10-CDEVI   PIC X(03).                                01130001
             05 MGV10-CFCRE   PIC X(05).                                01140001
             05 MGV10-NCRED   PIC X(14).                                01150001
             05 MGV10-NSEQN   PIC S9(5) COMP-3.                         01160001
             05 MGV10-TYPVT   PIC X(01).                                01170001
             05 MGV10-VTEGP   PIC X(01).                                01180001
             05 MGV10-CTRMR   PIC X(08).                                01190001
             05 MGV10-DATEN   PIC X(08).                                01200001
             05 MGV10-WDGRA   PIC X(01).                                01210001
             05 MGV10-NAUTO   PIC X(20).                                01220001
             05 MGV10-NSEQE   PIC S9(5) COMP-3.                         01230001
             05 MGV10-NSOCO   PIC X(03).                                01240001
             05 MGV10-NLIEUO  PIC X(03).                                01250001
             05 MGV10-NVENTO  PIC X(08).                                01260001
      *------DESCRIPTION TABLE RVGV0202 OU RVVE0200--LG=453 CAR------*  01270001
          02 GV02.                                                      01280001
             05 MGV02-NTAB    PIC X(04).                                01290001
             05 MGV02-NSOC    PIC X(03).                                01300001
             05 MGV02-NLIEU   PIC X(03).                                01310001
             05 MGV02-NORD    PIC X(05).                                01320001
             05 MGV02-NVTE    PIC X(07).                                01330001
             05 MGV02-WTYPAD  PIC X(01).                                01340001
             05 MGV02-CTRNOM  PIC X(05).                                01350001
             05 MGV02-LNOM    PIC X(25).                                01360001
             05 MGV02-LPRENOM PIC X(15).                                01370001
             05 MGV02-LBAT    PIC X(03).                                01380001
             05 MGV02-LESC    PIC X(03).                                01390001
             05 MGV02-LETAG   PIC X(03).                                01400001
             05 MGV02-LPORT   PIC X(03).                                01410001
             05 MGV02-LCMPAD1 PIC X(32).                                01420001
             05 MGV02-LCMPAD2 PIC X(32).                                01430001
             05 MGV02-CVOIE   PIC X(05).                                01440001
             05 MGV02-CTVOIE  PIC X(04).                                01450001
             05 MGV02-LNVOIE  PIC X(21).                                01460001
             05 MGV02-LCOMMUN PIC X(32).                                01470001
             05 MGV02-CPOST   PIC X(05).                                01480001
             05 MGV02-LBUR    PIC X(26).                                01490001
             05 MGV02-TELDOM  PIC X(10).                                01500001
             05 MGV02-TELBUR  PIC X(10).                                01510001
             05 MGV02-LPOSTB  PIC X(05).                                01520001
             05 MGV02-LCLIV1  PIC X(78).                                01530001
             05 MGV02-LCLIV2  PIC X(78).                                01540001
             05 MGV02-WETRAN  PIC X(01).                                01550001
             05 MGV02-CZONL   PIC X(05).                                01560001
             05 MGV02-CINSEE  PIC X(05).                                01570001
             05 MGV02-WCONTR  PIC X(01).                                01580001
             05 MGV02-DSYST   PIC S9(13) COMP-3.                        01590001
             05 MGV02-CILOT   PIC X(08).                                01600001
             05 MGV02-IDCLI   PIC X(08).                                01610001
             05 MGV02-CPAYS   PIC X(03).                                01620001
             05 MGV02-NGSM    PIC X(15).                                01630001
      *------DESCRIPTION TABLE RVGV1106 OU RVVE1100--LG=417 CAR------*  01640001
          02 GV11.                                                      01650001
             05 MGV11-NTAB    PIC X(04).                                01660001
             05 MGV11-NSOC    PIC X(03).                                01670001
             05 MGV11-NLIEU   PIC X(03).                                01680001
             05 MGV11-NVTE    PIC X(07).                                01690001
             05 MGV11-CTYPENR PIC X(01).                                01700001
             05 MGV11-NCGRP   PIC X(07).                                01710001
             05 MGV11-NCODIC  PIC X(07).                                01720001
             05 MGV11-NSEQ    PIC X(02).                                01730001
             05 MGV11-CMODEL  PIC X(03).                                01740001
             05 MGV11-DDELIV  PIC X(08).                                01750001
             05 MGV11-NORDRE  PIC X(05).                                01760001
             05 MGV11-CENREG  PIC X(05).                                01770001
             05 MGV11-QVENDU  PIC S9(5) COMP-3.                         01780001
             05 MGV11-PVUNIT  PIC S9(7)V99 COMP-3.                      01790001
             05 MGV11-PVUNITF PIC S9(7)V99 COMP-3.                      01800001
             05 MGV11-PVTOT   PIC S9(7)V99 COMP-3.                      01810001
             05 MGV11-CEQUIP  PIC X(05).                                01820001
             05 MGV11-NLIGNE  PIC X(02).                                01830001
             05 MGV11-TXTVA   PIC S9(3)V99 COMP-3.                      01840001
             05 MGV11-QCONDT  PIC S9(5) COMP-3.                         01850001
             05 MGV11-PRMP    PIC S9(7)V99 COMP-3.                      01860001
             05 MGV11-WEMP    PIC X(01).                                01870001
             05 MGV11-CPLAG   PIC X(02).                                01880001
             05 MGV11-CPROT   PIC X(05).                                01890001
             05 MGV11-CADRT   PIC X(01).                                01900001
             05 MGV11-CPOST   PIC X(05).                                01910001
             05 MGV11-LCOMM   PIC X(35).                                01920001
             05 MGV11-CVEND   PIC X(06).                                01930001
             05 MGV11-NSOCL   PIC X(03).                                01940001
             05 MGV11-NDEPOT  PIC X(03).                                01950001
             05 MGV11-NAUTOR  PIC X(05).                                01960001
             05 MGV11-WARTIN  PIC X(01).                                01970001
             05 MGV11-WEDITBL PIC X(01).                                01980001
             05 MGV11-WACOMM  PIC X(01).                                01990001
             05 MGV11-WCQRSF  PIC X(01).                                02000001
             05 MGV11-NMUTAT  PIC X(07).                                02010001
             05 MGV11-CTOURN  PIC X(08).                                02020001
             05 MGV11-WTLIVR  PIC X(01).                                02030001
             05 MGV11-DCOMPTA PIC X(08).                                02040001
             05 MGV11-DCREAT  PIC X(08).                                02050001
             05 MGV11-HCREAT  PIC X(04).                                02060001
             05 MGV11-DANNUL  PIC X(08).                                02070001
             05 MGV11-NLIEUO  PIC X(03).                                02080001
             05 MGV11-WIMAJ   PIC X(01).                                02090001
             05 MGV11-DSYST   PIC S9(13) COMP-3.                        02100001
             05 MGV11-DSTAT   PIC X(04).                                02110001
             05 MGV11-CPRESTG PIC X(05).                                02120001
             05 MGV11-NSOCM   PIC X(03).                                02130001
             05 MGV11-NLIEUM  PIC X(03).                                02140001
             05 MGV11-DATENC  PIC X(08).                                02150001
             05 MGV11-CDEV    PIC X(03).                                02160001
             05 MGV11-WUNITR  PIC X(20).                                02170001
             05 MGV11-LRCMMT  PIC X(10).                                02180001
             05 MGV11-NSOCP   PIC X(03).                                02190001
             05 MGV11-NLIEUP  PIC X(03).                                02200001
             05 MGV11-NTRANS  PIC S9(8) COMP-3.                         02210001
             05 MGV11-NSEQFV  PIC X(02).                                02220001
             05 MGV11-NSEQNQ  PIC S9(5) COMP-3.                         02230001
             05 MGV11-NSEQREF PIC S9(5) COMP-3.                         02240001
             05 MGV11-NMODIF  PIC S9(5) COMP-3.                         02250001
             05 MGV11-NSOCO   PIC X(03).                                02260001
             05 MGV11-DTOPE   PIC X(08).                                02270001
             05 MGV11-NSOC1   PIC X(03).                                02280001
             05 MGV11-NDEP1   PIC X(03).                                02290001
             05 MGV11-NSOCG   PIC X(03).                                02300001
             05 MGV11-NLIEUG  PIC X(03).                                02310001
             05 MGV11-NSOCDL  PIC X(03).                                02320001
             05 MGV11-NLIEUDL PIC X(03).                                02330001
             05 MGV11-TYPVTE  PIC X(01).                                02340001
             05 MGV11-CTYPENT PIC X(02).                                02350001
             05 MGV11-NLIEN   PIC S9(5) COMP-3.                         02360001
             05 MGV11-NACTVTE PIC S9(5) COMP-3.                         02370001
             05 MGV11-PVCODIG PIC S9(7)V99 COMP-3.                      02380001
             05 MGV11-QTCODIG PIC S9(5) COMP-3.                         02390001
             05 MGV11-DPRLG   PIC X(08).                                02400001
             05 MGV11-CALERT  PIC X(05).                                02410001
             05 MGV11-CREPR   PIC X(05).                                02420001
             05 MGV11-NSEQENS PIC S9(5) COMP-3.                         02430001
             05 MGV11-MPRIMCLI PIC S9(7)V99 COMP-3.                     02440001
      *------DESCRIPTION TABLE RVGV1404 OU RVVE1400--LG=139 CAR------*  02450001
          02 GV14.                                                      02460001
      *---TABLE RTGV14 : 1 OCCURENCE = 79 CAR ==> MAX 7900              02470001
           05 MES45-PAIMT.                                              02480001
               10   PAI-NOMTAB    PIC     X(4).                         02490001
               10   PAI-NORDV     PIC     X(8).                         02500001
               10   PAI-CDEV      PIC     X(3).                         02510001
               10   PAI-PVLTRF    PIC     S9(7)V99 COMP-3.              02520001
               10   PAI-CDEVRF    PIC     X(3).                         02530001
               10   PAI-PMECRT    PIC     S9(7)V99 COMP-3.              02540001
               10   PAI-PTOT      PIC     S9(7)V99 COMP-3.              02550001
               10   PAI-PTOTRF    PIC     S9(7)V99 COMP-3.              02560001
               10   PAI-PTECRT    PIC     S9(7)V99 COMP-3.              02570001
               10   PAI-DATECP    PIC     X(8).                         02580001
               10   PAI-NSOCP     PIC     X(3).                         02590001
               10   PAI-NLIEUP    PIC     X(3).                         02600001
               10   PAI-NTRANS    PIC     9(8).                         02610001
               10   PAI-CMOPAI    PIC     X(5).                         02620001
               10   PAI-CDEVP     PIC     X(3).                         02630001
               10   PAI-INMOD     PIC     X(1).                         02640001
               10   PAI-CMOPAC    PIC     X(5).                         02650001
      *------DESCRIPTION TABLE RVGV0500 OU RVVE0500--LG=68 CAR------*   02660001
          02 GV05.                                                      02670001
             05 MGV05-NTAB    PIC X(04).                                02680001
             05 MGV05-NSOC    PIC X(03).                                02690001
             05 MGV05-NLIEU   PIC X(03).                                02700001
             05 MGV05-NVTE    PIC X(07).                                02710001
             05 MGV05-NDOS    PIC S9(3) COMP-3.                         02720001
             05 MGV05-CDOS    PIC X(05).                                02730001
             05 MGV05-CTYP    PIC X(02).                                02740001
             05 MGV05-NENT    PIC X(07).                                02750001
             05 MGV05-NSEQENS PIC S9(3) COMP-3.                         02760001
             05 MGV05-CAGENT  PIC X(02).                                02770001
             05 MGV05-DCREAT  PIC X(08).                                02780001
             05 MGV05-DMODIF  PIC X(08).                                02790001
             05 MGV05-DANNUL  PIC X(08).                                02800001
             05 MGV05-DSYST   PIC S9(13) COMP-3.                        02810001
      *------DESCRIPTION TABLE RVGV0600 OU RVVE0600--LG=31 CAR------*   02820001
          02 GV06.                                                      02830001
             05 MGV06-NTAB    PIC X(04).                                02840001
             05 MGV06-NSOC    PIC X(03).                                02850001
             05 MGV06-NLIEU   PIC X(03).                                02860001
             05 MGV06-NVTE    PIC X(07).                                02870001
             05 MGV06-NDOS    PIC S9(3) COMP-3.                         02880001
             05 MGV06-NSEQNQ  PIC S9(5) COMP-3.                         02890001
             05 MGV06-NSEQ    PIC S9(3) COMP-3.                         02900001
             05 MGV06-DSYST   PIC S9(13) COMP-3.                        02910001
      *------DESCRIPTION TABLE RVGV0800 OU RVVE0800--LG=103 CAR------*  02920001
          02 GV08.                                                      02930001
             05 MGV08-NTAB    PIC X(04).                                02940001
             05 MGV08-NSOC    PIC X(03).                                02950001
             05 MGV08-NLIEU   PIC X(03).                                02960001
             05 MGV08-NVTE    PIC X(07).                                02970001
             05 MGV08-NDOS    PIC S9(3) COMP-3.                         02980001
             05 MGV08-NSEQ    PIC S9(3) COMP-3.                         02990001
             05 MGV08-CCTRL   PIC X(05).                                03000001
             05 MGV08-LGCTRL  PIC S9(3) COMP-3.                         03010001
             05 MGV08-VALCTRL PIC X(50).                                03020001
             05 MGV08-CAUTOR  PIC X(05).                                03030001
             05 MGV08-CJUSTIF PIC X(05).                                03040001
             05 MGV08-DMODIF  PIC X(08).                                03050001
             05 MGV08-DSYST   PIC S9(13) COMP-3.                        03060001
      *------DESCRIPTION TABLE RVNV2000  --LG=92 CAR------*             03070001
          02 NV20.                                                      03080001
             05 MNV20-NTAB    PIC X(04).                                03090001
             05 MNV20-NSOC    PIC X(03).                                03100001
             05 MNV20-NLIEU   PIC X(03).                                03110001
             05 MNV20-NVTE    PIC X(07).                                03120001
             05 MNV20-NSEQNQ  PIC S9(5) COMP-3.                         03130001
             05 MNV20-NCODIC  PIC X(07).                                03140001
             05 MNV20-NSOCR   PIC X(03).                                03150001
             05 MNV20-NLIEUR  PIC X(03).                                03160001
             05 MNV20-TYPR    PIC X(02).                                03170001
             05 MNV20-NUMRES  PIC S9(9) COMP-3.                         03180001
             05 MNV20-ORDID   PIC X(11).                                03190001
             05 MNV20-ORDITID PIC X(11).                                03200001
             05 MNV20-DSYST   PIC S9(13) COMP-3.                        03210001
             05 MNV20-ORDREF  PIC X(11).                                03220001
             05 MNV20-ORDRTID PIC X(11).                                03230001
             05 MNV20-BLOCKED PIC X(01).                                03240001
      *--------------------------------------------------------------*  03250001
      * CARACTéRISTIQUES SPéCIFIQUES LONGUEUR 19 CARACTèRES          *  03251002
      *--------------------------------------------------------------*  03252002
          02  GV15.                                                     03260001
           05  MGV15-NTAB                     PIC X(0004).              03270004
           05  MGV15-NSOCIETE                 PIC X(0003).              03271004
           05  MGV15-NLIEU                    PIC X(0003).              03280003
           05  MGV15-NORDRE                   PIC X(0005).              03290003
           05  MGV15-NVENTE                   PIC X(0007).              03300003
           05  MGV15-NCODIC                   PIC X(0007).              03310003
           05  MGV15-NCODICGRP                PIC X(0007).              03320003
           05  MGV15-NSEQ                     PIC X(0002).              03330003
           05  MGV15-CARACTSPE1               PIC X(0005).              03340003
           05  MGV15-CVCARACTSPE1             PIC X(0005).              03350003
           05  MGV15-CARACTSPE2               PIC X(0005).              03360003
           05  MGV15-CVCARACTSPE2             PIC X(0005).              03370003
           05  MGV15-CARACTSPE3               PIC X(0005).              03380003
           05  MGV15-CVCARACTSPE3             PIC X(0005).              03390003
           05  MGV15-CARACTSPE4               PIC X(0005).              03400003
           05  MGV15-CVCARACTSPE4             PIC X(0005).              03410003
           05  MGV15-CARACTSPE5               PIC X(0005).              03420003
           05  MGV15-CVCARACTSPE5             PIC X(0005).              03430003
           05  MGV15-WANNULCAR                PIC X(0001).              03440003
           05  MGV15-DSYST                    PIC S9(13) COMP-3.        03450003
      *--------------------------------------------------------------*  03460007
      * DONNEES COMPLEMENTAIRES CLIENT                               *  03470007
      *--------------------------------------------------------------*  03480007
          02  GV04.                                                     03490007
           05 MGV04-NTAB                      PIC X(0004).              03500009
           05 MGV04-NSOCIETE         PIC X(3).                          03510009
           05 MGV04-NLIEU            PIC X(3).                          03520009
           05 MGV04-NVENTE           PIC X(7).                          03530009
           05 MGV04-DCREATION        PIC X(8).                          03540009
           05 MGV04-DMAJ             PIC X(8).                          03550009
           05 MGV04-WACCES           PIC X(1).                          03560009
           05 MGV04-WREPRISE         PIC X(1).                          03570009
           05 MGV04-NBPRDREP         PIC S9(2)V USAGE COMP-3.           03580009
           05 MGV04-DATENAISS        PIC X(8).                          03590009
           05 MGV04-CPNAISS          PIC X(5).                          03600009
           05 MGV04-LIEUNAISS        PIC X(32).                         03610009
           05 MGV04-WBTOB            PIC X(1).                          03620009
           05 MGV04-WARF             PIC X(1).                          03630009
           05 MGV04-DSYST            PIC S9(13)V USAGE COMP-3.          03640009
           05 MGV04-WASCUTIL         PIC X(01).                                 
           05 MGV04-WMARCHES         PIC X(01).                                 
           05 MGV04-WCOL             PIC X(01).                                 
           05 MGV04-WSDEP            PIC X(01).                                 
           05 MGV04-WFLAG            PIC X(10).                                 
          02  GV03.                                                     03650009
           05 MGV03-NTAB             PIC X(4).                          03660010
           05 MGV03-NSOCIETE         PIC X(3).                          03661010
           05 MGV03-NLIEU            PIC X(3).                          03670009
           05 MGV03-NORDRE           PIC X(5).                          03680009
           05 MGV03-NVENTE           PIC X(7).                          03690009
           05 MGV03-WTYPEADR         PIC X(1).                          03700009
           05 MGV03-EMAIL            PIC X(63).                         03710009
           05 MGV03-DSYST            PIC S9(13)V USAGE COMP-3.          03720009
           05 MGV03-CPORTE           PIC X(10).                         03730009
           05 MGV03-WADPRP           PIC X(01).                                 
           05 MGV03-WASC             PIC X(01).                                 
           05 MGV03-WFLAG            PIC X(10).                                 
                                                                                
