      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-GG40-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-GG40-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      * ZONES RESERVEES A AIDA                                                  
           02 FILLER-COM-AIDA      PIC X(100).                                  
      * ZONES RESERVEES EN PROVENANCE DE CICS                                   
           02 COMM-CICS-APPLID     PIC X(8).                                    
           02 COMM-CICS-NETNAM     PIC X(8).                                    
           02 COMM-CICS-TRANSA     PIC X(4).                                    
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION                     
           02 COMM-DATE-SIECLE     PIC XX.                                      
           02 COMM-DATE-ANNEE      PIC XX.                                      
           02 COMM-DATE-MOIS       PIC XX.                                      
           02 COMM-DATE-JOUR       PIC 99.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA       PIC 999.                                     
           02 COMM-DATE-QNT0       PIC 99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX       PIC 9.                                       
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM        PIC 9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC     PIC XXX.                                     
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC    PIC XXX.                                     
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                                    
           02 COMM-DATE-AAMMJJ     PIC X(6).                                    
           02 COMM-DATE-JJMMSSAA   PIC X(8).                                    
           02 COMM-DATE-JJMMAA     PIC X(6).                                    
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                                    
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS   PIC 99.                                      
              05 COMM-DATE-SEMAA   PIC 99.                                      
              05 COMM-DATE-SEMNU   PIC 99.                                      
           02 COMM-DATE-FILLER     PIC X(08).                                   
      * ZONES RESERVEES TRAITEMENT DU SWAP                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                     
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                               
      *                                                                         
      * ZONES RESERVEES APPLICATIVES                                            
           02 COMM-GG40-APPLI.                                                  
              03  COMM-GG40-ENTETE.                                             
                 05  COMM-GG40-SIEGE        PIC X(01).                          
                 05  COMM-GG40-FONC         PIC X(03).                          
                 05  COMM-GG40-NSOCIETE     PIC X(03).                          
                 05  COMM-GG40-PGM          PIC X(05).                          
                 05  COMM-GG40-NMAG         PIC X(03).                          
                 05  COMM-GG40-UNUSED1      PIC X(22).                          
                 05  COMM-GG40-NCODIC       PIC X(07).                          
                 05  COMM-GG40-UNUSED2      PIC X(10).                          
                 05  COMM-GG40-NZONPRIX     PIC X(02).                          
              03  COMM-GG40-MAP.                                                
               04  COMM-GG40-MAP-ENTETE.                                        
                 05  COMM-GG40-PAGE         PIC 9(03).                          
                 05  COMM-GG40-PAGETOT      PIC 9(03).                          
                 05  COMM-GG40-LREFFOURN    PIC X(20).                          
                 05  COMM-GG40-WDACEM       PIC X(01).                          
                 05  COMM-GG40-CFAM         PIC X(05).                          
E0072G           05  COMM-TOP-CFAM-BS       PIC 9 VALUE 0.                      
  "                  88  FAMILLE-NOT-BS           VALUE 0.                      
  "                  88  FAMILLE-BS               VALUE 1.                      
                 05  COMM-TOP-CFAM-MOD      PIC 9 VALUE 0.                      
                     88  FAMILLE-MODIF            VALUE 0.                      
                     88  FAMILLE-NON-MODIF        VALUE 1.                      
                 05  COMM-GG40-LSTATCOMP    PIC X(03).                          
                 05  COMM-GG40-CMARQ        PIC X(05).                          
                 05  COMM-GG40-PCHT         PIC S9(7)V99 COMP-3.                
                 05  COMM-GG40-SRPTTC       PIC S9(7)V99 COMP-3.                
E0072            05  COMM-GG40-PRMP         PIC S9(7)V99 COMP-3.                
                 05  COMM-GG40-WSENSAPP     PIC X(01).                          
                 05  COMM-GG40-OANAT        PIC X(01).                          
                 05  COMM-GG40-DOANAT       PIC X(08).                          
                 05  COMM-GG40-OAZP         PIC X(01).                          
                 05  COMM-GG40-DDOAZP       PIC X(08).                          
                 05  COMM-GG40-DFOAZP       PIC X(08).                          
                 05  COMM-GG40-CEXPO        PIC X(05).                          
                 05  COMM-GG40-CAPPRO       PIC X(05).                          
                 05  COMM-GG40-PRAR         PIC S9(7)V9(2) COMP-3.              
                 05  COMM-GG40-CTAUXTVA     PIC X(05).                          
                 05  COMM-GG40-DEFFET       PIC 9(08).                          
                 05  COMM-GG40-QCONDT       PIC S9(5) COMP-3.                   
      * PRIX ET PRIMES EXCEPTIONNELS                                            
E0072          04  COMM-GG40-MAP-CORPS.                                         
                 05  COMM-GG40-PRIEX        PIC S9(7)V99 COMP-3.                
                 05  COMM-GG40-DDPRIEX      PIC X(08).                          
                 05  COMM-GG40-DFPRIEX      PIC X(08).                          
                 05  COMM-GG40-DDEXCEP      PIC X(08).                          
                 05  COMM-GG40-DFEXCEP      PIC X(08).                          
                 05  COMM-GG40-PMBUEX       PIC S9(5)V99 COMP-3.                
                 05  COMM-GG40-COMEX        PIC S9(4)V9 COMP-3.                 
                 05  COMM-GG40-COMADAPT     PIC S9(4)V9 COMP-3.                 
                 05  COMM-GG40-DDCOMEX      PIC X(08).                          
                 05  COMM-GG40-DFCOMEX      PIC X(08).                          
                 05  COMM-GG40-EDMEX        PIC S9(5)V99 COMP-3.                
                 05  COMM-GG40-NCONC        PIC X(04).                          
                 05  COMM-GG40-LCONC        PIC X(12).                          
                 05  COMM-GG40-LIEU OCCURS 20.                                  
                     10  COMM-GG40-NLIEU    PIC X(03).                          
                 05  COMM-GG40-PRIEX-AV     PIC X(08).                          
                 05  COMM-GG40-DDPRIEX-AV   PIC X(08).                          
                 05  COMM-GG40-DFPRIEX-AV   PIC X(08).                          
                 05  COMM-GG40-COMEX-AV     PIC X(05).                          
                 05  COMM-GG40-DDCOMEX-AV   PIC X(08).                          
                 05  COMM-GG40-DFCOMEX-AV   PIC X(08).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05  COMM-GG40-NBMAG-AV    PIC S9(4) COMP.                      
      *--                                                                       
                 05  COMM-GG40-NBMAG-AV    PIC S9(4) COMP-5.                    
      *}                                                                        
                 05  COMM-GG40-LIEU-AV  OCCURS 20.                              
                     10  COMM-GG40-NLIEU-AV PIC X(03).                          
                 05  COMM-GG40-TOP-PRIEX   PIC X.                               
                     88  PRIEX-EXISTANT             VALUE 'O'.                  
                     88  PRIEX-PAS-EXISTANT         VALUE 'N'.                  
                 05  COMM-GG40-TOP-PRIMEX  PIC X.                               
                     88  PRIMEX-EXISTANT            VALUE 'O'.                  
                     88  PRIMEX-PAS-EXISTANT        VALUE 'N'.                  
                 05  COMM-GG40-MAJ-PRIEX   PIC X.                               
                 05  COMM-GG40-CANNPRIEX   PIC X.                               
                 05  COMM-GG40-ANNUL-PRIEX PIC X.                               
                 05  COMM-GG40-MAJ-COMEX   PIC X.                               
                 05  COMM-GG40-CANNCOMEX   PIC X.                               
                 05  COMM-GG40-ANNUL-COMEX PIC X.                               
              03  COMM-GG40-AUTRE-DATA.                                         
                 05  COMM-GG40-SIMI.                                            
                     10  COMM-GG40-ART-SIM  PIC 9(7) OCCURS 20.                 
                     10  COMM-GG40-NBARTSIM PIC S9(3) COMP-3.                   
                     10  COMM-GG40-CPTSIM   PIC S9(3) COMP-3.                   
SR               05  COMM-GG40-GRP.                                             
                     10  COMM-GG40-CCODGROUP   PIC X.                           
                     10  COMM-GG40-TABLE-GROUP.                                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                  15  COMM-GG40-NBR-GROUP PIC S9(4) COMP.                
      *--                                                                       
                         15  COMM-GG40-NBR-GROUP PIC S9(4) COMP-5.              
      *}                                                                        
                         15  COMM-GG40-TAB-GROUP OCCURS 200.                    
                             20  COMM-GG40-TAB-NCODIC    PIC 9(7).              
                             20  COMM-GG40-TAB-QLIENS                           
                                      PIC S9(3) COMP-3.                         
                             20  COMM-GG40-GRP-OU-ELT    PIC X.                 
                 05  COMM-GG40-RESTE.                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *              10  COMM-GG40-IND-TS-MAX  PIC S9(04) COMP.                 
      *--                                                                       
                     10  COMM-GG40-IND-TS-MAX  PIC S9(04) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *              10  COMM-GG40-IND-RECH-TS PIC S9(04) COMP.                 
      *--                                                                       
                     10  COMM-GG40-IND-RECH-TS PIC S9(04) COMP-5.               
      *}                                                                        
                     10  COMM-GG40-QTAUXTVA    PIC S9V9(4) COMP-3.              
                     10  COMM-GG40-EDMMAX      PIC 99999.                       
                     10  COMM-GG40-CHGT-NCODIC PIC X.                           
                     10  COMM-GG40-NCODIC-SUIV PIC X(07).                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *              10  COMM-GG40-NBMAG-AP    PIC S9(4) COMP.                  
      *--                                                                       
                     10  COMM-GG40-NBMAG-AP    PIC S9(4) COMP-5.                
      *}                                                                        
                     10  COMM-GG40-AFF-COMMENT PIC X.                           
                         88  COMMENT-NON-AFFICHE        VALUE 'O'.              
                         88  COMMENT-AFFICHE            VALUE 'N'.              
SR               05  COMM-GG40-RESTE-B.                                         
                     10  COMM-GG40-PASSAGE-1  PIC X(01).                        
                     10  COMM-GG40-PASSAGE-2  PIC X(01).                        
                     10  COMM-GG40-DATEFFET   PIC X(08).                        
                     10  COMM-GG40-PVTTC      PIC 9(7)V99 COMP-3.               
                     10  COMM-GG40-MAJ        PIC X(01).                        
      * ZONE DE REFERENCE  ACTUEL                                               
                     10  COMM-GG40-PRIRAC   PIC S9(7)V99 COMP-3.                
                     10  COMM-GG40-WIMPAC   PIC X(01).                          
                     10  COMM-GG40-WMAXAC   PIC X(01).                          
                     10  COMM-GG40-DDREFA   PIC X(08).                          
                     10  COMM-GG40-DFREFA   PIC X(08).                          
                     10  COMM-GG40-INTREFA  PIC S9(7)V99 COMP-3.                
                     10  COMM-GG40-DINTREFA PIC X(08).                          
      *                    FUTUR                                                
                     10  COMM-GG40-PRIRNO   PIC S9(7)V99 COMP-3.                
                     10  COMM-GG40-WIMPN    PIC X(01).                          
                     10  COMM-GG40-WMAXN    PIC X(01).                          
                     10  COMM-GG40-DDREFN   PIC X(08).                          
                     10  COMM-GG40-DFREFN   PIC X(08).                          
                     10  COMM-GG40-INTREFN  PIC S9(7)V99 COMP-3.                
                     10  COMM-GG40-DINTREFN PIC X(08).                          
      * ZONE DE REFERENCE2 ACTUEL                                               
                     10  COMM-GG40-PRIR2AC  PIC S9(7)V99 COMP-3.                
                     10  COMM-GG40-WBLOCAC  PIC X(01).                          
                     10  COMM-GG40-DDREF2A  PIC X(08).                          
                     10  COMM-GG40-DFREF2A  PIC X(08).                          
      *                    FUTUR                                                
                     10  COMM-GG40-PRIR2NO  PIC S9(7)V99 COMP-3.                
                     10  COMM-GG40-WBLOCN   PIC X(01).                          
                     10  COMM-GG40-DDREF2N  PIC X(08).                          
                     10  COMM-GG40-DFREF2N  PIC X(08).                          
      * ZONE DU MAGASIN   ACTUEL                                                
                     10  COMM-GG40-PRIMAC   PIC S9(7)V99 COMP-3.                
                     10  COMM-GG40-EXIST-PVL-ACT  PIC X(01).                    
                     10  COMM-GG40-DPRIMAC  PIC X(08).                          
                     10  COMM-GG40-LCOMMMA  PIC X(15).                          
                     10  COMM-GG40-PCOMMAC  PIC S9(4)V9  COMP-3.                
                     10  COMM-GG40-DPCOMMA  PIC X(08).                          
                     10  COMM-GG40-INTFAMA  PIC S9(4)V9  COMP-3.                
      *                   FUTUR                                                 
                     10  COMM-GG40-EXIST-PVL-FUT  PIC X(01).                    
      *                   ZONE DE GESTION COMPLEMENTAIRE                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
E0072 *              10  COMM-GG40-IND-TSGG41-MAX PIC S9(4) COMP.               
      *--                                                                       
                     10  COMM-GG40-IND-TSGG41-MAX PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
  "   *              10  COMM-GG40-IND-TSGG41     PIC S9(4) COMP.               
      *--                                                                       
                     10  COMM-GG40-IND-TSGG41     PIC S9(4) COMP-5.             
      *}                                                                        
  "                  10  COMM-GG40-CTRL-PMINI     PIC X(01) VALUE 'N'.          
  "                      88  EXIST-PAS-PCOMMINI             VALUE 'N'.          
  "                      88  EXIST-PCOMMINI                 VALUE 'O'.          
  "                  10  COMM-GG40-PCOMMINIX      PIC X(10).                    
  "                  10  COMM-GG40-PCOMMINI9      PIC S9(7)V99 COMP-3.          
  "   *                                                                         
  "                  10  W-WCOPR1-MAX     PIC S9(4) COMP-3 VALUE +150.          
  "                  10  W-WCOPR1-TAB.                                          
  "                      15  W-WCOPR1-NBP         PIC S9(4) COMP-3.             
  "                      15  W-WCOPR1-POS         OCCURS 150                    
  "                                               INDEXED W-ICOPR1.             
  "                          20 W-WCOPR1-MT       PIC 999V99.                   
                     10 COMM-GG40-TOP-CFAM-CBN    PIC 9.                        
                         88  COMM-GG40-CFAM-NOT-CBN     VALUE 0.                
                         88  COMM-GG40-CFAM-CBN         VALUE 1.                
              03  COMM-GG40-CBGSM          PIC X(1).                            
              03  COMM-GG40-FILLER   PIC X(0171).                               
                                                                                
