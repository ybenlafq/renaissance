      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00030000
      *   COPY DE LA TABLE RVGN5900                                     00040001
      **********************************************************        00050000
      *   LISTE DES HOST VARIABLES DE LA VUE RVGN5900                   00060001
      **********************************************************        00070000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGN5900.                                                    00080001
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGN5900.                                                            
      *}                                                                        
           05  GN59-NCODIC     PIC X(7).                                00120001
           05  GN59-CREF       PIC X(1).                                00130001
           05  GN59-DEFFET     PIC X(8).                                00140001
           05  GN59-PREFTTC    PIC S9(5)V99 COMP-3.                     00150001
           05  GN59-WIMPERATIF PIC X(1).                                00151003
           05  GN59-CIMPER     PIC X(1).                                00151104
           05  GN59-WBLOQUE    PIC X(1).                                00151203
           05  GN59-LCOMMENT   PIC X(10).                               00152001
           05  GN59-DSYST      PIC S9(13) COMP-3.                       00160001
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************        00170000
      *   LISTE DES FLAGS DE LA TABLE RVGN5900                          00180001
      **********************************************************        00190000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGN5900-FLAGS.                                              00200001
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGN5900-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN59-NCODIC-F                                            00210002
      *        PIC S9(4) COMP.                                          00211001
      *--                                                                       
           05  GN59-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN59-CREF-F                                              00220001
      *        PIC S9(4) COMP.                                          00221001
      *--                                                                       
           05  GN59-CREF-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN59-DEFFET-F                                            00230001
      *        PIC S9(4) COMP.                                          00231001
      *--                                                                       
           05  GN59-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN59-PREFTTC-F                                           00240001
      *        PIC S9(4) COMP.                                          00241001
      *--                                                                       
           05  GN59-PREFTTC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN59-WIMPERATIF-F                                        00250003
      *        PIC S9(4) COMP.                                          00251001
      *--                                                                       
           05  GN59-WIMPERATIF-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN59-CIMPER-F                                            00251104
      *        PIC S9(4) COMP.                                          00251204
      *--                                                                       
           05  GN59-CIMPER-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN59-WBLOQUE-F                                           00252003
      *        PIC S9(4) COMP.                                          00253003
      *--                                                                       
           05  GN59-WBLOQUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN59-LCOMMENT-F                                          00260001
      *        PIC S9(4) COMP.                                          00271000
      *--                                                                       
           05  GN59-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN59-DSYST-F                                             00280001
      *        PIC S9(4) COMP.                                          00290000
      *                                                                         
      *--                                                                       
           05  GN59-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
