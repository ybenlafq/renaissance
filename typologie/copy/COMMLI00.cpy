      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TLI01 (TLI00 -> MENU)    TR: LI00  *    00002209
      *                 DEFINITION DES LIEUX                       *    00002309
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00009000
      *                                                                 00410100
      *            TRANSACTION LI01 : DEFINITION DES LIEUX            * 00411009
      *                                                                 00412000
      **************************************************************            
      * COMMAREA SPECIFIQUE PRG TLI00 (MENU)             TR: LI00  *            
      *                          TLI01                             *            
      *                           TLI02                            *            
      *                                                            *            
      **************************************************************            
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *            
      **************************************************************            
      *                                                                         
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET                    
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR          
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU                  
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                            
      *                                                                         
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                              
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES               
      * PAR AIDA                                                                
      *                                                                         
      *-------------------------------------------------------------            
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-LI01-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-LI01-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA ----------------------------------- 100          
          02 FILLER-COM-AIDA      PIC X(100).                                   
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020          
          02 COMM-CICS-APPLID     PIC X(8).                                     
          02 COMM-CICS-NETNAM     PIC X(8).                                     
          02 COMM-CICS-TRANSA     PIC X(4).                                     
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100          
          02 COMM-DATE-SIECLE     PIC XX.                                       
          02 COMM-DATE-ANNEE      PIC XX.                                       
          02 COMM-DATE-MOIS       PIC XX.                                       
          02 COMM-DATE-JOUR       PIC XX.                                       
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
          02 COMM-DATE-QNTA       PIC 999.                                      
          02 COMM-DATE-QNT0       PIC 99999.                                    
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
          02 COMM-DATE-BISX       PIC 9.                                        
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                   
          02 COMM-DATE-JSM        PIC 9.                                        
      *   LIBELLES DU JOUR COURT - LONG                                         
          02 COMM-DATE-JSM-LC     PIC XXX.                                      
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                 
      *   LIBELLES DU MOIS COURT - LONG                                         
          02 COMM-DATE-MOIS-LC    PIC XXX.                                      
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                 
      *   DIFFERENTES FORMES DE DATE                                            
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                                     
          02 COMM-DATE-AAMMJJ     PIC X(6).                                     
          02 COMM-DATE-JJMMSSAA   PIC X(8).                                     
          02 COMM-DATE-JJMMAA     PIC X(6).                                     
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                                     
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                    
      *   DIFFERENTES FORMES DE DATE                                            
          02 COMM-DATE-FILLER     PIC X(14).                                    
      *                                                                         
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                      
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                                
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724          
      *                                                                         
      *            TRANSACTION LI00 : ADMINISTRATION DES DONNEES      *         
      *                                                                         
      *****************************************************************         
          02 COMM-LI00-APPLI .                                          00420013
      *------------------------------ ZONE DONNEES TLI01                00510018
             03 COMM-LI00-DONNEES-TLI00.                                00520018
      *------------------------------ CODE SOCIETE                      00550009
                04 COMM-LI00-NSOCG          PIC XXX.                    00560018
      *------------------------------ CODE LIEU                         00521018
                04 COMM-LI00-NLIEUG         PIC XXX.                    00522018
      *------------------------------ CODE TRI                          00550009
                04 COMM-LI00-TRI            PIC X.                      00560018
      *------------------------------ CODE SOCIETE RATTACHEMENT         00550009
                04 COMM-LI00-NSOCGRPG       PIC XXX.                    00560018
      *------------------------------ LIBELLE SOCIETE                   00570009
                04 COMM-LI00-LIBSOCG           PIC X(20).               00580018
      *------------------------------ CODE TYPE DE SOCIETE              00580019
                04 COMM-LI00-CTYPSOCG          PIC X(3).                00580020
      *------------------------------ TYPE DE LIEU                      00580019
                04 COMM-LI00-CTYPLIEUG         PIC X(1).                00580020
      *------------------------------ MAGASIN PCF                       00580030
                04 COMM-LI00-PCFMAG.                                    00580040
                   05 COMM-LI00-PCFSOC         PIC X(03).                       
                   05 COMM-LI00-PCFLIEU        PIC X(03).                       
      *------------------------------ NUMERO PAGE ECRAN                 00730710
                04 COMM-LI00-NUMPAG            PIC 9(3).                00730818
      *------------------------------ NUMERO PAGE MAXI                  00730710
                04 COMM-LI00-PAGE-MAX          PIC 9(3).                00730818
      *------------------------------ ZONE LIBRE                        00735100
             03 COMM-LI00-SWAP           PIC X OCCURS 200.              00736019
      *------------------------------ ZONE LIBRE                        00738020
             03 COMM-LI00-LIBRE          PIC X(3478).                   00738030
      ***************************************************************** 00740000
                                                                                
