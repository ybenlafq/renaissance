      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MOPA2 MOPAI SPECIF FILIALE             *        
      *----------------------------------------------------------------*        
       01  RVGA01FB.                                                            
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  MOPA2-CTABLEG2    PIC X(15).                                     
           05  MOPA2-CTABLEG2-REDEF REDEFINES MOPA2-CTABLEG2.                   
               10  MOPA2-CMODRGLT        PIC X(05).                             
           05  MOPA2-WTABLEG     PIC X(80).                                     
           05  MOPA2-WTABLEG-REDEF  REDEFINES MOPA2-WTABLEG.                    
               10  MOPA2-LMOD            PIC X(20).                             
               10  MOPA2-WMTVTE          PIC X(10).                             
               10  MOPA2-C36S            PIC X(04).                             
               10  MOPA2-RENDU           PIC X(05).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  MOPA2-RENDU-N        REDEFINES MOPA2-RENDU                   
                                         PIC 9(03)V9(02).                       
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  MOPA2-IWERC           PIC X(14).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01FB-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MOPA2-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MOPA2-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MOPA2-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MOPA2-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
