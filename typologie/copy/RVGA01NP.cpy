      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HESOC SOCIETES GHS                     *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01NP.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01NP.                                                            
      *}                                                                        
           05  HESOC-CTABLEG2    PIC X(15).                                     
           05  HESOC-CTABLEG2-REDEF REDEFINES HESOC-CTABLEG2.                   
               10  HESOC-NSOCIETE        PIC X(03).                             
           05  HESOC-WTABLEG     PIC X(80).                                     
           05  HESOC-WTABLEG-REDEF  REDEFINES HESOC-WTABLEG.                    
               10  HESOC-WACTIF          PIC X(01).                             
               10  HESOC-WCTRLNHS        PIC X(01).                             
               10  HESOC-NBEXHE15        PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  HESOC-NBEXHE15-N     REDEFINES HESOC-NBEXHE15                
                                         PIC 9(02).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  HESOC-WPANNE          PIC X(01).                             
               10  HESOC-WSAVELA         PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01NP-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01NP-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HESOC-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HESOC-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HESOC-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HESOC-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
