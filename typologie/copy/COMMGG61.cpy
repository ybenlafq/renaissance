      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00010000
      **************************************************************    00020000
      * COMMAREA SPECIFIQUE PRG TGG61 (TGG60 -> MENU)    TR: GG60  *    00030000
      *                                                            *    00031000
      *                       RECYCLAGE DU PRMP                    *    00040000
      *                                                            *    00050000
      * ZONES RESERVEES APPLICATIVES ------------------------------*    00060003
      *                                                            *    00070000
      *        TRANSACTION GG60 : RECYCLAGE PRMP SUR PRA           *    00080000
      *                                                            *    00090000
      *------------------------------ ZONE DONNEES TGG60-----------*    00110000
      *                                                                 00720200
           02 COMM-GG61-APPLI   REDEFINES  COMM-GG60-APPLI.             00721000
              03 COMM-GG61-NCODIC     PIC X(7).                         00740102
              03 COMM-GG61-NENTCDE    PIC X(5).                         00740202
              03 COMM-GG61-LENTCDE    PIC X(20).                        00740302
              03 COMM-GG61-NREC       PIC X(7).                         00740402
              03 COMM-GG61-LREFFOURN  PIC X(20).                        00740502
              03 COMM-GG61-LREF       PIC X(20).                        00740503
              03 COMM-GG61-DREC       PIC X(8).                         00740603
              03 COMM-GG61-CMARQ      PIC X(5).                         00740702
              03 COMM-GG61-LMARQ      PIC X(20).                        00740703
              03 COMM-GG61-NCDE       PIC X(7).                         00740803
              03 COMM-GG61-CFAM       PIC X(5).                         00740804
              03 COMM-GG61-LFAM       PIC X(20).                        00740805
              03 COMM-GG61-QREC       PIC S9(5)      COMP-3.            00740806
              03 COMM-GG61-PRA        PIC S9(7)V99   COMP-3.            00740807
              03 COMM-GG61-PRAANC     PIC S9(7)V9(2) COMP-3.            00740808
              03 COMM-GG61-PRMPANC    PIC S9(7)V9(6) COMP-3.            00740809
              03 COMM-GG61-QPRA       PIC S9(5)      COMP-3.            00740806
      *                                                                 00741100
      *------------------------------ ZONE DONNEES TGG61                00741200
      *                                                                 00741300
      *       03 FILLER               PIC X(3557).                      00743004
GD            03 COMM-GG61-DONNEES.                                     00743004
                 05 FILLER            PIC X(71).                                
                 05 COMM-GG61-DEVISE.                                           
                    10 COMM-GG61-CDEVISE-DAR    PIC X(3).                       
                    10 COMM-GG61-CDEVISE-FNR    PIC X(3).                       
                    10 COMM-GG61-LDEVISE-DAR    PIC X(25).                      
                    10 COMM-GG61-LDEVISE-FNR    PIC X(25).                      
                    10 COMM-GG61-FLAG-EURO      PIC X.                          
                 05 COMM-GG61-FLAG-PRA          PIC X.                          
                 05 FILLER                      PIC X(3428).                    
      *                                                                 00750000
                                                                                
