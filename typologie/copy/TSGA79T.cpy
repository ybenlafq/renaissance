      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *===============================================================* 00010000
      *                                                               * 00020000
      *      TS ASSOCIE A LA TRANSACTION GA79                         * 00030021
      *            TAXE ECO-MOBILIER                                  * 00040017
      *                                                               * 00050000
      *===============================================================* 00060000
                                                                        00070000
       01 TS-GA79T.                                                     00080024
          05 TS-GA79T-LONG             PIC S9(5) COMP-3     VALUE +4.   00090025
          05 TS-GA79T-DONNEES.                                          00100024
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      10 TS-GA79T-POSIT-TAXE    PIC S9(3) COMP.                  00111024
      *--                                                                       
             10 TS-GA79T-POSIT-TAXE    PIC S9(3) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      10 TS-GA79T-POSIT-TAXEH   PIC S9(3) COMP.                  00112025
      *--                                                                       
             10 TS-GA79T-POSIT-TAXEH   PIC S9(3) COMP-5.                        
      *}                                                                        
      *===============================================================* 00320000
                                                                                
