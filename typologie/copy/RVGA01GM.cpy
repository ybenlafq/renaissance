      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE REGUT REGUL UTILISATEUR                *        
      *----------------------------------------------------------------*        
       01  RVGA01GM.                                                            
           05  REGUT-CTABLEG2    PIC X(15).                                     
           05  REGUT-CTABLEG2-REDEF REDEFINES REGUT-CTABLEG2.                   
               10  REGUT-COPER           PIC X(08).                             
               10  REGUT-CACID           PIC X(07).                             
           05  REGUT-WTABLEG     PIC X(80).                                     
           05  REGUT-WTABLEG-REDEF  REDEFINES REGUT-WTABLEG.                    
               10  REGUT-TOP             PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01GM-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  REGUT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  REGUT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  REGUT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  REGUT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
