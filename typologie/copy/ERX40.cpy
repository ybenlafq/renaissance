      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERX40   ERX40                                              00000020
      ***************************************************************** 00000030
       01   ERX40I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MZONCMDI  PIC X(10).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCI    PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNMAGI    PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRELEVL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNRELEVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNRELEVF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNRELEVI  PIC X(7).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCONCL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNCONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCONCF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNCONCI   PIC X(4).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCONCL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLCONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLCONCF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLCONCI   PIC X(15).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATRELL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDATRELL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATRELF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDATRELI  PIC X(10).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MHEUREL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MHEUREL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MHEUREF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MHEUREI   PIC X(2).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCVENDL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCVENDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCVENDF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCVENDI   PIC X(6).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLIBERRI  PIC X(78).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCODTRAI  PIC X(4).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCICSI    PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNETNAMI  PIC X(8).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MSCREENI  PIC X(4).                                       00000730
      ***************************************************************** 00000740
      * SDF: ERX40   ERX40                                              00000750
      ***************************************************************** 00000760
       01   ERX40O REDEFINES ERX40I.                                    00000770
           02 FILLER    PIC X(12).                                      00000780
           02 FILLER    PIC X(2).                                       00000790
           02 MDATJOUA  PIC X.                                          00000800
           02 MDATJOUC  PIC X.                                          00000810
           02 MDATJOUP  PIC X.                                          00000820
           02 MDATJOUH  PIC X.                                          00000830
           02 MDATJOUV  PIC X.                                          00000840
           02 MDATJOUO  PIC X(10).                                      00000850
           02 FILLER    PIC X(2).                                       00000860
           02 MTIMJOUA  PIC X.                                          00000870
           02 MTIMJOUC  PIC X.                                          00000880
           02 MTIMJOUP  PIC X.                                          00000890
           02 MTIMJOUH  PIC X.                                          00000900
           02 MTIMJOUV  PIC X.                                          00000910
           02 MTIMJOUO  PIC X(5).                                       00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MZONCMDA  PIC X.                                          00000940
           02 MZONCMDC  PIC X.                                          00000950
           02 MZONCMDP  PIC X.                                          00000960
           02 MZONCMDH  PIC X.                                          00000970
           02 MZONCMDV  PIC X.                                          00000980
           02 MZONCMDO  PIC X(10).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MWFONCA   PIC X.                                          00001010
           02 MWFONCC   PIC X.                                          00001020
           02 MWFONCP   PIC X.                                          00001030
           02 MWFONCH   PIC X.                                          00001040
           02 MWFONCV   PIC X.                                          00001050
           02 MWFONCO   PIC X(3).                                       00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MNSOCA    PIC X.                                          00001080
           02 MNSOCC    PIC X.                                          00001090
           02 MNSOCP    PIC X.                                          00001100
           02 MNSOCH    PIC X.                                          00001110
           02 MNSOCV    PIC X.                                          00001120
           02 MNSOCO    PIC X(3).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MNMAGA    PIC X.                                          00001150
           02 MNMAGC    PIC X.                                          00001160
           02 MNMAGP    PIC X.                                          00001170
           02 MNMAGH    PIC X.                                          00001180
           02 MNMAGV    PIC X.                                          00001190
           02 MNMAGO    PIC X(3).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MNRELEVA  PIC X.                                          00001220
           02 MNRELEVC  PIC X.                                          00001230
           02 MNRELEVP  PIC X.                                          00001240
           02 MNRELEVH  PIC X.                                          00001250
           02 MNRELEVV  PIC X.                                          00001260
           02 MNRELEVO  PIC X(7).                                       00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MNCONCA   PIC X.                                          00001290
           02 MNCONCC   PIC X.                                          00001300
           02 MNCONCP   PIC X.                                          00001310
           02 MNCONCH   PIC X.                                          00001320
           02 MNCONCV   PIC X.                                          00001330
           02 MNCONCO   PIC X(4).                                       00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MLCONCA   PIC X.                                          00001360
           02 MLCONCC   PIC X.                                          00001370
           02 MLCONCP   PIC X.                                          00001380
           02 MLCONCH   PIC X.                                          00001390
           02 MLCONCV   PIC X.                                          00001400
           02 MLCONCO   PIC X(15).                                      00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MDATRELA  PIC X.                                          00001430
           02 MDATRELC  PIC X.                                          00001440
           02 MDATRELP  PIC X.                                          00001450
           02 MDATRELH  PIC X.                                          00001460
           02 MDATRELV  PIC X.                                          00001470
           02 MDATRELO  PIC X(10).                                      00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MHEUREA   PIC X.                                          00001500
           02 MHEUREC   PIC X.                                          00001510
           02 MHEUREP   PIC X.                                          00001520
           02 MHEUREH   PIC X.                                          00001530
           02 MHEUREV   PIC X.                                          00001540
           02 MHEUREO   PIC X(2).                                       00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MCVENDA   PIC X.                                          00001570
           02 MCVENDC   PIC X.                                          00001580
           02 MCVENDP   PIC X.                                          00001590
           02 MCVENDH   PIC X.                                          00001600
           02 MCVENDV   PIC X.                                          00001610
           02 MCVENDO   PIC X(6).                                       00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MLIBERRA  PIC X.                                          00001640
           02 MLIBERRC  PIC X.                                          00001650
           02 MLIBERRP  PIC X.                                          00001660
           02 MLIBERRH  PIC X.                                          00001670
           02 MLIBERRV  PIC X.                                          00001680
           02 MLIBERRO  PIC X(78).                                      00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MCODTRAA  PIC X.                                          00001710
           02 MCODTRAC  PIC X.                                          00001720
           02 MCODTRAP  PIC X.                                          00001730
           02 MCODTRAH  PIC X.                                          00001740
           02 MCODTRAV  PIC X.                                          00001750
           02 MCODTRAO  PIC X(4).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MCICSA    PIC X.                                          00001780
           02 MCICSC    PIC X.                                          00001790
           02 MCICSP    PIC X.                                          00001800
           02 MCICSH    PIC X.                                          00001810
           02 MCICSV    PIC X.                                          00001820
           02 MCICSO    PIC X(5).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MNETNAMA  PIC X.                                          00001850
           02 MNETNAMC  PIC X.                                          00001860
           02 MNETNAMP  PIC X.                                          00001870
           02 MNETNAMH  PIC X.                                          00001880
           02 MNETNAMV  PIC X.                                          00001890
           02 MNETNAMO  PIC X(8).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MSCREENA  PIC X.                                          00001920
           02 MSCREENC  PIC X.                                          00001930
           02 MSCREENP  PIC X.                                          00001940
           02 MSCREENH  PIC X.                                          00001950
           02 MSCREENV  PIC X.                                          00001960
           02 MSCREENO  PIC X(4).                                       00001970
                                                                                
