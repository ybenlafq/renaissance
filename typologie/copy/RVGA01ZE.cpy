      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SLTRS NMD/SL : TYPES DE RESERVATION    *        
      *----------------------------------------------------------------*        
       01  RVGA01ZE.                                                            
           05  SLTRS-CTABLEG2    PIC X(15).                                     
           05  SLTRS-CTABLEG2-REDEF REDEFINES SLTRS-CTABLEG2.                   
               10  SLTRS-CTYPRS          PIC X(02).                             
           05  SLTRS-WTABLEG     PIC X(80).                                     
           05  SLTRS-WTABLEG-REDEF  REDEFINES SLTRS-WTABLEG.                    
               10  SLTRS-LTYPRS          PIC X(20).                             
               10  SLTRS-WVENTE          PIC X(01).                             
               10  SLTRS-WATAFF          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01ZE-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLTRS-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SLTRS-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLTRS-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SLTRS-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
