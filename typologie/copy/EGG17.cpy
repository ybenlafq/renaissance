      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGG17   EGG17                                              00000020
      ***************************************************************** 00000030
       01   EGG17I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATDEBL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDATDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATDEBF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDATDEBI  PIC X(8).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATFINL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDATFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATFINF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDATFINI  PIC X(8).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLFAMI    PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENSGNL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLENSGNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLENSGNF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLENSGNI  PIC X(15).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRAYONL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCRAYONL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCRAYONF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCRAYONI  PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLRAYONL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLRAYONL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLRAYONF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLRAYONI  PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLEMPLCL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLEMPLCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLEMPLCF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLEMPLCI  PIC X(15).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPROL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAPPROF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCAPPROI  PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAPPROL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLAPPROF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLAPPROI  PIC X(20).                                      00000610
           02 MSTATI OCCURS   11 TIMES .                                00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAM2L      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MCFAM2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCFAM2F      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCFAM2I      PIC X(5).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLENSEIL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MLENSEIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLENSEIF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MLENSEII     PIC X(15).                                 00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBEMPL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MLIBEMPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLIBEMPF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MLIBEMPI     PIC X(15).                                 00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQARTGL      COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MQARTGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQARTGF      PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MQARTGI      PIC X(4).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQRELINL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MQRELINL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQRELINF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MQRELINI     PIC X(4).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQARTINL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MQARTINL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQARTINF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MQARTINI     PIC X(4).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQAL    COMP PIC S9(4).                                 00000870
      *--                                                                       
             03 MQAL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MQAF    PIC X.                                          00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MQAI    PIC X(4).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQASL   COMP PIC S9(4).                                 00000910
      *--                                                                       
             03 MQASL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQASF   PIC X.                                          00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MQASI   PIC X(4).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQNAL   COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MQNAL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQNAF   PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MQNAI   PIC X(4).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTOTALL     COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MQTOTALL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTOTALF     PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MQTOTALI     PIC X(4).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MZONCMDI  PIC X(15).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MLIBERRI  PIC X(58).                                      00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCODTRAI  PIC X(4).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCICSI    PIC X(5).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MNETNAMI  PIC X(8).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MSCREENI  PIC X(4).                                       00001260
      ***************************************************************** 00001270
      * SDF: EGG17   EGG17                                              00001280
      ***************************************************************** 00001290
       01   EGG17O REDEFINES EGG17I.                                    00001300
           02 FILLER    PIC X(12).                                      00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MDATJOUA  PIC X.                                          00001330
           02 MDATJOUC  PIC X.                                          00001340
           02 MDATJOUP  PIC X.                                          00001350
           02 MDATJOUH  PIC X.                                          00001360
           02 MDATJOUV  PIC X.                                          00001370
           02 MDATJOUO  PIC X(10).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MTIMJOUA  PIC X.                                          00001400
           02 MTIMJOUC  PIC X.                                          00001410
           02 MTIMJOUP  PIC X.                                          00001420
           02 MTIMJOUH  PIC X.                                          00001430
           02 MTIMJOUV  PIC X.                                          00001440
           02 MTIMJOUO  PIC X(5).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MWPAGEA   PIC X.                                          00001470
           02 MWPAGEC   PIC X.                                          00001480
           02 MWPAGEP   PIC X.                                          00001490
           02 MWPAGEH   PIC X.                                          00001500
           02 MWPAGEV   PIC X.                                          00001510
           02 MWPAGEO   PIC X(3).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MWFONCA   PIC X.                                          00001540
           02 MWFONCC   PIC X.                                          00001550
           02 MWFONCP   PIC X.                                          00001560
           02 MWFONCH   PIC X.                                          00001570
           02 MWFONCV   PIC X.                                          00001580
           02 MWFONCO   PIC X(3).                                       00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MDATDEBA  PIC X.                                          00001610
           02 MDATDEBC  PIC X.                                          00001620
           02 MDATDEBP  PIC X.                                          00001630
           02 MDATDEBH  PIC X.                                          00001640
           02 MDATDEBV  PIC X.                                          00001650
           02 MDATDEBO  PIC X(8).                                       00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MDATFINA  PIC X.                                          00001680
           02 MDATFINC  PIC X.                                          00001690
           02 MDATFINP  PIC X.                                          00001700
           02 MDATFINH  PIC X.                                          00001710
           02 MDATFINV  PIC X.                                          00001720
           02 MDATFINO  PIC X(8).                                       00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MCFAMA    PIC X.                                          00001750
           02 MCFAMC    PIC X.                                          00001760
           02 MCFAMP    PIC X.                                          00001770
           02 MCFAMH    PIC X.                                          00001780
           02 MCFAMV    PIC X.                                          00001790
           02 MCFAMO    PIC X(5).                                       00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MLFAMA    PIC X.                                          00001820
           02 MLFAMC    PIC X.                                          00001830
           02 MLFAMP    PIC X.                                          00001840
           02 MLFAMH    PIC X.                                          00001850
           02 MLFAMV    PIC X.                                          00001860
           02 MLFAMO    PIC X(20).                                      00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MLENSGNA  PIC X.                                          00001890
           02 MLENSGNC  PIC X.                                          00001900
           02 MLENSGNP  PIC X.                                          00001910
           02 MLENSGNH  PIC X.                                          00001920
           02 MLENSGNV  PIC X.                                          00001930
           02 MLENSGNO  PIC X(15).                                      00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MCRAYONA  PIC X.                                          00001960
           02 MCRAYONC  PIC X.                                          00001970
           02 MCRAYONP  PIC X.                                          00001980
           02 MCRAYONH  PIC X.                                          00001990
           02 MCRAYONV  PIC X.                                          00002000
           02 MCRAYONO  PIC X(5).                                       00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MLRAYONA  PIC X.                                          00002030
           02 MLRAYONC  PIC X.                                          00002040
           02 MLRAYONP  PIC X.                                          00002050
           02 MLRAYONH  PIC X.                                          00002060
           02 MLRAYONV  PIC X.                                          00002070
           02 MLRAYONO  PIC X(20).                                      00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MLEMPLCA  PIC X.                                          00002100
           02 MLEMPLCC  PIC X.                                          00002110
           02 MLEMPLCP  PIC X.                                          00002120
           02 MLEMPLCH  PIC X.                                          00002130
           02 MLEMPLCV  PIC X.                                          00002140
           02 MLEMPLCO  PIC X(15).                                      00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCAPPROA  PIC X.                                          00002170
           02 MCAPPROC  PIC X.                                          00002180
           02 MCAPPROP  PIC X.                                          00002190
           02 MCAPPROH  PIC X.                                          00002200
           02 MCAPPROV  PIC X.                                          00002210
           02 MCAPPROO  PIC X(5).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MLAPPROA  PIC X.                                          00002240
           02 MLAPPROC  PIC X.                                          00002250
           02 MLAPPROP  PIC X.                                          00002260
           02 MLAPPROH  PIC X.                                          00002270
           02 MLAPPROV  PIC X.                                          00002280
           02 MLAPPROO  PIC X(20).                                      00002290
           02 MSTATO OCCURS   11 TIMES .                                00002300
             03 FILLER       PIC X(2).                                  00002310
             03 MCFAM2A      PIC X.                                     00002320
             03 MCFAM2C PIC X.                                          00002330
             03 MCFAM2P PIC X.                                          00002340
             03 MCFAM2H PIC X.                                          00002350
             03 MCFAM2V PIC X.                                          00002360
             03 MCFAM2O      PIC X(5).                                  00002370
             03 FILLER       PIC X(2).                                  00002380
             03 MLENSEIA     PIC X.                                     00002390
             03 MLENSEIC     PIC X.                                     00002400
             03 MLENSEIP     PIC X.                                     00002410
             03 MLENSEIH     PIC X.                                     00002420
             03 MLENSEIV     PIC X.                                     00002430
             03 MLENSEIO     PIC X(15).                                 00002440
             03 FILLER       PIC X(2).                                  00002450
             03 MLIBEMPA     PIC X.                                     00002460
             03 MLIBEMPC     PIC X.                                     00002470
             03 MLIBEMPP     PIC X.                                     00002480
             03 MLIBEMPH     PIC X.                                     00002490
             03 MLIBEMPV     PIC X.                                     00002500
             03 MLIBEMPO     PIC X(15).                                 00002510
             03 FILLER       PIC X(2).                                  00002520
             03 MQARTGA      PIC X.                                     00002530
             03 MQARTGC PIC X.                                          00002540
             03 MQARTGP PIC X.                                          00002550
             03 MQARTGH PIC X.                                          00002560
             03 MQARTGV PIC X.                                          00002570
             03 MQARTGO      PIC X(4).                                  00002580
             03 FILLER       PIC X(2).                                  00002590
             03 MQRELINA     PIC X.                                     00002600
             03 MQRELINC     PIC X.                                     00002610
             03 MQRELINP     PIC X.                                     00002620
             03 MQRELINH     PIC X.                                     00002630
             03 MQRELINV     PIC X.                                     00002640
             03 MQRELINO     PIC X(4).                                  00002650
             03 FILLER       PIC X(2).                                  00002660
             03 MQARTINA     PIC X.                                     00002670
             03 MQARTINC     PIC X.                                     00002680
             03 MQARTINP     PIC X.                                     00002690
             03 MQARTINH     PIC X.                                     00002700
             03 MQARTINV     PIC X.                                     00002710
             03 MQARTINO     PIC X(4).                                  00002720
             03 FILLER       PIC X(2).                                  00002730
             03 MQAA    PIC X.                                          00002740
             03 MQAC    PIC X.                                          00002750
             03 MQAP    PIC X.                                          00002760
             03 MQAH    PIC X.                                          00002770
             03 MQAV    PIC X.                                          00002780
             03 MQAO    PIC X(4).                                       00002790
             03 FILLER       PIC X(2).                                  00002800
             03 MQASA   PIC X.                                          00002810
             03 MQASC   PIC X.                                          00002820
             03 MQASP   PIC X.                                          00002830
             03 MQASH   PIC X.                                          00002840
             03 MQASV   PIC X.                                          00002850
             03 MQASO   PIC X(4).                                       00002860
             03 FILLER       PIC X(2).                                  00002870
             03 MQNAA   PIC X.                                          00002880
             03 MQNAC   PIC X.                                          00002890
             03 MQNAP   PIC X.                                          00002900
             03 MQNAH   PIC X.                                          00002910
             03 MQNAV   PIC X.                                          00002920
             03 MQNAO   PIC X(4).                                       00002930
             03 FILLER       PIC X(2).                                  00002940
             03 MQTOTALA     PIC X.                                     00002950
             03 MQTOTALC     PIC X.                                     00002960
             03 MQTOTALP     PIC X.                                     00002970
             03 MQTOTALH     PIC X.                                     00002980
             03 MQTOTALV     PIC X.                                     00002990
             03 MQTOTALO     PIC X(4).                                  00003000
           02 FILLER    PIC X(2).                                       00003010
           02 MZONCMDA  PIC X.                                          00003020
           02 MZONCMDC  PIC X.                                          00003030
           02 MZONCMDP  PIC X.                                          00003040
           02 MZONCMDH  PIC X.                                          00003050
           02 MZONCMDV  PIC X.                                          00003060
           02 MZONCMDO  PIC X(15).                                      00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MLIBERRA  PIC X.                                          00003090
           02 MLIBERRC  PIC X.                                          00003100
           02 MLIBERRP  PIC X.                                          00003110
           02 MLIBERRH  PIC X.                                          00003120
           02 MLIBERRV  PIC X.                                          00003130
           02 MLIBERRO  PIC X(58).                                      00003140
           02 FILLER    PIC X(2).                                       00003150
           02 MCODTRAA  PIC X.                                          00003160
           02 MCODTRAC  PIC X.                                          00003170
           02 MCODTRAP  PIC X.                                          00003180
           02 MCODTRAH  PIC X.                                          00003190
           02 MCODTRAV  PIC X.                                          00003200
           02 MCODTRAO  PIC X(4).                                       00003210
           02 FILLER    PIC X(2).                                       00003220
           02 MCICSA    PIC X.                                          00003230
           02 MCICSC    PIC X.                                          00003240
           02 MCICSP    PIC X.                                          00003250
           02 MCICSH    PIC X.                                          00003260
           02 MCICSV    PIC X.                                          00003270
           02 MCICSO    PIC X(5).                                       00003280
           02 FILLER    PIC X(2).                                       00003290
           02 MNETNAMA  PIC X.                                          00003300
           02 MNETNAMC  PIC X.                                          00003310
           02 MNETNAMP  PIC X.                                          00003320
           02 MNETNAMH  PIC X.                                          00003330
           02 MNETNAMV  PIC X.                                          00003340
           02 MNETNAMO  PIC X(8).                                       00003350
           02 FILLER    PIC X(2).                                       00003360
           02 MSCREENA  PIC X.                                          00003370
           02 MSCREENC  PIC X.                                          00003380
           02 MSCREENP  PIC X.                                          00003390
           02 MSCREENH  PIC X.                                          00003400
           02 MSCREENV  PIC X.                                          00003410
           02 MSCREENO  PIC X(4).                                       00003420
                                                                                
