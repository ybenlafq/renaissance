      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************00000010
      * COBOL DECLARATION FOR TABLE DSA000.RTPR24                      *00000020
      ******************************************************************00000030
       01  RVPR2400.                                                    00000040
           10 PR24-CTYPE           PIC X(1).                            00000050
           10 PR24-NENTCDE         PIC X(5).                            00000060
           10 PR24-CTYPTXP         PIC X(5).                            00000070
           10 PR24-NCHRONO         PIC X(5).                            00000080
           10 PR24-CTYPCND         PIC X(5).                            00000090
           10 PR24-CTYPPREST       PIC X(5).                            00000100
           10 PR24-TAUX            PIC S9(3)V99 USAGE COMP-3.           00000110
           10 PR24-DEFFET          PIC X(8).                            00000120
           10 PR24-DFINEFFET       PIC X(8).                            00000130
           10 PR24-DMAJ            PIC X(8).                            00000140
           10 PR24-DSYST           PIC S9(13)V USAGE COMP-3.            00000150
      ******************************************************************00000160
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 8       *00000170
      ******************************************************************00000180
      ******************************************************************00000190
      * INDICATOR VARIABLE STRUCTURE                                   *00000200
      ******************************************************************00000210
       01  RVPR2400-FLAGS.                                              00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR24-CTYPE-F         PIC S9(4) COMP.                      00000230
      *--                                                                       
           10 PR24-CTYPE-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR24-NENTCDE-F       PIC S9(4) COMP.                      00000240
      *--                                                                       
           10 PR24-NENTCDE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR24-CTYPTXP-F       PIC S9(4) COMP.                      00000250
      *--                                                                       
           10 PR24-CTYPTXP-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR24-NCHRONO-F       PIC S9(4) COMP.                      00000260
      *--                                                                       
           10 PR24-NCHRONO-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR24-CTYPCND-F       PIC S9(4) COMP.                      00000270
      *--                                                                       
           10 PR24-CTYPCND-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR24-CTYPPREST-F     PIC S9(4) COMP.                      00000280
      *--                                                                       
           10 PR24-CTYPPREST-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR24-TAUX-F          PIC S9(4) COMP.                      00000290
      *--                                                                       
           10 PR24-TAUX-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR24-DEFFET-F        PIC S9(4) COMP.                      00000300
      *--                                                                       
           10 PR24-DEFFET-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR24-DFINEFFET-F     PIC S9(4) COMP.                      00000310
      *--                                                                       
           10 PR24-DFINEFFET-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR24-DMAJ-F          PIC S9(4) COMP.                      00000320
      *--                                                                       
           10 PR24-DMAJ-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR24-DSYST-F         PIC S9(4) COMP.                      00000330
      *                                                                         
      *--                                                                       
           10 PR24-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
      *}                                                                        
