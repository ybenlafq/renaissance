           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FGPCG DEF� PLAN COMPTABLE GROUPE       *        
      *----------------------------------------------------------------*        
       01  RVGA01LX.                                                            
           05  FGPCG-CTABLEG2    PIC X(15).                                     
           05  FGPCG-CTABLEG2-REDEF REDEFINES FGPCG-CTABLEG2.                   
               10  FGPCG-COMPTE          PIC X(06).                             
           EXEC SQL END DECLARE SECTION END-EXEC.
               10  FGPCG-COMPTE-N       REDEFINES FGPCG-COMPTE                  
                                         PIC 9(06).                             
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  FGPCG-WTABLEG     PIC X(80).                                     
           05  FGPCG-WTABLEG-REDEF  REDEFINES FGPCG-WTABLEG.                    
               10  FGPCG-WACTIF          PIC X(01).                             
               10  FGPCG-LIBELLE         PIC X(30).                             
               10  FGPCG-TYPE            PIC X(01).                             
               10  FGPCG-NAUX            PIC X(01).                             
               10  FGPCG-TYPCPTE         PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01LX-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGPCG-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FGPCG-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGPCG-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FGPCG-WTABLEG-F   PIC S9(4) COMP-5.                              
      *}
           EXEC SQL END DECLARE SECTION END-EXEC.
 
                                                                                
