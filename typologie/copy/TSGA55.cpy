      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   TS : ENTITES DE COMMANDE FOURNISSEURS LIEES A L'ARTICLE     *         
      *        POUR MISE A JOUR VUE RVGA5500                          *         
      *****************************************************************         
      *                                                               *         
       01  TS-GA55.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GA55-LONG                 PIC S9(5) COMP-3                     
                                           VALUE +34.                           
           02 TS-GA55-DONNEES.                                                  
      *----------------------------------  TABLE ARTICLE FOURNISSEUR            
            03 TS-GA55-NCODIC              PIC X(7).                            
            03 TS-GA55-NENTCDE             PIC X(5).                            
            03 TS-GA55-WENTCDE             PIC X(1).                            
      *----------------------------------  MODE DE MAJ                          
      *                                         ' ' : PAS DE MAJ                
      *                                         'C' : CREATION                  
      *                                         'M' : MODIFICATION              
      *                                         'S' : SUPPRESSION               
            03 TS-GA55-CMAJ                PIC X(1).                            
      *----------------------------------  LIBELLE ENTITE DE COMMANDE           
            03 TS-GA55-LENTCDE             PIC X(20).                           
                                                                                
