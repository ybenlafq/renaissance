      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      * E0066                                                                   
      ******************************************************************        
      * DSA057 01/03/04 SUPPORT EVOLUTION                                       
      *                 GESTION AUTOMATIQUE ATTRIBUTS NUMERIQUES                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVGA2410.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVGA2410.                                                             
      *}                                                                        
           05 GA24-CDESCRIPTIF PIC X(5).                                        
           05 GA24-LDESCRIPTIF PIC X(20).                                       
           05 GA24-CTYPE       PIC X.                                           
           05 GA24-NBDEC       PIC X.                                           
           05 GA24-NMIN        PIC X(5).                                        
           05 GA24-NMAX        PIC X(5).                                        
           05 GA24-CMESURE     PIC X(5).                                        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVGA2410-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVGA2410-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 GA24-CDESCRIPTIF-F PIC S9(4) COMP.                                
      *--                                                                       
           05 GA24-CDESCRIPTIF-F PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 GA24-LDESCRIPTIF-F PIC S9(4) COMP.                                
      *--                                                                       
           05 GA24-LDESCRIPTIF-F PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 GA24-CTYPE-F       PIC S9(4) COMP.                                
      *--                                                                       
           05 GA24-CTYPE-F       PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 GA24-NBDEC-F       PIC S9(4) COMP.                                
      *--                                                                       
           05 GA24-NBDEC-F       PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 GA24-NMIN-F        PIC S9(4) COMP.                                
      *--                                                                       
           05 GA24-NMIN-F        PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 GA24-NMAX-F        PIC S9(4) COMP.                                
      *--                                                                       
           05 GA24-NMAX-F        PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 GA24-CMESURE-F     PIC S9(4) COMP.                                
      *                                                                         
      *--                                                                       
           05 GA24-CMESURE-F     PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
