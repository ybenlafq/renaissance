      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA03   EGA03                                              00000020
      ***************************************************************** 00000030
       01   EGA03I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MFONCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMEROL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNUMEROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNUMEROF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNUMEROI  PIC X(5).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOML     COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNOML COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNOMF     PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNOMI     PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVISAL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCDEVISAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEVISAF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCDEVISAI      PIC X(3).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADRES1L  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MADRES1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MADRES1F  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MADRES1I  PIC X(32).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVISEL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MCDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEVISEF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCDEVISEI      PIC X(3).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADRES2L  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MADRES2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MADRES2F  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MADRES2I  PIC X(32).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEVISEL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MLDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLDEVISEF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLDEVISEI      PIC X(23).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODPOSL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCODPOSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODPOSF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCODPOSI  PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVILLEL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MVILLEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MVILLEF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MVILLEI   PIC X(26).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFFETL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDEFFETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEFFETF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDEFFETI  PIC X(10).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELEPHL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MTELEPHL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELEPHF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MTELEPHI  PIC X(15).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPAYSL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCPAYSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCPAYSF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCPAYSI   PIC X(2).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPAYSL   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MLPAYSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLPAYSF   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLPAYSI   PIC X(6).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEDIL     COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MEDIL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MEDIF     PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MEDII     PIC X.                                          00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFFEDIL      COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MDEFFEDIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDEFFEDIF      PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MDEFFEDII      PIC X(10).                                 00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELEXL   COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MTELEXL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTELEXF   PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MTELEXI   PIC X(15).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWIMPORTL      COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MWIMPORTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWIMPORTF      PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MWIMPORTI      PIC X.                                     00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEANL     COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MEANL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MEANF     PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MEANI     PIC X.                                          00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMULTDEPL      COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MMULTDEPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MMULTDEPF      PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MMULTDEPI      PIC X.                                     00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFFCALL      COMP PIC S9(4).                            00000940
      *--                                                                       
           02 MDEFFCALL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDEFFCALF      PIC X.                                     00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MDEFFCALI      PIC X(10).                                 00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCECORGL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MCECORGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCECORGF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MCECORGI  PIC X(3).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLECORGL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MLECORGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLECORGF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MLECORGI  PIC X(10).                                      00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCOMPTEL      COMP PIC S9(4).                            00001060
      *--                                                                       
           02 MCCOMPTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCCOMPTEF      PIC X.                                     00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MCCOMPTEI      PIC X(2).                                  00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMPTEL      COMP PIC S9(4).                            00001100
      *--                                                                       
           02 MLCOMPTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLCOMPTEF      PIC X.                                     00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MLCOMPTEI      PIC X(20).                                 00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MKHKVENL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MKHKVENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MKHKVENF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MKHKVENI  PIC X.                                          00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCROSSL   COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MCROSSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCROSSF   PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MCROSSI   PIC X.                                          00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVALOL    COMP PIC S9(4).                                         
      *--                                                                       
           02 MVALOL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MVALOF    PIC X.                                                  
           02 FILLER    PIC X(4).                                               
           02 MVALOI    PIC X.                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVALOL   COMP PIC S9(4).                                         
      *--                                                                       
           02 MDVALOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDVALOF   PIC X.                                                  
           02 FILLER    PIC X(4).                                               
           02 MDVALOI   PIC X(10).                                              
           02 MINTERLI OCCURS   5 TIMES .                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODIN1L     COMP PIC S9(4).                            00001230
      *--                                                                       
             03 MCODIN1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCODIN1F     PIC X.                                     00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MCODIN1I     PIC X(5).                                  00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBIN1L     COMP PIC S9(4).                            00001270
      *--                                                                       
             03 MLIBIN1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLIBIN1F     PIC X.                                     00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MLIBIN1I     PIC X(20).                                 00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODIN2L     COMP PIC S9(4).                            00001310
      *--                                                                       
             03 MCODIN2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCODIN2F     PIC X.                                     00001320
             03 FILLER  PIC X(4).                                       00001330
             03 MCODIN2I     PIC X(5).                                  00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBIN2L     COMP PIC S9(4).                            00001350
      *--                                                                       
             03 MLIBIN2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLIBIN2F     PIC X.                                     00001360
             03 FILLER  PIC X(4).                                       00001370
             03 MLIBIN2I     PIC X(20).                                 00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MZONCMDI  PIC X(15).                                      00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MLIBERRI  PIC X(56).                                      00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MCODTRAI  PIC X(4).                                       00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MCICSI    PIC X(5).                                       00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MNETNAMI  PIC X(8).                                       00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001590
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001600
           02 FILLER    PIC X(4).                                       00001610
           02 MSCREENI  PIC X(4).                                       00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MORDERSL  COMP PIC S9(4).                                 00001630
      *--                                                                       
           02 MORDERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MORDERSF  PIC X.                                          00001640
           02 FILLER    PIC X(4).                                       00001650
           02 MORDERSI  PIC X.                                          00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCALAGEL  COMP PIC S9(4).                                 00001670
      *--                                                                       
           02 MCALAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCALAGEF  PIC X.                                          00001680
           02 FILLER    PIC X(4).                                       00001690
           02 MCALAGEI  PIC X.                                          00001700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEDITCDEL      COMP PIC S9(4).                            00001710
      *--                                                                       
           02 MEDITCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MEDITCDEF      PIC X.                                     00001720
           02 FILLER    PIC X(4).                                       00001730
           02 MEDITCDEI      PIC X.                                     00001740
      ***************************************************************** 00001750
      * SDF: EGA03   EGA03                                              00001760
      ***************************************************************** 00001770
       01   EGA03O REDEFINES EGA03I.                                    00001780
           02 FILLER    PIC X(12).                                      00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MDATJOUA  PIC X.                                          00001810
           02 MDATJOUC  PIC X.                                          00001820
           02 MDATJOUP  PIC X.                                          00001830
           02 MDATJOUH  PIC X.                                          00001840
           02 MDATJOUV  PIC X.                                          00001850
           02 MDATJOUO  PIC X(10).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MTIMJOUA  PIC X.                                          00001880
           02 MTIMJOUC  PIC X.                                          00001890
           02 MTIMJOUP  PIC X.                                          00001900
           02 MTIMJOUH  PIC X.                                          00001910
           02 MTIMJOUV  PIC X.                                          00001920
           02 MTIMJOUO  PIC X(5).                                       00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MFONCA    PIC X.                                          00001950
           02 MFONCC    PIC X.                                          00001960
           02 MFONCP    PIC X.                                          00001970
           02 MFONCH    PIC X.                                          00001980
           02 MFONCV    PIC X.                                          00001990
           02 MFONCO    PIC X(3).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MNUMEROA  PIC X.                                          00002020
           02 MNUMEROC  PIC X.                                          00002030
           02 MNUMEROP  PIC X.                                          00002040
           02 MNUMEROH  PIC X.                                          00002050
           02 MNUMEROV  PIC X.                                          00002060
           02 MNUMEROO  PIC X(5).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MNOMA     PIC X.                                          00002090
           02 MNOMC     PIC X.                                          00002100
           02 MNOMP     PIC X.                                          00002110
           02 MNOMH     PIC X.                                          00002120
           02 MNOMV     PIC X.                                          00002130
           02 MNOMO     PIC X(20).                                      00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MCDEVISAA      PIC X.                                     00002160
           02 MCDEVISAC PIC X.                                          00002170
           02 MCDEVISAP PIC X.                                          00002180
           02 MCDEVISAH PIC X.                                          00002190
           02 MCDEVISAV PIC X.                                          00002200
           02 MCDEVISAO      PIC X(3).                                  00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MADRES1A  PIC X.                                          00002230
           02 MADRES1C  PIC X.                                          00002240
           02 MADRES1P  PIC X.                                          00002250
           02 MADRES1H  PIC X.                                          00002260
           02 MADRES1V  PIC X.                                          00002270
           02 MADRES1O  PIC X(32).                                      00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MCDEVISEA      PIC X.                                     00002300
           02 MCDEVISEC PIC X.                                          00002310
           02 MCDEVISEP PIC X.                                          00002320
           02 MCDEVISEH PIC X.                                          00002330
           02 MCDEVISEV PIC X.                                          00002340
           02 MCDEVISEO      PIC X(3).                                  00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MADRES2A  PIC X.                                          00002370
           02 MADRES2C  PIC X.                                          00002380
           02 MADRES2P  PIC X.                                          00002390
           02 MADRES2H  PIC X.                                          00002400
           02 MADRES2V  PIC X.                                          00002410
           02 MADRES2O  PIC X(32).                                      00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MLDEVISEA      PIC X.                                     00002440
           02 MLDEVISEC PIC X.                                          00002450
           02 MLDEVISEP PIC X.                                          00002460
           02 MLDEVISEH PIC X.                                          00002470
           02 MLDEVISEV PIC X.                                          00002480
           02 MLDEVISEO      PIC X(23).                                 00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MCODPOSA  PIC X.                                          00002510
           02 MCODPOSC  PIC X.                                          00002520
           02 MCODPOSP  PIC X.                                          00002530
           02 MCODPOSH  PIC X.                                          00002540
           02 MCODPOSV  PIC X.                                          00002550
           02 MCODPOSO  PIC X(5).                                       00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MVILLEA   PIC X.                                          00002580
           02 MVILLEC   PIC X.                                          00002590
           02 MVILLEP   PIC X.                                          00002600
           02 MVILLEH   PIC X.                                          00002610
           02 MVILLEV   PIC X.                                          00002620
           02 MVILLEO   PIC X(26).                                      00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MDEFFETA  PIC X.                                          00002650
           02 MDEFFETC  PIC X.                                          00002660
           02 MDEFFETP  PIC X.                                          00002670
           02 MDEFFETH  PIC X.                                          00002680
           02 MDEFFETV  PIC X.                                          00002690
           02 MDEFFETO  PIC X(10).                                      00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MTELEPHA  PIC X.                                          00002720
           02 MTELEPHC  PIC X.                                          00002730
           02 MTELEPHP  PIC X.                                          00002740
           02 MTELEPHH  PIC X.                                          00002750
           02 MTELEPHV  PIC X.                                          00002760
           02 MTELEPHO  PIC X(15).                                      00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MCPAYSA   PIC X.                                          00002790
           02 MCPAYSC   PIC X.                                          00002800
           02 MCPAYSP   PIC X.                                          00002810
           02 MCPAYSH   PIC X.                                          00002820
           02 MCPAYSV   PIC X.                                          00002830
           02 MCPAYSO   PIC X(2).                                       00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MLPAYSA   PIC X.                                          00002860
           02 MLPAYSC   PIC X.                                          00002870
           02 MLPAYSP   PIC X.                                          00002880
           02 MLPAYSH   PIC X.                                          00002890
           02 MLPAYSV   PIC X.                                          00002900
           02 MLPAYSO   PIC X(6).                                       00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MEDIA     PIC X.                                          00002930
           02 MEDIC     PIC X.                                          00002940
           02 MEDIP     PIC X.                                          00002950
           02 MEDIH     PIC X.                                          00002960
           02 MEDIV     PIC X.                                          00002970
           02 MEDIO     PIC X.                                          00002980
           02 FILLER    PIC X(2).                                       00002990
           02 MDEFFEDIA      PIC X.                                     00003000
           02 MDEFFEDIC PIC X.                                          00003010
           02 MDEFFEDIP PIC X.                                          00003020
           02 MDEFFEDIH PIC X.                                          00003030
           02 MDEFFEDIV PIC X.                                          00003040
           02 MDEFFEDIO      PIC X(10).                                 00003050
           02 FILLER    PIC X(2).                                       00003060
           02 MTELEXA   PIC X.                                          00003070
           02 MTELEXC   PIC X.                                          00003080
           02 MTELEXP   PIC X.                                          00003090
           02 MTELEXH   PIC X.                                          00003100
           02 MTELEXV   PIC X.                                          00003110
           02 MTELEXO   PIC X(15).                                      00003120
           02 FILLER    PIC X(2).                                       00003130
           02 MWIMPORTA      PIC X.                                     00003140
           02 MWIMPORTC PIC X.                                          00003150
           02 MWIMPORTP PIC X.                                          00003160
           02 MWIMPORTH PIC X.                                          00003170
           02 MWIMPORTV PIC X.                                          00003180
           02 MWIMPORTO      PIC X.                                     00003190
           02 FILLER    PIC X(2).                                       00003200
           02 MEANA     PIC X.                                          00003210
           02 MEANC     PIC X.                                          00003220
           02 MEANP     PIC X.                                          00003230
           02 MEANH     PIC X.                                          00003240
           02 MEANV     PIC X.                                          00003250
           02 MEANO     PIC X.                                          00003260
           02 FILLER    PIC X(2).                                       00003270
           02 MMULTDEPA      PIC X.                                     00003280
           02 MMULTDEPC PIC X.                                          00003290
           02 MMULTDEPP PIC X.                                          00003300
           02 MMULTDEPH PIC X.                                          00003310
           02 MMULTDEPV PIC X.                                          00003320
           02 MMULTDEPO      PIC X.                                     00003330
           02 FILLER    PIC X(2).                                       00003340
           02 MDEFFCALA      PIC X.                                     00003350
           02 MDEFFCALC PIC X.                                          00003360
           02 MDEFFCALP PIC X.                                          00003370
           02 MDEFFCALH PIC X.                                          00003380
           02 MDEFFCALV PIC X.                                          00003390
           02 MDEFFCALO      PIC X(10).                                 00003400
           02 FILLER    PIC X(2).                                       00003410
           02 MCECORGA  PIC X.                                          00003420
           02 MCECORGC  PIC X.                                          00003430
           02 MCECORGP  PIC X.                                          00003440
           02 MCECORGH  PIC X.                                          00003450
           02 MCECORGV  PIC X.                                          00003460
           02 MCECORGO  PIC X(3).                                       00003470
           02 FILLER    PIC X(2).                                       00003480
           02 MLECORGA  PIC X.                                          00003490
           02 MLECORGC  PIC X.                                          00003500
           02 MLECORGP  PIC X.                                          00003510
           02 MLECORGH  PIC X.                                          00003520
           02 MLECORGV  PIC X.                                          00003530
           02 MLECORGO  PIC X(10).                                      00003540
           02 FILLER    PIC X(2).                                       00003550
           02 MCCOMPTEA      PIC X.                                     00003560
           02 MCCOMPTEC PIC X.                                          00003570
           02 MCCOMPTEP PIC X.                                          00003580
           02 MCCOMPTEH PIC X.                                          00003590
           02 MCCOMPTEV PIC X.                                          00003600
           02 MCCOMPTEO      PIC X(2).                                  00003610
           02 FILLER    PIC X(2).                                       00003620
           02 MLCOMPTEA      PIC X.                                     00003630
           02 MLCOMPTEC PIC X.                                          00003640
           02 MLCOMPTEP PIC X.                                          00003650
           02 MLCOMPTEH PIC X.                                          00003660
           02 MLCOMPTEV PIC X.                                          00003670
           02 MLCOMPTEO      PIC X(20).                                 00003680
           02 FILLER    PIC X(2).                                       00003690
           02 MKHKVENA  PIC X.                                          00003700
           02 MKHKVENC  PIC X.                                          00003710
           02 MKHKVENP  PIC X.                                          00003720
           02 MKHKVENH  PIC X.                                          00003730
           02 MKHKVENV  PIC X.                                          00003740
           02 MKHKVENO  PIC X.                                          00003750
           02 FILLER    PIC X(2).                                       00003760
           02 MCROSSA   PIC X.                                          00003770
           02 MCROSSC   PIC X.                                          00003780
           02 MCROSSP   PIC X.                                          00003790
           02 MCROSSH   PIC X.                                          00003800
           02 MCROSSV   PIC X.                                          00003810
           02 MCROSSO   PIC X.                                          00003820
           02 FILLER    PIC X(2).                                               
           02 MVALOA    PIC X.                                                  
           02 MVALOC    PIC X.                                                  
           02 MVALOP    PIC X.                                                  
           02 MVALOH    PIC X.                                                  
           02 MVALOV    PIC X.                                                  
           02 MVALOO    PIC X.                                                  
           02 FILLER    PIC X(2).                                               
           02 MDVALOA   PIC X.                                                  
           02 MDVALOC   PIC X.                                                  
           02 MDVALOP   PIC X.                                                  
           02 MDVALOH   PIC X.                                                  
           02 MDVALOV   PIC X.                                                  
           02 MDVALOO   PIC X(10).                                              
           02 MINTERLO OCCURS   5 TIMES .                                       
             03 FILLER       PIC X(2).                                  00003840
             03 MCODIN1A     PIC X.                                     00003850
             03 MCODIN1C     PIC X.                                     00003860
             03 MCODIN1P     PIC X.                                     00003870
             03 MCODIN1H     PIC X.                                     00003880
             03 MCODIN1V     PIC X.                                     00003890
             03 MCODIN1O     PIC X(5).                                  00003900
             03 FILLER       PIC X(2).                                  00003910
             03 MLIBIN1A     PIC X.                                     00003920
             03 MLIBIN1C     PIC X.                                     00003930
             03 MLIBIN1P     PIC X.                                     00003940
             03 MLIBIN1H     PIC X.                                     00003950
             03 MLIBIN1V     PIC X.                                     00003960
             03 MLIBIN1O     PIC X(20).                                 00003970
             03 FILLER       PIC X(2).                                  00003980
             03 MCODIN2A     PIC X.                                     00003990
             03 MCODIN2C     PIC X.                                     00004000
             03 MCODIN2P     PIC X.                                     00004010
             03 MCODIN2H     PIC X.                                     00004020
             03 MCODIN2V     PIC X.                                     00004030
             03 MCODIN2O     PIC X(5).                                  00004040
             03 FILLER       PIC X(2).                                  00004050
             03 MLIBIN2A     PIC X.                                     00004060
             03 MLIBIN2C     PIC X.                                     00004070
             03 MLIBIN2P     PIC X.                                     00004080
             03 MLIBIN2H     PIC X.                                     00004090
             03 MLIBIN2V     PIC X.                                     00004100
             03 MLIBIN2O     PIC X(20).                                 00004110
           02 FILLER    PIC X(2).                                       00004120
           02 MZONCMDA  PIC X.                                          00004130
           02 MZONCMDC  PIC X.                                          00004140
           02 MZONCMDP  PIC X.                                          00004150
           02 MZONCMDH  PIC X.                                          00004160
           02 MZONCMDV  PIC X.                                          00004170
           02 MZONCMDO  PIC X(15).                                      00004180
           02 FILLER    PIC X(2).                                       00004190
           02 MLIBERRA  PIC X.                                          00004200
           02 MLIBERRC  PIC X.                                          00004210
           02 MLIBERRP  PIC X.                                          00004220
           02 MLIBERRH  PIC X.                                          00004230
           02 MLIBERRV  PIC X.                                          00004240
           02 MLIBERRO  PIC X(56).                                      00004250
           02 FILLER    PIC X(2).                                       00004260
           02 MCODTRAA  PIC X.                                          00004270
           02 MCODTRAC  PIC X.                                          00004280
           02 MCODTRAP  PIC X.                                          00004290
           02 MCODTRAH  PIC X.                                          00004300
           02 MCODTRAV  PIC X.                                          00004310
           02 MCODTRAO  PIC X(4).                                       00004320
           02 FILLER    PIC X(2).                                       00004330
           02 MCICSA    PIC X.                                          00004340
           02 MCICSC    PIC X.                                          00004350
           02 MCICSP    PIC X.                                          00004360
           02 MCICSH    PIC X.                                          00004370
           02 MCICSV    PIC X.                                          00004380
           02 MCICSO    PIC X(5).                                       00004390
           02 FILLER    PIC X(2).                                       00004400
           02 MNETNAMA  PIC X.                                          00004410
           02 MNETNAMC  PIC X.                                          00004420
           02 MNETNAMP  PIC X.                                          00004430
           02 MNETNAMH  PIC X.                                          00004440
           02 MNETNAMV  PIC X.                                          00004450
           02 MNETNAMO  PIC X(8).                                       00004460
           02 FILLER    PIC X(2).                                       00004470
           02 MSCREENA  PIC X.                                          00004480
           02 MSCREENC  PIC X.                                          00004490
           02 MSCREENP  PIC X.                                          00004500
           02 MSCREENH  PIC X.                                          00004510
           02 MSCREENV  PIC X.                                          00004520
           02 MSCREENO  PIC X(4).                                       00004530
           02 FILLER    PIC X(2).                                       00004540
           02 MORDERSA  PIC X.                                          00004550
           02 MORDERSC  PIC X.                                          00004560
           02 MORDERSP  PIC X.                                          00004570
           02 MORDERSH  PIC X.                                          00004580
           02 MORDERSV  PIC X.                                          00004590
           02 MORDERSO  PIC X.                                          00004600
           02 FILLER    PIC X(2).                                       00004610
           02 MCALAGEA  PIC X.                                          00004620
           02 MCALAGEC  PIC X.                                          00004630
           02 MCALAGEP  PIC X.                                          00004640
           02 MCALAGEH  PIC X.                                          00004650
           02 MCALAGEV  PIC X.                                          00004660
           02 MCALAGEO  PIC X.                                          00004670
           02 FILLER    PIC X(2).                                       00004680
           02 MEDITCDEA      PIC X.                                     00004690
           02 MEDITCDEC PIC X.                                          00004700
           02 MEDITCDEP PIC X.                                          00004710
           02 MEDITCDEH PIC X.                                          00004720
           02 MEDITCDEV PIC X.                                          00004730
           02 MEDITCDEO      PIC X.                                     00004740
                                                                                
