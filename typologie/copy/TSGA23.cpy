      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************          
      *  TR GA23  : SELECTION DES FAMILLES DE PRESTATION                        
      ****************************************************************          
       01  TS-GA23.                                                             
         05  TS-LONG           PIC S9(4) COMP-3 VALUE 270.                      
         05  TS-DONNEES.                                                        
           07  TS-OCCURS       OCCURS 10.                                       
               10  TS-CAGREPRE     PIC  X(5).                                   
               10  TS-LAGREPRE     PIC  X(20).                                  
               10  TS-NAGREGATED   PIC  X(2).                                   
                                                                                
