      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *-----------------------------------------------------------------        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-GG59-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-GG59-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA -----------------------------------------        
           02 FILLER-COM-AIDA      PIC X(100).                                  
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS --------------------------        
           02 COMM-CICS-APPLID     PIC X(8).                                    
           02 COMM-CICS-NETNAM     PIC X(8).                                    
           02 COMM-CICS-TRANSA     PIC X(4).                                    
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------------        
           02 COMM-DATE-SIECLE     PIC XX.                                      
           02 COMM-DATE-ANNEE      PIC XX.                                      
           02 COMM-DATE-MOIS       PIC XX.                                      
           02 COMM-DATE-JOUR       PIC 99.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA       PIC 999.                                     
           02 COMM-DATE-QNT0       PIC 99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX       PIC 9.                                       
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM        PIC 9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC     PIC XXX.                                     
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC    PIC XXX.                                     
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                                    
           02 COMM-DATE-AAMMJJ     PIC X(6).                                    
           02 COMM-DATE-JJMMSSAA   PIC X(8).                                    
           02 COMM-DATE-JJMMAA     PIC X(6).                                    
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                                    
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS   PIC 99.                                      
              05 COMM-DATE-SEMAA   PIC 99.                                      
              05 COMM-DATE-SEMNU   PIC 99.                                      
      *                                                                         
           02 COMM-DATE-FILLER     PIC X(08).                                   
      *                                                                         
      * ZONES RESERVEES TRAITEMENT DU SWAP -----------------------------        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                     
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 180 PIC X(1).                               
      *                                                                         
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*        
      *>>>>>>>>>>>>>>>>> ZONES RESERVEES APPLICATIVES <<<<<<<<<<<<<<<<<*        
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<< 3694 <<<<<<<<<*        
      *                                                                         
      *----------------------------------------------------------------*        
      * MENU GENERAL DE L'APPLICATION (PROGRAMME TGG59)                *        
      *----------------------------------------------------------------*        
           02 COMM-GG59-APPLI.                                                  
      *------------------------------------------------- 2162 ---------*        
              03 COMM-GG59-CENTRALISE        PIC X.                             
              03 COMM-GG59-TOUT              PIC X.                             
              03 COMM-GG59-NSOCIETE          PIC X(3).                          
              03 COMM-GG59-MESSAGE           PIC X(79).                 01720001
              03 COMM-GG59-SIEGE             PIC X.                             
              03 COMM-GG59-NZONPRIX          PIC X(2).                          
              03 COMM-GG59-NZPMAG            PIC X(2).                          
              03 COMM-GG59-NMAG              PIC X(3).                          
              03 COMM-GG59-NCODIC            PIC X(07).                         
              03 COMM-GG59-CHEFP             PIC X(05).                         
              03 COMM-GG59-CRAYON            PIC X(05).                         
              03 COMM-GG59-CMARQ             PIC X(05).                         
              03 COMM-GG59-NZPCONC           PIC X(2).                          
              03 COMM-GG59-NCONC             PIC X(4).                          
              03 COMM-GG59-MOUCHARD          PIC X(01).                         
              03 COMM-GG59-DATE              PIC X(08).                         
              03 COMM-GG59-DATE2             PIC X(08).                         
              03 COMM-GG59-DATEMIN           PIC X(08).                         
              03 COMM-GG59-DATEMIN2          PIC X(08).                         
              03 COMM-GG59-ENSFAM            PIC X(2000).                       
              03 COMM-GG59-PAGE              PIC 9(4).                          
              03 COMM-GG59-PAGETOT           PIC 9(4).                          
              03 COMM-GG59-AFF-PRIX          PIC  X(01).                        
                 88  PRIX-AFFICHE             VALUE 'O'.                        
                 88  PRIX-NON-AFFICHE         VALUE 'N'.                        
      *----------------------------------------------------------------*        
      * MENU CONSULTATION AU CODIC (PROGRAMME TGG57)                   *        
      *----------------------------------------------------------------*        
           02 COMM-GG57-APPLI.                                                  
      *------------------------------------------------- 112 ----------*        
              03 COMM-GG57-NZONPRIX          PIC X(2).                          
              03 COMM-GG57-NCODIC            PIC X(7).                          
              03 COMM-GG57-NLIEU             PIC X(3).                          
              03 COMM-GG57-NCONC             PIC X(4).                          
              03 COMM-GG57-LCONC             PIC X(15).                         
              03 COMM-GG57-PAGE              PIC 9(2).                          
              03 COMM-GG57-PAGETOT           PIC 9(2).                          
              03 COMM-GG57-CFAM              PIC X(05).                         
              03 COMM-GG57-LFAM              PIC X(20).                         
              03 COMM-GG57-CAPPRO            PIC X(03).                         
              03 COMM-GG57-CMARQ             PIC X(05).                         
              03 COMM-GG57-LMARQ             PIC X(20).                         
              03 COMM-GG57-COMPAC            PIC X(03).                         
              03 COMM-GG57-REF               PIC X(20).                         
              03 COMM-GG57-CEXPO             PIC X(01).                         
      *--------------------------------------------------RESTE 1420     00743701
           02 COMM-GG59-LIBRE          PIC X(1420).                     01720001
      *-------------------------------------------------------          00743701
      ***************************************************************** 02170001
                                                                                
