      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PROGE GENERALITES PROFIL FICHE INFO    *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01GF.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01GF.                                                            
      *}                                                                        
           05  PROGE-CTABLEG2    PIC X(15).                                     
           05  PROGE-CTABLEG2-REDEF REDEFINES PROGE-CTABLEG2.                   
               10  PROGE-CODPRO          PIC X(10).                             
           05  PROGE-WTABLEG     PIC X(80).                                     
           05  PROGE-WTABLEG-REDEF  REDEFINES PROGE-WTABLEG.                    
               10  PROGE-FLAG            PIC X(01).                             
               10  PROGE-FAMCLT          PIC X(30).                             
               10  PROGE-SEPARTXT        PIC X(01).                             
               10  PROGE-DATEFFET        PIC X(08).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  PROGE-DATEFFET-N     REDEFINES PROGE-DATEFFET                
                                         PIC 9(08).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  PROGE-WCOUL           PIC X(07).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01GF-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01GF-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PROGE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PROGE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PROGE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PROGE-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
