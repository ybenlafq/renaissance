      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVPR5700                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   INFORMATIONS REDBOX DANS LA TABLE RVPR5700                            
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPR5700.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPR5700.                                                            
      *}                                                                        
           02  PR57-CPRESTATION                                                 
               PIC X(0005).                                                     
           02  PR57-LPRESTATION50                                               
               PIC X(0050).                                                     
           02  PR57-CAFFICHAGE                                                  
               PIC X(0005).                                                     
           02  PR57-NSEQAFFICH                                                  
               PIC X(0002).                                                     
           02  PR57-CPRISERDV                                                   
               PIC X(0003).                                                     
           02  PR57-CRATTACHE                                                   
               PIC X(0007).                                                     
           02  PR57-QTEMAX                                                      
               PIC X(0003).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
           02  PR57-INFOPRESTA                                                  
               PIC X(0255).                                                     
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           02  PR57-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVPR5700                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPR5700-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPR5700-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR57-CPRESTATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR57-CPRESTATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR57-LPRESTATION50-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR57-LPRESTATION50-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR57-CAFFICHAGE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR57-CAFFICHAGE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR57-NSEQAFFICH-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR57-NSEQAFFICH-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR57-CPRISERDV-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR57-CPRISERDV-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR57-CRATTACHE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR57-CRATTACHE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR57-QTEMAX-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR57-QTEMAX-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR57-INFOPRESTA-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR57-INFOPRESTA-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR57-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR57-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
