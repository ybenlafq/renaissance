      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGG5000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGG5000                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGG5000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGG5000.                                                            
      *}                                                                        
           02  GG50-NCODIC                                                      
               PIC X(0007).                                                     
           02  GG50-DJOUR                                                       
               PIC X(0008).                                                     
           02  GG50-PRMP                                                        
               PIC S9(7)V9(0006) COMP-3.                                        
           02  GG50-PRMPRECYCL                                                  
               PIC S9(7)V9(0006) COMP-3.                                        
           02  GG50-WPRMPUTIL                                                   
               PIC X(0001).                                                     
           02  GG50-QSTOCKINIT                                                  
               PIC S9(9) COMP-3.                                                
           02  GG50-QSTOCKFINAL                                                 
               PIC S9(9) COMP-3.                                                
           02  GG50-WRECYCLSTCK                                                 
               PIC X(0001).                                                     
           02  GG50-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GG50-PCF                                                         
               PIC S9(7)V9(0006) COMP-3.                                        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGG5000                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGG5000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGG5000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG50-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG50-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG50-DJOUR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG50-DJOUR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG50-PRMP-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG50-PRMP-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG50-PRMPRECYCL-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG50-PRMPRECYCL-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG50-WPRMPUTIL-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG50-WPRMPUTIL-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG50-QSTOCKINIT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG50-QSTOCKINIT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG50-QSTOCKFINAL-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG50-QSTOCKFINAL-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG50-WRECYCLSTCK-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG50-WRECYCLSTCK-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG50-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG50-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG50-PCF-F                                                       
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GG50-PCF-F                                                       
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
