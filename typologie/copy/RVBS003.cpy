      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE BS003 REGROUP. FAMILLES COMMISSIONS    *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVBS003.                                                             
           05  BS003-CTABLEG2    PIC X(15).                                     
           05  BS003-CTABLEG2-REDEF REDEFINES BS003-CTABLEG2.                   
               10  BS003-GROUPE          PIC X(05).                             
               10  BS003-FAMILLE         PIC X(05).                             
           05  BS003-WTABLEG     PIC X(80).                                     
           05  BS003-WTABLEG-REDEF  REDEFINES BS003-WTABLEG.                    
               10  BS003-LIBELLE         PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVBS003-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BS003-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  BS003-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BS003-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  BS003-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
