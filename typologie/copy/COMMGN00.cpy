      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGN00 (MENU)             TR: GN00  *    00020000
      *                          TGN10                             *    00040000
      *                           TGN20                            *    00050000
      *                            TGN30                           *            
      *                             TGN40                          *    00060000
      *                              TGN50                         *    00060000
      *                               TGN55                        *    00060000
      *                                TGN60                       *    00060000
      *                                 TGN70                      *    00060000
      *                 REFERENCE NATIONALE                        *    00100000
      **************************************************************    00110000
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00120000
      **************************************************************    00130000
      *                                                                 00140000
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00150000
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00160000
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00170000
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00180000
      *                                                                 00190000
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00200000
      * COMPRENANT :                                                    00210000
      * 1 - LES ZONES RESERVEES A AIDA                                  00220000
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00230000
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00240000
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00250000
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00260000
      *                                                                 00270000
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00280000
      * PAR AIDA                                                        00290000
      ******************************************************************        
      * DSA057 08/10/04 SUPPORT EVOLUTION FP826                                 
      *                 CONTROLE SRP TOUTES FILIALES                            
      ******************************************************************        
      * DE02005 18/01/08 SUPPORT EVOLUTION 4062                                 
      *                  IMPACT SUR TGN10 UNIQUEMENT                            
      ******************************************************************        
      * DE02005 28/03/08 SUPPORT MAINTENANCE INC 370923                         
      *                  PB TABLE INTERNE GROUPE TROP PETITE                    
      ******************************************************************        
      * DE02005 19/11/09 SUPPORT EVOLUTION IMPACT TGN10 UNIQUEMENT              
      *                  DESACTIVATION DU PRIX REF 2                            
      *                  B2B OFFRE CIBLE                                        
      ******************************************************************        
      * DE02034 19/11/09 SUPPORT EVOLUTION IMPACT TGN10 TGN30                   
      *                  AJOUT INFOS CROSSDOCK O/N                              
      ******************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-GN00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00330000
      *--                                                                       
       01  COM-GN00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00340000
       01  Z-COMMAREA.                                                  00350000
      *                                                                 00360000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00370000
          02 FILLER-COM-AIDA      PIC X(100).                           00380000
      *                                                                 00390000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00400000
          02 COMM-CICS-APPLID     PIC X(8).                             00410000
          02 COMM-CICS-NETNAM     PIC X(8).                             00420000
          02 COMM-CICS-TRANSA     PIC X(4).                             00430000
      *                                                                 00440000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00450000
          02 COMM-DATE-SIECLE     PIC XX.                               00460000
          02 COMM-DATE-ANNEE      PIC XX.                               00470000
          02 COMM-DATE-MOIS       PIC XX.                               00480000
          02 COMM-DATE-JOUR       PIC XX.                               00490000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00500000
          02 COMM-DATE-QNTA       PIC 999.                              00510000
          02 COMM-DATE-QNT0       PIC 99999.                            00520000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00530000
          02 COMM-DATE-BISX       PIC 9.                                00540000
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00550000
          02 COMM-DATE-JSM        PIC 9.                                00560000
      *   LIBELLES DU JOUR COURT - LONG                                 00570000
          02 COMM-DATE-JSM-LC     PIC XXX.                              00580000
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                         00590000
      *   LIBELLES DU MOIS COURT - LONG                                 00600000
          02 COMM-DATE-MOIS-LC    PIC XXX.                              00610000
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                         00620000
      *   DIFFERENTES FORMES DE DATE                                    00630000
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                             00640000
          02 COMM-DATE-AAMMJJ     PIC X(6).                             00650000
          02 COMM-DATE-JJMMSSAA   PIC X(8).                             00660000
          02 COMM-DATE-JJMMAA     PIC X(6).                             00670000
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                             00680000
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                            00690000
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00690010
          02 COMM-DATE-WEEK.                                            00690020
             05  COMM-DATE-SEMSS  PIC 99.                               00690030
             05  COMM-DATE-SEMAA  PIC 99.                               00690040
             05  COMM-DATE-SEMNU  PIC 99.                               00690050
          02 COMM-DATE-FILLER     PIC X(08).                            00690060
      *                                                                 00720000
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00730000
      *                                                                 00740000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.              00750000
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                        00760000
      *                                                                 00810000
      * ZONES RESERVES APPLICATIVES ------------------------------ 3724 00820000
      *                                                                 00830000
      * ZONES MENU COMMUNES A TOUTES LES TRANSACTIONS -----   207               
      *                                                                         
          02 COMM-GN00-MENU.                                            00840000
              03 COMM-GN00-OPTION            PIC X.                             
              03 COMM-GN00-FONC              PIC X(3).                          
              03 COMM-GN00-MESSAGE           PIC X(60).                         
              03 COMM-GN00-NSOCIETE          PIC X(3).                          
              03 COMM-GN00-NCONCN            PIC X(4).                          
              03 COMM-GN00-LCONCN            PIC X(20).                         
              03 COMM-GN00-LIENCO            PIC X(1).                          
              03 COMM-GN00-BBTE              PIC X(1).                          
              03 COMM-GN00-ACID              PIC X(7).                          
              03 COMM-GN00-NBDEC             PIC X(1).                          
              03 COMM-GN00-CFAM              PIC X(5).                          
              03 FILLER                      PIC X(45).                         
              03 COMM-GN00-NCODIC            PIC X(7).                          
      *                                                                         
      *                                                                         
      * ZONES APPLICATIVES (PAR REDEFINES) ---------------- 3517                
      *                                                                         
          02 COMM-GN00-APPLI.                                                   
             03 COMM-GN00-DATAS.                                        00742   
                05 COMM-GN00-FILLER             PIC X(3517).                    
      * ZONES ECRAN GN10                     --------------                     
      * 3217 + 300 DISPONIBLES AVANT MODIF -> 2217 + 1024 + 276 LIBRES          
      * 1239 + 1024 + 1252 LIBRES                                               
      * 1259 + 1024 + 1232 LIBRES                                               
      * 1254 + 5 + 1024 + 210 + 1024 LIBRES = 3517                              
          02 COMM-GN10-APPLI REDEFINES COMM-GN00-APPLI.                         
           03 COMM-GN10-DONNEES.                                                
             04 COMM-GN10-DATAS.                                                
                05 COMM-GN10-NUMPAG            PIC  9(03).               00742  
                05 COMM-GN10-NUMPAG2           PIC  9(03).               00742  
                05 COMM-GN10-NUMPAG3           PIC  9(03).               00742  
                05 COMM-GN10-PAGE-REF          PIC  9(03).               00742  
                05 COMM-GN10-PAGE-OA           PIC  9(03).               00742  
                05 COMM-GN10-PAGE-PRIME        PIC  9(03).               00742  
                05 COMM-GN10-NBRPAG            PIC  9(03).                 00743
                05 COMM-GN10-NBRPAG2           PIC  9(03).                 00743
                05 COMM-GN10-NBRPAG3           PIC  9(03).                 00743
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN10-AJOUT-TS          PIC S9(02) COMP.            00743
      *--                                                                       
                05 COMM-GN10-AJOUT-TS          PIC S9(02) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN10-MODIF-TS          PIC S9(02) COMP.            00743
      *--                                                                       
                05 COMM-GN10-MODIF-TS          PIC S9(02) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN10-MODIF-TSGN10      PIC S9(02) COMP.            00743
      *--                                                                       
                05 COMM-GN10-MODIF-TSGN10      PIC S9(02) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN10-MODIF-TSGN11      PIC S9(02) COMP.            00743
      *--                                                                       
                05 COMM-GN10-MODIF-TSGN11      PIC S9(02) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN10-MODIF-TSGN12      PIC S9(02) COMP.            00743
      *--                                                                       
                05 COMM-GN10-MODIF-TSGN12      PIC S9(02) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN10-MODIF-TSGN13      PIC S9(02) COMP.            00743
      *--                                                                       
                05 COMM-GN10-MODIF-TSGN13      PIC S9(02) COMP-5.               
      *}                                                                        
                05 COMM-GN10-TABLE-WIMPER.                                      
                   10 COMM-GN10-TAB-WIMPER OCCURS 4.                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *               15 COMM-GN10-MODIF-WIMPER   PIC S9(02) COMP.         00743
      *--                                                                       
                      15 COMM-GN10-MODIF-WIMPER   PIC S9(02) COMP-5.            
      *}                                                                        
                      15 COMM-GN10-MODIF-REF1     PIC X(1).                00743
                05 COMM-GN10-TABLE-CBLOQ.                                       
                   10 COMM-GN10-TAB-CBLOQ  OCCURS 3.                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *               15 COMM-GN10-MODIF-CBLOQ    PIC S9(02) COMP.         00743
      *--                                                                       
                      15 COMM-GN10-MODIF-CBLOQ    PIC S9(02) COMP-5.            
      *}                                                                        
                      15 COMM-GN10-MODIF-REF2     PIC X(1).                00743
                05 COMM-GN10-MODIF-CEXPO          PIC X.                        
                05 COMM-GN10-MODIF-COMPAC         PIC X.                        
                05 COMM-GN10-T-NSOCDEPOT          PIC X(30).                    
                05 COMM-GN10-T-NDEPOT             PIC X(30).                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN10-IND-RECH-TS       PIC S9(04) COMP.            00745
      *--                                                                       
                05 COMM-GN10-IND-RECH-TS       PIC S9(04) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN10-IND-RECH-TS2      PIC S9(04) COMP.            00745
      *--                                                                       
                05 COMM-GN10-IND-RECH-TS2      PIC S9(04) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN10-IND-RECH-REF2     PIC S9(04) COMP.            00745
      *--                                                                       
                05 COMM-GN10-IND-RECH-REF2     PIC S9(04) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN10-IND-RECH-TS3      PIC S9(04) COMP.            00745
      *--                                                                       
                05 COMM-GN10-IND-RECH-TS3      PIC S9(04) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN10-IND-TS-MAX        PIC S9(04) COMP.            00747
      *--                                                                       
                05 COMM-GN10-IND-TS-MAX        PIC S9(04) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN10-IND-MAX-REF       PIC S9(04) COMP.            00747
      *--                                                                       
                05 COMM-GN10-IND-MAX-REF       PIC S9(04) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN10-IND-MAX-REF2      PIC S9(04) COMP.            00747
      *--                                                                       
                05 COMM-GN10-IND-MAX-REF2      PIC S9(04) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN10-IND-TS2-MAX       PIC S9(04) COMP.            00747
      *--                                                                       
                05 COMM-GN10-IND-TS2-MAX       PIC S9(04) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN10-IND-TS3-MAX       PIC S9(04) COMP.            00747
      *--                                                                       
                05 COMM-GN10-IND-TS3-MAX       PIC S9(04) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN10-ENC-TSGN12        PIC S9(04) COMP.            00747
      *--                                                                       
                05 COMM-GN10-ENC-TSGN12        PIC S9(04) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN10-ENC-TSGN13        PIC S9(04) COMP.            00747
      *--                                                                       
                05 COMM-GN10-ENC-TSGN13        PIC S9(04) COMP-5.               
      *}                                                                        
                05 COMM-GN10-NCODIC-SUIV       PIC X(7).                        
                05 COMM-GN10-NCODIC-CHGT       PIC X(1).                        
                05 COMM-GN10-NSOC-SUIV      PIC X(3).                           
                05 COMM-GN10-NSOC-CHGT      PIC X(3).                           
                05 COMM-GN10-CCODGROUP         PIC  X(01).                      
                05 COMM-GN10-TABS OCCURS 04.                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10  COMM-GN10-NUM-TS         PIC S9(04) COMP.                
      *--                                                                       
                   10  COMM-GN10-NUM-TS         PIC S9(04) COMP-5.              
      *}                                                                        
                05 COMM-GN10-TABLE-GROUP.                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10  COMM-GN10-NBR-GROUP  PIC  S9(04) COMP.                   
      *--                                                                       
                   10  COMM-GN10-NBR-GROUP  PIC  S9(04) COMP-5.                 
      *}                                                                        
      *            10  COMM-GN10-TAB-GROUP  OCCURS 200.                         
      *            AURAIT DU ETRE UNE TS, NECESSITE TROP DE MODIF               
      *            REDUCTION SELON MAX BASE PROD DARTY                          
      *            SI POSE PROBLEME TRANSFORMER EN TS                           
      *            10  COMM-GN10-TAB-GROUP  OCCURS 100.                         
      *                15  COMM-GN10-TAB-NCODIC PIC  9(07).                     
      *                15  COMM-GN10-TAB-QLIENS PIC S9(03) COMP-3.              
      *                15  COMM-GN10-GRP-OU-ELT PIC  X(01).                     
      *            FILLER DE 976 A RECUPERER                                    
                   10 COMM-GN10-TAB-GROUP.                                      
                       15 COMM-GN10-TAB-NCODIC  PIC  9(07).                     
                       15 COMM-GN10-TAB-QLIENS  PIC S9(03) COMP-3.              
                       15 COMM-GN10-GRP-OU-ELT  PIC  X(01).                     
                   10 TS-GN10B-IDENT            PIC  X(08).                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-GN10B-NB               PIC S9(04) COMP.                
      *--                                                                       
                   10 TB-GN10B-NB               PIC S9(04) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-GN10B-POS              PIC S9(04) COMP.                
      *--                                                                       
                   10 TB-GN10B-POS              PIC S9(04) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-GN10B-MAX              PIC S9(04) COMP.                
      *--                                                                       
                   10 TB-GN10B-MAX              PIC S9(04) COMP-5.              
      *}                                                                        
                05 COMM-GN10-PCHT           PIC S9(07)V99 COMP-3.               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN10-IND-PRIME      PIC S9(04) COMP.                    
      *--                                                                       
                05 COMM-GN10-IND-PRIME      PIC S9(04) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN10-IND-OA         PIC S9(04) COMP.                    
      *--                                                                       
                05 COMM-GN10-IND-OA         PIC S9(04) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN10-IND-REF        PIC S9(04) COMP.                    
      *--                                                                       
                05 COMM-GN10-IND-REF        PIC S9(04) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN10-IND-REF2       PIC S9(04) COMP.                    
      *--                                                                       
                05 COMM-GN10-IND-REF2       PIC S9(04) COMP-5.                  
      *}                                                                        
                05 COMM-GN10-QTAUXTVA       PIC S9V9(4) COMP-3.                 
                05 COMM-GN10-TAB-DATEOA.                                        
                   10 COMM-GN10-TABOA      OCCURS 03.                           
                      15 COMM-GN10-DEBOA          PIC X(8).                     
                      15 COMM-GN10-DEBOA-AV       PIC X(8).                     
                      15 COMM-GN10-OA             PIC X.                        
                      15 COMM-GN10-CODSUB         PIC X(7).                     
                      15 COMM-GN10-MODIF-OA       PIC X(1).                     
                05 COMM-GN10-TAB-DATEPRR.                                       
                   10 COMM-GN10-TABPRR      OCCURS 09.                          
                      15 COMM-GN10-ACT4           PIC X(1).                     
                      15 COMM-GN10-DPRR           PIC X(8).                     
                      15 COMM-GN10-DPRR-AV        PIC X(8).                     
                      15 COMM-GN10-PRR            PIC X(6).                     
                      15 COMM-GN10-PRRC           PIC X(5).                     
                      15 COMM-GN10-CPRR           PIC X(10).                    
                      15 COMM-GN10-MODIF-PRIME    PIC X(01).                    
                05 COMM-GN10-TAB-DATEPVR.                                       
                   10 COMM-GN10-TABPVR      OCCURS 04.                          
                      15 COMM-GN10-DPVR           PIC X(8).                     
                      15 COMM-GN10-DPVR-AV        PIC X(8).                     
                      15 COMM-GN10-PVR            PIC X(8).                     
                      15 COMM-GN10-COMM           PIC X(10).                    
                      15 COMM-GN10-WIMPER         PIC X.                        
                      15 COMM-GN10-CIMPER         PIC X.                        
                      15 COMM-GN10-TMB            PIC 9(2)V9.                   
      *               15 COMM-GN10-MB             PIC 9(3)V99.                  
                      15 COMM-GN10-MB             PIC 9(5)V99.                  
                05 COMM-GN10-TAB-DATEPVR2.                                      
                   10 COMM-GN10-TABPVR2     OCCURS 03.                          
                      15 COMM-GN10-DPVR2          PIC X(8).                     
                      15 COMM-GN10-DPVR2-AV       PIC X(8).                     
                      15 COMM-GN10-PVR2           PIC X(8).                     
                      15 COMM-GN10-COMM2          PIC X(10).                    
                      15 COMM-GN10-CBLOQ          PIC X.                        
                      15 COMM-GN10-TMB2           PIC 9(2)V9.                   
      *               15 COMM-GN10-MB2            PIC 9(3)V99.                  
                      15 COMM-GN10-MB2            PIC 9(5)V99.                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      04 COMM-GN10-IND-RECH-ENC       PIC S9(04) COMP.                   
      *--                                                                       
             04 COMM-GN10-IND-RECH-ENC       PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      04 COMM-GN10-IND-RECH2          PIC S9(04) COMP.                   
      *--                                                                       
             04 COMM-GN10-IND-RECH2          PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      04 COMM-GN10-IND-RECH3          PIC S9(04) COMP.                   
      *--                                                                       
             04 COMM-GN10-IND-RECH3          PIC S9(04) COMP-5.                 
      *}                                                                        
             04 COMM-GN10-ENTETE1.                                              
                05 COMM-GN10-NCODIC            PIC X(7).                        
                05 COMM-GN10-FMILMAX           PIC S9(4)V99 COMP-3.             
                05 COMM-GN10-LREF              PIC X(20).                       
                05 COMM-GN10-CFAM              PIC X(5).                        
SR....          05 COMM-GN10-TOP-CFAM-BS       PIC 9.                           
                   88 FAMILLE-NOT-BS                 VALUE 0.                   
....SR             88 FAMILLE-BS                     VALUE 1.                   
                05 COMM-GN10-CMARQ             PIC X(5).                        
                05 COMM-GN10-LMARQ             PIC X(20).                       
                05 COMM-GN10-NSOC              PIC X(3).                        
                05 COMM-GN10-SOCNA             PIC X(3).                        
                05 COMM-GN10-PRMP-REF          PIC S9(5)V99.                    
                05 COMM-GN10-SRP-REF           PIC S9(5)V99.                    
                05 COMM-GN10-CAPPRO-REF        PIC X(5).                        
             04 COMM-GN10-ENTETE2.                                              
                05 COMM-GN10-CAPPRO            PIC X(5).                        
                05 COMM-GN10-COMPAC            PIC X(3).                        
                05 COMM-GN10-PRMP              PIC S9(5)V99.                    
      *         05 COMM-GN10-PRMP-REF          PIC S9(5)V99.                    
                05 COMM-GN10-DATCDE            PIC X(10).                       
                05 COMM-GN10-QTECDE            PIC 9(6).                        
                05 COMM-GN10-PRA               PIC S9(5)V99.                    
                05 COMM-GN10-SOCDEP            PIC X(3).                        
                05 COMM-GN10-DEPOT             PIC X(3).                        
                05 COMM-GN10-QTEDEP            PIC X(6).                        
                05 COMM-GN10-STKNAT            PIC X(7).                        
                05 COMM-GN10-CEXPO             PIC X(5).                        
                05 COMM-GN10-SSAPP             PIC X(1).                        
                05 COMM-GN10-SRP               PIC S9(5)V99.                    
      *         05 COMM-GN10-SRP-REF           PIC S9(5)V99.                    
                05 COMM-GN10-SOCIETE           PIC X(3).                        
                05 COMM-GN10-BLOCAGE           PIC X(1).                        
                05 COMM-GN10-CROSSDOCK         PIC X(1).                        
             04 COMM-GN10-INSERTION.                                            
                05 COMM-GN10-IPVR              PIC X(8).                        
                05 COMM-GN10-ICIMP             PIC X.                           
                05 COMM-GN10-IDPVR             PIC X(8).                        
                05 COMM-GN10-ICOMM             PIC X(10).                       
                05 COMM-GN10-IPRR              PIC X(6).                        
                05 COMM-GN10-IDPRR             PIC X(8).                        
                05 COMM-GN10-ICPRR             PIC X(10).                       
                05 COMM-GN10-IPVR2             PIC X(8).                        
                05 COMM-GN10-ICBLOQ            PIC X.                           
                05 COMM-GN10-IDPVR2            PIC X(8).                        
                05 COMM-GN10-ICOMM2            PIC X(10).                       
                05 COMM-GN10-IOA               PIC X(1).                        
                05 COMM-GN10-IDEB              PIC X(08).                       
                05 COMM-GN10-ICODSUB           PIC X(7).                        
                05 COMM-GN10-ITMB              PIC S9(2)V9.                     
      *         05 COMM-GN10-IMB               PIC S9(3)V99.                    
                05 COMM-GN10-IMB               PIC S9(5)V99.                    
                05 COMM-GN10-ITMB2             PIC S9(2)V9.                     
      *         05 COMM-GN10-IMB2              PIC S9(3)V99.                    
                05 COMM-GN10-IMB2              PIC S9(5)V99.                    
             04 COMM-GN10-PASS-SRP             PIC X.                           
             04 COMM-GN10-PASS-PRMP            PIC X.                           
             04 COMM-GN10-DACEM                PIC X.                           
             04 COMM-GN10-CAPPRO-DIF           PIC X(03).                       
             04 COMM-GN10-WAUTO                PIC X(01).                       
             04 COMM-GN10-APP-DEF              PIC X(01).                       
             04 COMM-GN10-OA-DEF               PIC X(01).                       
             04 COMM-GN10-OA-ENC               PIC X(01).                       
             04 COMM-GN10-PVR-ENC              PIC S9(5)V99.                    
             04 COMM-GN10-EPF-DEF              PIC X(01).                       
             04 COMM-GN10-IND-NOR              PIC 9.                           
      *      03 COMM-GN10-FILLER               PIC X(0034).                     
      *    GESTION                                                              
           03 COMM-GN10-GESTION.                                                
      *      GESTION CODIC SIMILAIRE                                            
             04 COMM-GN10-SIMILAIRE.                                            
                 05 COMM-GN10-CSPOS PIC S9(3) COMP-3.                           
                 05 COMM-GN10-CSMAX PIC S9(3) COMP-3.                           
             04 COMM-GN10-CSIMIL    PIC X(1).                                   
      *    1024                                                                 
           03 COMM-GN10-CB PIC X(1024).                                         
      *    0210                                                                 
           03 COMM-GN10-B2BOC.                                                  
              04 COMM-GN10-B2BOC-DATA.                                          
                 05 COMM-GN10-B2BOC-NCODIC PIC X(8).                            
                 05 COMM-GN10-B2BOC-OC     PIC X(1).                            
                 05 COMM-GN10-B2BOC-DEFFET PIC X(8).                            
                 05 COMM-GN10-B2BOC-D      PIC X(8).                            
                 05 COMM-GN10-B2BOC-LIB    PIC X(10).                           
              04 COMM-GN10-B2BOC-MAP.                                           
                 05 COMM-GN10-B2BOC-OCH    PIC X(1).                            
                 05 COMM-GN10-B2BOC-DH     PIC X(8).                            
                 05 COMM-GN10-B2BOC-LH     PIC X(10).                           
              04 FILLER PIC X(156).                                             
      *    0276                                                                 
      *    03 FILLER PIC X(276).                                                
      *    03 FILLER PIC X(1252).                                               
      *    03 FILLER PIC X(1232).                                               
      *    03 FILLER PIC X(1024).                                               
           03 FILLER PIC X(1023).                                               
      *                                                                         
      * ZONES ECRAN GN20                       ------------                     
      *                                                                         
          02 COMM-GN20-APPLI REDEFINES COMM-GN00-APPLI.                         
             03 COMM-GN20-DATAS.                                                
                05 COMM-GN20-NUMPAG         PIC  9(03).                  00742  
                05 COMM-GN20-NBRPAG         PIC  9(03).                    00743
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN20-MODIF-TS       PIC S9(02) COMP.               00743
      *--                                                                       
                05 COMM-GN20-MODIF-TS       PIC S9(02) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN20-IND-RECH-TS    PIC S9(04) COMP.               00745
      *--                                                                       
                05 COMM-GN20-IND-RECH-TS    PIC S9(04) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN20-IND-TS-MAX     PIC S9(04) COMP.               00747
      *--                                                                       
                05 COMM-GN20-IND-TS-MAX     PIC S9(04) COMP-5.                  
      *}                                                                        
                05 COMM-GN20-NCODICR        PIC X(7).                           
                05 COMM-GN20-NCODICR-SUIV   PIC X(7).                           
                05 COMM-GN20-NCODICR-CHGT   PIC X(1).                           
                05 COMM-GN20-CFAMR          PIC X(5).                           
                05 COMM-GN20-CFAMR-SUIV     PIC X(5).                           
                05 COMM-GN20-CFAMR-CHGT     PIC X(1).                           
                05 COMM-GN20-CMARQR         PIC X(5).                           
                05 COMM-GN20-CMARQR-SUIV    PIC X(5).                           
                05 COMM-GN20-CMARQR-CHGT    PIC X(1).                           
                05 COMM-GN20-LREFFOUR       PIC X(20).                          
                05 COMM-GN20-LREFFOUR-SUIV  PIC X(20).                          
                05 COMM-GN20-LREFFOUR-CHGT  PIC X(1).                           
                05 COMM-GN20-STATUTR        PIC X(20).                          
                05 COMM-GN20-STATUTR-SUIV   PIC X(20).                          
                05 COMM-GN20-STATUTR-CHGT   PIC X(1).                           
                05 COMM-GN20-WACTIF         PIC X(1).                           
                05 COMM-GN20-WACTIF2        PIC X(1).                           
                05 COMM-GN20-WOA            PIC X(1).                           
                05 COMM-GN20-STATAPP        PIC X(1).                           
                05 COMM-GN20-LIBELLE1       PIC X(32).                          
                05 COMM-GN20-LIBELLE2       PIC X(32).                          
                05 COMM-GN20-LIBF10F11      PIC X(33).                          
                05 COMM-GN20-IND-PAGE       PIC 9.                              
                05 COMM-GN20-TABS OCCURS 15.                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10  COMM-GN20-NUM-TS         PIC S9(04) COMP.                
      *--                                                                       
                   10  COMM-GN20-NUM-TS         PIC S9(04) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GN20-IND-RECH-ENC      PIC S9(04) COMP.            00745918
      *--                                                                       
             03 COMM-GN20-IND-RECH-ENC      PIC S9(04) COMP-5.                  
      *}                                                                        
             03 COMM-GN20-ENSFAM            PIC X(2000).                        
             03 COMM-GN20-FILLER            PIC X(0517).                        
      *                                                                         
      * ZONES ECRAN GN30                       ------------                     
      *                                                                         
          02 COMM-GN30-APPLI REDEFINES COMM-GN00-APPLI.                         
             03 COMM-GN30-DATAS.                                                
                05 COMM-GN30-NCODIC         PIC X(7).                           
                05 COMM-GN30-NUMPAG         PIC  9(03).                  00742  
                05 COMM-GN30-NBRPAG         PIC  9(03).                    00743
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN30-MODIF-TS       PIC S9(02) COMP.               00743
      *--                                                                       
                05 COMM-GN30-MODIF-TS       PIC S9(02) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN30-IND-RECH-TS    PIC S9(04) COMP.               00745
      *--                                                                       
                05 COMM-GN30-IND-RECH-TS    PIC S9(04) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN30-IND-TS-MAX     PIC S9(04) COMP.               00747
      *--                                                                       
                05 COMM-GN30-IND-TS-MAX     PIC S9(04) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN30-IND-TS-MAX2    PIC S9(04) COMP.               00747
      *--                                                                       
                05 COMM-GN30-IND-TS-MAX2    PIC S9(04) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN30-IND-TSGN30B    PIC S9(04) COMP.                    
      *--                                                                       
                05 COMM-GN30-IND-TSGN30B    PIC S9(04) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN30-ENC-TSGN30B    PIC S9(04) COMP.                    
      *--                                                                       
                05 COMM-GN30-ENC-TSGN30B    PIC S9(04) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN30-IND-OA         PIC S9(04) COMP.                    
      *--                                                                       
                05 COMM-GN30-IND-OA         PIC S9(04) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN30-IND-REF        PIC S9(04) COMP.                    
      *--                                                                       
                05 COMM-GN30-IND-REF        PIC S9(04) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN30-IND-PRR        PIC S9(04) COMP.                    
      *--                                                                       
                05 COMM-GN30-IND-PRR        PIC S9(04) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GN30-IND-REF2       PIC S9(04) COMP.                    
      *--                                                                       
                05 COMM-GN30-IND-REF2       PIC S9(04) COMP-5.                  
      *}                                                                        
                05 COMM-GN30-CCODGROUP      PIC  X(01).                         
                05 COMM-GN30-QTAUXTVA       PIC S9V9(4) COMP-3.                 
                05 COMM-GN30-NCODIC-SUIV    PIC X(7).                           
                05 COMM-GN30-NCODIC-CHGT    PIC X(7).                           
                05 COMM-GN30-SOCIETE        PIC X(3).                           
                05 COMM-GN30-SOCIETE-SUIV   PIC X(3).                           
                05 COMM-GN30-SOCIETE-CHGT   PIC X(3).                           
                05 COMM-GN30-IND-SWITCH     PIC X(1).                           
                05 COMM-GN30-IND-VAR        PIC X(1).                           
                05 COMM-GN30-TABLE-GROUP.                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10  COMM-GN30-NBR-GROUP  PIC  S9(04) COMP.                   
      *--                                                                       
                   10  COMM-GN30-NBR-GROUP  PIC  S9(04) COMP-5.                 
      *}                                                                        
                   10  COMM-GN30-TAB-GROUP  OCCURS 200.                         
                       15  COMM-GN30-TAB-NCODIC PIC  9(07).                     
                       15  COMM-GN30-TAB-QLIENS PIC S9(03) COMP-3.              
                       15  COMM-GN30-GRP-OU-ELT PIC  X(01).                     
                05 COMM-GN30-PCHT               PIC S9(07)V99 COMP-3.           
                05 COMM-GN30-TABS OCCURS 07.                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10  COMM-GN30-NUM-TS         PIC S9(04) COMP.                
      *--                                                                       
                   10  COMM-GN30-NUM-TS         PIC S9(04) COMP-5.              
      *}                                                                        
                05 COMM-GN30-TABLE-NSOCS.                                       
                   10 COMM-GN30-T-NSOCS OCCURS 10.                              
                      15  COMM-GN30-NSOCS       PIC 9(03).                      
                05 COMM-GN30-IND-NSOCS       PIC 99.                            
                05 COMM-GN30-PRR-ENC         PIC S9(3)V99.                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GN30-IND-RECH-ENC       PIC S9(04) COMP.           00745918
      *--                                                                       
             03 COMM-GN30-IND-RECH-ENC       PIC S9(04) COMP-5.                 
      *}                                                                        
             03 COMM-GN30-ENTETE1.                                              
                05 COMM-GN30-LREFFOU        PIC X(20).                          
                05 COMM-GN30-CFAM           PIC X(5).                           
SR....          05 COMM-GN30-TOP-CFAM-BS    PIC 9.                              
                   88 FAMILE-NOT-BS               VALUE 0.                      
....SR             88 FAMILE-BS                   VALUE 1.                      
                05 COMM-GN30-CMARQ          PIC X(5).                           
             03 COMM-GN30-ENTETE2.                                              
                05 COMM-GN30-CAPPRO               PIC X(3).                     
                05 COMM-GN30-CEXPO                PIC X(2).                     
                05 COMM-GN30-LSTATCOMP            PIC X(3).                     
                05 COMM-GN30-PRMP                 PIC S9(5)V99.                 
                05 COMM-GN30-SRP                  PIC S9(5)V99.                 
                05 COMM-GN30-PRA                  PIC S9(5)V99.                 
                05 COMM-GN30-SOCDEPOT             PIC X(3).                     
                05 COMM-GN30-DEPOT                PIC X(3).                     
                05 COMM-GN30-QSTOCKD              PIC 9(5).                     
                05 COMM-GN30-DATECDE              PIC X(10).                    
                05 COMM-GN30-QTECDE               PIC 9(5).                     
                05 COMM-GN30-CROSSDOCK            PIC X(1).                     
      *E0744 03 COMM-GN30-FILLER                  PIC X(0478).                  
             03 COMM-GN30-FILLER                  PIC X(0477).                  
      *                                                                         
      * ZONES ECRAN GN40                     --------------                     
      *                                                                         
          02 COMM-GN40-APPLI REDEFINES COMM-GN00-APPLI.                         
              03 COMM-GN40-DATAS.                                       00742   
                 05 COMM-GN40-NCODIC         PIC X(7).                          
                 05 COMM-GN40-NUMPAG         PIC  9(03).                   00742
                 05 COMM-GN40-NBRPAG         PIC  9(03).                   00743
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN40-MODIF-TS       PIC S9(02) COMP.              00743
      *--                                                                       
                 05 COMM-GN40-MODIF-TS       PIC S9(02) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN40-IND-RECH-TS    PIC S9(04) COMP.              00745
      *--                                                                       
                 05 COMM-GN40-IND-RECH-TS    PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN40-IND-TS-MAX     PIC S9(04) COMP.              00747
      *--                                                                       
                 05 COMM-GN40-IND-TS-MAX     PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN40-IND-TS-MAX2    PIC S9(04) COMP.              00747
      *--                                                                       
                 05 COMM-GN40-IND-TS-MAX2    PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN40-IND-TSGN40B    PIC S9(04) COMP.                   
      *--                                                                       
                 05 COMM-GN40-IND-TSGN40B    PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN40-ENC-TSGN40B    PIC S9(04) COMP.                   
      *--                                                                       
                 05 COMM-GN40-ENC-TSGN40B    PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN40-IND-OA         PIC S9(04) COMP.                   
      *--                                                                       
                 05 COMM-GN40-IND-OA         PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN40-IND-REF        PIC S9(04) COMP.                   
      *--                                                                       
                 05 COMM-GN40-IND-REF        PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN40-IND-REF2       PIC S9(04) COMP.                   
      *--                                                                       
                 05 COMM-GN40-IND-REF2       PIC S9(04) COMP-5.                 
      *}                                                                        
                 05 COMM-GN40-CCODGROUP      PIC  X(01).                        
                 05 COMM-GN40-QTAUXTVA       PIC S9V9(4) COMP-3.                
                 05 COMM-GN40-NCODIC-SUIV    PIC X(7).                          
                 05 COMM-GN40-NCODIC-CHGT    PIC X(7).                          
              03 COMM-GN40-ENTETE.                                              
                 05 COMM-GN40-LREFFOU        PIC X(20).                         
                 05 COMM-GN40-CFAM           PIC X(5).                          
                 05 COMM-GN40-CMARQ          PIC X(5).                          
                 05 COMM-GN40-TAB-DEROG.                                        
                    10 COMM-GN40-TABLED      OCCURS 10.                         
                       15 COMM-GN40-DDEFFETR       PIC X(8).                    
                       15 COMM-GN40-DFEFFETR       PIC X(8).                    
                       15 COMM-GN40-MODIF-DATE     PIC X(1).                    
                    10 COMM-GN40-SOC               PIC X(3).                    
                    10 COMM-GN40-ZP                PIC X(2).                    
                    10 COMM-GN40-ZLIEU             PIC X(3).                    
                    10 COMM-GN40-DERO              PIC X(4).                    
                    10 COMM-GN40-DDEFFETD          PIC X(8).                    
                    10 COMM-GN40-DFEFFETD          PIC X(8).                    
                 05 COMM-GN40-TABLE-GROUP.                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *              10  COMM-GN40-NBR-GROUP  PIC  S9(04) COMP.                 
      *--                                                                       
                     10  COMM-GN40-NBR-GROUP  PIC  S9(04) COMP-5.               
      *}                                                                        
                     10  COMM-GN40-TAB-GROUP  OCCURS 200.                       
                       15  COMM-GN40-TAB-NCODIC PIC  9(07).                     
                       15  COMM-GN40-TAB-QLIENS PIC S9(03) COMP-3.              
                       15  COMM-GN40-GRP-OU-ELT PIC  X(01).                     
                 05 COMM-GN40-PCHT               PIC S9(07)V99 COMP-3.          
                 05 COMM-GN40-TABS OCCURS 12.                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *             10  COMM-GN40-NUM-TS         PIC S9(04) COMP.               
      *--                                                                       
                    10  COMM-GN40-NUM-TS         PIC S9(04) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GN40-IND-RECH-ENC       PIC S9(04) COMP.           00745918
      *--                                                                       
             03 COMM-GN40-IND-RECH-ENC       PIC S9(04) COMP-5.                 
      *}                                                                        
             03 COMM-GN40-FILLER             PIC X(0362).                       
      *                                                                         
      * ZONES ECRAN GN50                                                        
      *                                                                         
          02 COMM-GN50-APPLI REDEFINES COMM-GN00-APPLI.                         
              03 COMM-GN50-DATAS.                                       00742   
                 05 COMM-GN50-NCONCN         PIC X(4).                          
                 05 COMM-GN50-LCONCN         PIC X(20).                         
                 05 COMM-GN50-LIENCON        PIC X(1).                          
                 05 COMM-GN50-NUMPAG         PIC  9(03).                   00742
                 05 COMM-GN50-NBRPAG         PIC  9(03).                   00743
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN50-MODIF-TS       PIC S9(02) COMP.              00743
      *--                                                                       
                 05 COMM-GN50-MODIF-TS       PIC S9(02) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN50-DEBRANCHEMENT  PIC S9(02) COMP.              00743
      *--                                                                       
                 05 COMM-GN50-DEBRANCHEMENT  PIC S9(02) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN50-INSERT-TS      PIC S9(02) COMP.              00743
      *--                                                                       
                 05 COMM-GN50-INSERT-TS      PIC S9(02) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN50-DOUBLON-TS      PIC S9(02) COMP.             00743
      *--                                                                       
                 05 COMM-GN50-DOUBLON-TS      PIC S9(02) COMP-5.                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN50-IND-RECH-TS    PIC S9(04) COMP.              00745
      *--                                                                       
                 05 COMM-GN50-IND-RECH-TS    PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN50-IND-TS-MAX     PIC S9(04) COMP.              00747
      *--                                                                       
                 05 COMM-GN50-IND-TS-MAX     PIC S9(04) COMP-5.                 
      *}                                                                        
                 05 COMM-GN50-TABS OCCURS 14.                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *             10  COMM-GN50-NUM-TS         PIC S9(04) COMP.               
      *--                                                                       
                    10  COMM-GN50-NUM-TS         PIC S9(04) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *             10  COMM-GN50-DELETE-TS      PIC S9(02) COMP.          00743
      *--                                                                       
                    10  COMM-GN50-DELETE-TS      PIC S9(02) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *             10  COMM-GN50-UPDATE-TS      PIC S9(02) COMP.          00743
      *--                                                                       
                    10  COMM-GN50-UPDATE-TS      PIC S9(02) COMP-5.             
      *}                                                                        
                    10  COMM-GN50-LCONC          PIC X(20).                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GN50-IND-RECH-ENC       PIC S9(04) COMP.           00745918
      *--                                                                       
             03 COMM-GN50-IND-RECH-ENC       PIC S9(04) COMP-5.                 
      *}                                                                        
             03 COMM-GN50-FILLER             PIC X(0362).                       
      *                                                                         
      * ZONES ECRAN GN55                                                        
      *                                                                         
          02 COMM-GN55-APPLI REDEFINES COMM-GN00-APPLI.                         
              03 COMM-GN55-DATAS.                                       00742   
                 05 COMM-GN55-SOCIETE        PIC X(3).                          
                 05 COMM-GN55-SOCIETE-SUIV   PIC X(3).                          
                 05 COMM-GN55-NCONCN         PIC X(4).                          
                 05 COMM-GN55-LCONCN         PIC X(20).                         
                 05 COMM-GN55-LIENCON        PIC X(1).                          
                 05 COMM-GN55-ANCONCN        PIC X(4).                          
                 05 COMM-GN55-ALCONCN        PIC X(20).                         
                 05 COMM-GN55-NUMPAG         PIC  9(03).                   00742
                 05 COMM-GN55-NBRPAG         PIC  9(03).                   00743
                 05 COMM-GN55-NUMPAG2        PIC  9(03).                   00742
                 05 COMM-GN55-NBRPAG2        PIC  9(03).                   00743
                 05 COMM-GN55-NUMPAG1        PIC  9(03).                   00742
                 05 COMM-GN55-NBRPAG1        PIC  9(03).                   00743
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN55-MODIF-TS       PIC S9(02) COMP.              00743
      *--                                                                       
                 05 COMM-GN55-MODIF-TS       PIC S9(02) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN55-MODIF-TS2      PIC S9(02) COMP.              00743
      *--                                                                       
                 05 COMM-GN55-MODIF-TS2      PIC S9(02) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN55-MODIF-LIB      PIC S9(02) COMP.              00743
      *--                                                                       
                 05 COMM-GN55-MODIF-LIB      PIC S9(02) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN55-MODIF-NCONCN   PIC S9(02) COMP.              00743
      *--                                                                       
                 05 COMM-GN55-MODIF-NCONCN   PIC S9(02) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN55-SOCIETE-CHGT   PIC S9(02) COMP.              00743
      *--                                                                       
                 05 COMM-GN55-SOCIETE-CHGT   PIC S9(02) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN55-DOUBLON-TS     PIC S9(02) COMP.              00743
      *--                                                                       
                 05 COMM-GN55-DOUBLON-TS     PIC S9(02) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN55-INSERT-TS      PIC S9(02) COMP.              00743
      *--                                                                       
                 05 COMM-GN55-INSERT-TS      PIC S9(02) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN55-IND-RECH-TS    PIC S9(04) COMP.              00745
      *--                                                                       
                 05 COMM-GN55-IND-RECH-TS    PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN55-IND-TS-MAX     PIC S9(04) COMP.              00747
      *--                                                                       
                 05 COMM-GN55-IND-TS-MAX     PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN55-IND-RECH-TS1   PIC S9(04) COMP.              00745
      *--                                                                       
                 05 COMM-GN55-IND-RECH-TS1   PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN55-IND-TS-MAX1    PIC S9(04) COMP.              00747
      *--                                                                       
                 05 COMM-GN55-IND-TS-MAX1    PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN55-IND-TS2        PIC S9(04) COMP.              00747
      *--                                                                       
                 05 COMM-GN55-IND-TS2        PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN55-IND-RECH-TS2   PIC S9(04) COMP.              00745
      *--                                                                       
                 05 COMM-GN55-IND-RECH-TS2   PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN55-IND-TS-MAX2    PIC S9(04) COMP.              00747
      *--                                                                       
                 05 COMM-GN55-IND-TS-MAX2    PIC S9(04) COMP-5.                 
      *}                                                                        
                 05 COMM-GN55-NCONC-ENC      PIC  X(04).                        
                 05 COMM-GN55-TABS OCCURS 14.                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *             10  COMM-GN55-NUM-TS         PIC S9(04) COMP.               
      *--                                                                       
                    10  COMM-GN55-NUM-TS         PIC S9(04) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *             10  COMM-GN55-NUM-TS2        PIC S9(04) COMP.               
      *--                                                                       
                    10  COMM-GN55-NUM-TS2        PIC S9(04) COMP-5.             
      *}                                                                        
                    10  COMM-GN55-NSOC           PIC X(3).                      
                    10  COMM-GN55-ACT            PIC X.                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *             10  COMM-GN55-DELETE-TS      PIC S9(02) COMP.          00743
      *--                                                                       
                    10  COMM-GN55-DELETE-TS      PIC S9(02) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GN55-IND-RECH-ENC       PIC S9(04) COMP.           00745918
      *--                                                                       
             03 COMM-GN55-IND-RECH-ENC       PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GN55-IND-RECH-ENC2      PIC S9(04) COMP.           00745918
      *--                                                                       
             03 COMM-GN55-IND-RECH-ENC2      PIC S9(04) COMP-5.                 
      *}                                                                        
             03 COMM-GN55-FILLER             PIC X(0362).                       
      *                                                                         
      * ZONES ECRAN GN60                                                        
      *                                                                         
          02 COMM-GN60-APPLI REDEFINES COMM-GN00-APPLI.                         
              03 COMM-GN60-DATAS.                                       00742   
                 05 COMM-GN60-NSOC           PIC X(3).                          
                 05 COMM-GN60-INCONCL        PIC X(4).                          
                 05 COMM-GN60-IZONPRIX       PIC X(3).                          
                 05 COMM-GN60-NUMPAG         PIC  9(03).                   00742
                 05 COMM-GN60-NBRPAG         PIC  9(03).                   00743
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN60-MODIF-TS       PIC S9(02) COMP.              00743
      *--                                                                       
                 05 COMM-GN60-MODIF-TS       PIC S9(02) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN60-MODIF-SEL      PIC S9(02) COMP.              00743
      *--                                                                       
                 05 COMM-GN60-MODIF-SEL      PIC S9(02) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN60-IND-RECH-TS    PIC S9(04) COMP.              00745
      *--                                                                       
                 05 COMM-GN60-IND-RECH-TS    PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN60-IND-TS-MAX     PIC S9(04) COMP.              00747
      *--                                                                       
                 05 COMM-GN60-IND-TS-MAX     PIC S9(04) COMP-5.                 
      *}                                                                        
                 05 COMM-GN60-TABS OCCURS 14.                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *             10  COMM-GN60-NUM-TS         PIC S9(04) COMP.               
      *--                                                                       
                    10  COMM-GN60-NUM-TS         PIC S9(04) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *             10  COMM-GN60-UPDATE-TS      PIC S9(04) COMP.               
      *--                                                                       
                    10  COMM-GN60-UPDATE-TS      PIC S9(04) COMP-5.             
      *}                                                                        
                    10  COMM-GN60-NCONCN         PIC X(4).                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GN60-IND-RECH-ENC       PIC S9(04) COMP.           00745918
      *--                                                                       
             03 COMM-GN60-IND-RECH-ENC       PIC S9(04) COMP-5.                 
      *}                                                                        
             03 COMM-GN60-FILLER             PIC X(0362).                       
      *                                                                         
      * ZONES ECRAN GN70                                                        
      *                                                                         
          02 COMM-GN70-APPLI REDEFINES COMM-GN00-APPLI.                         
              03 COMM-GN70-DATAS.                                       00742   
                 05 COMM-GN70-SOCIETE        PIC X(3).                          
                 05 COMM-GN70-SOCIETE-SUIV   PIC X(3).                          
                 05 COMM-GN70-SOCIETE-CHGT   PIC X(3).                          
                 05 COMM-GN70-IPEXPTTC       PIC X(8).                          
                 05 COMM-GN70-IDDEFFET       PIC X(8).                          
                 05 COMM-GN70-IDFEFFET       PIC X(8).                          
                 05 COMM-GN70-ICONCN         PIC X(4).                          
                 05 COMM-GN70-NUMPAG         PIC  9(03).                   00742
                 05 COMM-GN70-NBRPAG         PIC  9(03).                   00743
                 05 COMM-GN70-NUMPAG2        PIC  9(03).                   00742
                 05 COMM-GN70-NBRPAG2        PIC  9(03).                   00743
                 05 COMM-GN70-NUMPAG1        PIC  9(03).                   00742
                 05 COMM-GN70-NBRPAG1        PIC  9(03).                   00743
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN70-MODIF-TS       PIC S9(02) COMP.              00743
      *--                                                                       
                 05 COMM-GN70-MODIF-TS       PIC S9(02) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN70-INSERT-TS       PIC S9(02) COMP.             00743
      *--                                                                       
                 05 COMM-GN70-INSERT-TS       PIC S9(02) COMP-5.                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN70-MODIF-SEL      PIC S9(02) COMP.              00743
      *--                                                                       
                 05 COMM-GN70-MODIF-SEL      PIC S9(02) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN70-IND-RECH-TS    PIC S9(04) COMP.              00745
      *--                                                                       
                 05 COMM-GN70-IND-RECH-TS    PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN70-IND-TS-MAX     PIC S9(04) COMP.              00747
      *--                                                                       
                 05 COMM-GN70-IND-TS-MAX     PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN70-IND-RECH-TS1   PIC S9(04) COMP.              00745
      *--                                                                       
                 05 COMM-GN70-IND-RECH-TS1   PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN70-IND-RECH-TS2   PIC S9(04) COMP.              00745
      *--                                                                       
                 05 COMM-GN70-IND-RECH-TS2   PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN70-IND-TS-MAX2    PIC S9(04) COMP.              00747
      *--                                                                       
                 05 COMM-GN70-IND-TS-MAX2    PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-GN70-IND-TS-MAX1    PIC S9(04) COMP.              00747
      *--                                                                       
                 05 COMM-GN70-IND-TS-MAX1    PIC S9(04) COMP-5.                 
      *}                                                                        
                 05 COMM-GN70-SOCDEPOT       PIC X(3).                          
                 05 COMM-GN70-DEPOT          PIC X(3).                          
                 05 COMM-GN70-SWITCH         PIC X(1).                          
                 05 COMM-GN70-TABS OCCURS 10.                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *             10  COMM-GN70-NUM-TS         PIC S9(04) COMP.               
      *--                                                                       
                    10  COMM-GN70-NUM-TS         PIC S9(04) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *             10  COMM-GN70-NUM-TS2        PIC S9(04) COMP.               
      *--                                                                       
                    10  COMM-GN70-NUM-TS2        PIC S9(04) COMP-5.             
      *}                                                                        
                    10  COMM-GN70-NSOC           PIC X(3).                      
                    10  COMM-GN70-CAPPRO         PIC X(3).                      
                    10  COMM-GN70-CEXPO          PIC X(2).                      
                    10  COMM-GN70-LSTATCOMP      PIC X(3).                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *             10  COMM-GN70-UPDATE-TS      PIC S9(04) COMP.               
      *--                                                                       
                    10  COMM-GN70-UPDATE-TS      PIC S9(04) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *             10  COMM-GN70-INSERTION-TS   PIC S9(04) COMP.               
      *--                                                                       
                    10  COMM-GN70-INSERTION-TS   PIC S9(04) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *             10  COMM-GN70-DELETE-TS      PIC S9(04) COMP.               
      *--                                                                       
                    10  COMM-GN70-DELETE-TS      PIC S9(04) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *             10  COMM-GN70-MODIF-DFEFFET  PIC S9(04) COMP.               
      *--                                                                       
                    10  COMM-GN70-MODIF-DFEFFET  PIC S9(04) COMP-5.             
      *}                                                                        
                    10  COMM-GN70-ACT            PIC X.                         
                    10  COMM-GN70-PEXPTTC        PIC X(8).                      
                    10  COMM-GN70-DDEFFET        PIC X(8).                      
                    10  COMM-GN70-DFEFFET        PIC X(8).                      
                    10  COMM-GN70-NCONCN         PIC X(4).                      
             03 COMM-GN70-ENTETE1.                                              
                05 COMM-GN70-LREFFOU        PIC X(20).                          
                05 COMM-GN70-CFAM           PIC X(5).                           
                05 COMM-GN70-CMARQ          PIC X(5).                           
                05 COMM-GN70-WOA            PIC X.                              
                05 COMM-GN70-WIMPERATIF     PIC X.                              
                05 COMM-GN70-WBLOQUE        PIC X.                              
                05 COMM-GN70-NCODICS        PIC X(7).                           
                05 COMM-GN70-NCODIC-SUIV    PIC X(7).                           
                05 COMM-GN70-NCODIC-CHGT    PIC X(7).                           
                05 COMM-GN70-X-JOURS        PIC 9(03).                          
                05 COMM-GN70-DEFFETOA       PIC X(8).                           
                05 COMM-GN70-DFINEFOA       PIC X(8).                           
                05 COMM-GN70-TABREF.                                            
                   10 COMM-GN70-TABLE-REF      OCCURS 2.                        
                      15 COMM-GN70-PREFTTC  PIC S9(5)V99.                       
                      15 COMM-GN70-DEFFET   PIC X(8).                           
                      15 COMM-GN70-DFINEFFE PIC X(8).                           
                      15 COMM-GN70-LCOMMENT PIC X(10).                          
             03 COMM-GN70-ENTETE2.                                              
                05 COMM-GN70-CAPPRO-M       PIC X(3).                           
                05 COMM-GN70-CEXPO-M        PIC X(2).                           
                05 COMM-GN70-LSTATCOMP-M    PIC X(3).                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GN70-IND-RECH-ENC      PIC S9(04) COMP.            00745918
      *--                                                                       
             03 COMM-GN70-IND-RECH-ENC      PIC S9(04) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GN70-IND-RECH-ENC1     PIC S9(04) COMP.            00745918
      *--                                                                       
             03 COMM-GN70-IND-RECH-ENC1     PIC S9(04) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GN70-IND-TS2           PIC S9(04) COMP.            00745918
      *--                                                                       
             03 COMM-GN70-IND-TS2           PIC S9(04) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GN70-IND-RECH-ENC2     PIC S9(04) COMP.            00745918
      *--                                                                       
             03 COMM-GN70-IND-RECH-ENC2     PIC S9(04) COMP-5.                  
      *}                                                                        
             03 COMM-GN70-FILLER            PIC X(0301).                        
                                                                                
