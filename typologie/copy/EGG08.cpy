      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * MAJ DES PRIX ARTICLES ET PRIMES                                 00000020
      ***************************************************************** 00000030
       01   EGG08I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFFETL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MDEFFETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEFFETF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MDEFFETI  PIC X(10).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNCODICI  PIC X(7).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLREFI    PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRMPL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MPRMPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPRMPF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MPRMPI    PIC X(9).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLFAMI    PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPOTL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDEPOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEPOTF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDEPOTI   PIC X(6).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPROL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAPPROF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCAPPROI  PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPOL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXPOF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCEXPOI   PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMPACL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCOMPACL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCOMPACF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCOMPACI  PIC X(3).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDPRAL    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDPRAL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDPRAF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDPRAI    PIC X(9).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCMARQI   PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLMARQI   PIC X(20).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPOT2L  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MDEPOT2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEPOT2F  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MDEPOT2I  PIC X(6).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPRO2L      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MCAPPRO2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCAPPRO2F      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCAPPRO2I      PIC X(5).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPO2L  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCEXPO2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCEXPO2F  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCEXPO2I  PIC X(5).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMPAC2L      COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MCOMPAC2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCOMPAC2F      PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCOMPAC2I      PIC X(3).                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPCFL     COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MPCFL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MPCFF     PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MPCFI     PIC X(9).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBDACL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MLIBDACL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBDACF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MLIBDACI  PIC X(13).                                      00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRIDACL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MPRIDACL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPRIDACF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MPRIDACI  PIC X(9).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEFFICL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MCEFFICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCEFFICF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MCEFFICI  PIC X.                                          00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPOT3L  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MDEPOT3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEPOT3F  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MDEPOT3I  PIC X(6).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPRO3L      COMP PIC S9(4).                            00001020
      *--                                                                       
           02 MCAPPRO3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCAPPRO3F      PIC X.                                     00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MCAPPRO3I      PIC X(5).                                  00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPO3L  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MCEXPO3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCEXPO3F  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MCEXPO3I  PIC X(5).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMPAC3L      COMP PIC S9(4).                            00001100
      *--                                                                       
           02 MCOMPAC3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCOMPAC3F      PIC X.                                     00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MCOMPAC3I      PIC X(3).                                  00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRARL    COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MPRARL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPRARF    PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MPRARI    PIC X(9).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBLG1L  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MLIBLG1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBLG1F  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MLIBLG1I  PIC X(6).                                       00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIGNE1L  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MLIGNE1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIGNE1F  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MLIGNE1I  PIC X(72).                                      00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBLG2L  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MLIBLG2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBLG2F  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MLIBLG2I  PIC X(6).                                       00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIGNE2L  COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MLIGNE2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIGNE2F  PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MLIGNE2I  PIC X(72).                                      00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBVAR1L      COMP PIC S9(4).                            00001340
      *--                                                                       
           02 MLIBVAR1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBVAR1F      PIC X.                                     00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MLIBVAR1I      PIC X(4).                                  00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBVAR2L      COMP PIC S9(4).                            00001380
      *--                                                                       
           02 MLIBVAR2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBVAR2F      PIC X.                                     00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MLIBVAR2I      PIC X(5).                                  00001410
           02 MCOMMZD OCCURS   10 TIMES .                               00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMMZL      COMP PIC S9(4).                            00001430
      *--                                                                       
             03 MCOMMZL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCOMMZF      PIC X.                                     00001440
             03 FILLER  PIC X(4).                                       00001450
             03 MCOMMZI      PIC X(4).                                  00001460
           02 MPRZD OCCURS   10 TIMES .                                 00001470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRZL   COMP PIC S9(4).                                 00001480
      *--                                                                       
             03 MPRZL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MPRZF   PIC X.                                          00001490
             03 FILLER  PIC X(4).                                       00001500
             03 MPRZI   PIC X(5).                                       00001510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00001520
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00001530
           02 FILLER    PIC X(4).                                       00001540
           02 MPAGEI    PIC X(2).                                       00001550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETOTL      COMP PIC S9(4).                            00001560
      *--                                                                       
           02 MPAGETOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGETOTF      PIC X.                                     00001570
           02 FILLER    PIC X(4).                                       00001580
           02 MPAGETOTI      PIC X(2).                                  00001590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MASTERL   COMP PIC S9(4).                                 00001600
      *--                                                                       
           02 MASTERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MASTERF   PIC X.                                          00001610
           02 FILLER    PIC X(4).                                       00001620
           02 MASTERI   PIC X.                                          00001630
           02 MNZONPD OCCURS   10 TIMES .                               00001640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNZONPL      COMP PIC S9(4).                            00001650
      *--                                                                       
             03 MNZONPL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNZONPF      PIC X.                                     00001660
             03 FILLER  PIC X(4).                                       00001670
             03 MNZONPI      PIC X(2).                                  00001680
           02 MPRIXACD OCCURS   10 TIMES .                              00001690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIXACL     COMP PIC S9(4).                            00001700
      *--                                                                       
             03 MPRIXACL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPRIXACF     PIC X.                                     00001710
             03 FILLER  PIC X(4).                                       00001720
             03 MPRIXACI     PIC X(8).                                  00001730
           02 MPCOMMAD OCCURS   10 TIMES .                              00001740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPCOMMAL     COMP PIC S9(4).                            00001750
      *--                                                                       
             03 MPCOMMAL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPCOMMAF     PIC X.                                     00001760
             03 FILLER  PIC X(4).                                       00001770
             03 MPCOMMAI     PIC X(5).                                  00001780
           02 MDATEACD OCCURS   10 TIMES .                              00001790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEACL     COMP PIC S9(4).                            00001800
      *--                                                                       
             03 MDATEACL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDATEACF     PIC X.                                     00001810
             03 FILLER  PIC X(4).                                       00001820
             03 MDATEACI     PIC X(6).                                  00001830
           02 MPRIXND OCCURS   10 TIMES .                               00001840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIXNL      COMP PIC S9(4).                            00001850
      *--                                                                       
             03 MPRIXNL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPRIXNF      PIC X.                                     00001860
             03 FILLER  PIC X(4).                                       00001870
             03 MPRIXNI      PIC X(8).                                  00001880
           02 MDPRIXND OCCURS   10 TIMES .                              00001890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDPRIXNL     COMP PIC S9(4).                            00001900
      *--                                                                       
             03 MDPRIXNL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDPRIXNF     PIC X.                                     00001910
             03 FILLER  PIC X(4).                                       00001920
             03 MDPRIXNI     PIC X(6).                                  00001930
           02 MLCOMMD OCCURS   10 TIMES .                               00001940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOMML      COMP PIC S9(4).                            00001950
      *--                                                                       
             03 MLCOMML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLCOMMF      PIC X.                                     00001960
             03 FILLER  PIC X(4).                                       00001970
             03 MLCOMMI      PIC X(4).                                  00001980
           02 MPMBUND OCCURS   10 TIMES .                               00001990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPMBUNL      COMP PIC S9(4).                            00002000
      *--                                                                       
             03 MPMBUNL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPMBUNF      PIC X.                                     00002010
             03 FILLER  PIC X(4).                                       00002020
             03 MPMBUNI      PIC X(7).                                  00002030
           02 MPCOMMND OCCURS   10 TIMES .                              00002040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPCOMMNL     COMP PIC S9(4).                            00002050
      *--                                                                       
             03 MPCOMMNL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPCOMMNF     PIC X.                                     00002060
             03 FILLER  PIC X(4).                                       00002070
             03 MPCOMMNI     PIC X(5).                                  00002080
           02 MDCOMMND OCCURS   10 TIMES .                              00002090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCOMMNL     COMP PIC S9(4).                            00002100
      *--                                                                       
             03 MDCOMMNL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDCOMMNF     PIC X.                                     00002110
             03 FILLER  PIC X(4).                                       00002120
             03 MDCOMMNI     PIC X(6).                                  00002130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSLAVEL   COMP PIC S9(4).                                 00002140
      *--                                                                       
           02 MSLAVEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSLAVEF   PIC X.                                          00002150
           02 FILLER    PIC X(4).                                       00002160
           02 MSLAVEI   PIC X.                                          00002170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBC2L   COMP PIC S9(4).                                 00002180
      *--                                                                       
           02 MLIBC2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIBC2F   PIC X.                                          00002190
           02 FILLER    PIC X(4).                                       00002200
           02 MLIBC2I   PIC X(15).                                      00002210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODIC2L      COMP PIC S9(4).                            00002220
      *--                                                                       
           02 MNCODIC2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODIC2F      PIC X.                                     00002230
           02 FILLER    PIC X(4).                                       00002240
           02 MNCODIC2I      PIC X(7).                                  00002250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBD2L   COMP PIC S9(4).                                 00002260
      *--                                                                       
           02 MLIBD2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIBD2F   PIC X.                                          00002270
           02 FILLER    PIC X(4).                                       00002280
           02 MLIBD2I   PIC X(14).                                      00002290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFFET2L      COMP PIC S9(4).                            00002300
      *--                                                                       
           02 MDEFFET2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDEFFET2F      PIC X.                                     00002310
           02 FILLER    PIC X(4).                                       00002320
           02 MDEFFET2I      PIC X(6).                                  00002330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00002340
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00002350
           02 FILLER    PIC X(4).                                       00002360
           02 MZONCMDI  PIC X(15).                                      00002370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00002380
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00002390
           02 FILLER    PIC X(4).                                       00002400
           02 MLIBERRI  PIC X(58).                                      00002410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00002420
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00002430
           02 FILLER    PIC X(4).                                       00002440
           02 MCODTRAI  PIC X(4).                                       00002450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00002460
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00002470
           02 FILLER    PIC X(4).                                       00002480
           02 MCICSI    PIC X(5).                                       00002490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00002500
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00002510
           02 FILLER    PIC X(4).                                       00002520
           02 MNETNAMI  PIC X(8).                                       00002530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00002540
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00002550
           02 FILLER    PIC X(4).                                       00002560
           02 MSCREENI  PIC X(4).                                       00002570
      ***************************************************************** 00002580
      * MAJ DES PRIX ARTICLES ET PRIMES                                 00002590
      ***************************************************************** 00002600
       01   EGG08O REDEFINES EGG08I.                                    00002610
           02 FILLER    PIC X(12).                                      00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MDATJOUA  PIC X.                                          00002640
           02 MDATJOUC  PIC X.                                          00002650
           02 MDATJOUP  PIC X.                                          00002660
           02 MDATJOUH  PIC X.                                          00002670
           02 MDATJOUV  PIC X.                                          00002680
           02 MDATJOUO  PIC X(10).                                      00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MTIMJOUA  PIC X.                                          00002710
           02 MTIMJOUC  PIC X.                                          00002720
           02 MTIMJOUP  PIC X.                                          00002730
           02 MTIMJOUH  PIC X.                                          00002740
           02 MTIMJOUV  PIC X.                                          00002750
           02 MTIMJOUO  PIC X(5).                                       00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MDEFFETA  PIC X.                                          00002780
           02 MDEFFETC  PIC X.                                          00002790
           02 MDEFFETP  PIC X.                                          00002800
           02 MDEFFETH  PIC X.                                          00002810
           02 MDEFFETV  PIC X.                                          00002820
           02 MDEFFETO  PIC X(10).                                      00002830
           02 FILLER    PIC X(2).                                       00002840
           02 MNCODICA  PIC X.                                          00002850
           02 MNCODICC  PIC X.                                          00002860
           02 MNCODICP  PIC X.                                          00002870
           02 MNCODICH  PIC X.                                          00002880
           02 MNCODICV  PIC X.                                          00002890
           02 MNCODICO  PIC X(7).                                       00002900
           02 FILLER    PIC X(2).                                       00002910
           02 MLREFA    PIC X.                                          00002920
           02 MLREFC    PIC X.                                          00002930
           02 MLREFP    PIC X.                                          00002940
           02 MLREFH    PIC X.                                          00002950
           02 MLREFV    PIC X.                                          00002960
           02 MLREFO    PIC X(20).                                      00002970
           02 FILLER    PIC X(2).                                       00002980
           02 MPRMPA    PIC X.                                          00002990
           02 MPRMPC    PIC X.                                          00003000
           02 MPRMPP    PIC X.                                          00003010
           02 MPRMPH    PIC X.                                          00003020
           02 MPRMPV    PIC X.                                          00003030
           02 MPRMPO    PIC Z(5)9,99.                                   00003040
           02 FILLER    PIC X(2).                                       00003050
           02 MCFAMA    PIC X.                                          00003060
           02 MCFAMC    PIC X.                                          00003070
           02 MCFAMP    PIC X.                                          00003080
           02 MCFAMH    PIC X.                                          00003090
           02 MCFAMV    PIC X.                                          00003100
           02 MCFAMO    PIC X(5).                                       00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MLFAMA    PIC X.                                          00003130
           02 MLFAMC    PIC X.                                          00003140
           02 MLFAMP    PIC X.                                          00003150
           02 MLFAMH    PIC X.                                          00003160
           02 MLFAMV    PIC X.                                          00003170
           02 MLFAMO    PIC X(20).                                      00003180
           02 FILLER    PIC X(2).                                       00003190
           02 MDEPOTA   PIC X.                                          00003200
           02 MDEPOTC   PIC X.                                          00003210
           02 MDEPOTP   PIC X.                                          00003220
           02 MDEPOTH   PIC X.                                          00003230
           02 MDEPOTV   PIC X.                                          00003240
           02 MDEPOTO   PIC X(6).                                       00003250
           02 FILLER    PIC X(2).                                       00003260
           02 MCAPPROA  PIC X.                                          00003270
           02 MCAPPROC  PIC X.                                          00003280
           02 MCAPPROP  PIC X.                                          00003290
           02 MCAPPROH  PIC X.                                          00003300
           02 MCAPPROV  PIC X.                                          00003310
           02 MCAPPROO  PIC X(5).                                       00003320
           02 FILLER    PIC X(2).                                       00003330
           02 MCEXPOA   PIC X.                                          00003340
           02 MCEXPOC   PIC X.                                          00003350
           02 MCEXPOP   PIC X.                                          00003360
           02 MCEXPOH   PIC X.                                          00003370
           02 MCEXPOV   PIC X.                                          00003380
           02 MCEXPOO   PIC X(5).                                       00003390
           02 FILLER    PIC X(2).                                       00003400
           02 MCOMPACA  PIC X.                                          00003410
           02 MCOMPACC  PIC X.                                          00003420
           02 MCOMPACP  PIC X.                                          00003430
           02 MCOMPACH  PIC X.                                          00003440
           02 MCOMPACV  PIC X.                                          00003450
           02 MCOMPACO  PIC X(3).                                       00003460
           02 FILLER    PIC X(2).                                       00003470
           02 MDPRAA    PIC X.                                          00003480
           02 MDPRAC    PIC X.                                          00003490
           02 MDPRAP    PIC X.                                          00003500
           02 MDPRAH    PIC X.                                          00003510
           02 MDPRAV    PIC X.                                          00003520
           02 MDPRAO    PIC Z(5)9,99.                                   00003530
           02 FILLER    PIC X(2).                                       00003540
           02 MCMARQA   PIC X.                                          00003550
           02 MCMARQC   PIC X.                                          00003560
           02 MCMARQP   PIC X.                                          00003570
           02 MCMARQH   PIC X.                                          00003580
           02 MCMARQV   PIC X.                                          00003590
           02 MCMARQO   PIC X(5).                                       00003600
           02 FILLER    PIC X(2).                                       00003610
           02 MLMARQA   PIC X.                                          00003620
           02 MLMARQC   PIC X.                                          00003630
           02 MLMARQP   PIC X.                                          00003640
           02 MLMARQH   PIC X.                                          00003650
           02 MLMARQV   PIC X.                                          00003660
           02 MLMARQO   PIC X(20).                                      00003670
           02 FILLER    PIC X(2).                                       00003680
           02 MDEPOT2A  PIC X.                                          00003690
           02 MDEPOT2C  PIC X.                                          00003700
           02 MDEPOT2P  PIC X.                                          00003710
           02 MDEPOT2H  PIC X.                                          00003720
           02 MDEPOT2V  PIC X.                                          00003730
           02 MDEPOT2O  PIC X(6).                                       00003740
           02 FILLER    PIC X(2).                                       00003750
           02 MCAPPRO2A      PIC X.                                     00003760
           02 MCAPPRO2C PIC X.                                          00003770
           02 MCAPPRO2P PIC X.                                          00003780
           02 MCAPPRO2H PIC X.                                          00003790
           02 MCAPPRO2V PIC X.                                          00003800
           02 MCAPPRO2O      PIC X(5).                                  00003810
           02 FILLER    PIC X(2).                                       00003820
           02 MCEXPO2A  PIC X.                                          00003830
           02 MCEXPO2C  PIC X.                                          00003840
           02 MCEXPO2P  PIC X.                                          00003850
           02 MCEXPO2H  PIC X.                                          00003860
           02 MCEXPO2V  PIC X.                                          00003870
           02 MCEXPO2O  PIC X(5).                                       00003880
           02 FILLER    PIC X(2).                                       00003890
           02 MCOMPAC2A      PIC X.                                     00003900
           02 MCOMPAC2C PIC X.                                          00003910
           02 MCOMPAC2P PIC X.                                          00003920
           02 MCOMPAC2H PIC X.                                          00003930
           02 MCOMPAC2V PIC X.                                          00003940
           02 MCOMPAC2O      PIC X(3).                                  00003950
           02 FILLER    PIC X(2).                                       00003960
           02 MPCFA     PIC X.                                          00003970
           02 MPCFC     PIC X.                                          00003980
           02 MPCFP     PIC X.                                          00003990
           02 MPCFH     PIC X.                                          00004000
           02 MPCFV     PIC X.                                          00004010
           02 MPCFO     PIC Z(5)9,99.                                   00004020
           02 FILLER    PIC X(2).                                       00004030
           02 MLIBDACA  PIC X.                                          00004040
           02 MLIBDACC  PIC X.                                          00004050
           02 MLIBDACP  PIC X.                                          00004060
           02 MLIBDACH  PIC X.                                          00004070
           02 MLIBDACV  PIC X.                                          00004080
           02 MLIBDACO  PIC X(13).                                      00004090
           02 FILLER    PIC X(2).                                       00004100
           02 MPRIDACA  PIC X.                                          00004110
           02 MPRIDACC  PIC X.                                          00004120
           02 MPRIDACP  PIC X.                                          00004130
           02 MPRIDACH  PIC X.                                          00004140
           02 MPRIDACV  PIC X.                                          00004150
           02 MPRIDACO  PIC Z(5)9,99.                                   00004160
           02 FILLER    PIC X(2).                                       00004170
           02 MCEFFICA  PIC X.                                          00004180
           02 MCEFFICC  PIC X.                                          00004190
           02 MCEFFICP  PIC X.                                          00004200
           02 MCEFFICH  PIC X.                                          00004210
           02 MCEFFICV  PIC X.                                          00004220
           02 MCEFFICO  PIC X.                                          00004230
           02 FILLER    PIC X(2).                                       00004240
           02 MDEPOT3A  PIC X.                                          00004250
           02 MDEPOT3C  PIC X.                                          00004260
           02 MDEPOT3P  PIC X.                                          00004270
           02 MDEPOT3H  PIC X.                                          00004280
           02 MDEPOT3V  PIC X.                                          00004290
           02 MDEPOT3O  PIC X(6).                                       00004300
           02 FILLER    PIC X(2).                                       00004310
           02 MCAPPRO3A      PIC X.                                     00004320
           02 MCAPPRO3C PIC X.                                          00004330
           02 MCAPPRO3P PIC X.                                          00004340
           02 MCAPPRO3H PIC X.                                          00004350
           02 MCAPPRO3V PIC X.                                          00004360
           02 MCAPPRO3O      PIC X(5).                                  00004370
           02 FILLER    PIC X(2).                                       00004380
           02 MCEXPO3A  PIC X.                                          00004390
           02 MCEXPO3C  PIC X.                                          00004400
           02 MCEXPO3P  PIC X.                                          00004410
           02 MCEXPO3H  PIC X.                                          00004420
           02 MCEXPO3V  PIC X.                                          00004430
           02 MCEXPO3O  PIC X(5).                                       00004440
           02 FILLER    PIC X(2).                                       00004450
           02 MCOMPAC3A      PIC X.                                     00004460
           02 MCOMPAC3C PIC X.                                          00004470
           02 MCOMPAC3P PIC X.                                          00004480
           02 MCOMPAC3H PIC X.                                          00004490
           02 MCOMPAC3V PIC X.                                          00004500
           02 MCOMPAC3O      PIC X(3).                                  00004510
           02 FILLER    PIC X(2).                                       00004520
           02 MPRARA    PIC X.                                          00004530
           02 MPRARC    PIC X.                                          00004540
           02 MPRARP    PIC X.                                          00004550
           02 MPRARH    PIC X.                                          00004560
           02 MPRARV    PIC X.                                          00004570
           02 MPRARO    PIC Z(5)9,99.                                   00004580
           02 FILLER    PIC X(2).                                       00004590
           02 MLIBLG1A  PIC X.                                          00004600
           02 MLIBLG1C  PIC X.                                          00004610
           02 MLIBLG1P  PIC X.                                          00004620
           02 MLIBLG1H  PIC X.                                          00004630
           02 MLIBLG1V  PIC X.                                          00004640
           02 MLIBLG1O  PIC X(6).                                       00004650
           02 FILLER    PIC X(2).                                       00004660
           02 MLIGNE1A  PIC X.                                          00004670
           02 MLIGNE1C  PIC X.                                          00004680
           02 MLIGNE1P  PIC X.                                          00004690
           02 MLIGNE1H  PIC X.                                          00004700
           02 MLIGNE1V  PIC X.                                          00004710
           02 MLIGNE1O  PIC X(72).                                      00004720
           02 FILLER    PIC X(2).                                       00004730
           02 MLIBLG2A  PIC X.                                          00004740
           02 MLIBLG2C  PIC X.                                          00004750
           02 MLIBLG2P  PIC X.                                          00004760
           02 MLIBLG2H  PIC X.                                          00004770
           02 MLIBLG2V  PIC X.                                          00004780
           02 MLIBLG2O  PIC X(6).                                       00004790
           02 FILLER    PIC X(2).                                       00004800
           02 MLIGNE2A  PIC X.                                          00004810
           02 MLIGNE2C  PIC X.                                          00004820
           02 MLIGNE2P  PIC X.                                          00004830
           02 MLIGNE2H  PIC X.                                          00004840
           02 MLIGNE2V  PIC X.                                          00004850
           02 MLIGNE2O  PIC X(72).                                      00004860
           02 FILLER    PIC X(2).                                       00004870
           02 MLIBVAR1A      PIC X.                                     00004880
           02 MLIBVAR1C PIC X.                                          00004890
           02 MLIBVAR1P PIC X.                                          00004900
           02 MLIBVAR1H PIC X.                                          00004910
           02 MLIBVAR1V PIC X.                                          00004920
           02 MLIBVAR1O      PIC X(4).                                  00004930
           02 FILLER    PIC X(2).                                       00004940
           02 MLIBVAR2A      PIC X.                                     00004950
           02 MLIBVAR2C PIC X.                                          00004960
           02 MLIBVAR2P PIC X.                                          00004970
           02 MLIBVAR2H PIC X.                                          00004980
           02 MLIBVAR2V PIC X.                                          00004990
           02 MLIBVAR2O      PIC X(5).                                  00005000
           02 DFHMS1 OCCURS   10 TIMES .                                00005010
             03 FILLER       PIC X(2).                                  00005020
             03 MCOMMZA      PIC X.                                     00005030
             03 MCOMMZC PIC X.                                          00005040
             03 MCOMMZP PIC X.                                          00005050
             03 MCOMMZH PIC X.                                          00005060
             03 MCOMMZV PIC X.                                          00005070
             03 MCOMMZO      PIC X(4).                                  00005080
           02 DFHMS2 OCCURS   10 TIMES .                                00005090
             03 FILLER       PIC X(2).                                  00005100
             03 MPRZA   PIC X.                                          00005110
             03 MPRZC   PIC X.                                          00005120
             03 MPRZP   PIC X.                                          00005130
             03 MPRZH   PIC X.                                          00005140
             03 MPRZV   PIC X.                                          00005150
             03 MPRZO   PIC Z(2)9,9.                                    00005160
           02 FILLER    PIC X(2).                                       00005170
           02 MPAGEA    PIC X.                                          00005180
           02 MPAGEC    PIC X.                                          00005190
           02 MPAGEP    PIC X.                                          00005200
           02 MPAGEH    PIC X.                                          00005210
           02 MPAGEV    PIC X.                                          00005220
           02 MPAGEO    PIC ZZ.                                         00005230
           02 FILLER    PIC X(2).                                       00005240
           02 MPAGETOTA      PIC X.                                     00005250
           02 MPAGETOTC PIC X.                                          00005260
           02 MPAGETOTP PIC X.                                          00005270
           02 MPAGETOTH PIC X.                                          00005280
           02 MPAGETOTV PIC X.                                          00005290
           02 MPAGETOTO      PIC ZZ.                                    00005300
           02 FILLER    PIC X(2).                                       00005310
           02 MASTERA   PIC X.                                          00005320
           02 MASTERC   PIC X.                                          00005330
           02 MASTERP   PIC X.                                          00005340
           02 MASTERH   PIC X.                                          00005350
           02 MASTERV   PIC X.                                          00005360
           02 MASTERO   PIC X.                                          00005370
           02 DFHMS3 OCCURS   10 TIMES .                                00005380
             03 FILLER       PIC X(2).                                  00005390
             03 MNZONPA      PIC X.                                     00005400
             03 MNZONPC PIC X.                                          00005410
             03 MNZONPP PIC X.                                          00005420
             03 MNZONPH PIC X.                                          00005430
             03 MNZONPV PIC X.                                          00005440
             03 MNZONPO      PIC X(2).                                  00005450
           02 DFHMS4 OCCURS   10 TIMES .                                00005460
             03 FILLER       PIC X(2).                                  00005470
             03 MPRIXACA     PIC X.                                     00005480
             03 MPRIXACC     PIC X.                                     00005490
             03 MPRIXACP     PIC X.                                     00005500
             03 MPRIXACH     PIC X.                                     00005510
             03 MPRIXACV     PIC X.                                     00005520
             03 MPRIXACO     PIC Z(4)9,99.                              00005530
           02 DFHMS5 OCCURS   10 TIMES .                                00005540
             03 FILLER       PIC X(2).                                  00005550
             03 MPCOMMAA     PIC X.                                     00005560
             03 MPCOMMAC     PIC X.                                     00005570
             03 MPCOMMAP     PIC X.                                     00005580
             03 MPCOMMAH     PIC X.                                     00005590
             03 MPCOMMAV     PIC X.                                     00005600
             03 MPCOMMAO     PIC Z(2)9,9.                               00005610
           02 DFHMS6 OCCURS   10 TIMES .                                00005620
             03 FILLER       PIC X(2).                                  00005630
             03 MDATEACA     PIC X.                                     00005640
             03 MDATEACC     PIC X.                                     00005650
             03 MDATEACP     PIC X.                                     00005660
             03 MDATEACH     PIC X.                                     00005670
             03 MDATEACV     PIC X.                                     00005680
             03 MDATEACO     PIC X(6).                                  00005690
           02 DFHMS7 OCCURS   10 TIMES .                                00005700
             03 FILLER       PIC X(2).                                  00005710
             03 MPRIXNA      PIC X.                                     00005720
             03 MPRIXNC PIC X.                                          00005730
             03 MPRIXNP PIC X.                                          00005740
             03 MPRIXNH PIC X.                                          00005750
             03 MPRIXNV PIC X.                                          00005760
             03 MPRIXNO      PIC Z(4)9,99.                              00005770
           02 DFHMS8 OCCURS   10 TIMES .                                00005780
             03 FILLER       PIC X(2).                                  00005790
             03 MDPRIXNA     PIC X.                                     00005800
             03 MDPRIXNC     PIC X.                                     00005810
             03 MDPRIXNP     PIC X.                                     00005820
             03 MDPRIXNH     PIC X.                                     00005830
             03 MDPRIXNV     PIC X.                                     00005840
             03 MDPRIXNO     PIC X(6).                                  00005850
           02 DFHMS9 OCCURS   10 TIMES .                                00005860
             03 FILLER       PIC X(2).                                  00005870
             03 MLCOMMA      PIC X.                                     00005880
             03 MLCOMMC PIC X.                                          00005890
             03 MLCOMMP PIC X.                                          00005900
             03 MLCOMMH PIC X.                                          00005910
             03 MLCOMMV PIC X.                                          00005920
             03 MLCOMMO      PIC X(4).                                  00005930
           02 DFHMS10 OCCURS   10 TIMES .                               00005940
             03 FILLER       PIC X(2).                                  00005950
             03 MPMBUNA      PIC X.                                     00005960
             03 MPMBUNC PIC X.                                          00005970
             03 MPMBUNP PIC X.                                          00005980
             03 MPMBUNH PIC X.                                          00005990
             03 MPMBUNV PIC X.                                          00006000
             03 MPMBUNO      PIC -(4)9,9.                               00006010
           02 DFHMS11 OCCURS   10 TIMES .                               00006020
             03 FILLER       PIC X(2).                                  00006030
             03 MPCOMMNA     PIC X.                                     00006040
             03 MPCOMMNC     PIC X.                                     00006050
             03 MPCOMMNP     PIC X.                                     00006060
             03 MPCOMMNH     PIC X.                                     00006070
             03 MPCOMMNV     PIC X.                                     00006080
             03 MPCOMMNO     PIC Z(2)9,9.                               00006090
           02 DFHMS12 OCCURS   10 TIMES .                               00006100
             03 FILLER       PIC X(2).                                  00006110
             03 MDCOMMNA     PIC X.                                     00006120
             03 MDCOMMNC     PIC X.                                     00006130
             03 MDCOMMNP     PIC X.                                     00006140
             03 MDCOMMNH     PIC X.                                     00006150
             03 MDCOMMNV     PIC X.                                     00006160
             03 MDCOMMNO     PIC X(6).                                  00006170
           02 FILLER    PIC X(2).                                       00006180
           02 MSLAVEA   PIC X.                                          00006190
           02 MSLAVEC   PIC X.                                          00006200
           02 MSLAVEP   PIC X.                                          00006210
           02 MSLAVEH   PIC X.                                          00006220
           02 MSLAVEV   PIC X.                                          00006230
           02 MSLAVEO   PIC X.                                          00006240
           02 FILLER    PIC X(2).                                       00006250
           02 MLIBC2A   PIC X.                                          00006260
           02 MLIBC2C   PIC X.                                          00006270
           02 MLIBC2P   PIC X.                                          00006280
           02 MLIBC2H   PIC X.                                          00006290
           02 MLIBC2V   PIC X.                                          00006300
           02 MLIBC2O   PIC X(15).                                      00006310
           02 FILLER    PIC X(2).                                       00006320
           02 MNCODIC2A      PIC X.                                     00006330
           02 MNCODIC2C PIC X.                                          00006340
           02 MNCODIC2P PIC X.                                          00006350
           02 MNCODIC2H PIC X.                                          00006360
           02 MNCODIC2V PIC X.                                          00006370
           02 MNCODIC2O      PIC X(7).                                  00006380
           02 FILLER    PIC X(2).                                       00006390
           02 MLIBD2A   PIC X.                                          00006400
           02 MLIBD2C   PIC X.                                          00006410
           02 MLIBD2P   PIC X.                                          00006420
           02 MLIBD2H   PIC X.                                          00006430
           02 MLIBD2V   PIC X.                                          00006440
           02 MLIBD2O   PIC X(14).                                      00006450
           02 FILLER    PIC X(2).                                       00006460
           02 MDEFFET2A      PIC X.                                     00006470
           02 MDEFFET2C PIC X.                                          00006480
           02 MDEFFET2P PIC X.                                          00006490
           02 MDEFFET2H PIC X.                                          00006500
           02 MDEFFET2V PIC X.                                          00006510
           02 MDEFFET2O      PIC X(6).                                  00006520
           02 FILLER    PIC X(2).                                       00006530
           02 MZONCMDA  PIC X.                                          00006540
           02 MZONCMDC  PIC X.                                          00006550
           02 MZONCMDP  PIC X.                                          00006560
           02 MZONCMDH  PIC X.                                          00006570
           02 MZONCMDV  PIC X.                                          00006580
           02 MZONCMDO  PIC X(15).                                      00006590
           02 FILLER    PIC X(2).                                       00006600
           02 MLIBERRA  PIC X.                                          00006610
           02 MLIBERRC  PIC X.                                          00006620
           02 MLIBERRP  PIC X.                                          00006630
           02 MLIBERRH  PIC X.                                          00006640
           02 MLIBERRV  PIC X.                                          00006650
           02 MLIBERRO  PIC X(58).                                      00006660
           02 FILLER    PIC X(2).                                       00006670
           02 MCODTRAA  PIC X.                                          00006680
           02 MCODTRAC  PIC X.                                          00006690
           02 MCODTRAP  PIC X.                                          00006700
           02 MCODTRAH  PIC X.                                          00006710
           02 MCODTRAV  PIC X.                                          00006720
           02 MCODTRAO  PIC X(4).                                       00006730
           02 FILLER    PIC X(2).                                       00006740
           02 MCICSA    PIC X.                                          00006750
           02 MCICSC    PIC X.                                          00006760
           02 MCICSP    PIC X.                                          00006770
           02 MCICSH    PIC X.                                          00006780
           02 MCICSV    PIC X.                                          00006790
           02 MCICSO    PIC X(5).                                       00006800
           02 FILLER    PIC X(2).                                       00006810
           02 MNETNAMA  PIC X.                                          00006820
           02 MNETNAMC  PIC X.                                          00006830
           02 MNETNAMP  PIC X.                                          00006840
           02 MNETNAMH  PIC X.                                          00006850
           02 MNETNAMV  PIC X.                                          00006860
           02 MNETNAMO  PIC X(8).                                       00006870
           02 FILLER    PIC X(2).                                       00006880
           02 MSCREENA  PIC X.                                          00006890
           02 MSCREENC  PIC X.                                          00006900
           02 MSCREENP  PIC X.                                          00006910
           02 MSCREENH  PIC X.                                          00006920
           02 MSCREENV  PIC X.                                          00006930
           02 MSCREENO  PIC X(4).                                       00006940
                                                                                
