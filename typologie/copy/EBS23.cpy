      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * ecran de parametrage des liens                                  00000020
      ***************************************************************** 00000030
       01   EBS23I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPENT1L     COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MCTYPENT1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTYPENT1F     PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCTYPENT1I     PIC X(2).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPENT1L     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MLTYPENT1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLTYPENT1F     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLTYPENT1I     PIC X(20).                                 00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAM1L   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCFAM1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAM1F   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCFAM1I   PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAM1L   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLFAM1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLFAM1F   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLFAM1I   PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTA1L    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MSTA1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSTA1F    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MSTA1I    PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQ1L  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCMARQ1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMARQ1F  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCMARQ1I  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQ1L  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLMARQ1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLMARQ1F  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLMARQ1I  PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRIX1L   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MPRIX1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPRIX1F   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MPRIX1I   PIC X(8).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTITE1L     COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MNENTITE1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNENTITE1F     PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNENTITE1I     PIC X(7).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTITE1L     COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MLENTITE1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLENTITE1F     PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLENTITE1I     PIC X(20).                                 00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTACTIFL     COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MTOTACTIFL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MTOTACTIFF     PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MTOTACTIFI     PIC X(13).                                 00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MONGLET1L      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MONGLET1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MONGLET1F      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MONGLET1I      PIC X(8).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MONGLET2L      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MONGLET2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MONGLET2F      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MONGLET2I      PIC X(13).                                 00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MONGLET3L      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MONGLET3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MONGLET3F      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MONGLET3I      PIC X(13).                                 00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MONGLET4L      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MONGLET4L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MONGLET4F      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MONGLET4I      PIC X(11).                                 00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MPAGEI    PIC X(3).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MPAGEMAXI      PIC X(3).                                  00000810
           02 MTABI OCCURS   10 TIMES .                                 00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSEQL  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MNSEQL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSEQF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MNSEQI  PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCQUALIFL    COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MCQUALIFL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCQUALIFF    PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MCQUALIFI    PIC X.                                     00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRESELL     COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MPRESELL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPRESELF     PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MPRESELI     PIC X.                                     00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCLIENL      COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MCLIENL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCLIENF      PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MCLIENI      PIC X(2).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPENT2L   COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MCTYPENT2L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCTYPENT2F   PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MCTYPENT2I   PIC X(2).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNENTITE2L   COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MNENTITE2L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNENTITE2F   PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MNENTITE2I   PIC X(7).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLENTITE2L   COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MLENTITE2L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLENTITE2F   PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MLENTITE2I   PIC X(20).                                 00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAM2L      COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MCFAM2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCFAM2F      PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MCFAM2I      PIC X(5).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQ2L     COMP PIC S9(4).                            00001150
      *--                                                                       
             03 MCMARQ2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCMARQ2F     PIC X.                                     00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MCMARQ2I     PIC X(5).                                  00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCORIGL      COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MCORIGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCORIGF      PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MCORIGI      PIC X(5).                                  00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFAMILLEL    COMP PIC S9(4).                            00001230
      *--                                                                       
             03 MFAMILLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MFAMILLEF    PIC X.                                     00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MFAMILLEI    PIC X.                                     00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTIFL      COMP PIC S9(4).                            00001270
      *--                                                                       
             03 MACTIFL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MACTIFF      PIC X.                                     00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MACTIFI      PIC X.                                     00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRESELNL      COMP PIC S9(4).                            00001310
      *--                                                                       
           02 MPRESELNL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPRESELNF      PIC X.                                     00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MPRESELNI      PIC X.                                     00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIENNL  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MCLIENNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCLIENNF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MCLIENNI  PIC X(2).                                       00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTITENL     COMP PIC S9(4).                            00001390
      *--                                                                       
           02 MNENTITENL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNENTITENF     PIC X.                                     00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MNENTITENI     PIC X(7).                                  00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQUALIFNL      COMP PIC S9(4).                            00001430
      *--                                                                       
           02 MQUALIFNL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQUALIFNF      PIC X.                                     00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MQUALIFNI      PIC X.                                     00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPENTNL     COMP PIC S9(4).                            00001470
      *--                                                                       
           02 MCTYPENTNL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTYPENTNF     PIC X.                                     00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MCTYPENTNI     PIC X(2).                                  00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MZONCMDI  PIC X(15).                                      00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MLIBERRI  PIC X(58).                                      00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001590
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001600
           02 FILLER    PIC X(4).                                       00001610
           02 MCODTRAI  PIC X(4).                                       00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001630
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001640
           02 FILLER    PIC X(4).                                       00001650
           02 MCICSI    PIC X(5).                                       00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001670
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001680
           02 FILLER    PIC X(4).                                       00001690
           02 MNETNAMI  PIC X(8).                                       00001700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001710
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001720
           02 FILLER    PIC X(4).                                       00001730
           02 MSCREENI  PIC X(4).                                       00001740
      ***************************************************************** 00001750
      * ecran de parametrage des liens                                  00001760
      ***************************************************************** 00001770
       01   EBS23O REDEFINES EBS23I.                                    00001780
           02 FILLER    PIC X(12).                                      00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MDATJOUA  PIC X.                                          00001810
           02 MDATJOUC  PIC X.                                          00001820
           02 MDATJOUP  PIC X.                                          00001830
           02 MDATJOUH  PIC X.                                          00001840
           02 MDATJOUV  PIC X.                                          00001850
           02 MDATJOUO  PIC X(10).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MTIMJOUA  PIC X.                                          00001880
           02 MTIMJOUC  PIC X.                                          00001890
           02 MTIMJOUP  PIC X.                                          00001900
           02 MTIMJOUH  PIC X.                                          00001910
           02 MTIMJOUV  PIC X.                                          00001920
           02 MTIMJOUO  PIC X(5).                                       00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MCTYPENT1A     PIC X.                                     00001950
           02 MCTYPENT1C     PIC X.                                     00001960
           02 MCTYPENT1P     PIC X.                                     00001970
           02 MCTYPENT1H     PIC X.                                     00001980
           02 MCTYPENT1V     PIC X.                                     00001990
           02 MCTYPENT1O     PIC X(2).                                  00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MLTYPENT1A     PIC X.                                     00002020
           02 MLTYPENT1C     PIC X.                                     00002030
           02 MLTYPENT1P     PIC X.                                     00002040
           02 MLTYPENT1H     PIC X.                                     00002050
           02 MLTYPENT1V     PIC X.                                     00002060
           02 MLTYPENT1O     PIC X(20).                                 00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MCFAM1A   PIC X.                                          00002090
           02 MCFAM1C   PIC X.                                          00002100
           02 MCFAM1P   PIC X.                                          00002110
           02 MCFAM1H   PIC X.                                          00002120
           02 MCFAM1V   PIC X.                                          00002130
           02 MCFAM1O   PIC X(5).                                       00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MLFAM1A   PIC X.                                          00002160
           02 MLFAM1C   PIC X.                                          00002170
           02 MLFAM1P   PIC X.                                          00002180
           02 MLFAM1H   PIC X.                                          00002190
           02 MLFAM1V   PIC X.                                          00002200
           02 MLFAM1O   PIC X(20).                                      00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MSTA1A    PIC X.                                          00002230
           02 MSTA1C    PIC X.                                          00002240
           02 MSTA1P    PIC X.                                          00002250
           02 MSTA1H    PIC X.                                          00002260
           02 MSTA1V    PIC X.                                          00002270
           02 MSTA1O    PIC X(3).                                       00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MCMARQ1A  PIC X.                                          00002300
           02 MCMARQ1C  PIC X.                                          00002310
           02 MCMARQ1P  PIC X.                                          00002320
           02 MCMARQ1H  PIC X.                                          00002330
           02 MCMARQ1V  PIC X.                                          00002340
           02 MCMARQ1O  PIC X(5).                                       00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MLMARQ1A  PIC X.                                          00002370
           02 MLMARQ1C  PIC X.                                          00002380
           02 MLMARQ1P  PIC X.                                          00002390
           02 MLMARQ1H  PIC X.                                          00002400
           02 MLMARQ1V  PIC X.                                          00002410
           02 MLMARQ1O  PIC X(20).                                      00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MPRIX1A   PIC X.                                          00002440
           02 MPRIX1C   PIC X.                                          00002450
           02 MPRIX1P   PIC X.                                          00002460
           02 MPRIX1H   PIC X.                                          00002470
           02 MPRIX1V   PIC X.                                          00002480
           02 MPRIX1O   PIC X(8).                                       00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MNENTITE1A     PIC X.                                     00002510
           02 MNENTITE1C     PIC X.                                     00002520
           02 MNENTITE1P     PIC X.                                     00002530
           02 MNENTITE1H     PIC X.                                     00002540
           02 MNENTITE1V     PIC X.                                     00002550
           02 MNENTITE1O     PIC X(7).                                  00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MLENTITE1A     PIC X.                                     00002580
           02 MLENTITE1C     PIC X.                                     00002590
           02 MLENTITE1P     PIC X.                                     00002600
           02 MLENTITE1H     PIC X.                                     00002610
           02 MLENTITE1V     PIC X.                                     00002620
           02 MLENTITE1O     PIC X(20).                                 00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MTOTACTIFA     PIC X.                                     00002650
           02 MTOTACTIFC     PIC X.                                     00002660
           02 MTOTACTIFP     PIC X.                                     00002670
           02 MTOTACTIFH     PIC X.                                     00002680
           02 MTOTACTIFV     PIC X.                                     00002690
           02 MTOTACTIFO     PIC X(13).                                 00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MONGLET1A      PIC X.                                     00002720
           02 MONGLET1C PIC X.                                          00002730
           02 MONGLET1P PIC X.                                          00002740
           02 MONGLET1H PIC X.                                          00002750
           02 MONGLET1V PIC X.                                          00002760
           02 MONGLET1O      PIC X(8).                                  00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MONGLET2A      PIC X.                                     00002790
           02 MONGLET2C PIC X.                                          00002800
           02 MONGLET2P PIC X.                                          00002810
           02 MONGLET2H PIC X.                                          00002820
           02 MONGLET2V PIC X.                                          00002830
           02 MONGLET2O      PIC X(13).                                 00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MONGLET3A      PIC X.                                     00002860
           02 MONGLET3C PIC X.                                          00002870
           02 MONGLET3P PIC X.                                          00002880
           02 MONGLET3H PIC X.                                          00002890
           02 MONGLET3V PIC X.                                          00002900
           02 MONGLET3O      PIC X(13).                                 00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MONGLET4A      PIC X.                                     00002930
           02 MONGLET4C PIC X.                                          00002940
           02 MONGLET4P PIC X.                                          00002950
           02 MONGLET4H PIC X.                                          00002960
           02 MONGLET4V PIC X.                                          00002970
           02 MONGLET4O      PIC X(11).                                 00002980
           02 FILLER    PIC X(2).                                       00002990
           02 MPAGEA    PIC X.                                          00003000
           02 MPAGEC    PIC X.                                          00003010
           02 MPAGEP    PIC X.                                          00003020
           02 MPAGEH    PIC X.                                          00003030
           02 MPAGEV    PIC X.                                          00003040
           02 MPAGEO    PIC X(3).                                       00003050
           02 FILLER    PIC X(2).                                       00003060
           02 MPAGEMAXA      PIC X.                                     00003070
           02 MPAGEMAXC PIC X.                                          00003080
           02 MPAGEMAXP PIC X.                                          00003090
           02 MPAGEMAXH PIC X.                                          00003100
           02 MPAGEMAXV PIC X.                                          00003110
           02 MPAGEMAXO      PIC X(3).                                  00003120
           02 MTABO OCCURS   10 TIMES .                                 00003130
             03 FILLER       PIC X(2).                                  00003140
             03 MNSEQA  PIC X.                                          00003150
             03 MNSEQC  PIC X.                                          00003160
             03 MNSEQP  PIC X.                                          00003170
             03 MNSEQH  PIC X.                                          00003180
             03 MNSEQV  PIC X.                                          00003190
             03 MNSEQO  PIC X(5).                                       00003200
             03 FILLER       PIC X(2).                                  00003210
             03 MCQUALIFA    PIC X.                                     00003220
             03 MCQUALIFC    PIC X.                                     00003230
             03 MCQUALIFP    PIC X.                                     00003240
             03 MCQUALIFH    PIC X.                                     00003250
             03 MCQUALIFV    PIC X.                                     00003260
             03 MCQUALIFO    PIC X.                                     00003270
             03 FILLER       PIC X(2).                                  00003280
             03 MPRESELA     PIC X.                                     00003290
             03 MPRESELC     PIC X.                                     00003300
             03 MPRESELP     PIC X.                                     00003310
             03 MPRESELH     PIC X.                                     00003320
             03 MPRESELV     PIC X.                                     00003330
             03 MPRESELO     PIC X.                                     00003340
             03 FILLER       PIC X(2).                                  00003350
             03 MCLIENA      PIC X.                                     00003360
             03 MCLIENC PIC X.                                          00003370
             03 MCLIENP PIC X.                                          00003380
             03 MCLIENH PIC X.                                          00003390
             03 MCLIENV PIC X.                                          00003400
             03 MCLIENO      PIC X(2).                                  00003410
             03 FILLER       PIC X(2).                                  00003420
             03 MCTYPENT2A   PIC X.                                     00003430
             03 MCTYPENT2C   PIC X.                                     00003440
             03 MCTYPENT2P   PIC X.                                     00003450
             03 MCTYPENT2H   PIC X.                                     00003460
             03 MCTYPENT2V   PIC X.                                     00003470
             03 MCTYPENT2O   PIC X(2).                                  00003480
             03 FILLER       PIC X(2).                                  00003490
             03 MNENTITE2A   PIC X.                                     00003500
             03 MNENTITE2C   PIC X.                                     00003510
             03 MNENTITE2P   PIC X.                                     00003520
             03 MNENTITE2H   PIC X.                                     00003530
             03 MNENTITE2V   PIC X.                                     00003540
             03 MNENTITE2O   PIC X(7).                                  00003550
             03 FILLER       PIC X(2).                                  00003560
             03 MLENTITE2A   PIC X.                                     00003570
             03 MLENTITE2C   PIC X.                                     00003580
             03 MLENTITE2P   PIC X.                                     00003590
             03 MLENTITE2H   PIC X.                                     00003600
             03 MLENTITE2V   PIC X.                                     00003610
             03 MLENTITE2O   PIC X(20).                                 00003620
             03 FILLER       PIC X(2).                                  00003630
             03 MCFAM2A      PIC X.                                     00003640
             03 MCFAM2C PIC X.                                          00003650
             03 MCFAM2P PIC X.                                          00003660
             03 MCFAM2H PIC X.                                          00003670
             03 MCFAM2V PIC X.                                          00003680
             03 MCFAM2O      PIC X(5).                                  00003690
             03 FILLER       PIC X(2).                                  00003700
             03 MCMARQ2A     PIC X.                                     00003710
             03 MCMARQ2C     PIC X.                                     00003720
             03 MCMARQ2P     PIC X.                                     00003730
             03 MCMARQ2H     PIC X.                                     00003740
             03 MCMARQ2V     PIC X.                                     00003750
             03 MCMARQ2O     PIC X(5).                                  00003760
             03 FILLER       PIC X(2).                                  00003770
             03 MCORIGA      PIC X.                                     00003780
             03 MCORIGC PIC X.                                          00003790
             03 MCORIGP PIC X.                                          00003800
             03 MCORIGH PIC X.                                          00003810
             03 MCORIGV PIC X.                                          00003820
             03 MCORIGO      PIC X(5).                                  00003830
             03 FILLER       PIC X(2).                                  00003840
             03 MFAMILLEA    PIC X.                                     00003850
             03 MFAMILLEC    PIC X.                                     00003860
             03 MFAMILLEP    PIC X.                                     00003870
             03 MFAMILLEH    PIC X.                                     00003880
             03 MFAMILLEV    PIC X.                                     00003890
             03 MFAMILLEO    PIC X.                                     00003900
             03 FILLER       PIC X(2).                                  00003910
             03 MACTIFA      PIC X.                                     00003920
             03 MACTIFC PIC X.                                          00003930
             03 MACTIFP PIC X.                                          00003940
             03 MACTIFH PIC X.                                          00003950
             03 MACTIFV PIC X.                                          00003960
             03 MACTIFO      PIC X.                                     00003970
           02 FILLER    PIC X(2).                                       00003980
           02 MPRESELNA      PIC X.                                     00003990
           02 MPRESELNC PIC X.                                          00004000
           02 MPRESELNP PIC X.                                          00004010
           02 MPRESELNH PIC X.                                          00004020
           02 MPRESELNV PIC X.                                          00004030
           02 MPRESELNO      PIC X.                                     00004040
           02 FILLER    PIC X(2).                                       00004050
           02 MCLIENNA  PIC X.                                          00004060
           02 MCLIENNC  PIC X.                                          00004070
           02 MCLIENNP  PIC X.                                          00004080
           02 MCLIENNH  PIC X.                                          00004090
           02 MCLIENNV  PIC X.                                          00004100
           02 MCLIENNO  PIC X(2).                                       00004110
           02 FILLER    PIC X(2).                                       00004120
           02 MNENTITENA     PIC X.                                     00004130
           02 MNENTITENC     PIC X.                                     00004140
           02 MNENTITENP     PIC X.                                     00004150
           02 MNENTITENH     PIC X.                                     00004160
           02 MNENTITENV     PIC X.                                     00004170
           02 MNENTITENO     PIC X(7).                                  00004180
           02 FILLER    PIC X(2).                                       00004190
           02 MQUALIFNA      PIC X.                                     00004200
           02 MQUALIFNC PIC X.                                          00004210
           02 MQUALIFNP PIC X.                                          00004220
           02 MQUALIFNH PIC X.                                          00004230
           02 MQUALIFNV PIC X.                                          00004240
           02 MQUALIFNO      PIC X.                                     00004250
           02 FILLER    PIC X(2).                                       00004260
           02 MCTYPENTNA     PIC X.                                     00004270
           02 MCTYPENTNC     PIC X.                                     00004280
           02 MCTYPENTNP     PIC X.                                     00004290
           02 MCTYPENTNH     PIC X.                                     00004300
           02 MCTYPENTNV     PIC X.                                     00004310
           02 MCTYPENTNO     PIC X(2).                                  00004320
           02 FILLER    PIC X(2).                                       00004330
           02 MZONCMDA  PIC X.                                          00004340
           02 MZONCMDC  PIC X.                                          00004350
           02 MZONCMDP  PIC X.                                          00004360
           02 MZONCMDH  PIC X.                                          00004370
           02 MZONCMDV  PIC X.                                          00004380
           02 MZONCMDO  PIC X(15).                                      00004390
           02 FILLER    PIC X(2).                                       00004400
           02 MLIBERRA  PIC X.                                          00004410
           02 MLIBERRC  PIC X.                                          00004420
           02 MLIBERRP  PIC X.                                          00004430
           02 MLIBERRH  PIC X.                                          00004440
           02 MLIBERRV  PIC X.                                          00004450
           02 MLIBERRO  PIC X(58).                                      00004460
           02 FILLER    PIC X(2).                                       00004470
           02 MCODTRAA  PIC X.                                          00004480
           02 MCODTRAC  PIC X.                                          00004490
           02 MCODTRAP  PIC X.                                          00004500
           02 MCODTRAH  PIC X.                                          00004510
           02 MCODTRAV  PIC X.                                          00004520
           02 MCODTRAO  PIC X(4).                                       00004530
           02 FILLER    PIC X(2).                                       00004540
           02 MCICSA    PIC X.                                          00004550
           02 MCICSC    PIC X.                                          00004560
           02 MCICSP    PIC X.                                          00004570
           02 MCICSH    PIC X.                                          00004580
           02 MCICSV    PIC X.                                          00004590
           02 MCICSO    PIC X(5).                                       00004600
           02 FILLER    PIC X(2).                                       00004610
           02 MNETNAMA  PIC X.                                          00004620
           02 MNETNAMC  PIC X.                                          00004630
           02 MNETNAMP  PIC X.                                          00004640
           02 MNETNAMH  PIC X.                                          00004650
           02 MNETNAMV  PIC X.                                          00004660
           02 MNETNAMO  PIC X(8).                                       00004670
           02 FILLER    PIC X(2).                                       00004680
           02 MSCREENA  PIC X.                                          00004690
           02 MSCREENC  PIC X.                                          00004700
           02 MSCREENP  PIC X.                                          00004710
           02 MSCREENH  PIC X.                                          00004720
           02 MSCREENV  PIC X.                                          00004730
           02 MSCREENO  PIC X(4).                                       00004740
                                                                                
