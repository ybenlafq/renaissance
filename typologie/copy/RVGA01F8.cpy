      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE POMAD POPULATION ET MARCHE/ DEPT-SOC   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01F8.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01F8.                                                            
      *}                                                                        
           05  POMAD-CTABLEG2    PIC X(15).                                     
           05  POMAD-CTABLEG2-REDEF REDEFINES POMAD-CTABLEG2.                   
               10  POMAD-NSOC            PIC X(03).                             
               10  POMAD-CDEPART         PIC X(02).                             
           05  POMAD-WTABLEG     PIC X(80).                                     
           05  POMAD-WTABLEG-REDEF  REDEFINES POMAD-WTABLEG.                    
               10  POMAD-WACTIF          PIC X(01).                             
               10  POMAD-LDEPT           PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01F8-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01F8-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  POMAD-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  POMAD-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  POMAD-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  POMAD-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
