      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE POLPS DEFINITION POLICE POSTSCRIPT     *        
      *----------------------------------------------------------------*        
       01  RVGA01MG.                                                            
           05  POLPS-CTABLEG2    PIC X(15).                                     
           05  POLPS-CTABLEG2-REDEF REDEFINES POLPS-CTABLEG2.                   
               10  POLPS-NPOLPR          PIC X(03).                             
               10  POLPS-NPOLPR-N       REDEFINES POLPS-NPOLPR                  
                                         PIC 9(03).                             
           05  POLPS-WTABLEG     PIC X(80).                                     
           05  POLPS-WTABLEG-REDEF  REDEFINES POLPS-WTABLEG.                    
               10  POLPS-WACTIF          PIC X(01).                             
               10  POLPS-NPOLPS          PIC X(30).                             
               10  POLPS-ABC             PIC X(04).                             
               10  POLPS-ABC-N          REDEFINES POLPS-ABC                     
                                         PIC 9(04).                             
               10  POLPS-ORD             PIC X(04).                             
               10  POLPS-ORD-N          REDEFINES POLPS-ORD                     
                                         PIC 9(04).                             
               10  POLPS-LAR             PIC X(04).                             
               10  POLPS-LAR-N          REDEFINES POLPS-LAR                     
                                         PIC 9(04).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01MG-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  POLPS-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  POLPS-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  POLPS-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  POLPS-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
