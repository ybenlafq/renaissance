      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      ******************************************************************        
      * COPY DE LA TABLE RVTH0000                                      *        
      ******************************************************************        
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVTH0000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTH0000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTH0000.                                                            
      *}                                                                        
           02 TH00-NCODIC                                                       
              PIC X(7).                                                         
           02 TH00-COPCO                                                        
              PIC X(3).                                                         
           02 TH00-CODEOPCO                                                     
              PIC X(15).                                                        
           02 TH00-LIBOPCO                                                      
              PIC X(30).                                                        
           02 TH00-FLAGREF                                                      
              PIC X(1).                                                         
           02 TH00-FLAGVENDA                                                    
              PIC X(1).                                                         
           02 TH00-DCRESYST                                                     
              PIC X(26).                                                        
           02 TH00-DMAJSYST                                                     
              PIC X(26).                                                        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVLV0000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTH0000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTH0000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 TH00-NCODIC-F                                                     
      *       PIC S9(4) COMP.                                                   
      *--                                                                       
           02 TH00-NCODIC-F                                                     
              PIC S9(4) COMP-5.                                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 TH00-COPCO-F                                                      
      *       PIC S9(4) COMP.                                                   
      *--                                                                       
           02 TH00-COPCO-F                                                      
              PIC S9(4) COMP-5.                                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 TH00-CODEOPCO-F                                                   
      *       PIC S9(4) COMP.                                                   
      *--                                                                       
           02 TH00-CODEOPCO-F                                                   
              PIC S9(4) COMP-5.                                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 TH00-LIBOPCO-F                                                    
      *       PIC S9(4) COMP.                                                   
      *--                                                                       
           02 TH00-LIBOPCO-F                                                    
              PIC S9(4) COMP-5.                                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 TH00-FLAGREF-F                                                    
      *       PIC S9(4) COMP.                                                   
      *--                                                                       
           02 TH00-FLAGREF-F                                                    
              PIC S9(4) COMP-5.                                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 TH00-FLAGVENDA-F                                                  
      *       PIC S9(4) COMP.                                                   
      *--                                                                       
           02 TH00-FLAGVENDA-F                                                  
              PIC S9(4) COMP-5.                                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 TH00-DCRESYST-F                                                   
      *       PIC S9(4) COMP.                                                   
      *--                                                                       
           02 TH00-DCRESYST-F                                                   
              PIC S9(4) COMP-5.                                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 TH00-DMAJSYST-F                                                   
      *       PIC S9(4) COMP.                                                   
      *                                                                         
      *--                                                                       
           02 TH00-DMAJSYST-F                                                   
              PIC S9(4) COMP-5.                                                 
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
