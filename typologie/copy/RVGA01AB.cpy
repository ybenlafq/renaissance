      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE AGRAL AGREGAT. ALLEES STOCKAGE VRAC    *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01AB.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01AB.                                                            
      *}                                                                        
           05  AGRAL-CTABLEG2    PIC X(15).                                     
           05  AGRAL-CTABLEG2-REDEF REDEFINES AGRAL-CTABLEG2.                   
               10  AGRAL-NSOCDEPO        PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  AGRAL-NSOCDEPO-N     REDEFINES AGRAL-NSOCDEPO                
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  AGRAL-NDEPOT          PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  AGRAL-NDEPOT-N       REDEFINES AGRAL-NDEPOT                  
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  AGRAL-CZONE           PIC X(02).                             
               10  AGRAL-NSEQ            PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  AGRAL-NSEQ-N         REDEFINES AGRAL-NSEQ                    
                                         PIC 9(02).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  AGRAL-WTABLEG     PIC X(80).                                     
           05  AGRAL-WTABLEG-REDEF  REDEFINES AGRAL-WTABLEG.                    
               10  AGRAL-CNIVEAU         PIC X(04).                             
               10  AGRAL-CPOSIT          PIC X(06).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  AGRAL-CPOSIT-N       REDEFINES AGRAL-CPOSIT                  
                                         PIC 9(06).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01AB-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01AB-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AGRAL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  AGRAL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AGRAL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  AGRAL-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
