      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGG55   EGG55                                              00000020
      ***************************************************************** 00000030
       01   EGG55I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCIETEL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MSOCIETEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOCIETEF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSOCIETEI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONPRIXL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MZONPRIXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MZONPRIXF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MZONPRIXI      PIC X(2).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATUTL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MSTATUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTATUTF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MSTATUTI  PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSTATUTL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MDSTATUTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDSTATUTF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDSTATUTI      PIC X(8).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNCODICI  PIC X(7).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOUL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MLREFFOUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLREFFOUF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLREFFOUI      PIC X(20).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATCL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MSTATCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSTATCF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MSTATCI   PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPOL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXPOF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCEXPOI   PIC X.                                          00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSENSAPPL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MSENSAPPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSENSAPPF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MSENSAPPI      PIC X.                                     00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCFAMI    PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLFAMI    PIC X(20).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOANL     COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MOANL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MOANF     PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MOANI     PIC X.                                          00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDOANL    COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MDOANL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDOANF    PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MDOANI    PIC X(8).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCMARQI   PIC X(5).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLMARQI   PIC X(20).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOAZPL    COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MOAZPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MOAZPF    PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MOAZPI    PIC X.                                          00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDOAZPL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MDDOAZPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDDOAZPF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MDDOAZPI  PIC X(8).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFOAZPL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MDFOAZPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDFOAZPF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MDFOAZPI  PIC X(8).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSUPOAZPL      COMP PIC S9(4).                            00000940
      *--                                                                       
           02 MSUPOAZPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSUPOAZPF      PIC X.                                     00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MSUPOAZPI      PIC X.                                     00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPCL      COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MPCL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MPCF      PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MPCI      PIC X(8).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSRPL     COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MSRPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSRPF     PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MSRPI     PIC X(8).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPL      COMP PIC S9(4).                            00001060
      *--                                                                       
           02 MNSOCDEPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCDEPF      PIC X.                                     00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MNSOCDEPI      PIC X(3).                                  00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MNDEPOTI  PIC X(3).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTOCKDL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MSTOCKDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTOCKDF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MSTOCKDI  PIC X(6).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRARL    COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MPRARL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPRARF    PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MPRARI    PIC X(8).                                       00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPBFLIBL  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MPBFLIBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPBFLIBF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MPBFLIBI  PIC X(5).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPBFL     COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MPBFL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MPBFF     PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MPBFI     PIC X(8).                                       00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCDEFOUL      COMP PIC S9(4).                            00001300
      *--                                                                       
           02 MDCDEFOUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDCDEFOUF      PIC X.                                     00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MDCDEFOUI      PIC X(10).                                 00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQCDEFOUL      COMP PIC S9(4).                            00001340
      *--                                                                       
           02 MQCDEFOUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQCDEFOUF      PIC X.                                     00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MQCDEFOUI      PIC X(6).                                  00001370
           02 MTABLEI OCCURS   10 TIMES .                               00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOL1L  COMP PIC S9(4).                                 00001390
      *--                                                                       
             03 MCOL1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCOL1F  PIC X.                                          00001400
             03 FILLER  PIC X(4).                                       00001410
             03 MCOL1I  PIC X(4).                                       00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACT1L  COMP PIC S9(4).                                 00001430
      *--                                                                       
             03 MACT1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MACT1F  PIC X.                                          00001440
             03 FILLER  PIC X(4).                                       00001450
             03 MACT1I  PIC X.                                          00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIXL  COMP PIC S9(4).                                 00001470
      *--                                                                       
             03 MPRIXL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MPRIXF  PIC X.                                          00001480
             03 FILLER  PIC X(4).                                       00001490
             03 MPRIXI  PIC X(8).                                       00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCDPRXL      COMP PIC S9(4).                            00001510
      *--                                                                       
             03 MCDPRXL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCDPRXF      PIC X.                                     00001520
             03 FILLER  PIC X(4).                                       00001530
             03 MCDPRXI      PIC X(3).                                  00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00001550
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00001560
             03 FILLER  PIC X(4).                                       00001570
             03 MDEFFETI     PIC X(8).                                  00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMMENTL    COMP PIC S9(4).                            00001590
      *--                                                                       
             03 MCOMMENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCOMMENTF    PIC X.                                     00001600
             03 FILLER  PIC X(4).                                       00001610
             03 MCOMMENTI    PIC X(20).                                 00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACT2L  COMP PIC S9(4).                                 00001630
      *--                                                                       
             03 MACT2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MACT2F  PIC X.                                          00001640
             03 FILLER  PIC X(4).                                       00001650
             03 MACT2I  PIC X.                                          00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPCOMML      COMP PIC S9(4).                            00001670
      *--                                                                       
             03 MPCOMML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPCOMMF      PIC X.                                     00001680
             03 FILLER  PIC X(4).                                       00001690
             03 MPCOMMI      PIC X(6).                                  00001700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDPCOMML     COMP PIC S9(4).                            00001710
      *--                                                                       
             03 MDPCOMML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDPCOMMF     PIC X.                                     00001720
             03 FILLER  PIC X(4).                                       00001730
             03 MDPCOMMI     PIC X(8).                                  00001740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTOCKML     COMP PIC S9(4).                            00001750
      *--                                                                       
             03 MSTOCKML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTOCKMF     PIC X.                                     00001760
             03 FILLER  PIC X(4).                                       00001770
             03 MSTOCKMI     PIC X(4).                                  00001780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVTE4SL      COMP PIC S9(4).                            00001790
      *--                                                                       
             03 MVTE4SL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MVTE4SF      PIC X.                                     00001800
             03 FILLER  PIC X(4).                                       00001810
             03 MVTE4SI      PIC X(4).                                  00001820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICSL      COMP PIC S9(4).                            00001830
      *--                                                                       
           02 MNCODICSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODICSF      PIC X.                                     00001840
           02 FILLER    PIC X(4).                                       00001850
           02 MNCODICSI      PIC X(7).                                  00001860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001870
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001880
           02 FILLER    PIC X(4).                                       00001890
           02 MLIBERRI  PIC X(79).                                      00001900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001910
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001920
           02 FILLER    PIC X(4).                                       00001930
           02 MCODTRAI  PIC X(4).                                       00001940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001950
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001960
           02 FILLER    PIC X(4).                                       00001970
           02 MCICSI    PIC X(5).                                       00001980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001990
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00002000
           02 FILLER    PIC X(4).                                       00002010
           02 MNETNAMI  PIC X(8).                                       00002020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00002030
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00002040
           02 FILLER    PIC X(4).                                       00002050
           02 MSCREENI  PIC X(4).                                       00002060
      ***************************************************************** 00002070
      * SDF: EGG55   EGG55                                              00002080
      ***************************************************************** 00002090
       01   EGG55O REDEFINES EGG55I.                                    00002100
           02 FILLER    PIC X(12).                                      00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MDATJOUA  PIC X.                                          00002130
           02 MDATJOUC  PIC X.                                          00002140
           02 MDATJOUP  PIC X.                                          00002150
           02 MDATJOUH  PIC X.                                          00002160
           02 MDATJOUV  PIC X.                                          00002170
           02 MDATJOUO  PIC X(10).                                      00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MTIMJOUA  PIC X.                                          00002200
           02 MTIMJOUC  PIC X.                                          00002210
           02 MTIMJOUP  PIC X.                                          00002220
           02 MTIMJOUH  PIC X.                                          00002230
           02 MTIMJOUV  PIC X.                                          00002240
           02 MTIMJOUO  PIC X(5).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MPAGEA    PIC X.                                          00002270
           02 MPAGEC    PIC X.                                          00002280
           02 MPAGEP    PIC X.                                          00002290
           02 MPAGEH    PIC X.                                          00002300
           02 MPAGEV    PIC X.                                          00002310
           02 MPAGEO    PIC X(3).                                       00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MPAGEMAXA      PIC X.                                     00002340
           02 MPAGEMAXC PIC X.                                          00002350
           02 MPAGEMAXP PIC X.                                          00002360
           02 MPAGEMAXH PIC X.                                          00002370
           02 MPAGEMAXV PIC X.                                          00002380
           02 MPAGEMAXO      PIC X(3).                                  00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MSOCIETEA      PIC X.                                     00002410
           02 MSOCIETEC PIC X.                                          00002420
           02 MSOCIETEP PIC X.                                          00002430
           02 MSOCIETEH PIC X.                                          00002440
           02 MSOCIETEV PIC X.                                          00002450
           02 MSOCIETEO      PIC X(3).                                  00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MZONPRIXA      PIC X.                                     00002480
           02 MZONPRIXC PIC X.                                          00002490
           02 MZONPRIXP PIC X.                                          00002500
           02 MZONPRIXH PIC X.                                          00002510
           02 MZONPRIXV PIC X.                                          00002520
           02 MZONPRIXO      PIC X(2).                                  00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MSTATUTA  PIC X.                                          00002550
           02 MSTATUTC  PIC X.                                          00002560
           02 MSTATUTP  PIC X.                                          00002570
           02 MSTATUTH  PIC X.                                          00002580
           02 MSTATUTV  PIC X.                                          00002590
           02 MSTATUTO  PIC X(3).                                       00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MDSTATUTA      PIC X.                                     00002620
           02 MDSTATUTC PIC X.                                          00002630
           02 MDSTATUTP PIC X.                                          00002640
           02 MDSTATUTH PIC X.                                          00002650
           02 MDSTATUTV PIC X.                                          00002660
           02 MDSTATUTO      PIC X(8).                                  00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MNCODICA  PIC X.                                          00002690
           02 MNCODICC  PIC X.                                          00002700
           02 MNCODICP  PIC X.                                          00002710
           02 MNCODICH  PIC X.                                          00002720
           02 MNCODICV  PIC X.                                          00002730
           02 MNCODICO  PIC X(7).                                       00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MLREFFOUA      PIC X.                                     00002760
           02 MLREFFOUC PIC X.                                          00002770
           02 MLREFFOUP PIC X.                                          00002780
           02 MLREFFOUH PIC X.                                          00002790
           02 MLREFFOUV PIC X.                                          00002800
           02 MLREFFOUO      PIC X(20).                                 00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MSTATCA   PIC X.                                          00002830
           02 MSTATCC   PIC X.                                          00002840
           02 MSTATCP   PIC X.                                          00002850
           02 MSTATCH   PIC X.                                          00002860
           02 MSTATCV   PIC X.                                          00002870
           02 MSTATCO   PIC X(3).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MCEXPOA   PIC X.                                          00002900
           02 MCEXPOC   PIC X.                                          00002910
           02 MCEXPOP   PIC X.                                          00002920
           02 MCEXPOH   PIC X.                                          00002930
           02 MCEXPOV   PIC X.                                          00002940
           02 MCEXPOO   PIC X.                                          00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MSENSAPPA      PIC X.                                     00002970
           02 MSENSAPPC PIC X.                                          00002980
           02 MSENSAPPP PIC X.                                          00002990
           02 MSENSAPPH PIC X.                                          00003000
           02 MSENSAPPV PIC X.                                          00003010
           02 MSENSAPPO      PIC X.                                     00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MCFAMA    PIC X.                                          00003040
           02 MCFAMC    PIC X.                                          00003050
           02 MCFAMP    PIC X.                                          00003060
           02 MCFAMH    PIC X.                                          00003070
           02 MCFAMV    PIC X.                                          00003080
           02 MCFAMO    PIC X(5).                                       00003090
           02 FILLER    PIC X(2).                                       00003100
           02 MLFAMA    PIC X.                                          00003110
           02 MLFAMC    PIC X.                                          00003120
           02 MLFAMP    PIC X.                                          00003130
           02 MLFAMH    PIC X.                                          00003140
           02 MLFAMV    PIC X.                                          00003150
           02 MLFAMO    PIC X(20).                                      00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MOANA     PIC X.                                          00003180
           02 MOANC     PIC X.                                          00003190
           02 MOANP     PIC X.                                          00003200
           02 MOANH     PIC X.                                          00003210
           02 MOANV     PIC X.                                          00003220
           02 MOANO     PIC X.                                          00003230
           02 FILLER    PIC X(2).                                       00003240
           02 MDOANA    PIC X.                                          00003250
           02 MDOANC    PIC X.                                          00003260
           02 MDOANP    PIC X.                                          00003270
           02 MDOANH    PIC X.                                          00003280
           02 MDOANV    PIC X.                                          00003290
           02 MDOANO    PIC X(8).                                       00003300
           02 FILLER    PIC X(2).                                       00003310
           02 MCMARQA   PIC X.                                          00003320
           02 MCMARQC   PIC X.                                          00003330
           02 MCMARQP   PIC X.                                          00003340
           02 MCMARQH   PIC X.                                          00003350
           02 MCMARQV   PIC X.                                          00003360
           02 MCMARQO   PIC X(5).                                       00003370
           02 FILLER    PIC X(2).                                       00003380
           02 MLMARQA   PIC X.                                          00003390
           02 MLMARQC   PIC X.                                          00003400
           02 MLMARQP   PIC X.                                          00003410
           02 MLMARQH   PIC X.                                          00003420
           02 MLMARQV   PIC X.                                          00003430
           02 MLMARQO   PIC X(20).                                      00003440
           02 FILLER    PIC X(2).                                       00003450
           02 MOAZPA    PIC X.                                          00003460
           02 MOAZPC    PIC X.                                          00003470
           02 MOAZPP    PIC X.                                          00003480
           02 MOAZPH    PIC X.                                          00003490
           02 MOAZPV    PIC X.                                          00003500
           02 MOAZPO    PIC X.                                          00003510
           02 FILLER    PIC X(2).                                       00003520
           02 MDDOAZPA  PIC X.                                          00003530
           02 MDDOAZPC  PIC X.                                          00003540
           02 MDDOAZPP  PIC X.                                          00003550
           02 MDDOAZPH  PIC X.                                          00003560
           02 MDDOAZPV  PIC X.                                          00003570
           02 MDDOAZPO  PIC X(8).                                       00003580
           02 FILLER    PIC X(2).                                       00003590
           02 MDFOAZPA  PIC X.                                          00003600
           02 MDFOAZPC  PIC X.                                          00003610
           02 MDFOAZPP  PIC X.                                          00003620
           02 MDFOAZPH  PIC X.                                          00003630
           02 MDFOAZPV  PIC X.                                          00003640
           02 MDFOAZPO  PIC X(8).                                       00003650
           02 FILLER    PIC X(2).                                       00003660
           02 MSUPOAZPA      PIC X.                                     00003670
           02 MSUPOAZPC PIC X.                                          00003680
           02 MSUPOAZPP PIC X.                                          00003690
           02 MSUPOAZPH PIC X.                                          00003700
           02 MSUPOAZPV PIC X.                                          00003710
           02 MSUPOAZPO      PIC X.                                     00003720
           02 FILLER    PIC X(2).                                       00003730
           02 MPCA      PIC X.                                          00003740
           02 MPCC      PIC X.                                          00003750
           02 MPCP      PIC X.                                          00003760
           02 MPCH      PIC X.                                          00003770
           02 MPCV      PIC X.                                          00003780
           02 MPCO      PIC X(8).                                       00003790
           02 FILLER    PIC X(2).                                       00003800
           02 MSRPA     PIC X.                                          00003810
           02 MSRPC     PIC X.                                          00003820
           02 MSRPP     PIC X.                                          00003830
           02 MSRPH     PIC X.                                          00003840
           02 MSRPV     PIC X.                                          00003850
           02 MSRPO     PIC X(8).                                       00003860
           02 FILLER    PIC X(2).                                       00003870
           02 MNSOCDEPA      PIC X.                                     00003880
           02 MNSOCDEPC PIC X.                                          00003890
           02 MNSOCDEPP PIC X.                                          00003900
           02 MNSOCDEPH PIC X.                                          00003910
           02 MNSOCDEPV PIC X.                                          00003920
           02 MNSOCDEPO      PIC X(3).                                  00003930
           02 FILLER    PIC X(2).                                       00003940
           02 MNDEPOTA  PIC X.                                          00003950
           02 MNDEPOTC  PIC X.                                          00003960
           02 MNDEPOTP  PIC X.                                          00003970
           02 MNDEPOTH  PIC X.                                          00003980
           02 MNDEPOTV  PIC X.                                          00003990
           02 MNDEPOTO  PIC X(3).                                       00004000
           02 FILLER    PIC X(2).                                       00004010
           02 MSTOCKDA  PIC X.                                          00004020
           02 MSTOCKDC  PIC X.                                          00004030
           02 MSTOCKDP  PIC X.                                          00004040
           02 MSTOCKDH  PIC X.                                          00004050
           02 MSTOCKDV  PIC X.                                          00004060
           02 MSTOCKDO  PIC X(6).                                       00004070
           02 FILLER    PIC X(2).                                       00004080
           02 MPRARA    PIC X.                                          00004090
           02 MPRARC    PIC X.                                          00004100
           02 MPRARP    PIC X.                                          00004110
           02 MPRARH    PIC X.                                          00004120
           02 MPRARV    PIC X.                                          00004130
           02 MPRARO    PIC X(8).                                       00004140
           02 FILLER    PIC X(2).                                       00004150
           02 MPBFLIBA  PIC X.                                          00004160
           02 MPBFLIBC  PIC X.                                          00004170
           02 MPBFLIBP  PIC X.                                          00004180
           02 MPBFLIBH  PIC X.                                          00004190
           02 MPBFLIBV  PIC X.                                          00004200
           02 MPBFLIBO  PIC X(5).                                       00004210
           02 FILLER    PIC X(2).                                       00004220
           02 MPBFA     PIC X.                                          00004230
           02 MPBFC     PIC X.                                          00004240
           02 MPBFP     PIC X.                                          00004250
           02 MPBFH     PIC X.                                          00004260
           02 MPBFV     PIC X.                                          00004270
           02 MPBFO     PIC X(8).                                       00004280
           02 FILLER    PIC X(2).                                       00004290
           02 MDCDEFOUA      PIC X.                                     00004300
           02 MDCDEFOUC PIC X.                                          00004310
           02 MDCDEFOUP PIC X.                                          00004320
           02 MDCDEFOUH PIC X.                                          00004330
           02 MDCDEFOUV PIC X.                                          00004340
           02 MDCDEFOUO      PIC X(10).                                 00004350
           02 FILLER    PIC X(2).                                       00004360
           02 MQCDEFOUA      PIC X.                                     00004370
           02 MQCDEFOUC PIC X.                                          00004380
           02 MQCDEFOUP PIC X.                                          00004390
           02 MQCDEFOUH PIC X.                                          00004400
           02 MQCDEFOUV PIC X.                                          00004410
           02 MQCDEFOUO      PIC X(6).                                  00004420
           02 MTABLEO OCCURS   10 TIMES .                               00004430
             03 FILLER       PIC X(2).                                  00004440
             03 MCOL1A  PIC X.                                          00004450
             03 MCOL1C  PIC X.                                          00004460
             03 MCOL1P  PIC X.                                          00004470
             03 MCOL1H  PIC X.                                          00004480
             03 MCOL1V  PIC X.                                          00004490
             03 MCOL1O  PIC X(4).                                       00004500
             03 FILLER       PIC X(2).                                  00004510
             03 MACT1A  PIC X.                                          00004520
             03 MACT1C  PIC X.                                          00004530
             03 MACT1P  PIC X.                                          00004540
             03 MACT1H  PIC X.                                          00004550
             03 MACT1V  PIC X.                                          00004560
             03 MACT1O  PIC X.                                          00004570
             03 FILLER       PIC X(2).                                  00004580
             03 MPRIXA  PIC X.                                          00004590
             03 MPRIXC  PIC X.                                          00004600
             03 MPRIXP  PIC X.                                          00004610
             03 MPRIXH  PIC X.                                          00004620
             03 MPRIXV  PIC X.                                          00004630
             03 MPRIXO  PIC X(8).                                       00004640
             03 FILLER       PIC X(2).                                  00004650
             03 MCDPRXA      PIC X.                                     00004660
             03 MCDPRXC PIC X.                                          00004670
             03 MCDPRXP PIC X.                                          00004680
             03 MCDPRXH PIC X.                                          00004690
             03 MCDPRXV PIC X.                                          00004700
             03 MCDPRXO      PIC X(3).                                  00004710
             03 FILLER       PIC X(2).                                  00004720
             03 MDEFFETA     PIC X.                                     00004730
             03 MDEFFETC     PIC X.                                     00004740
             03 MDEFFETP     PIC X.                                     00004750
             03 MDEFFETH     PIC X.                                     00004760
             03 MDEFFETV     PIC X.                                     00004770
             03 MDEFFETO     PIC X(8).                                  00004780
             03 FILLER       PIC X(2).                                  00004790
             03 MCOMMENTA    PIC X.                                     00004800
             03 MCOMMENTC    PIC X.                                     00004810
             03 MCOMMENTP    PIC X.                                     00004820
             03 MCOMMENTH    PIC X.                                     00004830
             03 MCOMMENTV    PIC X.                                     00004840
             03 MCOMMENTO    PIC X(20).                                 00004850
             03 FILLER       PIC X(2).                                  00004860
             03 MACT2A  PIC X.                                          00004870
             03 MACT2C  PIC X.                                          00004880
             03 MACT2P  PIC X.                                          00004890
             03 MACT2H  PIC X.                                          00004900
             03 MACT2V  PIC X.                                          00004910
             03 MACT2O  PIC X.                                          00004920
             03 FILLER       PIC X(2).                                  00004930
             03 MPCOMMA      PIC X.                                     00004940
             03 MPCOMMC PIC X.                                          00004950
             03 MPCOMMP PIC X.                                          00004960
             03 MPCOMMH PIC X.                                          00004970
             03 MPCOMMV PIC X.                                          00004980
             03 MPCOMMO      PIC X(6).                                  00004990
             03 FILLER       PIC X(2).                                  00005000
             03 MDPCOMMA     PIC X.                                     00005010
             03 MDPCOMMC     PIC X.                                     00005020
             03 MDPCOMMP     PIC X.                                     00005030
             03 MDPCOMMH     PIC X.                                     00005040
             03 MDPCOMMV     PIC X.                                     00005050
             03 MDPCOMMO     PIC X(8).                                  00005060
             03 FILLER       PIC X(2).                                  00005070
             03 MSTOCKMA     PIC X.                                     00005080
             03 MSTOCKMC     PIC X.                                     00005090
             03 MSTOCKMP     PIC X.                                     00005100
             03 MSTOCKMH     PIC X.                                     00005110
             03 MSTOCKMV     PIC X.                                     00005120
             03 MSTOCKMO     PIC X(4).                                  00005130
             03 FILLER       PIC X(2).                                  00005140
             03 MVTE4SA      PIC X.                                     00005150
             03 MVTE4SC PIC X.                                          00005160
             03 MVTE4SP PIC X.                                          00005170
             03 MVTE4SH PIC X.                                          00005180
             03 MVTE4SV PIC X.                                          00005190
             03 MVTE4SO      PIC X(4).                                  00005200
           02 FILLER    PIC X(2).                                       00005210
           02 MNCODICSA      PIC X.                                     00005220
           02 MNCODICSC PIC X.                                          00005230
           02 MNCODICSP PIC X.                                          00005240
           02 MNCODICSH PIC X.                                          00005250
           02 MNCODICSV PIC X.                                          00005260
           02 MNCODICSO      PIC X(7).                                  00005270
           02 FILLER    PIC X(2).                                       00005280
           02 MLIBERRA  PIC X.                                          00005290
           02 MLIBERRC  PIC X.                                          00005300
           02 MLIBERRP  PIC X.                                          00005310
           02 MLIBERRH  PIC X.                                          00005320
           02 MLIBERRV  PIC X.                                          00005330
           02 MLIBERRO  PIC X(79).                                      00005340
           02 FILLER    PIC X(2).                                       00005350
           02 MCODTRAA  PIC X.                                          00005360
           02 MCODTRAC  PIC X.                                          00005370
           02 MCODTRAP  PIC X.                                          00005380
           02 MCODTRAH  PIC X.                                          00005390
           02 MCODTRAV  PIC X.                                          00005400
           02 MCODTRAO  PIC X(4).                                       00005410
           02 FILLER    PIC X(2).                                       00005420
           02 MCICSA    PIC X.                                          00005430
           02 MCICSC    PIC X.                                          00005440
           02 MCICSP    PIC X.                                          00005450
           02 MCICSH    PIC X.                                          00005460
           02 MCICSV    PIC X.                                          00005470
           02 MCICSO    PIC X(5).                                       00005480
           02 FILLER    PIC X(2).                                       00005490
           02 MNETNAMA  PIC X.                                          00005500
           02 MNETNAMC  PIC X.                                          00005510
           02 MNETNAMP  PIC X.                                          00005520
           02 MNETNAMH  PIC X.                                          00005530
           02 MNETNAMV  PIC X.                                          00005540
           02 MNETNAMO  PIC X(8).                                       00005550
           02 FILLER    PIC X(2).                                       00005560
           02 MSCREENA  PIC X.                                          00005570
           02 MSCREENC  PIC X.                                          00005580
           02 MSCREENP  PIC X.                                          00005590
           02 MSCREENH  PIC X.                                          00005600
           02 MSCREENV  PIC X.                                          00005610
           02 MSCREENO  PIC X(4).                                       00005620
                                                                                
