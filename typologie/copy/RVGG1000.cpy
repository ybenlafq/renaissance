      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGG1000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGG1000                         
      **********************************************************                
       01  RVGG1000.                                                            
           02  GG10-NDEMALI                                                     
               PIC X(0007).                                                     
           02  GG10-NCONC                                                       
               PIC X(0004).                                                     
           02  GG10-DCREATION                                                   
               PIC X(0008).                                                     
           02  GG10-DRELEVE                                                     
               PIC X(0008).                                                     
           02  GG10-LCARACTR                                                    
               PIC X(0015).                                                     
           02  GG10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGG1000                                  
      **********************************************************                
       01  RVGG1000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG10-NDEMALI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG10-NDEMALI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG10-NCONC-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG10-NCONC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG10-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG10-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG10-DRELEVE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG10-DRELEVE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG10-LCARACTR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG10-LCARACTR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GG10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
      *}                                                                        
