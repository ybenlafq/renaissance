      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVRX5500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRX5500                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRX5500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRX5500.                                                            
      *}                                                                        
           02  RX55-NCODIC                                                      
               PIC X(0007).                                                     
           02  RX55-NCONC                                                       
               PIC X(0004).                                                     
           02  RX55-DRELEVE                                                     
               PIC X(0008).                                                     
           02  RX55-DTRTREL                                                     
               PIC X(0008).                                                     
           02  RX55-PRELEVE                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  RX55-LCOMMENT                                                    
               PIC X(0020).                                                     
           02  RX55-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRX5500                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRX5500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRX5500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX55-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX55-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX55-NCONC-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX55-NCONC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX55-DRELEVE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX55-DRELEVE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX55-DTRTREL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX55-DTRTREL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX55-PRELEVE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX55-PRELEVE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX55-LCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX55-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX55-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX55-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
