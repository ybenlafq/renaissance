      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE BSPRO REFERENCEMENT PROFIL LIEN        *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBSPRO.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBSPRO.                                                             
      *}                                                                        
           05  BSPRO-CTABLEG2    PIC X(15).                                     
           05  BSPRO-CTABLEG2-REDEF REDEFINES BSPRO-CTABLEG2.                   
               10  BSPRO-PROFLIEN        PIC X(05).                             
           05  BSPRO-WTABLEG     PIC X(80).                                     
           05  BSPRO-WTABLEG-REDEF  REDEFINES BSPRO-WTABLEG.                    
               10  BSPRO-WACTIF          PIC X(01).                             
               10  BSPRO-LIBELLE         PIC X(20).                             
               10  BSPRO-WDONNEES        PIC X(16).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBSPRO-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBSPRO-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BSPRO-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  BSPRO-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BSPRO-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  BSPRO-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
