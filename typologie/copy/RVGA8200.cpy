      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RVGA8200                           *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA8200.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA8200.                                                            
      *}                                                                        
           10 GA82-CTAXE             PIC X(05).                                 
           10 GA82-NCODIC            PIC X(07).                                 
           10 GA82-CPAYS             PIC X(02).                                 
           10 GA82-NENTCDE           PIC X(05).                                 
           10 GA82-DEFFET            PIC X(08).                                 
           10 GA82-DFINEFFET         PIC X(08).                                 
           10 GA82-PMONTANT          PIC S9(5)V9(2) COMP-3.                     
           10 GA82-WIMPORT           PIC X(01).                                 
           10 GA82-CPRODECO          PIC X(20).                                 
           10 GA82-DSYST             PIC S9(13) COMP-3.                         
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA8200-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA8200-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA82-CTAXE-F           PIC S9(4) COMP.                            
      *--                                                                       
           10 GA82-CTAXE-F           PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA82-NCODIC-F          PIC S9(4) COMP.                            
      *--                                                                       
           10 GA82-NCODIC-F          PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA82-CPAYS-F           PIC S9(4) COMP.                            
      *--                                                                       
           10 GA82-CPAYS-F           PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA82-NENTCDE-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 GA82-NENTCDE-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA82-DEFFET-F          PIC S9(4) COMP.                            
      *--                                                                       
           10 GA82-DEFFET-F          PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA82-DFINEFFET-F       PIC S9(4) COMP.                            
      *--                                                                       
           10 GA82-DFINEFFET-F       PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA82-PMONTANT-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 GA82-PMONTANT-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA82-WIMPORT-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 GA82-WIMPORT-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA82-CPRODECO-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 GA82-CPRODECO-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA82-DSYST-F           PIC S9(4) COMP.                            
      *                                                                         
      *--                                                                       
           10 GA82-DSYST-F           PIC S9(4) COMP-5.                          
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
