      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE RX4500                       
      ******************************************************************        
      *                                                                         
       CLEF-RX4500             SECTION.                                         
      *                                                                         
           MOVE 'RVRX4500          '       TO   TABLE-NAME.                     
           MOVE 'RX4500'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-RX4500. EXIT.                                                   
                EJECT                                                           
                                                                                
