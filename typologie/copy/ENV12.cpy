      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: parametrage dtd flux websp                                 00000020
      ***************************************************************** 00000030
       01   ENV12I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDTDWCSL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MCDTDWCSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDTDWCSF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCDTDWCSI      PIC X(20).                                 00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDTDNCGL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MCDTDNCGL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDTDNCGF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCDTDNCGI      PIC X(20).                                 00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCACTIONL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MCACTIONL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCACTIONF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCACTIONI      PIC X.                                     00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCNOMTRAL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCNOMTRAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCNOMTRAF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCNOMTRAI      PIC X(4).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CNOMEXTRACTL   COMP PIC S9(4).                            00000300
      *--                                                                       
           02 CNOMEXTRACTL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 CNOMEXTRACTF   PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 CNOMEXTRACTI   PIC X(6).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBATTENTEL   COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MQNBATTENTEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MQNBATTENTEF   PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MQNBATTENTEI   PIC X(7).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQDELAIATTL    COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MQDELAIATTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MQDELAIATTF    PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MQDELAIATTI    PIC X(5).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFONCTIONL    COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MCFONCTIONL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCFONCTIONF    PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCFONCTIONI    PIC X(3).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCNOMTSL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCNOMTSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCNOMTSF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCNOMTSI  PIC X(8).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQMSGALERTL    COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MQMSGALERTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MQMSGALERTF    PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MQMSGALERTI    PIC X(7).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQMSGREACTL    COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MQMSGREACTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MQMSGREACTF    PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MQMSGREACTI    PIC X(7).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQMSGERRORL    COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MQMSGERRORL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MQMSGERRORF    PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MQMSGERRORI    PIC X(7).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQMSGOKL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MQMSGOKL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQMSGOKF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MQMSGOKI  PIC X(7).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQMSGSSRETL    COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MQMSGSSRETL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MQMSGSSRETF    PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MQMSGSSRETI    PIC X(7).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLIBERRI  PIC X(78).                                      00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCODTRAI  PIC X(4).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCICSI    PIC X(5).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MNETNAMI  PIC X(8).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MSCREENI  PIC X(4).                                       00000890
      ***************************************************************** 00000900
      * SDF: parametrage dtd flux websp                                 00000910
      ***************************************************************** 00000920
       01   ENV12O REDEFINES ENV12I.                                    00000930
           02 FILLER    PIC X(12).                                      00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MDATJOUA  PIC X.                                          00000960
           02 MDATJOUC  PIC X.                                          00000970
           02 MDATJOUP  PIC X.                                          00000980
           02 MDATJOUH  PIC X.                                          00000990
           02 MDATJOUV  PIC X.                                          00001000
           02 MDATJOUO  PIC X(10).                                      00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MTIMJOUA  PIC X.                                          00001030
           02 MTIMJOUC  PIC X.                                          00001040
           02 MTIMJOUP  PIC X.                                          00001050
           02 MTIMJOUH  PIC X.                                          00001060
           02 MTIMJOUV  PIC X.                                          00001070
           02 MTIMJOUO  PIC X(5).                                       00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MCDTDWCSA      PIC X.                                     00001100
           02 MCDTDWCSC PIC X.                                          00001110
           02 MCDTDWCSP PIC X.                                          00001120
           02 MCDTDWCSH PIC X.                                          00001130
           02 MCDTDWCSV PIC X.                                          00001140
           02 MCDTDWCSO      PIC X(20).                                 00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MCDTDNCGA      PIC X.                                     00001170
           02 MCDTDNCGC PIC X.                                          00001180
           02 MCDTDNCGP PIC X.                                          00001190
           02 MCDTDNCGH PIC X.                                          00001200
           02 MCDTDNCGV PIC X.                                          00001210
           02 MCDTDNCGO      PIC X(20).                                 00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MCACTIONA      PIC X.                                     00001240
           02 MCACTIONC PIC X.                                          00001250
           02 MCACTIONP PIC X.                                          00001260
           02 MCACTIONH PIC X.                                          00001270
           02 MCACTIONV PIC X.                                          00001280
           02 MCACTIONO      PIC X.                                     00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MCNOMTRAA      PIC X.                                     00001310
           02 MCNOMTRAC PIC X.                                          00001320
           02 MCNOMTRAP PIC X.                                          00001330
           02 MCNOMTRAH PIC X.                                          00001340
           02 MCNOMTRAV PIC X.                                          00001350
           02 MCNOMTRAO      PIC X(4).                                  00001360
           02 FILLER    PIC X(2).                                       00001370
           02 CNOMEXTRACTA   PIC X.                                     00001380
           02 CNOMEXTRACTC   PIC X.                                     00001390
           02 CNOMEXTRACTP   PIC X.                                     00001400
           02 CNOMEXTRACTH   PIC X.                                     00001410
           02 CNOMEXTRACTV   PIC X.                                     00001420
           02 CNOMEXTRACTO   PIC X(6).                                  00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MQNBATTENTEA   PIC X.                                     00001450
           02 MQNBATTENTEC   PIC X.                                     00001460
           02 MQNBATTENTEP   PIC X.                                     00001470
           02 MQNBATTENTEH   PIC X.                                     00001480
           02 MQNBATTENTEV   PIC X.                                     00001490
           02 MQNBATTENTEO   PIC X(7).                                  00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MQDELAIATTA    PIC X.                                     00001520
           02 MQDELAIATTC    PIC X.                                     00001530
           02 MQDELAIATTP    PIC X.                                     00001540
           02 MQDELAIATTH    PIC X.                                     00001550
           02 MQDELAIATTV    PIC X.                                     00001560
           02 MQDELAIATTO    PIC X(5).                                  00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MCFONCTIONA    PIC X.                                     00001590
           02 MCFONCTIONC    PIC X.                                     00001600
           02 MCFONCTIONP    PIC X.                                     00001610
           02 MCFONCTIONH    PIC X.                                     00001620
           02 MCFONCTIONV    PIC X.                                     00001630
           02 MCFONCTIONO    PIC X(3).                                  00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MCNOMTSA  PIC X.                                          00001660
           02 MCNOMTSC  PIC X.                                          00001670
           02 MCNOMTSP  PIC X.                                          00001680
           02 MCNOMTSH  PIC X.                                          00001690
           02 MCNOMTSV  PIC X.                                          00001700
           02 MCNOMTSO  PIC X(8).                                       00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MQMSGALERTA    PIC X.                                     00001730
           02 MQMSGALERTC    PIC X.                                     00001740
           02 MQMSGALERTP    PIC X.                                     00001750
           02 MQMSGALERTH    PIC X.                                     00001760
           02 MQMSGALERTV    PIC X.                                     00001770
           02 MQMSGALERTO    PIC X(7).                                  00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MQMSGREACTA    PIC X.                                     00001800
           02 MQMSGREACTC    PIC X.                                     00001810
           02 MQMSGREACTP    PIC X.                                     00001820
           02 MQMSGREACTH    PIC X.                                     00001830
           02 MQMSGREACTV    PIC X.                                     00001840
           02 MQMSGREACTO    PIC X(7).                                  00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MQMSGERRORA    PIC X.                                     00001870
           02 MQMSGERRORC    PIC X.                                     00001880
           02 MQMSGERRORP    PIC X.                                     00001890
           02 MQMSGERRORH    PIC X.                                     00001900
           02 MQMSGERRORV    PIC X.                                     00001910
           02 MQMSGERRORO    PIC X(7).                                  00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MQMSGOKA  PIC X.                                          00001940
           02 MQMSGOKC  PIC X.                                          00001950
           02 MQMSGOKP  PIC X.                                          00001960
           02 MQMSGOKH  PIC X.                                          00001970
           02 MQMSGOKV  PIC X.                                          00001980
           02 MQMSGOKO  PIC X(7).                                       00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MQMSGSSRETA    PIC X.                                     00002010
           02 MQMSGSSRETC    PIC X.                                     00002020
           02 MQMSGSSRETP    PIC X.                                     00002030
           02 MQMSGSSRETH    PIC X.                                     00002040
           02 MQMSGSSRETV    PIC X.                                     00002050
           02 MQMSGSSRETO    PIC X(7).                                  00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MLIBERRA  PIC X.                                          00002080
           02 MLIBERRC  PIC X.                                          00002090
           02 MLIBERRP  PIC X.                                          00002100
           02 MLIBERRH  PIC X.                                          00002110
           02 MLIBERRV  PIC X.                                          00002120
           02 MLIBERRO  PIC X(78).                                      00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MCODTRAA  PIC X.                                          00002150
           02 MCODTRAC  PIC X.                                          00002160
           02 MCODTRAP  PIC X.                                          00002170
           02 MCODTRAH  PIC X.                                          00002180
           02 MCODTRAV  PIC X.                                          00002190
           02 MCODTRAO  PIC X(4).                                       00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MCICSA    PIC X.                                          00002220
           02 MCICSC    PIC X.                                          00002230
           02 MCICSP    PIC X.                                          00002240
           02 MCICSH    PIC X.                                          00002250
           02 MCICSV    PIC X.                                          00002260
           02 MCICSO    PIC X(5).                                       00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MNETNAMA  PIC X.                                          00002290
           02 MNETNAMC  PIC X.                                          00002300
           02 MNETNAMP  PIC X.                                          00002310
           02 MNETNAMH  PIC X.                                          00002320
           02 MNETNAMV  PIC X.                                          00002330
           02 MNETNAMO  PIC X(8).                                       00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MSCREENA  PIC X.                                          00002360
           02 MSCREENC  PIC X.                                          00002370
           02 MSCREENP  PIC X.                                          00002380
           02 MSCREENH  PIC X.                                          00002390
           02 MSCREENV  PIC X.                                          00002400
           02 MSCREENO  PIC X(4).                                       00002410
                                                                                
