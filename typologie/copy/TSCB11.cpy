      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * ITSSC                                                                   
      * GESTION PRIX PRIMES                                                     
E0452 ******************************************************************        
      * DE02005 26/09/08 SUPPORT EVOLUTION CBN                                  
      *                  TS TRANS CB11                                          
      ******************************************************************        
      * CONSTANTES RELATIVES AU TRAITEMENT                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB11-INIT                     PIC S9(4) COMP VALUE +000.           
      *--                                                                       
       77 TC-CB11-INIT                     PIC S9(4) COMP-5 VALUE +000.         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB11-AF-DATE                  PIC S9(4) COMP VALUE +020.           
      *--                                                                       
       77 TC-CB11-AF-DATE                  PIC S9(4) COMP-5 VALUE +020.         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB11-AF-REL                   PIC S9(4) COMP VALUE +110.           
      *--                                                                       
       77 TC-CB11-AF-REL                   PIC S9(4) COMP-5 VALUE +110.         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB11-AF-MAG                   PIC S9(4) COMP VALUE +120.           
      *--                                                                       
       77 TC-CB11-AF-MAG                   PIC S9(4) COMP-5 VALUE +120.         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB11-AF-PX                    PIC S9(4) COMP VALUE +200.           
      *--                                                                       
       77 TC-CB11-AF-PX                    PIC S9(4) COMP-5 VALUE +200.         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB11-AF-PX-ZON                PIC S9(4) COMP VALUE +210.           
      *--                                                                       
       77 TC-CB11-AF-PX-ZON                PIC S9(4) COMP-5 VALUE +210.         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB11-AF-PX-MAG                PIC S9(4) COMP VALUE +220.           
      *--                                                                       
       77 TC-CB11-AF-PX-MAG                PIC S9(4) COMP-5 VALUE +220.         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB11-AF-PX-NR                 PIC S9(4) COMP VALUE +221.           
      *--                                                                       
       77 TC-CB11-AF-PX-NR                 PIC S9(4) COMP-5 VALUE +221.         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB11-MOD                      PIC S9(4) COMP VALUE +100.           
      *--                                                                       
       77 TC-CB11-MOD                      PIC S9(4) COMP-5 VALUE +100.         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB11-SUP                      PIC S9(4) COMP VALUE -100.           
      *--                                                                       
       77 TC-CB11-SUP                      PIC S9(4) COMP-5 VALUE -100.         
      *}                                                                        
       77 TC-CB11-NC-IDENT                 PIC X(8) VALUE 'CB11CONC'.           
       77 TC-CB11-NL-IDENT                 PIC X(8) VALUE 'CB11LIEU'.           
      * TS                                                                      
       01 TS-CB11-DATA.                                                         
           02 TS-CB11-GESTION.                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB11-FL-POS                 PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB11-FL-POS                 PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB11-RL-POS                 PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB11-RL-POS                 PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB11-AF-POS                 PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB11-AF-POS                 PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB11-NC-POS                 PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB11-NC-POS                 PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB11-NL-POS                 PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB11-NL-POS                 PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB11-MG-POS                 PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB11-MG-POS                 PIC S9(4) COMP-5.              
      *}                                                                        
      *    TS FILTRE                                                            
           02 TS-CB11-FL-ENR.                                                   
      *        PRODUIT TRAITE                                                   
               05 TS-CB11-FL-PRODUIT.                                           
                   10 TS-CB11-FL-NZONPRIX            PIC X(2).                  
                   10 TS-CB11-FL-CHEFPROD            PIC X(5).                  
                   10 TS-CB11-FL-LCHEFPROD           PIC X(20).                 
                   10 TS-CB11-FL-CFAM                PIC X(5).                  
                   10 TS-CB11-FL-LFAM                PIC X(20).                 
                   10 TS-CB11-FL-NCODIC              PIC X(7).                  
                   10 TS-CB11-FL-CMARQ               PIC X(5).                  
                   10 TS-CB11-FL-LREFFOURN           PIC X(20).                 
      *        LIEN AVEC TS RL                                                  
               05 TS-CB11-FL-LIEN.                                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB11-FL-RL-MIN          PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB11-FL-RL-MIN          PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB11-FL-RL-MAX          PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB11-FL-RL-MAX          PIC S9(4) COMP-5.              
      *}                                                                        
      *        LIEN AVEC DEBUT ZONE                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB11-FL-FL                  PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB11-FL-FL                  PIC S9(4) COMP-5.              
      *}                                                                        
      *    TS RELEVE                                                            
           02 TS-CB11-RL-ENR.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB11-RL-STATUT              PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB11-RL-STATUT              PIC S9(4) COMP-5.              
      *}                                                                        
      *        PRODUIT TRAITE                                                   
               05 TS-CB11-RL-PRODUIT.                                           
                   10 TS-CB11-RL-NCODIC              PIC X(7).                  
      *        RELEVE DE PRIX                                                   
               05 TS-CB11-RL-RELEVE.                                            
                   10 TS-CB11-RL-NRELEVE             PIC X(7).                  
                   10 TP-CB11-RL-PRELEVE         PIC S9(7)V9(2) COMP-3.         
                   10 TS-CB11-RL-LCOMMENT            PIC X(20).                 
                   10 TS-CB11-RL-NMAG                PIC X(3).                  
                   10 TS-CB11-RL-NCONC               PIC X(4).                  
                   10 TS-CB11-RL-LENSCONC            PIC X(15).                 
                   10 TS-CB11-RL-DRELEVE             PIC X(8).                  
      **       PRIX EXCEPTIONNELS                                               
      *        05 TS-CB11-RL-PEXC.                                              
      *            10 TS-CB11-RL-NLIEU               PIC X(5).                  
      *            10 TS-CB11-RL-NCONC               PIC X(5).                  
      *            10 TS-CB11-RL-PEXPTTC             PIC X(5).                  
      *            10 TS-CB11-RL-DEFFET              PIC X(5).                  
      *            10 TS-CB11-RL-DFINEFFET           PIC X(8).                  
      *    TS AFFICHAGE                                                         
           02 TS-CB11-AF-ENR.                                                   
      *        PRODUIT TRAITE                                                   
               05 TS-CB11-AF-RELEVE.                                            
                   10 TS-CB11-AF-SEL                 PIC X(1).                  
                   10 TP-CB11-AF-MONTANT          PIC S9(7)V9(2) COMP-3.        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB11-AF-ZONE1-TYPE          PIC S9(4) COMP.            
      *--                                                                       
                   10 TB-CB11-AF-ZONE1-TYPE          PIC S9(4) COMP-5.          
      *}                                                                        
                   10 TS-CB11-AF-ZONE1            PIC X(8)  VALUE SPACE.        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB11-AF-ZONE2-TYPE          PIC S9(4) COMP.            
      *--                                                                       
                   10 TB-CB11-AF-ZONE2-TYPE          PIC S9(4) COMP-5.          
      *}                                                                        
                   10 TS-CB11-AF-ZONE2            PIC X(36) VALUE SPACE.        
                   10 TS-CB11-AF-LMAG REDEFINES TS-CB11-AF-ZONE2.               
                       15 TS-CB11-AF-SMAG OCCURS 9.                             
                           20 TS-CB11-AF-NLIEU       PIC X(3).                  
                           20 FILLER                 PIC X(1).                  
                   10 TS-CB11-AF-RL     REDEFINES TS-CB11-AF-ZONE2.             
                       15 TS-CB11-AF-NMAG            PIC X(3).                  
                       15 FILLER                     PIC X(1).                  
                       15 TS-CB11-AF-NCONC           PIC X(4).                  
                       15 FILLER                     PIC X(1).                  
                       15 TS-CB11-AF-LENSCONC        PIC X(15).                 
                       15 FILLER                     PIC X(1).                  
                       15 TS-CB11-AF-DRELEVE         PIC X(5).                  
                       15 FILLER                     PIC X(1).                  
                       15 FILLER                     PIC X(1).                  
                       15 TS-CB11-AF-TMB             PIC X(4).                  
                   10 TS-CB11-AF-RL-LCOMMENT             PIC X(20).             
                   10 TS-CB11-AF-PEXC.                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                15 TS-CB11-AF-PX-TYPE             PIC S9(4) COMP.        
      *--                                                                       
                       15 TS-CB11-AF-PX-TYPE             PIC S9(4)              
                                                                 COMP-5.        
      *}                                                                        
                       15 TS-CB11-AF-PX-SIGN             PIC X(1).              
                       15 TS-CB11-AF-PX-NLIEU            PIC X(3).              
                       15 TS-CB11-AF-PX-NCONC            PIC X(4).              
                       15 TS-CB11-AF-PX-PEXPTTC          PIC X(8).              
                       15 TS-CB11-AF-PX-DFINEFFET        PIC X(5).              
                       15 TS-CB11-AF-PX-STOCK            PIC X(4).              
                       15 TS-CB11-AF-PX-V4S              PIC X(4).              
      *        LIEN AVEC TS                                                     
               05 TS-CB11-AF-LIEN.                                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB11-AF-RL-MIN          PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB11-AF-RL-MIN          PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB11-AF-RL-MAX          PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB11-AF-RL-MAX          PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB11-AF-AF-MAX          PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB11-AF-AF-MAX          PIC S9(4) COMP-5.              
      *}                                                                        
      *    TS CONCURRENT                                                        
           02 TS-CB11-NC-ENR.                                                   
      *        1 LIGNE PAR CONCURRENT, L'INDEX DONNE SON NUMERO                 
      *        LIEN AVEC TS NL                                                  
               05 TS-CB11-NC-LIEN.                                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB11-NC-NL-MIN          PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB11-NC-NL-MIN          PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB11-NC-NL-MAX          PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB11-NC-NL-MAX          PIC S9(4) COMP-5.              
      *}                                                                        
      *    TS CONCURRENT, MAGASINS LIES                                         
           02 TS-CB11-NL-ENR.                                                   
      *        LECTURE VIA LA TS CONCURRENT                                     
               05 TS-CB11-NL-NLIEU               PIC X(3).                      
      *        LIEN AVEC TS NC                                                  
               05 TS-CB11-NL-LIEN.                                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB11-NL-NC-POS          PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB11-NL-NC-POS          PIC S9(4) COMP-5.              
      *}                                                                        
      *    TS MAGASINS                                                          
           02 TS-CB11-MG-ENR.                                                   
      *        1 LIGNE PAR MAGASIN, L'INDEX DONNE SON NUMERO                    
               05 TS-CB11-MG-NZONPRIX            PIC X(2).                      
      *        LIEN AVEC TS LIEU                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB11-MG-LI                  PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB11-MG-LI                  PIC S9(4) COMP-5.              
      *}                                                                        
      *        LIEN AVEC TS PRIX POUR PRIX EXCEPTIONNEL EN COURS                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB11-MG-PP                  PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB11-MG-PP                  PIC S9(4) COMP-5.              
      *}                                                                        
      *        NOMBRE DE RELEVES LIES                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB11-MG-RL                  PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB11-MG-RL                  PIC S9(4) COMP-5.              
      *}                                                                        
      *        PRIX EN COURS                                                    
               05 TP-CB11-MG-PX-MIN              PIC S9(7)V9(2) COMP-3.         
E04866*        PRIX POUR VALIDITE RELEVE (AVEC PEREMPTION)                      
E04866         05 TP-CB11-MG-PX-VAL              PIC S9(7)V9(2) COMP-3.         
      *        STATUT CB EN COURS                                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB11-MG-STATUT              PIC S9(4)      COMP.           
      *                                                                         
      *--                                                                       
               05 TB-CB11-MG-STATUT              PIC S9(4) COMP-5.              
                                                                                
      *}                                                                        
