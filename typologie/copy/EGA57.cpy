      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA57   EGA57                                              00000020
      ***************************************************************** 00000030
       01   EGA57I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNCODICI  PIC X(7).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLREFI    PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFAML    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNFAMF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNFAMI    PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLFAMI    PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIENL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCLIENL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCLIENF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCLIENI   PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIENL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLLIENL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIENF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLLIENI   PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMARQL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNMARQF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNMARQI   PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLMARQI   PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNZONPVL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNZONPVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNZONPVF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNZONPVI  PIC X(2).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLZONPVL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLZONPVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLZONPVF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLZONPVI  PIC X(20).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MZONCMDI  PIC X(15).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLIBERRI  PIC X(58).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCODTRAI  PIC X(4).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCICSI    PIC X(5).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MNETNAMI  PIC X(8).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MSCREENI  PIC X(4).                                       00000810
      ***************************************************************** 00000820
      * SDF: EGA57   EGA57                                              00000830
      ***************************************************************** 00000840
       01   EGA57O REDEFINES EGA57I.                                    00000850
           02 FILLER    PIC X(12).                                      00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MDATJOUA  PIC X.                                          00000880
           02 MDATJOUC  PIC X.                                          00000890
           02 MDATJOUP  PIC X.                                          00000900
           02 MDATJOUH  PIC X.                                          00000910
           02 MDATJOUV  PIC X.                                          00000920
           02 MDATJOUO  PIC X(10).                                      00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MTIMJOUA  PIC X.                                          00000950
           02 MTIMJOUC  PIC X.                                          00000960
           02 MTIMJOUP  PIC X.                                          00000970
           02 MTIMJOUH  PIC X.                                          00000980
           02 MTIMJOUV  PIC X.                                          00000990
           02 MTIMJOUO  PIC X(5).                                       00001000
           02 FILLER    PIC X(2).                                       00001010
           02 MWFONCA   PIC X.                                          00001020
           02 MWFONCC   PIC X.                                          00001030
           02 MWFONCP   PIC X.                                          00001040
           02 MWFONCH   PIC X.                                          00001050
           02 MWFONCV   PIC X.                                          00001060
           02 MWFONCO   PIC X(3).                                       00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MNCODICA  PIC X.                                          00001090
           02 MNCODICC  PIC X.                                          00001100
           02 MNCODICP  PIC X.                                          00001110
           02 MNCODICH  PIC X.                                          00001120
           02 MNCODICV  PIC X.                                          00001130
           02 MNCODICO  PIC X(7).                                       00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MLREFA    PIC X.                                          00001160
           02 MLREFC    PIC X.                                          00001170
           02 MLREFP    PIC X.                                          00001180
           02 MLREFH    PIC X.                                          00001190
           02 MLREFV    PIC X.                                          00001200
           02 MLREFO    PIC X(20).                                      00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MNFAMA    PIC X.                                          00001230
           02 MNFAMC    PIC X.                                          00001240
           02 MNFAMP    PIC X.                                          00001250
           02 MNFAMH    PIC X.                                          00001260
           02 MNFAMV    PIC X.                                          00001270
           02 MNFAMO    PIC X(5).                                       00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MLFAMA    PIC X.                                          00001300
           02 MLFAMC    PIC X.                                          00001310
           02 MLFAMP    PIC X.                                          00001320
           02 MLFAMH    PIC X.                                          00001330
           02 MLFAMV    PIC X.                                          00001340
           02 MLFAMO    PIC X(20).                                      00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MCLIENA   PIC X.                                          00001370
           02 MCLIENC   PIC X.                                          00001380
           02 MCLIENP   PIC X.                                          00001390
           02 MCLIENH   PIC X.                                          00001400
           02 MCLIENV   PIC X.                                          00001410
           02 MCLIENO   PIC X(5).                                       00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MLLIENA   PIC X.                                          00001440
           02 MLLIENC   PIC X.                                          00001450
           02 MLLIENP   PIC X.                                          00001460
           02 MLLIENH   PIC X.                                          00001470
           02 MLLIENV   PIC X.                                          00001480
           02 MLLIENO   PIC X(20).                                      00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MNMARQA   PIC X.                                          00001510
           02 MNMARQC   PIC X.                                          00001520
           02 MNMARQP   PIC X.                                          00001530
           02 MNMARQH   PIC X.                                          00001540
           02 MNMARQV   PIC X.                                          00001550
           02 MNMARQO   PIC X(5).                                       00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MLMARQA   PIC X.                                          00001580
           02 MLMARQC   PIC X.                                          00001590
           02 MLMARQP   PIC X.                                          00001600
           02 MLMARQH   PIC X.                                          00001610
           02 MLMARQV   PIC X.                                          00001620
           02 MLMARQO   PIC X(20).                                      00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MNZONPVA  PIC X.                                          00001650
           02 MNZONPVC  PIC X.                                          00001660
           02 MNZONPVP  PIC X.                                          00001670
           02 MNZONPVH  PIC X.                                          00001680
           02 MNZONPVV  PIC X.                                          00001690
           02 MNZONPVO  PIC X(2).                                       00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MLZONPVA  PIC X.                                          00001720
           02 MLZONPVC  PIC X.                                          00001730
           02 MLZONPVP  PIC X.                                          00001740
           02 MLZONPVH  PIC X.                                          00001750
           02 MLZONPVV  PIC X.                                          00001760
           02 MLZONPVO  PIC X(20).                                      00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MZONCMDA  PIC X.                                          00001790
           02 MZONCMDC  PIC X.                                          00001800
           02 MZONCMDP  PIC X.                                          00001810
           02 MZONCMDH  PIC X.                                          00001820
           02 MZONCMDV  PIC X.                                          00001830
           02 MZONCMDO  PIC X(15).                                      00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MLIBERRA  PIC X.                                          00001860
           02 MLIBERRC  PIC X.                                          00001870
           02 MLIBERRP  PIC X.                                          00001880
           02 MLIBERRH  PIC X.                                          00001890
           02 MLIBERRV  PIC X.                                          00001900
           02 MLIBERRO  PIC X(58).                                      00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MCODTRAA  PIC X.                                          00001930
           02 MCODTRAC  PIC X.                                          00001940
           02 MCODTRAP  PIC X.                                          00001950
           02 MCODTRAH  PIC X.                                          00001960
           02 MCODTRAV  PIC X.                                          00001970
           02 MCODTRAO  PIC X(4).                                       00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MCICSA    PIC X.                                          00002000
           02 MCICSC    PIC X.                                          00002010
           02 MCICSP    PIC X.                                          00002020
           02 MCICSH    PIC X.                                          00002030
           02 MCICSV    PIC X.                                          00002040
           02 MCICSO    PIC X(5).                                       00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MNETNAMA  PIC X.                                          00002070
           02 MNETNAMC  PIC X.                                          00002080
           02 MNETNAMP  PIC X.                                          00002090
           02 MNETNAMH  PIC X.                                          00002100
           02 MNETNAMV  PIC X.                                          00002110
           02 MNETNAMO  PIC X(8).                                       00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MSCREENA  PIC X.                                          00002140
           02 MSCREENC  PIC X.                                          00002150
           02 MSCREENP  PIC X.                                          00002160
           02 MSCREENH  PIC X.                                          00002170
           02 MSCREENV  PIC X.                                          00002180
           02 MSCREENO  PIC X(4).                                       00002190
                                                                                
