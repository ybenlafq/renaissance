      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
       01  Z-COMMAREA-MSE50.                                            00000020
           02 COMM-MSE50-ENTETE-RETOUR.                                 00000030
      *---- BOOLEEN TYPE D'ERREUR                                       00000040
      *    ----         EN SORTIE                                       00000050
              03 COMM-MSE50-TYPERR     PIC 9(01).                       00000060
                 88 MSE50-NO-ERROR       VALUE 0.                       00000070
                 88 MSE50-APPL-ERROR     VALUE 1.                       00000080
                 88 MSE50-DB2-ERROR      VALUE 2.                       00000090
              03 COMM-MSE50-FUNC-SQL   PIC X(08).                       00000100
              03 COMM-MSE50-TABLE-NAME PIC X(08).                       00000110
              03 COMM-MSE50-SQLCODE    PIC S9(04).                      00000120
              03 COMM-MSE50-POSITION   PIC  9(03).                      00000130
      *    ----         EN ENTREE                                       00000140
           02 COMM-MSE50-MODE-CHGT       PIC 9(01).                     00000150
              88 CHGT-DATA-ALL        VALUE 0.                          00000160
              88 CHGT-DATA-GENE       VALUE 1.                          00000170
              88 CHGT-DATA-DESCR      VALUE 2.                          00000180
              88 CHGT-DATA-DOSS       VALUE 3.                          00000190
              88 CHGT-DATA-FOURN      VALUE 4.                          00000200
           02 COMM-MSE50-MODE-CHGT2      PIC 9(01).                     00000210
              88 PAS-CHGT-CFAM        VALUE 0.                          00000220
              88 CHGT-CFAM            VALUE 1.                          00000230
      *    ---- DONNEES EN ENTREE                                       00000240
           02 COMM-MSE50-CODESFONCTION   PIC X(12).                     00000250
           02 COMM-MSE50-DROIT           PIC X(01).                     00000260
           02 COMM-MSE50-DATEJ           PIC X(08).                     00000270
           02 COMM-MSE50-DATEJ1          PIC X(08).                     00000280
           02 COMM-MSE50-WFONC           PIC X(03).                     00000290
      *   ---- DONNEES EN ENTREE / SORTIE                               00000300
           02 COMM-MSE50-NCSERV          PIC X(05).                     00000310
           02 COMM-MSE50-CFAM            PIC X(05).                     00000320
      *    ---- DONNEES EN SORTIE                                       00000330
           02 COMM-MSE50-LCSERVL         PIC X(20).                     00000340
           02 COMM-MSE50-LCSERVC         PIC X(05).                     00000350
           02 COMM-MSE50-NORDRE          PIC X(03).                     00000360
           02 COMM-MSE50-LCFAM           PIC X(20).                     00000370
           02 COMM-MSE50-DCREATION       PIC X(08).                     00000380
           02 COMM-MSE50-DMAJ            PIC X(08).                     00000390
           02 COMM-MSE50-COPER           PIC X(05).                     00000400
           02 COMM-MSE50-LOPER           PIC X(20).                     00000410
           02 COMM-MSE50-TYPSERV         PIC X(01).                     00000420
           02 COMM-MSE50-CTXTVA          PIC X(01).                     00000430
           02 COMM-MSE50-NTXTVA          PIC S999V99.                   00000440
           02 COMM-MSE50-LTXTVA          PIC X(20).                     00000450
           02 COMM-MSE50-CASSORTC        PIC X(05).                     00000460
           02 COMM-MSE50-CASSORTF        PIC X(05).                     00000470
           02 COMM-MSE50-DATEF           PIC X(08).                     00000480
           02 COMM-MSE50-DATEC           PIC X(08).                     00000490
           02 COMM-MSE50-CSIGNE          PIC X(01).                     00000500
           02 COMM-MSE50-CHEF            PIC X(05).                     00000510
           02 COMM-MSE50-LCHEF           PIC X(20).                     00000520
FV-3.2     02 FILLER                     PIC X(13).                     00000530
FV-3.2*    02 COMM-MSE50-EAN             PIC X(13).                     00000540
FVM        02 COMM-MSE50-CTYPINN         PIC X(1).                      00000550
FVM        02 COMM-MSE50-CLIREQ          PIC X.                         00000560
           02 COMM-MSE50-CTYP            PIC X(05).                     00000570
           02 COMM-MSE50-LTYP            PIC X(20).                     00000580
           02 COMM-MSE50-DATA-GESTION.                                  00000590
FV1           03 COMM-MSE50-LIGNE-GESTION   OCCURS 30.                  00000600
FV1   *       03 COMM-MSE50-LIGNE-GESTION   OCCURS 20.                  00000610
                 05 COMM-MSE50-LGEST           PIC X(30).               00000620
                 05 COMM-MSE50-CGEST           PIC X(05).               00000630
FV1   *          05 COMM-MSE50-VGEST           PIC X.                   00000640
FV1              05 COMM-MSE50-FGEST           PIC X.                   00000650
FV1              05 COMM-MSE50-VGEST           PIC X(20).               00000660
FV1              05 COMM-MSE50-TYPE            PIC X.                   00000670
FV1              05 COMM-MSE50-TABLE           PIC X(6).                00000680
FV1              05 COMM-MSE50-VISIBLE         PIC X.                   00000690
FV1              05 COMM-MSE50-OBLIG           PIC X.                   00000700
FV1              05 COMM-MSE50-SAISIE          PIC X(5).                00000710
FV1              05 COMM-MSE50-VALDFAUT        PIC X(20).               00000720
              03 COMM-MSE50-NBRATT             PIC 99.                  00000730
FV1           03 COMM-MSE50-NBRATT-VISIBLE     PIC 99.                  00000740
           02 COMM-MSE50-DATA-DESCR.                                    00000750
              03 COMM-MSE50-OBLIG2             PIC X.                   00000760
              03 COMM-MSE50-LIGNE-DESCR     OCCURS 30.                  00000770
                 05 COMM-MSE50-CCDESC          PIC X(05).               00000780
                 05 COMM-MSE50-LCDESC          PIC X(20).               00000790
                 05 COMM-MSE50-CVDESC          PIC X(05).               00000800
                 05 COMM-MSE50-LVDESC          PIC X(20).               00000810
           02 COMM-MSE50-DATA-DOSS.                                     00000820
              03 COMM-MSE50-LIGNE-DOSS      OCCURS 30.                  00000830
                 05 COMM-MSE50-CDOSS           PIC X(05).               00000840
                 05 COMM-MSE50-LDOSS           PIC X(20).               00000850
                 05 COMM-MSE50-DEFFET          PIC X(08).               00000860
                 05 COMM-MSE50-DFINEFFET       PIC X(08).               00000870
                 05 COMM-MSE50-NSEQ            PIC X(03).               00000880
           02 COMM-MSE50-DATA-CFOURN.                                   00000890
              03 COMM-MSE50-OBLIG5             PIC X.                   00000900
              03 COMM-MSE50-NENTCDE-SAVE       PIC X(5).                00000910
              03 COMM-MSE50-LIGNE-CFOURN    OCCURS 30.                  00000920
                 05 COMM-MSE50-NENTCDE         PIC X(05).               00000930
                 05 COMM-MSE50-LENTCDE         PIC X(20).               00000940
                 05 COMM-MSE50-WENTCDE         PIC X(01).               00000950
                                                                        00000960
           02 COMM-MSE50-DATA-PRIX.                                     00000970
              03 COMM-MSE50-OBLIG4             PIC X.                   00000980
              03 COMM-MSE50-PRMPAC             PIC S9(7)V99 COMP-3.     00000990
              03 COMM-MSE50-PRMPAC-F           PIC X.                   00001000
              03 COMM-MSE50-DPRMPAC            PIC X(8).                00001010
              03 COMM-MSE50-PRMPN              PIC S9(7)V99 COMP-3.     00001020
              03 COMM-MSE50-PRMPN-F            PIC X.                   00001030
              03 COMM-MSE50-DPRMPN             PIC X(8).                00001040
              03 COMM-MSE50-LIGNE-PRIX      OCCURS 30.                  00001050
                 05 COMM-MSE50-NZONPRIX        PIC X(2).                00001060
                 05 COMM-MSE50-PRIXAC          PIC S9(7)V99 COMP-3.     00001070
                 05 COMM-MSE50-PRIXAC-F        PIC X.                   00001080
                 05 COMM-MSE50-DATEPAC         PIC X(8).                00001090
                 05 COMM-MSE50-INTERAC         PIC S9(5)V99 COMP-3.     00001100
                 05 COMM-MSE50-INTERAC-F       PIC X.                   00001110
                 05 COMM-MSE50-DATEIAC         PIC X(8).                00001120
                 05 COMM-MSE50-PRIXN           PIC S9(7)V99 COMP-3.     00001130
                 05 COMM-MSE50-PRIXN-F         PIC X.                   00001140
                 05 COMM-MSE50-DATEN           PIC X(8).                00001150
                 05 COMM-MSE50-INTERN          PIC S9(5)V99 COMP-3.     00001160
                 05 COMM-MSE50-INTERN-F        PIC X.                   00001170
           02 COMM-MSE50-CTYPINN-SAI           PIC X.                   00001180
           02 COMM-MSE50-FLAG-AFF.                                      00001190
              03 COMM-MSE50-MLCSERV-AFF  PIC X.                         00001200
              03 COMM-MSE50-MNORDRE-AFF  PIC X.                         00001210
              03 COMM-MSE50-COPER-AFF    PIC X.                         00001220
              03 COMM-MSE50-CFAM-AFF     PIC X.                         00001230
              03 COMM-MSE50-MCTYPSER-AFF PIC X.                         00001240
              03 COMM-MSE50-MCTXTVA-AFF  PIC X.                         00001250
              03 COMM-MSE50-MCHEF-AFF    PIC X.                         00001260
FV-3.2        03 FILLER                  PIC X.                         00001270
FV-3.2*       03 COMM-MSE50-EAN-AFF      PIC X.                         00001280
FVM           03 COMM-MSE50-CTYPINN-AFF  PIC X.                         00001290
              03 COMM-MSE50-MCASSOR-AFF  PIC X.                         00001300
              03 COMM-MSE50-MSIGNE-AFF   PIC X.                         00001310
              03 COMM-MSE50-MCTYP-AFF    PIC X.                         00001320
FV1   *       03 COMM-MSE50-CODGES-AFF   OCCURS 20.                     00001330
FV2        02    COMM-MSE50-CODGES-AFF-GR.                              00001340
FV1             05 COMM-MSE50-CODGES-AFF   OCCURS 30.                   00001350
                   07 TS-CGESTION-AFF      PIC X.                       00001360
                                                                        00001370
FV1   *    02 COMM-MSE50-DATA-INNOV.                                    00001380
FV1   *       03 COMM-MSE50-OBLIG6             PIC X.                   00001390
FV1   ****    03 COMM-MSE50-CTYPINN            PIC X.                   00001400
FV1   *       03 COMM-MSE50-FQTEMUL            PIC X.                   00001410
FV1   *       03 COMM-MSE50-FREMISE            PIC X.                   00001420
FV1   *       03 COMM-MSE50-FTICKET            PIC X.                   00001430
FV1   *       03 COMM-MSE50-FGEO               PIC X.                   00001440
FV1   *       03 COMM-MSE50-FASSOC             PIC X.                   00001450
FV1   *       03 COMM-MSE50-MAXASSO            PIC S9(05) COMP-3.       00001460
FV1   *       03 COMM-MSE50-CLTREQ             PIC X.                   00001470
FV1   *       03 COMM-MSE50-CLIREQ             PIC X.                   00001480
FV1   *       03 COMM-MSE50-SERCTRA            PIC X.                   00001490
FV1   *       03 COMM-MSE50-CCONTRA            PIC X(05).               00001500
FV1   *       03 COMM-MSE50-LCCONTR            PIC X(20).               00001510
                                                                        00001520
FV1   *    02 COMM-MSE50-FLAG-AFF6.                                     00001530
      *       03 COMM-MSE50-CTYPINN-AFF6 PIC X.                         00001540
      *       03 COMM-MSE50-FQTEMUL-AFF6 PIC X.                         00001550
      *       03 COMM-MSE50-FREMISE-AFF6 PIC X.                         00001560
      *       03 COMM-MSE50-FTICKET-AFF6 PIC X.                         00001570
      *       03 COMM-MSE50-FGEO-AFF6    PIC X.                         00001580
      *       03 COMM-MSE50-FASSOC-AFF6  PIC X.                         00001590
      *       03 COMM-MSE50-MAXASSO-AFF6 PIC X.                         00001600
      *       03 COMM-MSE50-CLTREQ-AFF6  PIC X.                         00001610
      *       03 COMM-MSE50-SERCTRA-AFF6 PIC X.                         00001620
      *       03 COMM-MSE50-CCONTRA-AFF6 PIC X.                         00001630
                                                                        00001640
                                                                        00001650
211208     02 COMM-MSE57-DATA-REDBOX.                                   00001660
211208        03 COMM-MSE50-OBLIG7             PIC X.                   00001670
211208        03 COMM-MSE50-LIBELLE            PIC X(50).               00001680
211208        03 COMM-MSE50-AFFICH             PIC X(05).               00001690
211208        03 COMM-MSE50-SEQFAM             PIC X(02).               00001700
211208        03 COMM-MSE50-RDV                PIC X(03).               00001710
211208        03 COMM-MSE50-LIEN               PIC X(07).               00001720
211208        03 COMM-MSE50-LLIEN              PIC X(20).               00001730
211208        03 COMM-MSE50-QTE                PIC X(3).                00001740
211208        03 COMM-MSE50-INFO1              PIC X(39).               00001750
211208        03 COMM-MSE50-INFO2              PIC X(72).               00001760
211208        03 COMM-MSE50-INFO3              PIC X(72).               00001770
211208        03 COMM-MSE50-INFO4              PIC X(72).               00001780
                                                                        00001790
211208     02 COMM-MSE50-FLAG-AFF7.                                     00001800
              03 COMM-MSE50-LIBELLE-AFF7 PIC X.                         00001810
              03 COMM-MSE50-AFFICH-AFF7  PIC X.                         00001820
              03 COMM-MSE50-SEQFAM-AFF7  PIC X.                         00001830
              03 COMM-MSE50-RDV-AFF7     PIC X.                         00001840
              03 COMM-MSE50-LIEN-AFF7    PIC X.                         00001850
              03 COMM-MSE50-QTE-AFF7     PIC X.                         00001860
              03 COMM-MSE50-INFO1-AFF7   PIC X.                         00001870
              03 COMM-MSE50-INFO2-AFF7   PIC X.                         00001880
              03 COMM-MSE50-INFO3-AFF7   PIC X.                         00001890
              03 COMM-MSE50-INFO4-AFF7   PIC X.                         00001900
                                                                        00001910
PV0408*    02 COMM-MSE50-FILLER          PIC X(916).                    00001920
PV0408*RG0102 COMM-MSE50-FILLER          PIC X(513).                    00001930
RG01  *    02 COMM-MSE50-FILLER          PIC X(512).                    00001940
FVM   *    02 COMM-MSE50-FILLER          PIC X(511).                    00001950
FV1        02 COMM-MSE50-FILLER          PIC X(452).                    00001960
                                                                                
