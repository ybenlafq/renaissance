      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GVCOM COMMENTAIRE DE VENTE STRUCTURE   *        
      *----------------------------------------------------------------*        
       01  RVGA01T7.                                                            
           05  GVCOM-CTABLEG2    PIC X(15).                                     
           05  GVCOM-CTABLEG2-REDEF REDEFINES GVCOM-CTABLEG2.                   
               10  GVCOM-CFONCT          PIC X(03).                             
               10  GVCOM-CODE            PIC X(05).                             
           05  GVCOM-WTABLEG     PIC X(80).                                     
           05  GVCOM-WTABLEG-REDEF  REDEFINES GVCOM-WTABLEG.                    
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  GVCOM-LIBELLE         PIC X(60).                             
           EXEC SQL END DECLARE SECTION END-EXEC.
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01T7-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GVCOM-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GVCOM-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GVCOM-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GVCOM-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
