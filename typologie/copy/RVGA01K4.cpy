      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE DSTAU TAUX POUR CODIC DEJA DEPRECIE    *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVGA01K4.                                                            
           05  DSTAU-CTABLEG2    PIC X(15).                                     
           05  DSTAU-CTABLEG2-REDEF REDEFINES DSTAU-CTABLEG2.                   
               10  DSTAU-CTAUX           PIC X(05).                             
           05  DSTAU-WTABLEG     PIC X(80).                                     
           05  DSTAU-WTABLEG-REDEF  REDEFINES DSTAU-WTABLEG.                    
               10  DSTAU-QTAUX           PIC S9(03)V9(02)     COMP-3.           
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01K4-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  DSTAU-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  DSTAU-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  DSTAU-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  DSTAU-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
