      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGG14   EGG14                                              00000020
      ***************************************************************** 00000030
       01   EGG14I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCODICI  PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLREFI    PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLFAMI    PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCMARQI   PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLMARQI   PIC X(20).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMZPL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNUMZPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNUMZPF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNUMZPI   PIC X(2).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLZPL     COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLZPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MLZPF     PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLZPI     PIC X(20).                                      00000530
           02 MPREXI OCCURS   12 TIMES .                                00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIXL  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MPRIXL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MPRIXF  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MPRIXI  PIC X(9).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEL  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MDATEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDATEF  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MDATEI  PIC X(8).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMMENL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MCOMMENL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCOMMENF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCOMMENI     PIC X(10).                                 00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MZONCMDI  PIC X(15).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MLIBERRI  PIC X(58).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCODTRAI  PIC X(4).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCICSI    PIC X(5).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MNETNAMI  PIC X(8).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MSCREENI  PIC X(4).                                       00000900
      ***************************************************************** 00000910
      * SDF: EGG14   EGG14                                              00000920
      ***************************************************************** 00000930
       01   EGG14O REDEFINES EGG14I.                                    00000940
           02 FILLER    PIC X(12).                                      00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MDATJOUA  PIC X.                                          00000970
           02 MDATJOUC  PIC X.                                          00000980
           02 MDATJOUP  PIC X.                                          00000990
           02 MDATJOUH  PIC X.                                          00001000
           02 MDATJOUV  PIC X.                                          00001010
           02 MDATJOUO  PIC X(10).                                      00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MTIMJOUA  PIC X.                                          00001040
           02 MTIMJOUC  PIC X.                                          00001050
           02 MTIMJOUP  PIC X.                                          00001060
           02 MTIMJOUH  PIC X.                                          00001070
           02 MTIMJOUV  PIC X.                                          00001080
           02 MTIMJOUO  PIC X(5).                                       00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MWPAGEA   PIC X.                                          00001110
           02 MWPAGEC   PIC X.                                          00001120
           02 MWPAGEP   PIC X.                                          00001130
           02 MWPAGEH   PIC X.                                          00001140
           02 MWPAGEV   PIC X.                                          00001150
           02 MWPAGEO   PIC X(3).                                       00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MWFONCA   PIC X.                                          00001180
           02 MWFONCC   PIC X.                                          00001190
           02 MWFONCP   PIC X.                                          00001200
           02 MWFONCH   PIC X.                                          00001210
           02 MWFONCV   PIC X.                                          00001220
           02 MWFONCO   PIC X(3).                                       00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MNCODICA  PIC X.                                          00001250
           02 MNCODICC  PIC X.                                          00001260
           02 MNCODICP  PIC X.                                          00001270
           02 MNCODICH  PIC X.                                          00001280
           02 MNCODICV  PIC X.                                          00001290
           02 MNCODICO  PIC X(7).                                       00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MLREFA    PIC X.                                          00001320
           02 MLREFC    PIC X.                                          00001330
           02 MLREFP    PIC X.                                          00001340
           02 MLREFH    PIC X.                                          00001350
           02 MLREFV    PIC X.                                          00001360
           02 MLREFO    PIC X(20).                                      00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MCFAMA    PIC X.                                          00001390
           02 MCFAMC    PIC X.                                          00001400
           02 MCFAMP    PIC X.                                          00001410
           02 MCFAMH    PIC X.                                          00001420
           02 MCFAMV    PIC X.                                          00001430
           02 MCFAMO    PIC X(5).                                       00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MLFAMA    PIC X.                                          00001460
           02 MLFAMC    PIC X.                                          00001470
           02 MLFAMP    PIC X.                                          00001480
           02 MLFAMH    PIC X.                                          00001490
           02 MLFAMV    PIC X.                                          00001500
           02 MLFAMO    PIC X(20).                                      00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MCMARQA   PIC X.                                          00001530
           02 MCMARQC   PIC X.                                          00001540
           02 MCMARQP   PIC X.                                          00001550
           02 MCMARQH   PIC X.                                          00001560
           02 MCMARQV   PIC X.                                          00001570
           02 MCMARQO   PIC X(5).                                       00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MLMARQA   PIC X.                                          00001600
           02 MLMARQC   PIC X.                                          00001610
           02 MLMARQP   PIC X.                                          00001620
           02 MLMARQH   PIC X.                                          00001630
           02 MLMARQV   PIC X.                                          00001640
           02 MLMARQO   PIC X(20).                                      00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MNUMZPA   PIC X.                                          00001670
           02 MNUMZPC   PIC X.                                          00001680
           02 MNUMZPP   PIC X.                                          00001690
           02 MNUMZPH   PIC X.                                          00001700
           02 MNUMZPV   PIC X.                                          00001710
           02 MNUMZPO   PIC X(2).                                       00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MLZPA     PIC X.                                          00001740
           02 MLZPC     PIC X.                                          00001750
           02 MLZPP     PIC X.                                          00001760
           02 MLZPH     PIC X.                                          00001770
           02 MLZPV     PIC X.                                          00001780
           02 MLZPO     PIC X(20).                                      00001790
           02 MPREXO OCCURS   12 TIMES .                                00001800
             03 FILLER       PIC X(2).                                  00001810
             03 MPRIXA  PIC X.                                          00001820
             03 MPRIXC  PIC X.                                          00001830
             03 MPRIXP  PIC X.                                          00001840
             03 MPRIXH  PIC X.                                          00001850
             03 MPRIXV  PIC X.                                          00001860
             03 MPRIXO  PIC X(9).                                       00001870
             03 FILLER       PIC X(2).                                  00001880
             03 MDATEA  PIC X.                                          00001890
             03 MDATEC  PIC X.                                          00001900
             03 MDATEP  PIC X.                                          00001910
             03 MDATEH  PIC X.                                          00001920
             03 MDATEV  PIC X.                                          00001930
             03 MDATEO  PIC X(8).                                       00001940
             03 FILLER       PIC X(2).                                  00001950
             03 MCOMMENA     PIC X.                                     00001960
             03 MCOMMENC     PIC X.                                     00001970
             03 MCOMMENP     PIC X.                                     00001980
             03 MCOMMENH     PIC X.                                     00001990
             03 MCOMMENV     PIC X.                                     00002000
             03 MCOMMENO     PIC X(10).                                 00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MZONCMDA  PIC X.                                          00002030
           02 MZONCMDC  PIC X.                                          00002040
           02 MZONCMDP  PIC X.                                          00002050
           02 MZONCMDH  PIC X.                                          00002060
           02 MZONCMDV  PIC X.                                          00002070
           02 MZONCMDO  PIC X(15).                                      00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MLIBERRA  PIC X.                                          00002100
           02 MLIBERRC  PIC X.                                          00002110
           02 MLIBERRP  PIC X.                                          00002120
           02 MLIBERRH  PIC X.                                          00002130
           02 MLIBERRV  PIC X.                                          00002140
           02 MLIBERRO  PIC X(58).                                      00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCODTRAA  PIC X.                                          00002170
           02 MCODTRAC  PIC X.                                          00002180
           02 MCODTRAP  PIC X.                                          00002190
           02 MCODTRAH  PIC X.                                          00002200
           02 MCODTRAV  PIC X.                                          00002210
           02 MCODTRAO  PIC X(4).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MCICSA    PIC X.                                          00002240
           02 MCICSC    PIC X.                                          00002250
           02 MCICSP    PIC X.                                          00002260
           02 MCICSH    PIC X.                                          00002270
           02 MCICSV    PIC X.                                          00002280
           02 MCICSO    PIC X(5).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MNETNAMA  PIC X.                                          00002310
           02 MNETNAMC  PIC X.                                          00002320
           02 MNETNAMP  PIC X.                                          00002330
           02 MNETNAMH  PIC X.                                          00002340
           02 MNETNAMV  PIC X.                                          00002350
           02 MNETNAMO  PIC X(8).                                       00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MSCREENA  PIC X.                                          00002380
           02 MSCREENC  PIC X.                                          00002390
           02 MSCREENP  PIC X.                                          00002400
           02 MSCREENH  PIC X.                                          00002410
           02 MSCREENV  PIC X.                                          00002420
           02 MSCREENO  PIC X(4).                                       00002430
                                                                                
