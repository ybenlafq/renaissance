      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      *        IDENTIFICATION SERVICE - CODES DESCRIPTIFS             * 00000020
      ***************************************************************** 00000030
      *                                                               * 00000040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS-52-LONG          PIC S9(4) COMP VALUE +210.               00000050
      *--                                                                       
       01  TS-52-LONG          PIC S9(4) COMP-5 VALUE +210.                     
      *}                                                                        
       01  TS-52-DATA.                                                  00000060
           05 TS-52-LIGNE.                                              00000070
              10 TS-POSTE-TSSE52    OCCURS 2.                           00000080
                 15 TS-52-DATA-AV.                                      00000090
                    20 TS-52-CCDESC-AV           PIC X(05).             00000100
                    20 TS-52-LCDESCC-AV          PIC X(20).             00000110
                    20 TS-52-CVDESC-AV           PIC X(05).             00000120
                    20 TS-52-LVDESC-AV           PIC X(20).             00000130
                 15 TS-52-DATA-AP.                                      00000140
                    20 TS-52-CCDESC-AP           PIC X(05).             00000150
                    20 TS-52-LCDESCC-AP          PIC X(20).             00000160
                    20 TS-52-CVDESC-AP           PIC X(05).             00000170
                    20 TS-52-LVDESC-AP           PIC X(20).             00000180
                 15 TS-52-FLAG-CTRL.                                    00000190
                    20 TS-52-CDRET-CVDESC        PIC X(04).             00000200
                 15 TS-52-FLAG-MAJ.                                     00000210
                    20 TS-52-WMAJTS              PIC X(01).             00000220
                                                                                
