      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************            
      * COMMAREA CHAINE GA50                                                    
      *     ZONE AIDA                                                           
      *     ZONE CICS                                                           
      *     ZONE DATE                                                           
      *     ZONE SWAP                                                           
      *     ZONE COMMUNE CHAINE GA50                                            
      *     ZONES RESERVEES APPLICATIVES                                        
      **************************************************************            
      * MODIF 09/08/02 DSA057 CORRECTION ANOMALIE TEST INTEGRATION              
      **************************************************************            
      * MODIF 09/08/02 DSA065 MISE A JOUR PAR RAPPORT A LA PROD                 
      * MODIF 03/10/02 DSA056 AJOUT NOUVEAU CHAMP DANS EGA51                    
      * MODIF 10/10/02 DSA056 CUSINE X(1) + NOUVEAU CUSINEVRAI X(5)             
      * MODIF 14/10/02 DSA057 TEST RECETTE ANO 329                              
      * MODIF 22/10/02 DSA065 GESTION PF3 POUR TGA52                            
      * MODIF 10/12/02 DSA057 REFONTE INTERFACE INT PRODUIT (TGA75)             
      ******************************************************************        
      * DSA057 01/07/03 SUPPORT EVOLUTION                                       
      *                 MISE EN COM CODLANG CODPIC CFONC                        
      ******************************************************************        
      * DSA057 13/08/03 SUPPORT MAINTENANCE                                     
      *                 GESTION QNBPRACK PAR GA50                               
      *                 ZONE RTGA00 (3) PLUS PETITE QUE RTFL50 (5)              
      *                 ZONE COMMAREA AJOUTEE POUR FL50                         
      *                 GESTION SEPAREE DE GA00 ET FL50                         
      ******************************************************************        
      * DSA057 25/08/03 SUPPORT EVOLUTION                                       
      *                 WEB EXTENDED RANGE                                      
      ******************************************************************        
      * DSA057 08/09/03 SUPPORT EVOLUTION                                       
      *                 GESTION EAN MIXTE                                       
      *                 PASSAGE MONO A MULTI COMPOSANT                          
      ******************************************************************        
      * DSA057 24/11/03 SUPPORT EVOLUTION                                       
      *                 GESTION DE LIEN BMAST/BESCL                             
      ******************************************************************        
      * DSA052 16/12/03 SUPPORT EVOLUTION                                       
      *                 ACTIVATION DES CODES BARRES SELON AUTORISATION          
      *                 UTILISATEUR                                             
      ******************************************************************        
      * DSA057 17/12/03 SUPPORT MAINTENANCE EVOLUTION 48                        
      *                 GESTION STATUT MULTI DEPOT SUR TGA53                    
      ******************************************************************        
      * DSA015 19/01/07 PROJET CONVERGENCE                                      
      *                 AJOUT D'UN FLAG D'ENVOI VERS SAP                        
      ******************************************************************        
      * DE02015 03/04/08 SUPPORT EVOLUTION                                      
      *                  INNOVENTE LOT 2                                        
      *                  PAS BESOIN DE RECOMPILER, FILLER UTILISE               
      ******************************************************************        
      * DE02005 03/05/04 SUPPORT MAINTENANCE EVOLUTION D004404                  
      *                  CHANGEMENT STATUT SUR SA                               
      *                  FILLER UTILISE                                         
      ******************************************************************        
      * DE02005 16/03/11 SUPPORT MAINTENANCE                                    
      *                  REFERENCE TECHNIQUE                                    
      *                  CORRECTION LONGUEUR TOTALE 4109 => 4096                
      *                  UTILISATION FILLER                                     
      ******************************************************************        
      * DE02001 29/11/11 SUPPORT MAINTENANCE                                    
      *                  AJOUT DE LA DONNEE QUI DEFINIT LE NOMBRE D'UN          
      *                  M�ME ARTICLE OU CODIC POUR REMPLIR UN CAMION           
      ******************************************************************        
      * DE02001 14/03/12 SUPPORT MAINTENANCE EVOLUTION M0016680                 
      *                  CONTROLE DU PARAMETRAGE FAMILLE SUR LA SAISIE          
      *                  DU CLIENT REQUIS                                       
      ******************************************************************        
      * DE02007 18/09/12 MAINTENANCE EVOLUTIVE :                                
      *                  BRIDAGE DE GA50 SUITE � MISE EN PLACE D'HYBRIS         
      ******************************************************************        
      * DE02034 17/03/14 MAINTENANCE EVOLUTIVE :                                
      *                  CHOIX D�P�T PRINCIPAL OU SECONDAIRE DANS OPT 5         
      ******************************************************************        
      * DE02004 03/12/14 PROJET MGD                                             
      *                  AJOUT NB DE COLIS                                      
      ******************************************************************        
      * DE02034 07/05/2015 CROSS DOCK                                           
      *                  AJOUT FLAG CROSS DOCK                                  
      ******************************************************************        
      * DE02034 05/10/2015 SEGMENT GAMME                                        
      *         SEGMENT GAMME MODIFIABLE SUR GA50                               
      ******************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 COM-GA50-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                    
      *--                                                                       
       01 COM-GA50-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                  
      *}                                                                        
       01 Z-COMMAREA.                                                           
      ******************************************************************        
      ***  ZONES RESERVEES A AIDA ---------------------------------- 100        
           02 COMM-AIDA.                                                        
               05 FILLER-COM-AIDA             PIC X(100).                       
      ******************************************************************        
      ***  ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 20        
           02 COMM-CICS.                                                        
               05 COMM-CICS-APPLID            PIC X(8).                         
               05 COMM-CICS-NETNAM            PIC X(8).                         
               05 COMM-CICS-TRANSA            PIC X(4).                         
      ******************************************************************        
      ***  ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ----- 100        
           02 COMM-DATE.                                                        
               05 COMM-DATE-SIECLE            PIC X(02).                        
               05 COMM-DATE-ANNEE             PIC X(02).                        
               05 COMM-DATE-MOIS              PIC X(02).                        
               05 COMM-DATE-JOUR              PIC X(02).                        
      *        QUANTIEMES CALENDAIRE ET STANDARD                                
               05 COMM-DATE-QNTA              PIC 9(03).                        
               05 COMM-DATE-QNT0              PIC 9(05).                        
      *        ANNEE BISSEXTILE 1=OUI 0=NON                                     
               05 COMM-DATE-BISX              PIC 9(01).                        
      *        JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                              
               05 COMM-DATE-JSM               PIC 9(01).                        
      *        LIBELLES DU JOUR COURT - LONG                                    
               05 COMM-DATE-JSM-LC            PIC X(03).                        
               05 COMM-DATE-JSM-LL            PIC X(08).                        
      *        LIBELLES DU MOIS COURT - LONG                                    
               05 COMM-DATE-MOIS-LC           PIC X(03).                        
               05 COMM-DATE-MOIS-LL           PIC X(08).                        
      *        DIFFERENTES FORMES DE DATE                                       
               05 COMM-DATE-SSAAMMJJ          PIC X(08).                        
               05 COMM-DATE-AAMMJJ            PIC X(06).                        
               05 COMM-DATE-JJMMSSAA          PIC X(08).                        
               05 COMM-DATE-JJMMAA            PIC X(06).                        
               05 COMM-DATE-JJ-MM-AA          PIC X(08).                        
               05 COMM-DATE-JJ-MM-SSAA        PIC X(10).                        
      *        DIFFERENTES FORMES DE DATE                                       
      *        05 COMM-DATE-FILLER            PIC X(14).                        
               05 COMM-DATE-JSUIVANT          PIC X(08).                        
               05 COMM-DATE-FILLER            PIC X(06).                        
      ******************************************************************        
      ***  ZONES RESERVEES TRAITEMENT DU SWAP ---------------------- 152        
           02 COMM-SWAP.                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                 
      *--                                                                       
               05 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.               
      *}                                                                        
               05 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                           
      ******************************************************************        
      ***  ZONES COMMUNE CHAINE GA50 ------------------------------- 818        
           02 COMM-GA50-APPLI.                                                  
      **       ZONE DONNEES COMMUNES ------------------------------- 525        
               03 COMM-GA50-DATA.                                               
      *            ZONE DONNEES ARTICLE ---------------------------- 216        
                   05 COMM-INFO-ARTICLE.                                        
                       10 COMM-GA00-NCODIC      PIC X(07).                      
                       10 COMM-GA00-LREFFOURN   PIC X(20).                      
                       10 COMM-GA00-CFAM        PIC X(05).                      
                       10 COMM-GA00-CMARQ       PIC X(05).                      
                       10 COMM-GA00-DCREATION   PIC X(08).                      
                       10 COMM-GA00-D1RECEPT    PIC X(08).                      
                       10 COMM-GA00-DEFSTATUT   PIC X(08).                      
                       10 COMM-GA00-DMAJ        PIC X(08).                      
                       10 COMM-GA00-LSTATCOMP   PIC X(03).                      
                       10 COMM-GA00-LREFO       PIC X(20).                      
                       10 COMM-GA00-NEAN        PIC X(13).                      
                       10 COMM-GA00-CCOLOR      PIC X(05).                      
      *                10 COMM-GA00-CUSINE      PIC X(05).                      
                       10 COMM-GA00-CUSINEVRAI  PIC X(05).                      
                       10 COMM-GA00-CORIGPROD   PIC X(05).                      
                       10 COMM-GA49-NBCOMPO     PIC S9(2) COMP-3.               
                       10 COMM-GA00-CASSORT     PIC X(05).                      
                       10 COMM-GA00-CAPPRO      PIC X(05).                      
                       10 COMM-GA00-CEXPO       PIC X(05).                      
                       10 COMM-GA00-LREFDARTY   PIC X(20).                      
                       10 COMM-GA00-LCOMMENT    PIC X(50).                      
                       10 COMM-GA00-WSENSVTE    PIC X(01).                      
                       10 COMM-GA00-WSENSAPPRO  PIC X(01).                      
                       10 COMM-GA00-DEPOT1.                                     
                           15 COMM-GA00-NSOCDEPOT1  PIC X(03).                  
                           15 COMM-GA00-NDEPOT1     PIC X(03).                  
                       10 COMM-GA00-MULTISOC    PIC X(01).                      
      *            INFO FAMILLE ------------------------------------- 52        
                   05 COMM-INFO-FAMILLE.                                        
                       10 COMM-GA14-CFAM        PIC X(05).                      
                       10 COMM-GA14-LFAM        PIC X(20).                      
                       10 COMM-GA14-CMODSTOCK   PIC X(05).                      
                       10 COMM-GA14-CTAUXTVA    PIC X(05).                      
                       10 COMM-GA14-CGARANTIE   PIC X(05).                      
                       10 COMM-GA14-QBORNESOL   PIC S9(2)V9(3) COMP-3.          
                       10 COMM-GA14-QBORNEVRAC  PIC S9(2)V9(3) COMP-3.          
                       10 COMM-GA14-QNBPVSOL    PIC S9(3) COMP-3.               
                       10 COMM-GA14-WMULTIFAM   PIC X(01).                      
                       10 COMM-GA14-WSEQFAM     PIC S9(5) COMP-3.               
      *            INFO VRAC --------------------------------------- 207        
                   05 COMM-INFO-VRAC.                                           
                       10 COMM-LMARQ         PIC    X(20).                      
                       10 COMM-LAPPRO        PIC    X(20).                      
                       10 COMM-LASSORT       PIC    X(20).                      
                       10 COMM-LEXPO         PIC    X(20).                      
                       10 COMM-LIAISON       PIC    X(5).                       
                       10 COMM-ZONEPV        PIC    X(2).                       
                       10 COMM-LCOLOR        PIC    X(20).                      
                       10 COMM-LUSINE        PIC    X(20).                      
                       10 COMM-LORIGPROD     PIC    X(20).                      
                       10 COMM-LIATRB3       PIC    X(30).                      
                       10 COMM-LIRPLCD       PIC    X(30).                      
      *            FILLER COMM-GA50-DATA 15% MIN 5 MAX 50------------ 50        
                   05 COMM-AUTRE.                                               
                       10 COMM-GA00-WRESFOURN PIC X.                            
      *            FILLER COMM-GA50-DATA 15% MIN 5 MAX 50------------ 50        
      *            FILLER COMM-GA50-DATA 15% MIN 5 MAX 50------------ 49        
      *            05 FILLER                 PIC    X(50).                      
      *            05 FILLER PIC X(49).                                         
      *            05 FILLER PIC X(48).                                         
                   05 COMM-SAP.                                                 
                      10 COMM-LREFFOURN-ORIG  PIC X(20).                        
                      10 COMM-CMARQ-ORIG      PIC X(05).                        
                      10 COMM-CORIGPROD-ORIG  PIC X(05).                        
                      10 COMM-QPOIDSDE-ORIG   PIC S9(07) COMP-3.                
                      10 COMM-CNOMDOU-ORIG    PIC X(08).                        
      *            05 FILLER PIC X(06).                                         
                   05 COMM-GA14-CTYPENT       PIC X(02).                        
                   05 COMM-GA00-CROSSDOCK     PIC X(01).                        
      *E0733       05 FILLER PIC X(04).                                         
                   05 FILLER PIC X(03).                                         
                   05 COMM-GA00-CUSINE       PIC X(1).                          
      **       ZONES DE GESTION TGA50 ------------------------------ 450        
               03 COMM-GA50-GESTION.                                            
      *            GESTION UTILISATEUR ------------------------------ 23        
                   05 COMM-GA50-ACCES.                                          
                       10 COMM-NSOCIETE        PIC    X(3).                     
      *                10 COMM-GA00-COPCO      PIC    X(03).                    
                       10 COMM-CSOC PIC X(3).                                   
      *                DROIT D'ACCES VALEURS POSSIBLES : P, S, D                
                       10 COMM-DROIT           PIC    X.                        
                       10 COMM-GARANT          PIC    X.                        
                       10 COMM-TSSACIDA        PIC    X(8).                     
                       10 COMM-CFONC           PIC    X(01).                    
                       10 COMM-CODE-CFONC      PIC    X(03).                    
      *                10 FILLER               PIC    X(03).                    
                       10 COMM-NLIEU           PIC X(3).                        
      *            PARAMETRE D'AIGUILLAGE -------------------------- 300        
                   05 COMM-INFO-PARAM.                                          
                       10 COMM-PARAM   OCCURS 30.                               
                           15 COMM-CTRANS PIC X(5).                             
                           15 COMM-NOPT   PIC 9(2).                             
                           15 COMM-NSOPT  PIC 9.                                
                           15 COMM-WOBLIG PIC X.                                
                           15 COMM-WPASS  PIC X.                                
      *            GESTION TERMINAL ---------------------------------- 4        
                   05 COMM-NTERMID PIC X(4).                                    
      *            GESTION PRODUIT ---------------------------------- 80        
                   05 COMM-GA50-GPROD.                                          
                       10 COMM-GA00-PROTEGE-FL50     PIC X(1).                  
                       10 COMM-GA00-PROTEGE-NATIONAL PIC X(1).                  
                       10 COMM-VALIDATION       PIC X(01).                      
                       10 COMM-PROTEGE          PIC X(01).                      
                       10 COMM-TOP-BLOCAGE      PIC X(01).                      
                       10 COMM-NCODIC           PIC X(07).                      
                       10 COMM-NCODICK-ORIG     PIC X(07).                      
                       10 COMM-CFAM             PIC X(05).                      
                       10 COMM-CFAM-ORIG        PIC X(05).                      
                       10 COMM-CASSORT-ORIG     PIC X(05).                      
                       10 COMM-CAPPRO-ORIG      PIC X(05).                      
                       10 COMM-CEXPO-ORIG       PIC X(05).                      
                       10 COMM-CTYPCONDT-ORIG   PIC X(05).                      
                       10 COMM-QCONDT-ORIG      PIC S9(5) COMP-3.               
                       10 COMM-WMULTIFAM-ORIG   PIC X(01).                      
                       10 COMM-CPROFIL          PIC X(10).                      
                       10 COMM-CPROFIL-ORIG     PIC X(10).                      
                       10 COMM-DDMAJ-PROF       PIC X(08).                      
                       10 COMM-WSENSVTE-ORIG    PIC X(01).                      
                       10 COMM-WSENSAPPRO-ORIG  PIC X(01).                      
                       10 COMM-WTLMELA-ORIG     PIC X(01).                      
                       10 COMM-WABC             PIC X(01).                      
                       10 COMM-RETOUR-KESA      PIC X(01).                      
                       10 COMM-MULTISOC-R       PIC X(01).                      
                       10 COMM-WMS              PIC X(01).                      
      *            FILLER COMM-GA50-GESTION 15% MIN 5 MAX 50--------- 42        
      *            GESTION SYSTEME INTEGRE OPCO                                 
                   05 COMM-GA50-ICS.                                            
                       10 COMM-ICS PIC X.                                       
                       10 COMM-ICS-ENVOI PIC X.                                 
      *            05 FILLER PIC X(39).                                         
      *            GESTION INTERNATIONAL                                        
                   05 COMM-GA50-INTER.                                          
                       10 COMM-CODLANG PIC X(2).                                
                       10 COMM-CODPIC  PIC X(2).                                
                       10 COMM-CFONCTIONS.                                      
                           15 COMM-CFONCTION  OCCURS 4.                         
                               20 COMM-FONC01 PIC X(3).                         
                               20 FILLER      PIC X.                            
      *            05 FILLER PIC X(19).                                         
                   05 COMM-LOG-NSOC PIC X(3).                                   
      *            05 FILLER PIC X(16).                                         
                   05 COMM-SAP-ENVOI PIC X(01).                                 
      *            FLAG 'CODIFICATION GEREE PAR HYBRIS' : O/N                   
                   05 COMM-FLAG-ACTIF      PIC X(1).                            
                   05 FILLER PIC X(14).                                         
      ******************************************************************        
           02 COMM-ECRAN-APPLI.                                                 
      ******************************************************************        
      ***      TGA74 DONNEES SECTEURS ------------------------------ 146        
      ***      TGA74 DONNEES SECTEURS ------------------------------ 145        
               03 COMM-EGA74-APPLI.                                             
                   05 COMM-EGA74-DATA.                                          
      *                DONNEES ------------------------------------- 112        
                       10 COMM-GA00-NCODICK     PIC X(07).                      
                       10 COMM-GA00-NPROD       PIC X(15).                      
                       10 COMM-GA33-CDESTPROD   PIC X(05).                      
                       10 COMM-LDESTPROD        PIC X(20).                      
                       10 COMM-GA00-CLIGPROD    PIC X(05).                      
                       10 COMM-LLIGPROD         PIC X(20).                      
                       10 COMM-GA00-LMODBASE    PIC X(20).                      
                       10 COMM-GA00-VERSION     PIC X(20).                      
      *                FILLER COMM-GA74-DATA 15% MIN 5 MAX 50-------- 20        
                       10 COMM-GA74-SKUK PIC X(7).                              
                       10 COMM-GA74-SKU  PIC X(7).                              
      *                10 FILLER                PIC X(06).                      
                       10 COMM-GA00-COPCO PIC X(3).                             
                       10 FILLER                PIC X(03).                      
                   05 COMM-EGA74-GESTION.                                       
      *                GESTION ---------------------------------------11        
                       10 COMM-GA74-PROGSUI     PIC X(5).                       
                       10 COMM-GA74-RECHERCHE      PIC 9.                       
                           88 COMM-GA74-NULLE      VALUE 0.                     
                           88 COMM-GA74-CODICK     VALUE 1.                     
                           88 COMM-GA74-SECTEUR    VALUE 2.                     
                           88 COMM-GA74-COPIE      VALUE 3.                     
                       10 COMM-GA74-TRAITEMENT     PIC 9.                       
                           88 COMM-GA74-AUCUN      VALUE 0.                     
                           88 COMM-GA74-INTER      VALUE 1.                     
                           88 COMM-GA74-SUITE      VALUE 2.                     
                           88 COMM-GA74-VALIDE     VALUE 3.                     
                           88 COMM-GA74-DESTIN     VALUE 4.                     
      * -- GESTION TSGA74                                                       
                       10 COMM-GA74-PAGE        PIC 9.                          
                       10 COMM-GA74-ITEM-MAX    PIC 9(2) COMP-3.                
                       10 COMM-GA74-ITEM-SUP    PIC 9(2) COMP-3.                
      *                FILLER COMM-GA74-GESTION 15% MIN 5 MAX 50------ 3        
      *                FILLER COMM-GA74-GESTION 15% MIN 5 MAX 50------ 2        
      *                INDIQUE SI ON A PRECEDENT FAIT UNE COPIE DE CODIC        
                       10 COMM-GA74-INFO PIC 9 VALUE 0.                         
                            88 COMM-GA74-JETTE VALUE 0.                         
                            88 COMM-GA74-GARDE VALUE 1.                         
      *                10 FILLER                PIC X(03).                      
      *                10 FILLER                PIC X(02).                      
                       10 FILLER                PIC X.                          
      ******************************************************************        
      ***      TGA75 ARTICLES SIMILAIRES ---------------------------- 73        
      ***      TGA75 ARTICLES SIMILAIRES ---------------------------- 74        
               03 COMM-EGA75-APPLI.                                             
                   05 COMM-EGA75-DATA.                                          
      *                LONG 56                                                  
      *                DONNEES -------------------------------------- 51        
                       10 COMM-GA75-CMARQ PIC X(5).                             
                       10 COMM-GA75-CCOLOR      PIC X(05).                      
                       10 COMM-GA75-CFAM        PIC X(05).                      
                       10 COMM-GA75-LFAM        PIC X(20).                      
                       10 COMM-GA75-LREFO       PIC X(20).                      
                       10 COMM-GA75-RREF PIC 9 VALUE 0.                         
                           88 COMM-GA75-SREF VALUE 0.                           
                           88 COMM-GA75-GREF VALUE 1.                           
                       10 COMM-GA75-RFAM PIC 9 VALUE 0.                         
                           88 COMM-GA75-SFAM VALUE 0.                           
                           88 COMM-GA75-GFAM VALUE 1.                           
      *                FILLER COMM-GA75-DATA 15% MIN 5 MAX 50--------- 7        
      *                10 FILLER                PIC X(07).                      
                   05 COMM-EGA75-GESTION.                                       
      *                LONG 17                                                  
      *                GESTION -------------------------------------- 11        
      *                10 COMM-GA75-NBPRODUCT   PIC S9(6) COMP-3.               
                       10 COMM-GA75-NBPRODUCT PIC S9(7) COMP-3.                 
                       10 COMM-GA75-COUNTPROD PIC S9(7) COMP-3.                 
                       10 COMM-GA75-POSPROD PIC S9(7) COMP-3.                   
                       10 COMM-GA75-NUMPAG      PIC 9(03).                      
      *                10 COMM-GA75-MAX-PAGE1   PIC S9(03) COMP-3.              
                       10 COMM-GA75-IND-TS      PIC S9(03) COMP-3.              
      *                FILLER COMM-GA75-GESTION 15% MIN 5 MAX 50------ 5        
      *                10 FILLER                PIC X(05).                      
      ******************************************************************        
      ***      TGA51 DONNEES GENERALES I ---------------------------- 39        
               03 COMM-EGA51-APPLI.                                             
                   05 COMM-EGA51-DATA.                                          
      *                DONNEES -------------------------------------- 20        
                       10 COMM-GA00-CGESTVTE    PIC X(03).                      
                       10 COMM-GA00-WDACEM      PIC X(01).                      
                       10 COMM-GA00-CTAUXTVA    PIC X(05).                      
                       10 COMM-GA00-WTLMELA     PIC X(01).                      
                       10 COMM-GA00-CGARANTIE   PIC X(05).                      
                       10 COMM-GA00-CGARCONST   PIC X(05).                      
      *                FILLER COMM-GA51-DATA 15% MIN 5 MAX 50--------- 5        
                   05 COMM-GA49-CCOLORM     PIC X(05).                          
                   05 COMM-EGA51-GESTION.                                       
      *                GESTION --------------------------------------- 9        
                       10 COMM-GA51-EXISTS      PIC X(01).                      
                       10 COMM-GA52-EXISTS      PIC X(01).                      
                       10 COMM-GA51-NCODIC      PIC X(07).                      
      *                FILLER COMM-GA51-GESTION 15% MIN 5 MAX 50------ 5        
                       10 FILLER                PIC X(05).                      
      ******************************************************************        
      ***      TGA76 DONNEES GENERALES II -------------------------- 285        
               03 COMM-EGA76-APPLI.                                             
                   05 COMM-EGA76-DATA.                                          
      *                DONNEES ------------------------------------- 235        
                       10 COMM-GA49-IDEPT       PIC X(03).                      
                       10 COMM-GA49-ILDEPT      PIC X(20).                      
                       10 COMM-GA49-ISDEPT      PIC X(03).                      
                       10 COMM-GA49-ILSDEPT     PIC X(20).                      
                       10 COMM-GA49-ICLASS      PIC X(03).                      
                       10 COMM-GA49-ILCLASS     PIC X(20).                      
                       10 COMM-GA49-ISCLASS     PIC X(03).                      
                       10 COMM-GA49-ILSCLASS    PIC X(20).                      
                       10 COMM-GA49-IATRB4      PIC X(02).                      
                       10 COMM-GA49-IATRB2      PIC X(02).                      
                       10 COMM-GA49-ISTYPE      PIC X(02).                      
                       10 COMM-GA49-IWARGC      PIC X(02).                      
                       10 COMM-GA49-I2TKTD      PIC X(60).                      
                       10 COMM-GA49-I2ADVD      PIC X(60).                      
                       10 COMM-GA49-I3MKDS      PIC X(15).                      
      *                FILLER COMM-GA51-DATA 15% MIN 5 MAX 50-------- 35        
      *                10 FILLER                PIC X(35).                      
                       10 COMM-GA00-LDISTM      PIC X(20).                      
      *                10 COMM-FLAG-GA31        PIC X.                          
      *                10 FILLER                PIC X(14).                      
                       10 COMM-GA76-NBCOMPO     PIC 9(2).                       
                       10 COMM-GA76-COMPOSE     PIC 9.                          
                       10 COMM-GA76-INFO        PIC 9.                          
                       10 FILLER                PIC X(11).                      
                   05 COMM-EGA76-GESTION.                                       
      *                GESTION -------------------------------------- 10        
      *                10 COMM-GA76-NUMPAG      PIC 9(03).                      
      *                10 COMM-GA76-NBRPAG      PIC 9(03).                      
      *                10 COMM-GA76-ITEM-MAX    PIC S9(2) COMP-3.               
      *                10 COMM-GA76-CRE         PIC X(1).                       
      *                10 COMM-GA76-TOP         PIC 9(1).                       
                       10 COMM-GA76-PAGE PIC 9(03).                             
      *                FILLER COMM-GA51-GESTION 15% MIN 5 MAX 50------ 5        
      *                10 FILLER                PIC X(05).                      
                       10 FILLER PIC X(12).                                     
      ******************************************************************        
      ***      TGA52 CODES DESCRIPTIFS ------------------------------ 24        
               03 COMM-EGA52-APPLI.                                             
                   05 COMM-EGA52-DATA.                                          
      *                DONNEES --------------------------------------- 0        
      *                FILLER COMM-GA52-DATA 15% MIN 5 MAX 50--------- 5        
      *                10 FILLER                PIC X(05).                      
                       10 COMM-GA52-STATUS PIC 9 VALUE 0.                       
                           88 COMM-GA52-TRAD VALUE 0.                           
                           88 COMM-GA52-NTRAD VALUE 1.                          
                       10 FILLER PIC X(4).                                      
                   05 COMM-EGA52-GESTION.                                       
      *                GESTION -------------------------------------- 14        
                       10 COMM-GA52-NUMPAG      PIC 9(03).                      
                       10 COMM-GA52-NBRPAG      PIC 9(03).                      
                       10 COMM-GA52-CODE-NON-RENSEIGNE  PIC 9(04).              
                       10 COMM-GA52-NBR-TOTAL-CODE      PIC 9(04).              
                       10 COMM-GA52-PGMPRC              PIC X(05).              
      *                FILLER COMM-GA52-GESTION 15% MIN 5 MAX 50------ 5        
      ***              10 FILLER                PIC X(05).                      
      ******************************************************************        
      *        TGA53 STATUTS ARTICLE DARTY ------------------------- 271        
               03 COMM-EGA53-APPLI.                                             
      *            DONNEES ----------------------------------------- 230        
                   05 COMM-EGA53-DATA.                                          
      *                10 FILLER PIC X(92).                                     
      *                10 COMM-GA00-DEPOT2.                                     
      *                    15 COMM-GA00-NSOC2       PIC X(03).                  
      *                    15 COMM-GA00-NDEP2       PIC X(03).                  
      *                10 COMM-GA00-DEPOT3.                                     
      *                    15 COMM-GA00-NSOC3       PIC X(03).                  
      *                    15 COMM-GA00-NDEP3       PIC X(03).                  
      *                10 COMM-GA00-WSENSAPPRO2 PIC X(01).                      
      *                10 COMM-GA00-WSENSAPPRO3 PIC X(01).                      
      *                10 COMM-WSENSAPPRO-ORIG2 PIC X(01).                      
      *                10 COMM-WSENSAPPRO-ORIG3 PIC X(01).                      
      *                10 COMM-GA00-CAPPRO2     PIC X(05).                      
      *                10 COMM-GA00-CAP2-ORIG   PIC X(05).                      
      *                10 COMM-GA00-CAPPRO3     PIC X(05).                      
      *                10 COMM-GA00-CAP3-ORIG   PIC X(05).                      
      *                10 COMM-GA00-LAPPRO2     PIC X(20).                      
      *                10 COMM-GA00-LAPPRO3     PIC X(20).                      
      *                10 COMM-GA00-CEXPO2      PIC X(05).                      
      *                10 COMM-GA00-CEXPO3      PIC X(05).                      
      *                10 COMM-GA00-LEXPO2      PIC X(20).                      
      *                10 COMM-GA00-LEXPO3      PIC X(20).                      
      *                10 COMM-GA00-LSTATCOMP2  PIC X(03).                      
      *                10 COMM-GA00-LSTATCOMP3  PIC X(03).                      
      *                10 COMM-GA00-DEPSUP3     PIC X(01).                      
      *                10 COMM-GA53-CASSORT     PIC X(05).                      
      *                10 FILLER                PIC X(230).                     
                       10 FILLER                PIC X(127).                     
      *                REFERENCE TECHNIQUE                                      
                       10 COMM-REFT.                                            
                          15 COMM-REFT-EAN         PIC X(13).                   
                          15 COMM-REFT-INDEX       PIC X(02).                   
                          15 COMM-REFT-DEFFET      PIC X(08).                   
                          15 COMM-REFT-LIB         PIC X(40).                   
                          15 COMM-REFT-INI         PIC X(40).                   
      *                GESTION LOGISTIQUE GA50 ----------------------- 9        
                       10 COMM-NBSOC            PIC S9(3) COMP-3.               
                       10 COMM-NBDEP-SOC        PIC S9(3) COMP-3.               
                       10 COMM-NBDEP-CFAM       PIC S9(3) COMP-3.               
                       10 COMM-DEPOT1           PIC S9(3) COMP-3.               
                       10 COMM-LOG-NAT          PIC X.                          
      *                FILLER COMM-GA53-DATA 15% MIN 5 MAX 50 ------- 20        
      *                10 FILLER                PIC X(20).                      
                       10 COMM-NBDEP-TS         PIC S9(3) COMP-3.               
                       10 COMM-GA00-COLLECTION  PIC X(05).                      
                       10 COMM-BRID-COLLECTION  PIC X.                          
      *E0742           10 FILLER                PIC X(18).                      
                       10 FILLER                PIC X(12).                      
      *            GESTION ------------------------------------------- 7        
                   05 COMM-EGA53-GESTION.                                       
      *                10 COMM-GA53-NUMPAG      PIC 9(03).                      
      *                10 COMM-GA53-NBRPAG      PIC 9(03).                      
                       10 COMM-GA53-PAGE        PIC 9(2).                       
                       10 COMM-GA53-PAGEMAX     PIC 9(2).                       
      *                10 COMM-GA53-EXPO-PARIS  PIC X(01).                      
      *                BINAIRE PEUT STOCKER 32 ETATS MAXIMUM->4 OCTETS          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                10 COMM-GA53-ALERTE      PIC S9(5) COMP VALUE 0.         
      *--                                                                       
                       10 COMM-GA53-ALERTE      PIC S9(5) COMP-5 VALUE          
                                                                      0.        
      *}                                                                        
                       10 FILLER                PIC X(4).                       
      *                10 FILLER                PIC X(3).                       
      *                FILLER COMM-GA53-GESTION 15% MIN 5 MAX 50------ 5        
      *                10 FILLER                PIC X(05).                      
      ******************************************************************        
      ***      TGA77 STATUTS ARTICLE COMET -------------------------- 59        
               03 COMM-EGA77-APPLI.                                             
                   05 COMM-EGA77-DATA.                                          
      *                DONNEES -------------------------------------- 46        
                       10 COMM-GA49-IRPLCD        PIC X(01).                    
                       10 COMM-GA49-IATRB3        PIC X(02).                    
                       10 COMM-GA49-I3WKMX        PIC S9(02) COMP-3.            
                       10 COMM-GA77-EXPO-COMET    PIC X(01).                    
                       10 COMM-GA77-TAB1          OCCURS 4.                     
                           15 COMM-GA77-TAB2       OCCURS 10.                   
                               20 COMM-GA48-SPPRTP  PIC X(01).                  
      *                FILLER COMM-GA77 DATA 15% MIN 5 MAX 50--------- 8        
      *                10 FILLER                  PIC X(08).                    
                       10 COMM-GA49-WWER          PIC X(01).                    
                       10 FILLER                  PIC X(07).                    
                   05 COMM-EGA77-GESTION.                                       
      *                GESTION --------------------------------------- 0        
      *                FILLER COMM-GA77-GESTION 15% MIN 5 MAX 50------ 5        
                       10 FILLER                  PIC X(05).                    
      ******************************************************************        
      ***      TGA55 DONNEES APPRO ---------------------------------- 41        
               03 COMM-EGA55-APPLI.                                             
                   05 COMM-EGA55-DATA.                                          
      *                DONNEES -------------------------------------- 31        
                       10 COMM-GA00-CHEFPROD      PIC X(05).                    
                       10 COMM-GA00-LCHEFPROD     PIC X(20).                    
                       10 COMM-GA00-QDELAIAPPRO   PIC X(03).                    
                       10 COMM-GA00-QCOLICDE      PIC S9(3) COMP-3.             
                       10 COMM-GA00-WSTOCKAVANCE  PIC X(01).                    
      *                FILLER COMM-GA55-DATA 15% MIN 5 MAX 50--------- 5        
                       10 COMM-DOUANE             PIC X.                        
                       10 COMM-GA00-CNOMDOU       PIC X(08).                    
                   05 COMM-EGA55-GESTION.                                       
      *                GESTION --------------------------------------- 0        
      *                FILLER COMM-GA55-GESTION 15% MIN 5 MAX 50------ 5        
      *                10 FILLER PIC X.                                         
                       10 COMM-GA55-CHEFP-FLG PIC 9 VALUE 0.                    
                           88 COMM-GA55-CHEFP-IDM VALUE 0.                      
                           88 COMM-GA55-CHEFP-CHG VALUE 1.                      
      ******************************************************************        
      ***      TGA62 ENTITES DE COMMANDE ---------------------------- 11        
               03 COMM-EGA62-APPLI.                                             
                   05 COMM-EGA62-DATA.                                          
      *                DONNEES --------------------------------------- 0        
      *                FILLER COMM-GA62-DATA 15% MIN 5 MAX 50--------- 5        
      *                10 FILLER                PIC X(05).                      
                       10 COMM-GA62-EAN-EDI     PIC X(01).                      
                       10 FILLER                PIC X(04).                      
                   05 COMM-EGA62-GESTION.                                       
      *                GESTION --------------------------------------- 1        
                       10 COMM-GA62-EXISTS      PIC X(01).                      
      *                FILLER COMM-GA62-GESTION 15% MIN 5 MAX 50------ 5        
      *                10 FILLER                PIC X(05).                      
                       10 COMM-GA62-ENTCD-FLG PIC 9 VALUE 0.                    
                           88 COMM-GA62-ENTCD-IDM VALUE 0.                      
                           88 COMM-GA62-ENTCD-SUP VALUE 1.                      
                       10 FILLER PIC X(4).                                      
      ******************************************************************        
      ***      TGA56 DONNEES ENTREPOT I ----------------------------- 99        
               03 COMM-EGA56-APPLI.                                             
                   05 COMM-EGA56-DATA.                                          
      *                DONNEES -------------------------------------- 48        
                       10 COMM-GA00-CCOTEHOLD   PIC X(01).                      
                       10 COMM-GA00-CMODSTOCK1  PIC X(05).                      
                       10 COMM-GA00-WMODSTOCK1  PIC X(01).                      
                       10 COMM-GA00-CMODSTOCK2  PIC X(05).                      
                       10 COMM-GA00-WMODSTOCK2  PIC X(01).                      
                       10 COMM-GA00-CMODSTOCK3  PIC X(05).                      
                       10 COMM-GA00-WMODSTOCK3  PIC X(01).                      
                       10 COMM-GA00-CZONEACCES  PIC X(01).                      
                       10 COMM-GA00-QNBPRACK    PIC S9(03) COMP-3.              
                       10 COMM-GA00-QNBPVSOL    PIC S9(03) COMP-3.              
                       10 COMM-GA00-CCONTENEUR  PIC X(01).                      
                       10 COMM-GA00-QNBRANMAIL  PIC S9(03) COMP-3.              
                       10 COMM-GA00-CSPECIFSTK  PIC X(01).                      
                       10 COMM-GA00-QNBNIVGERB  PIC S9(03) COMP-3.              
                       10 COMM-GA00-WCROSSDOCK  PIC X(01).                      
                       10 COMM-FL50-CUNITRECEPT PIC X(03).                      
                       10 COMM-FL40-CUNITVTE    PIC X(03).                      
                       10 COMM-GA00-CQUOTA      PIC X(05).                      
                       10 COMM-GA00-SYSMEC      PIC X(01).                      
                       10 COMM-GA00-CLASSE      PIC X(01).                      
      *                FILLER COMM-GA56-DATA 15% MIN 5 MAX 50--------- 4        
      *                10 FILLER                PIC X(04).                      
                       10 COMM-FL50-QNBPRACK    PIC S9(5) COMP-3.               
                       10 FILLER                PIC X(01).                      
                   05 COMM-EGA56-GESTION.                                       
      *                GESTION -------------------------------------- 41        
      *E0673           10 COMM-GA56-LTYPCONDT   PIC X(20).                      
                       10 COMM-DEPOT-SEC        PIC S9(3) COMP-3.               
                       10 COMM-DEPOT-S-SEC      PIC S9(3) COMP-3.               
                       10 COMM-DEPOT-S-1        PIC S9(3) COMP-3.               
                       10 COMM-GA00-DEPOT-SEC.                                  
                           15 COMM-GA00-NSOCDEPOT-S  PIC X(03).                 
                           15 COMM-GA00-NDEPOT-S     PIC X(03).                 
                       10 COMM-GA56-DEP-COURANT PIC X(01).                      
                       10 FILLER                PIC X(07).                      
                       10 COMM-GA56-LQUOTA      PIC X(20).                      
                       10 COMM-GA56-WABC        PIC X(01).                      
                       10 COMM-GA56-PROT-QNBPRC PIC X(01).                      
                         88 COMM-GA56-PROT-QNBPRC-O VALUE 'O'.                  
                         88 COMM-GA56-PROT-QNBPRC-N VALUE 'N'.                  
                       10 COMM-GA56-XCTRL-QNBPRC PIC X(01).                     
      *                FILLER COMM-GA56-GESTION 15% MIN 5 MAX 50------ 6        
                       10 FILLER                PIC X(04).                      
      *                10 FILLER                PIC X(06).                      
      ******************************************************************        
      ***      TGA78 DONNEES ENTREPOT II --------------------------- 140        
               03 COMM-EGA78-APPLI.                                             
                   05 COMM-EGA78-DATA.                                          
      *                DONNEES ------------------------------------- 100        
                       10 COMM-GA00-LEMBALLAGE     PIC X(50).                   
                       10 COMM-GA00-CTYPCONDT      PIC X(05).                   
                       10 COMM-GA00-QCONDT         PIC S9(05) COMP-3.           
                       10 COMM-GA00-QCOLIRECEPT    PIC S9(05) COMP-3.           
                       10 COMM-GA00-QCOLIDSTOCK    PIC S9(05) COMP-3.           
                       10 COMM-GA00-CFETEMPL       PIC X(01).                   
                       10 COMM-GA00-QCOLIVTE       PIC S9(05) COMP-3.           
                       10 COMM-GA00-QPOIDS         PIC S9(07) COMP-3.           
                       10 COMM-GA00-QLARGEUR       PIC S9(03) COMP-3.           
                       10 COMM-GA00-QPROFONDEUR    PIC S9(03) COMP-3.           
                       10 COMM-GA00-QHAUTEUR       PIC S9(03) COMP-3.           
                       10 COMM-GA00-QPOIDSDE       PIC S9(07) COMP-3.           
                       10 COMM-GA00-QLARGEURDE     PIC S9(03) COMP-3.           
                       10 COMM-GA00-QPROFONDEURDE  PIC S9(03) COMP-3.           
                       10 COMM-GA00-QHAUTEURDE     PIC S9(03) COMP-3.           
                       10 COMM-FL50-QCOUCHPAL      PIC S9(03) COMP-3.           
                       10 COMM-FL50-QCARTCOUCH     PIC S9(03) COMP-3.           
                       10 COMM-FL50-QBOXCART       PIC S9(07) COMP-3.           
                       10 COMM-FL50-QPRODBOX       PIC S9(07) COMP-3.           
                       10 COMM-GA00-QLARGDE        PIC S9(04)V9 COMP-3.         
                       10 COMM-GA00-QPROFDE        PIC S9(04)V9 COMP-3.         
                       10 COMM-GA00-QHAUTDE        PIC S9(04)V9 COMP-3.         
                       10 COMM-FL50-QPRODCAM       PIC S9(05) COMP-3.           
                       10 COMM-FL50-QNBCOL         PIC S9(05) COMP-3.           
      *                FILLER COMM-GA78-DATA 15% MIN 5 MAX 50-------- 15        
      *                10 FILLER                   PIC X(06).                   
      *                10 FILLER                   PIC X(03).                   
                   05 COMM-EGA78-GESTION.                                       
      *                GESTION -------------------------------------- 20        
                       10 COMM-GA78-LTYPCONDT      PIC X(20).                   
      *                FILLER COMM-GA78-GESTION 15% MIN 5 MAX 50------ 5        
                       10 FILLER                   PIC X(05).                   
      ******************************************************************        
      ***      TGA57-58 DONNEES LIENS ENTRE ARTICLE ----------------- 77        
               03 COMM-EGA57-58-APPLI.                                          
                   05 COMM-EGA57-58-DATA.                                       
      *                DONNEES -------------------------------------- 60        
                       10 COMM-GA57-CODICLIE       PIC X(07).                   
                       10 COMM-GA57-WDEGRELIB      PIC X(02).                   
                       10 COMM-GA57-LIENLIE        PIC X(05).                   
                       10 COMM-GA57-CTYPLIEN       PIC X(05).                   
                       10 COMM-GA57-LTYPLIEN       PIC X(20).                   
                       10 COMM-GA57-LZONEPV        PIC X(20).                   
                       10 COMM-GA57-GROUPE         PIC X.                       
                           88 COMM-GA57-DEJA-RECU  VALUE '1'.                   
                           88 COMM-GA57-NON-RECU   VALUE '0'.                   
      *                FILLER COMM-GA57-DATA 15% MIN 5 MAX 50--------- 9        
                       10  FILLER                  PIC X(09).                   
                   05 COMM-EGA57-58-GESTION.                                    
      *                GESTION --------------------------------------- 3        
                       10 COMM-GA57-LAST-TS        PIC 9(03).                   
      *                FILLER COMM-GA57-GESTION 15% MIN 5 MAX 50------ 5        
                       10  FILLER                  PIC X(05).                   
      ******************************************************************        
      ***      TGA59 PRIX DE VENTE DARTY ---------------------------- 19        
               03 COMM-EGA59-APPLI.                                             
                   05 COMM-EGA59-DATA.                                          
      *                DONNEES --------------------------------------- 9        
                       10 COMM-GA00-PRAR        PIC S9(7)V9(2) COMP-3.          
                       10 COMM-GA00-PBF         PIC S9(7)V9(2) COMP-3.          
      *                FILLER COMM-GA59-DATA 15% MIN 5 MAX 50--------- 1        
      *                10 FILLER                PIC X(01).                      
                   05 COMM-EGA59-GESTION.                                       
      *                GESTION --------------------------------------- 4        
                       10 COMM-GA59-ITEM        PIC 99.                         
                       10 COMM-GA59-ITEM-MAX    PIC 99.                         
      *                FILLER COMM-GA59-GESTION 15% MIN 5 MAX 50------ 5        
                       10 FILLER                PIC X(05).                      
      ******************************************************************        
      ***      TGA79 PRIX DE VENTE COMET A DEVELOPPER --------------- 30        
               03 COMM-EGA79-APPLI.                                             
                   05 COMM-EGA79-DATA.                                          
      *                DONNEES --------------------------------------- 0        
      *                FILLER COMM-GA59-DATA 15% MIN 5 MAX 50-------- 15        
                       10 FILLER                PIC X(15).                      
                   05 COMM-EGA79-GESTION.                                       
      *                GESTION --------------------------------------- 0        
      *                FILLER COMM-GA59-GESTION 15% MIN 5 MAX 50----- 15        
                       10 FILLER                PIC X(15).                      
      ******************************************************************        
      ***      TGA60 MISE EN SERVICE -------------------------------- 13        
               03 COMM-EGA60-APPLI.                                             
                   05 COMM-EGA60-DATA.                                          
      *                DONNEES --------------------------------------- 0        
      *                FILLER COMM-GA60-DATA 15% MIN 5 MAX 50--------- 5        
                       10 FILLER                PIC X(05).                      
                   05 COMM-EGA60-GESTION.                                       
      *                GESTION --------------------------------------- 0        
      *                FILLER COMM-GA60-GESTION 15% MIN 5 MAX 50------ 8        
                       10 FILLER                PIC X(08).                      
      ******************************************************************        
      ***      TGA61 DONNEES ETIQUETTES INFORMATIVES ---------------- 51        
               03 COMM-EGA61-APPLI.                                             
                   05 COMM-EGA61-DATA.                                          
      *                DONNEES -------------------------------------- 39        
                       10 COMM-GA61-FAMCLT         PIC X(30).                   
                       10 COMM-GA61-DMAJ           PIC X(08).                   
                       10 COMM-GA61-WPROFIL-COUL   PIC X.                       
                           88 COMM-GA61-PROFIL-COULEUR   VALUE 'O'.             
      *                FILLER COMM-GA61-DATA 15% MIN 5 MAX 50--------- 5        
                       10 FILLER                   PIC X(05).                   
                   05 COMM-EGA61-GESTION.                                       
      *                GESTION --------------------------------------- 2        
                       10 COMM-GA61-NUMPAG         PIC 9(02).                   
      *                FILLER COMM-GA61-GESTION 15% MIN 5 MAX 50------ 5        
                       10 FILLER                   PIC X(05).                   
      ******************************************************************        
      ***      TGA95 DONNEES ETIQUETTES INFORMATIVES COMET --------- 303        
               03 COMM-EGA95-APPLI.                                             
                   05 COMM-EGA95-DATA.                                          
      *                DONNEES ------------------------------------- 251        
                       10 COMM-GA34-TKTID          PIC X(25).                   
                       10 COMM-GA34-TKTQTY         PIC 9(03).                   
                       10 COMM-GA34-RECICN         PIC X(20).                   
                       10 COMM-GA34-INVFLG         PIC X(01).                   
                       10 COMM-GA34-FRCFLG         PIC X(01).                   
                       10 COMM-GA34-STKTKT         PIC X(01).                   
                       10 COMM-GA34-RSNTXT         PIC X(200).                  
      *                FILLER COMM-GA95-DATA 15% MIN 5 MAX 50-------- 37        
                       10 FILLER                   PIC X(37).                   
                   05 COMM-EGA95-GESTION.                                       
      *                GESTION --------------------------------------- 0        
      *                FILLER COMM-GA95-GESTION 15% MIN 5 MAX 50----- 15        
                       10 FILLER                   PIC X(15).                   
      ******************************************************************        
      ***      TGA96 DONNEES ETIQUETTES INFORMATIVES FEATURE COMET -- 30        
               03 COMM-EGA96-APPLI.                                             
                   05 COMM-EGA96-DATA.                                          
      *                DONNEES --------------------------------------- 0        
      *                FILLER COMM-GA96-DATA 15% MIN 5 MAX 50-------- 15        
                       10 FILLER                PIC X(15).                      
                   05 COMM-EGA96-GESTION.                                       
      *                GESTION --------------------------------------- 0        
      *                FILLER COMM-GA96-GESTION 15% MIN 5 MAX 50----- 15        
                       10 FILLER                PIC X(15).                      
      ******************************************************************        
      ***      TGA63 DONNEES VENTE ---------------------------------- 53        
               03 COMM-EGA63-APPLI.                                             
                   05 COMM-EGA63-DATA.                                          
      *                DONNEES --------------------------------------  9        
                       10 COMM-GA00-WLCONF         PIC X(01).                   
                       10 COMM-GA00-QCONTENU       PIC S9(05) COMP-3.           
                       10 COMM-GA00-QGRATUITE      PIC S9(05) COMP-3.           
                       10 COMM-GA00-CFETIQINFO     PIC X(01).                   
                       10 COMM-GA00-CFETIQPRIX     PIC X(01).                   
                       10 COMM-NV30-WQTEMULT       PIC X(01).                   
                       10 COMM-NV30-WREMISE        PIC X(01).                   
                       10 COMM-NV30-WTICKET        PIC X(01).                   
                       10 COMM-GA63-WLCONF-CFAM    PIC X(01).                   
      *                FILLER COMM-GA63-DATA 15% MIN 5 MAX 50--------- 9        
      *                10 FILLER                   PIC X(09).                   
      *                10 FILLER                   PIC X(06).                   
                       10 FILLER                   PIC X(05).                   
                   05 COMM-EGA63-GESTION.                                       
      *                GESTION -------------------------------------- 30        
                       10 COMM-GA63-NBRPAGE        PIC 9(05) COMP-3.            
                       10 COMM-GA63-NUMPAGE        PIC 9(05) COMP-3.            
                       10 COMM-GA63-ENCOURS-GA63   PIC X(05).                   
                       10 COMM-GA63-PRECEDE-GA63   PIC X(05).                   
                       10 COMM-GA63-SUIVANT-GA63   PIC X(05).                   
                       10 COMM-GA63-ENCOURS-GA64   PIC X(03).                   
                       10 COMM-GA63-PRECEDE-GA64   PIC X(03).                   
                       10 COMM-GA63-SUIVANT-GA64   PIC X(03).                   
      *                FILLER COMM-GA63-GESTION 15% MIN 5 MAX 50------ 5        
                       10 FILLER                   PIC X(05).                   
      ******************************************************************        
      ***      TGA67 PRESTATIONS LIEES ------------------------------ 75        
               03 COMM-EGA67-APPLI.                                             
                   05 COMM-EGA67-DATA.                                          
      *                DATA ----------------------------------------- 58        
                       10 COMM-GA67-CPREST      PIC X(05).                      
                       10 COMM-GA67-CFOURN      PIC X(05).                      
                       10 COMM-GA67-LPREST      PIC X(20).                      
                       10 COMM-GA67-LFOURN      PIC X(20).                      
                       10 COMM-GA67-DEFFET      PIC X(08).                      
      *                FILLER COMM-GA67-DATAN 15% MIN 5 MAX 50-------- 8        
                       10 FILLER                PIC X(08).                      
                   05 COMM-EGA67-GESTION.                                       
      *                GESTION --------------------------------------- 4        
                       10 COMM-GA67-ITEM        PIC 99.                         
                       10 COMM-GA67-ITEM-MAX    PIC 99.                         
      *                FILLER COMM-GA67-GESTION 15% MIN 5 MAX 50------ 5        
                       10 FILLER                PIC X(05).                      
      ******************************************************************        
      ***      TGA97 OFFRES LIEES COMET ----------------------------- 30        
               03 COMM-EGA97-APPLI.                                             
                   05 COMM-EGA97-DATA.                                          
      *                DONNEES --------------------------------------- 0        
      *                FILLER COMM-GA97-DATA 15% MIN 5 MAX 50-------- 15        
                       10 FILLER                PIC X(15).                      
                   05 COMM-EGA97-GESTION.                                       
      *                GESTION --------------------------------------- 0        
      *                FILLER COMM-GA97-GESTION 15% MIN 5 MAX 50----- 15        
                       10 FILLER                PIC X(15).                      
      ******************************************************************        
      ***      TGA98 CODE BARRE EAN --------------------------------- 21        
               03 COMM-EGA98-APPLI.                                             
                   05 COMM-EGA98-DATA.                                          
      *                DONNEES --------------------------------------- 0        
                       10 COMM-GA98-DES PIC X.                                  
                       10 COMM-GA98-OBL PIC X.                                  
                       10 COMM-GA98-AUT PIC X.                                  
                       10 COMM-GA98-SUP PIC X.                                  
                       10 COMM-GA98-ACT PIC X.                                  
      *                FILLER COMM-GA98-DATA 15% MIN 5 MAX 50--------- 5        
      *                10 FILLER                PIC X(05).                      
      *                10 FILLER                PIC X(01).                      
                   05 COMM-EGA98-GESTION.                                       
      *                GESTION -------------------------------------- 14        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                10 COMM-GA98-PAGE        PIC S9(3) COMP.                 
      *--                                                                       
                       10 COMM-GA98-PAGE        PIC S9(3) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                10 COMM-GA98-PAGEMAX     PIC S9(3) COMP.                 
      *--                                                                       
                       10 COMM-GA98-PAGEMAX     PIC S9(3) COMP-5.               
      *}                                                                        
                       10 COMM-GA98-NSEQUENCE   PIC S9(2) COMP-3.               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                10 COMM-GA98-TSDEB       PIC S9(3) COMP.                 
      *--                                                                       
                       10 COMM-GA98-TSDEB       PIC S9(3) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                10 COMM-GA98-TSFIN       PIC S9(3) COMP.                 
      *--                                                                       
                       10 COMM-GA98-TSFIN       PIC S9(3) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                10 COMM-GA98-TSENR       PIC S9(3) COMP.                 
      *--                                                                       
                       10 COMM-GA98-TSENR       PIC S9(3) COMP-5.               
      *}                                                                        
                       10 COMM-GA98-MAJ PIC 9.                                  
                           88 COMM-GA98-VALIDE VALUE 0.                         
                           88 COMM-GA98-CHANGE VALUE 1.                         
      *                10 COMM-GA98-FLAG-VERSION PIC X.                         
      *                FILLER COMM-GA98-GESTION 15% MIN 5 MAX 50------ 3        
      *                10 FILLER                PIC X(01).                      
                       10 FILLER                PIC X(02).                      
      ******************************************************************        
      ***      TGA68 COPIE FICHE INFO ------------------------------- 30        
               03 COMM-EGA68-APPLI.                                             
                   05 COMM-EGA68-DATA.                                          
      *                DONNEES --------------------------------------- 0        
      *                FILLER COMM-GA68-DATA 15% MIN 5 MAX 50-------- 15        
                       10 FILLER                PIC X(15).                      
                   05 COMM-EGA68-GESTION.                                       
      *                GESTION --------------------------------------- 0        
      *                FILLER COMM-GA68-GESTION 15% MIN 5 MAX 50----- 15        
                       10 FILLER                PIC X(15).                      
      ******************************************************************        
      ***      ZONE LIBRE TGA50 (MINI 756 TGA62)-------------------- 840        
      * !!!!!! ZONE UTILISEE PAR LE TGA62, TGA51...                             
               03 COMM-GA50-LIBRE.                                              
      *            05 FILLER                   PIC X(840).                      
                   05 FILLER                   PIC X(827).                      
                                                                                
