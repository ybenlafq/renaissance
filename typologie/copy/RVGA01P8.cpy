      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE QCNBS QUOTAS CARTES T SAV              *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01P8.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01P8.                                                            
      *}                                                                        
           05  QCNBS-CTABLEG2    PIC X(15).                                     
           05  QCNBS-CTABLEG2-REDEF REDEFINES QCNBS-CTABLEG2.                   
               10  QCNBS-NSOCIETE        PIC X(03).                             
               10  QCNBS-NLIEU           PIC X(03).                             
               10  QCNBS-CTCARTE         PIC X(01).                             
               10  QCNBS-G36             PIC X(03).                             
           05  QCNBS-WTABLEG     PIC X(80).                                     
           05  QCNBS-WTABLEG-REDEF  REDEFINES QCNBS-WTABLEG.                    
               10  QCNBS-WACTIF          PIC X(01).                             
               10  QCNBS-QUOTA           PIC X(04).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  QCNBS-QUOTA-N        REDEFINES QCNBS-QUOTA                   
                                         PIC 9(04).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01P8-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01P8-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QCNBS-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  QCNBS-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QCNBS-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  QCNBS-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
