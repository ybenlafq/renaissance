      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
000010****************************************************************  00010000
000020* TS SPECIFIQUE TRX24                                          *  00020001
000030****************************************************************  00050000
000040*                                                                 00060000
000050*------------------------------ LONGUEUR                          00070000
000060 01  TS-LONG              PIC S9(2) COMP-3 VALUE 92.              00080001
000070 01  TS-DONNEES.                                                  00090000
000080        10 TS-MNNUME      PIC X(07).                              00100001
000090        10 TS-MNCODE      PIC X(04).                              00120001
000100        10 TS-MLENSC      PIC X(15).                              00121001
000110        10 TS-MNMAGR      PIC X(03).                              00122001
000120        10 TS-MNZP        PIC X(02).                                      
000130        10 TS-MNNBREF     PIC 9(04).                              00123001
000140        10 TS-MDRELEVE    PIC 9(08).                                      
000150        10 TS-MHRELEVE    PIC 9(02).                                      
000160        10 TS-MDEDIT      PIC 9(08).                              00130001
000170        10 TS-MDSAISI     PIC 9(08).                              00140001
000180        10 TS-MDVALID     PIC 9(08).                              00141001
000190        10 TS-MDCLOREL    PIC 9(08).                              00141001
000200        10 TS-MFTRAIT     PIC X(01).                              00150001
000210        10 TS-MWTYPER     PIC X(01).                                      
000220        10 TS-MDSYST      PIC 9(13).                                      
                                                                                
