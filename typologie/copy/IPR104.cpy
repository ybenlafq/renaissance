      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPR104 AU 01/03/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,01,BI,A,                          *        
      *                           21,05,BI,A,                          *        
      *                           26,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPR104.                                                        
            05 NOMETAT-IPR104           PIC X(6) VALUE 'IPR104'.                
            05 RUPTURES-IPR104.                                                 
           10 IPR104-NLIEU              PIC X(03).                      007  003
           10 IPR104-CTYPPREST          PIC X(05).                      010  005
           10 IPR104-NENTCDE            PIC X(05).                      015  005
           10 IPR104-WMAJOPT            PIC X(01).                      020  001
           10 IPR104-CPRESTATION        PIC X(05).                      021  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPR104-SEQUENCE           PIC S9(04) COMP.                026  002
      *--                                                                       
           10 IPR104-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPR104.                                                   
           10 IPR104-CDEVISE            PIC X(06).                      028  006
           10 IPR104-LMAJOPT            PIC X(16).                      034  016
           10 IPR104-LPRESTATION        PIC X(20).                      050  020
           10 IPR104-NSOCIETE           PIC X(03).                      070  003
           10 IPR104-CA                 PIC S9(09)      COMP-3.         073  005
           10 IPR104-CA-MOIS            PIC S9(09)      COMP-3.         078  005
           10 IPR104-CA-7               PIC S9(09)      COMP-3.         083  005
           10 IPR104-NBPREST            PIC S9(06)      COMP-3.         088  004
           10 IPR104-NBPREST-MOIS       PIC S9(06)      COMP-3.         092  004
           10 IPR104-NBPREST-7          PIC S9(06)      COMP-3.         096  004
           10 IPR104-VOLNET             PIC S9(06)      COMP-3.         100  004
           10 IPR104-VOLNET-7           PIC S9(06)      COMP-3.         104  004
           10 IPR104-DVENTE             PIC X(08).                      108  008
            05 FILLER                      PIC X(397).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPR104-LONG           PIC S9(4)   COMP  VALUE +115.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPR104-LONG           PIC S9(4) COMP-5  VALUE +115.           
                                                                                
      *}                                                                        
