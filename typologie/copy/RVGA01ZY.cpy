      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SLRSE NMD SL : LISTES D EMPLACEMENT    *        
      *----------------------------------------------------------------*        
       01  RVGA01ZY.                                                            
           05  SLRSE-CTABLEG2    PIC X(15).                                     
           05  SLRSE-CTABLEG2-REDEF REDEFINES SLRSE-CTABLEG2.                   
               10  SLRSE-CRSL            PIC X(05).                             
               10  SLRSE-NSEQ            PIC X(02).                             
               10  SLRSE-NSEQ-N         REDEFINES SLRSE-NSEQ                    
                                         PIC 9(02).                             
           05  SLRSE-WTABLEG     PIC X(80).                                     
           05  SLRSE-WTABLEG-REDEF  REDEFINES SLRSE-WTABLEG.                    
               10  SLRSE-CEMPL           PIC X(05).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01ZY-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLRSE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SLRSE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLRSE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SLRSE-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
