      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE LITRT TABLE DES LIEUX DE TRAITEMENT    *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01EP.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01EP.                                                            
      *}                                                                        
           05  LITRT-CTABLEG2    PIC X(15).                                     
           05  LITRT-CTABLEG2-REDEF REDEFINES LITRT-CTABLEG2.                   
               10  LITRT-NSOCDEP         PIC X(06).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  LITRT-NSOCDEP-N      REDEFINES LITRT-NSOCDEP                 
                                         PIC 9(06).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  LITRT-CLITRT          PIC X(05).                             
           05  LITRT-WTABLEG     PIC X(80).                                     
           05  LITRT-WTABLEG-REDEF  REDEFINES LITRT-WTABLEG.                    
               10  LITRT-LLITRT          PIC X(20).                             
               10  LITRT-ACTIF           PIC X(01).                             
               10  LITRT-ENTMAG          PIC X(01).                             
               10  LITRT-ASSOCIAT        PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01EP-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01EP-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LITRT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  LITRT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LITRT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  LITRT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
