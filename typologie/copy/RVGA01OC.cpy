      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HSAPP CODES APPLICATIONS INV HS        *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01OC.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01OC.                                                            
      *}                                                                        
           05  HSAPP-CTABLEG2    PIC X(15).                                     
           05  HSAPP-CTABLEG2-REDEF REDEFINES HSAPP-CTABLEG2.                   
               10  HSAPP-NSOCLIEU        PIC X(06).                             
               10  HSAPP-NSSLIEU         PIC X(03).                             
               10  HSAPP-CLIEUTRT        PIC X(05).                             
           05  HSAPP-WTABLEG     PIC X(80).                                     
           05  HSAPP-WTABLEG-REDEF  REDEFINES HSAPP-WTABLEG.                    
               10  HSAPP-WACTIF          PIC X(01).                             
               10  HSAPP-CAPPLI          PIC X(03).                             
               10  HSAPP-WCONTROL        PIC X(04).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01OC-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01OC-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HSAPP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HSAPP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HSAPP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HSAPP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
