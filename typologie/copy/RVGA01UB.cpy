      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE AV002 SS-TABLE ECS : MOTIFS D AVOIR    *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVGA01UB.                                                            
           05  AV002-CTABLEG2    PIC X(15).                                     
           05  AV002-CTABLEG2-REDEF REDEFINES AV002-CTABLEG2.                   
               10  AV002-NSOCIETE        PIC X(03).                             
               10  AV002-CMOTIFAV        PIC X(05).                             
           05  AV002-WTABLEG     PIC X(80).                                     
           05  AV002-WTABLEG-REDEF  REDEFINES AV002-WTABLEG.                    
               10  AV002-LMOTIFAV        PIC X(20).                             
               10  AV002-NUECS           PIC X(05).                             
               10  AV002-CTAUXTVA        PIC X(05).                             
               10  AV002-WTECVENT        PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01UB-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AV002-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  AV002-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AV002-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  AV002-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
