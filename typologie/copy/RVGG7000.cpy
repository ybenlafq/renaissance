      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGG7000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGG7000                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGG7000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGG7000.                                                            
      *}                                                                        
           02  GG70-NCODIC                                                      
               PIC X(0007).                                                     
           02  GG70-DREC                                                        
               PIC X(0008).                                                     
           02  GG70-NREC                                                        
               PIC X(0007).                                                     
           02  GG70-NCDE                                                        
               PIC X(0007).                                                     
           02  GG70-PRA                                                         
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GG70-QTEREC                                                      
               PIC S9(5) COMP-3.                                                
           02  GG70-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGG7000                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGG7000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGG7000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG70-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG70-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG70-DREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG70-DREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG70-NREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG70-NREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG70-NCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG70-NCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG70-PRA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG70-PRA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG70-QTEREC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG70-QTEREC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG70-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GG70-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
