      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE BAMAX BA: MT MAXI POUR SIGNAT AUTOMA   *        
      *----------------------------------------------------------------*        
       01  RVGA01J9.                                                            
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  BAMAX-CTABLEG2    PIC X(15).                                     
           05  BAMAX-CTABLEG2-REDEF REDEFINES BAMAX-CTABLEG2.                   
               10  BAMAX-CMTMAXI         PIC X(01).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  BAMAX-CMTMAXI-N      REDEFINES BAMAX-CMTMAXI                 
                                         PIC 9(01).                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  BAMAX-WTABLEG     PIC X(80).                                     
           05  BAMAX-WTABLEG-REDEF  REDEFINES BAMAX-WTABLEG.                    
               10  BAMAX-PMTMAXI         PIC S9(05)V9(02)     COMP-3.           
       EXEC SQL END DECLARE SECTION END-EXEC.
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01J9-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BAMAX-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  BAMAX-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BAMAX-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  BAMAX-WTABLEG-F   PIC S9(4) COMP-5.                              
      *}                                                                        
