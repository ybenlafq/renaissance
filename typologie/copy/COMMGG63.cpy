      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00010000
      **************************************************************    00020000
      * COMMAREA SPECIFIQUE PRG TGG63 (TGG60 -> MENU)    TR: GG60  *    00030000
      *                                                            *    00031000
      *                       RECYCLAGE DU PRMP                    *    00040000
      *                                                            *    00050000
      * ZONES RESERVEES APPLICATIVES ------------------------------*    00060003
      *                                                            *    00070000
      *        TRANSACTION GG60 : RECYCLAGE PRMP SUR STOCK         *    00080000
      *                                                            *    00090000
      *------------------------------ ZONE DONNEES TGG60---3724----*    00110000
      *                                                                 00720200
           02 COMM-GG63-APPLI   REDEFINES  COMM-GG60-APPLI.             00721000
              03 COMM-GG63-NCODIC     PIC X(7).                         00740102
              03 COMM-GG63-NENTCDE    PIC X(5).                         00740202
              03 COMM-GG63-LENTCDE    PIC X(20).                        00740302
              03 COMM-GG63-NREC       PIC X(7).                         00740402
              03 COMM-GG63-LREFFOURN  PIC X(20).                        00740502
              03 COMM-GG63-LREF       PIC X(20).                        00740503
              03 COMM-GG63-DREC       PIC X(8).                         00740603
              03 COMM-GG63-CMARQ      PIC X(5).                         00740702
              03 COMM-GG63-LMARQ      PIC X(20).                        00740703
              03 COMM-GG63-NCDE       PIC X(7).                         00740803
              03 COMM-GG63-CFAM       PIC X(5).                         00740804
              03 COMM-GG63-LFAM       PIC X(20).                        00740805
              03 COMM-GG63-QREC       PIC S9(5)      COMP-3.            00740806
              03 COMM-GG63-PRA        PIC S9(7)V99   COMP-3.            00740807
              03 COMM-GG63-PRAANC     PIC S9(7)V9(2) COMP-3.            00740808
              03 COMM-GG63-PRMPANC    PIC S9(7)V9(6) COMP-3.            00740809
              03 COMM-GG63-DATE       PIC X(8).                         00740810
              03 COMM-GG63-DJOUR      PIC X(8).                         00740820
              03 COMM-GG63-MESS       PIC X(58).                        00740830
      *                                                                 00741100
      *------------------------------ ZONE DONNEES TGG62                00741200
      *                                                                 00741300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-GG63-ITEM       PIC S9(4)    COMP.                00743004
      *--                                                                       
              03 COMM-GG63-ITEM       PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-GG63-POS-MAX    PIC S9(4)    COMP.                00743005
      *--                                                                       
              03 COMM-GG63-POS-MAX    PIC S9(4) COMP-5.                         
      *}                                                                        
              03 COMM-GG63-CDEVISE    PIC X(3).                                 
              03 COMM-GG63-LDEVISE    PIC X(25).                                
              03 COMM-GG63-CAUTRE     PIC X(3).                                 
              03 COMM-GG63-LAUTRE     PIC X(25).                                
              03 FILLER               PIC X(3426).                      00743006
                                                                                
                                                                        00750000
