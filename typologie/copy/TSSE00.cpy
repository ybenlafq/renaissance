      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      *                    TS SPECIFIQUE TSE00                        * 00000020
      ***************************************************************** 00000030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 TS-LONG             PIC S9(4) COMP VALUE +124.                00000040
      *--                                                                       
       01 TS-LONG             PIC S9(4) COMP-5 VALUE +124.                      
      *}                                                                        
       01 TS-DATA.                                                      00000050
          05 TS-LIGNE.                                                  00000060
             10 TS-MNCSERV    PIC X(05).                                00000070
             10 TS-MCFAM      PIC X(05).                                00000080
             10 TS-MCOPER     PIC X(05).                                00000090
             10 TS-MCASSOR    PIC X(05).                                00000100
             10 TS-MLCSERV    PIC X(20).                                00000110
             10 TS-MLCFAM     PIC X(20).                                00000120
             10 TS-MLCOPER    PIC X(20).                                00000130
             10 TS-MLASSOR    PIC X(20).                                00000140
             10 TS-WPRODMAJ   PIC X(01).                                00000150
             10 TS-SEGMENT    PIC X(20).                                00000160
             10 TS-MACTION    PIC X(03).                                00000170
                                                                                
