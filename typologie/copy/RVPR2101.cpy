      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ****************************************************************  00000010
      *   DECLARATION COBOL DE LA VUE RVPR2101                       *  00000020
      ****************************************************************  00000030
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVPR2101.                                                    00000040
           10 PR21-CTYPE           PIC X(1).                            00000050
           10 PR21-NENTCDE         PIC X(5).                            00000060
           10 PR21-NCHRONO         PIC X(5).                            00000070
           10 PR21-LIBELLE         PIC X(26).                           00000080
           10 PR21-CTYPPREST       PIC X(5).                            00000090
           10 PR21-CPREST          PIC X(5).                            00000100
           10 PR21-NCODIC          PIC X(7).                            00000110
           10 PR21-WINCCOND        PIC X(1).                            00000120
           10 PR21-CRGLT           PIC X(3).                            00000130
           10 PR21-CFACT           PIC X(3).                            00000140
           10 PR21-WMTVENTE        PIC X(1).                            00000150
           10 PR21-DMAJ            PIC X(8).                            00000160
           10 PR21-DSYST           PIC S9(13)V USAGE COMP-3.            00000170
           10 PR21-NCODICGRP       PIC X(7).                            00000180
           10 PR21-CTYPCND         PIC X(5).                            00000190
           10 PR21-LCOMMENT        PIC X(30).                           00000200
                                                                        00000210
       01  RVPR2101-FLAGS.                                              00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR21-CTYPE-F         PIC S9(4) COMP.                      00000230
      *--                                                                       
           10 PR21-CTYPE-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR21-NENTCDE-F       PIC S9(4) COMP.                      00000240
      *--                                                                       
           10 PR21-NENTCDE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR21-NCHRONO-F       PIC S9(4) COMP.                      00000250
      *--                                                                       
           10 PR21-NCHRONO-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR21-LIBELLE-F       PIC S9(4) COMP.                      00000260
      *--                                                                       
           10 PR21-LIBELLE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR21-CTYPPREST-F     PIC S9(4) COMP.                      00000270
      *--                                                                       
           10 PR21-CTYPPREST-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR21-CPREST-F        PIC S9(4) COMP.                      00000280
      *--                                                                       
           10 PR21-CPREST-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR21-NCODIC-F        PIC S9(4) COMP.                      00000290
      *--                                                                       
           10 PR21-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR21-WINCCOND-F      PIC S9(4) COMP.                      00000300
      *--                                                                       
           10 PR21-WINCCOND-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR21-CRGLT-F         PIC S9(4) COMP.                      00000310
      *--                                                                       
           10 PR21-CRGLT-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR21-CFACT-F         PIC S9(4) COMP.                      00000320
      *--                                                                       
           10 PR21-CFACT-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR21-WMTVENTE-F      PIC S9(4) COMP.                      00000330
      *--                                                                       
           10 PR21-WMTVENTE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR21-DMAJ-F          PIC S9(4) COMP.                      00000340
      *--                                                                       
           10 PR21-DMAJ-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR21-DSYST-F         PIC S9(4) COMP.                      00000350
      *--                                                                       
           10 PR21-DSYST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR21-NCODICGRP-F     PIC S9(4) COMP.                      00000360
      *--                                                                       
           10 PR21-NCODICGRP-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR21-CTYPCND-F       PIC S9(4) COMP.                      00000370
      *--                                                                       
           10 PR21-CTYPCND-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR21-LCOMMENT-F      PIC S9(4) COMP.                      00000380
      *                                                                         
      *--                                                                       
           10 PR21-LCOMMENT-F      PIC S9(4) COMP-5.                            
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
