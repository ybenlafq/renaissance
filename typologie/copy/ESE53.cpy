      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESE03   ESE03                                              00000020
      ***************************************************************** 00000030
       01   ESE53I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCSERVL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNCSERVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCSERVF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNCSERVI  PIC X(5).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCSERVL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLCSERVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCSERVF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLCSERVI  PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPERL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCOPERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOPERF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCOPERI   PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLOPERL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLOPERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLOPERF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLOPERI   PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCFAMI    PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLFAMI    PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MPAGEI    PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MPAGEMAXI      PIC X(3).                                  00000490
           02 MWTABLEI OCCURS   8 TIMES .                               00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCDOSSL      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MCDOSSL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCDOSSF      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCDOSSI      PIC X(5).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLDOSSL      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MLDOSSL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLDOSSF      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MLDOSSI      PIC X(20).                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATDEBL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MDATDEBL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDATDEBF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MDATDEBI     PIC X(8).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATFINL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MDATFINL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDATFINF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MDATFINI     PIC X(8).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSEQL  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MNSEQL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSEQF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNSEQI  PIC X(3).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSUPPL  COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MSUPPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSUPPF  PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MSUPPI  PIC X.                                          00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDOSS1L  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCDOSS1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCDOSS1F  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCDOSS1I  PIC X(5).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATDEB1L      COMP PIC S9(4).                            00000790
      *--                                                                       
           02 MDATDEB1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATDEB1F      PIC X.                                     00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MDATDEB1I      PIC X(8).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATFIN1L      COMP PIC S9(4).                            00000830
      *--                                                                       
           02 MDATFIN1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATFIN1F      PIC X.                                     00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MDATFIN1I      PIC X(8).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSEQ1L   COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MNSEQ1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSEQ1F   PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MNSEQ1I   PIC X(3).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MZONCMDI  PIC X(15).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLIBERRI  PIC X(58).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCODTRAI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCICSI    PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MNETNAMI  PIC X(8).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MSCREENI  PIC X(4).                                       00001140
      ***************************************************************** 00001150
      * SDF: ESE03   ESE03                                              00001160
      ***************************************************************** 00001170
       01   ESE53O REDEFINES ESE53I.                                    00001180
           02 FILLER    PIC X(12).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTIMJOUA  PIC X.                                          00001280
           02 MTIMJOUC  PIC X.                                          00001290
           02 MTIMJOUP  PIC X.                                          00001300
           02 MTIMJOUH  PIC X.                                          00001310
           02 MTIMJOUV  PIC X.                                          00001320
           02 MTIMJOUO  PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MWFONCA   PIC X.                                          00001350
           02 MWFONCC   PIC X.                                          00001360
           02 MWFONCP   PIC X.                                          00001370
           02 MWFONCH   PIC X.                                          00001380
           02 MWFONCV   PIC X.                                          00001390
           02 MWFONCO   PIC X(3).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MNCSERVA  PIC X.                                          00001420
           02 MNCSERVC  PIC X.                                          00001430
           02 MNCSERVP  PIC X.                                          00001440
           02 MNCSERVH  PIC X.                                          00001450
           02 MNCSERVV  PIC X.                                          00001460
           02 MNCSERVO  PIC X(5).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MLCSERVA  PIC X.                                          00001490
           02 MLCSERVC  PIC X.                                          00001500
           02 MLCSERVP  PIC X.                                          00001510
           02 MLCSERVH  PIC X.                                          00001520
           02 MLCSERVV  PIC X.                                          00001530
           02 MLCSERVO  PIC X(20).                                      00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MCOPERA   PIC X.                                          00001560
           02 MCOPERC   PIC X.                                          00001570
           02 MCOPERP   PIC X.                                          00001580
           02 MCOPERH   PIC X.                                          00001590
           02 MCOPERV   PIC X.                                          00001600
           02 MCOPERO   PIC X(5).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MLOPERA   PIC X.                                          00001630
           02 MLOPERC   PIC X.                                          00001640
           02 MLOPERP   PIC X.                                          00001650
           02 MLOPERH   PIC X.                                          00001660
           02 MLOPERV   PIC X.                                          00001670
           02 MLOPERO   PIC X(20).                                      00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MCFAMA    PIC X.                                          00001700
           02 MCFAMC    PIC X.                                          00001710
           02 MCFAMP    PIC X.                                          00001720
           02 MCFAMH    PIC X.                                          00001730
           02 MCFAMV    PIC X.                                          00001740
           02 MCFAMO    PIC X(5).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MLFAMA    PIC X.                                          00001770
           02 MLFAMC    PIC X.                                          00001780
           02 MLFAMP    PIC X.                                          00001790
           02 MLFAMH    PIC X.                                          00001800
           02 MLFAMV    PIC X.                                          00001810
           02 MLFAMO    PIC X(20).                                      00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MPAGEA    PIC X.                                          00001840
           02 MPAGEC    PIC X.                                          00001850
           02 MPAGEP    PIC X.                                          00001860
           02 MPAGEH    PIC X.                                          00001870
           02 MPAGEV    PIC X.                                          00001880
           02 MPAGEO    PIC X(3).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MPAGEMAXA      PIC X.                                     00001910
           02 MPAGEMAXC PIC X.                                          00001920
           02 MPAGEMAXP PIC X.                                          00001930
           02 MPAGEMAXH PIC X.                                          00001940
           02 MPAGEMAXV PIC X.                                          00001950
           02 MPAGEMAXO      PIC X(3).                                  00001960
           02 MWTABLEO OCCURS   8 TIMES .                               00001970
             03 FILLER       PIC X(2).                                  00001980
             03 MCDOSSA      PIC X.                                     00001990
             03 MCDOSSC PIC X.                                          00002000
             03 MCDOSSP PIC X.                                          00002010
             03 MCDOSSH PIC X.                                          00002020
             03 MCDOSSV PIC X.                                          00002030
             03 MCDOSSO      PIC X(5).                                  00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MLDOSSA      PIC X.                                     00002060
             03 MLDOSSC PIC X.                                          00002070
             03 MLDOSSP PIC X.                                          00002080
             03 MLDOSSH PIC X.                                          00002090
             03 MLDOSSV PIC X.                                          00002100
             03 MLDOSSO      PIC X(20).                                 00002110
             03 FILLER       PIC X(2).                                  00002120
             03 MDATDEBA     PIC X.                                     00002130
             03 MDATDEBC     PIC X.                                     00002140
             03 MDATDEBP     PIC X.                                     00002150
             03 MDATDEBH     PIC X.                                     00002160
             03 MDATDEBV     PIC X.                                     00002170
             03 MDATDEBO     PIC X(8).                                  00002180
             03 FILLER       PIC X(2).                                  00002190
             03 MDATFINA     PIC X.                                     00002200
             03 MDATFINC     PIC X.                                     00002210
             03 MDATFINP     PIC X.                                     00002220
             03 MDATFINH     PIC X.                                     00002230
             03 MDATFINV     PIC X.                                     00002240
             03 MDATFINO     PIC X(8).                                  00002250
             03 FILLER       PIC X(2).                                  00002260
             03 MNSEQA  PIC X.                                          00002270
             03 MNSEQC  PIC X.                                          00002280
             03 MNSEQP  PIC X.                                          00002290
             03 MNSEQH  PIC X.                                          00002300
             03 MNSEQV  PIC X.                                          00002310
             03 MNSEQO  PIC X(3).                                       00002320
             03 FILLER       PIC X(2).                                  00002330
             03 MSUPPA  PIC X.                                          00002340
             03 MSUPPC  PIC X.                                          00002350
             03 MSUPPP  PIC X.                                          00002360
             03 MSUPPH  PIC X.                                          00002370
             03 MSUPPV  PIC X.                                          00002380
             03 MSUPPO  PIC X.                                          00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MCDOSS1A  PIC X.                                          00002410
           02 MCDOSS1C  PIC X.                                          00002420
           02 MCDOSS1P  PIC X.                                          00002430
           02 MCDOSS1H  PIC X.                                          00002440
           02 MCDOSS1V  PIC X.                                          00002450
           02 MCDOSS1O  PIC X(5).                                       00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MDATDEB1A      PIC X.                                     00002480
           02 MDATDEB1C PIC X.                                          00002490
           02 MDATDEB1P PIC X.                                          00002500
           02 MDATDEB1H PIC X.                                          00002510
           02 MDATDEB1V PIC X.                                          00002520
           02 MDATDEB1O      PIC X(8).                                  00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MDATFIN1A      PIC X.                                     00002550
           02 MDATFIN1C PIC X.                                          00002560
           02 MDATFIN1P PIC X.                                          00002570
           02 MDATFIN1H PIC X.                                          00002580
           02 MDATFIN1V PIC X.                                          00002590
           02 MDATFIN1O      PIC X(8).                                  00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MNSEQ1A   PIC X.                                          00002620
           02 MNSEQ1C   PIC X.                                          00002630
           02 MNSEQ1P   PIC X.                                          00002640
           02 MNSEQ1H   PIC X.                                          00002650
           02 MNSEQ1V   PIC X.                                          00002660
           02 MNSEQ1O   PIC X(3).                                       00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MZONCMDA  PIC X.                                          00002690
           02 MZONCMDC  PIC X.                                          00002700
           02 MZONCMDP  PIC X.                                          00002710
           02 MZONCMDH  PIC X.                                          00002720
           02 MZONCMDV  PIC X.                                          00002730
           02 MZONCMDO  PIC X(15).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MLIBERRA  PIC X.                                          00002760
           02 MLIBERRC  PIC X.                                          00002770
           02 MLIBERRP  PIC X.                                          00002780
           02 MLIBERRH  PIC X.                                          00002790
           02 MLIBERRV  PIC X.                                          00002800
           02 MLIBERRO  PIC X(58).                                      00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MCODTRAA  PIC X.                                          00002830
           02 MCODTRAC  PIC X.                                          00002840
           02 MCODTRAP  PIC X.                                          00002850
           02 MCODTRAH  PIC X.                                          00002860
           02 MCODTRAV  PIC X.                                          00002870
           02 MCODTRAO  PIC X(4).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MCICSA    PIC X.                                          00002900
           02 MCICSC    PIC X.                                          00002910
           02 MCICSP    PIC X.                                          00002920
           02 MCICSH    PIC X.                                          00002930
           02 MCICSV    PIC X.                                          00002940
           02 MCICSO    PIC X(5).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MNETNAMA  PIC X.                                          00002970
           02 MNETNAMC  PIC X.                                          00002980
           02 MNETNAMP  PIC X.                                          00002990
           02 MNETNAMH  PIC X.                                          00003000
           02 MNETNAMV  PIC X.                                          00003010
           02 MNETNAMO  PIC X(8).                                       00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MSCREENA  PIC X.                                          00003040
           02 MSCREENC  PIC X.                                          00003050
           02 MSCREENP  PIC X.                                          00003060
           02 MSCREENH  PIC X.                                          00003070
           02 MSCREENV  PIC X.                                          00003080
           02 MSCREENO  PIC X(4).                                       00003090
                                                                                
