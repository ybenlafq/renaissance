      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE BAAUX NUMEROS D AUXILIAIRES POUR B.A   *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVGA01L6.                                                            
           05  BAAUX-CTABLEG2    PIC X(15).                                     
           05  BAAUX-CTABLEG2-REDEF REDEFINES BAAUX-CTABLEG2.                   
               10  BAAUX-NSOCGL          PIC X(05).                             
               10  BAAUX-NAUX            PIC X(03).                             
           05  BAAUX-WTABLEG     PIC X(80).                                     
           05  BAAUX-WTABLEG-REDEF  REDEFINES BAAUX-WTABLEG.                    
               10  BAAUX-NETAB           PIC X(03).                             
               10  BAAUX-WACTIF          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01L6-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BAAUX-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  BAAUX-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BAAUX-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  BAAUX-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
