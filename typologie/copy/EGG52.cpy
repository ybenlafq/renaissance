      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * MAJ DES PRIX ARTICLES ET PRIMES                                 00000020
      ***************************************************************** 00000030
       01   EGG52I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNCODICI  PIC X(7).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPCHTL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPCHTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPCHTF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPCHTI    PIC X(8).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPROL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAPPROF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCAPPROI  PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLREFI    PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPOL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXPOF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCEXPOI   PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCMARQI   PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCFAMI    PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMPACL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCOMPACL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCOMPACF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCOMPACI  PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOANATL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MOANATL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MOANATF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MOANATI   PIC X.                                          00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOAZPL    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MOAZPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MOAZPF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MOAZPI    PIC X.                                          00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDOFDEBL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDOFDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDOFDEBF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDOFDEBI  PIC X(8).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDOFFINL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MDOFFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDOFFINF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MDOFFINI  PIC X(8).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRXREFAL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MPRXREFAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPRXREFAF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MPRXREFAI      PIC X(9).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWIMPAL   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MWIMPAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWIMPAF   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MWIMPAI   PIC X(3).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDREFAL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MDDREFAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDDREFAF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MDDREFAI  PIC X(8).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIB1L    COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MLIB1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIB1F    PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLIB1I    PIC X(2).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFREFAL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MDFREFAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDFREFAF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MDFREFAI  PIC X(8).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRXREFNL      COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MPRXREFNL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPRXREFNF      PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MPRXREFNI      PIC X(9).                                  00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWIMPNL   COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MWIMPNL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWIMPNF   PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MWIMPNI   PIC X(3).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDREFNL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MDDREFNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDDREFNF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MDDREFNI  PIC X(8).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIB2L    COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MLIB2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIB2F    PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MLIB2I    PIC X(2).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFREFNL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MDFREFNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDFREFNF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MDFREFNI  PIC X(8).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRXREF2AL     COMP PIC S9(4).                            00001020
      *--                                                                       
           02 MPRXREF2AL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MPRXREF2AF     PIC X.                                     00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MPRXREF2AI     PIC X(9).                                  00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBLOCAL   COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MBLOCAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MBLOCAF   PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MBLOCAI   PIC X(3).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDREF2AL      COMP PIC S9(4).                            00001100
      *--                                                                       
           02 MDDREF2AL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDDREF2AF      PIC X.                                     00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MDDREF2AI      PIC X(8).                                  00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIB3L    COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MLIB3L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIB3F    PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MLIB3I    PIC X(2).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFREF2AL      COMP PIC S9(4).                            00001180
      *--                                                                       
           02 MDFREF2AL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDFREF2AF      PIC X.                                     00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MDFREF2AI      PIC X(8).                                  00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRXREF2NL     COMP PIC S9(4).                            00001220
      *--                                                                       
           02 MPRXREF2NL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MPRXREF2NF     PIC X.                                     00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MPRXREF2NI     PIC X(9).                                  00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBLOCNL   COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MBLOCNL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MBLOCNF   PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MBLOCNI   PIC X(3).                                       00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDREF2NL      COMP PIC S9(4).                            00001300
      *--                                                                       
           02 MDDREF2NL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDDREF2NF      PIC X.                                     00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MDDREF2NI      PIC X(8).                                  00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIB4L    COMP PIC S9(4).                                 00001340
      *--                                                                       
           02 MLIB4L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIB4F    PIC X.                                          00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MLIB4I    PIC X(2).                                       00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFREF2NL      COMP PIC S9(4).                            00001380
      *--                                                                       
           02 MDFREF2NL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDFREF2NF      PIC X.                                     00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MDFREF2NI      PIC X(8).                                  00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNZONMOL  COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MNZONMOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNZONMOF  PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MNZONMOI  PIC X(2).                                       00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRIMOL   COMP PIC S9(4).                                 00001460
      *--                                                                       
           02 MPRIMOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPRIMOF   PIC X.                                          00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MPRIMOI   PIC X(8).                                       00001490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPCOMMOL  COMP PIC S9(4).                                 00001500
      *--                                                                       
           02 MPCOMMOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPCOMMOF  PIC X.                                          00001510
           02 FILLER    PIC X(4).                                       00001520
           02 MPCOMMOI  PIC X(6).                                       00001530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MINFAMOL  COMP PIC S9(4).                                 00001540
      *--                                                                       
           02 MINFAMOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MINFAMOF  PIC X.                                          00001550
           02 FILLER    PIC X(4).                                       00001560
           02 MINFAMOI  PIC X(3).                                       00001570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDPRIMOL  COMP PIC S9(4).                                 00001580
      *--                                                                       
           02 MDPRIMOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDPRIMOF  PIC X.                                          00001590
           02 FILLER    PIC X(4).                                       00001600
           02 MDPRIMOI  PIC X(8).                                       00001610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPMBUMOL  COMP PIC S9(4).                                 00001620
      *--                                                                       
           02 MPMBUMOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPMBUMOF  PIC X.                                          00001630
           02 FILLER    PIC X(4).                                       00001640
           02 MPMBUMOI  PIC X(8).                                       00001650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTMAMOL  COMP PIC S9(4).                                 00001660
      *--                                                                       
           02 MQTMAMOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQTMAMOF  PIC X.                                          00001670
           02 FILLER    PIC X(4).                                       00001680
           02 MQTMAMOI  PIC X(5).                                       00001690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFMILMOL  COMP PIC S9(4).                                 00001700
      *--                                                                       
           02 MFMILMOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MFMILMOF  PIC X.                                          00001710
           02 FILLER    PIC X(4).                                       00001720
           02 MFMILMOI  PIC X(6).                                       00001730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMMOL  COMP PIC S9(4).                                 00001740
      *--                                                                       
           02 MLCOMMOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCOMMOF  PIC X.                                          00001750
           02 FILLER    PIC X(4).                                       00001760
           02 MLCOMMOI  PIC X(10).                                      00001770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNZONMAL  COMP PIC S9(4).                                 00001780
      *--                                                                       
           02 MNZONMAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNZONMAF  PIC X.                                          00001790
           02 FILLER    PIC X(4).                                       00001800
           02 MNZONMAI  PIC X(2).                                       00001810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRIMACL  COMP PIC S9(4).                                 00001820
      *--                                                                       
           02 MPRIMACL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPRIMACF  PIC X.                                          00001830
           02 FILLER    PIC X(4).                                       00001840
           02 MPRIMACI  PIC X(8).                                       00001850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPCOMMAL  COMP PIC S9(4).                                 00001860
      *--                                                                       
           02 MPCOMMAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPCOMMAF  PIC X.                                          00001870
           02 FILLER    PIC X(4).                                       00001880
           02 MPCOMMAI  PIC X(6).                                       00001890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MINFAMAL  COMP PIC S9(4).                                 00001900
      *--                                                                       
           02 MINFAMAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MINFAMAF  PIC X.                                          00001910
           02 FILLER    PIC X(4).                                       00001920
           02 MINFAMAI  PIC X(3).                                       00001930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDPRIMAL  COMP PIC S9(4).                                 00001940
      *--                                                                       
           02 MDPRIMAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDPRIMAF  PIC X.                                          00001950
           02 FILLER    PIC X(4).                                       00001960
           02 MDPRIMAI  PIC X(8).                                       00001970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPMBUMAL  COMP PIC S9(4).                                 00001980
      *--                                                                       
           02 MPMBUMAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPMBUMAF  PIC X.                                          00001990
           02 FILLER    PIC X(4).                                       00002000
           02 MPMBUMAI  PIC X(8).                                       00002010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTMAMAL  COMP PIC S9(4).                                 00002020
      *--                                                                       
           02 MQTMAMAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQTMAMAF  PIC X.                                          00002030
           02 FILLER    PIC X(4).                                       00002040
           02 MQTMAMAI  PIC X(5).                                       00002050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFMILMAL  COMP PIC S9(4).                                 00002060
      *--                                                                       
           02 MFMILMAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MFMILMAF  PIC X.                                          00002070
           02 FILLER    PIC X(4).                                       00002080
           02 MFMILMAI  PIC X(6).                                       00002090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMMAL  COMP PIC S9(4).                                 00002100
      *--                                                                       
           02 MLCOMMAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCOMMAF  PIC X.                                          00002110
           02 FILLER    PIC X(4).                                       00002120
           02 MLCOMMAI  PIC X(10).                                      00002130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNZONMNL  COMP PIC S9(4).                                 00002140
      *--                                                                       
           02 MNZONMNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNZONMNF  PIC X.                                          00002150
           02 FILLER    PIC X(4).                                       00002160
           02 MNZONMNI  PIC X(2).                                       00002170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRIMNOL  COMP PIC S9(4).                                 00002180
      *--                                                                       
           02 MPRIMNOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPRIMNOF  PIC X.                                          00002190
           02 FILLER    PIC X(4).                                       00002200
           02 MPRIMNOI  PIC X(8).                                       00002210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPCOMMNL  COMP PIC S9(4).                                 00002220
      *--                                                                       
           02 MPCOMMNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPCOMMNF  PIC X.                                          00002230
           02 FILLER    PIC X(4).                                       00002240
           02 MPCOMMNI  PIC X(6).                                       00002250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MINFAMNL  COMP PIC S9(4).                                 00002260
      *--                                                                       
           02 MINFAMNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MINFAMNF  PIC X.                                          00002270
           02 FILLER    PIC X(4).                                       00002280
           02 MINFAMNI  PIC X(3).                                       00002290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDPRIMNL  COMP PIC S9(4).                                 00002300
      *--                                                                       
           02 MDPRIMNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDPRIMNF  PIC X.                                          00002310
           02 FILLER    PIC X(4).                                       00002320
           02 MDPRIMNI  PIC X(8).                                       00002330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPMBUMNL  COMP PIC S9(4).                                 00002340
      *--                                                                       
           02 MPMBUMNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPMBUMNF  PIC X.                                          00002350
           02 FILLER    PIC X(4).                                       00002360
           02 MPMBUMNI  PIC X(8).                                       00002370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTMAMNL  COMP PIC S9(4).                                 00002380
      *--                                                                       
           02 MQTMAMNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQTMAMNF  PIC X.                                          00002390
           02 FILLER    PIC X(4).                                       00002400
           02 MQTMAMNI  PIC X(5).                                       00002410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFMILMNL  COMP PIC S9(4).                                 00002420
      *--                                                                       
           02 MFMILMNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MFMILMNF  PIC X.                                          00002430
           02 FILLER    PIC X(4).                                       00002440
           02 MFMILMNI  PIC X(6).                                       00002450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMMNL  COMP PIC S9(4).                                 00002460
      *--                                                                       
           02 MLCOMMNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCOMMNF  PIC X.                                          00002470
           02 FILLER    PIC X(4).                                       00002480
           02 MLCOMMNI  PIC X(10).                                      00002490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00002500
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00002510
           02 FILLER    PIC X(4).                                       00002520
           02 MNPAGEI   PIC X(2).                                       00002530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGML   COMP PIC S9(4).                                 00002540
      *--                                                                       
           02 MNPAGML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGMF   PIC X.                                          00002550
           02 FILLER    PIC X(4).                                       00002560
           02 MNPAGMI   PIC X(2).                                       00002570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNZONEXL  COMP PIC S9(4).                                 00002580
      *--                                                                       
           02 MNZONEXL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNZONEXF  PIC X.                                          00002590
           02 FILLER    PIC X(4).                                       00002600
           02 MNZONEXI  PIC X(2).                                       00002610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRIEXL   COMP PIC S9(4).                                 00002620
      *--                                                                       
           02 MPRIEXL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPRIEXF   PIC X.                                          00002630
           02 FILLER    PIC X(4).                                       00002640
           02 MPRIEXI   PIC X(8).                                       00002650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMEXL   COMP PIC S9(4).                                 00002660
      *--                                                                       
           02 MCOMEXL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOMEXF   PIC X.                                          00002670
           02 FILLER    PIC X(4).                                       00002680
           02 MCOMEXI   PIC X(6).                                       00002690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDPRIEXL  COMP PIC S9(4).                                 00002700
      *--                                                                       
           02 MDPRIEXL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDPRIEXF  PIC X.                                          00002710
           02 FILLER    PIC X(4).                                       00002720
           02 MDPRIEXI  PIC X(8).                                       00002730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPMBUEXL  COMP PIC S9(4).                                 00002740
      *--                                                                       
           02 MPMBUEXL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPMBUEXF  PIC X.                                          00002750
           02 FILLER    PIC X(4).                                       00002760
           02 MPMBUEXI  PIC X(8).                                       00002770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTMAEXL  COMP PIC S9(4).                                 00002780
      *--                                                                       
           02 MQTMAEXL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQTMAEXF  PIC X.                                          00002790
           02 FILLER    PIC X(4).                                       00002800
           02 MQTMAEXI  PIC X(5).                                       00002810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFMILEXL  COMP PIC S9(4).                                 00002820
      *--                                                                       
           02 MFMILEXL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MFMILEXF  PIC X.                                          00002830
           02 FILLER    PIC X(4).                                       00002840
           02 MFMILEXI  PIC X(6).                                       00002850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMEXL  COMP PIC S9(4).                                 00002860
      *--                                                                       
           02 MLCOMEXL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCOMEXF  PIC X.                                          00002870
           02 FILLER    PIC X(4).                                       00002880
           02 MLCOMEXI  PIC X(10).                                      00002890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFINEFL  COMP PIC S9(4).                                 00002900
      *--                                                                       
           02 MDFINEFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDFINEFF  PIC X.                                          00002910
           02 FILLER    PIC X(4).                                       00002920
           02 MDFINEFI  PIC X(8).                                       00002930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCONCL   COMP PIC S9(4).                                 00002940
      *--                                                                       
           02 MNCONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCONCF   PIC X.                                          00002950
           02 FILLER    PIC X(4).                                       00002960
           02 MNCONCI   PIC X(4).                                       00002970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCONCL   COMP PIC S9(4).                                 00002980
      *--                                                                       
           02 MLCONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLCONCF   PIC X.                                          00002990
           02 FILLER    PIC X(4).                                       00003000
           02 MLCONCI   PIC X(15).                                      00003010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLORIGL   COMP PIC S9(4).                                 00003020
      *--                                                                       
           02 MLORIGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLORIGF   PIC X.                                          00003030
           02 FILLER    PIC X(4).                                       00003040
           02 MLORIGI   PIC X(12).                                      00003050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEU1L  COMP PIC S9(4).                                 00003060
      *--                                                                       
           02 MNLIEU1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIEU1F  PIC X.                                          00003070
           02 FILLER    PIC X(4).                                       00003080
           02 MNLIEU1I  PIC X(3).                                       00003090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEU2L  COMP PIC S9(4).                                 00003100
      *--                                                                       
           02 MNLIEU2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIEU2F  PIC X.                                          00003110
           02 FILLER    PIC X(4).                                       00003120
           02 MNLIEU2I  PIC X(3).                                       00003130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEU3L  COMP PIC S9(4).                                 00003140
      *--                                                                       
           02 MNLIEU3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIEU3F  PIC X.                                          00003150
           02 FILLER    PIC X(4).                                       00003160
           02 MNLIEU3I  PIC X(3).                                       00003170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEU4L  COMP PIC S9(4).                                 00003180
      *--                                                                       
           02 MNLIEU4L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIEU4F  PIC X.                                          00003190
           02 FILLER    PIC X(4).                                       00003200
           02 MNLIEU4I  PIC X(3).                                       00003210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEU5L  COMP PIC S9(4).                                 00003220
      *--                                                                       
           02 MNLIEU5L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIEU5F  PIC X.                                          00003230
           02 FILLER    PIC X(4).                                       00003240
           02 MNLIEU5I  PIC X(3).                                       00003250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEU6L  COMP PIC S9(4).                                 00003260
      *--                                                                       
           02 MNLIEU6L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIEU6F  PIC X.                                          00003270
           02 FILLER    PIC X(4).                                       00003280
           02 MNLIEU6I  PIC X(3).                                       00003290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEU7L  COMP PIC S9(4).                                 00003300
      *--                                                                       
           02 MNLIEU7L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIEU7F  PIC X.                                          00003310
           02 FILLER    PIC X(4).                                       00003320
           02 MNLIEU7I  PIC X(3).                                       00003330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEU8L  COMP PIC S9(4).                                 00003340
      *--                                                                       
           02 MNLIEU8L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIEU8F  PIC X.                                          00003350
           02 FILLER    PIC X(4).                                       00003360
           02 MNLIEU8I  PIC X(3).                                       00003370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEU9L  COMP PIC S9(4).                                 00003380
      *--                                                                       
           02 MNLIEU9L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIEU9F  PIC X.                                          00003390
           02 FILLER    PIC X(4).                                       00003400
           02 MNLIEU9I  PIC X(3).                                       00003410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEU10L      COMP PIC S9(4).                            00003420
      *--                                                                       
           02 MNLIEU10L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIEU10F      PIC X.                                     00003430
           02 FILLER    PIC X(4).                                       00003440
           02 MNLIEU10I      PIC X(3).                                  00003450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODISL  COMP PIC S9(4).                                 00003460
      *--                                                                       
           02 MNCODISL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODISF  PIC X.                                          00003470
           02 FILLER    PIC X(4).                                       00003480
           02 MNCODISI  PIC X(7).                                       00003490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00003500
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00003510
           02 FILLER    PIC X(4).                                       00003520
           02 MZONCMDI  PIC X(15).                                      00003530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00003540
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00003550
           02 FILLER    PIC X(4).                                       00003560
           02 MLIBERRI  PIC X(58).                                      00003570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00003580
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00003590
           02 FILLER    PIC X(4).                                       00003600
           02 MCODTRAI  PIC X(4).                                       00003610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00003620
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00003630
           02 FILLER    PIC X(4).                                       00003640
           02 MCICSI    PIC X(5).                                       00003650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00003660
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00003670
           02 FILLER    PIC X(4).                                       00003680
           02 MNETNAMI  PIC X(8).                                       00003690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00003700
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00003710
           02 FILLER    PIC X(4).                                       00003720
           02 MSCREENI  PIC X(4).                                       00003730
      ***************************************************************** 00003740
      * MAJ DES PRIX ARTICLES ET PRIMES                                 00003750
      ***************************************************************** 00003760
       01   EGG52O REDEFINES EGG52I.                                    00003770
           02 FILLER    PIC X(12).                                      00003780
           02 FILLER    PIC X(2).                                       00003790
           02 MDATJOUA  PIC X.                                          00003800
           02 MDATJOUC  PIC X.                                          00003810
           02 MDATJOUP  PIC X.                                          00003820
           02 MDATJOUH  PIC X.                                          00003830
           02 MDATJOUV  PIC X.                                          00003840
           02 MDATJOUO  PIC X(10).                                      00003850
           02 FILLER    PIC X(2).                                       00003860
           02 MTIMJOUA  PIC X.                                          00003870
           02 MTIMJOUC  PIC X.                                          00003880
           02 MTIMJOUP  PIC X.                                          00003890
           02 MTIMJOUH  PIC X.                                          00003900
           02 MTIMJOUV  PIC X.                                          00003910
           02 MTIMJOUO  PIC X(5).                                       00003920
           02 FILLER    PIC X(2).                                       00003930
           02 MNCODICA  PIC X.                                          00003940
           02 MNCODICC  PIC X.                                          00003950
           02 MNCODICP  PIC X.                                          00003960
           02 MNCODICH  PIC X.                                          00003970
           02 MNCODICV  PIC X.                                          00003980
           02 MNCODICO  PIC X(7).                                       00003990
           02 FILLER    PIC X(2).                                       00004000
           02 MPCHTA    PIC X.                                          00004010
           02 MPCHTC    PIC X.                                          00004020
           02 MPCHTP    PIC X.                                          00004030
           02 MPCHTH    PIC X.                                          00004040
           02 MPCHTV    PIC X.                                          00004050
           02 MPCHTO    PIC X(8).                                       00004060
           02 FILLER    PIC X(2).                                       00004070
           02 MCAPPROA  PIC X.                                          00004080
           02 MCAPPROC  PIC X.                                          00004090
           02 MCAPPROP  PIC X.                                          00004100
           02 MCAPPROH  PIC X.                                          00004110
           02 MCAPPROV  PIC X.                                          00004120
           02 MCAPPROO  PIC X(5).                                       00004130
           02 FILLER    PIC X(2).                                       00004140
           02 MLREFA    PIC X.                                          00004150
           02 MLREFC    PIC X.                                          00004160
           02 MLREFP    PIC X.                                          00004170
           02 MLREFH    PIC X.                                          00004180
           02 MLREFV    PIC X.                                          00004190
           02 MLREFO    PIC X(20).                                      00004200
           02 FILLER    PIC X(2).                                       00004210
           02 MCEXPOA   PIC X.                                          00004220
           02 MCEXPOC   PIC X.                                          00004230
           02 MCEXPOP   PIC X.                                          00004240
           02 MCEXPOH   PIC X.                                          00004250
           02 MCEXPOV   PIC X.                                          00004260
           02 MCEXPOO   PIC X(5).                                       00004270
           02 FILLER    PIC X(2).                                       00004280
           02 MCMARQA   PIC X.                                          00004290
           02 MCMARQC   PIC X.                                          00004300
           02 MCMARQP   PIC X.                                          00004310
           02 MCMARQH   PIC X.                                          00004320
           02 MCMARQV   PIC X.                                          00004330
           02 MCMARQO   PIC X(5).                                       00004340
           02 FILLER    PIC X(2).                                       00004350
           02 MCFAMA    PIC X.                                          00004360
           02 MCFAMC    PIC X.                                          00004370
           02 MCFAMP    PIC X.                                          00004380
           02 MCFAMH    PIC X.                                          00004390
           02 MCFAMV    PIC X.                                          00004400
           02 MCFAMO    PIC X(5).                                       00004410
           02 FILLER    PIC X(2).                                       00004420
           02 MCOMPACA  PIC X.                                          00004430
           02 MCOMPACC  PIC X.                                          00004440
           02 MCOMPACP  PIC X.                                          00004450
           02 MCOMPACH  PIC X.                                          00004460
           02 MCOMPACV  PIC X.                                          00004470
           02 MCOMPACO  PIC X(3).                                       00004480
           02 FILLER    PIC X(2).                                       00004490
           02 MOANATA   PIC X.                                          00004500
           02 MOANATC   PIC X.                                          00004510
           02 MOANATP   PIC X.                                          00004520
           02 MOANATH   PIC X.                                          00004530
           02 MOANATV   PIC X.                                          00004540
           02 MOANATO   PIC X.                                          00004550
           02 FILLER    PIC X(2).                                       00004560
           02 MOAZPA    PIC X.                                          00004570
           02 MOAZPC    PIC X.                                          00004580
           02 MOAZPP    PIC X.                                          00004590
           02 MOAZPH    PIC X.                                          00004600
           02 MOAZPV    PIC X.                                          00004610
           02 MOAZPO    PIC X.                                          00004620
           02 FILLER    PIC X(2).                                       00004630
           02 MDOFDEBA  PIC X.                                          00004640
           02 MDOFDEBC  PIC X.                                          00004650
           02 MDOFDEBP  PIC X.                                          00004660
           02 MDOFDEBH  PIC X.                                          00004670
           02 MDOFDEBV  PIC X.                                          00004680
           02 MDOFDEBO  PIC X(8).                                       00004690
           02 FILLER    PIC X(2).                                       00004700
           02 MDOFFINA  PIC X.                                          00004710
           02 MDOFFINC  PIC X.                                          00004720
           02 MDOFFINP  PIC X.                                          00004730
           02 MDOFFINH  PIC X.                                          00004740
           02 MDOFFINV  PIC X.                                          00004750
           02 MDOFFINO  PIC X(8).                                       00004760
           02 FILLER    PIC X(2).                                       00004770
           02 MPRXREFAA      PIC X.                                     00004780
           02 MPRXREFAC PIC X.                                          00004790
           02 MPRXREFAP PIC X.                                          00004800
           02 MPRXREFAH PIC X.                                          00004810
           02 MPRXREFAV PIC X.                                          00004820
           02 MPRXREFAO      PIC X(9).                                  00004830
           02 FILLER    PIC X(2).                                       00004840
           02 MWIMPAA   PIC X.                                          00004850
           02 MWIMPAC   PIC X.                                          00004860
           02 MWIMPAP   PIC X.                                          00004870
           02 MWIMPAH   PIC X.                                          00004880
           02 MWIMPAV   PIC X.                                          00004890
           02 MWIMPAO   PIC X(3).                                       00004900
           02 FILLER    PIC X(2).                                       00004910
           02 MDDREFAA  PIC X.                                          00004920
           02 MDDREFAC  PIC X.                                          00004930
           02 MDDREFAP  PIC X.                                          00004940
           02 MDDREFAH  PIC X.                                          00004950
           02 MDDREFAV  PIC X.                                          00004960
           02 MDDREFAO  PIC X(8).                                       00004970
           02 FILLER    PIC X(2).                                       00004980
           02 MLIB1A    PIC X.                                          00004990
           02 MLIB1C    PIC X.                                          00005000
           02 MLIB1P    PIC X.                                          00005010
           02 MLIB1H    PIC X.                                          00005020
           02 MLIB1V    PIC X.                                          00005030
           02 MLIB1O    PIC X(2).                                       00005040
           02 FILLER    PIC X(2).                                       00005050
           02 MDFREFAA  PIC X.                                          00005060
           02 MDFREFAC  PIC X.                                          00005070
           02 MDFREFAP  PIC X.                                          00005080
           02 MDFREFAH  PIC X.                                          00005090
           02 MDFREFAV  PIC X.                                          00005100
           02 MDFREFAO  PIC X(8).                                       00005110
           02 FILLER    PIC X(2).                                       00005120
           02 MPRXREFNA      PIC X.                                     00005130
           02 MPRXREFNC PIC X.                                          00005140
           02 MPRXREFNP PIC X.                                          00005150
           02 MPRXREFNH PIC X.                                          00005160
           02 MPRXREFNV PIC X.                                          00005170
           02 MPRXREFNO      PIC X(9).                                  00005180
           02 FILLER    PIC X(2).                                       00005190
           02 MWIMPNA   PIC X.                                          00005200
           02 MWIMPNC   PIC X.                                          00005210
           02 MWIMPNP   PIC X.                                          00005220
           02 MWIMPNH   PIC X.                                          00005230
           02 MWIMPNV   PIC X.                                          00005240
           02 MWIMPNO   PIC X(3).                                       00005250
           02 FILLER    PIC X(2).                                       00005260
           02 MDDREFNA  PIC X.                                          00005270
           02 MDDREFNC  PIC X.                                          00005280
           02 MDDREFNP  PIC X.                                          00005290
           02 MDDREFNH  PIC X.                                          00005300
           02 MDDREFNV  PIC X.                                          00005310
           02 MDDREFNO  PIC X(8).                                       00005320
           02 FILLER    PIC X(2).                                       00005330
           02 MLIB2A    PIC X.                                          00005340
           02 MLIB2C    PIC X.                                          00005350
           02 MLIB2P    PIC X.                                          00005360
           02 MLIB2H    PIC X.                                          00005370
           02 MLIB2V    PIC X.                                          00005380
           02 MLIB2O    PIC X(2).                                       00005390
           02 FILLER    PIC X(2).                                       00005400
           02 MDFREFNA  PIC X.                                          00005410
           02 MDFREFNC  PIC X.                                          00005420
           02 MDFREFNP  PIC X.                                          00005430
           02 MDFREFNH  PIC X.                                          00005440
           02 MDFREFNV  PIC X.                                          00005450
           02 MDFREFNO  PIC X(8).                                       00005460
           02 FILLER    PIC X(2).                                       00005470
           02 MPRXREF2AA     PIC X.                                     00005480
           02 MPRXREF2AC     PIC X.                                     00005490
           02 MPRXREF2AP     PIC X.                                     00005500
           02 MPRXREF2AH     PIC X.                                     00005510
           02 MPRXREF2AV     PIC X.                                     00005520
           02 MPRXREF2AO     PIC X(9).                                  00005530
           02 FILLER    PIC X(2).                                       00005540
           02 MBLOCAA   PIC X.                                          00005550
           02 MBLOCAC   PIC X.                                          00005560
           02 MBLOCAP   PIC X.                                          00005570
           02 MBLOCAH   PIC X.                                          00005580
           02 MBLOCAV   PIC X.                                          00005590
           02 MBLOCAO   PIC X(3).                                       00005600
           02 FILLER    PIC X(2).                                       00005610
           02 MDDREF2AA      PIC X.                                     00005620
           02 MDDREF2AC PIC X.                                          00005630
           02 MDDREF2AP PIC X.                                          00005640
           02 MDDREF2AH PIC X.                                          00005650
           02 MDDREF2AV PIC X.                                          00005660
           02 MDDREF2AO      PIC X(8).                                  00005670
           02 FILLER    PIC X(2).                                       00005680
           02 MLIB3A    PIC X.                                          00005690
           02 MLIB3C    PIC X.                                          00005700
           02 MLIB3P    PIC X.                                          00005710
           02 MLIB3H    PIC X.                                          00005720
           02 MLIB3V    PIC X.                                          00005730
           02 MLIB3O    PIC X(2).                                       00005740
           02 FILLER    PIC X(2).                                       00005750
           02 MDFREF2AA      PIC X.                                     00005760
           02 MDFREF2AC PIC X.                                          00005770
           02 MDFREF2AP PIC X.                                          00005780
           02 MDFREF2AH PIC X.                                          00005790
           02 MDFREF2AV PIC X.                                          00005800
           02 MDFREF2AO      PIC X(8).                                  00005810
           02 FILLER    PIC X(2).                                       00005820
           02 MPRXREF2NA     PIC X.                                     00005830
           02 MPRXREF2NC     PIC X.                                     00005840
           02 MPRXREF2NP     PIC X.                                     00005850
           02 MPRXREF2NH     PIC X.                                     00005860
           02 MPRXREF2NV     PIC X.                                     00005870
           02 MPRXREF2NO     PIC X(9).                                  00005880
           02 FILLER    PIC X(2).                                       00005890
           02 MBLOCNA   PIC X.                                          00005900
           02 MBLOCNC   PIC X.                                          00005910
           02 MBLOCNP   PIC X.                                          00005920
           02 MBLOCNH   PIC X.                                          00005930
           02 MBLOCNV   PIC X.                                          00005940
           02 MBLOCNO   PIC X(3).                                       00005950
           02 FILLER    PIC X(2).                                       00005960
           02 MDDREF2NA      PIC X.                                     00005970
           02 MDDREF2NC PIC X.                                          00005980
           02 MDDREF2NP PIC X.                                          00005990
           02 MDDREF2NH PIC X.                                          00006000
           02 MDDREF2NV PIC X.                                          00006010
           02 MDDREF2NO      PIC X(8).                                  00006020
           02 FILLER    PIC X(2).                                       00006030
           02 MLIB4A    PIC X.                                          00006040
           02 MLIB4C    PIC X.                                          00006050
           02 MLIB4P    PIC X.                                          00006060
           02 MLIB4H    PIC X.                                          00006070
           02 MLIB4V    PIC X.                                          00006080
           02 MLIB4O    PIC X(2).                                       00006090
           02 FILLER    PIC X(2).                                       00006100
           02 MDFREF2NA      PIC X.                                     00006110
           02 MDFREF2NC PIC X.                                          00006120
           02 MDFREF2NP PIC X.                                          00006130
           02 MDFREF2NH PIC X.                                          00006140
           02 MDFREF2NV PIC X.                                          00006150
           02 MDFREF2NO      PIC X(8).                                  00006160
           02 FILLER    PIC X(2).                                       00006170
           02 MNZONMOA  PIC X.                                          00006180
           02 MNZONMOC  PIC X.                                          00006190
           02 MNZONMOP  PIC X.                                          00006200
           02 MNZONMOH  PIC X.                                          00006210
           02 MNZONMOV  PIC X.                                          00006220
           02 MNZONMOO  PIC X(2).                                       00006230
           02 FILLER    PIC X(2).                                       00006240
           02 MPRIMOA   PIC X.                                          00006250
           02 MPRIMOC   PIC X.                                          00006260
           02 MPRIMOP   PIC X.                                          00006270
           02 MPRIMOH   PIC X.                                          00006280
           02 MPRIMOV   PIC X.                                          00006290
           02 MPRIMOO   PIC Z(4)9,99.                                   00006300
           02 FILLER    PIC X(2).                                       00006310
           02 MPCOMMOA  PIC X.                                          00006320
           02 MPCOMMOC  PIC X.                                          00006330
           02 MPCOMMOP  PIC X.                                          00006340
           02 MPCOMMOH  PIC X.                                          00006350
           02 MPCOMMOV  PIC X.                                          00006360
           02 MPCOMMOO  PIC Z(3)9,9.                                    00006370
           02 FILLER    PIC X(2).                                       00006380
           02 MINFAMOA  PIC X.                                          00006390
           02 MINFAMOC  PIC X.                                          00006400
           02 MINFAMOP  PIC X.                                          00006410
           02 MINFAMOH  PIC X.                                          00006420
           02 MINFAMOV  PIC X.                                          00006430
           02 MINFAMOO  PIC ZZ9.                                        00006440
           02 FILLER    PIC X(2).                                       00006450
           02 MDPRIMOA  PIC X.                                          00006460
           02 MDPRIMOC  PIC X.                                          00006470
           02 MDPRIMOP  PIC X.                                          00006480
           02 MDPRIMOH  PIC X.                                          00006490
           02 MDPRIMOV  PIC X.                                          00006500
           02 MDPRIMOO  PIC X(8).                                       00006510
           02 FILLER    PIC X(2).                                       00006520
           02 MPMBUMOA  PIC X.                                          00006530
           02 MPMBUMOC  PIC X.                                          00006540
           02 MPMBUMOP  PIC X.                                          00006550
           02 MPMBUMOH  PIC X.                                          00006560
           02 MPMBUMOV  PIC X.                                          00006570
           02 MPMBUMOO  PIC X(8).                                       00006580
           02 FILLER    PIC X(2).                                       00006590
           02 MQTMAMOA  PIC X.                                          00006600
           02 MQTMAMOC  PIC X.                                          00006610
           02 MQTMAMOP  PIC X.                                          00006620
           02 MQTMAMOH  PIC X.                                          00006630
           02 MQTMAMOV  PIC X.                                          00006640
           02 MQTMAMOO  PIC --9,9.                                      00006650
           02 FILLER    PIC X(2).                                       00006660
           02 MFMILMOA  PIC X.                                          00006670
           02 MFMILMOC  PIC X.                                          00006680
           02 MFMILMOP  PIC X.                                          00006690
           02 MFMILMOH  PIC X.                                          00006700
           02 MFMILMOV  PIC X.                                          00006710
           02 MFMILMOO  PIC ZZ9,99.                                     00006720
           02 FILLER    PIC X(2).                                       00006730
           02 MLCOMMOA  PIC X.                                          00006740
           02 MLCOMMOC  PIC X.                                          00006750
           02 MLCOMMOP  PIC X.                                          00006760
           02 MLCOMMOH  PIC X.                                          00006770
           02 MLCOMMOV  PIC X.                                          00006780
           02 MLCOMMOO  PIC X(10).                                      00006790
           02 FILLER    PIC X(2).                                       00006800
           02 MNZONMAA  PIC X.                                          00006810
           02 MNZONMAC  PIC X.                                          00006820
           02 MNZONMAP  PIC X.                                          00006830
           02 MNZONMAH  PIC X.                                          00006840
           02 MNZONMAV  PIC X.                                          00006850
           02 MNZONMAO  PIC X(2).                                       00006860
           02 FILLER    PIC X(2).                                       00006870
           02 MPRIMACA  PIC X.                                          00006880
           02 MPRIMACC  PIC X.                                          00006890
           02 MPRIMACP  PIC X.                                          00006900
           02 MPRIMACH  PIC X.                                          00006910
           02 MPRIMACV  PIC X.                                          00006920
           02 MPRIMACO  PIC Z(4)9,99.                                   00006930
           02 FILLER    PIC X(2).                                       00006940
           02 MPCOMMAA  PIC X.                                          00006950
           02 MPCOMMAC  PIC X.                                          00006960
           02 MPCOMMAP  PIC X.                                          00006970
           02 MPCOMMAH  PIC X.                                          00006980
           02 MPCOMMAV  PIC X.                                          00006990
           02 MPCOMMAO  PIC Z(3)9,9.                                    00007000
           02 FILLER    PIC X(2).                                       00007010
           02 MINFAMAA  PIC X.                                          00007020
           02 MINFAMAC  PIC X.                                          00007030
           02 MINFAMAP  PIC X.                                          00007040
           02 MINFAMAH  PIC X.                                          00007050
           02 MINFAMAV  PIC X.                                          00007060
           02 MINFAMAO  PIC ZZ9.                                        00007070
           02 FILLER    PIC X(2).                                       00007080
           02 MDPRIMAA  PIC X.                                          00007090
           02 MDPRIMAC  PIC X.                                          00007100
           02 MDPRIMAP  PIC X.                                          00007110
           02 MDPRIMAH  PIC X.                                          00007120
           02 MDPRIMAV  PIC X.                                          00007130
           02 MDPRIMAO  PIC X(8).                                       00007140
           02 FILLER    PIC X(2).                                       00007150
           02 MPMBUMAA  PIC X.                                          00007160
           02 MPMBUMAC  PIC X.                                          00007170
           02 MPMBUMAP  PIC X.                                          00007180
           02 MPMBUMAH  PIC X.                                          00007190
           02 MPMBUMAV  PIC X.                                          00007200
           02 MPMBUMAO  PIC X(8).                                       00007210
           02 FILLER    PIC X(2).                                       00007220
           02 MQTMAMAA  PIC X.                                          00007230
           02 MQTMAMAC  PIC X.                                          00007240
           02 MQTMAMAP  PIC X.                                          00007250
           02 MQTMAMAH  PIC X.                                          00007260
           02 MQTMAMAV  PIC X.                                          00007270
           02 MQTMAMAO  PIC --9,9.                                      00007280
           02 FILLER    PIC X(2).                                       00007290
           02 MFMILMAA  PIC X.                                          00007300
           02 MFMILMAC  PIC X.                                          00007310
           02 MFMILMAP  PIC X.                                          00007320
           02 MFMILMAH  PIC X.                                          00007330
           02 MFMILMAV  PIC X.                                          00007340
           02 MFMILMAO  PIC ZZ9,99.                                     00007350
           02 FILLER    PIC X(2).                                       00007360
           02 MLCOMMAA  PIC X.                                          00007370
           02 MLCOMMAC  PIC X.                                          00007380
           02 MLCOMMAP  PIC X.                                          00007390
           02 MLCOMMAH  PIC X.                                          00007400
           02 MLCOMMAV  PIC X.                                          00007410
           02 MLCOMMAO  PIC X(10).                                      00007420
           02 FILLER    PIC X(2).                                       00007430
           02 MNZONMNA  PIC X.                                          00007440
           02 MNZONMNC  PIC X.                                          00007450
           02 MNZONMNP  PIC X.                                          00007460
           02 MNZONMNH  PIC X.                                          00007470
           02 MNZONMNV  PIC X.                                          00007480
           02 MNZONMNO  PIC X(2).                                       00007490
           02 FILLER    PIC X(2).                                       00007500
           02 MPRIMNOA  PIC X.                                          00007510
           02 MPRIMNOC  PIC X.                                          00007520
           02 MPRIMNOP  PIC X.                                          00007530
           02 MPRIMNOH  PIC X.                                          00007540
           02 MPRIMNOV  PIC X.                                          00007550
           02 MPRIMNOO  PIC Z(4)9,99.                                   00007560
           02 FILLER    PIC X(2).                                       00007570
           02 MPCOMMNA  PIC X.                                          00007580
           02 MPCOMMNC  PIC X.                                          00007590
           02 MPCOMMNP  PIC X.                                          00007600
           02 MPCOMMNH  PIC X.                                          00007610
           02 MPCOMMNV  PIC X.                                          00007620
           02 MPCOMMNO  PIC Z(3)9,9.                                    00007630
           02 FILLER    PIC X(2).                                       00007640
           02 MINFAMNA  PIC X.                                          00007650
           02 MINFAMNC  PIC X.                                          00007660
           02 MINFAMNP  PIC X.                                          00007670
           02 MINFAMNH  PIC X.                                          00007680
           02 MINFAMNV  PIC X.                                          00007690
           02 MINFAMNO  PIC ZZ9.                                        00007700
           02 FILLER    PIC X(2).                                       00007710
           02 MDPRIMNA  PIC X.                                          00007720
           02 MDPRIMNC  PIC X.                                          00007730
           02 MDPRIMNP  PIC X.                                          00007740
           02 MDPRIMNH  PIC X.                                          00007750
           02 MDPRIMNV  PIC X.                                          00007760
           02 MDPRIMNO  PIC X(8).                                       00007770
           02 FILLER    PIC X(2).                                       00007780
           02 MPMBUMNA  PIC X.                                          00007790
           02 MPMBUMNC  PIC X.                                          00007800
           02 MPMBUMNP  PIC X.                                          00007810
           02 MPMBUMNH  PIC X.                                          00007820
           02 MPMBUMNV  PIC X.                                          00007830
           02 MPMBUMNO  PIC X(8).                                       00007840
           02 FILLER    PIC X(2).                                       00007850
           02 MQTMAMNA  PIC X.                                          00007860
           02 MQTMAMNC  PIC X.                                          00007870
           02 MQTMAMNP  PIC X.                                          00007880
           02 MQTMAMNH  PIC X.                                          00007890
           02 MQTMAMNV  PIC X.                                          00007900
           02 MQTMAMNO  PIC --9,9.                                      00007910
           02 FILLER    PIC X(2).                                       00007920
           02 MFMILMNA  PIC X.                                          00007930
           02 MFMILMNC  PIC X.                                          00007940
           02 MFMILMNP  PIC X.                                          00007950
           02 MFMILMNH  PIC X.                                          00007960
           02 MFMILMNV  PIC X.                                          00007970
           02 MFMILMNO  PIC ZZ9,99.                                     00007980
           02 FILLER    PIC X(2).                                       00007990
           02 MLCOMMNA  PIC X.                                          00008000
           02 MLCOMMNC  PIC X.                                          00008010
           02 MLCOMMNP  PIC X.                                          00008020
           02 MLCOMMNH  PIC X.                                          00008030
           02 MLCOMMNV  PIC X.                                          00008040
           02 MLCOMMNO  PIC X(10).                                      00008050
           02 FILLER    PIC X(2).                                       00008060
           02 MNPAGEA   PIC X.                                          00008070
           02 MNPAGEC   PIC X.                                          00008080
           02 MNPAGEP   PIC X.                                          00008090
           02 MNPAGEH   PIC X.                                          00008100
           02 MNPAGEV   PIC X.                                          00008110
           02 MNPAGEO   PIC X(2).                                       00008120
           02 FILLER    PIC X(2).                                       00008130
           02 MNPAGMA   PIC X.                                          00008140
           02 MNPAGMC   PIC X.                                          00008150
           02 MNPAGMP   PIC X.                                          00008160
           02 MNPAGMH   PIC X.                                          00008170
           02 MNPAGMV   PIC X.                                          00008180
           02 MNPAGMO   PIC X(2).                                       00008190
           02 FILLER    PIC X(2).                                       00008200
           02 MNZONEXA  PIC X.                                          00008210
           02 MNZONEXC  PIC X.                                          00008220
           02 MNZONEXP  PIC X.                                          00008230
           02 MNZONEXH  PIC X.                                          00008240
           02 MNZONEXV  PIC X.                                          00008250
           02 MNZONEXO  PIC X(2).                                       00008260
           02 FILLER    PIC X(2).                                       00008270
           02 MPRIEXA   PIC X.                                          00008280
           02 MPRIEXC   PIC X.                                          00008290
           02 MPRIEXP   PIC X.                                          00008300
           02 MPRIEXH   PIC X.                                          00008310
           02 MPRIEXV   PIC X.                                          00008320
           02 MPRIEXO   PIC X(8).                                       00008330
           02 FILLER    PIC X(2).                                       00008340
           02 MCOMEXA   PIC X.                                          00008350
           02 MCOMEXC   PIC X.                                          00008360
           02 MCOMEXP   PIC X.                                          00008370
           02 MCOMEXH   PIC X.                                          00008380
           02 MCOMEXV   PIC X.                                          00008390
           02 MCOMEXO   PIC Z(3)9,9.                                    00008400
           02 FILLER    PIC X(2).                                       00008410
           02 MDPRIEXA  PIC X.                                          00008420
           02 MDPRIEXC  PIC X.                                          00008430
           02 MDPRIEXP  PIC X.                                          00008440
           02 MDPRIEXH  PIC X.                                          00008450
           02 MDPRIEXV  PIC X.                                          00008460
           02 MDPRIEXO  PIC X(8).                                       00008470
           02 FILLER    PIC X(2).                                       00008480
           02 MPMBUEXA  PIC X.                                          00008490
           02 MPMBUEXC  PIC X.                                          00008500
           02 MPMBUEXP  PIC X.                                          00008510
           02 MPMBUEXH  PIC X.                                          00008520
           02 MPMBUEXV  PIC X.                                          00008530
           02 MPMBUEXO  PIC X(8).                                       00008540
           02 FILLER    PIC X(2).                                       00008550
           02 MQTMAEXA  PIC X.                                          00008560
           02 MQTMAEXC  PIC X.                                          00008570
           02 MQTMAEXP  PIC X.                                          00008580
           02 MQTMAEXH  PIC X.                                          00008590
           02 MQTMAEXV  PIC X.                                          00008600
           02 MQTMAEXO  PIC --9,9.                                      00008610
           02 FILLER    PIC X(2).                                       00008620
           02 MFMILEXA  PIC X.                                          00008630
           02 MFMILEXC  PIC X.                                          00008640
           02 MFMILEXP  PIC X.                                          00008650
           02 MFMILEXH  PIC X.                                          00008660
           02 MFMILEXV  PIC X.                                          00008670
           02 MFMILEXO  PIC ZZ9,99.                                     00008680
           02 FILLER    PIC X(2).                                       00008690
           02 MLCOMEXA  PIC X.                                          00008700
           02 MLCOMEXC  PIC X.                                          00008710
           02 MLCOMEXP  PIC X.                                          00008720
           02 MLCOMEXH  PIC X.                                          00008730
           02 MLCOMEXV  PIC X.                                          00008740
           02 MLCOMEXO  PIC X(10).                                      00008750
           02 FILLER    PIC X(2).                                       00008760
           02 MDFINEFA  PIC X.                                          00008770
           02 MDFINEFC  PIC X.                                          00008780
           02 MDFINEFP  PIC X.                                          00008790
           02 MDFINEFH  PIC X.                                          00008800
           02 MDFINEFV  PIC X.                                          00008810
           02 MDFINEFO  PIC X(8).                                       00008820
           02 FILLER    PIC X(2).                                       00008830
           02 MNCONCA   PIC X.                                          00008840
           02 MNCONCC   PIC X.                                          00008850
           02 MNCONCP   PIC X.                                          00008860
           02 MNCONCH   PIC X.                                          00008870
           02 MNCONCV   PIC X.                                          00008880
           02 MNCONCO   PIC X(4).                                       00008890
           02 FILLER    PIC X(2).                                       00008900
           02 MLCONCA   PIC X.                                          00008910
           02 MLCONCC   PIC X.                                          00008920
           02 MLCONCP   PIC X.                                          00008930
           02 MLCONCH   PIC X.                                          00008940
           02 MLCONCV   PIC X.                                          00008950
           02 MLCONCO   PIC X(15).                                      00008960
           02 FILLER    PIC X(2).                                       00008970
           02 MLORIGA   PIC X.                                          00008980
           02 MLORIGC   PIC X.                                          00008990
           02 MLORIGP   PIC X.                                          00009000
           02 MLORIGH   PIC X.                                          00009010
           02 MLORIGV   PIC X.                                          00009020
           02 MLORIGO   PIC X(12).                                      00009030
           02 FILLER    PIC X(2).                                       00009040
           02 MNLIEU1A  PIC X.                                          00009050
           02 MNLIEU1C  PIC X.                                          00009060
           02 MNLIEU1P  PIC X.                                          00009070
           02 MNLIEU1H  PIC X.                                          00009080
           02 MNLIEU1V  PIC X.                                          00009090
           02 MNLIEU1O  PIC X(3).                                       00009100
           02 FILLER    PIC X(2).                                       00009110
           02 MNLIEU2A  PIC X.                                          00009120
           02 MNLIEU2C  PIC X.                                          00009130
           02 MNLIEU2P  PIC X.                                          00009140
           02 MNLIEU2H  PIC X.                                          00009150
           02 MNLIEU2V  PIC X.                                          00009160
           02 MNLIEU2O  PIC X(3).                                       00009170
           02 FILLER    PIC X(2).                                       00009180
           02 MNLIEU3A  PIC X.                                          00009190
           02 MNLIEU3C  PIC X.                                          00009200
           02 MNLIEU3P  PIC X.                                          00009210
           02 MNLIEU3H  PIC X.                                          00009220
           02 MNLIEU3V  PIC X.                                          00009230
           02 MNLIEU3O  PIC X(3).                                       00009240
           02 FILLER    PIC X(2).                                       00009250
           02 MNLIEU4A  PIC X.                                          00009260
           02 MNLIEU4C  PIC X.                                          00009270
           02 MNLIEU4P  PIC X.                                          00009280
           02 MNLIEU4H  PIC X.                                          00009290
           02 MNLIEU4V  PIC X.                                          00009300
           02 MNLIEU4O  PIC X(3).                                       00009310
           02 FILLER    PIC X(2).                                       00009320
           02 MNLIEU5A  PIC X.                                          00009330
           02 MNLIEU5C  PIC X.                                          00009340
           02 MNLIEU5P  PIC X.                                          00009350
           02 MNLIEU5H  PIC X.                                          00009360
           02 MNLIEU5V  PIC X.                                          00009370
           02 MNLIEU5O  PIC X(3).                                       00009380
           02 FILLER    PIC X(2).                                       00009390
           02 MNLIEU6A  PIC X.                                          00009400
           02 MNLIEU6C  PIC X.                                          00009410
           02 MNLIEU6P  PIC X.                                          00009420
           02 MNLIEU6H  PIC X.                                          00009430
           02 MNLIEU6V  PIC X.                                          00009440
           02 MNLIEU6O  PIC X(3).                                       00009450
           02 FILLER    PIC X(2).                                       00009460
           02 MNLIEU7A  PIC X.                                          00009470
           02 MNLIEU7C  PIC X.                                          00009480
           02 MNLIEU7P  PIC X.                                          00009490
           02 MNLIEU7H  PIC X.                                          00009500
           02 MNLIEU7V  PIC X.                                          00009510
           02 MNLIEU7O  PIC X(3).                                       00009520
           02 FILLER    PIC X(2).                                       00009530
           02 MNLIEU8A  PIC X.                                          00009540
           02 MNLIEU8C  PIC X.                                          00009550
           02 MNLIEU8P  PIC X.                                          00009560
           02 MNLIEU8H  PIC X.                                          00009570
           02 MNLIEU8V  PIC X.                                          00009580
           02 MNLIEU8O  PIC X(3).                                       00009590
           02 FILLER    PIC X(2).                                       00009600
           02 MNLIEU9A  PIC X.                                          00009610
           02 MNLIEU9C  PIC X.                                          00009620
           02 MNLIEU9P  PIC X.                                          00009630
           02 MNLIEU9H  PIC X.                                          00009640
           02 MNLIEU9V  PIC X.                                          00009650
           02 MNLIEU9O  PIC X(3).                                       00009660
           02 FILLER    PIC X(2).                                       00009670
           02 MNLIEU10A      PIC X.                                     00009680
           02 MNLIEU10C PIC X.                                          00009690
           02 MNLIEU10P PIC X.                                          00009700
           02 MNLIEU10H PIC X.                                          00009710
           02 MNLIEU10V PIC X.                                          00009720
           02 MNLIEU10O      PIC X(3).                                  00009730
           02 FILLER    PIC X(2).                                       00009740
           02 MNCODISA  PIC X.                                          00009750
           02 MNCODISC  PIC X.                                          00009760
           02 MNCODISP  PIC X.                                          00009770
           02 MNCODISH  PIC X.                                          00009780
           02 MNCODISV  PIC X.                                          00009790
           02 MNCODISO  PIC X(7).                                       00009800
           02 FILLER    PIC X(2).                                       00009810
           02 MZONCMDA  PIC X.                                          00009820
           02 MZONCMDC  PIC X.                                          00009830
           02 MZONCMDP  PIC X.                                          00009840
           02 MZONCMDH  PIC X.                                          00009850
           02 MZONCMDV  PIC X.                                          00009860
           02 MZONCMDO  PIC X(15).                                      00009870
           02 FILLER    PIC X(2).                                       00009880
           02 MLIBERRA  PIC X.                                          00009890
           02 MLIBERRC  PIC X.                                          00009900
           02 MLIBERRP  PIC X.                                          00009910
           02 MLIBERRH  PIC X.                                          00009920
           02 MLIBERRV  PIC X.                                          00009930
           02 MLIBERRO  PIC X(58).                                      00009940
           02 FILLER    PIC X(2).                                       00009950
           02 MCODTRAA  PIC X.                                          00009960
           02 MCODTRAC  PIC X.                                          00009970
           02 MCODTRAP  PIC X.                                          00009980
           02 MCODTRAH  PIC X.                                          00009990
           02 MCODTRAV  PIC X.                                          00010000
           02 MCODTRAO  PIC X(4).                                       00010010
           02 FILLER    PIC X(2).                                       00010020
           02 MCICSA    PIC X.                                          00010030
           02 MCICSC    PIC X.                                          00010040
           02 MCICSP    PIC X.                                          00010050
           02 MCICSH    PIC X.                                          00010060
           02 MCICSV    PIC X.                                          00010070
           02 MCICSO    PIC X(5).                                       00010080
           02 FILLER    PIC X(2).                                       00010090
           02 MNETNAMA  PIC X.                                          00010100
           02 MNETNAMC  PIC X.                                          00010110
           02 MNETNAMP  PIC X.                                          00010120
           02 MNETNAMH  PIC X.                                          00010130
           02 MNETNAMV  PIC X.                                          00010140
           02 MNETNAMO  PIC X(8).                                       00010150
           02 FILLER    PIC X(2).                                       00010160
           02 MSCREENA  PIC X.                                          00010170
           02 MSCREENC  PIC X.                                          00010180
           02 MSCREENP  PIC X.                                          00010190
           02 MSCREENH  PIC X.                                          00010200
           02 MSCREENV  PIC X.                                          00010210
           02 MSCREENO  PIC X(4).                                       00010220
                                                                                
