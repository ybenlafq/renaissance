      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * LISTE DES PRIX EXCEPTIONNELS                                    00000020
      ***************************************************************** 00000030
       01   EGG59I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPI     PIC X(4).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCL     COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MSOCL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSOCF     PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSOCI     PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIDATEL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MIDATEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MIDATEF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MIDATEI   PIC X(8).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIZPL     COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MIZPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MIZPF     PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MIZPI     PIC X(2).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIMAGL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MIMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MIMAGF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MIMAGI    PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MICODICL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MICODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MICODICF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MICODICI  PIC X(7).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MICHEFPL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MICHEFPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MICHEFPF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MICHEFPI  PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MICRAYONL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MICRAYONL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MICRAYONF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MICRAYONI      PIC X(5).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MICMARQL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MICMARQL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MICMARQF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MICMARQI  PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MICONCL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MICONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MICONCF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MICONCI   PIC X(4).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIMOUCHARDL    COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MIMOUCHARDL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MIMOUCHARDF    PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MIMOUCHARDI    PIC X.                                     00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLE2L     COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MLIBELLE2L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIBELLE2F     PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLIBELLE2I     PIC X(17).                                 00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLE1L     COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MLIBELLE1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIBELLE1F     PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLIBELLE1I     PIC X(20).                                 00000690
           02 MLISTEI OCCURS   14 TIMES .                               00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZPL    COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MZPL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MZPF    PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MZPI    PIC X(2).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMAGL   COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MMAGL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MMAGF   PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MMAGI   PIC X(3).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODICL      COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MCODICL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCODICF      PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MCODICI      PIC X(7).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCHEFPL      COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MCHEFPL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCHEFPF      PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MCHEFPI      PIC X(5).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000870
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MCFAMI  PIC X(5).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MCMARQI      PIC X(5).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MLREFI  PIC X(20).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCONCL  COMP PIC S9(4).                                 00000990
      *--                                                                       
             03 MCONCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCONCF  PIC X.                                          00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MCONCI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMOUCHARDL   COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MMOUCHARDL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MMOUCHARDF   PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MMOUCHARDI   PIC X.                                     00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MDEFFETI     PIC X(8).                                  00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATFINL     COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MDATFINL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDATFINF     PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MDATFINI     PIC X(8).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MLIBERRI  PIC X(74).                                      00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MCODTRAI  PIC X(4).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MCICSI    PIC X(5).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MNETNAMI  PIC X(8).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MSCREENI  PIC X(4).                                       00001340
      ***************************************************************** 00001350
      * LISTE DES PRIX EXCEPTIONNELS                                    00001360
      ***************************************************************** 00001370
       01   EGG59O REDEFINES EGG59I.                                    00001380
           02 FILLER    PIC X(12).                                      00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MDATJOUA  PIC X.                                          00001410
           02 MDATJOUC  PIC X.                                          00001420
           02 MDATJOUP  PIC X.                                          00001430
           02 MDATJOUH  PIC X.                                          00001440
           02 MDATJOUV  PIC X.                                          00001450
           02 MDATJOUO  PIC X(10).                                      00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MTIMJOUA  PIC X.                                          00001480
           02 MTIMJOUC  PIC X.                                          00001490
           02 MTIMJOUP  PIC X.                                          00001500
           02 MTIMJOUH  PIC X.                                          00001510
           02 MTIMJOUV  PIC X.                                          00001520
           02 MTIMJOUO  PIC X(5).                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MNPAGEA   PIC X.                                          00001550
           02 MNPAGEC   PIC X.                                          00001560
           02 MNPAGEP   PIC X.                                          00001570
           02 MNPAGEH   PIC X.                                          00001580
           02 MNPAGEV   PIC X.                                          00001590
           02 MNPAGEO   PIC X(4).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MNBPA     PIC X.                                          00001620
           02 MNBPC     PIC X.                                          00001630
           02 MNBPP     PIC X.                                          00001640
           02 MNBPH     PIC X.                                          00001650
           02 MNBPV     PIC X.                                          00001660
           02 MNBPO     PIC X(4).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MSOCA     PIC X.                                          00001690
           02 MSOCC     PIC X.                                          00001700
           02 MSOCP     PIC X.                                          00001710
           02 MSOCH     PIC X.                                          00001720
           02 MSOCV     PIC X.                                          00001730
           02 MSOCO     PIC X(3).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MIDATEA   PIC X.                                          00001760
           02 MIDATEC   PIC X.                                          00001770
           02 MIDATEP   PIC X.                                          00001780
           02 MIDATEH   PIC X.                                          00001790
           02 MIDATEV   PIC X.                                          00001800
           02 MIDATEO   PIC X(8).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MIZPA     PIC X.                                          00001830
           02 MIZPC     PIC X.                                          00001840
           02 MIZPP     PIC X.                                          00001850
           02 MIZPH     PIC X.                                          00001860
           02 MIZPV     PIC X.                                          00001870
           02 MIZPO     PIC X(2).                                       00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MIMAGA    PIC X.                                          00001900
           02 MIMAGC    PIC X.                                          00001910
           02 MIMAGP    PIC X.                                          00001920
           02 MIMAGH    PIC X.                                          00001930
           02 MIMAGV    PIC X.                                          00001940
           02 MIMAGO    PIC X(3).                                       00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MICODICA  PIC X.                                          00001970
           02 MICODICC  PIC X.                                          00001980
           02 MICODICP  PIC X.                                          00001990
           02 MICODICH  PIC X.                                          00002000
           02 MICODICV  PIC X.                                          00002010
           02 MICODICO  PIC X(7).                                       00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MICHEFPA  PIC X.                                          00002040
           02 MICHEFPC  PIC X.                                          00002050
           02 MICHEFPP  PIC X.                                          00002060
           02 MICHEFPH  PIC X.                                          00002070
           02 MICHEFPV  PIC X.                                          00002080
           02 MICHEFPO  PIC X(5).                                       00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MICRAYONA      PIC X.                                     00002110
           02 MICRAYONC PIC X.                                          00002120
           02 MICRAYONP PIC X.                                          00002130
           02 MICRAYONH PIC X.                                          00002140
           02 MICRAYONV PIC X.                                          00002150
           02 MICRAYONO      PIC X(5).                                  00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MICMARQA  PIC X.                                          00002180
           02 MICMARQC  PIC X.                                          00002190
           02 MICMARQP  PIC X.                                          00002200
           02 MICMARQH  PIC X.                                          00002210
           02 MICMARQV  PIC X.                                          00002220
           02 MICMARQO  PIC X(5).                                       00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MICONCA   PIC X.                                          00002250
           02 MICONCC   PIC X.                                          00002260
           02 MICONCP   PIC X.                                          00002270
           02 MICONCH   PIC X.                                          00002280
           02 MICONCV   PIC X.                                          00002290
           02 MICONCO   PIC X(4).                                       00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MIMOUCHARDA    PIC X.                                     00002320
           02 MIMOUCHARDC    PIC X.                                     00002330
           02 MIMOUCHARDP    PIC X.                                     00002340
           02 MIMOUCHARDH    PIC X.                                     00002350
           02 MIMOUCHARDV    PIC X.                                     00002360
           02 MIMOUCHARDO    PIC X.                                     00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MLIBELLE2A     PIC X.                                     00002390
           02 MLIBELLE2C     PIC X.                                     00002400
           02 MLIBELLE2P     PIC X.                                     00002410
           02 MLIBELLE2H     PIC X.                                     00002420
           02 MLIBELLE2V     PIC X.                                     00002430
           02 MLIBELLE2O     PIC X(17).                                 00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MLIBELLE1A     PIC X.                                     00002460
           02 MLIBELLE1C     PIC X.                                     00002470
           02 MLIBELLE1P     PIC X.                                     00002480
           02 MLIBELLE1H     PIC X.                                     00002490
           02 MLIBELLE1V     PIC X.                                     00002500
           02 MLIBELLE1O     PIC X(20).                                 00002510
           02 MLISTEO OCCURS   14 TIMES .                               00002520
             03 FILLER       PIC X(2).                                  00002530
             03 MZPA    PIC X.                                          00002540
             03 MZPC    PIC X.                                          00002550
             03 MZPP    PIC X.                                          00002560
             03 MZPH    PIC X.                                          00002570
             03 MZPV    PIC X.                                          00002580
             03 MZPO    PIC X(2).                                       00002590
             03 FILLER       PIC X(2).                                  00002600
             03 MMAGA   PIC X.                                          00002610
             03 MMAGC   PIC X.                                          00002620
             03 MMAGP   PIC X.                                          00002630
             03 MMAGH   PIC X.                                          00002640
             03 MMAGV   PIC X.                                          00002650
             03 MMAGO   PIC X(3).                                       00002660
             03 FILLER       PIC X(2).                                  00002670
             03 MCODICA      PIC X.                                     00002680
             03 MCODICC PIC X.                                          00002690
             03 MCODICP PIC X.                                          00002700
             03 MCODICH PIC X.                                          00002710
             03 MCODICV PIC X.                                          00002720
             03 MCODICO      PIC X(7).                                  00002730
             03 FILLER       PIC X(2).                                  00002740
             03 MCHEFPA      PIC X.                                     00002750
             03 MCHEFPC PIC X.                                          00002760
             03 MCHEFPP PIC X.                                          00002770
             03 MCHEFPH PIC X.                                          00002780
             03 MCHEFPV PIC X.                                          00002790
             03 MCHEFPO      PIC X(5).                                  00002800
             03 FILLER       PIC X(2).                                  00002810
             03 MCFAMA  PIC X.                                          00002820
             03 MCFAMC  PIC X.                                          00002830
             03 MCFAMP  PIC X.                                          00002840
             03 MCFAMH  PIC X.                                          00002850
             03 MCFAMV  PIC X.                                          00002860
             03 MCFAMO  PIC X(5).                                       00002870
             03 FILLER       PIC X(2).                                  00002880
             03 MCMARQA      PIC X.                                     00002890
             03 MCMARQC PIC X.                                          00002900
             03 MCMARQP PIC X.                                          00002910
             03 MCMARQH PIC X.                                          00002920
             03 MCMARQV PIC X.                                          00002930
             03 MCMARQO      PIC X(5).                                  00002940
             03 FILLER       PIC X(2).                                  00002950
             03 MLREFA  PIC X.                                          00002960
             03 MLREFC  PIC X.                                          00002970
             03 MLREFP  PIC X.                                          00002980
             03 MLREFH  PIC X.                                          00002990
             03 MLREFV  PIC X.                                          00003000
             03 MLREFO  PIC X(20).                                      00003010
             03 FILLER       PIC X(2).                                  00003020
             03 MCONCA  PIC X.                                          00003030
             03 MCONCC  PIC X.                                          00003040
             03 MCONCP  PIC X.                                          00003050
             03 MCONCH  PIC X.                                          00003060
             03 MCONCV  PIC X.                                          00003070
             03 MCONCO  PIC X(4).                                       00003080
             03 FILLER       PIC X(2).                                  00003090
             03 MMOUCHARDA   PIC X.                                     00003100
             03 MMOUCHARDC   PIC X.                                     00003110
             03 MMOUCHARDP   PIC X.                                     00003120
             03 MMOUCHARDH   PIC X.                                     00003130
             03 MMOUCHARDV   PIC X.                                     00003140
             03 MMOUCHARDO   PIC X.                                     00003150
             03 FILLER       PIC X(2).                                  00003160
             03 MDEFFETA     PIC X.                                     00003170
             03 MDEFFETC     PIC X.                                     00003180
             03 MDEFFETP     PIC X.                                     00003190
             03 MDEFFETH     PIC X.                                     00003200
             03 MDEFFETV     PIC X.                                     00003210
             03 MDEFFETO     PIC X(8).                                  00003220
             03 FILLER       PIC X(2).                                  00003230
             03 MDATFINA     PIC X.                                     00003240
             03 MDATFINC     PIC X.                                     00003250
             03 MDATFINP     PIC X.                                     00003260
             03 MDATFINH     PIC X.                                     00003270
             03 MDATFINV     PIC X.                                     00003280
             03 MDATFINO     PIC X(8).                                  00003290
           02 FILLER    PIC X(2).                                       00003300
           02 MLIBERRA  PIC X.                                          00003310
           02 MLIBERRC  PIC X.                                          00003320
           02 MLIBERRP  PIC X.                                          00003330
           02 MLIBERRH  PIC X.                                          00003340
           02 MLIBERRV  PIC X.                                          00003350
           02 MLIBERRO  PIC X(74).                                      00003360
           02 FILLER    PIC X(2).                                       00003370
           02 MCODTRAA  PIC X.                                          00003380
           02 MCODTRAC  PIC X.                                          00003390
           02 MCODTRAP  PIC X.                                          00003400
           02 MCODTRAH  PIC X.                                          00003410
           02 MCODTRAV  PIC X.                                          00003420
           02 MCODTRAO  PIC X(4).                                       00003430
           02 FILLER    PIC X(2).                                       00003440
           02 MCICSA    PIC X.                                          00003450
           02 MCICSC    PIC X.                                          00003460
           02 MCICSP    PIC X.                                          00003470
           02 MCICSH    PIC X.                                          00003480
           02 MCICSV    PIC X.                                          00003490
           02 MCICSO    PIC X(5).                                       00003500
           02 FILLER    PIC X(2).                                       00003510
           02 MNETNAMA  PIC X.                                          00003520
           02 MNETNAMC  PIC X.                                          00003530
           02 MNETNAMP  PIC X.                                          00003540
           02 MNETNAMH  PIC X.                                          00003550
           02 MNETNAMV  PIC X.                                          00003560
           02 MNETNAMO  PIC X(8).                                       00003570
           02 FILLER    PIC X(2).                                       00003580
           02 MSCREENA  PIC X.                                          00003590
           02 MSCREENC  PIC X.                                          00003600
           02 MSCREENP  PIC X.                                          00003610
           02 MSCREENH  PIC X.                                          00003620
           02 MSCREENV  PIC X.                                          00003630
           02 MSCREENO  PIC X(4).                                       00003640
                                                                                
