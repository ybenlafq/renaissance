      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *===============================================================* 00000010
      *                                                               * 00000020
      *      TS ASSOCIE A LA TRANSACTION GG19                         * 00000030
      *      CONTIENT LES RENSEIGNEMENTS POUR CHAQUE ZONE DE PRIX     * 00000040
      *                                                               * 00000050
      *                                                               * 00000060
      *      NOM: 'GG19' + EIBTRMID                                   * 00000070
      *                                                               * 00000080
      *===============================================================* 00000090
                                                                        00000100
       01  TS-GG19.                                                     00000110
           02 TS-GG19-NOM.                                              00000120
              04 TS-GG19-GG19            PIC X(4)         VALUE 'GG19'. 00000130
              04 TS-GG19-TRMID           PIC X(4).                      00000140
           02 TS-GG19-LONG               PIC S9(5) COMP-3 VALUE +411.   00000150
      *                                                                 00000160
      *                                                                 00000170
           02 TS-GG19-DONNEES.                                          00000180
              04 TS-GG19-PAR-MARKETING.                                 00000190
                 06 TS-GG19-CVMARK1      PIC X(05).                     00000200
                 06 TS-GG19-LVMARK1      PIC X(20).                     00000210
                 06 TS-GG19-CVMARK2      PIC X(05).                     00000220
                 06 TS-GG19-LVMARK2      PIC X(20).                     00000230
                 06 TS-GG19-NSEQ         PIC 99.                        00000240
              04 TS-GG19-PAR-NCODIC REDEFINES TS-GG19-PAR-MARKETING.    00000250
                 06 TS-GG19-NCODIC       PIC X(7).                      00000260
                 06 TS-GG19-CMARQ        PIC X(5).                      00000270
                 06 TS-GG19-LREF         PIC X(20).                     00000280
                 06 FILLER               PIC X(20).                     00000290
0052                                                                    00000300
              04    TS-GG19-CSUP         PIC X(1).                      00000310
              04    TS-GG19-PRMP         PIC S9(7)V99 COMP-3.           00000320
              04    TS-GG19-WTAUXTVA     PIC S9V9999  COMP-3.           00000330
0009  *                                                                 00000340
      *                                                                 00000350
              04 TS-GG19-TAB-ZONPRIX.                                   00000360
                 06 TS-GG19-LIGNE        OCCURS 10.                     00000370
      * 10 X 34                                                         00000380
                    08 TS-GG19-NZONPRIX  PIC X(02).                     00000390
                    08 TS-GG19-DEFFET    PIC X(08).                     00000400
                    08 TS-GG19-DFINEFFET PIC X(08).                     00000410
      *                                                                 00000420
                    08 TS-GG19-TYPP      PIC X.                         00000430
                    08 TS-GG19-MTP       PIC S9(3)V99 COMP-3.           00000440
      *                                                                 00000450
                    08 TS-GG19-TYPPN     PIC X.                         00000460
                    08 TS-GG19-MTPN      PIC S9(3)V99 COMP-3.           00000470
      *                                                                 00000480
                    08 TS-GG19-PXV       PIC S9(7)V99 COMP-3.           00000490
                    08 TS-GG19-PCOMMART  PIC S9(3)V99 COMP-3.           00000500
                    08 TS-GG19-MOD       PIC  9.                        00000510
0350  *                                                                 00000520
      *===============================================================* 00000530
                                                                                
