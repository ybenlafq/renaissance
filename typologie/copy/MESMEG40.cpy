      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:08 >
      
      ******************************************************************
      * COPY POUR LA LIAISON MQSERIES MEG40 <-> MODULE AS/400          *
      * - MESMEG40 COPY GENERALISEE 50000 OCTETS                       *
      *   =   105 DE DEFINITION D ENTETE                               *
      *   +    65 DE ZONE RETOUR                                       *
      *   + 49830 DE MESSAGE REEL                                      *
      *                                                                *
      * ATTENTION: CETTE COPY DOIT CORRESPONDRE A LA COPY 'CCBCOMMQ21' *
      * DE L'ENVIRONNEMENT AS/400                                      *
      ******************************************************************
       01 MESMEG40-MESSAGE.
          05 MESMEG40E-ENTETE.
             10 MESMEG40E-TYPE            PIC  X(03).
             10 MESMEG40E-NSOCMSG         PIC  X(03).
             10 MESMEG40E-NLIEUMSG        PIC  X(03).
             10 MESMEG40E-NSOCDST         PIC  X(03).
             10 MESMEG40E-NLIEUDST        PIC  X(03).
             10 MESMEG40E-NORD            PIC  9(08).
             10 MESMEG40E-LPROG           PIC  X(10).
             10 MESMEG40E-DJOUR           PIC  X(08).
             10 MESMEG40E-WSID            PIC  X(10).
             10 MESMEG40E-USER            PIC  X(10).
             10 MESMEG40E-CHRONO          PIC  9(07).
             10 MESMEG40E-NBRMSG          PIC  9(07).
             10 MESMEG40E-NBRENR          PIC  9(05).
             10 MESMEG40E-TAILLE          PIC  9(05).
             10 MESMEG40E-VERSION         PIC  X(02).
             10 MESMEG40E-DSYST           PIC S9(13).
             10 MESMEG40E-FILLER          PIC  X(05).
          05 MESMEG40R-RETOUR.
             10 MESMEG40R-CODE-RETOUR.
                15 MESMEG40R-CODRET       PIC  X(01).
                   88 MESMEG40R-CODRET-OK      VALUE ' '.
                   88 MESMEG40R-CODRET-ERREUR  VALUE '1'.
                15 MESMEG40R-LIBERR       PIC  X(64).
          05 MESMEG40D-DATA.
             10 MESMEG40D-CODE-ETAT       PIC  X(10).
             10 MESMEG40D-IMPRIMANTE      PIC  X(10).
             10 MESMEG40D-NB-EXEMPL       PIC  9(03).
             10 MESMEG40D-QUICKPRESS      PIC  X(01).
             10 MESMEG40D-NB-LIGNE        PIC  X(03).
             10 MESMEG40D-FILLER          PIC  X(96).
             10 MESMEG40D-DONNEES         PIC  X(49707).
      
