      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * ITSSC                                                                   
      * GESTION PRIX PRIMES                                                     
E0452 ******************************************************************        
      * DE02005 26/09/08 CODE INITIAL                                           
      ******************************************************************        
      * 4096                                                            00370000
       01 Z-COMMAREA.                                                   00350000
*******  ZONES COMMUNES D'APPEL A LA TRANSACTION                        00370000
      *  1024                                                           00370000
         02 COMM-CB11-COMMUN.                                                   
      *    ZONES STANDARD RESERVEES                                     00370000
      *    0372                                                         00370000
           05 COMM-RESERVE.                                                     
               10 FILLER-COM-AIDA              PIC X(100).                  0038
               10 COMM-CICS-APPLID             PIC X(8).                    0041
               10 COMM-CICS-NETNAM             PIC X(8).                    0042
               10 COMM-CICS-TRANSA             PIC X(4).                    0043
               10 COMM-DATE-SIECLE             PIC XX.                      0046
               10 COMM-DATE-ANNEE              PIC XX.                      0047
               10 COMM-DATE-MOIS               PIC XX.                      0048
               10 COMM-DATE-JOUR               PIC XX.                      0049
               10 COMM-DATE-QNTA               PIC 999.                     0051
               10 COMM-DATE-QNT0               PIC 99999.                   0052
               10 COMM-DATE-BISX               PIC 9.                       0054
               10 COMM-DATE-JSM                PIC 9.                       0056
               10 COMM-DATE-JSM-LC             PIC XXX.                     0058
               10 COMM-DATE-JSM-LL             PIC XXXXXXXX.                0059
               10 COMM-DATE-MOIS-LC            PIC XXX.                     0061
               10 COMM-DATE-MOIS-LL            PIC XXXXXXXX.                0062
               10 COMM-DATE-SSAAMMJJ           PIC X(8).                    0064
               10 COMM-DATE-AAMMJJ             PIC X(6).                    0065
               10 COMM-DATE-JJMMSSAA           PIC X(8).                    0066
               10 COMM-DATE-JJMMAA             PIC X(6).                    0067
               10 COMM-DATE-JJ-MM-AA           PIC X(8).                    0068
               10 COMM-DATE-JJ-MM-SSAA         PIC X(10).                   0069
               10 COMM-DATE-WEEK.                                           0069
                   15 COMM-DATE-SEMSS          PIC 99.                      0069
                   15 COMM-DATE-SEMAA          PIC 99.                      0069
                   15 COMM-DATE-SEMNU          PIC 99.                      0069
               10 COMM-DATE-FILLER             PIC X(08).                   0069
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10 COMM-SWAP-CURS               PIC S9(4) COMP VALUE -1.     0075
      *--                                                                       
               10 COMM-SWAP-CURS               PIC S9(4) COMP-5 VALUE           
                                                                     -1.        
      *}                                                                        
               10 COMM-SWAP-ATTR OCCURS 150    PIC X(1).                    0076
      *    FILLER                                                       00370000
      *    0652                                                         00370000
           05 FILLER                           PIC X(652).                      
*******  ZONES SPECIFIQUES TCB11                                        00370000
      *  1024                                                           00370000
         02 COMM-CB11-TCB11.                                                    
      *    COMMAREA SPECIFIQUE TCB11                                            
      *    0138                                                                 
E0486 *    0149                                                                 
           05 CS-CB11-DATA.                                                     
               10 CS-CB11-NSOCIETE            PIC X(3) VALUE SPACE.             
               10 CS-CB11-DRELEVE             PIC X(8) VALUE SPACE.             
               10 CS-CB11-DDEBPX              PIC X(8) VALUE SPACE.             
               10 CS-CB11-DFINPX              PIC X(8) VALUE SPACE.             
E04866         10 CS-CB11-DPERIME             PIC X(8) VALUE SPACE.             
      *        0093                                                             
E0486 *        0096                                                             
               10 CS-CB11-GESTION.                                              
                   15 CP-CB11-STATUT          PIC S9(3) COMP-3 VALUE 0. .       
                       88 CC-CB11-AUCUN           VALUE  000.                   
                       88 CC-CB11-INIT            VALUE +001.                   
                       88 CC-CB11-OK              VALUE +010.                   
                       88 CC-CB11-FLT             VALUE +011.                   
                       88 CC-CB11-SAS             VALUE +012.                   
                       88 CC-CB11-MOD             VALUE +100.                   
                       88 CC-CB11-ANO             VALUE -100.                   
E0486-             15 CP-CB11-SEL             PIC S9(3) COMP-3 VALUE 0. .       
4                  15 CF-CB11-SEL             PIC 9(1)  VALUE 0.        .       
                       88 CC-CB11-SEL-NONE        VALUE 0.                      
-E0486                 88 CC-CB11-SEL-ZONE        VALUE 1.                      
                   15 CP-CB11-RL-SUP          PIC S9(3) COMP-3 VALUE 0. .       
?????              15 CP-CB11-ACT             PIC S9(3) COMP-3 VALUE 0. .       
                       88 CC-CB11-ACT-NONE        VALUE  000.                   
                       88 CC-CB11-ACT-EVAL        VALUE +100.                   
                   15 CF-CB11-STT             PIC X(4)  VALUE 'STCK'.   .       
                       88 CC-CB11-STT-STOCK       VALUE 'Stck'.                 
                       88 CC-CB11-STT-V4S         VALUE ' V4S'.                 
                   15 CF-CB11-RL              PIC 9(1)  VALUE 0.        .       
                       88 CC-CB11-RL-LIB          VALUE 0.                      
                       88 CC-CB11-RL-COM          VALUE 1.                      
                   15 CP-CB11-PAGE            PIC S9(3) COMP-3.                 
                   15 CP-CB11-PAGEMAX         PIC S9(3) COMP-3.                 
                   15 CP-CB11-TAB             PIC S9(4) COMP-3.                 
                   15 CP-CB11-TABMAX          PIC S9(4) COMP-3.                 
                   15 CS-CB11-TS.                                               
                       20 TS-CB11-FL-IDENT    PIC X(8).                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                20 TB-CB11-FL-NB       PIC S9(4) COMP.                   
      *--                                                                       
                       20 TB-CB11-FL-NB       PIC S9(4) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                20 TB-CB11-FL-MAX      PIC S9(4) COMP.                   
      *--                                                                       
                       20 TB-CB11-FL-MAX      PIC S9(4) COMP-5.                 
      *}                                                                        
                       20 TS-CB11-RL-IDENT    PIC X(8).                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                20 TB-CB11-RL-NB       PIC S9(4) COMP.                   
      *--                                                                       
                       20 TB-CB11-RL-NB       PIC S9(4) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                20 TB-CB11-RL-MAX      PIC S9(4) COMP.                   
      *--                                                                       
                       20 TB-CB11-RL-MAX      PIC S9(4) COMP-5.                 
      *}                                                                        
                       20 TS-CB11-AF-IDENT    PIC X(8).                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                20 TB-CB11-AF-NB       PIC S9(4) COMP.                   
      *--                                                                       
                       20 TB-CB11-AF-NB       PIC S9(4) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                20 TB-CB11-AF-MAX      PIC S9(4) COMP.                   
      *--                                                                       
                       20 TB-CB11-AF-MAX      PIC S9(4) COMP-5.                 
      *}                                                                        
                       20 TS-CB11-NC-IDENT    PIC X(8).                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                20 TB-CB11-NC-NB       PIC S9(4) COMP.                   
      *--                                                                       
                       20 TB-CB11-NC-NB       PIC S9(4) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                20 TB-CB11-NC-MAX      PIC S9(4) COMP.                   
      *--                                                                       
                       20 TB-CB11-NC-MAX      PIC S9(4) COMP-5.                 
      *}                                                                        
                       20 TS-CB11-NL-IDENT    PIC X(8).                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                20 TB-CB11-NL-NB       PIC S9(4) COMP.                   
      *--                                                                       
                       20 TB-CB11-NL-NB       PIC S9(4) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                20 TB-CB11-NL-MAX      PIC S9(4) COMP.                   
      *--                                                                       
                       20 TB-CB11-NL-MAX      PIC S9(4) COMP-5.                 
      *}                                                                        
                       20 TS-CB11-MG-IDENT    PIC X(8).                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                20 TB-CB11-MG-NB       PIC S9(4) COMP.                   
      *--                                                                       
                       20 TB-CB11-MG-NB       PIC S9(4) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                20 TB-CB11-MG-MAX      PIC S9(4) COMP.                   
      *--                                                                       
                       20 TB-CB11-MG-MAX      PIC S9(4) COMP-5.                 
      *}                                                                        
      *        0018                                                             
               10 CS-CB11-ENR.                                                  
                   15 CS-CB11-FILTRE.                                           
                       20 CS-CB11-NZONPRIX    PIC X(2).                         
                       20 CS-CB11-CHEFPROD    PIC X(5).                         
                       20 CS-CB11-CFAM        PIC X(5).                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                20 CB-CB11-FL-NZONPRIX PIC S9(4) COMP.                   
      *--                                                                       
                       20 CB-CB11-FL-NZONPRIX PIC S9(4) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                20 CB-CB11-FL-POS      PIC S9(4) COMP.                   
      *--                                                                       
                       20 CB-CB11-FL-POS      PIC S9(4) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                20 CB-CB11-LI-POS      PIC S9(4) COMP.                   
      *--                                                                       
                       20 CB-CB11-LI-POS      PIC S9(4) COMP-5.                 
      *}                                                                        
      *    1024-0138                                                            
E0486 *    1024-0149                                                            
E0486 *    05 FILLER                          PIC X(886).                       
E0486      05 FILLER                          PIC X(875).                       
*******  ZONES SPECIFIQUES MCB00                                        00370000
      *  1024                                                           00370000
         02 COMM-CB11-MCB00.                                                    
      *{Pos-translation Correct-Appel-copie
      *     COPY COMMCB00.                                                       
           EXEC SQL INCLUDE COMMCB00 END-EXEC.
      *}
*******  FILLER LIBRE                                                   00370000
      *  1024                                                           00370000
         02 FILLER                            PIC X(1024).                      
                                                                                
