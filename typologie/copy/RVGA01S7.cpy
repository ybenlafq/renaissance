      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PRGRP REGROUPEMENT DE PRESTATION       *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01S7.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01S7.                                                            
      *}                                                                        
           05  PRGRP-CTABLEG2    PIC X(15).                                     
           05  PRGRP-CTABLEG2-REDEF REDEFINES PRGRP-CTABLEG2.                   
               10  PRGRP-CGRP            PIC X(05).                             
               10  PRGRP-TGRP            PIC X(01).                             
           05  PRGRP-WTABLEG     PIC X(80).                                     
           05  PRGRP-WTABLEG-REDEF  REDEFINES PRGRP-WTABLEG.                    
               10  PRGRP-WACTIF          PIC X(01).                             
               10  PRGRP-LGRP            PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01S7-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01S7-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRGRP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PRGRP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRGRP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PRGRP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
