      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGG3000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGG3000                         
      **********************************************************                
       01  RVGG3000.                                                            
           02  GG30-DANNEE                                                      
               PIC X(0004).                                                     
           02  GG30-DMOIS                                                       
               PIC X(0002).                                                     
           02  GG30-CFAM                                                        
               PIC X(0005).                                                     
           02  GG30-CHEFPROD                                                    
               PIC X(0005).                                                     
           02  GG30-NCONC                                                       
               PIC X(0004).                                                     
           02  GG30-QNBRELAPP                                                   
               PIC S9(5) COMP-3.                                                
           02  GG30-QNBARTIAPP                                                  
               PIC S9(5) COMP-3.                                                
           02  GG30-QNBALIAAPP                                                  
               PIC S9(5) COMP-3.                                                
           02  GG30-QNBALIASAPP                                                 
               PIC S9(5) COMP-3.                                                
           02  GG30-QNBALINAAPP                                                 
               PIC S9(5) COMP-3.                                                
           02  GG30-QNBRELC                                                     
               PIC S9(5) COMP-3.                                                
           02  GG30-QNBARTIC                                                    
               PIC S9(5) COMP-3.                                                
           02  GG30-QNBALIAC                                                    
               PIC S9(5) COMP-3.                                                
           02  GG30-QNBALIASC                                                   
               PIC S9(5) COMP-3.                                                
           02  GG30-QNBALINAC                                                   
               PIC S9(5) COMP-3.                                                
           02  GG30-QNBRELEPD                                                   
               PIC S9(5) COMP-3.                                                
           02  GG30-QNBARTIEPD                                                  
               PIC S9(5) COMP-3.                                                
           02  GG30-QNBALIAEPD                                                  
               PIC S9(5) COMP-3.                                                
           02  GG30-QNBALIASEPD                                                 
               PIC S9(5) COMP-3.                                                
           02  GG30-QNBALINAEPD                                                 
               PIC S9(5) COMP-3.                                                
           02  GG30-QNBRELEPF                                                   
               PIC S9(5) COMP-3.                                                
           02  GG30-QNBARTIEPF                                                  
               PIC S9(5) COMP-3.                                                
           02  GG30-QNBALIAEPF                                                  
               PIC S9(5) COMP-3.                                                
           02  GG30-QNBALIASEPF                                                 
               PIC S9(5) COMP-3.                                                
           02  GG30-QNBALINAEPF                                                 
               PIC S9(5) COMP-3.                                                
           02  GG30-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGG3000                                  
      **********************************************************                
       01  RVGG3000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-DANNEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-DANNEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-DMOIS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-DMOIS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-CHEFPROD-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-CHEFPROD-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-NCONC-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-NCONC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-QNBRELAPP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-QNBRELAPP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-QNBARTIAPP-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-QNBARTIAPP-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-QNBALIAAPP-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-QNBALIAAPP-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-QNBALIASAPP-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-QNBALIASAPP-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-QNBALINAAPP-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-QNBALINAAPP-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-QNBRELC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-QNBRELC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-QNBARTIC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-QNBARTIC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-QNBALIAC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-QNBALIAC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-QNBALIASC-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-QNBALIASC-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-QNBALINAC-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-QNBALINAC-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-QNBRELEPD-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-QNBRELEPD-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-QNBARTIEPD-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-QNBARTIEPD-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-QNBALIAEPD-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-QNBALIAEPD-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-QNBALIASEPD-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-QNBALIASEPD-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-QNBALINAEPD-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-QNBALINAEPD-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-QNBRELEPF-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-QNBRELEPF-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-QNBARTIEPF-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-QNBARTIEPF-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-QNBALIAEPF-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-QNBALIAEPF-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-QNBALIASEPF-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-QNBALIASEPF-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-QNBALINAEPF-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG30-QNBALINAEPF-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG30-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GG30-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
      *}                                                                        
