      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GCPLT GARANTIES COMPLEMENTAIRES        *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01DI.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01DI.                                                            
      *}                                                                        
           05  GCPLT-CTABLEG2    PIC X(15).                                     
           05  GCPLT-CTABLEG2-REDEF REDEFINES GCPLT-CTABLEG2.                   
               10  GCPLT-CGCPLT          PIC X(05).                             
           05  GCPLT-WTABLEG     PIC X(80).                                     
           05  GCPLT-WTABLEG-REDEF  REDEFINES GCPLT-WTABLEG.                    
               10  GCPLT-LGCPLT          PIC X(20).                             
               10  GCPLT-CTVA            PIC X(05).                             
               10  GCPLT-QDC             PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  GCPLT-QDC-N          REDEFINES GCPLT-QDC                     
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  GCPLT-QDS             PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  GCPLT-QDS-N          REDEFINES GCPLT-QDS                     
                                         PIC 9(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01DI-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01DI-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GCPLT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GCPLT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GCPLT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GCPLT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
