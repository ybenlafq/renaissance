      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HETHS TYPES DE HS GHS                  *        
      *----------------------------------------------------------------*        
       01  RVGA01L9.                                                            
           05  HETHS-CTABLEG2    PIC X(15).                                     
           05  HETHS-CTABLEG2-REDEF REDEFINES HETHS-CTABLEG2.                   
               10  HETHS-CTYPEHS         PIC X(05).                             
           05  HETHS-WTABLEG     PIC X(80).                                     
           05  HETHS-WTABLEG-REDEF  REDEFINES HETHS-WTABLEG.                    
               10  HETHS-WACTIF          PIC X(01).                             
               10  HETHS-LTYPEHS         PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01L9-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HETHS-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HETHS-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HETHS-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HETHS-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
