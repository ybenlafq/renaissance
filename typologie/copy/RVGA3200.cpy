      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE CMTD00.RTGA32                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVGA3200.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVGA3200.                                                             
      *}                                                                        
           10 GA32-NCODIC          PIC X(07).                                   
           10 GA32-NSEQUENCE       PIC S9(2) COMP-3.                            
           10 GA32-NEANCOMPO       PIC X(13).                                   
           10 GA32-WACTIF          PIC X(01).                                   
           10 GA32-LNEANCOMPO      PIC X(15).                                   
           10 GA32-WAUTHENT        PIC X(01).                                   
           10 GA32-LIDENTIFIANT    PIC X(10).                                   
           10 GA32-DACTIF          PIC X(08).                                   
           10 GA32-DSYST           PIC S9(13) COMP-3.                           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVGA3200-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVGA3200-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA32-NCODIC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA32-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA32-NSEQUENCE-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GA32-NSEQUENCE-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA32-NEANCOMPO-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GA32-NEANCOMPO-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA32-WACTIF-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA32-WACTIF-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA32-LNEANCOMPO-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 GA32-LNEANCOMPO-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA32-WAUTHENT-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA32-WAUTHENT-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA32-LIDENTIFIANT-F  PIC S9(4) COMP.                              
      *--                                                                       
           10 GA32-LIDENTIFIANT-F  PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA32-DACTIF-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA32-DACTIF-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA32-DSYST-F         PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 GA32-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
