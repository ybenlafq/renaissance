      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA63   EGA63                                              00000020
      ***************************************************************** 00000030
       01   EGA63I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * NUMERO DE PAGE                                                  00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MPAGEI    PIC X(3).                                       00000200
      * FONCTION                                                        00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MWFONCI   PIC X(3).                                       00000250
      * CODIC                                                           00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MNCODICI  PIC X(7).                                       00000300
      * REFERENCE FOURN                                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MLREFI    PIC X(20).                                      00000350
      * quantite vendue par                                             00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTECL    COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MQTECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MQTECF    PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MQTECI    PIC X(5).                                       00000400
      * CODE FAMILLE                                                    00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFAML    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNFAMF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNFAMI    PIC X(5).                                       00000450
      * LIBELLE FAMILLE                                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MLFAMI    PIC X(20).                                      00000500
      * quantite gratuite                                               00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTEGRAL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MQTEGRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQTEGRAF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MQTEGRAI  PIC X(5).                                       00000550
      * CODE MARQUE                                                     00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMARQL   COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MNMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNMARQF   PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MNMARQI   PIC X(5).                                       00000600
      * LIBELLE MARQUE                                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLMARQI   PIC X(20).                                      00000650
      * LIVRET CONFIANCE                                                00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWLCONFL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MWLCONFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWLCONFF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MWLCONFI  PIC X.                                          00000700
      * ETIQUETTE FORMAT INFORMATIVE                                    00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFINFOL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MCFINFOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCFINFOF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MCFINFOI  PIC X.                                          00000750
      * ETIQUETTE FORMAT PRIX                                           00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFPRIXL  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MCFPRIXL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCFPRIXF  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MCFPRIXI  PIC X.                                          00000800
           02 M69I OCCURS   10 TIMES .                                  00000810
      * CODE CARACTERISTIQUE SPECIFIQUE                                 00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCCARL  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MCCARL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCCARF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MCCARI  PIC X(5).                                       00000860
      * LIBELLE CARACT SPECIFIQUE                                       00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCARL  COMP PIC S9(4).                                 00000880
      *--                                                                       
             03 MLCARL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLCARF  PIC X.                                          00000890
             03 FILLER  PIC X(4).                                       00000900
             03 MLCARI  PIC X(20).                                      00000910
      * CODE MODE DE DELIVRANCE                                         00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMODL  COMP PIC S9(4).                                 00000930
      *--                                                                       
             03 MCMODL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCMODF  PIC X.                                          00000940
             03 FILLER  PIC X(4).                                       00000950
             03 MCMODI  PIC X(3).                                       00000960
      * LIBELLE MODE DE DELIVRANCE                                      00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMODL  COMP PIC S9(4).                                 00000980
      *--                                                                       
             03 MLMODL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLMODF  PIC X.                                          00000990
             03 FILLER  PIC X(4).                                       00001000
             03 MLMODI  PIC X(20).                                      00001010
      * ZONE CMD AIDA                                                   00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MZONCMDI  PIC X(15).                                      00001060
      * MESSAGE ERREUR                                                  00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001080
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001090
           02 FILLER    PIC X(4).                                       00001100
           02 MLIBERRI  PIC X(58).                                      00001110
      * CODE TRANSACTION                                                00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001130
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001140
           02 FILLER    PIC X(4).                                       00001150
           02 MCODTRAI  PIC X(4).                                       00001160
      * CICS DE TRAVAIL                                                 00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MCICSI    PIC X(5).                                       00001210
      * NETNAME                                                         00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MNETNAMI  PIC X(8).                                       00001260
      * CODE TERMINAL                                                   00001270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001280
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001290
           02 FILLER    PIC X(4).                                       00001300
           02 MSCREENI  PIC X(5).                                       00001310
      * qte multiple autorisee                                          00001320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWQTEML   COMP PIC S9(4).                                 00001330
      *--                                                                       
           02 MWQTEML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWQTEMF   PIC X.                                          00001340
           02 FILLER    PIC X(4).                                       00001350
           02 MWQTEMI   PIC X.                                          00001360
      * remise autorisee                                                00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWREMISEL      COMP PIC S9(4).                            00001380
      *--                                                                       
           02 MWREMISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWREMISEF      PIC X.                                     00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MWREMISEI      PIC X.                                     00001410
      * a afficher sur ticket                                           00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTICKETL      COMP PIC S9(4).                            00001430
      *--                                                                       
           02 MWTICKETL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWTICKETF      PIC X.                                     00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MWTICKETI      PIC X.                                     00001460
      ***************************************************************** 00001470
      * SDF: EGA63   EGA63                                              00001480
      ***************************************************************** 00001490
       01   EGA63O REDEFINES EGA63I.                                    00001500
           02 FILLER    PIC X(12).                                      00001510
      * DATE DU JOUR                                                    00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MDATJOUA  PIC X.                                          00001540
           02 MDATJOUC  PIC X.                                          00001550
           02 MDATJOUP  PIC X.                                          00001560
           02 MDATJOUH  PIC X.                                          00001570
           02 MDATJOUV  PIC X.                                          00001580
           02 MDATJOUO  PIC X(10).                                      00001590
      * HEURE                                                           00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MTIMJOUA  PIC X.                                          00001620
           02 MTIMJOUC  PIC X.                                          00001630
           02 MTIMJOUP  PIC X.                                          00001640
           02 MTIMJOUH  PIC X.                                          00001650
           02 MTIMJOUV  PIC X.                                          00001660
           02 MTIMJOUO  PIC X(5).                                       00001670
      * NUMERO DE PAGE                                                  00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MPAGEA    PIC X.                                          00001700
           02 MPAGEC    PIC X.                                          00001710
           02 MPAGEP    PIC X.                                          00001720
           02 MPAGEH    PIC X.                                          00001730
           02 MPAGEV    PIC X.                                          00001740
           02 MPAGEO    PIC X(3).                                       00001750
      * FONCTION                                                        00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MWFONCA   PIC X.                                          00001780
           02 MWFONCC   PIC X.                                          00001790
           02 MWFONCP   PIC X.                                          00001800
           02 MWFONCH   PIC X.                                          00001810
           02 MWFONCV   PIC X.                                          00001820
           02 MWFONCO   PIC X(3).                                       00001830
      * CODIC                                                           00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MNCODICA  PIC X.                                          00001860
           02 MNCODICC  PIC X.                                          00001870
           02 MNCODICP  PIC X.                                          00001880
           02 MNCODICH  PIC X.                                          00001890
           02 MNCODICV  PIC X.                                          00001900
           02 MNCODICO  PIC X(7).                                       00001910
      * REFERENCE FOURN                                                 00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MLREFA    PIC X.                                          00001940
           02 MLREFC    PIC X.                                          00001950
           02 MLREFP    PIC X.                                          00001960
           02 MLREFH    PIC X.                                          00001970
           02 MLREFV    PIC X.                                          00001980
           02 MLREFO    PIC X(20).                                      00001990
      * quantite vendue par                                             00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MQTECA    PIC X.                                          00002020
           02 MQTECC    PIC X.                                          00002030
           02 MQTECP    PIC X.                                          00002040
           02 MQTECH    PIC X.                                          00002050
           02 MQTECV    PIC X.                                          00002060
           02 MQTECO    PIC ZZZZ9.                                      00002070
      * CODE FAMILLE                                                    00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MNFAMA    PIC X.                                          00002100
           02 MNFAMC    PIC X.                                          00002110
           02 MNFAMP    PIC X.                                          00002120
           02 MNFAMH    PIC X.                                          00002130
           02 MNFAMV    PIC X.                                          00002140
           02 MNFAMO    PIC X(5).                                       00002150
      * LIBELLE FAMILLE                                                 00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MLFAMA    PIC X.                                          00002180
           02 MLFAMC    PIC X.                                          00002190
           02 MLFAMP    PIC X.                                          00002200
           02 MLFAMH    PIC X.                                          00002210
           02 MLFAMV    PIC X.                                          00002220
           02 MLFAMO    PIC X(20).                                      00002230
      * quantite gratuite                                               00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MQTEGRAA  PIC X.                                          00002260
           02 MQTEGRAC  PIC X.                                          00002270
           02 MQTEGRAP  PIC X.                                          00002280
           02 MQTEGRAH  PIC X.                                          00002290
           02 MQTEGRAV  PIC X.                                          00002300
           02 MQTEGRAO  PIC ZZZZ9.                                      00002310
      * CODE MARQUE                                                     00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MNMARQA   PIC X.                                          00002340
           02 MNMARQC   PIC X.                                          00002350
           02 MNMARQP   PIC X.                                          00002360
           02 MNMARQH   PIC X.                                          00002370
           02 MNMARQV   PIC X.                                          00002380
           02 MNMARQO   PIC X(5).                                       00002390
      * LIBELLE MARQUE                                                  00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MLMARQA   PIC X.                                          00002420
           02 MLMARQC   PIC X.                                          00002430
           02 MLMARQP   PIC X.                                          00002440
           02 MLMARQH   PIC X.                                          00002450
           02 MLMARQV   PIC X.                                          00002460
           02 MLMARQO   PIC X(20).                                      00002470
      * LIVRET CONFIANCE                                                00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MWLCONFA  PIC X.                                          00002500
           02 MWLCONFC  PIC X.                                          00002510
           02 MWLCONFP  PIC X.                                          00002520
           02 MWLCONFH  PIC X.                                          00002530
           02 MWLCONFV  PIC X.                                          00002540
           02 MWLCONFO  PIC X.                                          00002550
      * ETIQUETTE FORMAT INFORMATIVE                                    00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MCFINFOA  PIC X.                                          00002580
           02 MCFINFOC  PIC X.                                          00002590
           02 MCFINFOP  PIC X.                                          00002600
           02 MCFINFOH  PIC X.                                          00002610
           02 MCFINFOV  PIC X.                                          00002620
           02 MCFINFOO  PIC X.                                          00002630
      * ETIQUETTE FORMAT PRIX                                           00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MCFPRIXA  PIC X.                                          00002660
           02 MCFPRIXC  PIC X.                                          00002670
           02 MCFPRIXP  PIC X.                                          00002680
           02 MCFPRIXH  PIC X.                                          00002690
           02 MCFPRIXV  PIC X.                                          00002700
           02 MCFPRIXO  PIC X.                                          00002710
           02 M69O OCCURS   10 TIMES .                                  00002720
      * CODE CARACTERISTIQUE SPECIFIQUE                                 00002730
             03 FILLER       PIC X(2).                                  00002740
             03 MCCARA  PIC X.                                          00002750
             03 MCCARC  PIC X.                                          00002760
             03 MCCARP  PIC X.                                          00002770
             03 MCCARH  PIC X.                                          00002780
             03 MCCARV  PIC X.                                          00002790
             03 MCCARO  PIC X(5).                                       00002800
      * LIBELLE CARACT SPECIFIQUE                                       00002810
             03 FILLER       PIC X(2).                                  00002820
             03 MLCARA  PIC X.                                          00002830
             03 MLCARC  PIC X.                                          00002840
             03 MLCARP  PIC X.                                          00002850
             03 MLCARH  PIC X.                                          00002860
             03 MLCARV  PIC X.                                          00002870
             03 MLCARO  PIC X(20).                                      00002880
      * CODE MODE DE DELIVRANCE                                         00002890
             03 FILLER       PIC X(2).                                  00002900
             03 MCMODA  PIC X.                                          00002910
             03 MCMODC  PIC X.                                          00002920
             03 MCMODP  PIC X.                                          00002930
             03 MCMODH  PIC X.                                          00002940
             03 MCMODV  PIC X.                                          00002950
             03 MCMODO  PIC X(3).                                       00002960
      * LIBELLE MODE DE DELIVRANCE                                      00002970
             03 FILLER       PIC X(2).                                  00002980
             03 MLMODA  PIC X.                                          00002990
             03 MLMODC  PIC X.                                          00003000
             03 MLMODP  PIC X.                                          00003010
             03 MLMODH  PIC X.                                          00003020
             03 MLMODV  PIC X.                                          00003030
             03 MLMODO  PIC X(20).                                      00003040
      * ZONE CMD AIDA                                                   00003050
           02 FILLER    PIC X(2).                                       00003060
           02 MZONCMDA  PIC X.                                          00003070
           02 MZONCMDC  PIC X.                                          00003080
           02 MZONCMDP  PIC X.                                          00003090
           02 MZONCMDH  PIC X.                                          00003100
           02 MZONCMDV  PIC X.                                          00003110
           02 MZONCMDO  PIC X(15).                                      00003120
      * MESSAGE ERREUR                                                  00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MLIBERRA  PIC X.                                          00003150
           02 MLIBERRC  PIC X.                                          00003160
           02 MLIBERRP  PIC X.                                          00003170
           02 MLIBERRH  PIC X.                                          00003180
           02 MLIBERRV  PIC X.                                          00003190
           02 MLIBERRO  PIC X(58).                                      00003200
      * CODE TRANSACTION                                                00003210
           02 FILLER    PIC X(2).                                       00003220
           02 MCODTRAA  PIC X.                                          00003230
           02 MCODTRAC  PIC X.                                          00003240
           02 MCODTRAP  PIC X.                                          00003250
           02 MCODTRAH  PIC X.                                          00003260
           02 MCODTRAV  PIC X.                                          00003270
           02 MCODTRAO  PIC X(4).                                       00003280
      * CICS DE TRAVAIL                                                 00003290
           02 FILLER    PIC X(2).                                       00003300
           02 MCICSA    PIC X.                                          00003310
           02 MCICSC    PIC X.                                          00003320
           02 MCICSP    PIC X.                                          00003330
           02 MCICSH    PIC X.                                          00003340
           02 MCICSV    PIC X.                                          00003350
           02 MCICSO    PIC X(5).                                       00003360
      * NETNAME                                                         00003370
           02 FILLER    PIC X(2).                                       00003380
           02 MNETNAMA  PIC X.                                          00003390
           02 MNETNAMC  PIC X.                                          00003400
           02 MNETNAMP  PIC X.                                          00003410
           02 MNETNAMH  PIC X.                                          00003420
           02 MNETNAMV  PIC X.                                          00003430
           02 MNETNAMO  PIC X(8).                                       00003440
      * CODE TERMINAL                                                   00003450
           02 FILLER    PIC X(2).                                       00003460
           02 MSCREENA  PIC X.                                          00003470
           02 MSCREENC  PIC X.                                          00003480
           02 MSCREENP  PIC X.                                          00003490
           02 MSCREENH  PIC X.                                          00003500
           02 MSCREENV  PIC X.                                          00003510
           02 MSCREENO  PIC X(5).                                       00003520
      * qte multiple autorisee                                          00003530
           02 FILLER    PIC X(2).                                       00003540
           02 MWQTEMA   PIC X.                                          00003550
           02 MWQTEMC   PIC X.                                          00003560
           02 MWQTEMP   PIC X.                                          00003570
           02 MWQTEMH   PIC X.                                          00003580
           02 MWQTEMV   PIC X.                                          00003590
           02 MWQTEMO   PIC X.                                          00003600
      * remise autorisee                                                00003610
           02 FILLER    PIC X(2).                                       00003620
           02 MWREMISEA      PIC X.                                     00003630
           02 MWREMISEC PIC X.                                          00003640
           02 MWREMISEP PIC X.                                          00003650
           02 MWREMISEH PIC X.                                          00003660
           02 MWREMISEV PIC X.                                          00003670
           02 MWREMISEO      PIC X.                                     00003680
      * a afficher sur ticket                                           00003690
           02 FILLER    PIC X(2).                                       00003700
           02 MWTICKETA      PIC X.                                     00003710
           02 MWTICKETC PIC X.                                          00003720
           02 MWTICKETP PIC X.                                          00003730
           02 MWTICKETH PIC X.                                          00003740
           02 MWTICKETV PIC X.                                          00003750
           02 MWTICKETO      PIC X.                                     00003760
                                                                                
