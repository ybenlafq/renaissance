      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HCTLI TABLE DES LIEUX DU CENTRE HS     *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01OB.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01OB.                                                            
      *}                                                                        
           05  HCTLI-CTABLEG2    PIC X(15).                                     
           05  HCTLI-CTABLEG2-REDEF REDEFINES HCTLI-CTABLEG2.                   
               10  HCTLI-LIEUHC          PIC X(03).                             
           05  HCTLI-WTABLEG     PIC X(80).                                     
           05  HCTLI-WTABLEG-REDEF  REDEFINES HCTLI-WTABLEG.                    
               10  HCTLI-LLIEUHC         PIC X(20).                             
               10  HCTLI-LIEUST          PIC X(14).                             
               10  HCTLI-WCMOTIF         PIC X(10).                             
               10  HCTLI-WACTIF          PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01OB-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01OB-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HCTLI-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HCTLI-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HCTLI-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HCTLI-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
