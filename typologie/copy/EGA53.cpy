      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA53   EGA53                                              00000020
      ***************************************************************** 00000030
       01   EGA53I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNCODICI  PIC X(7).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLREFI    PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCRECCL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDCRECCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDCRECCF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDCRECCI  PIC X(8).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNFAMI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLFAMI    PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRECEPL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDRECEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDRECEPF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDRECEPI  PIC X(8).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMARQL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNMARQF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNMARQI   PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLMARQI   PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMAJSTL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MDMAJSTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDMAJSTF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDMAJSTI  PIC X(8).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLRESUML  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLRESUML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLRESUMF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLRESUMI  PIC X(20).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCROSSL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCROSSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCROSSF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCROSSI   PIC X.                                          00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFCL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLREFCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLREFCF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLREFCI   PIC X(50).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCASSORL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCASSORL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCASSORF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCASSORI  PIC X(5).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLASSORL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MLASSORL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLASSORF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLASSORI  PIC X(20).                                      00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWSENVTL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MWSENVTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWSENVTF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MWSENVTI  PIC X.                                          00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOLLECL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MCOLLECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCOLLECF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCOLLECI  PIC X(5).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBCOLLL      COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MLIBCOLLL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBCOLLF      PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLIBCOLLI      PIC X(20).                                 00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSELL     COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MSELL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSELF     PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MSELI     PIC X.                                          00000890
           02 MTABI OCCURS   10 TIMES .                                 00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00000910
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MNSOCI  PIC X(3).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNDEPOTL     COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MNDEPOTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNDEPOTF     PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MNDEPOTI     PIC X(3).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAPPROL     COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MCAPPROL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCAPPROF     PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MCAPPROI     PIC X(5).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLAPPROL     COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MLAPPROL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLAPPROF     PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MLAPPROI     PIC X(20).                                 00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSAPPRL     COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MWSAPPRL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWSAPPRF     PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MWSAPPRI     PIC X.                                     00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCEXPOL      COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MCEXPOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCEXPOF      PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MCEXPOI      PIC X(5).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLEXPOL      COMP PIC S9(4).                            00001150
      *--                                                                       
             03 MLEXPOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLEXPOF      PIC X.                                     00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MLEXPOI      PIC X(20).                                 00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOMPTL     COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MLCOMPTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLCOMPTF     PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MLCOMPTI     PIC X(3).                                  00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MZONCMDI  PIC X(15).                                      00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MLIBERRI  PIC X(58).                                      00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MCODTRAI  PIC X(4).                                       00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MCICSI    PIC X(5).                                       00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MNETNAMI  PIC X(8).                                       00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MSCREENI  PIC X(4).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MPAGEI    PIC X(5).                                       00001500
      ***************************************************************** 00001510
      * SDF: EGA53   EGA53                                              00001520
      ***************************************************************** 00001530
       01   EGA53O REDEFINES EGA53I.                                    00001540
           02 FILLER    PIC X(12).                                      00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MDATJOUA  PIC X.                                          00001570
           02 MDATJOUC  PIC X.                                          00001580
           02 MDATJOUP  PIC X.                                          00001590
           02 MDATJOUH  PIC X.                                          00001600
           02 MDATJOUV  PIC X.                                          00001610
           02 MDATJOUO  PIC X(10).                                      00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MTIMJOUA  PIC X.                                          00001640
           02 MTIMJOUC  PIC X.                                          00001650
           02 MTIMJOUP  PIC X.                                          00001660
           02 MTIMJOUH  PIC X.                                          00001670
           02 MTIMJOUV  PIC X.                                          00001680
           02 MTIMJOUO  PIC X(5).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MWFONCA   PIC X.                                          00001710
           02 MWFONCC   PIC X.                                          00001720
           02 MWFONCP   PIC X.                                          00001730
           02 MWFONCH   PIC X.                                          00001740
           02 MWFONCV   PIC X.                                          00001750
           02 MWFONCO   PIC X(3).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MNCODICA  PIC X.                                          00001780
           02 MNCODICC  PIC X.                                          00001790
           02 MNCODICP  PIC X.                                          00001800
           02 MNCODICH  PIC X.                                          00001810
           02 MNCODICV  PIC X.                                          00001820
           02 MNCODICO  PIC X(7).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MLREFA    PIC X.                                          00001850
           02 MLREFC    PIC X.                                          00001860
           02 MLREFP    PIC X.                                          00001870
           02 MLREFH    PIC X.                                          00001880
           02 MLREFV    PIC X.                                          00001890
           02 MLREFO    PIC X(20).                                      00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MDCRECCA  PIC X.                                          00001920
           02 MDCRECCC  PIC X.                                          00001930
           02 MDCRECCP  PIC X.                                          00001940
           02 MDCRECCH  PIC X.                                          00001950
           02 MDCRECCV  PIC X.                                          00001960
           02 MDCRECCO  PIC X(8).                                       00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MNFAMA    PIC X.                                          00001990
           02 MNFAMC    PIC X.                                          00002000
           02 MNFAMP    PIC X.                                          00002010
           02 MNFAMH    PIC X.                                          00002020
           02 MNFAMV    PIC X.                                          00002030
           02 MNFAMO    PIC X(5).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MLFAMA    PIC X.                                          00002060
           02 MLFAMC    PIC X.                                          00002070
           02 MLFAMP    PIC X.                                          00002080
           02 MLFAMH    PIC X.                                          00002090
           02 MLFAMV    PIC X.                                          00002100
           02 MLFAMO    PIC X(20).                                      00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MDRECEPA  PIC X.                                          00002130
           02 MDRECEPC  PIC X.                                          00002140
           02 MDRECEPP  PIC X.                                          00002150
           02 MDRECEPH  PIC X.                                          00002160
           02 MDRECEPV  PIC X.                                          00002170
           02 MDRECEPO  PIC X(8).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MNMARQA   PIC X.                                          00002200
           02 MNMARQC   PIC X.                                          00002210
           02 MNMARQP   PIC X.                                          00002220
           02 MNMARQH   PIC X.                                          00002230
           02 MNMARQV   PIC X.                                          00002240
           02 MNMARQO   PIC X(5).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MLMARQA   PIC X.                                          00002270
           02 MLMARQC   PIC X.                                          00002280
           02 MLMARQP   PIC X.                                          00002290
           02 MLMARQH   PIC X.                                          00002300
           02 MLMARQV   PIC X.                                          00002310
           02 MLMARQO   PIC X(20).                                      00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MDMAJSTA  PIC X.                                          00002340
           02 MDMAJSTC  PIC X.                                          00002350
           02 MDMAJSTP  PIC X.                                          00002360
           02 MDMAJSTH  PIC X.                                          00002370
           02 MDMAJSTV  PIC X.                                          00002380
           02 MDMAJSTO  PIC X(8).                                       00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MLRESUMA  PIC X.                                          00002410
           02 MLRESUMC  PIC X.                                          00002420
           02 MLRESUMP  PIC X.                                          00002430
           02 MLRESUMH  PIC X.                                          00002440
           02 MLRESUMV  PIC X.                                          00002450
           02 MLRESUMO  PIC X(20).                                      00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MCROSSA   PIC X.                                          00002480
           02 MCROSSC   PIC X.                                          00002490
           02 MCROSSP   PIC X.                                          00002500
           02 MCROSSH   PIC X.                                          00002510
           02 MCROSSV   PIC X.                                          00002520
           02 MCROSSO   PIC X.                                          00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MLREFCA   PIC X.                                          00002550
           02 MLREFCC   PIC X.                                          00002560
           02 MLREFCP   PIC X.                                          00002570
           02 MLREFCH   PIC X.                                          00002580
           02 MLREFCV   PIC X.                                          00002590
           02 MLREFCO   PIC X(50).                                      00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MCASSORA  PIC X.                                          00002620
           02 MCASSORC  PIC X.                                          00002630
           02 MCASSORP  PIC X.                                          00002640
           02 MCASSORH  PIC X.                                          00002650
           02 MCASSORV  PIC X.                                          00002660
           02 MCASSORO  PIC X(5).                                       00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MLASSORA  PIC X.                                          00002690
           02 MLASSORC  PIC X.                                          00002700
           02 MLASSORP  PIC X.                                          00002710
           02 MLASSORH  PIC X.                                          00002720
           02 MLASSORV  PIC X.                                          00002730
           02 MLASSORO  PIC X(20).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MWSENVTA  PIC X.                                          00002760
           02 MWSENVTC  PIC X.                                          00002770
           02 MWSENVTP  PIC X.                                          00002780
           02 MWSENVTH  PIC X.                                          00002790
           02 MWSENVTV  PIC X.                                          00002800
           02 MWSENVTO  PIC X.                                          00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MCOLLECA  PIC X.                                          00002830
           02 MCOLLECC  PIC X.                                          00002840
           02 MCOLLECP  PIC X.                                          00002850
           02 MCOLLECH  PIC X.                                          00002860
           02 MCOLLECV  PIC X.                                          00002870
           02 MCOLLECO  PIC X(5).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MLIBCOLLA      PIC X.                                     00002900
           02 MLIBCOLLC PIC X.                                          00002910
           02 MLIBCOLLP PIC X.                                          00002920
           02 MLIBCOLLH PIC X.                                          00002930
           02 MLIBCOLLV PIC X.                                          00002940
           02 MLIBCOLLO      PIC X(20).                                 00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MSELA     PIC X.                                          00002970
           02 MSELC     PIC X.                                          00002980
           02 MSELP     PIC X.                                          00002990
           02 MSELH     PIC X.                                          00003000
           02 MSELV     PIC X.                                          00003010
           02 MSELO     PIC X.                                          00003020
           02 MTABO OCCURS   10 TIMES .                                 00003030
             03 FILLER       PIC X(2).                                  00003040
             03 MNSOCA  PIC X.                                          00003050
             03 MNSOCC  PIC X.                                          00003060
             03 MNSOCP  PIC X.                                          00003070
             03 MNSOCH  PIC X.                                          00003080
             03 MNSOCV  PIC X.                                          00003090
             03 MNSOCO  PIC X(3).                                       00003100
             03 FILLER       PIC X(2).                                  00003110
             03 MNDEPOTA     PIC X.                                     00003120
             03 MNDEPOTC     PIC X.                                     00003130
             03 MNDEPOTP     PIC X.                                     00003140
             03 MNDEPOTH     PIC X.                                     00003150
             03 MNDEPOTV     PIC X.                                     00003160
             03 MNDEPOTO     PIC X(3).                                  00003170
             03 FILLER       PIC X(2).                                  00003180
             03 MCAPPROA     PIC X.                                     00003190
             03 MCAPPROC     PIC X.                                     00003200
             03 MCAPPROP     PIC X.                                     00003210
             03 MCAPPROH     PIC X.                                     00003220
             03 MCAPPROV     PIC X.                                     00003230
             03 MCAPPROO     PIC X(5).                                  00003240
             03 FILLER       PIC X(2).                                  00003250
             03 MLAPPROA     PIC X.                                     00003260
             03 MLAPPROC     PIC X.                                     00003270
             03 MLAPPROP     PIC X.                                     00003280
             03 MLAPPROH     PIC X.                                     00003290
             03 MLAPPROV     PIC X.                                     00003300
             03 MLAPPROO     PIC X(20).                                 00003310
             03 FILLER       PIC X(2).                                  00003320
             03 MWSAPPRA     PIC X.                                     00003330
             03 MWSAPPRC     PIC X.                                     00003340
             03 MWSAPPRP     PIC X.                                     00003350
             03 MWSAPPRH     PIC X.                                     00003360
             03 MWSAPPRV     PIC X.                                     00003370
             03 MWSAPPRO     PIC X.                                     00003380
             03 FILLER       PIC X(2).                                  00003390
             03 MCEXPOA      PIC X.                                     00003400
             03 MCEXPOC PIC X.                                          00003410
             03 MCEXPOP PIC X.                                          00003420
             03 MCEXPOH PIC X.                                          00003430
             03 MCEXPOV PIC X.                                          00003440
             03 MCEXPOO      PIC X(5).                                  00003450
             03 FILLER       PIC X(2).                                  00003460
             03 MLEXPOA      PIC X.                                     00003470
             03 MLEXPOC PIC X.                                          00003480
             03 MLEXPOP PIC X.                                          00003490
             03 MLEXPOH PIC X.                                          00003500
             03 MLEXPOV PIC X.                                          00003510
             03 MLEXPOO      PIC X(20).                                 00003520
             03 FILLER       PIC X(2).                                  00003530
             03 MLCOMPTA     PIC X.                                     00003540
             03 MLCOMPTC     PIC X.                                     00003550
             03 MLCOMPTP     PIC X.                                     00003560
             03 MLCOMPTH     PIC X.                                     00003570
             03 MLCOMPTV     PIC X.                                     00003580
             03 MLCOMPTO     PIC X(3).                                  00003590
           02 FILLER    PIC X(2).                                       00003600
           02 MZONCMDA  PIC X.                                          00003610
           02 MZONCMDC  PIC X.                                          00003620
           02 MZONCMDP  PIC X.                                          00003630
           02 MZONCMDH  PIC X.                                          00003640
           02 MZONCMDV  PIC X.                                          00003650
           02 MZONCMDO  PIC X(15).                                      00003660
           02 FILLER    PIC X(2).                                       00003670
           02 MLIBERRA  PIC X.                                          00003680
           02 MLIBERRC  PIC X.                                          00003690
           02 MLIBERRP  PIC X.                                          00003700
           02 MLIBERRH  PIC X.                                          00003710
           02 MLIBERRV  PIC X.                                          00003720
           02 MLIBERRO  PIC X(58).                                      00003730
           02 FILLER    PIC X(2).                                       00003740
           02 MCODTRAA  PIC X.                                          00003750
           02 MCODTRAC  PIC X.                                          00003760
           02 MCODTRAP  PIC X.                                          00003770
           02 MCODTRAH  PIC X.                                          00003780
           02 MCODTRAV  PIC X.                                          00003790
           02 MCODTRAO  PIC X(4).                                       00003800
           02 FILLER    PIC X(2).                                       00003810
           02 MCICSA    PIC X.                                          00003820
           02 MCICSC    PIC X.                                          00003830
           02 MCICSP    PIC X.                                          00003840
           02 MCICSH    PIC X.                                          00003850
           02 MCICSV    PIC X.                                          00003860
           02 MCICSO    PIC X(5).                                       00003870
           02 FILLER    PIC X(2).                                       00003880
           02 MNETNAMA  PIC X.                                          00003890
           02 MNETNAMC  PIC X.                                          00003900
           02 MNETNAMP  PIC X.                                          00003910
           02 MNETNAMH  PIC X.                                          00003920
           02 MNETNAMV  PIC X.                                          00003930
           02 MNETNAMO  PIC X(8).                                       00003940
           02 FILLER    PIC X(2).                                       00003950
           02 MSCREENA  PIC X.                                          00003960
           02 MSCREENC  PIC X.                                          00003970
           02 MSCREENP  PIC X.                                          00003980
           02 MSCREENH  PIC X.                                          00003990
           02 MSCREENV  PIC X.                                          00004000
           02 MSCREENO  PIC X(4).                                       00004010
           02 FILLER    PIC X(2).                                       00004020
           02 MPAGEA    PIC X.                                          00004030
           02 MPAGEC    PIC X.                                          00004040
           02 MPAGEP    PIC X.                                          00004050
           02 MPAGEH    PIC X.                                          00004060
           02 MPAGEV    PIC X.                                          00004070
           02 MPAGEO    PIC X(5).                                       00004080
                                                                                
