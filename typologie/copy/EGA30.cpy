      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA30   EGA30                                              00000020
      ***************************************************************** 00000030
       01   EGA30I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNCODICI  PIC X(7).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLREFI    PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCASSORL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCASSORL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCASSORF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCASSORI  PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLASSORL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLASSORL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLASSORF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLASSORI  PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCFAMI    PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLFAMI    PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPROL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAPPROF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCAPPROI  PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAPPROL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLAPPROF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLAPPROI  PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCMARQI   PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLMARQI   PIC X(20).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPOL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXPOF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCEXPOI   PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLEXPOL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLEXPOF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLEXPOI   PIC X(20).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCRECCL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MDCRECCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDCRECCF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MDCRECCI  PIC X(8).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MZONCMDI  PIC X(15).                                      00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLIBERRI  PIC X(58).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCODTRAI  PIC X(4).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCICSI    PIC X(5).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MNETNAMI  PIC X(8).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MSCREENI  PIC X(4).                                       00000930
      ***************************************************************** 00000940
      * SDF: EGA30   EGA30                                              00000950
      ***************************************************************** 00000960
       01   EGA30O REDEFINES EGA30I.                                    00000970
           02 FILLER    PIC X(12).                                      00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MDATJOUA  PIC X.                                          00001000
           02 MDATJOUC  PIC X.                                          00001010
           02 MDATJOUP  PIC X.                                          00001020
           02 MDATJOUH  PIC X.                                          00001030
           02 MDATJOUV  PIC X.                                          00001040
           02 MDATJOUO  PIC X(10).                                      00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MTIMJOUA  PIC X.                                          00001070
           02 MTIMJOUC  PIC X.                                          00001080
           02 MTIMJOUP  PIC X.                                          00001090
           02 MTIMJOUH  PIC X.                                          00001100
           02 MTIMJOUV  PIC X.                                          00001110
           02 MTIMJOUO  PIC X(5).                                       00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MWFONCA   PIC X.                                          00001140
           02 MWFONCC   PIC X.                                          00001150
           02 MWFONCP   PIC X.                                          00001160
           02 MWFONCH   PIC X.                                          00001170
           02 MWFONCV   PIC X.                                          00001180
           02 MWFONCO   PIC X(3).                                       00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MNCODICA  PIC X.                                          00001210
           02 MNCODICC  PIC X.                                          00001220
           02 MNCODICP  PIC X.                                          00001230
           02 MNCODICH  PIC X.                                          00001240
           02 MNCODICV  PIC X.                                          00001250
           02 MNCODICO  PIC X(7).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MLREFA    PIC X.                                          00001280
           02 MLREFC    PIC X.                                          00001290
           02 MLREFP    PIC X.                                          00001300
           02 MLREFH    PIC X.                                          00001310
           02 MLREFV    PIC X.                                          00001320
           02 MLREFO    PIC X(20).                                      00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MCASSORA  PIC X.                                          00001350
           02 MCASSORC  PIC X.                                          00001360
           02 MCASSORP  PIC X.                                          00001370
           02 MCASSORH  PIC X.                                          00001380
           02 MCASSORV  PIC X.                                          00001390
           02 MCASSORO  PIC X(5).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MLASSORA  PIC X.                                          00001420
           02 MLASSORC  PIC X.                                          00001430
           02 MLASSORP  PIC X.                                          00001440
           02 MLASSORH  PIC X.                                          00001450
           02 MLASSORV  PIC X.                                          00001460
           02 MLASSORO  PIC X(20).                                      00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MCFAMA    PIC X.                                          00001490
           02 MCFAMC    PIC X.                                          00001500
           02 MCFAMP    PIC X.                                          00001510
           02 MCFAMH    PIC X.                                          00001520
           02 MCFAMV    PIC X.                                          00001530
           02 MCFAMO    PIC X(5).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MLFAMA    PIC X.                                          00001560
           02 MLFAMC    PIC X.                                          00001570
           02 MLFAMP    PIC X.                                          00001580
           02 MLFAMH    PIC X.                                          00001590
           02 MLFAMV    PIC X.                                          00001600
           02 MLFAMO    PIC X(20).                                      00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MCAPPROA  PIC X.                                          00001630
           02 MCAPPROC  PIC X.                                          00001640
           02 MCAPPROP  PIC X.                                          00001650
           02 MCAPPROH  PIC X.                                          00001660
           02 MCAPPROV  PIC X.                                          00001670
           02 MCAPPROO  PIC X(5).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MLAPPROA  PIC X.                                          00001700
           02 MLAPPROC  PIC X.                                          00001710
           02 MLAPPROP  PIC X.                                          00001720
           02 MLAPPROH  PIC X.                                          00001730
           02 MLAPPROV  PIC X.                                          00001740
           02 MLAPPROO  PIC X(20).                                      00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MCMARQA   PIC X.                                          00001770
           02 MCMARQC   PIC X.                                          00001780
           02 MCMARQP   PIC X.                                          00001790
           02 MCMARQH   PIC X.                                          00001800
           02 MCMARQV   PIC X.                                          00001810
           02 MCMARQO   PIC X(5).                                       00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MLMARQA   PIC X.                                          00001840
           02 MLMARQC   PIC X.                                          00001850
           02 MLMARQP   PIC X.                                          00001860
           02 MLMARQH   PIC X.                                          00001870
           02 MLMARQV   PIC X.                                          00001880
           02 MLMARQO   PIC X(20).                                      00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MCEXPOA   PIC X.                                          00001910
           02 MCEXPOC   PIC X.                                          00001920
           02 MCEXPOP   PIC X.                                          00001930
           02 MCEXPOH   PIC X.                                          00001940
           02 MCEXPOV   PIC X.                                          00001950
           02 MCEXPOO   PIC X(5).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MLEXPOA   PIC X.                                          00001980
           02 MLEXPOC   PIC X.                                          00001990
           02 MLEXPOP   PIC X.                                          00002000
           02 MLEXPOH   PIC X.                                          00002010
           02 MLEXPOV   PIC X.                                          00002020
           02 MLEXPOO   PIC X(20).                                      00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MDCRECCA  PIC X.                                          00002050
           02 MDCRECCC  PIC X.                                          00002060
           02 MDCRECCP  PIC X.                                          00002070
           02 MDCRECCH  PIC X.                                          00002080
           02 MDCRECCV  PIC X.                                          00002090
           02 MDCRECCO  PIC X(8).                                       00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MZONCMDA  PIC X.                                          00002120
           02 MZONCMDC  PIC X.                                          00002130
           02 MZONCMDP  PIC X.                                          00002140
           02 MZONCMDH  PIC X.                                          00002150
           02 MZONCMDV  PIC X.                                          00002160
           02 MZONCMDO  PIC X(15).                                      00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MLIBERRA  PIC X.                                          00002190
           02 MLIBERRC  PIC X.                                          00002200
           02 MLIBERRP  PIC X.                                          00002210
           02 MLIBERRH  PIC X.                                          00002220
           02 MLIBERRV  PIC X.                                          00002230
           02 MLIBERRO  PIC X(58).                                      00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MCODTRAA  PIC X.                                          00002260
           02 MCODTRAC  PIC X.                                          00002270
           02 MCODTRAP  PIC X.                                          00002280
           02 MCODTRAH  PIC X.                                          00002290
           02 MCODTRAV  PIC X.                                          00002300
           02 MCODTRAO  PIC X(4).                                       00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MCICSA    PIC X.                                          00002330
           02 MCICSC    PIC X.                                          00002340
           02 MCICSP    PIC X.                                          00002350
           02 MCICSH    PIC X.                                          00002360
           02 MCICSV    PIC X.                                          00002370
           02 MCICSO    PIC X(5).                                       00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MNETNAMA  PIC X.                                          00002400
           02 MNETNAMC  PIC X.                                          00002410
           02 MNETNAMP  PIC X.                                          00002420
           02 MNETNAMH  PIC X.                                          00002430
           02 MNETNAMV  PIC X.                                          00002440
           02 MNETNAMO  PIC X(8).                                       00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MSCREENA  PIC X.                                          00002470
           02 MSCREENC  PIC X.                                          00002480
           02 MSCREENP  PIC X.                                          00002490
           02 MSCREENH  PIC X.                                          00002500
           02 MSCREENV  PIC X.                                          00002510
           02 MSCREENO  PIC X(4).                                       00002520
                                                                                
