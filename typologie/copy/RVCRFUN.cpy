      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CRFUN CODE ALGO                        *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCRFUN.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCRFUN.                                                             
      *}                                                                        
           05  CRFUN-CTABLEG2    PIC X(15).                                     
           05  CRFUN-CTABLEG2-REDEF REDEFINES CRFUN-CTABLEG2.                   
               10  CRFUN-CCTRL           PIC X(05).                             
               10  CRFUN-NSEQ            PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  CRFUN-NSEQ-N         REDEFINES CRFUN-NSEQ                    
                                         PIC 9(02).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  CRFUN-WTABLEG     PIC X(80).                                     
           05  CRFUN-WTABLEG-REDEF  REDEFINES CRFUN-WTABLEG.                    
               10  CRFUN-CFUNC           PIC X(05).                             
               10  CRFUN-PDEBUT          PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  CRFUN-PDEBUT-N       REDEFINES CRFUN-PDEBUT                  
                                         PIC 9(02).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  CRFUN-PLONG           PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  CRFUN-PLONG-N        REDEFINES CRFUN-PLONG                   
                                         PIC 9(02).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCRFUN-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCRFUN-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CRFUN-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CRFUN-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CRFUN-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CRFUN-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
