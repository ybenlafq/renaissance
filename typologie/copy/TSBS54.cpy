      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      *        IDENTIFICATION SERVICE - CODES CONTROLES               * 00000020
      ***************************************************************** 00000030
      *                                                               * 00000040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS-54-LONG          PIC S9(4) COMP VALUE +212.               00000050
      *--                                                                       
       01  TS-54-LONG          PIC S9(4) COMP-5 VALUE +212.                     
      *}                                                                        
       01  TS-54-DATA.                                                  00000060
           05 TS-54-LIGNE.                                              00000070
              10 TS-POSTE-TSBS54    OCCURS 2.                           00000080
                 15 TS-54-NSEQENT                PIC XX.                00000090
                 15 TS-54-NBRSEQENT              PIC 999.               00000100
                 15 TS-54-DATA-AV.                                      00000110
                    20 TS-54-LIEN-AV             PIC X.                 00000120
                    20 TS-54-CDOSS-AV            PIC X(05).             00000130
                    20 TS-54-LDOSS-AV            PIC X(20).             00000140
                    20 TS-54-NBRENT-AV           PIC XX.                00000150
                    20 TS-54-DATDEB-AV           PIC X(08).             00000160
                    20 TS-54-DATFIN-AV           PIC X(08).             00000170
                    20 TS-54-NSEQDOS-AV          PIC X(03).             00000180
                    20 TS-54-SUPP-AV             PIC X.                 00000190
                 15 TS-54-DATA-AP.                                      00000200
                    20 TS-54-LIEN-AP             PIC X.                 00000210
                    20 TS-54-CDOSS-AP            PIC X(05).             00000220
                    20 TS-54-LDOSS-AP            PIC X(20).             00000230
                    20 TS-54-NBRENT-AP           PIC XX.                00000240
                    20 TS-54-DATDEB-AP           PIC X(08).             00000250
                    20 TS-54-DATFIN-AP           PIC X(08).             00000260
                    20 TS-54-NSEQDOS-AP          PIC X(03).             00000270
                    20 TS-54-SUPP-AP             PIC X.                 00000280
                 15 TS-54-FLAG-CTRL.                                    00000290
                    20 TS-54-CDRET-CDOSS         PIC X(04).             00000300
                 15 TS-54-FLAG-MAJ.                                     00000310
                    20 TS-54-WMAJTS              PIC X(01).             00000320
                                                                                
