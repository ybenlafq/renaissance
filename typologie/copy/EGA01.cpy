      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA01   EGA01                                              00000020
      ***************************************************************** 00000030
       01   EGA01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 MDATJOUI  PIC X(10).                                      00000080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000090
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000100
           02 MTIMJOUI  PIC X(5).                                       00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTABGLL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MCTABGLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTABGLF  PIC X.                                          00000130
           02 MCTABGLI  PIC X(23).                                      00000140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000150
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 MWFONCI   PIC X(3).                                       00000200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTETL  COMP PIC S9(4).                                 00000210
      *--                                                                       
           02 MLENTETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLENTETF  PIC X.                                          00000220
           02 MLENTETI  PIC X(76).                                      00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 ML1L      COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 ML1L COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 ML1F      PIC X.                                          00000250
           02 ML1I      PIC X(76).                                      00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 ML2L      COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 ML2L COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 ML2F      PIC X.                                          00000280
           02 ML2I      PIC X(76).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 ML3L      COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 ML3L COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 ML3F      PIC X.                                          00000310
           02 ML3I      PIC X(76).                                      00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 ML4L      COMP PIC S9(4).                                 00000330
      *--                                                                       
           02 ML4L COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 ML4F      PIC X.                                          00000340
           02 ML4I      PIC X(76).                                      00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 ML5L      COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 ML5L COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 ML5F      PIC X.                                          00000370
           02 ML5I      PIC X(76).                                      00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 ML6L      COMP PIC S9(4).                                 00000390
      *--                                                                       
           02 ML6L COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 ML6F      PIC X.                                          00000400
           02 ML6I      PIC X(76).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 ML7L      COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 ML7L COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 ML7F      PIC X.                                          00000430
           02 ML7I      PIC X(76).                                      00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 ML8L      COMP PIC S9(4).                                 00000450
      *--                                                                       
           02 ML8L COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 ML8F      PIC X.                                          00000460
           02 ML8I      PIC X(76).                                      00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 ML9L      COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 ML9L COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 ML9F      PIC X.                                          00000490
           02 ML9I      PIC X(76).                                      00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 ML10L     COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 ML10L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 ML10F     PIC X.                                          00000520
           02 ML10I     PIC X(76).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 ML11L     COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 ML11L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 ML11F     PIC X.                                          00000550
           02 ML11I     PIC X(76).                                      00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 ML12L     COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 ML12L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 ML12F     PIC X.                                          00000580
           02 ML12I     PIC X(76).                                      00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 ML13L     COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 ML13L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 ML13F     PIC X.                                          00000610
           02 ML13I     PIC X(76).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 ML14L     COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 ML14L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 ML14F     PIC X.                                          00000640
           02 ML14I     PIC X(76).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000670
           02 MZONCMDI  PIC X(15).                                      00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000700
           02 MLIBERRI  PIC X(58).                                      00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000730
           02 MCODTRAI  PIC X(4).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000760
           02 MCICSI    PIC X(5).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000790
           02 MNETNAMI  PIC X(8).                                       00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000810
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000820
           02 MSCREENI  PIC X(4).                                       00000830
      ***************************************************************** 00000840
      * SDF: EGA01   EGA01                                              00000850
      ***************************************************************** 00000860
       01   EGA01O REDEFINES EGA01I.                                    00000870
           02 FILLER    PIC X(12).                                      00000880
           02 FILLER    PIC X(2).                                       00000890
           02 MDATJOUA  PIC X.                                          00000900
           02 MDATJOUO  PIC X(10).                                      00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MTIMJOUA  PIC X.                                          00000930
           02 MTIMJOUO  PIC X(5).                                       00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MCTABGLA  PIC X.                                          00000960
           02 MCTABGLO  PIC X(23).                                      00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MPAGEA    PIC X.                                          00000990
           02 MPAGEO    PIC X(3).                                       00001000
           02 FILLER    PIC X(2).                                       00001010
           02 MWFONCA   PIC X.                                          00001020
           02 MWFONCO   PIC X(3).                                       00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MLENTETA  PIC X.                                          00001050
           02 MLENTETO  PIC X(76).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 ML1A      PIC X.                                          00001080
           02 ML1O      PIC X(76).                                      00001090
           02 FILLER    PIC X(2).                                       00001100
           02 ML2A      PIC X.                                          00001110
           02 ML2O      PIC X(76).                                      00001120
           02 FILLER    PIC X(2).                                       00001130
           02 ML3A      PIC X.                                          00001140
           02 ML3O      PIC X(76).                                      00001150
           02 FILLER    PIC X(2).                                       00001160
           02 ML4A      PIC X.                                          00001170
           02 ML4O      PIC X(76).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 ML5A      PIC X.                                          00001200
           02 ML5O      PIC X(76).                                      00001210
           02 FILLER    PIC X(2).                                       00001220
           02 ML6A      PIC X.                                          00001230
           02 ML6O      PIC X(76).                                      00001240
           02 FILLER    PIC X(2).                                       00001250
           02 ML7A      PIC X.                                          00001260
           02 ML7O      PIC X(76).                                      00001270
           02 FILLER    PIC X(2).                                       00001280
           02 ML8A      PIC X.                                          00001290
           02 ML8O      PIC X(76).                                      00001300
           02 FILLER    PIC X(2).                                       00001310
           02 ML9A      PIC X.                                          00001320
           02 ML9O      PIC X(76).                                      00001330
           02 FILLER    PIC X(2).                                       00001340
           02 ML10A     PIC X.                                          00001350
           02 ML10O     PIC X(76).                                      00001360
           02 FILLER    PIC X(2).                                       00001370
           02 ML11A     PIC X.                                          00001380
           02 ML11O     PIC X(76).                                      00001390
           02 FILLER    PIC X(2).                                       00001400
           02 ML12A     PIC X.                                          00001410
           02 ML12O     PIC X(76).                                      00001420
           02 FILLER    PIC X(2).                                       00001430
           02 ML13A     PIC X.                                          00001440
           02 ML13O     PIC X(76).                                      00001450
           02 FILLER    PIC X(2).                                       00001460
           02 ML14A     PIC X.                                          00001470
           02 ML14O     PIC X(76).                                      00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MZONCMDA  PIC X.                                          00001500
           02 MZONCMDO  PIC X(15).                                      00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MLIBERRA  PIC X.                                          00001530
           02 MLIBERRO  PIC X(58).                                      00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MCODTRAA  PIC X.                                          00001560
           02 MCODTRAO  PIC X(4).                                       00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MCICSA    PIC X.                                          00001590
           02 MCICSO    PIC X(5).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MNETNAMA  PIC X.                                          00001620
           02 MNETNAMO  PIC X(8).                                       00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MSCREENA  PIC X.                                          00001650
           02 MSCREENO  PIC X(4).                                       00001660
                                                                                
