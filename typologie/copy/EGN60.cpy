      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF:parametragge des concurrents                                00000020
      ***************************************************************** 00000030
       01   EGN60I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(5).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(5).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(5).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(5).                                       00000200
           02 MPAGEMAXI      PIC X(4).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(5).                                       00000240
           02 MNSOCI    PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MINCONCLL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MINCONCLL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MINCONCLF      PIC X.                                     00000270
           02 FILLER    PIC X(5).                                       00000280
           02 MINCONCLI      PIC X(4).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIZONPRIXL     COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MIZONPRIXL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MIZONPRIXF     PIC X.                                     00000310
           02 FILLER    PIC X(5).                                       00000320
           02 MIZONPRIXI     PIC X(2).                                  00000330
           02 MNCONCLD OCCURS   14 TIMES .                              00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCONCLL     COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MNCONCLL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCONCLF     PIC X.                                     00000360
             03 FILLER  PIC X(5).                                       00000370
             03 MNCONCLI     PIC X(4).                                  00000380
           02 MLCONCLD OCCURS   14 TIMES .                              00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCONCLL     COMP PIC S9(4).                            00000400
      *--                                                                       
             03 MLCONCLL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLCONCLF     PIC X.                                     00000410
             03 FILLER  PIC X(5).                                       00000420
             03 MLCONCLI     PIC X(20).                                 00000430
           02 MLVILLED OCCURS   14 TIMES .                              00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLVILLEL     COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MLVILLEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLVILLEF     PIC X.                                     00000460
             03 FILLER  PIC X(5).                                       00000470
             03 MLVILLEI     PIC X(18).                                 00000480
           02 MNZONPRIXD OCCURS   14 TIMES .                            00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNZONPRIXL   COMP PIC S9(4).                            00000500
      *--                                                                       
             03 MNZONPRIXL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNZONPRIXF   PIC X.                                     00000510
             03 FILLER  PIC X(5).                                       00000520
             03 MNZONPRIXI   PIC X(2).                                  00000530
           02 MNCONCND OCCURS   14 TIMES .                              00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCONCNL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNCONCNL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCONCNF     PIC X.                                     00000560
             03 FILLER  PIC X(5).                                       00000570
             03 MNCONCNI     PIC X(4).                                  00000580
           02 MLCONCND OCCURS   14 TIMES .                              00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCONCNL     COMP PIC S9(4).                            00000600
      *--                                                                       
             03 MLCONCNL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLCONCNF     PIC X.                                     00000610
             03 FILLER  PIC X(5).                                       00000620
             03 MLCONCNI     PIC X(20).                                 00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000650
           02 FILLER    PIC X(5).                                       00000660
           02 MLIBERRI  PIC X(78).                                      00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000690
           02 FILLER    PIC X(5).                                       00000700
           02 MCODTRAI  PIC X(4).                                       00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000730
           02 FILLER    PIC X(5).                                       00000740
           02 MCICSI    PIC X(5).                                       00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000770
           02 FILLER    PIC X(5).                                       00000780
           02 MNETNAMI  PIC X(8).                                       00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000810
           02 FILLER    PIC X(5).                                       00000820
           02 MSCREENI  PIC X(4).                                       00000830
      ***************************************************************** 00000840
      * SDF:parametragge des concurrents                                00000850
      ***************************************************************** 00000860
       01   EGN60O REDEFINES EGN60I.                                    00000870
           02 FILLER    PIC X(12).                                      00000880
           02 FILLER    PIC X(2).                                       00000890
           02 MDATJOUA  PIC X.                                          00000900
           02 MDATJOUC  PIC X.                                          00000910
           02 MDATJOUP  PIC X.                                          00000920
           02 MDATJOUH  PIC X.                                          00000930
           02 MDATJOUV  PIC X.                                          00000940
           02 MDATJOUU  PIC X.                                          00000950
           02 MDATJOUO  PIC X(10).                                      00000960
           02 FILLER    PIC X(2).                                       00000970
           02 MTIMJOUA  PIC X.                                          00000980
           02 MTIMJOUC  PIC X.                                          00000990
           02 MTIMJOUP  PIC X.                                          00001000
           02 MTIMJOUH  PIC X.                                          00001010
           02 MTIMJOUV  PIC X.                                          00001020
           02 MTIMJOUU  PIC X.                                          00001030
           02 MTIMJOUO  PIC X(5).                                       00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MPAGEA    PIC X.                                          00001060
           02 MPAGEC    PIC X.                                          00001070
           02 MPAGEP    PIC X.                                          00001080
           02 MPAGEH    PIC X.                                          00001090
           02 MPAGEV    PIC X.                                          00001100
           02 MPAGEU    PIC X.                                          00001110
           02 MPAGEO    PIC X(3).                                       00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MPAGEMAXA      PIC X.                                     00001140
           02 MPAGEMAXC PIC X.                                          00001150
           02 MPAGEMAXP PIC X.                                          00001160
           02 MPAGEMAXH PIC X.                                          00001170
           02 MPAGEMAXV PIC X.                                          00001180
           02 MPAGEMAXU PIC X.                                          00001190
           02 MPAGEMAXO      PIC X(4).                                  00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MNSOCA    PIC X.                                          00001220
           02 MNSOCC    PIC X.                                          00001230
           02 MNSOCP    PIC X.                                          00001240
           02 MNSOCH    PIC X.                                          00001250
           02 MNSOCV    PIC X.                                          00001260
           02 MNSOCU    PIC X.                                          00001270
           02 MNSOCO    PIC X(3).                                       00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MINCONCLA      PIC X.                                     00001300
           02 MINCONCLC PIC X.                                          00001310
           02 MINCONCLP PIC X.                                          00001320
           02 MINCONCLH PIC X.                                          00001330
           02 MINCONCLV PIC X.                                          00001340
           02 MINCONCLU PIC X.                                          00001350
           02 MINCONCLO      PIC X(4).                                  00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MIZONPRIXA     PIC X.                                     00001380
           02 MIZONPRIXC     PIC X.                                     00001390
           02 MIZONPRIXP     PIC X.                                     00001400
           02 MIZONPRIXH     PIC X.                                     00001410
           02 MIZONPRIXV     PIC X.                                     00001420
           02 MIZONPRIXU     PIC X.                                     00001430
           02 MIZONPRIXO     PIC X(2).                                  00001440
           02 DFHMS1 OCCURS   14 TIMES .                                00001450
             03 FILLER       PIC X(2).                                  00001460
             03 MNCONCLA     PIC X.                                     00001470
             03 MNCONCLC     PIC X.                                     00001480
             03 MNCONCLP     PIC X.                                     00001490
             03 MNCONCLH     PIC X.                                     00001500
             03 MNCONCLV     PIC X.                                     00001510
             03 MNCONCLU     PIC X.                                     00001520
             03 MNCONCLO     PIC X(4).                                  00001530
           02 DFHMS2 OCCURS   14 TIMES .                                00001540
             03 FILLER       PIC X(2).                                  00001550
             03 MLCONCLA     PIC X.                                     00001560
             03 MLCONCLC     PIC X.                                     00001570
             03 MLCONCLP     PIC X.                                     00001580
             03 MLCONCLH     PIC X.                                     00001590
             03 MLCONCLV     PIC X.                                     00001600
             03 MLCONCLU     PIC X.                                     00001610
             03 MLCONCLO     PIC X(20).                                 00001620
           02 DFHMS3 OCCURS   14 TIMES .                                00001630
             03 FILLER       PIC X(2).                                  00001640
             03 MLVILLEA     PIC X.                                     00001650
             03 MLVILLEC     PIC X.                                     00001660
             03 MLVILLEP     PIC X.                                     00001670
             03 MLVILLEH     PIC X.                                     00001680
             03 MLVILLEV     PIC X.                                     00001690
             03 MLVILLEU     PIC X.                                     00001700
             03 MLVILLEO     PIC X(18).                                 00001710
           02 DFHMS4 OCCURS   14 TIMES .                                00001720
             03 FILLER       PIC X(2).                                  00001730
             03 MNZONPRIXA   PIC X.                                     00001740
             03 MNZONPRIXC   PIC X.                                     00001750
             03 MNZONPRIXP   PIC X.                                     00001760
             03 MNZONPRIXH   PIC X.                                     00001770
             03 MNZONPRIXV   PIC X.                                     00001780
             03 MNZONPRIXU   PIC X.                                     00001790
             03 MNZONPRIXO   PIC X(2).                                  00001800
           02 DFHMS5 OCCURS   14 TIMES .                                00001810
             03 FILLER       PIC X(2).                                  00001820
             03 MNCONCNA     PIC X.                                     00001830
             03 MNCONCNC     PIC X.                                     00001840
             03 MNCONCNP     PIC X.                                     00001850
             03 MNCONCNH     PIC X.                                     00001860
             03 MNCONCNV     PIC X.                                     00001870
             03 MNCONCNU     PIC X.                                     00001880
             03 MNCONCNO     PIC X(4).                                  00001890
           02 DFHMS6 OCCURS   14 TIMES .                                00001900
             03 FILLER       PIC X(2).                                  00001910
             03 MLCONCNA     PIC X.                                     00001920
             03 MLCONCNC     PIC X.                                     00001930
             03 MLCONCNP     PIC X.                                     00001940
             03 MLCONCNH     PIC X.                                     00001950
             03 MLCONCNV     PIC X.                                     00001960
             03 MLCONCNU     PIC X.                                     00001970
             03 MLCONCNO     PIC X(20).                                 00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MLIBERRA  PIC X.                                          00002000
           02 MLIBERRC  PIC X.                                          00002010
           02 MLIBERRP  PIC X.                                          00002020
           02 MLIBERRH  PIC X.                                          00002030
           02 MLIBERRV  PIC X.                                          00002040
           02 MLIBERRU  PIC X.                                          00002050
           02 MLIBERRO  PIC X(78).                                      00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MCODTRAA  PIC X.                                          00002080
           02 MCODTRAC  PIC X.                                          00002090
           02 MCODTRAP  PIC X.                                          00002100
           02 MCODTRAH  PIC X.                                          00002110
           02 MCODTRAV  PIC X.                                          00002120
           02 MCODTRAU  PIC X.                                          00002130
           02 MCODTRAO  PIC X(4).                                       00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MCICSA    PIC X.                                          00002160
           02 MCICSC    PIC X.                                          00002170
           02 MCICSP    PIC X.                                          00002180
           02 MCICSH    PIC X.                                          00002190
           02 MCICSV    PIC X.                                          00002200
           02 MCICSU    PIC X.                                          00002210
           02 MCICSO    PIC X(5).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MNETNAMA  PIC X.                                          00002240
           02 MNETNAMC  PIC X.                                          00002250
           02 MNETNAMP  PIC X.                                          00002260
           02 MNETNAMH  PIC X.                                          00002270
           02 MNETNAMV  PIC X.                                          00002280
           02 MNETNAMU  PIC X.                                          00002290
           02 MNETNAMO  PIC X(8).                                       00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MSCREENA  PIC X.                                          00002320
           02 MSCREENC  PIC X.                                          00002330
           02 MSCREENP  PIC X.                                          00002340
           02 MSCREENH  PIC X.                                          00002350
           02 MSCREENV  PIC X.                                          00002360
           02 MSCREENU  PIC X.                                          00002370
           02 MSCREENO  PIC X(4).                                       00002380
                                                                                
