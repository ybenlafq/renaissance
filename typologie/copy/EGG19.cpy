      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * PRIME VOLUME PAR FAMILLE                                        00000020
      ***************************************************************** 00000030
       01   EGG19I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFFETL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MDEFFETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEFFETF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MDEFFETI  PIC X(10).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCFAMI    PIC X(5).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLFAMI    PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPMAJL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MTYPMAJL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPMAJF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MTYPMAJI  PIC X.                                          00000290
           02 MLIGMKGI OCCURS   2 TIMES .                               00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBCMKGL    COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MLIBCMKGL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBCMKGF    PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MLIBCMKGI    PIC X(13).                                 00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARKL      COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MCMARKL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARKF      PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MCMARKI      PIC X(5).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMARKL      COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MLMARKL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLMARKF      PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MLMARKI      PIC X(20).                                 00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBVMKGL    COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MLIBVMKGL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBVMKGF    PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MLIBVMKGI    PIC X(8).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCVMARKL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MCVMARKL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCVMARKF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MCVMARKI     PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLVMARKL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MLVMARKL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLVMARKF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MLVMARKI     PIC X(20).                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBCODICL     COMP PIC S9(4).                            00000550
      *--                                                                       
           02 MLIBCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIBCODICF     PIC X.                                     00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MLIBCODICI     PIC X(7).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSUPL    COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MCSUPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCSUPF    PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MCSUPI    PIC X.                                          00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MNCODICI  PIC X(7).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCMARQI   PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MLREFI    PIC X(20).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGE1L      COMP PIC S9(4).                            00000750
      *--                                                                       
           02 MNBPAGE1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGE1F      PIC X.                                     00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MNBPAGE1I      PIC X(3).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGE2L      COMP PIC S9(4).                            00000790
      *--                                                                       
           02 MNBPAGE2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGE2F      PIC X.                                     00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MNBPAGE2I      PIC X(3).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBLIG1L      COMP PIC S9(4).                            00000830
      *--                                                                       
           02 MLIBLIG1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBLIG1F      PIC X.                                     00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MLIBLIG1I      PIC X(40).                                 00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBLIG2L      COMP PIC S9(4).                            00000870
      *--                                                                       
           02 MLIBLIG2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBLIG2F      PIC X.                                     00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MLIBLIG2I      PIC X(40).                                 00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MASTERL   COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MASTERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MASTERF   PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MASTERI   PIC X.                                          00000940
           02 MLIGNESI OCCURS   10 TIMES .                              00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNZONPL      COMP PIC S9(4).                            00000960
      *--                                                                       
             03 MNZONPL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNZONPF      PIC X.                                     00000970
             03 FILLER  PIC X(4).                                       00000980
             03 MNZONPI      PIC X(2).                                  00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPPL  COMP PIC S9(4).                                 00001000
      *--                                                                       
             03 MTYPPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTYPPF  PIC X.                                          00001010
             03 FILLER  PIC X(4).                                       00001020
             03 MTYPPI  PIC X.                                          00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMTPL   COMP PIC S9(4).                                 00001040
      *--                                                                       
             03 MMTPL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MMTPF   PIC X.                                          00001050
             03 FILLER  PIC X(4).                                       00001060
             03 MMTPI   PIC X(6).                                       00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPXVL   COMP PIC S9(4).                                 00001080
      *--                                                                       
             03 MPXVL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MPXVF   PIC X.                                          00001090
             03 FILLER  PIC X(4).                                       00001100
             03 MPXVI   PIC X(9).                                       00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMBUL   COMP PIC S9(4).                                 00001120
      *--                                                                       
             03 MMBUL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MMBUF   PIC X.                                          00001130
             03 FILLER  PIC X(4).                                       00001140
             03 MMBUI   PIC X(8).                                       00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTXMBUL      COMP PIC S9(4).                            00001160
      *--                                                                       
             03 MTXMBUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MTXMBUF      PIC X.                                     00001170
             03 FILLER  PIC X(4).                                       00001180
             03 MTXMBUI      PIC X(5).                                  00001190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPCOMML      COMP PIC S9(4).                            00001200
      *--                                                                       
             03 MPCOMML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPCOMMF      PIC X.                                     00001210
             03 FILLER  PIC X(4).                                       00001220
             03 MPCOMMI      PIC X(5).                                  00001230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFMILL  COMP PIC S9(4).                                 00001240
      *--                                                                       
             03 MFMILL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MFMILF  PIC X.                                          00001250
             03 FILLER  PIC X(4).                                       00001260
             03 MFMILI  PIC X(6).                                       00001270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001280
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001290
           02 FILLER    PIC X(4).                                       00001300
           02 MZONCMDI  PIC X(15).                                      00001310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001320
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001330
           02 FILLER    PIC X(4).                                       00001340
           02 MLIBERRI  PIC X(58).                                      00001350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001360
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001370
           02 FILLER    PIC X(4).                                       00001380
           02 MCODTRAI  PIC X(4).                                       00001390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001400
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001410
           02 FILLER    PIC X(4).                                       00001420
           02 MCICSI    PIC X(5).                                       00001430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001440
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001450
           02 FILLER    PIC X(4).                                       00001460
           02 MNETNAMI  PIC X(8).                                       00001470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001480
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001490
           02 FILLER    PIC X(4).                                       00001500
           02 MSCREENI  PIC X(4).                                       00001510
      ***************************************************************** 00001520
      * PRIME VOLUME PAR FAMILLE                                        00001530
      ***************************************************************** 00001540
       01   EGG19O REDEFINES EGG19I.                                    00001550
           02 FILLER    PIC X(12).                                      00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MDATJOUA  PIC X.                                          00001580
           02 MDATJOUC  PIC X.                                          00001590
           02 MDATJOUP  PIC X.                                          00001600
           02 MDATJOUH  PIC X.                                          00001610
           02 MDATJOUV  PIC X.                                          00001620
           02 MDATJOUO  PIC X(10).                                      00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MTIMJOUA  PIC X.                                          00001650
           02 MTIMJOUC  PIC X.                                          00001660
           02 MTIMJOUP  PIC X.                                          00001670
           02 MTIMJOUH  PIC X.                                          00001680
           02 MTIMJOUV  PIC X.                                          00001690
           02 MTIMJOUO  PIC X(5).                                       00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MDEFFETA  PIC X.                                          00001720
           02 MDEFFETC  PIC X.                                          00001730
           02 MDEFFETP  PIC X.                                          00001740
           02 MDEFFETH  PIC X.                                          00001750
           02 MDEFFETV  PIC X.                                          00001760
           02 MDEFFETO  PIC X(10).                                      00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MCFAMA    PIC X.                                          00001790
           02 MCFAMC    PIC X.                                          00001800
           02 MCFAMP    PIC X.                                          00001810
           02 MCFAMH    PIC X.                                          00001820
           02 MCFAMV    PIC X.                                          00001830
           02 MCFAMO    PIC X(5).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MLFAMA    PIC X.                                          00001860
           02 MLFAMC    PIC X.                                          00001870
           02 MLFAMP    PIC X.                                          00001880
           02 MLFAMH    PIC X.                                          00001890
           02 MLFAMV    PIC X.                                          00001900
           02 MLFAMO    PIC X(20).                                      00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MTYPMAJA  PIC X.                                          00001930
           02 MTYPMAJC  PIC X.                                          00001940
           02 MTYPMAJP  PIC X.                                          00001950
           02 MTYPMAJH  PIC X.                                          00001960
           02 MTYPMAJV  PIC X.                                          00001970
           02 MTYPMAJO  PIC X.                                          00001980
           02 MLIGMKGO OCCURS   2 TIMES .                               00001990
             03 FILLER       PIC X(2).                                  00002000
             03 MLIBCMKGA    PIC X.                                     00002010
             03 MLIBCMKGC    PIC X.                                     00002020
             03 MLIBCMKGP    PIC X.                                     00002030
             03 MLIBCMKGH    PIC X.                                     00002040
             03 MLIBCMKGV    PIC X.                                     00002050
             03 MLIBCMKGO    PIC X(13).                                 00002060
             03 FILLER       PIC X(2).                                  00002070
             03 MCMARKA      PIC X.                                     00002080
             03 MCMARKC PIC X.                                          00002090
             03 MCMARKP PIC X.                                          00002100
             03 MCMARKH PIC X.                                          00002110
             03 MCMARKV PIC X.                                          00002120
             03 MCMARKO      PIC X(5).                                  00002130
             03 FILLER       PIC X(2).                                  00002140
             03 MLMARKA      PIC X.                                     00002150
             03 MLMARKC PIC X.                                          00002160
             03 MLMARKP PIC X.                                          00002170
             03 MLMARKH PIC X.                                          00002180
             03 MLMARKV PIC X.                                          00002190
             03 MLMARKO      PIC X(20).                                 00002200
             03 FILLER       PIC X(2).                                  00002210
             03 MLIBVMKGA    PIC X.                                     00002220
             03 MLIBVMKGC    PIC X.                                     00002230
             03 MLIBVMKGP    PIC X.                                     00002240
             03 MLIBVMKGH    PIC X.                                     00002250
             03 MLIBVMKGV    PIC X.                                     00002260
             03 MLIBVMKGO    PIC X(8).                                  00002270
             03 FILLER       PIC X(2).                                  00002280
             03 MCVMARKA     PIC X.                                     00002290
             03 MCVMARKC     PIC X.                                     00002300
             03 MCVMARKP     PIC X.                                     00002310
             03 MCVMARKH     PIC X.                                     00002320
             03 MCVMARKV     PIC X.                                     00002330
             03 MCVMARKO     PIC X(5).                                  00002340
             03 FILLER       PIC X(2).                                  00002350
             03 MLVMARKA     PIC X.                                     00002360
             03 MLVMARKC     PIC X.                                     00002370
             03 MLVMARKP     PIC X.                                     00002380
             03 MLVMARKH     PIC X.                                     00002390
             03 MLVMARKV     PIC X.                                     00002400
             03 MLVMARKO     PIC X(20).                                 00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MLIBCODICA     PIC X.                                     00002430
           02 MLIBCODICC     PIC X.                                     00002440
           02 MLIBCODICP     PIC X.                                     00002450
           02 MLIBCODICH     PIC X.                                     00002460
           02 MLIBCODICV     PIC X.                                     00002470
           02 MLIBCODICO     PIC X(7).                                  00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MCSUPA    PIC X.                                          00002500
           02 MCSUPC    PIC X.                                          00002510
           02 MCSUPP    PIC X.                                          00002520
           02 MCSUPH    PIC X.                                          00002530
           02 MCSUPV    PIC X.                                          00002540
           02 MCSUPO    PIC X.                                          00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MNCODICA  PIC X.                                          00002570
           02 MNCODICC  PIC X.                                          00002580
           02 MNCODICP  PIC X.                                          00002590
           02 MNCODICH  PIC X.                                          00002600
           02 MNCODICV  PIC X.                                          00002610
           02 MNCODICO  PIC X(7).                                       00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MCMARQA   PIC X.                                          00002640
           02 MCMARQC   PIC X.                                          00002650
           02 MCMARQP   PIC X.                                          00002660
           02 MCMARQH   PIC X.                                          00002670
           02 MCMARQV   PIC X.                                          00002680
           02 MCMARQO   PIC X(5).                                       00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MLREFA    PIC X.                                          00002710
           02 MLREFC    PIC X.                                          00002720
           02 MLREFP    PIC X.                                          00002730
           02 MLREFH    PIC X.                                          00002740
           02 MLREFV    PIC X.                                          00002750
           02 MLREFO    PIC X(20).                                      00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MNBPAGE1A      PIC X.                                     00002780
           02 MNBPAGE1C PIC X.                                          00002790
           02 MNBPAGE1P PIC X.                                          00002800
           02 MNBPAGE1H PIC X.                                          00002810
           02 MNBPAGE1V PIC X.                                          00002820
           02 MNBPAGE1O      PIC X(3).                                  00002830
           02 FILLER    PIC X(2).                                       00002840
           02 MNBPAGE2A      PIC X.                                     00002850
           02 MNBPAGE2C PIC X.                                          00002860
           02 MNBPAGE2P PIC X.                                          00002870
           02 MNBPAGE2H PIC X.                                          00002880
           02 MNBPAGE2V PIC X.                                          00002890
           02 MNBPAGE2O      PIC X(3).                                  00002900
           02 FILLER    PIC X(2).                                       00002910
           02 MLIBLIG1A      PIC X.                                     00002920
           02 MLIBLIG1C PIC X.                                          00002930
           02 MLIBLIG1P PIC X.                                          00002940
           02 MLIBLIG1H PIC X.                                          00002950
           02 MLIBLIG1V PIC X.                                          00002960
           02 MLIBLIG1O      PIC X(40).                                 00002970
           02 FILLER    PIC X(2).                                       00002980
           02 MLIBLIG2A      PIC X.                                     00002990
           02 MLIBLIG2C PIC X.                                          00003000
           02 MLIBLIG2P PIC X.                                          00003010
           02 MLIBLIG2H PIC X.                                          00003020
           02 MLIBLIG2V PIC X.                                          00003030
           02 MLIBLIG2O      PIC X(40).                                 00003040
           02 FILLER    PIC X(2).                                       00003050
           02 MASTERA   PIC X.                                          00003060
           02 MASTERC   PIC X.                                          00003070
           02 MASTERP   PIC X.                                          00003080
           02 MASTERH   PIC X.                                          00003090
           02 MASTERV   PIC X.                                          00003100
           02 MASTERO   PIC X.                                          00003110
           02 MLIGNESO OCCURS   10 TIMES .                              00003120
             03 FILLER       PIC X(2).                                  00003130
             03 MNZONPA      PIC X.                                     00003140
             03 MNZONPC PIC X.                                          00003150
             03 MNZONPP PIC X.                                          00003160
             03 MNZONPH PIC X.                                          00003170
             03 MNZONPV PIC X.                                          00003180
             03 MNZONPO      PIC X(2).                                  00003190
             03 FILLER       PIC X(2).                                  00003200
             03 MTYPPA  PIC X.                                          00003210
             03 MTYPPC  PIC X.                                          00003220
             03 MTYPPP  PIC X.                                          00003230
             03 MTYPPH  PIC X.                                          00003240
             03 MTYPPV  PIC X.                                          00003250
             03 MTYPPO  PIC X.                                          00003260
             03 FILLER       PIC X(2).                                  00003270
             03 MMTPA   PIC X.                                          00003280
             03 MMTPC   PIC X.                                          00003290
             03 MMTPP   PIC X.                                          00003300
             03 MMTPH   PIC X.                                          00003310
             03 MMTPV   PIC X.                                          00003320
             03 MMTPO   PIC X(6).                                       00003330
             03 FILLER       PIC X(2).                                  00003340
             03 MPXVA   PIC X.                                          00003350
             03 MPXVC   PIC X.                                          00003360
             03 MPXVP   PIC X.                                          00003370
             03 MPXVH   PIC X.                                          00003380
             03 MPXVV   PIC X.                                          00003390
             03 MPXVO   PIC X(9).                                       00003400
             03 FILLER       PIC X(2).                                  00003410
             03 MMBUA   PIC X.                                          00003420
             03 MMBUC   PIC X.                                          00003430
             03 MMBUP   PIC X.                                          00003440
             03 MMBUH   PIC X.                                          00003450
             03 MMBUV   PIC X.                                          00003460
             03 MMBUO   PIC X(8).                                       00003470
             03 FILLER       PIC X(2).                                  00003480
             03 MTXMBUA      PIC X.                                     00003490
             03 MTXMBUC PIC X.                                          00003500
             03 MTXMBUP PIC X.                                          00003510
             03 MTXMBUH PIC X.                                          00003520
             03 MTXMBUV PIC X.                                          00003530
             03 MTXMBUO      PIC X(5).                                  00003540
             03 FILLER       PIC X(2).                                  00003550
             03 MPCOMMA      PIC X.                                     00003560
             03 MPCOMMC PIC X.                                          00003570
             03 MPCOMMP PIC X.                                          00003580
             03 MPCOMMH PIC X.                                          00003590
             03 MPCOMMV PIC X.                                          00003600
             03 MPCOMMO      PIC X(5).                                  00003610
             03 FILLER       PIC X(2).                                  00003620
             03 MFMILA  PIC X.                                          00003630
             03 MFMILC  PIC X.                                          00003640
             03 MFMILP  PIC X.                                          00003650
             03 MFMILH  PIC X.                                          00003660
             03 MFMILV  PIC X.                                          00003670
             03 MFMILO  PIC X(6).                                       00003680
           02 FILLER    PIC X(2).                                       00003690
           02 MZONCMDA  PIC X.                                          00003700
           02 MZONCMDC  PIC X.                                          00003710
           02 MZONCMDP  PIC X.                                          00003720
           02 MZONCMDH  PIC X.                                          00003730
           02 MZONCMDV  PIC X.                                          00003740
           02 MZONCMDO  PIC X(15).                                      00003750
           02 FILLER    PIC X(2).                                       00003760
           02 MLIBERRA  PIC X.                                          00003770
           02 MLIBERRC  PIC X.                                          00003780
           02 MLIBERRP  PIC X.                                          00003790
           02 MLIBERRH  PIC X.                                          00003800
           02 MLIBERRV  PIC X.                                          00003810
           02 MLIBERRO  PIC X(58).                                      00003820
           02 FILLER    PIC X(2).                                       00003830
           02 MCODTRAA  PIC X.                                          00003840
           02 MCODTRAC  PIC X.                                          00003850
           02 MCODTRAP  PIC X.                                          00003860
           02 MCODTRAH  PIC X.                                          00003870
           02 MCODTRAV  PIC X.                                          00003880
           02 MCODTRAO  PIC X(4).                                       00003890
           02 FILLER    PIC X(2).                                       00003900
           02 MCICSA    PIC X.                                          00003910
           02 MCICSC    PIC X.                                          00003920
           02 MCICSP    PIC X.                                          00003930
           02 MCICSH    PIC X.                                          00003940
           02 MCICSV    PIC X.                                          00003950
           02 MCICSO    PIC X(5).                                       00003960
           02 FILLER    PIC X(2).                                       00003970
           02 MNETNAMA  PIC X.                                          00003980
           02 MNETNAMC  PIC X.                                          00003990
           02 MNETNAMP  PIC X.                                          00004000
           02 MNETNAMH  PIC X.                                          00004010
           02 MNETNAMV  PIC X.                                          00004020
           02 MNETNAMO  PIC X(8).                                       00004030
           02 FILLER    PIC X(2).                                       00004040
           02 MSCREENA  PIC X.                                          00004050
           02 MSCREENC  PIC X.                                          00004060
           02 MSCREENP  PIC X.                                          00004070
           02 MSCREENH  PIC X.                                          00004080
           02 MSCREENV  PIC X.                                          00004090
           02 MSCREENO  PIC X(4).                                       00004100
                                                                                
