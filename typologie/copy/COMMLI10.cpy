      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TLI01 (TLI10 -> MENU)    TR: LI10  *    00002201
      *                 DEFINITION DES LIEUX                       *    00002301
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00009000
      *                                                                 00410100
      *            TRANSACTION LI01 : DEFINITION DES LIEUX            * 00411001
      *                                                                 00412000
      **************************************************************    00413001
      *   DATES    * USERID * NOMS            * FAIRE FIND              00414001
      ******************************************************************00415001
      * 24/09/2013 * DSA004 * Y.SCOPIN        * YS2493                  00416001
      *-----------------------------------------------------------------00417001
      * LA TRANSACTION LV00 APPELLE LE PROGRAMME TLI13                  00418001
      * AJOUT DE CERTAINES ZONES SPECIFIQUES :                          00419001
      *    CODE PAYS                                                    00420001
      *    OP REGROUPABLE                                               00430001
      *    CODE LANGUE                                                  00440001
      *    TYPE DE CLIENT                                               00450001
      *    CONTRAINTE DE COLISAGE                                       00460001
      *    CODE TRANSPORTEUR                                            00470001
      *    TYPE DE REGROUPEMENT UNIVERS                                 00480001
      ******************************************************************00490001
      * 16/01/2014 * BF1                                                00500001
      *-----------------------------------------------------------------00510001
      * AJOUT DES COORDONNéES GPS                                       00520001
      ******************************************************************00530001
YS2534* 25/03/2014 * Y.SCOPIN (DSA004)                                  00531001
YS2534*-----------------------------------------------------------------00532001
YS2534* AJOUT DES ZONES POUR LE PROGRAMME TLI22                         00533001
      ******************************************************************00534001
      * COMMAREA SPECIFIQUE PRG TLI10 (MENU)             TR: LI10      *00540001
      *                          TLI11                                 *00550001
      *                           TLI12                                *00560001
      *                            TLI13                               *00570001
YS2534*                             TLI22                              *00571001
      *                                                                *00580001
      ******************************************************************00590001
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00600001
      **************************************************************    00610001
      *                                                                 00620001
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00630001
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00640001
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00650001
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00660001
      *                                                                 00670001
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00680001
      * COMPRENANT :                                                    00690001
      * 1 - LES ZONES RESERVEES A AIDA                                  00700001
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00710001
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00720001
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00730001
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00740001
      *                                                                 00750001
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00760001
      * PAR AIDA                                                        00770001
      *                                                                 00780001
      *-------------------------------------------------------------    00790001
      *                                                                 00800001
      *01  COM-LI01-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00810001
      *                                                                 00820001
       01  Z-COMMAREA.                                                  00830001
      *                                                                 00840001
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00850001
          02 FILLER-COM-AIDA      PIC X(100).                           00860001
      *                                                                 00870001
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00880001
          02 COMM-CICS-APPLID     PIC X(8).                             00890001
          02 COMM-CICS-NETNAM     PIC X(8).                             00900001
          02 COMM-CICS-TRANSA     PIC X(4).                             00910001
NV0309    02 COMM-CICS-TSSACIDA   PIC X(7).                             00920001
      *                                                                 00930001
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00940001
          02 COMM-DATE-SIECLE     PIC XX.                               00950001
          02 COMM-DATE-ANNEE      PIC XX.                               00960001
          02 COMM-DATE-MOIS       PIC XX.                               00970001
          02 COMM-DATE-JOUR       PIC XX.                               00980001
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00990001
          02 COMM-DATE-QNTA       PIC 999.                              01000001
          02 COMM-DATE-QNT0       PIC 99999.                            01010001
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  01020001
          02 COMM-DATE-BISX       PIC 9.                                01030001
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           01040001
          02 COMM-DATE-JSM        PIC 9.                                01050001
      *   LIBELLES DU JOUR COURT - LONG                                 01060001
          02 COMM-DATE-JSM-LC     PIC XXX.                              01070001
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                         01080001
      *   LIBELLES DU MOIS COURT - LONG                                 01090001
          02 COMM-DATE-MOIS-LC    PIC XXX.                              01100001
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                         01110001
      *   DIFFERENTES FORMES DE DATE                                    01120001
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                             01130001
          02 COMM-DATE-AAMMJJ     PIC X(6).                             01140001
          02 COMM-DATE-JJMMSSAA   PIC X(8).                             01150001
          02 COMM-DATE-JJMMAA     PIC X(6).                             01160001
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                             01170001
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                            01180001
      *   DIFFERENTES FORMES DE DATE                                    01190001
          02 COMM-DATE-FILLER     PIC X(14).                            01200001
      *                                                                 01210001
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  01220001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.              01230001
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                        01240001
      *                                                                 01250001
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  01260001
      *                                                                 01270001
      *            TRANSACTION LI10 : ADMINISTRATION DES DONNEES      * 01280001
      *                                                                 01290001
      ***************************************************************** 01290101
          02 COMM-LI10-APPLI .                                          01290201
      *------------------------------ ZONE DONNEES TLI01                01290301
             03 COMM-LI10-DONNEES-TLI10.                                01290401
      *------------------------------ CODE ZONE COMANDE                 01290501
                05 COMM-LI10-ZONCMD         PIC X.                      01290601
      *------------------------------ CODE PARAMTRE LIEU                01290701
                05 COMM-LI10-CPARAM         PIC X(05).                  01290801
      *------------------------------ TRANSAACTION LI11.                01291104
             03 COMM-LI10-DONNEES-TLI11.                                01291204
      *------------------------------ PAGE MAX                          01291304
                05 COMM-LI11-PAGE-MAX       PIC 9(03).                  01291404
      *------------------------------ NUMERO PAGE                       01291504
                05 COMM-LI11-NUMPAG         PIC 9(03).                  01291604
      *------------------------------ CODE TRI                          01291704
                05 COMM-LI11-POS-MAX        PIC 9(03).                  01291804
      *------------------------------ NUMERO ITEM TS                    01291904
                05 COMM-LI11-ITEM           PIC 9(03).                  01292004
YS2534*      03 COMM-LI10-DONNEES-TLI11.                                01292107
YS2534       03 COMM-LI10-DONNEES-TLI12.                                01292207
      *------------------------------ PAGE MAX                          01292307
                05 COMM-LI12-PAGE-MAX       PIC 9(03).                  01292407
      *------------------------------ PAGE MAX EXISTANT LOR 1° LECTURE  01292507
                05 COMM-LI12-PAGE-EXIST     PIC 9(03).                  01292607
      *------------------------------ NUMERO PAGE                       01292707
                05 COMM-LI12-NUMPAG         PIC 9(03).                  01292807
      *------------------------------ CODE TRI                          01292907
                05 COMM-LI12-POS-MAX        PIC 9(03).                  01293007
      *------------------------------ DERNIER NOLIGNE REMPLIE DE LA TS. 01293107
                05 COMM-LI12-NOLIGNE        PIC 9(03).                  01293207
      *------------------------------ TS QUI EST SUR L'ECRAN            01293307
                05   COMM-LI12-TS-LIGNE .                               01294001
                  08 COMM-LI12-LIGNE         OCCURS 15.                 01295001
                   10 COMM-LI12-NSOC           PIC X(3).                01296001
                   10 COMM-LI12-NLIEU          PIC X(3).                01297001
                   10 COMM-LI12-LVPARAM        PIC X(20).               01297101
      *------------------------------ ZONES ECRAN                       01297201
                05   COMM-LI12-ECR-LIGNE .                              01297301
                  08 COMM-LI12-ELIGNE         OCCURS 15.                01297401
                   10 COMM-LI12-ENSOC           PIC X(3).               01297501
                   10 COMM-LI12-ENLIEU          PIC X(3).               01297601
                   10 COMM-LI12-ELVPARAM        PIC X(20).              01297701
      *------------------------------ ZONE LIBRE                        01297801
AD5   *      03 COMM-LI10-LIBRE          PIC X(2000).                   01297901
      *                                                                 01298001
      *------------------------------ ZONES UTILISEES PAR LE TLI13      01298101
AD5          03 COMM-LI13.                                              01298201
                05 COMM-LI13-NSOC        PIC X(03).                     01298301
                05 COMM-LI13-NLIEU       PIC X(03).                     01298401
                05 COMM-LI13-WTYPEADR    PIC X(01).                     01298501
                05 COMM-LI13-CTYPLIEU    PIC X(01).                     01298601
                05 COMM-LI13-LLIEU       PIC X(50).                     01298701
                05 COMM-LI13-LADR1       PIC X(50).                     01298801
                05 COMM-LI13-LADR2       PIC X(50).                     01298901
                05 COMM-LI13-LADR3       PIC X(50).                     01299001
                05 COMM-LI13-LCOMMUNE    PIC X(50).                     01299101
                05 COMM-LI13-CPOSTAL     PIC X(05).                     01299201
                05 COMM-LI13-LBUREAU     PIC X(50).                     01299301
                05 COMM-LI13-CDEPT       PIC X(02).                     01299401
                05 COMM-LI13-TEL         PIC X(15).                     01299501
                05 COMM-LI13-FAX         PIC X(15).                     01299601
                05 COMM-LI13-QDELAIAPPRO PIC X(03).                     01299701
                05 COMM-LI13-CINSEE      PIC X(05).                     01299801
YS2493          05 COMM-LI13-CPAYS         PIC X(03).                   01299901
YS2493          05 COMM-LI13-CLANGUE       PIC X(03).                   01300001
YS2493          05 COMM-LI13-CTYPCLIEN     PIC X(06).                   01300101
YS2493          05 COMM-LI13-CREGROUPABLE  PIC X(01).                   01300201
YS2493          05 COMM-LI13-CCOLISAGE     PIC 9(03).                   01300301
YS2493          05 COMM-LI13-CUNIVERS      PIC X(05).                   01300401
YS2493          05 COMM-LI13-CTRANSPORTEUR PIC X(13).                   01300501
YS2493          05 COMM-LI13-CFACONNAGE    PIC 9(02).                   01300601
BF1             05 COMM-LI13-GPSLAT      PIC S9(03)V9(10).              01300702
BF1             05 COMM-LI13-GPSLON      PIC S9(03)V9(10).              01300803
YS2534          05 COMM-LI13-LIBRE       PIC X(1438).                   01300902
      *                                                                 01301001
YS2534       03 COMM-LI10-DONNEES-TLI22 REDEFINES COMM-LI13.            01301102
  |   *------------------------------ CODE PARAMETRE                    01301206
  |             05 COMM-LI22-CPARAM         PIC X(05).                  01301306
  |   *------------------------------ LIBELLE PARAMETRE                 01301408
  |             05 COMM-LI22-LPARAM         PIC X(20).                  01301508
  |   *------------------------------ PAGE MAX                          01301608
  |             05 COMM-LI22-PAGE-MAX       PIC 9(03).                  01301708
  |   *------------------------------ PAGE MAX EXISTANT LOR 1° LECTURE  01301808
  |             05 COMM-LI22-PAGE-EXIST     PIC 9(03).                  01301908
  |   *------------------------------ NUMERO PAGE                       01302008
  |             05 COMM-LI22-NUMPAG         PIC 9(03).                  01302108
  |   *------------------------------ CODE TRI                          01302208
  |             05 COMM-LI22-POS-MAX        PIC 9(03).                  01302308
  |   *------------------------------ DERNIER NOLIGNE REMPLIE DE LA TS. 01302408
  |             05 COMM-LI22-NOLIGNE        PIC 9(03).                  01302508
  |   *------------------------------ TS QUI EST SUR L'ECRAN            01302608
  |             05   COMM-LI22-TS-LIGNE .                               01302708
  |               08 COMM-LI22-LIGNE         OCCURS 15.                 01302808
  |                10 COMM-LI22-NSOCO          PIC X(3).                01302908
  |                10 COMM-LI22-NLIEUO         PIC X(3).                01303008
  |                10 COMM-LI22-SSLIEO         PIC X(5).                01303108
  |                10 COMM-LI22-NSOCD          PIC X(3).                01303208
  |                10 COMM-LI22-NLIEUD         PIC X(3).                01303308
  |                10 COMM-LI22-SSLIED         PIC X(5).                01303408
  |                10 COMM-LI22-LVPARAM        PIC X(20).               01303508
  |   *------------------------------ ZONES ECRAN                       01303608
  |             05   COMM-LI22-ECR-LIGNE .                              01303708
  |               08 COMM-LI22-ELIGNE         OCCURS 15.                01303808
  |                10 COMM-LI22-ENSOCO          PIC X(3).               01303908
  |                10 COMM-LI22-ENLIEUO         PIC X(3).               01304008
  |                10 COMM-LI22-ESSLIEO         PIC X(5).               01304108
  |                10 COMM-LI22-ENSOCD          PIC X(3).               01304208
  |                10 COMM-LI22-ENLIEUD         PIC X(3).               01304308
  |                10 COMM-LI22-ESSLIED         PIC X(5).               01304408
  |                10 COMM-LI22-ELVPARAM        PIC X(20).              01304508
YS2534          05 COMM-LI22-LIBRE              PIC X(553).             01304608
BF1   *YS253403 COMM-LI13-LIBRE       PIC X(1438).                      01304708
YS2493*BF1   03 COMM-LI10-LIBRE            PIC X(1464).                 01304808
      *                                                                 01304908
AD5   *      03 COMM-LI10-LIBRE          PIC X(1500).                   01305008
      *                                                                 01305108
      ***************************************************************** 01306001
                                                                                
