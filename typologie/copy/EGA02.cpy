      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA02   EGA02                                              00000020
      ***************************************************************** 00000030
       01   EGA02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTMKGL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCTMKGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCTMKGF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCTMKGI   PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTMKGL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLTMKGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLTMKGF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLTMKGI   PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTDESCL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCTDESCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTDESCF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCTDESCI  PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTDESCL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLTDESCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTDESCF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLTDESCI  PIC X(20).                                      00000370
           02 M147I OCCURS   10 TIMES .                                 00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCCDESCL     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MCCDESCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCCDESCF     PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MCCDESCI     PIC X(5).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCDESCL     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MLCDESCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLCDESCF     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MLCDESCI     PIC X(20).                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCCMKGL      COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MCCMKGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCCMKGF      PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MCCMKGI      PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MZONCMDI  PIC X(15).                                      00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MLIBERRI  PIC X(58).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MCODTRAI  PIC X(4).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCICSI    PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MNETNAMI  PIC X(8).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MSCREENI  PIC X(4).                                       00000740
      ***************************************************************** 00000750
      * SDF: EGA02   EGA02                                              00000760
      ***************************************************************** 00000770
       01   EGA02O REDEFINES EGA02I.                                    00000780
           02 FILLER    PIC X(12).                                      00000790
           02 FILLER    PIC X(2).                                       00000800
           02 MDATJOUA  PIC X.                                          00000810
           02 MDATJOUC  PIC X.                                          00000820
           02 MDATJOUP  PIC X.                                          00000830
           02 MDATJOUH  PIC X.                                          00000840
           02 MDATJOUV  PIC X.                                          00000850
           02 MDATJOUO  PIC X(10).                                      00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MTIMJOUA  PIC X.                                          00000880
           02 MTIMJOUC  PIC X.                                          00000890
           02 MTIMJOUP  PIC X.                                          00000900
           02 MTIMJOUH  PIC X.                                          00000910
           02 MTIMJOUV  PIC X.                                          00000920
           02 MTIMJOUO  PIC X(5).                                       00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MPAGEA    PIC X.                                          00000950
           02 MPAGEC    PIC X.                                          00000960
           02 MPAGEP    PIC X.                                          00000970
           02 MPAGEH    PIC X.                                          00000980
           02 MPAGEV    PIC X.                                          00000990
           02 MPAGEO    PIC X(3).                                       00001000
           02 FILLER    PIC X(2).                                       00001010
           02 MWFONCA   PIC X.                                          00001020
           02 MWFONCC   PIC X.                                          00001030
           02 MWFONCP   PIC X.                                          00001040
           02 MWFONCH   PIC X.                                          00001050
           02 MWFONCV   PIC X.                                          00001060
           02 MWFONCO   PIC X(3).                                       00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MCTMKGA   PIC X.                                          00001090
           02 MCTMKGC   PIC X.                                          00001100
           02 MCTMKGP   PIC X.                                          00001110
           02 MCTMKGH   PIC X.                                          00001120
           02 MCTMKGV   PIC X.                                          00001130
           02 MCTMKGO   PIC X(5).                                       00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MLTMKGA   PIC X.                                          00001160
           02 MLTMKGC   PIC X.                                          00001170
           02 MLTMKGP   PIC X.                                          00001180
           02 MLTMKGH   PIC X.                                          00001190
           02 MLTMKGV   PIC X.                                          00001200
           02 MLTMKGO   PIC X(20).                                      00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MCTDESCA  PIC X.                                          00001230
           02 MCTDESCC  PIC X.                                          00001240
           02 MCTDESCP  PIC X.                                          00001250
           02 MCTDESCH  PIC X.                                          00001260
           02 MCTDESCV  PIC X.                                          00001270
           02 MCTDESCO  PIC X(5).                                       00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MLTDESCA  PIC X.                                          00001300
           02 MLTDESCC  PIC X.                                          00001310
           02 MLTDESCP  PIC X.                                          00001320
           02 MLTDESCH  PIC X.                                          00001330
           02 MLTDESCV  PIC X.                                          00001340
           02 MLTDESCO  PIC X(20).                                      00001350
           02 M147O OCCURS   10 TIMES .                                 00001360
             03 FILLER       PIC X(2).                                  00001370
             03 MCCDESCA     PIC X.                                     00001380
             03 MCCDESCC     PIC X.                                     00001390
             03 MCCDESCP     PIC X.                                     00001400
             03 MCCDESCH     PIC X.                                     00001410
             03 MCCDESCV     PIC X.                                     00001420
             03 MCCDESCO     PIC X(5).                                  00001430
             03 FILLER       PIC X(2).                                  00001440
             03 MLCDESCA     PIC X.                                     00001450
             03 MLCDESCC     PIC X.                                     00001460
             03 MLCDESCP     PIC X.                                     00001470
             03 MLCDESCH     PIC X.                                     00001480
             03 MLCDESCV     PIC X.                                     00001490
             03 MLCDESCO     PIC X(20).                                 00001500
             03 FILLER       PIC X(2).                                  00001510
             03 MCCMKGA      PIC X.                                     00001520
             03 MCCMKGC PIC X.                                          00001530
             03 MCCMKGP PIC X.                                          00001540
             03 MCCMKGH PIC X.                                          00001550
             03 MCCMKGV PIC X.                                          00001560
             03 MCCMKGO      PIC X(5).                                  00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MZONCMDA  PIC X.                                          00001590
           02 MZONCMDC  PIC X.                                          00001600
           02 MZONCMDP  PIC X.                                          00001610
           02 MZONCMDH  PIC X.                                          00001620
           02 MZONCMDV  PIC X.                                          00001630
           02 MZONCMDO  PIC X(15).                                      00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MLIBERRA  PIC X.                                          00001660
           02 MLIBERRC  PIC X.                                          00001670
           02 MLIBERRP  PIC X.                                          00001680
           02 MLIBERRH  PIC X.                                          00001690
           02 MLIBERRV  PIC X.                                          00001700
           02 MLIBERRO  PIC X(58).                                      00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MCODTRAA  PIC X.                                          00001730
           02 MCODTRAC  PIC X.                                          00001740
           02 MCODTRAP  PIC X.                                          00001750
           02 MCODTRAH  PIC X.                                          00001760
           02 MCODTRAV  PIC X.                                          00001770
           02 MCODTRAO  PIC X(4).                                       00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MCICSA    PIC X.                                          00001800
           02 MCICSC    PIC X.                                          00001810
           02 MCICSP    PIC X.                                          00001820
           02 MCICSH    PIC X.                                          00001830
           02 MCICSV    PIC X.                                          00001840
           02 MCICSO    PIC X(5).                                       00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MNETNAMA  PIC X.                                          00001870
           02 MNETNAMC  PIC X.                                          00001880
           02 MNETNAMP  PIC X.                                          00001890
           02 MNETNAMH  PIC X.                                          00001900
           02 MNETNAMV  PIC X.                                          00001910
           02 MNETNAMO  PIC X(8).                                       00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MSCREENA  PIC X.                                          00001940
           02 MSCREENC  PIC X.                                          00001950
           02 MSCREENP  PIC X.                                          00001960
           02 MSCREENH  PIC X.                                          00001970
           02 MSCREENV  PIC X.                                          00001980
           02 MSCREENO  PIC X(4).                                       00001990
                                                                                
