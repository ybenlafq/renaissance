           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FGFRQ DEFINITION DES FREQUENCES        *        
      *----------------------------------------------------------------*        
       01  RVGA01L2.                                                            
           05  FGFRQ-CTABLEG2    PIC X(15).                                     
           05  FGFRQ-CTABLEG2-REDEF REDEFINES FGFRQ-CTABLEG2.                   
               10  FGFRQ-NATURE          PIC X(05).                             
           05  FGFRQ-WTABLEG     PIC X(80).                                     
           05  FGFRQ-WTABLEG-REDEF  REDEFINES FGFRQ-WTABLEG.                    
               10  FGFRQ-WACTIF          PIC X(01).                             
               10  FGFRQ-ALIM            PIC X(01).                             
               10  FGFRQ-FREQ            PIC X(02).                             
           EXEC SQL END DECLARE SECTION END-EXEC.
               10  FGFRQ-FREQ-N         REDEFINES FGFRQ-FREQ                    
                                         PIC 9(02).                             
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  FGFRQ-ECHEANCE        PIC X(01).                             
               10  FGFRQ-JOUR            PIC X(02).                             
           EXEC SQL END DECLARE SECTION END-EXEC.
               10  FGFRQ-JOUR-N         REDEFINES FGFRQ-JOUR                    
                                         PIC 9(02).                             
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01L2-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGFRQ-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FGFRQ-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGFRQ-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FGFRQ-WTABLEG-F   PIC S9(4) COMP-5.                              
      *}
           EXEC SQL END DECLARE SECTION END-EXEC.
 
                                                                                
