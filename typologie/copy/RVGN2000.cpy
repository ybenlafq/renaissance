      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00030000
      *   COPY DE LA TABLE RVGN2000                                     00040003
      **********************************************************        00050000
      *   LISTE DES HOST VARIABLES DE LA VUE RVGN2000                   00060003
      **********************************************************        00070000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGN2000.                                                    00080003
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGN2000.                                                            
      *}                                                                        
           05  GN20-NCODIC     PIC X(7).                                00090003
           05  GN20-NCONCN     PIC X(4).                                00091003
           05  GN20-DEFFET     PIC X(8).                                00100003
           05  GN20-DFINEFFET  PIC X(8).                                00101003
           05  GN20-PEXPTTC    PIC S9(5)V9(2) COMP-3.                   00110004
           05  GN20-WACTIF     PIC X(1).                                00120003
           05  GN20-LCOMMENT   PIC X(10).                               00130003
           05  GN20-DSYST PIC S9(13) COMP-3.                            00160003
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************        00170000
      *   LISTE DES FLAGS DE LA TABLE RVGN2000                          00180003
      **********************************************************        00190000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGN2000-FLAGS.                                              00200003
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGN2000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN20-NCODIC-F                                            00210003
      *        PIC S9(4) COMP.                                          00211000
      *--                                                                       
           05  GN20-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN20-NCONCN-F                                            00212003
      *        PIC S9(4) COMP.                                          00213003
      *--                                                                       
           05  GN20-NCONCN-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN20-DEFFET-F                                            00220003
      *        PIC S9(4) COMP.                                          00221000
      *--                                                                       
           05  GN20-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN20-DFINEFFET-F                                         00222003
      *        PIC S9(4) COMP.                                          00223003
      *--                                                                       
           05  GN20-DFINEFFET-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN20-PEXPTTC-F                                           00230003
      *        PIC S9(4) COMP.                                          00231000
      *--                                                                       
           05  GN20-PEXPTTC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN20-WACTIF-F                                            00232003
      *        PIC S9(4) COMP.                                          00233003
      *--                                                                       
           05  GN20-WACTIF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN20-LCOMMENT-F                                          00234003
      *        PIC S9(4) COMP.                                          00235003
      *--                                                                       
           05  GN20-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN20-DSYST-F                                             00280003
      *        PIC S9(4) COMP.                                          00290000
      *                                                                         
      *--                                                                       
           05  GN20-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
