      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA03   EGA03                                              00000020
      ***************************************************************** 00000030
       01   EMP01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMOPAIL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCMOPAIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMOPAIF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCMOPAII  PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMPANCL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCMPANCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMPANCF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCMPANCI  PIC X.                                          00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTPRGTL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MWTPRGTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWTPRGTF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MWTPRGTI  PIC X.                                          00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMDPAIL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLMDPAIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLMDPAIF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLMDPAII  PIC X(15).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMDPRBL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLMDPRBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLMDPRBF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLMDPRBI  PIC X(15).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWINFCPL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MWINFCPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWINFCPF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MWINFCPI  PIC X.                                          00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTCTRLL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MWTCTRLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWTCTRLF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MWTCTRLI  PIC X(2).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWSEQEDL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MWSEQEDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWSEQEDF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MWSEQEDI  PIC X(2).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWDCMPTL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MWDCMPTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWDCMPTF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MWDCMPTI  PIC X.                                          00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMOPACL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCMOPACL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMOPACF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCMOPACI  PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWMCTRLL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MWMCTRLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWMCTRLF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MWMCTRLI  PIC X(10).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWCTFDCL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MWCTFDCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWCTFDCF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MWCTFDCI  PIC X.                                          00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWRCOFFL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MWRCOFFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWRCOFFF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MWRCOFFI  PIC X.                                          00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNBCHQL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MLNBCHQL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLNBCHQF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLNBCHQI  PIC X(12).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVTIPAL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLVTIPAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLVTIPAF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLVTIPAI  PIC X(10).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLRTIPAL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MLRTIPAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLRTIPAF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLRTIPAI  PIC X(10).                                      00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLUTIPAL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MLUTIPAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLUTIPAF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MLUTIPAI  PIC X(10).                                      00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFLAGSL   COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MFLAGSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MFLAGSF   PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MFLAGSI   PIC X(40).                                      00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MZONCMDI  PIC X(15).                                      00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MLIBERRI  PIC X(56).                                      00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MCODTRAI  PIC X(4).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MCICSI    PIC X(5).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MNETNAMI  PIC X(8).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MSCREENI  PIC X(4).                                       00001170
      ***************************************************************** 00001180
      * SDF: EGA03   EGA03                                              00001190
      ***************************************************************** 00001200
       01   EMP01O REDEFINES EMP01I.                                    00001210
           02 FILLER    PIC X(12).                                      00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MDATJOUA  PIC X.                                          00001240
           02 MDATJOUC  PIC X.                                          00001250
           02 MDATJOUP  PIC X.                                          00001260
           02 MDATJOUH  PIC X.                                          00001270
           02 MDATJOUV  PIC X.                                          00001280
           02 MDATJOUO  PIC X(10).                                      00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MTIMJOUA  PIC X.                                          00001310
           02 MTIMJOUC  PIC X.                                          00001320
           02 MTIMJOUP  PIC X.                                          00001330
           02 MTIMJOUH  PIC X.                                          00001340
           02 MTIMJOUV  PIC X.                                          00001350
           02 MTIMJOUO  PIC X(5).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MWFONCA   PIC X.                                          00001380
           02 MWFONCC   PIC X.                                          00001390
           02 MWFONCP   PIC X.                                          00001400
           02 MWFONCH   PIC X.                                          00001410
           02 MWFONCV   PIC X.                                          00001420
           02 MWFONCO   PIC X(3).                                       00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MNSOCA    PIC X.                                          00001450
           02 MNSOCC    PIC X.                                          00001460
           02 MNSOCP    PIC X.                                          00001470
           02 MNSOCH    PIC X.                                          00001480
           02 MNSOCV    PIC X.                                          00001490
           02 MNSOCO    PIC X(3).                                       00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MCMOPAIA  PIC X.                                          00001520
           02 MCMOPAIC  PIC X.                                          00001530
           02 MCMOPAIP  PIC X.                                          00001540
           02 MCMOPAIH  PIC X.                                          00001550
           02 MCMOPAIV  PIC X.                                          00001560
           02 MCMOPAIO  PIC X(5).                                       00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MCMPANCA  PIC X.                                          00001590
           02 MCMPANCC  PIC X.                                          00001600
           02 MCMPANCP  PIC X.                                          00001610
           02 MCMPANCH  PIC X.                                          00001620
           02 MCMPANCV  PIC X.                                          00001630
           02 MCMPANCO  PIC X.                                          00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MWTPRGTA  PIC X.                                          00001660
           02 MWTPRGTC  PIC X.                                          00001670
           02 MWTPRGTP  PIC X.                                          00001680
           02 MWTPRGTH  PIC X.                                          00001690
           02 MWTPRGTV  PIC X.                                          00001700
           02 MWTPRGTO  PIC X.                                          00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MLMDPAIA  PIC X.                                          00001730
           02 MLMDPAIC  PIC X.                                          00001740
           02 MLMDPAIP  PIC X.                                          00001750
           02 MLMDPAIH  PIC X.                                          00001760
           02 MLMDPAIV  PIC X.                                          00001770
           02 MLMDPAIO  PIC X(15).                                      00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MLMDPRBA  PIC X.                                          00001800
           02 MLMDPRBC  PIC X.                                          00001810
           02 MLMDPRBP  PIC X.                                          00001820
           02 MLMDPRBH  PIC X.                                          00001830
           02 MLMDPRBV  PIC X.                                          00001840
           02 MLMDPRBO  PIC X(15).                                      00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MWINFCPA  PIC X.                                          00001870
           02 MWINFCPC  PIC X.                                          00001880
           02 MWINFCPP  PIC X.                                          00001890
           02 MWINFCPH  PIC X.                                          00001900
           02 MWINFCPV  PIC X.                                          00001910
           02 MWINFCPO  PIC X.                                          00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MWTCTRLA  PIC X.                                          00001940
           02 MWTCTRLC  PIC X.                                          00001950
           02 MWTCTRLP  PIC X.                                          00001960
           02 MWTCTRLH  PIC X.                                          00001970
           02 MWTCTRLV  PIC X.                                          00001980
           02 MWTCTRLO  PIC X(2).                                       00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MWSEQEDA  PIC X.                                          00002010
           02 MWSEQEDC  PIC X.                                          00002020
           02 MWSEQEDP  PIC X.                                          00002030
           02 MWSEQEDH  PIC X.                                          00002040
           02 MWSEQEDV  PIC X.                                          00002050
           02 MWSEQEDO  PIC X(2).                                       00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MWDCMPTA  PIC X.                                          00002080
           02 MWDCMPTC  PIC X.                                          00002090
           02 MWDCMPTP  PIC X.                                          00002100
           02 MWDCMPTH  PIC X.                                          00002110
           02 MWDCMPTV  PIC X.                                          00002120
           02 MWDCMPTO  PIC X.                                          00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MCMOPACA  PIC X.                                          00002150
           02 MCMOPACC  PIC X.                                          00002160
           02 MCMOPACP  PIC X.                                          00002170
           02 MCMOPACH  PIC X.                                          00002180
           02 MCMOPACV  PIC X.                                          00002190
           02 MCMOPACO  PIC X(5).                                       00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MWMCTRLA  PIC X.                                          00002220
           02 MWMCTRLC  PIC X.                                          00002230
           02 MWMCTRLP  PIC X.                                          00002240
           02 MWMCTRLH  PIC X.                                          00002250
           02 MWMCTRLV  PIC X.                                          00002260
           02 MWMCTRLO  PIC X(10).                                      00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MWCTFDCA  PIC X.                                          00002290
           02 MWCTFDCC  PIC X.                                          00002300
           02 MWCTFDCP  PIC X.                                          00002310
           02 MWCTFDCH  PIC X.                                          00002320
           02 MWCTFDCV  PIC X.                                          00002330
           02 MWCTFDCO  PIC X.                                          00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MWRCOFFA  PIC X.                                          00002360
           02 MWRCOFFC  PIC X.                                          00002370
           02 MWRCOFFP  PIC X.                                          00002380
           02 MWRCOFFH  PIC X.                                          00002390
           02 MWRCOFFV  PIC X.                                          00002400
           02 MWRCOFFO  PIC X.                                          00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MLNBCHQA  PIC X.                                          00002430
           02 MLNBCHQC  PIC X.                                          00002440
           02 MLNBCHQP  PIC X.                                          00002450
           02 MLNBCHQH  PIC X.                                          00002460
           02 MLNBCHQV  PIC X.                                          00002470
           02 MLNBCHQO  PIC X(12).                                      00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MLVTIPAA  PIC X.                                          00002500
           02 MLVTIPAC  PIC X.                                          00002510
           02 MLVTIPAP  PIC X.                                          00002520
           02 MLVTIPAH  PIC X.                                          00002530
           02 MLVTIPAV  PIC X.                                          00002540
           02 MLVTIPAO  PIC X(10).                                      00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MLRTIPAA  PIC X.                                          00002570
           02 MLRTIPAC  PIC X.                                          00002580
           02 MLRTIPAP  PIC X.                                          00002590
           02 MLRTIPAH  PIC X.                                          00002600
           02 MLRTIPAV  PIC X.                                          00002610
           02 MLRTIPAO  PIC X(10).                                      00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MLUTIPAA  PIC X.                                          00002640
           02 MLUTIPAC  PIC X.                                          00002650
           02 MLUTIPAP  PIC X.                                          00002660
           02 MLUTIPAH  PIC X.                                          00002670
           02 MLUTIPAV  PIC X.                                          00002680
           02 MLUTIPAO  PIC X(10).                                      00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MFLAGSA   PIC X.                                          00002710
           02 MFLAGSC   PIC X.                                          00002720
           02 MFLAGSP   PIC X.                                          00002730
           02 MFLAGSH   PIC X.                                          00002740
           02 MFLAGSV   PIC X.                                          00002750
           02 MFLAGSO   PIC X(40).                                      00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MZONCMDA  PIC X.                                          00002780
           02 MZONCMDC  PIC X.                                          00002790
           02 MZONCMDP  PIC X.                                          00002800
           02 MZONCMDH  PIC X.                                          00002810
           02 MZONCMDV  PIC X.                                          00002820
           02 MZONCMDO  PIC X(15).                                      00002830
           02 FILLER    PIC X(2).                                       00002840
           02 MLIBERRA  PIC X.                                          00002850
           02 MLIBERRC  PIC X.                                          00002860
           02 MLIBERRP  PIC X.                                          00002870
           02 MLIBERRH  PIC X.                                          00002880
           02 MLIBERRV  PIC X.                                          00002890
           02 MLIBERRO  PIC X(56).                                      00002900
           02 FILLER    PIC X(2).                                       00002910
           02 MCODTRAA  PIC X.                                          00002920
           02 MCODTRAC  PIC X.                                          00002930
           02 MCODTRAP  PIC X.                                          00002940
           02 MCODTRAH  PIC X.                                          00002950
           02 MCODTRAV  PIC X.                                          00002960
           02 MCODTRAO  PIC X(4).                                       00002970
           02 FILLER    PIC X(2).                                       00002980
           02 MCICSA    PIC X.                                          00002990
           02 MCICSC    PIC X.                                          00003000
           02 MCICSP    PIC X.                                          00003010
           02 MCICSH    PIC X.                                          00003020
           02 MCICSV    PIC X.                                          00003030
           02 MCICSO    PIC X(5).                                       00003040
           02 FILLER    PIC X(2).                                       00003050
           02 MNETNAMA  PIC X.                                          00003060
           02 MNETNAMC  PIC X.                                          00003070
           02 MNETNAMP  PIC X.                                          00003080
           02 MNETNAMH  PIC X.                                          00003090
           02 MNETNAMV  PIC X.                                          00003100
           02 MNETNAMO  PIC X(8).                                       00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MSCREENA  PIC X.                                          00003130
           02 MSCREENC  PIC X.                                          00003140
           02 MSCREENP  PIC X.                                          00003150
           02 MSCREENH  PIC X.                                          00003160
           02 MSCREENV  PIC X.                                          00003170
           02 MSCREENO  PIC X(4).                                       00003180
                                                                                
