      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MQGET PARAM GET MQSERIES HOST          *        
      *----------------------------------------------------------------*        
       01  RVGA01V3.                                                            
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  MQGET-CTABLEG2    PIC X(15).                                     
           05  MQGET-CTABLEG2-REDEF REDEFINES MQGET-CTABLEG2.                   
               10  MQGET-NOMPROG         PIC X(06).                             
               10  MQGET-CFONC           PIC X(03).                             
           05  MQGET-WTABLEG     PIC X(80).                                     
           05  MQGET-WTABLEG-REDEF  REDEFINES MQGET-WTABLEG.                    
               10  MQGET-QALIAS          PIC X(18).                             
               10  MQGET-MSGID           PIC X(24).                             
               10  MQGET-DUREE           PIC X(05).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  MQGET-DUREE-N        REDEFINES MQGET-DUREE                   
                                         PIC S9(05).                            
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  MQGET-LONGUEUR        PIC X(05).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  MQGET-LONGUEUR-N     REDEFINES MQGET-LONGUEUR                
                                         PIC 9(05).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01V3-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MQGET-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MQGET-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MQGET-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MQGET-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
