      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGA0900                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA0900                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA0900.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA0900.                                                            
      *}                                                                        
           02  GA09-CETAT                                                       
               PIC X(0010).                                                     
           02  GA09-CFAM                                                        
               PIC X(0005).                                                     
           02  GA09-WSEQED                                                      
               PIC X(0002).                                                     
           02  GA09-CMARKETIN1                                                  
               PIC X(0005).                                                     
           02  GA09-CVMARKETIN1                                                 
               PIC X(0005).                                                     
           02  GA09-CMARKETIN2                                                  
               PIC X(0005).                                                     
           02  GA09-CVMARKETIN2                                                 
               PIC X(0005).                                                     
           02  GA09-CMARKETIN3                                                  
               PIC X(0005).                                                     
           02  GA09-CVMARKETIN3                                                 
               PIC X(0005).                                                     
           02  GA09-NAGREGATED                                                  
               PIC X(0002).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGA0900                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA0900-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA0900-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA09-CETAT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA09-CETAT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA09-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA09-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA09-WSEQED-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA09-WSEQED-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA09-CMARKETIN1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA09-CMARKETIN1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA09-CVMARKETIN1-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA09-CVMARKETIN1-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA09-CMARKETIN2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA09-CMARKETIN2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA09-CVMARKETIN2-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA09-CVMARKETIN2-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA09-CMARKETIN3-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA09-CMARKETIN3-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA09-CVMARKETIN3-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA09-CVMARKETIN3-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA09-NAGREGATED-F                                                
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GA09-NAGREGATED-F                                                
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
