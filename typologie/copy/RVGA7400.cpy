      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGA7400                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA7400                         
      **********************************************************                
       01  RVGA7400.                                                            
           02  GA74-NCODIC                                                      
               PIC X(0007).                                                     
           02  GA74-NTITRE                                                      
               PIC X(0005).                                                     
           02  GA74-LTEXTE                                                      
               PIC X(0040).                                                     
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGA7400                                  
      **********************************************************                
       01  RVGA7400-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA74-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA74-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA74-NTITRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA74-NTITRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA74-LTEXTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GA74-LTEXTE-F                                                    
               PIC S9(4) COMP-5.                                                
EMOD                                                                            
      *}                                                                        
