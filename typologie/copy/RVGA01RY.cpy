      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SRPK  EXCEPTION FACTURATION SRP        *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01RY.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01RY.                                                            
      *}                                                                        
           05  SRPK-CTABLEG2     PIC X(15).                                     
           05  SRPK-CTABLEG2-REDEF  REDEFINES SRPK-CTABLEG2.                    
               10  SRPK-SOC              PIC X(03).                             
               10  SRPK-MARQUE           PIC X(05).                             
               10  SRPK-FAMILLE          PIC X(05).                             
           05  SRPK-WTABLEG      PIC X(80).                                     
           05  SRPK-WTABLEG-REDEF   REDEFINES SRPK-WTABLEG.                     
               10  SRPK-COEFF            PIC S9(01)V9(02)     COMP-3.           
               10  SRPK-FLAG             PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01RY-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01RY-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SRPK-CTABLEG2-F   PIC S9(4)  COMP.                               
      *--                                                                       
           05  SRPK-CTABLEG2-F   PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SRPK-WTABLEG-F    PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SRPK-WTABLEG-F    PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
