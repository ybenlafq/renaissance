      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESE03   ESE03                                              00000020
      ***************************************************************** 00000030
       01   EBS11I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHEFPRODL     COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MCHEFPRODL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCHEFPRODF     PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCHEFPRODI     PIC X(5).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCHEFPRODL    COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MLCHEFPRODL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLCHEFPRODF    PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLCHEFPRODI    PIC X(20).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLISTRL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNLISTRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLISTRF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNLISTRI  PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPENTRL     COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MCTYPENTRL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTYPENTRF     PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCTYPENTRI     PIC X(2).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPENTRL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLTYPENTRL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLTYPENTRF     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLTYPENTRI     PIC X(20).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTITERL     COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MNENTITERL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNENTITERF     PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNENTITERI     PIC X(7).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMRL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCFAMRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAMRF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCFAMRI   PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQRL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCMARQRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMARQRF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCMARQRI  PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBENTRL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MLIBENTRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBENTRF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLIBENTRI      PIC X(20).                                 00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MPAGEI    PIC X(3).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MPAGEMAXI      PIC X(3).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCRIT11L      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MCCRIT11L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCCRIT11F      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCCRIT11I      PIC X(5).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCRIT21L      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MCCRIT21L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCCRIT21F      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCCRIT21I      PIC X(5).                                  00000690
           02 MTABLISTI OCCURS   10 TIMES .                             00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLISTL      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MNLISTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLISTF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MNLISTI      PIC X(7).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLLISTL      COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MLLISTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLLISTF      PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MLLISTI      PIC X(20).                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPENTL    COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MCTYPENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCTYPENTF    PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MCTYPENTI    PIC X(2).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCCRIT1L     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MCCRIT1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCCRIT1F     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MCCRIT1I     PIC X(5).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCCRIT2L     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MCCRIT2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCCRIT2F     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MCCRIT2I     PIC X(5).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCREATIONL  COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MDCREATIONL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MDCREATIONF  PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MDCREATIONI  PIC X(8).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MGENERIQUEL  COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MGENERIQUEL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MGENERIQUEF  PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MGENERIQUEI  PIC X.                                     00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MORIGINEL    COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MORIGINEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MORIGINEF    PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MORIGINEI    PIC X(5).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWACTL  COMP PIC S9(4).                                 00001030
      *--                                                                       
             03 MWACTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MWACTF  PIC X.                                          00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MWACTI  PIC X.                                          00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MZONCMDI  PIC X(15).                                      00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MLIBERRI  PIC X(58).                                      00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCODTRAI  PIC X(4).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MCICSI    PIC X(5).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MNETNAMI  PIC X(8).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MSCREENI  PIC X(4).                                       00001300
      ***************************************************************** 00001310
      * SDF: ESE03   ESE03                                              00001320
      ***************************************************************** 00001330
       01   EBS11O REDEFINES EBS11I.                                    00001340
           02 FILLER    PIC X(12).                                      00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MDATJOUA  PIC X.                                          00001370
           02 MDATJOUC  PIC X.                                          00001380
           02 MDATJOUP  PIC X.                                          00001390
           02 MDATJOUH  PIC X.                                          00001400
           02 MDATJOUV  PIC X.                                          00001410
           02 MDATJOUO  PIC X(10).                                      00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MTIMJOUA  PIC X.                                          00001440
           02 MTIMJOUC  PIC X.                                          00001450
           02 MTIMJOUP  PIC X.                                          00001460
           02 MTIMJOUH  PIC X.                                          00001470
           02 MTIMJOUV  PIC X.                                          00001480
           02 MTIMJOUO  PIC X(5).                                       00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MCHEFPRODA     PIC X.                                     00001510
           02 MCHEFPRODC     PIC X.                                     00001520
           02 MCHEFPRODP     PIC X.                                     00001530
           02 MCHEFPRODH     PIC X.                                     00001540
           02 MCHEFPRODV     PIC X.                                     00001550
           02 MCHEFPRODO     PIC X(5).                                  00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MWFONCA   PIC X.                                          00001580
           02 MWFONCC   PIC X.                                          00001590
           02 MWFONCP   PIC X.                                          00001600
           02 MWFONCH   PIC X.                                          00001610
           02 MWFONCV   PIC X.                                          00001620
           02 MWFONCO   PIC X(3).                                       00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MLCHEFPRODA    PIC X.                                     00001650
           02 MLCHEFPRODC    PIC X.                                     00001660
           02 MLCHEFPRODP    PIC X.                                     00001670
           02 MLCHEFPRODH    PIC X.                                     00001680
           02 MLCHEFPRODV    PIC X.                                     00001690
           02 MLCHEFPRODO    PIC X(20).                                 00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MNLISTRA  PIC X.                                          00001720
           02 MNLISTRC  PIC X.                                          00001730
           02 MNLISTRP  PIC X.                                          00001740
           02 MNLISTRH  PIC X.                                          00001750
           02 MNLISTRV  PIC X.                                          00001760
           02 MNLISTRO  PIC X(7).                                       00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MCTYPENTRA     PIC X.                                     00001790
           02 MCTYPENTRC     PIC X.                                     00001800
           02 MCTYPENTRP     PIC X.                                     00001810
           02 MCTYPENTRH     PIC X.                                     00001820
           02 MCTYPENTRV     PIC X.                                     00001830
           02 MCTYPENTRO     PIC X(2).                                  00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MLTYPENTRA     PIC X.                                     00001860
           02 MLTYPENTRC     PIC X.                                     00001870
           02 MLTYPENTRP     PIC X.                                     00001880
           02 MLTYPENTRH     PIC X.                                     00001890
           02 MLTYPENTRV     PIC X.                                     00001900
           02 MLTYPENTRO     PIC X(20).                                 00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MNENTITERA     PIC X.                                     00001930
           02 MNENTITERC     PIC X.                                     00001940
           02 MNENTITERP     PIC X.                                     00001950
           02 MNENTITERH     PIC X.                                     00001960
           02 MNENTITERV     PIC X.                                     00001970
           02 MNENTITERO     PIC X(7).                                  00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MCFAMRA   PIC X.                                          00002000
           02 MCFAMRC   PIC X.                                          00002010
           02 MCFAMRP   PIC X.                                          00002020
           02 MCFAMRH   PIC X.                                          00002030
           02 MCFAMRV   PIC X.                                          00002040
           02 MCFAMRO   PIC X(5).                                       00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MCMARQRA  PIC X.                                          00002070
           02 MCMARQRC  PIC X.                                          00002080
           02 MCMARQRP  PIC X.                                          00002090
           02 MCMARQRH  PIC X.                                          00002100
           02 MCMARQRV  PIC X.                                          00002110
           02 MCMARQRO  PIC X(5).                                       00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MLIBENTRA      PIC X.                                     00002140
           02 MLIBENTRC PIC X.                                          00002150
           02 MLIBENTRP PIC X.                                          00002160
           02 MLIBENTRH PIC X.                                          00002170
           02 MLIBENTRV PIC X.                                          00002180
           02 MLIBENTRO      PIC X(20).                                 00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MPAGEA    PIC X.                                          00002210
           02 MPAGEC    PIC X.                                          00002220
           02 MPAGEP    PIC X.                                          00002230
           02 MPAGEH    PIC X.                                          00002240
           02 MPAGEV    PIC X.                                          00002250
           02 MPAGEO    PIC X(3).                                       00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MPAGEMAXA      PIC X.                                     00002280
           02 MPAGEMAXC PIC X.                                          00002290
           02 MPAGEMAXP PIC X.                                          00002300
           02 MPAGEMAXH PIC X.                                          00002310
           02 MPAGEMAXV PIC X.                                          00002320
           02 MPAGEMAXO      PIC X(3).                                  00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCCRIT11A      PIC X.                                     00002350
           02 MCCRIT11C PIC X.                                          00002360
           02 MCCRIT11P PIC X.                                          00002370
           02 MCCRIT11H PIC X.                                          00002380
           02 MCCRIT11V PIC X.                                          00002390
           02 MCCRIT11O      PIC X(5).                                  00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MCCRIT21A      PIC X.                                     00002420
           02 MCCRIT21C PIC X.                                          00002430
           02 MCCRIT21P PIC X.                                          00002440
           02 MCCRIT21H PIC X.                                          00002450
           02 MCCRIT21V PIC X.                                          00002460
           02 MCCRIT21O      PIC X(5).                                  00002470
           02 MTABLISTO OCCURS   10 TIMES .                             00002480
             03 FILLER       PIC X(2).                                  00002490
             03 MNLISTA      PIC X.                                     00002500
             03 MNLISTC PIC X.                                          00002510
             03 MNLISTP PIC X.                                          00002520
             03 MNLISTH PIC X.                                          00002530
             03 MNLISTV PIC X.                                          00002540
             03 MNLISTO      PIC X(7).                                  00002550
             03 FILLER       PIC X(2).                                  00002560
             03 MLLISTA      PIC X.                                     00002570
             03 MLLISTC PIC X.                                          00002580
             03 MLLISTP PIC X.                                          00002590
             03 MLLISTH PIC X.                                          00002600
             03 MLLISTV PIC X.                                          00002610
             03 MLLISTO      PIC X(20).                                 00002620
             03 FILLER       PIC X(2).                                  00002630
             03 MCTYPENTA    PIC X.                                     00002640
             03 MCTYPENTC    PIC X.                                     00002650
             03 MCTYPENTP    PIC X.                                     00002660
             03 MCTYPENTH    PIC X.                                     00002670
             03 MCTYPENTV    PIC X.                                     00002680
             03 MCTYPENTO    PIC X(2).                                  00002690
             03 FILLER       PIC X(2).                                  00002700
             03 MCCRIT1A     PIC X.                                     00002710
             03 MCCRIT1C     PIC X.                                     00002720
             03 MCCRIT1P     PIC X.                                     00002730
             03 MCCRIT1H     PIC X.                                     00002740
             03 MCCRIT1V     PIC X.                                     00002750
             03 MCCRIT1O     PIC X(5).                                  00002760
             03 FILLER       PIC X(2).                                  00002770
             03 MCCRIT2A     PIC X.                                     00002780
             03 MCCRIT2C     PIC X.                                     00002790
             03 MCCRIT2P     PIC X.                                     00002800
             03 MCCRIT2H     PIC X.                                     00002810
             03 MCCRIT2V     PIC X.                                     00002820
             03 MCCRIT2O     PIC X(5).                                  00002830
             03 FILLER       PIC X(2).                                  00002840
             03 MDCREATIONA  PIC X.                                     00002850
             03 MDCREATIONC  PIC X.                                     00002860
             03 MDCREATIONP  PIC X.                                     00002870
             03 MDCREATIONH  PIC X.                                     00002880
             03 MDCREATIONV  PIC X.                                     00002890
             03 MDCREATIONO  PIC X(8).                                  00002900
             03 FILLER       PIC X(2).                                  00002910
             03 MGENERIQUEA  PIC X.                                     00002920
             03 MGENERIQUEC  PIC X.                                     00002930
             03 MGENERIQUEP  PIC X.                                     00002940
             03 MGENERIQUEH  PIC X.                                     00002950
             03 MGENERIQUEV  PIC X.                                     00002960
             03 MGENERIQUEO  PIC X.                                     00002970
             03 FILLER       PIC X(2).                                  00002980
             03 MORIGINEA    PIC X.                                     00002990
             03 MORIGINEC    PIC X.                                     00003000
             03 MORIGINEP    PIC X.                                     00003010
             03 MORIGINEH    PIC X.                                     00003020
             03 MORIGINEV    PIC X.                                     00003030
             03 MORIGINEO    PIC X(5).                                  00003040
             03 FILLER       PIC X(2).                                  00003050
             03 MWACTA  PIC X.                                          00003060
             03 MWACTC  PIC X.                                          00003070
             03 MWACTP  PIC X.                                          00003080
             03 MWACTH  PIC X.                                          00003090
             03 MWACTV  PIC X.                                          00003100
             03 MWACTO  PIC X.                                          00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MZONCMDA  PIC X.                                          00003130
           02 MZONCMDC  PIC X.                                          00003140
           02 MZONCMDP  PIC X.                                          00003150
           02 MZONCMDH  PIC X.                                          00003160
           02 MZONCMDV  PIC X.                                          00003170
           02 MZONCMDO  PIC X(15).                                      00003180
           02 FILLER    PIC X(2).                                       00003190
           02 MLIBERRA  PIC X.                                          00003200
           02 MLIBERRC  PIC X.                                          00003210
           02 MLIBERRP  PIC X.                                          00003220
           02 MLIBERRH  PIC X.                                          00003230
           02 MLIBERRV  PIC X.                                          00003240
           02 MLIBERRO  PIC X(58).                                      00003250
           02 FILLER    PIC X(2).                                       00003260
           02 MCODTRAA  PIC X.                                          00003270
           02 MCODTRAC  PIC X.                                          00003280
           02 MCODTRAP  PIC X.                                          00003290
           02 MCODTRAH  PIC X.                                          00003300
           02 MCODTRAV  PIC X.                                          00003310
           02 MCODTRAO  PIC X(4).                                       00003320
           02 FILLER    PIC X(2).                                       00003330
           02 MCICSA    PIC X.                                          00003340
           02 MCICSC    PIC X.                                          00003350
           02 MCICSP    PIC X.                                          00003360
           02 MCICSH    PIC X.                                          00003370
           02 MCICSV    PIC X.                                          00003380
           02 MCICSO    PIC X(5).                                       00003390
           02 FILLER    PIC X(2).                                       00003400
           02 MNETNAMA  PIC X.                                          00003410
           02 MNETNAMC  PIC X.                                          00003420
           02 MNETNAMP  PIC X.                                          00003430
           02 MNETNAMH  PIC X.                                          00003440
           02 MNETNAMV  PIC X.                                          00003450
           02 MNETNAMO  PIC X(8).                                       00003460
           02 FILLER    PIC X(2).                                       00003470
           02 MSCREENA  PIC X.                                          00003480
           02 MSCREENC  PIC X.                                          00003490
           02 MSCREENP  PIC X.                                          00003500
           02 MSCREENH  PIC X.                                          00003510
           02 MSCREENV  PIC X.                                          00003520
           02 MSCREENO  PIC X(4).                                       00003530
                                                                                
