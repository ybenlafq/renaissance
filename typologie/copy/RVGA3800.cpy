      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE CMTD00.RTGA38                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVGA3800.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVGA3800.                                                             
      *}                                                                        
           10 GA38-NCODIC          PIC X(07).                                   
           10 GA38-NENTCDE         PIC X(05).                                   
           10 GA38-WACHVEN         PIC X(01).                                   
           10 GA38-DEFFET          PIC X(08).                                   
           10 GA38-CPAYS           PIC X(02).                                   
           10 GA38-WENTCDE         PIC X(01).                                   
           10 GA38-WIMPORTE        PIC X(01).                                   
           10 GA38-PMONTANT        PIC S9(5)V9(2) COMP-3.                       
           10 GA38-NSEQ            PIC S9(9)      COMP-3.                       
           10 GA38-DSYST           PIC S9(13)     COMP-3.                       
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVGA3800-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVGA3800-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA38-NCODIC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA38-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA38-NENTCDE-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GA38-NENTCDE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA38-WACHVEN-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GA38-WACHVEN-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA38-DEFFET-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA38-DEFFET-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA38-CPAYS-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 GA38-CPAYS-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA38-WENTCDE-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GA38-WENTCDE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA38-WIMPORTE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA38-WIMPORTE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA38-NSEQ-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 GA38-NSEQ-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA38-PMONTANT-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA38-PMONTANT-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA38-DSYST-F         PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 GA38-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
