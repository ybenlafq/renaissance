      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************            
      * TS GA98 LONG ENR 65                                                     
       01 TS-EGA98-APPLI.                                                       
           05 TS-GA98-LONG PIC S9(5) COMP-3 VALUE +65.                          
           05 TS-EGA98-DATA.                                                    
               10 TS-GA98-TOPMAJ PIC X.                                         
               10 TS-GA98-ENR.                                                  
                   15 TS-GA98-NCODIC PIC X(7).                                  
                   15 TS-GA98-NSEQUENCE PIC S9(2) COMP-3.                       
                   15 TS-GA98-EAN.                                              
                       20 TS-GA98-NEANCOMPO PIC X(13).                          
                       20 TS-GA98-WACTIF PIC X.                                 
                       20 TS-GA98-LNEANCOMPO PIC X(15).                         
                       20 TS-GA98-WAUTHENT PIC X.                               
                   15 TS-GA98-LIDENTIFIANT PIC X(10).                           
                   15 TS-GA98-DACTIF PIC X(8).                                  
                   15 TS-GA98-DSYST PIC S9(13) COMP-3.                          
                                                                                
