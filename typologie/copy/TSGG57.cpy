      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************          
      *       PRIX EXCEPTIONNELS                                     *          
      *       CONSULTATION AU CODIC                                  *          
      ****************************************************************          
       01  TS-LONG              PIC S9(2) COMP-3 VALUE 67.                      
       01  TS-DONNEES.                                                          
              10 TS-MOUCHARD    PIC X(1).                                       
              10 TS-DEFFET      PIC X(08).                                      
              10 TS-DFINEFFET   PIC X(08).                                      
              10 TS-CONC        PIC X(04).                                      
              10 TS-LCONC       PIC X(20).                                      
              10 TS-PEXPTTC     PIC X(9).                                       
              10 TS-PSTDTTC     PIC X(9).                                       
              10 TS-DATE        PIC X(08).                                      
                                                                                
