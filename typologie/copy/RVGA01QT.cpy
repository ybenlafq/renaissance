      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GCATD GCA - TYPE DE DOCUMENTS          *        
      *----------------------------------------------------------------*        
       01  RVGA01QT.                                                            
           05  GCATD-CTABLEG2    PIC X(15).                                     
           05  GCATD-CTABLEG2-REDEF REDEFINES GCATD-CTABLEG2.                   
               10  GCATD-CTYPE           PIC X(01).                             
               10  GCATD-TYPDOC          PIC X(02).                             
           05  GCATD-WTABLEG     PIC X(80).                                     
           05  GCATD-WTABLEG-REDEF  REDEFINES GCATD-WTABLEG.                    
               10  GCATD-LIBELLE         PIC X(30).                             
               10  GCATD-WPARAM          PIC X(06).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01QT-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GCATD-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GCATD-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GCATD-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GCATD-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
