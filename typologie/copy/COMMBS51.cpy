      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                                 00000010
       01  Z-COMMAREA-MBS51.                                            00000020
           02 COMM-MBS51-ENTETE-RETOUR.                                 00000030
      *---- BOOLEEN TYPE D'ERREUR                                       00000040
              03 COMM-MBS51-TYPERR     PIC 9(01).                       00000050
                 88 MBS51-NO-ERROR       VALUE 0.                       00000060
                 88 MBS51-APPL-ERROR     VALUE 1.                       00000070
                 88 MBS51-DB2-ERROR      VALUE 2.                       00000080
              03 COMM-MBS51-FUNC-SQL   PIC X(08).                       00000090
              03 COMM-MBS51-TABLE-NAME PIC X(08).                       00000100
              03 COMM-MBS51-SQLCODE    PIC S9(04).                      00000110
              03 COMM-MBS51-POSITION   PIC  9(03).                      00000120
      *---- BOOLEEN CODE FONCTION                                       00000130
           02 COMM-MBS51-FONC      PIC 9(01).                           00000140
              88 MBS51-CTRL-DATA       VALUE 0.                         00000150
              88 MBS51-CONT50          VALUE 1.                         00000160
              88 MBS51-CONT51          VALUE 2.                         00000170
              88 MBS51-MAJ-DB2         VALUE 3.                         00000180
      *---- DONNEES EN ENTREE / SORTIE                                  00000190
           02 COMM-MBS51-ZINOUT.                                        00000200
              03 COMM-MBS51-CODESFONCTION   PIC X(12).                  00000210
              03 COMM-MBS51-WFONC           PIC X(03).                  00000220
              03 COMM-MBS51-NCOFFRE         PIC X(07).                  00000230
              03 COMM-MBS51-LCOFFRE         PIC X(20).                  00000240
              03 COMM-MBS51-CMARQ           PIC X(05).                  00000250
              03 COMM-MBS51-CMARQ-CTRL      PIC X(05).                  00000260
              03 COMM-MBS51-LMARQ           PIC X(20).                  00000270
              03 COMM-MBS51-CFAM            PIC X(05).                  00000280
              03 COMM-MBS51-CFAM-CTRL       PIC X(05).                  00000290
              03 COMM-MBS51-LCFAM           PIC X(20).                  00000300
              03 COMM-MBS51-CPROFASS        PIC X(05).                  00000310
              03 COMM-MBS51-LPROFASS        PIC X(20).                  00000320
              03 COMM-MBS51-CASSORTC        PIC X(05).                  00000330
              03 COMM-MBS51-LCASSORTC       PIC X(20).                  00000340
              03 COMM-MBS51-CASSORTF        PIC X(05).                  00000350
              03 COMM-MBS51-LCASSORTF       PIC X(20).                  00000360
              03 COMM-MBS51-DATEF           PIC X(20).                  00000370
              03 COMM-MBS51-CTVA            PIC X(01).                  00000380
              03 COMM-MBS51-NTVA            PIC S999V99.                00000390
              03 COMM-MBS51-CTVALIB         PIC X(20).                  00000400
              03 COMM-MBS51-DMAJ            PIC X(08).                  00000410
              03 COMM-MBS51-CHEF            PIC X(05).                  00000420
              03 COMM-MBS51-LCHEF           PIC X(20).                  00000430
              03 COMM-MBS51-STATCOMP        PIC X(05).                  00000440
              03 COMM-MBS51-LSTATCOMP       PIC X(20).                  00000450
              03 COMM-MBS51-CODPROF         PIC X(10).                  00000460
              03 COMM-MBS51-FAMCLT          PIC X(30).                  00000470
      *---- BOOLEENS POSITIONNEMENT ERREURS                             00000480
           02 COMM-MBS51-ERROR1    PIC 9(01).                           00000490
              88 CMARQ-OK            VALUE 0.                           00000500
              88 CMARQ-KO            VALUE 1.                           00000510
           02 COMM-MBS51-CDRET1    PIC X(04).                           00000520
           02 COMM-MBS51-ERROR2    PIC 9(01).                           00000530
              88 CFAM-OK             VALUE 0.                           00000540
              88 CFAM-KO             VALUE 1.                           00000550
           02 COMM-MBS51-CDRET2    PIC X(04).                           00000560
           02 COMM-MBS51-ERROR3    PIC 9(01).                           00000570
              88 TVA-OK              VALUE 0.                           00000580
              88 TVA-KO              VALUE 1.                           00000590
           02 COMM-MBS51-CDRET3    PIC X(04).                           00000600
           02 COMM-MBS51-ERROR4    PIC 9(01).                           00000610
              88 CASSORT-OK          VALUE 0.                           00000620
              88 CASSORT-KO          VALUE 1.                           00000630
           02 COMM-MBS51-CDRET4    PIC X(04).                           00000640
           02 COMM-MBS51-ERROR5    PIC 9(01).                           00000650
              88 CHEF-OK             VALUE 0.                           00000660
              88 CHEF-KO             VALUE 1.                           00000670
           02 COMM-MBS51-CDRET5    PIC X(04).                           00000680
           02 COMM-MBS51-ERROR6    PIC 9(01).                           00000690
              88 STATCOMP-KO         VALUE 0.                           00000700
              88 STATCOMP-OK         VALUE 1.                           00000710
           02 COMM-MBS51-CDRET6    PIC X(04).                           00000720
           02 COMM-MBS51-ERROR7    PIC 9(01).                           00000730
              88 CPROFASS-KO         VALUE 0.                           00000740
              88 CPROFASS-OK         VALUE 1.                           00000750
           02 COMM-MBS51-CDRET7    PIC X(04).                           00000760
           02 COMM-MBS51-ERROR8    PIC 9(01).                           00000770
              88 LCOFFRE-KO          VALUE 0.                           00000780
              88 LCOFFRE-OK          VALUE 1.                           00000790
           02 COMM-MBS51-CDRET8    PIC X(04).                           00000800
           02 COMM-MBS51-ERROR9    PIC 9(01).                           00000810
              88 CODPROF-OK          VALUE 0.                           00000820
              88 CODPROF-KO          VALUE 1.                           00000830
           02 COMM-MBS51-CDRET9    PIC X(04).                           00000840
           02 COMM-MBS51-FILLER    PIC X(2959).                         00000850
                                                                                
