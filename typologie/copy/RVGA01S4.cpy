      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CREXC PRODUITS CREDIT PAR MAGASIN      *        
      *----------------------------------------------------------------*        
       01  RVGA01S4.                                                            
           05  CREXC-CTABLEG2    PIC X(15).                                     
           05  CREXC-CTABLEG2-REDEF REDEFINES CREXC-CTABLEG2.                   
               10  CREXC-NCLE            PIC X(12).                             
               10  CREXC-NCLE-N         REDEFINES CREXC-NCLE                    
                                         PIC 9(12).                             
           05  CREXC-WTABLEG     PIC X(80).                                     
           05  CREXC-WTABLEG-REDEF  REDEFINES CREXC-WTABLEG.                    
               10  CREXC-DEFFET          PIC X(08).                             
               10  CREXC-DEFFET-N       REDEFINES CREXC-DEFFET                  
                                         PIC 9(08).                             
               10  CREXC-WACTASS         PIC X(02).                             
               10  CREXC-PTEG            PIC S9(03)V9(02)     COMP-3.           
               10  CREXC-PMENSUAL        PIC S9(03)V9(02)     COMP-3.           
               10  CREXC-PCOMPTAN        PIC S9(03)V9(02)     COMP-3.           
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01S4-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CREXC-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CREXC-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CREXC-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CREXC-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
