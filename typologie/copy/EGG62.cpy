      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGG62   EGG62                                              00000020
      ***************************************************************** 00000030
       01   EGG62I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNCODICI  PIC X(7).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRECL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNRECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNRECF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNRECI    PIC X(7).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCDEF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCDEI    PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLREFFOURNI    PIC X(20).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCMARQI   PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRMPL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MPRMPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPRMPF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MPRMPI    PIC X(13).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRMPANCL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MPRMPANCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPRMPANCF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MPRMPANCI      PIC X(13).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MZONCMDI  PIC X(15).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLIBERRI  PIC X(58).                                      00000530
      * CODE TRANSACTION                                                00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MCODTRAI  PIC X(4).                                       00000580
      * CICS DE TRAVAIL                                                 00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MCICSI    PIC X(5).                                       00000630
      * NETNAME                                                         00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MNETNAMI  PIC X(8).                                       00000680
      * CODE TERMINAL                                                   00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MSCREENI  PIC X(5).                                       00000730
      ***************************************************************** 00000740
      * SDF: EGG62   EGG62                                              00000750
      ***************************************************************** 00000760
       01   EGG62O REDEFINES EGG62I.                                    00000770
           02 FILLER    PIC X(12).                                      00000780
           02 FILLER    PIC X(2).                                       00000790
           02 MDATJOUA  PIC X.                                          00000800
           02 MDATJOUC  PIC X.                                          00000810
           02 MDATJOUP  PIC X.                                          00000820
           02 MDATJOUH  PIC X.                                          00000830
           02 MDATJOUV  PIC X.                                          00000840
           02 MDATJOUO  PIC X(10).                                      00000850
           02 FILLER    PIC X(2).                                       00000860
           02 MTIMJOUA  PIC X.                                          00000870
           02 MTIMJOUC  PIC X.                                          00000880
           02 MTIMJOUP  PIC X.                                          00000890
           02 MTIMJOUH  PIC X.                                          00000900
           02 MTIMJOUV  PIC X.                                          00000910
           02 MTIMJOUO  PIC X(5).                                       00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MNCODICA  PIC X.                                          00000940
           02 MNCODICC  PIC X.                                          00000950
           02 MNCODICP  PIC X.                                          00000960
           02 MNCODICH  PIC X.                                          00000970
           02 MNCODICV  PIC X.                                          00000980
           02 MNCODICO  PIC X(7).                                       00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MNRECA    PIC X.                                          00001010
           02 MNRECC    PIC X.                                          00001020
           02 MNRECP    PIC X.                                          00001030
           02 MNRECH    PIC X.                                          00001040
           02 MNRECV    PIC X.                                          00001050
           02 MNRECO    PIC X(7).                                       00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MNCDEA    PIC X.                                          00001080
           02 MNCDEC    PIC X.                                          00001090
           02 MNCDEP    PIC X.                                          00001100
           02 MNCDEH    PIC X.                                          00001110
           02 MNCDEV    PIC X.                                          00001120
           02 MNCDEO    PIC X(7).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MLREFFOURNA    PIC X.                                     00001150
           02 MLREFFOURNC    PIC X.                                     00001160
           02 MLREFFOURNP    PIC X.                                     00001170
           02 MLREFFOURNH    PIC X.                                     00001180
           02 MLREFFOURNV    PIC X.                                     00001190
           02 MLREFFOURNO    PIC X(20).                                 00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MCFAMA    PIC X.                                          00001220
           02 MCFAMC    PIC X.                                          00001230
           02 MCFAMP    PIC X.                                          00001240
           02 MCFAMH    PIC X.                                          00001250
           02 MCFAMV    PIC X.                                          00001260
           02 MCFAMO    PIC X(5).                                       00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MCMARQA   PIC X.                                          00001290
           02 MCMARQC   PIC X.                                          00001300
           02 MCMARQP   PIC X.                                          00001310
           02 MCMARQH   PIC X.                                          00001320
           02 MCMARQV   PIC X.                                          00001330
           02 MCMARQO   PIC X(5).                                       00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MPRMPA    PIC X.                                          00001360
           02 MPRMPC    PIC X.                                          00001370
           02 MPRMPP    PIC X.                                          00001380
           02 MPRMPH    PIC X.                                          00001390
           02 MPRMPV    PIC X.                                          00001400
           02 MPRMPO    PIC X(13).                                      00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MPRMPANCA      PIC X.                                     00001430
           02 MPRMPANCC PIC X.                                          00001440
           02 MPRMPANCP PIC X.                                          00001450
           02 MPRMPANCH PIC X.                                          00001460
           02 MPRMPANCV PIC X.                                          00001470
           02 MPRMPANCO      PIC X(13).                                 00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MZONCMDA  PIC X.                                          00001500
           02 MZONCMDC  PIC X.                                          00001510
           02 MZONCMDP  PIC X.                                          00001520
           02 MZONCMDH  PIC X.                                          00001530
           02 MZONCMDV  PIC X.                                          00001540
           02 MZONCMDO  PIC X(15).                                      00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MLIBERRA  PIC X.                                          00001570
           02 MLIBERRC  PIC X.                                          00001580
           02 MLIBERRP  PIC X.                                          00001590
           02 MLIBERRH  PIC X.                                          00001600
           02 MLIBERRV  PIC X.                                          00001610
           02 MLIBERRO  PIC X(58).                                      00001620
      * CODE TRANSACTION                                                00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MCODTRAA  PIC X.                                          00001650
           02 MCODTRAC  PIC X.                                          00001660
           02 MCODTRAP  PIC X.                                          00001670
           02 MCODTRAH  PIC X.                                          00001680
           02 MCODTRAV  PIC X.                                          00001690
           02 MCODTRAO  PIC X(4).                                       00001700
      * CICS DE TRAVAIL                                                 00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MCICSA    PIC X.                                          00001730
           02 MCICSC    PIC X.                                          00001740
           02 MCICSP    PIC X.                                          00001750
           02 MCICSH    PIC X.                                          00001760
           02 MCICSV    PIC X.                                          00001770
           02 MCICSO    PIC X(5).                                       00001780
      * NETNAME                                                         00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MNETNAMA  PIC X.                                          00001810
           02 MNETNAMC  PIC X.                                          00001820
           02 MNETNAMP  PIC X.                                          00001830
           02 MNETNAMH  PIC X.                                          00001840
           02 MNETNAMV  PIC X.                                          00001850
           02 MNETNAMO  PIC X(8).                                       00001860
      * CODE TERMINAL                                                   00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MSCREENA  PIC X.                                          00001890
           02 MSCREENC  PIC X.                                          00001900
           02 MSCREENP  PIC X.                                          00001910
           02 MSCREENH  PIC X.                                          00001920
           02 MSCREENV  PIC X.                                          00001930
           02 MSCREENO  PIC X(5).                                       00001940
                                                                                
