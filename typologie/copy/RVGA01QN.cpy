      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PPSIG VALEURS DES FICHIERS SIGA        *        
      *----------------------------------------------------------------*        
       01  RVGA01QN.                                                            
           05  PPSIG-CTABLEG2    PIC X(15).                                     
           05  PPSIG-CTABLEG2-REDEF REDEFINES PPSIG-CTABLEG2.                   
               10  PPSIG-CTYPSIGA        PIC X(01).                             
           05  PPSIG-WTABLEG     PIC X(80).                                     
           05  PPSIG-WTABLEG-REDEF  REDEFINES PPSIG-WTABLEG.                    
               10  PPSIG-WACTIF          PIC X(01).                             
               10  PPSIG-WCONST          PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01QN-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PPSIG-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PPSIG-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PPSIG-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PPSIG-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
