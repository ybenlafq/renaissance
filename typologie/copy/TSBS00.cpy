      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      *                    TS SPECIFIQUE TBS00                        * 00000020
      ***************************************************************** 00000030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 TS-LONG             PIC S9(4) COMP VALUE +125.                00000040
      *--                                                                       
       01 TS-LONG             PIC S9(4) COMP-5 VALUE +125.                      
      *}                                                                        
       01 TS-DATA.                                                      00000050
          05 TS-LIGNE.                                                  00000060
             10 TS-MNCOFFRE   PIC X(07).                                00000070
             10 TS-MCFAM      PIC X(05).                                00000080
             10 TS-MCMARQ     PIC X(05).                                00000090
             10 TS-MCASSOR    PIC X(05).                                00000100
             10 TS-MLCOFFRE   PIC X(20).                                00000110
             10 TS-MLCFAM     PIC X(20).                                00000120
             10 TS-MLCMARQ    PIC X(20).                                00000130
             10 TS-MLASSOR    PIC X(20).                                00000140
             10 TS-SEGMENT    PIC X(20).                                00000150
             10 TS-MACTION    PIC X(03).                                00000160
                                                                                
