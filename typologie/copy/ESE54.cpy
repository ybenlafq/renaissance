      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESE01   ESE01                                              00000020
      ***************************************************************** 00000030
       01   ESE54I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * FONCTION                                                        00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MWFONCI   PIC X(3).                                       00000200
      * CODE SERVICE                                                    00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCSERVL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCSERVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCSERVF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCSERVI  PIC X(5).                                       00000250
      * LIBELLE LONG CODE SERVICE                                       00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCSERVL  COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MLCSERVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCSERVF  PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MLCSERVI  PIC X(20).                                      00000300
      * CODE OPERATEUR                                                  00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPERL   COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MCOPERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOPERF   PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MCOPERI   PIC X(5).                                       00000350
      * LIBELLE CODE OPERATEUR                                          00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLOPERL   COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MLOPERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLOPERF   PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MLOPERI   PIC X(20).                                      00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRMPACL  COMP PIC S9(4).                                 00000410
      *--                                                                       
           02 MPRMPACL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPRMPACF  PIC X.                                          00000420
           02 FILLER    PIC X(4).                                       00000430
           02 MPRMPACI  PIC X(8).                                       00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDPRMPACL      COMP PIC S9(4).                            00000450
      *--                                                                       
           02 MDPRMPACL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDPRMPACF      PIC X.                                     00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MDPRMPACI      PIC X(8).                                  00000480
      * CODE FAMILLE                                                    00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCFAMI    PIC X(5).                                       00000530
      * LIBELLE CODE FAMILLE                                            00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MLFAMI    PIC X(20).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRMPNL   COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MPRMPNL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPRMPNF   PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MPRMPNI   PIC X(8).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDPRMPNL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MDPRMPNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDPRMPNF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MDPRMPNI  PIC X(8).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSIGNEL   COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MSIGNEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSIGNEF   PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MSIGNEI   PIC X.                                          00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPOSITIFL      COMP PIC S9(4).                            00000710
      *--                                                                       
           02 MPOSITIFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPOSITIFF      PIC X.                                     00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MPOSITIFI      PIC X(3).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNEGATIFL      COMP PIC S9(4).                            00000750
      *--                                                                       
           02 MNEGATIFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNEGATIFF      PIC X.                                     00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MNEGATIFI      PIC X(3).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNULL     COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MNULL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNULF     PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MNULI     PIC X(3).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MPAGEI    PIC X(3).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000870
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MPAGEMAXI      PIC X(3).                                  00000900
           02 MTABLEI OCCURS   8 TIMES .                                00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNZONPL      COMP PIC S9(4).                            00000920
      *--                                                                       
             03 MNZONPL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNZONPF      PIC X.                                     00000930
             03 FILLER  PIC X(4).                                       00000940
             03 MNZONPI      PIC X(2).                                  00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIXACL     COMP PIC S9(4).                            00000960
      *--                                                                       
             03 MPRIXACL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPRIXACF     PIC X.                                     00000970
             03 FILLER  PIC X(4).                                       00000980
             03 MPRIXACI     PIC X(8).                                  00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEPACL    COMP PIC S9(4).                            00001000
      *--                                                                       
             03 MDATEPACL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDATEPACF    PIC X.                                     00001010
             03 FILLER  PIC X(4).                                       00001020
             03 MDATEPACI    PIC X(8).                                  00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MINTERACL    COMP PIC S9(4).                            00001040
      *--                                                                       
             03 MINTERACL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MINTERACF    PIC X.                                     00001050
             03 FILLER  PIC X(4).                                       00001060
             03 MINTERACI    PIC X(5).                                  00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEIACL    COMP PIC S9(4).                            00001080
      *--                                                                       
             03 MDATEIACL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDATEIACF    PIC X.                                     00001090
             03 FILLER  PIC X(4).                                       00001100
             03 MDATEIACI    PIC X(8).                                  00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIXNL      COMP PIC S9(4).                            00001120
      *--                                                                       
             03 MPRIXNL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPRIXNF      PIC X.                                     00001130
             03 FILLER  PIC X(4).                                       00001140
             03 MPRIXNI      PIC X(8).                                  00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MINTERNL     COMP PIC S9(4).                            00001160
      *--                                                                       
             03 MINTERNL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MINTERNF     PIC X.                                     00001170
             03 FILLER  PIC X(4).                                       00001180
             03 MINTERNI     PIC X(5).                                  00001190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATENL      COMP PIC S9(4).                            00001200
      *--                                                                       
             03 MDATENL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDATENF      PIC X.                                     00001210
             03 FILLER  PIC X(4).                                       00001220
             03 MDATENI      PIC X(8).                                  00001230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMBUL   COMP PIC S9(4).                                 00001240
      *--                                                                       
             03 MMBUL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MMBUF   PIC X.                                          00001250
             03 FILLER  PIC X(4).                                       00001260
             03 MMBUI   PIC X(7).                                       00001270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTMBUL  COMP PIC S9(4).                                 00001280
      *--                                                                       
             03 MTMBUL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTMBUF  PIC X.                                          00001290
             03 FILLER  PIC X(4).                                       00001300
             03 MTMBUI  PIC X(4).                                       00001310
      * ZONE CMD AIDA                                                   00001320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001330
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001340
           02 FILLER    PIC X(4).                                       00001350
           02 MZONCMDI  PIC X(15).                                      00001360
      * MESSAGE ERREUR                                                  00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001380
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MLIBERRI  PIC X(58).                                      00001410
      * CODE TRANSACTION                                                00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MCODTRAI  PIC X(4).                                       00001460
      * CICS DE TRAVAIL                                                 00001470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001480
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001490
           02 FILLER    PIC X(4).                                       00001500
           02 MCICSI    PIC X(5).                                       00001510
      * NETNAME                                                         00001520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001530
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001540
           02 FILLER    PIC X(4).                                       00001550
           02 MNETNAMI  PIC X(8).                                       00001560
      * CODE TERMINAL                                                   00001570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001580
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001590
           02 FILLER    PIC X(4).                                       00001600
           02 MSCREENI  PIC X(5).                                       00001610
      ***************************************************************** 00001620
      * SDF: ESE01   ESE01                                              00001630
      ***************************************************************** 00001640
       01   ESE54O REDEFINES ESE54I.                                    00001650
           02 FILLER    PIC X(12).                                      00001660
      * DATE DU JOUR                                                    00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MDATJOUA  PIC X.                                          00001690
           02 MDATJOUC  PIC X.                                          00001700
           02 MDATJOUP  PIC X.                                          00001710
           02 MDATJOUH  PIC X.                                          00001720
           02 MDATJOUV  PIC X.                                          00001730
           02 MDATJOUO  PIC X(10).                                      00001740
      * HEURE                                                           00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MTIMJOUA  PIC X.                                          00001770
           02 MTIMJOUC  PIC X.                                          00001780
           02 MTIMJOUP  PIC X.                                          00001790
           02 MTIMJOUH  PIC X.                                          00001800
           02 MTIMJOUV  PIC X.                                          00001810
           02 MTIMJOUO  PIC X(5).                                       00001820
      * FONCTION                                                        00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MWFONCA   PIC X.                                          00001850
           02 MWFONCC   PIC X.                                          00001860
           02 MWFONCP   PIC X.                                          00001870
           02 MWFONCH   PIC X.                                          00001880
           02 MWFONCV   PIC X.                                          00001890
           02 MWFONCO   PIC X(3).                                       00001900
      * CODE SERVICE                                                    00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MNCSERVA  PIC X.                                          00001930
           02 MNCSERVC  PIC X.                                          00001940
           02 MNCSERVP  PIC X.                                          00001950
           02 MNCSERVH  PIC X.                                          00001960
           02 MNCSERVV  PIC X.                                          00001970
           02 MNCSERVO  PIC X(5).                                       00001980
      * LIBELLE LONG CODE SERVICE                                       00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MLCSERVA  PIC X.                                          00002010
           02 MLCSERVC  PIC X.                                          00002020
           02 MLCSERVP  PIC X.                                          00002030
           02 MLCSERVH  PIC X.                                          00002040
           02 MLCSERVV  PIC X.                                          00002050
           02 MLCSERVO  PIC X(20).                                      00002060
      * CODE OPERATEUR                                                  00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MCOPERA   PIC X.                                          00002090
           02 MCOPERC   PIC X.                                          00002100
           02 MCOPERP   PIC X.                                          00002110
           02 MCOPERH   PIC X.                                          00002120
           02 MCOPERV   PIC X.                                          00002130
           02 MCOPERO   PIC X(5).                                       00002140
      * LIBELLE CODE OPERATEUR                                          00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MLOPERA   PIC X.                                          00002170
           02 MLOPERC   PIC X.                                          00002180
           02 MLOPERP   PIC X.                                          00002190
           02 MLOPERH   PIC X.                                          00002200
           02 MLOPERV   PIC X.                                          00002210
           02 MLOPERO   PIC X(20).                                      00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MPRMPACA  PIC X.                                          00002240
           02 MPRMPACC  PIC X.                                          00002250
           02 MPRMPACP  PIC X.                                          00002260
           02 MPRMPACH  PIC X.                                          00002270
           02 MPRMPACV  PIC X.                                          00002280
           02 MPRMPACO  PIC X(8).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MDPRMPACA      PIC X.                                     00002310
           02 MDPRMPACC PIC X.                                          00002320
           02 MDPRMPACP PIC X.                                          00002330
           02 MDPRMPACH PIC X.                                          00002340
           02 MDPRMPACV PIC X.                                          00002350
           02 MDPRMPACO      PIC X(8).                                  00002360
      * CODE FAMILLE                                                    00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MCFAMA    PIC X.                                          00002390
           02 MCFAMC    PIC X.                                          00002400
           02 MCFAMP    PIC X.                                          00002410
           02 MCFAMH    PIC X.                                          00002420
           02 MCFAMV    PIC X.                                          00002430
           02 MCFAMO    PIC X(5).                                       00002440
      * LIBELLE CODE FAMILLE                                            00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MLFAMA    PIC X.                                          00002470
           02 MLFAMC    PIC X.                                          00002480
           02 MLFAMP    PIC X.                                          00002490
           02 MLFAMH    PIC X.                                          00002500
           02 MLFAMV    PIC X.                                          00002510
           02 MLFAMO    PIC X(20).                                      00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MPRMPNA   PIC X.                                          00002540
           02 MPRMPNC   PIC X.                                          00002550
           02 MPRMPNP   PIC X.                                          00002560
           02 MPRMPNH   PIC X.                                          00002570
           02 MPRMPNV   PIC X.                                          00002580
           02 MPRMPNO   PIC X(8).                                       00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MDPRMPNA  PIC X.                                          00002610
           02 MDPRMPNC  PIC X.                                          00002620
           02 MDPRMPNP  PIC X.                                          00002630
           02 MDPRMPNH  PIC X.                                          00002640
           02 MDPRMPNV  PIC X.                                          00002650
           02 MDPRMPNO  PIC X(8).                                       00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MSIGNEA   PIC X.                                          00002680
           02 MSIGNEC   PIC X.                                          00002690
           02 MSIGNEP   PIC X.                                          00002700
           02 MSIGNEH   PIC X.                                          00002710
           02 MSIGNEV   PIC X.                                          00002720
           02 MSIGNEO   PIC X.                                          00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MPOSITIFA      PIC X.                                     00002750
           02 MPOSITIFC PIC X.                                          00002760
           02 MPOSITIFP PIC X.                                          00002770
           02 MPOSITIFH PIC X.                                          00002780
           02 MPOSITIFV PIC X.                                          00002790
           02 MPOSITIFO      PIC X(3).                                  00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MNEGATIFA      PIC X.                                     00002820
           02 MNEGATIFC PIC X.                                          00002830
           02 MNEGATIFP PIC X.                                          00002840
           02 MNEGATIFH PIC X.                                          00002850
           02 MNEGATIFV PIC X.                                          00002860
           02 MNEGATIFO      PIC X(3).                                  00002870
           02 FILLER    PIC X(2).                                       00002880
           02 MNULA     PIC X.                                          00002890
           02 MNULC     PIC X.                                          00002900
           02 MNULP     PIC X.                                          00002910
           02 MNULH     PIC X.                                          00002920
           02 MNULV     PIC X.                                          00002930
           02 MNULO     PIC X(3).                                       00002940
           02 FILLER    PIC X(2).                                       00002950
           02 MPAGEA    PIC X.                                          00002960
           02 MPAGEC    PIC X.                                          00002970
           02 MPAGEP    PIC X.                                          00002980
           02 MPAGEH    PIC X.                                          00002990
           02 MPAGEV    PIC X.                                          00003000
           02 MPAGEO    PIC X(3).                                       00003010
           02 FILLER    PIC X(2).                                       00003020
           02 MPAGEMAXA      PIC X.                                     00003030
           02 MPAGEMAXC PIC X.                                          00003040
           02 MPAGEMAXP PIC X.                                          00003050
           02 MPAGEMAXH PIC X.                                          00003060
           02 MPAGEMAXV PIC X.                                          00003070
           02 MPAGEMAXO      PIC X(3).                                  00003080
           02 MTABLEO OCCURS   8 TIMES .                                00003090
             03 FILLER       PIC X(2).                                  00003100
             03 MNZONPA      PIC X.                                     00003110
             03 MNZONPC PIC X.                                          00003120
             03 MNZONPP PIC X.                                          00003130
             03 MNZONPH PIC X.                                          00003140
             03 MNZONPV PIC X.                                          00003150
             03 MNZONPO      PIC X(2).                                  00003160
             03 FILLER       PIC X(2).                                  00003170
             03 MPRIXACA     PIC X.                                     00003180
             03 MPRIXACC     PIC X.                                     00003190
             03 MPRIXACP     PIC X.                                     00003200
             03 MPRIXACH     PIC X.                                     00003210
             03 MPRIXACV     PIC X.                                     00003220
             03 MPRIXACO     PIC X(8).                                  00003230
             03 FILLER       PIC X(2).                                  00003240
             03 MDATEPACA    PIC X.                                     00003250
             03 MDATEPACC    PIC X.                                     00003260
             03 MDATEPACP    PIC X.                                     00003270
             03 MDATEPACH    PIC X.                                     00003280
             03 MDATEPACV    PIC X.                                     00003290
             03 MDATEPACO    PIC X(8).                                  00003300
             03 FILLER       PIC X(2).                                  00003310
             03 MINTERACA    PIC X.                                     00003320
             03 MINTERACC    PIC X.                                     00003330
             03 MINTERACP    PIC X.                                     00003340
             03 MINTERACH    PIC X.                                     00003350
             03 MINTERACV    PIC X.                                     00003360
             03 MINTERACO    PIC X(5).                                  00003370
             03 FILLER       PIC X(2).                                  00003380
             03 MDATEIACA    PIC X.                                     00003390
             03 MDATEIACC    PIC X.                                     00003400
             03 MDATEIACP    PIC X.                                     00003410
             03 MDATEIACH    PIC X.                                     00003420
             03 MDATEIACV    PIC X.                                     00003430
             03 MDATEIACO    PIC X(8).                                  00003440
             03 FILLER       PIC X(2).                                  00003450
             03 MPRIXNA      PIC X.                                     00003460
             03 MPRIXNC PIC X.                                          00003470
             03 MPRIXNP PIC X.                                          00003480
             03 MPRIXNH PIC X.                                          00003490
             03 MPRIXNV PIC X.                                          00003500
             03 MPRIXNO      PIC X(8).                                  00003510
             03 FILLER       PIC X(2).                                  00003520
             03 MINTERNA     PIC X.                                     00003530
             03 MINTERNC     PIC X.                                     00003540
             03 MINTERNP     PIC X.                                     00003550
             03 MINTERNH     PIC X.                                     00003560
             03 MINTERNV     PIC X.                                     00003570
             03 MINTERNO     PIC X(5).                                  00003580
             03 FILLER       PIC X(2).                                  00003590
             03 MDATENA      PIC X.                                     00003600
             03 MDATENC PIC X.                                          00003610
             03 MDATENP PIC X.                                          00003620
             03 MDATENH PIC X.                                          00003630
             03 MDATENV PIC X.                                          00003640
             03 MDATENO      PIC X(8).                                  00003650
             03 FILLER       PIC X(2).                                  00003660
             03 MMBUA   PIC X.                                          00003670
             03 MMBUC   PIC X.                                          00003680
             03 MMBUP   PIC X.                                          00003690
             03 MMBUH   PIC X.                                          00003700
             03 MMBUV   PIC X.                                          00003710
             03 MMBUO   PIC X(7).                                       00003720
             03 FILLER       PIC X(2).                                  00003730
             03 MTMBUA  PIC X.                                          00003740
             03 MTMBUC  PIC X.                                          00003750
             03 MTMBUP  PIC X.                                          00003760
             03 MTMBUH  PIC X.                                          00003770
             03 MTMBUV  PIC X.                                          00003780
             03 MTMBUO  PIC X(4).                                       00003790
      * ZONE CMD AIDA                                                   00003800
           02 FILLER    PIC X(2).                                       00003810
           02 MZONCMDA  PIC X.                                          00003820
           02 MZONCMDC  PIC X.                                          00003830
           02 MZONCMDP  PIC X.                                          00003840
           02 MZONCMDH  PIC X.                                          00003850
           02 MZONCMDV  PIC X.                                          00003860
           02 MZONCMDO  PIC X(15).                                      00003870
      * MESSAGE ERREUR                                                  00003880
           02 FILLER    PIC X(2).                                       00003890
           02 MLIBERRA  PIC X.                                          00003900
           02 MLIBERRC  PIC X.                                          00003910
           02 MLIBERRP  PIC X.                                          00003920
           02 MLIBERRH  PIC X.                                          00003930
           02 MLIBERRV  PIC X.                                          00003940
           02 MLIBERRO  PIC X(58).                                      00003950
      * CODE TRANSACTION                                                00003960
           02 FILLER    PIC X(2).                                       00003970
           02 MCODTRAA  PIC X.                                          00003980
           02 MCODTRAC  PIC X.                                          00003990
           02 MCODTRAP  PIC X.                                          00004000
           02 MCODTRAH  PIC X.                                          00004010
           02 MCODTRAV  PIC X.                                          00004020
           02 MCODTRAO  PIC X(4).                                       00004030
      * CICS DE TRAVAIL                                                 00004040
           02 FILLER    PIC X(2).                                       00004050
           02 MCICSA    PIC X.                                          00004060
           02 MCICSC    PIC X.                                          00004070
           02 MCICSP    PIC X.                                          00004080
           02 MCICSH    PIC X.                                          00004090
           02 MCICSV    PIC X.                                          00004100
           02 MCICSO    PIC X(5).                                       00004110
      * NETNAME                                                         00004120
           02 FILLER    PIC X(2).                                       00004130
           02 MNETNAMA  PIC X.                                          00004140
           02 MNETNAMC  PIC X.                                          00004150
           02 MNETNAMP  PIC X.                                          00004160
           02 MNETNAMH  PIC X.                                          00004170
           02 MNETNAMV  PIC X.                                          00004180
           02 MNETNAMO  PIC X(8).                                       00004190
      * CODE TERMINAL                                                   00004200
           02 FILLER    PIC X(2).                                       00004210
           02 MSCREENA  PIC X.                                          00004220
           02 MSCREENC  PIC X.                                          00004230
           02 MSCREENP  PIC X.                                          00004240
           02 MSCREENH  PIC X.                                          00004250
           02 MSCREENV  PIC X.                                          00004260
           02 MSCREENO  PIC X(5).                                       00004270
                                                                                
