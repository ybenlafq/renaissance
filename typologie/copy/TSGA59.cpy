      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00010000
      * TS SPECIFIQUE TABLE RTGA59                                 *    00020000
      *       TR : GA50  IDENTIFICATION ARTICLE                    *    00030000
      *       PG : TGA59 CREATION DES PRIX STANDARDS ET COMMISSIONS*    00040000
      *       PE : TGA59 PASSAGE EN VERSION 0                      *    00040100
      **************************************************************    00050000
      *                                                                 00060000
      * ZONES RESERVEES APPLICATIVES ----------------------------- 100  00070000
      *                                                                 00080000
      * PROGRAMME  TGA59 : CREATION DES PRIX STANDARDS ET COMMISSIONS * 00090000
      *                    CREATION DE LA TABLE RTGA59                * 00100000
      *                                                                 00110000
      *------------------------------ LONGUEUR                          00120000
       01  TS59-LONG            PIC S9(3) COMP-3 VALUE 281.                     
       01  TS59-DONNEES.                                                00140001
      *------------------------------ CODIC                             00150000
           05 TS59-NCODIC       PIC X(7).                               00160001
           05 TS59-MAP.                                                 00170000
      *------------------------------ LIBELLE SOCIETE                   00180000
              10 TS59-LSOCIETE  PIC X(20).                              00190001
      *------------------------------ DATE EFFET PRIX DE VENTE / COMM   00200001
              10 TS59-DEFFET    PIC X(8).                               00210001
      *------------------------------ CODE MAJ                          00220001
              10 TS59-CMAJ      PIC X(01).                              00230001
      *------------------------------ PRIX DE VENTE TTC NUMERIQUE       00240001
              10 TS59-PRIXDAC   PIC 9(7)V99 COMP-3.                     00250001
      *------------------------------                                   00260000
           05 TS-GA59-LIGNE   OCCURS 10.                                00270000
      *------------------------------ ZONE DE PRIX                      00280001
              10 TS59-NZONPRIX     PIC X(2).                            00290001
      *------------------------------ PRIX DE VENTE TTC NUMERIQUE       00300000
              10 TS59-PSTDTTC      PIC 9(7)V99 COMP-3.                  00310000
      *------------------------------ COMMENTAIRE                       00320000
              10 TS59-LCOMMENT     PIC X(10).                           00330001
      *------------------------------ PRIX COMMISSION NUMERIQUE         00340000
      *       10 TS59-PCOMM        PIC 9(5) COMP-3.                     00350001
              10 TS59-PCOMM        PIC 9(5)V99 COMP-3.                          
      *------------------------------ PRIX DE VENTE SAISI (MEME A 0)    00360001
              10 TS59-SAISIE       PIC X.                               00370001
      *                                                                 00380000
              10 TS59-FLAG         PIC X(2).                                    
      ***************************************************************** 00390000
                                                                                
