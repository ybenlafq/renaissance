      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * DCLGEN TABLE(PNMD.RTGA36)                                      *        
      *        LIBRARY(AIDAC.USER.COPY(RVGA3600))                      *        
      ******************************************************************        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RTGA36                        *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA3600.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA3600.                                                            
      *}                                                                        
      *    *************************************************************        
      *                       CODE PRODUIT                                      
           10 GA36-NCODIC          PIC X(7).                                    
      *    *************************************************************        
      *                       CODE FAM NCG                                      
           10 GA36-CFAM            PIC X(5).                                    
      *    *************************************************************        
      *                       CODE FAM SAV                                      
           10 GA36-CFAMSAV         PIC X(5).                                    
      *    *************************************************************        
      *                       CODE APPLICATION                                  
           10 GA36-CAPP            PIC X(5).                                    
      *    *************************************************************        
      *                       CODE CATALOGUE                                    
           10 GA36-CCTLG           PIC X(3).                                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 5       *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA3600-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA3600-FLAGS.                                                      
      *}                                                                        
      *    *************************************************************        
      *                       CODE PRODUIT                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA36-NCODIC-F                                                     
      *       PIC S9(4) COMP.                                                   
      *--                                                                       
           10 GA36-NCODIC-F                                                     
              PIC S9(4) COMP-5.                                                 
      *}                                                                        
      *    *************************************************************        
      *                       CODE FAM NCG                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA36-CFAM-F                                                       
      *       PIC S9(4) COMP.                                                   
      *--                                                                       
           10 GA36-CFAM-F                                                       
              PIC S9(4) COMP-5.                                                 
      *}                                                                        
      *    *************************************************************        
      *                       CODE FAM SAV                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA36-CFAMSAV-F                                                    
      *       PIC S9(4) COMP.                                                   
      *--                                                                       
           10 GA36-CFAMSAV-F                                                    
              PIC S9(4) COMP-5.                                                 
      *}                                                                        
      *    *************************************************************        
      *                       CODE APPLICATION                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA36-CAPP-F                                                       
      *       PIC S9(4) COMP.                                                   
      *--                                                                       
           10 GA36-CAPP-F                                                       
              PIC S9(4) COMP-5.                                                 
      *}                                                                        
      *    *************************************************************        
      *                       CODE CATALOGUE                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA36-CCTLG-F                                                      
      *       PIC S9(4) COMP.                                                   
      *--                                                                       
           10 GA36-CCTLG-F                                                      
              PIC S9(4) COMP-5.                                                 
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      ******************************************************************        
                                                                                
