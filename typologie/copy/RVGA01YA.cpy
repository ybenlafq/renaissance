      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PRRGL CODES REGLEMENT GSM CT           *        
      *----------------------------------------------------------------*        
       01  RVGA01YA.                                                            
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  PRRGL-CTABLEG2    PIC X(15).                                     
           05  PRRGL-CTABLEG2-REDEF REDEFINES PRRGL-CTABLEG2.                   
               10  PRRGL-CODE            PIC X(03).                             
           05  PRRGL-WTABLEG     PIC X(80).                                     
           05  PRRGL-WTABLEG-REDEF  REDEFINES PRRGL-WTABLEG.                    
               10  PRRGL-LIBELLE         PIC X(20).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01YA-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRRGL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PRRGL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRRGL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PRRGL-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
