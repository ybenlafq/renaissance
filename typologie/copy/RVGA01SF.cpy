      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PRGES DEFINITION DES CODES GESTION     *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01S.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01S.                                                             
      *}                                                                        
           05  PRGES-CTABLEG2    PIC X(15).                                     
           05  PRGES-CTABLEG2-REDEF REDEFINES PRGES-CTABLEG2.                   
               10  PRGES-CGESTION        PIC X(05).                             
           05  PRGES-WTABLEG     PIC X(80).                                     
           05  PRGES-WTABLEG-REDEF  REDEFINES PRGES-WTABLEG.                    
               10  PRGES-WACTIF          PIC X(01).                             
               10  PRGES-LGESTION        PIC X(30).                             
               10  PRGES-WENVOI          PIC X(01).                             
               10  PRGES-TYPE            PIC X(01).                             
               10  PRGES-TABLE           PIC X(06).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01S-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01S-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRGES-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PRGES-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRGES-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PRGES-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
