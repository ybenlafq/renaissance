      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************00000010
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *00000020
      ******************************************************************00000030
      *                                                                *00000040
      *  PROJET     :                                                   00000050
      *  PROGRAMME  : MFA11                                            *00000060
      *  TITRE      : COMMAREA DU MODULE TP                            *00000070
      *               D'IMPRESSION DE LA LISTE DE MISE EN CASE         *00000080
      *               DES MUTATIONS CLIENTS POUR UNE PLATE FORME       *00000090
      *  LONGUEUR   : 200 C                                            *00000100
      *                                                                *00000110
      ******************************************************************00000120
       01  COMM-MA11-APPLI.                                             00000130
           05 COMM-MA11-ZONES-ENTREE.                                   00000140
             10 COMM-MA11-NSOCIETE           PIC X(3).                  00000150
             10 COMM-MA11-NLIEU              PIC X(3).                  00000160
             10 COMM-MA11-LLIEU              PIC X(20).                 00000170
             10 COMM-MA11-TRI                PIC X(1).                  00000180
             10 COMM-MA11-SSAAMMJJ           PIC X(8).                  00000190
             10 COMM-MA11-TNMUTATION.                                   00000200
                20 COMM-MA11-NMUTATION OCCURS 10 PIC X(7).              00000210
           05 COMM-MA11-ZONES-SORTIE.                                   00000220
             10 COMM-MA11-MESSAGE            PIC X(80).                 00000230
           05 COMM-MA11-FILLER          PIC X(15).                      00000240
                                                                                
