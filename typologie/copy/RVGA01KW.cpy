      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EFRDF MVT STOCK RENDUS GEF             *        
      *----------------------------------------------------------------*        
       01  RVGA01KW.                                                            
           05  EFRDF-CTABLEG2    PIC X(15).                                     
           05  EFRDF-CTABLEG2-REDEF REDEFINES EFRDF-CTABLEG2.                   
               10  EFRDF-CTRAIT          PIC X(05).                             
               10  EFRDF-CRENDU          PIC X(05).                             
           05  EFRDF-WTABLEG     PIC X(80).                                     
           05  EFRDF-WTABLEG-REDEF  REDEFINES EFRDF-WTABLEG.                    
               10  EFRDF-WACTIF          PIC X(01).                             
               10  EFRDF-CMOTIF          PIC X(10).                             
               10  EFRDF-DESTRET         PIC X(14).                             
               10  EFRDF-FLAGIR          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01KW-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EFRDF-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EFRDF-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EFRDF-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EFRDF-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
