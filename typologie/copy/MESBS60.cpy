      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00010000
      *   COPY MESSAGE MQ INTERROGATION DE VENTE B&S                  * 00020000
      ***************************************************************** 00030000
      *                                                                 00040000
      *                                                                 00050000
       01  WS-MQ10.                                                     00060000
         02 WS-QUEUE.                                                   00070000
               10   MQ10-MSGID     PIC    X(24).                        00080001
               10   MQ10-CORRELID  PIC    X(24).                        00081001
         02 WS-CODRET              PIC    XX.                           00090000
         02  MESSAGE60.                                                 00100000
           03 MESSAGE60-ENTETE.                                         00110000
               05   MES60-TYPE     PIC    X(3).                         00120001
               05   MES60-NSOCMSG  PIC    X(3).                         00130001
               05   MES60-NLIEUMSG PIC    X(3).                         00140001
               05   MES60-NSOCDST  PIC    X(3).                         00150001
               05   MES60-NLIEUDST PIC    X(3).                         00160001
               05   MES60-NORD     PIC    9(8).                         00170001
               05   MES60-LPROG    PIC    X(10).                        00180001
               05   MES60-DJOUR    PIC    X(8).                         00190001
               05   MES60-WSID     PIC    X(10).                        00200001
               05   MES60-USER     PIC    X(10).                        00210001
               05   MES60-CHRONO   PIC    9(7).                         00220001
               05   MES60-NBRMSG   PIC    9(7).                         00230001
               05   MES60-FILLER   PIC    X(30).                        00240001
      *                                                                 00250000
      *  DATA ENTETE : 1 OCCURENCE = 22 CAR                             00260000
         03 MESSAGE45-DATA.                                             00270001
      *  SOCIETE ET LIEU DE DEMANDE DE LA VENTE                         00280001
           05  MES60-NSOCD         PIC    X(3).                         00290001
           05  MES60-NLIEUD        PIC    X(3).                         00300001
      *  SOCIETE ET LIEU DE LA VENTE                                    00310001
           05  MES60-NSOCV         PIC    X(3).                         00320001
           05  MES60-NLIEUV        PIC    X(3).                         00330001
           05  MES60-NVENTE        PIC    X(7).                         00340001
           05  MES60-TYPVTE        PIC    X(1).                         00350001
      *  SOCIETE ET LIEU DE PAIEMENT DE LA VENTE                        00360001
           05  MES60-NSOCP         PIC    X(3).                         00370001
           05  MES60-NLIEUP        PIC    X(3).                         00380001
      *  TOP ACCUSE RECEPTION O/N                                       00390001
           05  MES60-ACCUSE        PIC    X(1).                         00400001
      *  ZONE CODE RETOUR = OK OU KO                                    00410001
           05  MES60-CRET          PIC    X(2).                         00420001
      *--------------------------------------------------------------*  00430000
                                                                                
