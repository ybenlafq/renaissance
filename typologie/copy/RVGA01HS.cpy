      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TRETO MOTIF DES RETOURS DE LIVRAISON   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01HS.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01HS.                                                            
      *}                                                                        
           05  TRETO-CTABLEG2    PIC X(15).                                     
           05  TRETO-CTABLEG2-REDEF REDEFINES TRETO-CTABLEG2.                   
               10  TRETO-CRETOUR         PIC X(05).                             
           05  TRETO-WTABLEG     PIC X(80).                                     
           05  TRETO-WTABLEG-REDEF  REDEFINES TRETO-WTABLEG.                    
               10  TRETO-LRETOUR         PIC X(20).                             
               10  TRETO-WRETOUR         PIC X(01).                             
               10  TRETO-WINPUTAB        PIC X(01).                             
               10  TRETO-AGREGAT         PIC X(10).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01HS-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01HS-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TRETO-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TRETO-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TRETO-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TRETO-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
