      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GMZON ACID/ZONE DE PRIX CB DECENTRAL   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01S5.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01S5.                                                            
      *}                                                                        
           05  GMZON-CTABLEG2    PIC X(15).                                     
           05  GMZON-CTABLEG2-REDEF REDEFINES GMZON-CTABLEG2.                   
               10  GMZON-ACID            PIC X(04).                             
           05  GMZON-WTABLEG     PIC X(80).                                     
           05  GMZON-WTABLEG-REDEF  REDEFINES GMZON-WTABLEG.                    
               10  GMZON-NZONPRIX        PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01S5-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01S5-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GMZON-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GMZON-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GMZON-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GMZON-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
