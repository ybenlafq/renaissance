           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FGAFE AFFEC.SERV.DES FACTURES EMISES   *        
      *----------------------------------------------------------------*        
       01  RVGA01NC.                                                            
           05  FGAFE-CTABLEG2    PIC X(15).                                     
           05  FGAFE-CTABLEG2-REDEF REDEFINES FGAFE-CTABLEG2.                   
               10  FGAFE-NSOC            PIC X(03).                             
           EXEC SQL END DECLARE SECTION END-EXEC.
               10  FGAFE-NSOC-N         REDEFINES FGAFE-NSOC                    
                                         PIC 9(03).                             
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  FGAFE-NATURE          PIC X(05).                             
               10  FGAFE-CVENT           PIC X(05).                             
           05  FGAFE-WTABLEG     PIC X(80).                                     
           05  FGAFE-WTABLEG-REDEF  REDEFINES FGAFE-WTABLEG.                    
               10  FGAFE-ACTIF           PIC X(01).                             
               10  FGAFE-SERVORIG        PIC X(05).                             
               10  FGAFE-WTRANSPO        PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01NC-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGAFE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FGAFE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGAFE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FGAFE-WTABLEG-F   PIC S9(4) COMP-5.                              
      *}
           EXEC SQL END DECLARE SECTION END-EXEC.
 
                                                                                
