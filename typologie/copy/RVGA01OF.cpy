      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE BATVA CODE TVA POUR LES BONS D ACHAT   *        
      *----------------------------------------------------------------*        
       01  RVGA01OF.                                                            
           05  BATVA-CTABLEG2    PIC X(15).                                     
           05  BATVA-CTABLEG2-REDEF REDEFINES BATVA-CTABLEG2.                   
               10  BATVA-CBATVA          PIC X(01).                             
           05  BATVA-WTABLEG     PIC X(80).                                     
           05  BATVA-WTABLEG-REDEF  REDEFINES BATVA-WTABLEG.                    
               10  BATVA-CTAUXTVA        PIC X(05).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01OF-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BATVA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  BATVA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BATVA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  BATVA-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
