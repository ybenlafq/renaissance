      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HEPAN CODES PANNE GHE                  *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01N5.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01N5.                                                            
      *}                                                                        
           05  HEPAN-CTABLEG2    PIC X(15).                                     
           05  HEPAN-CTABLEG2-REDEF REDEFINES HEPAN-CTABLEG2.                   
               10  HEPAN-CPANNE          PIC X(05).                             
           05  HEPAN-WTABLEG     PIC X(80).                                     
           05  HEPAN-WTABLEG-REDEF  REDEFINES HEPAN-WTABLEG.                    
               10  HEPAN-WACTIF          PIC X(01).                             
               10  HEPAN-LPANNE          PIC X(20).                             
               10  HEPAN-WGENER          PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01N5-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01N5-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HEPAN-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HEPAN-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HEPAN-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HEPAN-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
