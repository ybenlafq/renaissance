      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGA0400                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA0400                         
      **********************************************************                
       01  RVGA0400.                                                            
           02  GA04-NCONC                                                       
               PIC X(0004).                                                     
           02  GA04-NTYPCONC                                                    
               PIC X(0002).                                                     
           02  GA04-NZONPRIX                                                    
               PIC X(0002).                                                     
           02  GA04-LENSCONC                                                    
               PIC X(0015).                                                     
           02  GA04-LEMPCONC                                                    
               PIC X(0015).                                                     
           02  GA04-LADDR1CONC                                                  
               PIC X(0032).                                                     
           02  GA04-LADDR2CONC                                                  
               PIC X(0032).                                                     
           02  GA04-CPOSTCONC                                                   
               PIC X(0005).                                                     
           02  GA04-LVILLECONC                                                  
               PIC X(0018).                                                     
           02  GA04-DCRECONC                                                    
               PIC X(0008).                                                     
           02  GA04-CSTATUTCONC                                                 
               PIC X(0001).                                                     
           02  GA04-DSTATUTCONC                                                 
               PIC X(0008).                                                     
           02  GA04-CPONDCONC                                                   
               PIC X(0001).                                                     
           02  GA04-DRELVCONC                                                   
               PIC X(0008).                                                     
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGA0400                                  
      **********************************************************                
       01  RVGA0400-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA04-NCONC-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA04-NCONC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA04-NTYPCONC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA04-NTYPCONC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA04-NZONPRIX-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA04-NZONPRIX-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA04-LENSCONC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA04-LENSCONC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA04-LEMPCONC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA04-LEMPCONC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA04-LADDR1CONC-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA04-LADDR1CONC-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA04-LADDR2CONC-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA04-LADDR2CONC-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA04-CPOSTCONC-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA04-CPOSTCONC-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA04-LVILLECONC-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA04-LVILLECONC-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA04-DCRECONC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA04-DCRECONC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA04-CSTATUTCONC-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA04-CSTATUTCONC-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA04-DSTATUTCONC-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA04-DSTATUTCONC-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA04-CPONDCONC-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA04-CPONDCONC-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA04-DRELVCONC-F                                                 
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GA04-DRELVCONC-F                                                 
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
      *}                                                                        
