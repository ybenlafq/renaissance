      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRX100 AU 24/04/1998  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,06,BI,A,                          *        
      *                           19,04,BI,A,                          *        
      *                           23,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRX100.                                                        
            05 NOMETAT-IRX100           PIC X(6) VALUE 'IRX100'.                
            05 RUPTURES-IRX100.                                                 
           10 IRX100-NSOCIETE           PIC X(03).                      007  003
           10 IRX100-NLIEU              PIC X(03).                      010  003
           10 IRX100-CVENDEUR           PIC X(06).                      013  006
           10 IRX100-NCONC              PIC X(04).                      019  004
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRX100-SEQUENCE           PIC S9(04) COMP.                023  002
      *--                                                                       
           10 IRX100-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRX100.                                                   
           10 IRX100-HRELEVE            PIC X(03).                      025  003
           10 IRX100-LCONC              PIC X(20).                      028  020
           10 IRX100-LIBVENDEUR         PIC X(09).                      048  009
           10 IRX100-LLIEU              PIC X(20).                      057  020
           10 IRX100-LVENDEUR           PIC X(19).                      077  019
           10 IRX100-DACEM              PIC S9(05)      COMP-3.         096  003
           10 IRX100-ELABL              PIC S9(05)      COMP-3.         099  003
           10 IRX100-ELABR              PIC S9(05)      COMP-3.         102  003
           10 IRX100-TLMBL              PIC S9(05)      COMP-3.         105  003
           10 IRX100-TLMBR              PIC S9(05)      COMP-3.         108  003
           10 IRX100-DATEAU             PIC X(08).                      111  008
           10 IRX100-DATEDU             PIC X(08).                      119  008
           10 IRX100-DRELEVE            PIC X(08).                      127  008
            05 FILLER                      PIC X(378).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IRX100-LONG           PIC S9(4)   COMP  VALUE +134.           
      *                                                                         
      *--                                                                       
        01  DSECT-IRX100-LONG           PIC S9(4) COMP-5  VALUE +134.           
                                                                                
      *}                                                                        
