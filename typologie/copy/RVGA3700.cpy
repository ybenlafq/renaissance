      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE CMTD00.RTGA37                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVGA3700.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVGA3700.                                                             
      *}                                                                        
           10 GA37-CFAM            PIC X(05).                                   
           10 GA37-CECO            PIC X(03).                                   
           10 GA37-WACHVEN         PIC X(01).                                   
           10 GA37-DEFFET          PIC X(08).                                   
           10 GA37-CPAYS           PIC X(02).                                   
           10 GA37-PMONTANT        PIC S9(5)V9(2) COMP-3.                       
           10 GA37-DSYST           PIC S9(13)     COMP-3.                       
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVGA3700-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVGA3700-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA37-CFAM-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 GA37-CFAM-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA37-CECO-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 GA37-CECO-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA37-WACHVEN-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GA37-WACHVEN-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA37-DEFFET-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA37-DEFFET-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA37-CPAYS-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 GA37-CPAYS-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA37-PMONTANT-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA37-PMONTANT-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA37-DSYST-F         PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 GA37-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
