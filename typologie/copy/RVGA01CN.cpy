      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE ELEMG ZONE ELEMENTAIRE/GENERALITES     *        
      *----------------------------------------------------------------*        
       01  RVGA01CN.                                                            
           05  ELEMG-CTABLEG2    PIC X(15).                                     
           05  ELEMG-CTABLEG2-REDEF REDEFINES ELEMG-CTABLEG2.                   
               10  ELEMG-CZONELE         PIC X(05).                             
           05  ELEMG-WTABLEG     PIC X(80).                                     
           05  ELEMG-WTABLEG-REDEF  REDEFINES ELEMG-WTABLEG.                    
               10  ELEMG-LZONELE         PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01CN-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ELEMG-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  ELEMG-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ELEMG-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  ELEMG-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
