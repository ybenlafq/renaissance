      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERX13   ERX13                                              00000020
      ***************************************************************** 00000030
       01   ERX13I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTPAGEL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MTOTPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTOTPAGEF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MTOTPAGEI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCONCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCONCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCONCI   PIC X(4).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNSOCI    PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENSEIL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNENSEIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNENSEIF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNENSEII  PIC X(15).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNMAGI    PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMAGL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLMAGF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLMAGI    PIC X(20).                                      00000410
           02 MDETAILMUTI OCCURS   15 TIMES .                           00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MENSEIL      COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MENSEIL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MENSEIF      PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MENSEII      PIC X(15).                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MEMPLL  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MEMPLL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MEMPLF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MEMPLI  PIC X(15).                                      00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODEL  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MCODEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCODEF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCODEI  PIC X(4).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZONPRIL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MZONPRIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MZONPRIF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MZONPRII     PIC X(2).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPEL  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MTYPEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTYPEF  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MTYPEI  PIC X(20).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MLIBERRI  PIC X(78).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCODTRAI  PIC X(4).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCICSI    PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MNETNAMI  PIC X(8).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MSCREENI  PIC X(4).                                       00000820
      ***************************************************************** 00000830
      * SDF: ERX13   ERX13                                              00000840
      ***************************************************************** 00000850
       01   ERX13O REDEFINES ERX13I.                                    00000860
           02 FILLER    PIC X(12).                                      00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MDATJOUA  PIC X.                                          00000890
           02 MDATJOUC  PIC X.                                          00000900
           02 MDATJOUP  PIC X.                                          00000910
           02 MDATJOUH  PIC X.                                          00000920
           02 MDATJOUV  PIC X.                                          00000930
           02 MDATJOUO  PIC X(10).                                      00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MTIMJOUA  PIC X.                                          00000960
           02 MTIMJOUC  PIC X.                                          00000970
           02 MTIMJOUP  PIC X.                                          00000980
           02 MTIMJOUH  PIC X.                                          00000990
           02 MTIMJOUV  PIC X.                                          00001000
           02 MTIMJOUO  PIC X(5).                                       00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MNPAGEA   PIC X.                                          00001030
           02 MNPAGEC   PIC X.                                          00001040
           02 MNPAGEP   PIC X.                                          00001050
           02 MNPAGEH   PIC X.                                          00001060
           02 MNPAGEV   PIC X.                                          00001070
           02 MNPAGEO   PIC X(2).                                       00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MTOTPAGEA      PIC X.                                     00001100
           02 MTOTPAGEC PIC X.                                          00001110
           02 MTOTPAGEP PIC X.                                          00001120
           02 MTOTPAGEH PIC X.                                          00001130
           02 MTOTPAGEV PIC X.                                          00001140
           02 MTOTPAGEO      PIC X(2).                                  00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MNCONCA   PIC X.                                          00001170
           02 MNCONCC   PIC X.                                          00001180
           02 MNCONCP   PIC X.                                          00001190
           02 MNCONCH   PIC X.                                          00001200
           02 MNCONCV   PIC X.                                          00001210
           02 MNCONCO   PIC X(4).                                       00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MNSOCA    PIC X.                                          00001240
           02 MNSOCC    PIC X.                                          00001250
           02 MNSOCP    PIC X.                                          00001260
           02 MNSOCH    PIC X.                                          00001270
           02 MNSOCV    PIC X.                                          00001280
           02 MNSOCO    PIC X(3).                                       00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MNENSEIA  PIC X.                                          00001310
           02 MNENSEIC  PIC X.                                          00001320
           02 MNENSEIP  PIC X.                                          00001330
           02 MNENSEIH  PIC X.                                          00001340
           02 MNENSEIV  PIC X.                                          00001350
           02 MNENSEIO  PIC X(15).                                      00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MNMAGA    PIC X.                                          00001380
           02 MNMAGC    PIC X.                                          00001390
           02 MNMAGP    PIC X.                                          00001400
           02 MNMAGH    PIC X.                                          00001410
           02 MNMAGV    PIC X.                                          00001420
           02 MNMAGO    PIC X(3).                                       00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MLMAGA    PIC X.                                          00001450
           02 MLMAGC    PIC X.                                          00001460
           02 MLMAGP    PIC X.                                          00001470
           02 MLMAGH    PIC X.                                          00001480
           02 MLMAGV    PIC X.                                          00001490
           02 MLMAGO    PIC X(20).                                      00001500
           02 MDETAILMUTO OCCURS   15 TIMES .                           00001510
             03 FILLER       PIC X(2).                                  00001520
             03 MENSEIA      PIC X.                                     00001530
             03 MENSEIC PIC X.                                          00001540
             03 MENSEIP PIC X.                                          00001550
             03 MENSEIH PIC X.                                          00001560
             03 MENSEIV PIC X.                                          00001570
             03 MENSEIO      PIC X(15).                                 00001580
             03 FILLER       PIC X(2).                                  00001590
             03 MEMPLA  PIC X.                                          00001600
             03 MEMPLC  PIC X.                                          00001610
             03 MEMPLP  PIC X.                                          00001620
             03 MEMPLH  PIC X.                                          00001630
             03 MEMPLV  PIC X.                                          00001640
             03 MEMPLO  PIC X(15).                                      00001650
             03 FILLER       PIC X(2).                                  00001660
             03 MCODEA  PIC X.                                          00001670
             03 MCODEC  PIC X.                                          00001680
             03 MCODEP  PIC X.                                          00001690
             03 MCODEH  PIC X.                                          00001700
             03 MCODEV  PIC X.                                          00001710
             03 MCODEO  PIC X(4).                                       00001720
             03 FILLER       PIC X(2).                                  00001730
             03 MZONPRIA     PIC X.                                     00001740
             03 MZONPRIC     PIC X.                                     00001750
             03 MZONPRIP     PIC X.                                     00001760
             03 MZONPRIH     PIC X.                                     00001770
             03 MZONPRIV     PIC X.                                     00001780
             03 MZONPRIO     PIC X(2).                                  00001790
             03 FILLER       PIC X(2).                                  00001800
             03 MTYPEA  PIC X.                                          00001810
             03 MTYPEC  PIC X.                                          00001820
             03 MTYPEP  PIC X.                                          00001830
             03 MTYPEH  PIC X.                                          00001840
             03 MTYPEV  PIC X.                                          00001850
             03 MTYPEO  PIC X(20).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MLIBERRA  PIC X.                                          00001880
           02 MLIBERRC  PIC X.                                          00001890
           02 MLIBERRP  PIC X.                                          00001900
           02 MLIBERRH  PIC X.                                          00001910
           02 MLIBERRV  PIC X.                                          00001920
           02 MLIBERRO  PIC X(78).                                      00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MCODTRAA  PIC X.                                          00001950
           02 MCODTRAC  PIC X.                                          00001960
           02 MCODTRAP  PIC X.                                          00001970
           02 MCODTRAH  PIC X.                                          00001980
           02 MCODTRAV  PIC X.                                          00001990
           02 MCODTRAO  PIC X(4).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MCICSA    PIC X.                                          00002020
           02 MCICSC    PIC X.                                          00002030
           02 MCICSP    PIC X.                                          00002040
           02 MCICSH    PIC X.                                          00002050
           02 MCICSV    PIC X.                                          00002060
           02 MCICSO    PIC X(5).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MNETNAMA  PIC X.                                          00002090
           02 MNETNAMC  PIC X.                                          00002100
           02 MNETNAMP  PIC X.                                          00002110
           02 MNETNAMH  PIC X.                                          00002120
           02 MNETNAMV  PIC X.                                          00002130
           02 MNETNAMO  PIC X(8).                                       00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MSCREENA  PIC X.                                          00002160
           02 MSCREENC  PIC X.                                          00002170
           02 MSCREENP  PIC X.                                          00002180
           02 MSCREENH  PIC X.                                          00002190
           02 MSCREENV  PIC X.                                          00002200
           02 MSCREENO  PIC X(4).                                       00002210
                                                                                
