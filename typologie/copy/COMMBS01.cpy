      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      *                                                                 00000010
       01  Z-COMMAREA-MBS01.                                            00000020
           02 COMM-MBS01-ENTETE-RETOUR.                                 00000030
      *---- BOOLEEN TYPE D'ERREUR                                       00000040
              03 COMM-MBS01-TYPERR     PIC 9(01).                       00000050
                 88 MBS01-NO-ERROR       VALUE 0.                       00000060
                 88 MBS01-APPL-ERROR     VALUE 1.                       00000070
                 88 MBS01-DB2-ERROR      VALUE 2.                       00000080
              03 COMM-MBS01-FUNC-SQL   PIC X(08).                       00000090
              03 COMM-MBS01-TABLE-NAME PIC X(08).                       00000100
              03 COMM-MBS01-SQLCODE    PIC S9(04).                      00000110
              03 COMM-MBS01-POSITION   PIC  9(03).                      00000120
      *---- BOOLEEN POSITIONNEMENT CURSEUR                              00000130
      *    02 COMM-MBS01-CURSEUR   PIC 9(02).                           00000140
      *       88 NO-CURSOR           VALUE 0.                           00000150
      *       88 COFFRE-CURS         VALUE 1.                           00000160
      *       88 CFAM-CURS           VALUE 2.                           00000170
      *       88 CMARQ-CURS          VALUE 3.                           00000180
      *       88 CASSOR-CURS         VALUE 4.                           00000190
      *---- BOOLEEN UTILISATION                                         00000200
           02 COMM-MBS01-UTILISATION  PIC 9.                            00000210
              88 AUTORISATION         VALUE 1.                          00000220
              88 CONTROLE             VALUE 2.                          00000230
                                                                        00000240
           02 COMM-MBS01-DROIT     PIC X(01).                           00000250
           02 COMM-MBS01-CICS      PIC X(05).                           00000260
                                                                        00000270
                                                                        00000280
           02 COMM-MBS01-NCOFFRE   PIC X(07).                           00000290
           02 COMM-MBS01-LCOFFRE   PIC X(20).                           00000300
           02 COMM-MBS01-CMARQ     PIC X(05).                           00000310
           02 COMM-MBS01-LMARQ     PIC X(20).                           00000320
           02 COMM-MBS01-CFAM      PIC X(05).                           00000330
           02 COMM-MBS01-LCFAM     PIC X(20).                           00000340
           02 COMM-MBS01-CASSOR    PIC X(05).                           00000350
           02 COMM-MBS01-LASSOR    PIC X(20).                           00000360
           02 COMM-MBS01-CPROFASS  PIC X(05).                           00000370
           02 COMM-MBS01-LPROFASS  PIC X(20).                           00000380
           02 COMM-MBS01-ENTITE    PIC X(07).                           00000390
           02 COMM-MBS01-LFAMENT   PIC X(05).                           00000400
           02 COMM-MBS01-LMARQENT  PIC X(05).                           00000410
           02 COMM-MBS01-LIBENT    PIC X(20).                           00000420
           02 COMM-MBS01-CTYPENT   PIC X(02).                           00000430
                                                                        00000440
      *---- BOOLEENS POSITIONNEMENT ERREURS                             00000450
           02 COMM-MBS01-ERROR1    PIC X(01).                           00000460
           02 COMM-MBS01-ERROR2    PIC X(01).                           00000470
           02 COMM-MBS01-ERROR3    PIC X(01).                           00000480
           02 COMM-MBS01-ERROR4    PIC X(01).                           00000490
           02 COMM-MBS01-ERROR5    PIC X(01).                           00000500
           02 COMM-MBS01-ERROR6    PIC X(01).                           00000510
           02 COMM-MBS01-ERROR7    PIC X(01).                           00000520
           02 COMM-MBS01-FILLER    PIC X(3959).                         00000530
                                                                                
