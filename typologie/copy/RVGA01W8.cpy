      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SYSID CONNECTIONS SYSID                *        
      *----------------------------------------------------------------*        
       01  RVGA01W8.                                                            
           05  SYSID-CTABLEG2    PIC X(15).                                     
           05  SYSID-CTABLEG2-REDEF REDEFINES SYSID-CTABLEG2.                   
               10  SYSID-NSOCORIG        PIC X(03).                             
               10  SYSID-NSOCORIG-N     REDEFINES SYSID-NSOCORIG                
                                         PIC 9(03).                             
               10  SYSID-NSOCDEST        PIC X(03).                             
               10  SYSID-NSOCDEST-N     REDEFINES SYSID-NSOCDEST                
                                         PIC 9(03).                             
           05  SYSID-WTABLEG     PIC X(80).                                     
           05  SYSID-WTABLEG-REDEF  REDEFINES SYSID-WTABLEG.                    
               10  SYSID-SYSID           PIC X(04).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01W8-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SYSID-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SYSID-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SYSID-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SYSID-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
