      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVNV1000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA VUE RVNV1000                           
      **********************************************************                
       01  RVNV1000.                                                            
      *---------------------  DTD WEBSPHERE                                     
           05  NV10-CDTDWCS         PIC X(20).                                  
      *---------------------  DTD GENERIQUE NCG                                 
           05  NV10-CDTDNCG         PIC X(20).                                  
      *---------------------  CODE ACTION                                       
           05  NV10-CACTION         PIC X(1).                                   
      *---------------------  NB MSG EN ATTENTE D ENVOI                         
           05  NV10-QNBATTENTE      PIC S9(7)V USAGE COMP-3.                    
      *---------------------  DELAI ATTENTE EN SEC                              
           05  NV10-QDELAIATT       PIC S9(5)V USAGE COMP-3.                    
      *---------------------  SEUIL MISE EN ALERTE                              
           05  NV10-QMSGALERT       PIC S9(7)V USAGE COMP-3.                    
      *---------------------  SEUIL CONSERVATION MSG EN ERREUR                  
           05  NV10-QMSGERROR       PIC S9(7)V USAGE COMP-3.                    
      *---------------------  SEUIL CONSERVATION MSG OK                         
           05  NV10-QMSGOK          PIC S9(7)V USAGE COMP-3.                    
      *---------------------  SEUIL CONSERVATION MSG SANS RETOUR                
           05  NV10-QMSGSSRET       PIC S9(7)V USAGE COMP-3.                    
      *---------------------  SEUIL REACTIVATION MSG                            
           05  NV10-QMSGREACT       PIC S9(7)V USAGE COMP-3.                    
      *---------------------  CODE FONCTION                                     
           05  NV10-CFONCTION       PIC X(3).                                   
      *---------------------  NOM TS                                            
           05  NV10-CNOMTS          PIC X(8).                                   
      *---------------------  NOM TRANSACTION                                   
           05  NV10-CNOMTRA         PIC X(4).                                   
      *---------------------  NOM MODULE EXTRACTR                               
           05  NV10-CNOMEXTRACT     PIC X(6).                                   
      *---------------------  DSYST                                             
           05  NV10-DSYST           PIC S9(13)V USAGE COMP-3.                   
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVNV1000                                  
      **********************************************************                
       01  RVNV1000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV10-CDTDWCS-F           PIC S9(4) COMP.                         
      *--                                                                       
           05  NV10-CDTDWCS-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV10-CDTDNCG-F           PIC S9(4) COMP.                         
      *--                                                                       
           05  NV10-CDTDNCG-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV10-CACTION-F           PIC S9(4) COMP.                         
      *--                                                                       
           05  NV10-CACTION-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV10-QNBATTENTE-F        PIC S9(4) COMP.                         
      *--                                                                       
           05  NV10-QNBATTENTE-F        PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV10-QDELAIATT-F         PIC S9(4) COMP.                         
      *--                                                                       
           05  NV10-QDELAIATT-F         PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV10-QMSGALERT-F         PIC S9(4) COMP.                         
      *--                                                                       
           05  NV10-QMSGALERT-F         PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV10-QMSGERROR-F         PIC S9(4) COMP.                         
      *--                                                                       
           05  NV10-QMSGERROR-F         PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV10-QMSGOK-F            PIC S9(4) COMP.                         
      *--                                                                       
           05  NV10-QMSGOK-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV10-QMSGSSRET-F         PIC S9(4) COMP.                         
      *--                                                                       
           05  NV10-QMSGSSRET-F         PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV10-QMSGREACT-F         PIC S9(4) COMP.                         
      *--                                                                       
           05  NV10-QMSGREACT-F         PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV10-CFONCTION-F         PIC S9(4) COMP.                         
      *--                                                                       
           05  NV10-CFONCTION-F         PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV10-CNOMTS-F            PIC S9(4) COMP.                         
      *--                                                                       
           05  NV10-CNOMTS-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV10-CNOMTRA-F           PIC S9(4) COMP.                         
      *--                                                                       
           05  NV10-CNOMTRA-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV10-CNOMEXTRACT-F       PIC S9(4) COMP.                         
      *--                                                                       
           05  NV10-CNOMEXTRACT-F       PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV10-DSYST-F             PIC S9(4) COMP.                         
      *                                                                         
      *--                                                                       
           05  NV10-DSYST-F             PIC S9(4) COMP-5.                       
                                                                                
      *}                                                                        
