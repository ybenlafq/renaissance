      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GVETR CODES REGIONS A L ETRANGER       *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01Q9.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01Q9.                                                            
      *}                                                                        
           05  GVETR-CTABLEG2    PIC X(15).                                     
           05  GVETR-CTABLEG2-REDEF REDEFINES GVETR-CTABLEG2.                   
               10  GVETR-CINSEE          PIC X(05).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  GVETR-CINSEE-N       REDEFINES GVETR-CINSEE                  
                                         PIC 9(05).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  GVETR-WTABLEG     PIC X(80).                                     
           05  GVETR-WTABLEG-REDEF  REDEFINES GVETR-WTABLEG.                    
               10  GVETR-LREGION         PIC X(26).                             
               10  GVETR-CPAYS           PIC X(05).                             
               10  GVETR-WPARBUR         PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01Q9-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01Q9-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GVETR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GVETR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GVETR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GVETR-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
