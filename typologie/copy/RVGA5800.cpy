      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGA5800                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA5800                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA5800.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA5800.                                                            
      *}                                                                        
           02  GA58-NCODIC                                                      
               PIC X(0007).                                                     
           02  GA58-NCODICLIE                                                   
               PIC X(0007).                                                     
           02  GA58-CTYPLIEN                                                    
               PIC X(0005).                                                     
           02  GA58-QTYPLIEN                                                    
               PIC S9(3) COMP-3.                                                
           02  GA58-WDEGRELIB                                                   
               PIC X(0005).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGA5800                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA5800-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA5800-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA58-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA58-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA58-NCODICLIE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA58-NCODICLIE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA58-CTYPLIEN-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA58-CTYPLIEN-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA58-QTYPLIEN-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA58-QTYPLIEN-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA58-WDEGRELIB-F                                                 
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GA58-WDEGRELIB-F                                                 
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
