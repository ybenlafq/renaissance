      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * CICS SAISIE ADRESSE D'UN LIEU                                   00000020
      ***************************************************************** 00000030
       01   ELI33I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MNSOCI    PIC X(3).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNLIEUI   PIC X(3).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MLLIEUI   PIC X(35).                                      00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPADRL      COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MCTYPADRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTYPADRF      PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MCTYPADRI      PIC X.                                     00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPLIEUL     COMP PIC S9(4).                            00000320
      *--                                                                       
           02 MCTYPLIEUL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTYPLIEUF     PIC X.                                     00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MCTYPLIEUI     PIC X.                                     00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADR1L    COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MADR1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MADR1F    PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MADR1I    PIC X(50).                                      00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADR2L    COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MADR2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MADR2F    PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MADR2I    PIC X(50).                                      00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADR3L    COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MADR3L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MADR3F    PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MADR3I    PIC X(50).                                      00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMMUNEL     COMP PIC S9(4).                            00000480
      *--                                                                       
           02 MLCOMMUNEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLCOMMUNEF     PIC X.                                     00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MLCOMMUNEI     PIC X(50).                                 00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTALL      COMP PIC S9(4).                            00000520
      *--                                                                       
           02 MCPOSTALL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPOSTALF      PIC X.                                     00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MCPOSTALI      PIC X(5).                                  00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLBUREAUL      COMP PIC S9(4).                            00000560
      *--                                                                       
           02 MLBUREAUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLBUREAUF      PIC X.                                     00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MLBUREAUI      PIC X(50).                                 00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPTL    COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MDEPTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDEPTF    PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MDEPTI    PIC X(2).                                       00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPAYL    COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MLPAYL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLPAYF    PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MLPAYI    PIC X(11).                                      00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPAYL    COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MCPAYL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCPAYF    PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MCPAYI    PIC X(3).                                       00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELL     COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MTELL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTELF     PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MTELI     PIC X(15).                                      00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFAXL     COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MFAXL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MFAXF     PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MFAXI     PIC X(15).                                      00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MGPSLATL  COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MGPSLATL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MGPSLATF  PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MGPSLATI  PIC X(15).                                      00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MGPSLONL  COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MGPSLONL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MGPSLONF  PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MGPSLONI  PIC X(15).                                      00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINSEEL  COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MCINSEEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCINSEEF  PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MCINSEEI  PIC X(5).                                       00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQAPPROL  COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MQAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQAPPROF  PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MQAPPROI  PIC X(3).                                       00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLANGL   COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MCLANGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCLANGF   PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MCLANGI   PIC X(3).                                       00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOPREGL   COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MOPREGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MOPREGF   PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MOPREGI   PIC X.                                          00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPCLIL  COMP PIC S9(4).                                 00001040
      *--                                                                       
           02 MTYPCLIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPCLIF  PIC X.                                          00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MTYPCLII  PIC X(6).                                       00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOCOCOL  COMP PIC S9(4).                                 00001080
      *--                                                                       
           02 MNOCOCOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNOCOCOF  PIC X.                                          00001090
           02 FILLER    PIC X(4).                                       00001100
           02 MNOCOCOI  PIC X(3).                                       00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRANSL  COMP PIC S9(4).                                 00001120
      *--                                                                       
           02 MCTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTRANSF  PIC X.                                          00001130
           02 FILLER    PIC X(4).                                       00001140
           02 MCTRANSI  PIC X(13).                                      00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPREGL  COMP PIC S9(4).                                 00001160
      *--                                                                       
           02 MTYPREGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPREGF  PIC X.                                          00001170
           02 FILLER    PIC X(4).                                       00001180
           02 MTYPREGI  PIC X(5).                                       00001190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFACONNAL     COMP PIC S9(4).                            00001200
      *--                                                                       
           02 MCFACONNAL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCFACONNAF     PIC X.                                     00001210
           02 FILLER    PIC X(4).                                       00001220
           02 MCFACONNAI     PIC X(2).                                  00001230
      * ZONE CMD AIDA                                                   00001240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001250
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001260
           02 FILLER    PIC X(4).                                       00001270
           02 MLIBERRI  PIC X(74).                                      00001280
      * CODE TRANSACTION                                                00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MCODTRAI  PIC X(4).                                       00001330
      * CICS DE TRAVAIL                                                 00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MCICSI    PIC X(5).                                       00001380
      * NETNAME                                                         00001390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001400
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001410
           02 FILLER    PIC X(4).                                       00001420
           02 MNETNAMI  PIC X(8).                                       00001430
      * CODE TERMINAL                                                   00001440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001450
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001460
           02 FILLER    PIC X(4).                                       00001470
           02 MSCREENI  PIC X(4).                                       00001480
      ***************************************************************** 00001490
      * CICS SAISIE ADRESSE D'UN LIEU                                   00001500
      ***************************************************************** 00001510
       01   ELI33O REDEFINES ELI33I.                                    00001520
           02 FILLER    PIC X(12).                                      00001530
      * DATE DU JOUR                                                    00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MDATJOUA  PIC X.                                          00001560
           02 MDATJOUC  PIC X.                                          00001570
           02 MDATJOUP  PIC X.                                          00001580
           02 MDATJOUH  PIC X.                                          00001590
           02 MDATJOUV  PIC X.                                          00001600
           02 MDATJOUO  PIC X(10).                                      00001610
      * HEURE                                                           00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MTIMJOUA  PIC X.                                          00001640
           02 MTIMJOUC  PIC X.                                          00001650
           02 MTIMJOUP  PIC X.                                          00001660
           02 MTIMJOUH  PIC X.                                          00001670
           02 MTIMJOUV  PIC X.                                          00001680
           02 MTIMJOUO  PIC X(5).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MNSOCA    PIC X.                                          00001710
           02 MNSOCC    PIC X.                                          00001720
           02 MNSOCP    PIC X.                                          00001730
           02 MNSOCH    PIC X.                                          00001740
           02 MNSOCV    PIC X.                                          00001750
           02 MNSOCO    PIC X(3).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MNLIEUA   PIC X.                                          00001780
           02 MNLIEUC   PIC X.                                          00001790
           02 MNLIEUP   PIC X.                                          00001800
           02 MNLIEUH   PIC X.                                          00001810
           02 MNLIEUV   PIC X.                                          00001820
           02 MNLIEUO   PIC X(3).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MLLIEUA   PIC X.                                          00001850
           02 MLLIEUC   PIC X.                                          00001860
           02 MLLIEUP   PIC X.                                          00001870
           02 MLLIEUH   PIC X.                                          00001880
           02 MLLIEUV   PIC X.                                          00001890
           02 MLLIEUO   PIC X(35).                                      00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MCTYPADRA      PIC X.                                     00001920
           02 MCTYPADRC PIC X.                                          00001930
           02 MCTYPADRP PIC X.                                          00001940
           02 MCTYPADRH PIC X.                                          00001950
           02 MCTYPADRV PIC X.                                          00001960
           02 MCTYPADRO      PIC X.                                     00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MCTYPLIEUA     PIC X.                                     00001990
           02 MCTYPLIEUC     PIC X.                                     00002000
           02 MCTYPLIEUP     PIC X.                                     00002010
           02 MCTYPLIEUH     PIC X.                                     00002020
           02 MCTYPLIEUV     PIC X.                                     00002030
           02 MCTYPLIEUO     PIC X.                                     00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MADR1A    PIC X.                                          00002060
           02 MADR1C    PIC X.                                          00002070
           02 MADR1P    PIC X.                                          00002080
           02 MADR1H    PIC X.                                          00002090
           02 MADR1V    PIC X.                                          00002100
           02 MADR1O    PIC X(50).                                      00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MADR2A    PIC X.                                          00002130
           02 MADR2C    PIC X.                                          00002140
           02 MADR2P    PIC X.                                          00002150
           02 MADR2H    PIC X.                                          00002160
           02 MADR2V    PIC X.                                          00002170
           02 MADR2O    PIC X(50).                                      00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MADR3A    PIC X.                                          00002200
           02 MADR3C    PIC X.                                          00002210
           02 MADR3P    PIC X.                                          00002220
           02 MADR3H    PIC X.                                          00002230
           02 MADR3V    PIC X.                                          00002240
           02 MADR3O    PIC X(50).                                      00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MLCOMMUNEA     PIC X.                                     00002270
           02 MLCOMMUNEC     PIC X.                                     00002280
           02 MLCOMMUNEP     PIC X.                                     00002290
           02 MLCOMMUNEH     PIC X.                                     00002300
           02 MLCOMMUNEV     PIC X.                                     00002310
           02 MLCOMMUNEO     PIC X(50).                                 00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MCPOSTALA      PIC X.                                     00002340
           02 MCPOSTALC PIC X.                                          00002350
           02 MCPOSTALP PIC X.                                          00002360
           02 MCPOSTALH PIC X.                                          00002370
           02 MCPOSTALV PIC X.                                          00002380
           02 MCPOSTALO      PIC X(5).                                  00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MLBUREAUA      PIC X.                                     00002410
           02 MLBUREAUC PIC X.                                          00002420
           02 MLBUREAUP PIC X.                                          00002430
           02 MLBUREAUH PIC X.                                          00002440
           02 MLBUREAUV PIC X.                                          00002450
           02 MLBUREAUO      PIC X(50).                                 00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MDEPTA    PIC X.                                          00002480
           02 MDEPTC    PIC X.                                          00002490
           02 MDEPTP    PIC X.                                          00002500
           02 MDEPTH    PIC X.                                          00002510
           02 MDEPTV    PIC X.                                          00002520
           02 MDEPTO    PIC X(2).                                       00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MLPAYA    PIC X.                                          00002550
           02 MLPAYC    PIC X.                                          00002560
           02 MLPAYP    PIC X.                                          00002570
           02 MLPAYH    PIC X.                                          00002580
           02 MLPAYV    PIC X.                                          00002590
           02 MLPAYO    PIC X(11).                                      00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MCPAYA    PIC X.                                          00002620
           02 MCPAYC    PIC X.                                          00002630
           02 MCPAYP    PIC X.                                          00002640
           02 MCPAYH    PIC X.                                          00002650
           02 MCPAYV    PIC X.                                          00002660
           02 MCPAYO    PIC X(3).                                       00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MTELA     PIC X.                                          00002690
           02 MTELC     PIC X.                                          00002700
           02 MTELP     PIC X.                                          00002710
           02 MTELH     PIC X.                                          00002720
           02 MTELV     PIC X.                                          00002730
           02 MTELO     PIC X(15).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MFAXA     PIC X.                                          00002760
           02 MFAXC     PIC X.                                          00002770
           02 MFAXP     PIC X.                                          00002780
           02 MFAXH     PIC X.                                          00002790
           02 MFAXV     PIC X.                                          00002800
           02 MFAXO     PIC X(15).                                      00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MGPSLATA  PIC X.                                          00002830
           02 MGPSLATC  PIC X.                                          00002840
           02 MGPSLATP  PIC X.                                          00002850
           02 MGPSLATH  PIC X.                                          00002860
           02 MGPSLATV  PIC X.                                          00002870
           02 MGPSLATO  PIC X(15).                                      00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MGPSLONA  PIC X.                                          00002900
           02 MGPSLONC  PIC X.                                          00002910
           02 MGPSLONP  PIC X.                                          00002920
           02 MGPSLONH  PIC X.                                          00002930
           02 MGPSLONV  PIC X.                                          00002940
           02 MGPSLONO  PIC X(15).                                      00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MCINSEEA  PIC X.                                          00002970
           02 MCINSEEC  PIC X.                                          00002980
           02 MCINSEEP  PIC X.                                          00002990
           02 MCINSEEH  PIC X.                                          00003000
           02 MCINSEEV  PIC X.                                          00003010
           02 MCINSEEO  PIC X(5).                                       00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MQAPPROA  PIC X.                                          00003040
           02 MQAPPROC  PIC X.                                          00003050
           02 MQAPPROP  PIC X.                                          00003060
           02 MQAPPROH  PIC X.                                          00003070
           02 MQAPPROV  PIC X.                                          00003080
           02 MQAPPROO  PIC X(3).                                       00003090
           02 FILLER    PIC X(2).                                       00003100
           02 MCLANGA   PIC X.                                          00003110
           02 MCLANGC   PIC X.                                          00003120
           02 MCLANGP   PIC X.                                          00003130
           02 MCLANGH   PIC X.                                          00003140
           02 MCLANGV   PIC X.                                          00003150
           02 MCLANGO   PIC X(3).                                       00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MOPREGA   PIC X.                                          00003180
           02 MOPREGC   PIC X.                                          00003190
           02 MOPREGP   PIC X.                                          00003200
           02 MOPREGH   PIC X.                                          00003210
           02 MOPREGV   PIC X.                                          00003220
           02 MOPREGO   PIC X.                                          00003230
           02 FILLER    PIC X(2).                                       00003240
           02 MTYPCLIA  PIC X.                                          00003250
           02 MTYPCLIC  PIC X.                                          00003260
           02 MTYPCLIP  PIC X.                                          00003270
           02 MTYPCLIH  PIC X.                                          00003280
           02 MTYPCLIV  PIC X.                                          00003290
           02 MTYPCLIO  PIC X(6).                                       00003300
           02 FILLER    PIC X(2).                                       00003310
           02 MNOCOCOA  PIC X.                                          00003320
           02 MNOCOCOC  PIC X.                                          00003330
           02 MNOCOCOP  PIC X.                                          00003340
           02 MNOCOCOH  PIC X.                                          00003350
           02 MNOCOCOV  PIC X.                                          00003360
           02 MNOCOCOO  PIC X(3).                                       00003370
           02 FILLER    PIC X(2).                                       00003380
           02 MCTRANSA  PIC X.                                          00003390
           02 MCTRANSC  PIC X.                                          00003400
           02 MCTRANSP  PIC X.                                          00003410
           02 MCTRANSH  PIC X.                                          00003420
           02 MCTRANSV  PIC X.                                          00003430
           02 MCTRANSO  PIC X(13).                                      00003440
           02 FILLER    PIC X(2).                                       00003450
           02 MTYPREGA  PIC X.                                          00003460
           02 MTYPREGC  PIC X.                                          00003470
           02 MTYPREGP  PIC X.                                          00003480
           02 MTYPREGH  PIC X.                                          00003490
           02 MTYPREGV  PIC X.                                          00003500
           02 MTYPREGO  PIC X(5).                                       00003510
           02 FILLER    PIC X(2).                                       00003520
           02 MCFACONNAA     PIC X.                                     00003530
           02 MCFACONNAC     PIC X.                                     00003540
           02 MCFACONNAP     PIC X.                                     00003550
           02 MCFACONNAH     PIC X.                                     00003560
           02 MCFACONNAV     PIC X.                                     00003570
           02 MCFACONNAO     PIC X(2).                                  00003580
      * ZONE CMD AIDA                                                   00003590
           02 FILLER    PIC X(2).                                       00003600
           02 MLIBERRA  PIC X.                                          00003610
           02 MLIBERRC  PIC X.                                          00003620
           02 MLIBERRP  PIC X.                                          00003630
           02 MLIBERRH  PIC X.                                          00003640
           02 MLIBERRV  PIC X.                                          00003650
           02 MLIBERRO  PIC X(74).                                      00003660
      * CODE TRANSACTION                                                00003670
           02 FILLER    PIC X(2).                                       00003680
           02 MCODTRAA  PIC X.                                          00003690
           02 MCODTRAC  PIC X.                                          00003700
           02 MCODTRAP  PIC X.                                          00003710
           02 MCODTRAH  PIC X.                                          00003720
           02 MCODTRAV  PIC X.                                          00003730
           02 MCODTRAO  PIC X(4).                                       00003740
      * CICS DE TRAVAIL                                                 00003750
           02 FILLER    PIC X(2).                                       00003760
           02 MCICSA    PIC X.                                          00003770
           02 MCICSC    PIC X.                                          00003780
           02 MCICSP    PIC X.                                          00003790
           02 MCICSH    PIC X.                                          00003800
           02 MCICSV    PIC X.                                          00003810
           02 MCICSO    PIC X(5).                                       00003820
      * NETNAME                                                         00003830
           02 FILLER    PIC X(2).                                       00003840
           02 MNETNAMA  PIC X.                                          00003850
           02 MNETNAMC  PIC X.                                          00003860
           02 MNETNAMP  PIC X.                                          00003870
           02 MNETNAMH  PIC X.                                          00003880
           02 MNETNAMV  PIC X.                                          00003890
           02 MNETNAMO  PIC X(8).                                       00003900
      * CODE TERMINAL                                                   00003910
           02 FILLER    PIC X(2).                                       00003920
           02 MSCREENA  PIC X.                                          00003930
           02 MSCREENC  PIC X.                                          00003940
           02 MSCREENP  PIC X.                                          00003950
           02 MSCREENH  PIC X.                                          00003960
           02 MSCREENV  PIC X.                                          00003970
           02 MSCREENO  PIC X(4).                                       00003980
                                                                                
