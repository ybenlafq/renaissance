      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE AV001 SOUS TABLE TYPE D AVOIR          *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVGA01UA.                                                            
           05  AV001-CTABLEG2    PIC X(15).                                     
           05  AV001-CTABLEG2-REDEF REDEFINES AV001-CTABLEG2.                   
               10  AV001-CTYPAV          PIC X(05).                             
           05  AV001-WTABLEG     PIC X(80).                                     
           05  AV001-WTABLEG-REDEF  REDEFINES AV001-WTABLEG.                    
               10  AV001-LTYPAV          PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01UA-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AV001-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  AV001-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AV001-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  AV001-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
