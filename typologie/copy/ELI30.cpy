      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * MENU PARAMETRAGE LIEUX                                          00000020
      ***************************************************************** 00000030
       01   ELI30I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MZONCMDI  PIC X(15).                                      00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPARAML  COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MCPARAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPARAMF  PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MCPARAMI  PIC X(5).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNSOCI    PIC X(3).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MNLIEUI   PIC X(3).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWADRL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MWADRL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MWADRF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MWADRI    PIC X.                                          00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPARAM1L      COMP PIC S9(4).                            00000360
      *--                                                                       
           02 MCPARAM1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPARAM1F      PIC X.                                     00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MCPARAM1I      PIC X(5).                                  00000390
      * ZONE CMD AIDA                                                   00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000410
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000420
           02 FILLER    PIC X(4).                                       00000430
           02 MLIBERRI  PIC X(74).                                      00000440
      * CODE TRANSACTION                                                00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCODTRAI  PIC X(4).                                       00000490
      * CICS DE TRAVAIL                                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MCICSI    PIC X(5).                                       00000540
      * NETNAME                                                         00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MNETNAMI  PIC X(8).                                       00000590
      * CODE TERMINAL                                                   00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000610
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MSCREENI  PIC X(4).                                       00000640
      ***************************************************************** 00000650
      * MENU PARAMETRAGE LIEUX                                          00000660
      ***************************************************************** 00000670
       01   ELI30O REDEFINES ELI30I.                                    00000680
           02 FILLER    PIC X(12).                                      00000690
      * DATE DU JOUR                                                    00000700
           02 FILLER    PIC X(2).                                       00000710
           02 MDATJOUA  PIC X.                                          00000720
           02 MDATJOUC  PIC X.                                          00000730
           02 MDATJOUP  PIC X.                                          00000740
           02 MDATJOUH  PIC X.                                          00000750
           02 MDATJOUV  PIC X.                                          00000760
           02 MDATJOUO  PIC X(10).                                      00000770
      * HEURE                                                           00000780
           02 FILLER    PIC X(2).                                       00000790
           02 MTIMJOUA  PIC X.                                          00000800
           02 MTIMJOUC  PIC X.                                          00000810
           02 MTIMJOUP  PIC X.                                          00000820
           02 MTIMJOUH  PIC X.                                          00000830
           02 MTIMJOUV  PIC X.                                          00000840
           02 MTIMJOUO  PIC X(5).                                       00000850
           02 FILLER    PIC X(2).                                       00000860
           02 MZONCMDA  PIC X.                                          00000870
           02 MZONCMDC  PIC X.                                          00000880
           02 MZONCMDP  PIC X.                                          00000890
           02 MZONCMDH  PIC X.                                          00000900
           02 MZONCMDV  PIC X.                                          00000910
           02 MZONCMDO  PIC X(15).                                      00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MCPARAMA  PIC X.                                          00000940
           02 MCPARAMC  PIC X.                                          00000950
           02 MCPARAMP  PIC X.                                          00000960
           02 MCPARAMH  PIC X.                                          00000970
           02 MCPARAMV  PIC X.                                          00000980
           02 MCPARAMO  PIC X(5).                                       00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MNSOCA    PIC X.                                          00001010
           02 MNSOCC    PIC X.                                          00001020
           02 MNSOCP    PIC X.                                          00001030
           02 MNSOCH    PIC X.                                          00001040
           02 MNSOCV    PIC X.                                          00001050
           02 MNSOCO    PIC X(3).                                       00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MNLIEUA   PIC X.                                          00001080
           02 MNLIEUC   PIC X.                                          00001090
           02 MNLIEUP   PIC X.                                          00001100
           02 MNLIEUH   PIC X.                                          00001110
           02 MNLIEUV   PIC X.                                          00001120
           02 MNLIEUO   PIC X(3).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MWADRA    PIC X.                                          00001150
           02 MWADRC    PIC X.                                          00001160
           02 MWADRP    PIC X.                                          00001170
           02 MWADRH    PIC X.                                          00001180
           02 MWADRV    PIC X.                                          00001190
           02 MWADRO    PIC X.                                          00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MCPARAM1A      PIC X.                                     00001220
           02 MCPARAM1C PIC X.                                          00001230
           02 MCPARAM1P PIC X.                                          00001240
           02 MCPARAM1H PIC X.                                          00001250
           02 MCPARAM1V PIC X.                                          00001260
           02 MCPARAM1O      PIC X(5).                                  00001270
      * ZONE CMD AIDA                                                   00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MLIBERRA  PIC X.                                          00001300
           02 MLIBERRC  PIC X.                                          00001310
           02 MLIBERRP  PIC X.                                          00001320
           02 MLIBERRH  PIC X.                                          00001330
           02 MLIBERRV  PIC X.                                          00001340
           02 MLIBERRO  PIC X(74).                                      00001350
      * CODE TRANSACTION                                                00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MCODTRAA  PIC X.                                          00001380
           02 MCODTRAC  PIC X.                                          00001390
           02 MCODTRAP  PIC X.                                          00001400
           02 MCODTRAH  PIC X.                                          00001410
           02 MCODTRAV  PIC X.                                          00001420
           02 MCODTRAO  PIC X(4).                                       00001430
      * CICS DE TRAVAIL                                                 00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MCICSA    PIC X.                                          00001460
           02 MCICSC    PIC X.                                          00001470
           02 MCICSP    PIC X.                                          00001480
           02 MCICSH    PIC X.                                          00001490
           02 MCICSV    PIC X.                                          00001500
           02 MCICSO    PIC X(5).                                       00001510
      * NETNAME                                                         00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MNETNAMA  PIC X.                                          00001540
           02 MNETNAMC  PIC X.                                          00001550
           02 MNETNAMP  PIC X.                                          00001560
           02 MNETNAMH  PIC X.                                          00001570
           02 MNETNAMV  PIC X.                                          00001580
           02 MNETNAMO  PIC X(8).                                       00001590
      * CODE TERMINAL                                                   00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MSCREENA  PIC X.                                          00001620
           02 MSCREENC  PIC X.                                          00001630
           02 MSCREENP  PIC X.                                          00001640
           02 MSCREENH  PIC X.                                          00001650
           02 MSCREENV  PIC X.                                          00001660
           02 MSCREENO  PIC X(4).                                       00001670
                                                                                
