      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PSGRP GROUPE DE CONTRAT DE SERVICE     *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01NV.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01NV.                                                            
      *}                                                                        
           05  PSGRP-CTABLEG2    PIC X(15).                                     
           05  PSGRP-CTABLEG2-REDEF REDEFINES PSGRP-CTABLEG2.                   
               10  PSGRP-CGRP            PIC X(05).                             
               10  PSGRP-TGRP            PIC X(01).                             
           05  PSGRP-WTABLEG     PIC X(80).                                     
           05  PSGRP-WTABLEG-REDEF  REDEFINES PSGRP-WTABLEG.                    
               10  PSGRP-LGRP            PIC X(20).                             
               10  PSGRP-WACTIF          PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01NV-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01NV-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PSGRP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PSGRP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PSGRP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PSGRP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
