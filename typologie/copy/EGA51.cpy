      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA51   EGA51                                              00000020
      ***************************************************************** 00000030
       01   EGA51I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * FONCTION                                                        00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MWFONCI   PIC X(3).                                       00000200
      * CODIC                                                           00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCODICI  PIC X(7).                                       00000250
      * REFERENCE FOURN                                                 00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MLREFI    PIC X(20).                                      00000300
      * DATE CREATION CODIC                                             00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCRECCL  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MDCRECCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDCRECCF  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MDCRECCI  PIC X(8).                                       00000350
      * CODE FAMILLE                                                    00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFAML    COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MNFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNFAMF    PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MNFAMI    PIC X(5).                                       00000400
      * LIBELLE FAMILLE                                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLFAMI    PIC X(20).                                      00000450
      * DATE MAJ CODIC                                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMAJCCL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MDMAJCCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDMAJCCF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MDMAJCCI  PIC X(8).                                       00000500
      * CODE MARQUE                                                     00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMARQL   COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MNMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNMARQF   PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MNMARQI   PIC X(5).                                       00000550
      * LIBELLE MARQUE                                                  00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MLMARQI   PIC X(20).                                      00000600
      * DATE 1ERE RECEPTION                                             00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRECCCL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MDRECCCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDRECCCF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MDRECCCI  PIC X(8).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCOLORML      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MCCOLORML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCCOLORMF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCCOLORMI      PIC X(5).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOLORML      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MLCOLORML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLCOLORMF      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLCOLORMI      PIC X(20).                                 00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWGVTEL   COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MWGVTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWGVTEF   PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MWGVTEI   PIC X(3).                                       00000770
      * CODE ASSORTIMENT                                                00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCASSORL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCASSORL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCASSORF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCASSORI  PIC X(5).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLASSORL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MLASSORL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLASSORF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MLASSORI  PIC X(20).                                      00000860
      * FLAG APPART DACEM                                               00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWDACEML  COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MWDACEML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWDACEMF  PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MWDACEMI  PIC X.                                          00000910
      * CODE TVA                                                        00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTVAL    COMP PIC S9(4).                                 00000930
      *--                                                                       
           02 MCTVAL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCTVAF    PIC X.                                          00000940
           02 FILLER    PIC X(4).                                       00000950
           02 MCTVAI    PIC X(5).                                       00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTVAL    COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MLTVAL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLTVAF    PIC X.                                          00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MLTVAI    PIC X(20).                                      00001000
      * FLAG TLM OU ELA                                                 00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTMELAL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MWTMELAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWTMELAF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MWTMELAI  PIC X.                                          00001050
      * CODE GARANTIE                                                   00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGARANL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCGARANL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCGARANF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCGARANI  PIC X(5).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLGARANL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MLGARANL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLGARANF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MLGARANI  PIC X(20).                                      00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGCONSL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCGCONSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCGCONSF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCGCONSI  PIC X(5).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGMGDL   COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MCGMGDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCGMGDF   PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MCGMGDI   PIC X(5).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCGMGDL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MLCGMGDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCGMGDF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MLCGMGDI  PIC X(20).                                      00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLGCONSL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MLGCONSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLGCONSF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MLGCONSI  PIC X(20).                                      00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEANL     COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MEANL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MEANF     PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MEANI     PIC X(13).                                      00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MREFTECHL      COMP PIC S9(4).                            00001350
      *--                                                                       
           02 MREFTECHL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MREFTECHF      PIC X.                                     00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MREFTECHI      PIC X(40).                                 00001380
           02 MWTABLEI OCCURS   5 TIMES .                               00001390
      * CODE TYPE DCL                                                   00001400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYDCLL     COMP PIC S9(4).                            00001410
      *--                                                                       
             03 MCTYDCLL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCTYDCLF     PIC X.                                     00001420
             03 FILLER  PIC X(4).                                       00001430
             03 MCTYDCLI     PIC X(5).                                  00001440
      * LIBELLE TYPE DCL                                                00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLTYDCLL     COMP PIC S9(4).                            00001460
      *--                                                                       
             03 MLTYDCLL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLTYDCLF     PIC X.                                     00001470
             03 FILLER  PIC X(4).                                       00001480
             03 MLTYDCLI     PIC X(20).                                 00001490
      * CODE GARANTIE CPLT                                              00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCGCPLTL     COMP PIC S9(4).                            00001510
      *--                                                                       
             03 MCGCPLTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCGCPLTF     PIC X.                                     00001520
             03 FILLER  PIC X(4).                                       00001530
             03 MCGCPLTI     PIC X(5).                                  00001540
      * LIBELLE G. CPLT                                                 00001550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLGCPLTL     COMP PIC S9(4).                            00001560
      *--                                                                       
             03 MLGCPLTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLGCPLTF     PIC X.                                     00001570
             03 FILLER  PIC X(4).                                       00001580
             03 MLGCPLTI     PIC X(20).                                 00001590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCVGCPLL     COMP PIC S9(4).                            00001600
      *--                                                                       
             03 MCVGCPLL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCVGCPLF     PIC X.                                     00001610
             03 FILLER  PIC X(4).                                       00001620
             03 MCVGCPLI     PIC X(3).                                  00001630
      * ZONE CMD AIDA                                                   00001640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001650
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001660
           02 FILLER    PIC X(4).                                       00001670
           02 MZONCMDI  PIC X(15).                                      00001680
      * MESSAGE ERREUR                                                  00001690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001700
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001710
           02 FILLER    PIC X(4).                                       00001720
           02 MLIBERRI  PIC X(58).                                      00001730
      * CODE TRANSACTION                                                00001740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001750
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001760
           02 FILLER    PIC X(4).                                       00001770
           02 MCODTRAI  PIC X(4).                                       00001780
      * CICS DE TRAVAIL                                                 00001790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001800
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001810
           02 FILLER    PIC X(4).                                       00001820
           02 MCICSI    PIC X(5).                                       00001830
      * NETNAME                                                         00001840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001850
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001860
           02 FILLER    PIC X(4).                                       00001870
           02 MNETNAMI  PIC X(8).                                       00001880
      * CODE TERMINAL                                                   00001890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001900
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001910
           02 FILLER    PIC X(4).                                       00001920
           02 MSCREENI  PIC X(5).                                       00001930
      ***************************************************************** 00001940
      * SDF: EGA51   EGA51                                              00001950
      ***************************************************************** 00001960
       01   EGA51O REDEFINES EGA51I.                                    00001970
           02 FILLER    PIC X(12).                                      00001980
      * DATE DU JOUR                                                    00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MDATJOUA  PIC X.                                          00002010
           02 MDATJOUC  PIC X.                                          00002020
           02 MDATJOUP  PIC X.                                          00002030
           02 MDATJOUH  PIC X.                                          00002040
           02 MDATJOUV  PIC X.                                          00002050
           02 MDATJOUO  PIC X(10).                                      00002060
      * HEURE                                                           00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MTIMJOUA  PIC X.                                          00002090
           02 MTIMJOUC  PIC X.                                          00002100
           02 MTIMJOUP  PIC X.                                          00002110
           02 MTIMJOUH  PIC X.                                          00002120
           02 MTIMJOUV  PIC X.                                          00002130
           02 MTIMJOUO  PIC X(5).                                       00002140
      * FONCTION                                                        00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MWFONCA   PIC X.                                          00002170
           02 MWFONCC   PIC X.                                          00002180
           02 MWFONCP   PIC X.                                          00002190
           02 MWFONCH   PIC X.                                          00002200
           02 MWFONCV   PIC X.                                          00002210
           02 MWFONCO   PIC X(3).                                       00002220
      * CODIC                                                           00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MNCODICA  PIC X.                                          00002250
           02 MNCODICC  PIC X.                                          00002260
           02 MNCODICP  PIC X.                                          00002270
           02 MNCODICH  PIC X.                                          00002280
           02 MNCODICV  PIC X.                                          00002290
           02 MNCODICO  PIC X(7).                                       00002300
      * REFERENCE FOURN                                                 00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MLREFA    PIC X.                                          00002330
           02 MLREFC    PIC X.                                          00002340
           02 MLREFP    PIC X.                                          00002350
           02 MLREFH    PIC X.                                          00002360
           02 MLREFV    PIC X.                                          00002370
           02 MLREFO    PIC X(20).                                      00002380
      * DATE CREATION CODIC                                             00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MDCRECCA  PIC X.                                          00002410
           02 MDCRECCC  PIC X.                                          00002420
           02 MDCRECCP  PIC X.                                          00002430
           02 MDCRECCH  PIC X.                                          00002440
           02 MDCRECCV  PIC X.                                          00002450
           02 MDCRECCO  PIC X(8).                                       00002460
      * CODE FAMILLE                                                    00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MNFAMA    PIC X.                                          00002490
           02 MNFAMC    PIC X.                                          00002500
           02 MNFAMP    PIC X.                                          00002510
           02 MNFAMH    PIC X.                                          00002520
           02 MNFAMV    PIC X.                                          00002530
           02 MNFAMO    PIC X(5).                                       00002540
      * LIBELLE FAMILLE                                                 00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MLFAMA    PIC X.                                          00002570
           02 MLFAMC    PIC X.                                          00002580
           02 MLFAMP    PIC X.                                          00002590
           02 MLFAMH    PIC X.                                          00002600
           02 MLFAMV    PIC X.                                          00002610
           02 MLFAMO    PIC X(20).                                      00002620
      * DATE MAJ CODIC                                                  00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MDMAJCCA  PIC X.                                          00002650
           02 MDMAJCCC  PIC X.                                          00002660
           02 MDMAJCCP  PIC X.                                          00002670
           02 MDMAJCCH  PIC X.                                          00002680
           02 MDMAJCCV  PIC X.                                          00002690
           02 MDMAJCCO  PIC X(8).                                       00002700
      * CODE MARQUE                                                     00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MNMARQA   PIC X.                                          00002730
           02 MNMARQC   PIC X.                                          00002740
           02 MNMARQP   PIC X.                                          00002750
           02 MNMARQH   PIC X.                                          00002760
           02 MNMARQV   PIC X.                                          00002770
           02 MNMARQO   PIC X(5).                                       00002780
      * LIBELLE MARQUE                                                  00002790
           02 FILLER    PIC X(2).                                       00002800
           02 MLMARQA   PIC X.                                          00002810
           02 MLMARQC   PIC X.                                          00002820
           02 MLMARQP   PIC X.                                          00002830
           02 MLMARQH   PIC X.                                          00002840
           02 MLMARQV   PIC X.                                          00002850
           02 MLMARQO   PIC X(20).                                      00002860
      * DATE 1ERE RECEPTION                                             00002870
           02 FILLER    PIC X(2).                                       00002880
           02 MDRECCCA  PIC X.                                          00002890
           02 MDRECCCC  PIC X.                                          00002900
           02 MDRECCCP  PIC X.                                          00002910
           02 MDRECCCH  PIC X.                                          00002920
           02 MDRECCCV  PIC X.                                          00002930
           02 MDRECCCO  PIC X(8).                                       00002940
           02 FILLER    PIC X(2).                                       00002950
           02 MCCOLORMA      PIC X.                                     00002960
           02 MCCOLORMC PIC X.                                          00002970
           02 MCCOLORMP PIC X.                                          00002980
           02 MCCOLORMH PIC X.                                          00002990
           02 MCCOLORMV PIC X.                                          00003000
           02 MCCOLORMO      PIC X(5).                                  00003010
           02 FILLER    PIC X(2).                                       00003020
           02 MLCOLORMA      PIC X.                                     00003030
           02 MLCOLORMC PIC X.                                          00003040
           02 MLCOLORMP PIC X.                                          00003050
           02 MLCOLORMH PIC X.                                          00003060
           02 MLCOLORMV PIC X.                                          00003070
           02 MLCOLORMO      PIC X(20).                                 00003080
           02 FILLER    PIC X(2).                                       00003090
           02 MWGVTEA   PIC X.                                          00003100
           02 MWGVTEC   PIC X.                                          00003110
           02 MWGVTEP   PIC X.                                          00003120
           02 MWGVTEH   PIC X.                                          00003130
           02 MWGVTEV   PIC X.                                          00003140
           02 MWGVTEO   PIC X(3).                                       00003150
      * CODE ASSORTIMENT                                                00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MCASSORA  PIC X.                                          00003180
           02 MCASSORC  PIC X.                                          00003190
           02 MCASSORP  PIC X.                                          00003200
           02 MCASSORH  PIC X.                                          00003210
           02 MCASSORV  PIC X.                                          00003220
           02 MCASSORO  PIC X(5).                                       00003230
           02 FILLER    PIC X(2).                                       00003240
           02 MLASSORA  PIC X.                                          00003250
           02 MLASSORC  PIC X.                                          00003260
           02 MLASSORP  PIC X.                                          00003270
           02 MLASSORH  PIC X.                                          00003280
           02 MLASSORV  PIC X.                                          00003290
           02 MLASSORO  PIC X(20).                                      00003300
      * FLAG APPART DACEM                                               00003310
           02 FILLER    PIC X(2).                                       00003320
           02 MWDACEMA  PIC X.                                          00003330
           02 MWDACEMC  PIC X.                                          00003340
           02 MWDACEMP  PIC X.                                          00003350
           02 MWDACEMH  PIC X.                                          00003360
           02 MWDACEMV  PIC X.                                          00003370
           02 MWDACEMO  PIC X.                                          00003380
      * CODE TVA                                                        00003390
           02 FILLER    PIC X(2).                                       00003400
           02 MCTVAA    PIC X.                                          00003410
           02 MCTVAC    PIC X.                                          00003420
           02 MCTVAP    PIC X.                                          00003430
           02 MCTVAH    PIC X.                                          00003440
           02 MCTVAV    PIC X.                                          00003450
           02 MCTVAO    PIC X(5).                                       00003460
           02 FILLER    PIC X(2).                                       00003470
           02 MLTVAA    PIC X.                                          00003480
           02 MLTVAC    PIC X.                                          00003490
           02 MLTVAP    PIC X.                                          00003500
           02 MLTVAH    PIC X.                                          00003510
           02 MLTVAV    PIC X.                                          00003520
           02 MLTVAO    PIC X(20).                                      00003530
      * FLAG TLM OU ELA                                                 00003540
           02 FILLER    PIC X(2).                                       00003550
           02 MWTMELAA  PIC X.                                          00003560
           02 MWTMELAC  PIC X.                                          00003570
           02 MWTMELAP  PIC X.                                          00003580
           02 MWTMELAH  PIC X.                                          00003590
           02 MWTMELAV  PIC X.                                          00003600
           02 MWTMELAO  PIC X.                                          00003610
      * CODE GARANTIE                                                   00003620
           02 FILLER    PIC X(2).                                       00003630
           02 MCGARANA  PIC X.                                          00003640
           02 MCGARANC  PIC X.                                          00003650
           02 MCGARANP  PIC X.                                          00003660
           02 MCGARANH  PIC X.                                          00003670
           02 MCGARANV  PIC X.                                          00003680
           02 MCGARANO  PIC X(5).                                       00003690
           02 FILLER    PIC X(2).                                       00003700
           02 MLGARANA  PIC X.                                          00003710
           02 MLGARANC  PIC X.                                          00003720
           02 MLGARANP  PIC X.                                          00003730
           02 MLGARANH  PIC X.                                          00003740
           02 MLGARANV  PIC X.                                          00003750
           02 MLGARANO  PIC X(20).                                      00003760
           02 FILLER    PIC X(2).                                       00003770
           02 MCGCONSA  PIC X.                                          00003780
           02 MCGCONSC  PIC X.                                          00003790
           02 MCGCONSP  PIC X.                                          00003800
           02 MCGCONSH  PIC X.                                          00003810
           02 MCGCONSV  PIC X.                                          00003820
           02 MCGCONSO  PIC X(5).                                       00003830
           02 FILLER    PIC X(2).                                       00003840
           02 MCGMGDA   PIC X.                                          00003850
           02 MCGMGDC   PIC X.                                          00003860
           02 MCGMGDP   PIC X.                                          00003870
           02 MCGMGDH   PIC X.                                          00003880
           02 MCGMGDV   PIC X.                                          00003890
           02 MCGMGDO   PIC X(5).                                       00003900
           02 FILLER    PIC X(2).                                       00003910
           02 MLCGMGDA  PIC X.                                          00003920
           02 MLCGMGDC  PIC X.                                          00003930
           02 MLCGMGDP  PIC X.                                          00003940
           02 MLCGMGDH  PIC X.                                          00003950
           02 MLCGMGDV  PIC X.                                          00003960
           02 MLCGMGDO  PIC X(20).                                      00003970
           02 FILLER    PIC X(2).                                       00003980
           02 MLGCONSA  PIC X.                                          00003990
           02 MLGCONSC  PIC X.                                          00004000
           02 MLGCONSP  PIC X.                                          00004010
           02 MLGCONSH  PIC X.                                          00004020
           02 MLGCONSV  PIC X.                                          00004030
           02 MLGCONSO  PIC X(20).                                      00004040
           02 FILLER    PIC X(2).                                       00004050
           02 MEANA     PIC X.                                          00004060
           02 MEANC     PIC X.                                          00004070
           02 MEANP     PIC X.                                          00004080
           02 MEANH     PIC X.                                          00004090
           02 MEANV     PIC X.                                          00004100
           02 MEANO     PIC X(13).                                      00004110
           02 FILLER    PIC X(2).                                       00004120
           02 MREFTECHA      PIC X.                                     00004130
           02 MREFTECHC PIC X.                                          00004140
           02 MREFTECHP PIC X.                                          00004150
           02 MREFTECHH PIC X.                                          00004160
           02 MREFTECHV PIC X.                                          00004170
           02 MREFTECHO      PIC X(40).                                 00004180
           02 MWTABLEO OCCURS   5 TIMES .                               00004190
      * CODE TYPE DCL                                                   00004200
             03 FILLER       PIC X(2).                                  00004210
             03 MCTYDCLA     PIC X.                                     00004220
             03 MCTYDCLC     PIC X.                                     00004230
             03 MCTYDCLP     PIC X.                                     00004240
             03 MCTYDCLH     PIC X.                                     00004250
             03 MCTYDCLV     PIC X.                                     00004260
             03 MCTYDCLO     PIC X(5).                                  00004270
      * LIBELLE TYPE DCL                                                00004280
             03 FILLER       PIC X(2).                                  00004290
             03 MLTYDCLA     PIC X.                                     00004300
             03 MLTYDCLC     PIC X.                                     00004310
             03 MLTYDCLP     PIC X.                                     00004320
             03 MLTYDCLH     PIC X.                                     00004330
             03 MLTYDCLV     PIC X.                                     00004340
             03 MLTYDCLO     PIC X(20).                                 00004350
      * CODE GARANTIE CPLT                                              00004360
             03 FILLER       PIC X(2).                                  00004370
             03 MCGCPLTA     PIC X.                                     00004380
             03 MCGCPLTC     PIC X.                                     00004390
             03 MCGCPLTP     PIC X.                                     00004400
             03 MCGCPLTH     PIC X.                                     00004410
             03 MCGCPLTV     PIC X.                                     00004420
             03 MCGCPLTO     PIC X(5).                                  00004430
      * LIBELLE G. CPLT                                                 00004440
             03 FILLER       PIC X(2).                                  00004450
             03 MLGCPLTA     PIC X.                                     00004460
             03 MLGCPLTC     PIC X.                                     00004470
             03 MLGCPLTP     PIC X.                                     00004480
             03 MLGCPLTH     PIC X.                                     00004490
             03 MLGCPLTV     PIC X.                                     00004500
             03 MLGCPLTO     PIC X(20).                                 00004510
             03 FILLER       PIC X(2).                                  00004520
             03 MCVGCPLA     PIC X.                                     00004530
             03 MCVGCPLC     PIC X.                                     00004540
             03 MCVGCPLP     PIC X.                                     00004550
             03 MCVGCPLH     PIC X.                                     00004560
             03 MCVGCPLV     PIC X.                                     00004570
             03 MCVGCPLO     PIC X(3).                                  00004580
      * ZONE CMD AIDA                                                   00004590
           02 FILLER    PIC X(2).                                       00004600
           02 MZONCMDA  PIC X.                                          00004610
           02 MZONCMDC  PIC X.                                          00004620
           02 MZONCMDP  PIC X.                                          00004630
           02 MZONCMDH  PIC X.                                          00004640
           02 MZONCMDV  PIC X.                                          00004650
           02 MZONCMDO  PIC X(15).                                      00004660
      * MESSAGE ERREUR                                                  00004670
           02 FILLER    PIC X(2).                                       00004680
           02 MLIBERRA  PIC X.                                          00004690
           02 MLIBERRC  PIC X.                                          00004700
           02 MLIBERRP  PIC X.                                          00004710
           02 MLIBERRH  PIC X.                                          00004720
           02 MLIBERRV  PIC X.                                          00004730
           02 MLIBERRO  PIC X(58).                                      00004740
      * CODE TRANSACTION                                                00004750
           02 FILLER    PIC X(2).                                       00004760
           02 MCODTRAA  PIC X.                                          00004770
           02 MCODTRAC  PIC X.                                          00004780
           02 MCODTRAP  PIC X.                                          00004790
           02 MCODTRAH  PIC X.                                          00004800
           02 MCODTRAV  PIC X.                                          00004810
           02 MCODTRAO  PIC X(4).                                       00004820
      * CICS DE TRAVAIL                                                 00004830
           02 FILLER    PIC X(2).                                       00004840
           02 MCICSA    PIC X.                                          00004850
           02 MCICSC    PIC X.                                          00004860
           02 MCICSP    PIC X.                                          00004870
           02 MCICSH    PIC X.                                          00004880
           02 MCICSV    PIC X.                                          00004890
           02 MCICSO    PIC X(5).                                       00004900
      * NETNAME                                                         00004910
           02 FILLER    PIC X(2).                                       00004920
           02 MNETNAMA  PIC X.                                          00004930
           02 MNETNAMC  PIC X.                                          00004940
           02 MNETNAMP  PIC X.                                          00004950
           02 MNETNAMH  PIC X.                                          00004960
           02 MNETNAMV  PIC X.                                          00004970
           02 MNETNAMO  PIC X(8).                                       00004980
      * CODE TERMINAL                                                   00004990
           02 FILLER    PIC X(2).                                       00005000
           02 MSCREENA  PIC X.                                          00005010
           02 MSCREENC  PIC X.                                          00005020
           02 MSCREENP  PIC X.                                          00005030
           02 MSCREENH  PIC X.                                          00005040
           02 MSCREENV  PIC X.                                          00005050
           02 MSCREENO  PIC X(5).                                       00005060
                                                                                
