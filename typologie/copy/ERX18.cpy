      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERX18   ERX18                                              00000020
      ***************************************************************** 00000030
       01   ERX18I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTPAGEL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MTOTPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTOTPAGEF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MTOTPAGEI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MWFONCI   PIC X(4).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNSOCI    PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPROFL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNPROFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPROFF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNPROFI   PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPROFL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLPROFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLPROFF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLPROFI   PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNMAGI    PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMAGL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLMAGF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLMAGI    PIC X(20).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPPROFL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MTYPPROFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPPROFF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MTYPPROFI      PIC X(7).                                  00000490
           02 MDETAILCODI OCCURS   12 TIMES .                           00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNCODICI     PIC X(7).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNFAML  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MNFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNFAMF  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNFAMI  PIC X(5).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMARQL      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MNMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNMARQF      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNMARQI      PIC X(5).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNREFL  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MNREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNREFF  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MNREFI  PIC X(20).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLIBERRI  PIC X(78).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCODTRAI  PIC X(4).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCICSI    PIC X(5).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MNETNAMI  PIC X(8).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MSCREENI  PIC X(4).                                       00000860
      ***************************************************************** 00000870
      * SDF: ERX18   ERX18                                              00000880
      ***************************************************************** 00000890
       01   ERX18O REDEFINES ERX18I.                                    00000900
           02 FILLER    PIC X(12).                                      00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MDATJOUA  PIC X.                                          00000930
           02 MDATJOUC  PIC X.                                          00000940
           02 MDATJOUP  PIC X.                                          00000950
           02 MDATJOUH  PIC X.                                          00000960
           02 MDATJOUV  PIC X.                                          00000970
           02 MDATJOUO  PIC X(10).                                      00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MTIMJOUA  PIC X.                                          00001000
           02 MTIMJOUC  PIC X.                                          00001010
           02 MTIMJOUP  PIC X.                                          00001020
           02 MTIMJOUH  PIC X.                                          00001030
           02 MTIMJOUV  PIC X.                                          00001040
           02 MTIMJOUO  PIC X(5).                                       00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MNPAGEA   PIC X.                                          00001070
           02 MNPAGEC   PIC X.                                          00001080
           02 MNPAGEP   PIC X.                                          00001090
           02 MNPAGEH   PIC X.                                          00001100
           02 MNPAGEV   PIC X.                                          00001110
           02 MNPAGEO   PIC X(2).                                       00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MTOTPAGEA      PIC X.                                     00001140
           02 MTOTPAGEC PIC X.                                          00001150
           02 MTOTPAGEP PIC X.                                          00001160
           02 MTOTPAGEH PIC X.                                          00001170
           02 MTOTPAGEV PIC X.                                          00001180
           02 MTOTPAGEO      PIC X(2).                                  00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MWFONCA   PIC X.                                          00001210
           02 MWFONCC   PIC X.                                          00001220
           02 MWFONCP   PIC X.                                          00001230
           02 MWFONCH   PIC X.                                          00001240
           02 MWFONCV   PIC X.                                          00001250
           02 MWFONCO   PIC X(4).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MNSOCA    PIC X.                                          00001280
           02 MNSOCC    PIC X.                                          00001290
           02 MNSOCP    PIC X.                                          00001300
           02 MNSOCH    PIC X.                                          00001310
           02 MNSOCV    PIC X.                                          00001320
           02 MNSOCO    PIC X(3).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MNPROFA   PIC X.                                          00001350
           02 MNPROFC   PIC X.                                          00001360
           02 MNPROFP   PIC X.                                          00001370
           02 MNPROFH   PIC X.                                          00001380
           02 MNPROFV   PIC X.                                          00001390
           02 MNPROFO   PIC X(5).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MLPROFA   PIC X.                                          00001420
           02 MLPROFC   PIC X.                                          00001430
           02 MLPROFP   PIC X.                                          00001440
           02 MLPROFH   PIC X.                                          00001450
           02 MLPROFV   PIC X.                                          00001460
           02 MLPROFO   PIC X(20).                                      00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MNMAGA    PIC X.                                          00001490
           02 MNMAGC    PIC X.                                          00001500
           02 MNMAGP    PIC X.                                          00001510
           02 MNMAGH    PIC X.                                          00001520
           02 MNMAGV    PIC X.                                          00001530
           02 MNMAGO    PIC X(3).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MLMAGA    PIC X.                                          00001560
           02 MLMAGC    PIC X.                                          00001570
           02 MLMAGP    PIC X.                                          00001580
           02 MLMAGH    PIC X.                                          00001590
           02 MLMAGV    PIC X.                                          00001600
           02 MLMAGO    PIC X(20).                                      00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MTYPPROFA      PIC X.                                     00001630
           02 MTYPPROFC PIC X.                                          00001640
           02 MTYPPROFP PIC X.                                          00001650
           02 MTYPPROFH PIC X.                                          00001660
           02 MTYPPROFV PIC X.                                          00001670
           02 MTYPPROFO      PIC X(7).                                  00001680
           02 MDETAILCODO OCCURS   12 TIMES .                           00001690
             03 FILLER       PIC X(2).                                  00001700
             03 MNCODICA     PIC X.                                     00001710
             03 MNCODICC     PIC X.                                     00001720
             03 MNCODICP     PIC X.                                     00001730
             03 MNCODICH     PIC X.                                     00001740
             03 MNCODICV     PIC X.                                     00001750
             03 MNCODICO     PIC X(7).                                  00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MNFAMA  PIC X.                                          00001780
             03 MNFAMC  PIC X.                                          00001790
             03 MNFAMP  PIC X.                                          00001800
             03 MNFAMH  PIC X.                                          00001810
             03 MNFAMV  PIC X.                                          00001820
             03 MNFAMO  PIC X(5).                                       00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MNMARQA      PIC X.                                     00001850
             03 MNMARQC PIC X.                                          00001860
             03 MNMARQP PIC X.                                          00001870
             03 MNMARQH PIC X.                                          00001880
             03 MNMARQV PIC X.                                          00001890
             03 MNMARQO      PIC X(5).                                  00001900
             03 FILLER       PIC X(2).                                  00001910
             03 MNREFA  PIC X.                                          00001920
             03 MNREFC  PIC X.                                          00001930
             03 MNREFP  PIC X.                                          00001940
             03 MNREFH  PIC X.                                          00001950
             03 MNREFV  PIC X.                                          00001960
             03 MNREFO  PIC X(20).                                      00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MLIBERRA  PIC X.                                          00001990
           02 MLIBERRC  PIC X.                                          00002000
           02 MLIBERRP  PIC X.                                          00002010
           02 MLIBERRH  PIC X.                                          00002020
           02 MLIBERRV  PIC X.                                          00002030
           02 MLIBERRO  PIC X(78).                                      00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCODTRAA  PIC X.                                          00002060
           02 MCODTRAC  PIC X.                                          00002070
           02 MCODTRAP  PIC X.                                          00002080
           02 MCODTRAH  PIC X.                                          00002090
           02 MCODTRAV  PIC X.                                          00002100
           02 MCODTRAO  PIC X(4).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MCICSA    PIC X.                                          00002130
           02 MCICSC    PIC X.                                          00002140
           02 MCICSP    PIC X.                                          00002150
           02 MCICSH    PIC X.                                          00002160
           02 MCICSV    PIC X.                                          00002170
           02 MCICSO    PIC X(5).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MNETNAMA  PIC X.                                          00002200
           02 MNETNAMC  PIC X.                                          00002210
           02 MNETNAMP  PIC X.                                          00002220
           02 MNETNAMH  PIC X.                                          00002230
           02 MNETNAMV  PIC X.                                          00002240
           02 MNETNAMO  PIC X(8).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MSCREENA  PIC X.                                          00002270
           02 MSCREENC  PIC X.                                          00002280
           02 MSCREENP  PIC X.                                          00002290
           02 MSCREENH  PIC X.                                          00002300
           02 MSCREENV  PIC X.                                          00002310
           02 MSCREENO  PIC X(4).                                       00002320
                                                                                
