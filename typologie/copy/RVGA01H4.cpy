      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE VANLI LIEUX DE VALORISATIO             *        
      *----------------------------------------------------------------*        
       01  RVGA01H4.                                                            
           05  VANLI-CTABLEG2    PIC X(15).                                     
           05  VANLI-CTABLEG2-REDEF REDEFINES VANLI-CTABLEG2.                   
               10  VANLI-SOCLIE          PIC X(06).                             
               10  VANLI-NSEQ            PIC X(03).                             
               10  VANLI-NSEQ-N         REDEFINES VANLI-NSEQ                    
                                         PIC 9(03).                             
           05  VANLI-WTABLEG     PIC X(80).                                     
           05  VANLI-WTABLEG-REDEF  REDEFINES VANLI-WTABLEG.                    
               10  VANLI-NSOCLI          PIC X(06).                             
               10  VANLI-NSSLIEU         PIC X(03).                             
               10  VANLI-CLIEUTRT        PIC X(05).                             
               10  VANLI-DEFAUT          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01H4-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  VANLI-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  VANLI-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  VANLI-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  VANLI-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
