      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVTH1400                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTH1499.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTH1499.                                                            
      *}                                                                        
      *    *************************************************************        
      *                       NCODIC                                            
           10 TH14-NCODIC          PIC X(7).                                    
      *    *************************************************************        
      *                       COPCO                                             
           10 TH14-COPCO           PIC X(3).                                    
      *    *************************************************************        
      *                       DJOUR                                             
           10 TH14-DEFFET          PIC X(8).                                    
      *    *************************************************************        
      *                       PTC                                               
           10 TH14-PTC             PIC S9(5)V9(2) USAGE COMP-3.                 
      *    *************************************************************        
      *                       PCF                                               
           10 TH14-PCF             PIC S9(5)V9(2) USAGE COMP-3.                 
      *    *************************************************************        
      *                       PCF CALCULE                                       
           10 TH14-PCFCALC         PIC S9(5)V9(2) USAGE COMP-3.                 
      *    *************************************************************        
      *                       FORCE                                             
           10 TH14-FORCE           PIC X(1).                                    
      *    *************************************************************        
      *                       C1                                                
           10 TH14-C1              PIC S9(3)V9(2) USAGE COMP-3.                 
      *    *************************************************************        
      *                       C2                                                
           10 TH14-C2              PIC S9(3)V9(2) USAGE COMP-3.                 
      *    *************************************************************        
      *                       C3                                                
           10 TH14-C3              PIC S9(3)V9(2) USAGE COMP-3.                 
      *    *************************************************************        
      *                       C4                                                
           10 TH14-C4              PIC S9(3)V9(2) USAGE COMP-3.                 
      *    *************************************************************        
      *                       C5                                                
           10 TH14-C5              PIC S9(3)V9(2) USAGE COMP-3.                 
      *    *************************************************************        
      *                       C6                                                
           10 TH14-C6              PIC S9(3)V9(2) USAGE COMP-3.                 
      *    *************************************************************        
      *                       C7                                                
           10 TH14-C7              PIC S9(3)V9(2) USAGE COMP-3.                 
      *    *************************************************************        
      *                       C8                                                
           10 TH14-C8              PIC S9(3)V9(2) USAGE COMP-3.                 
      *    *************************************************************        
      *                       C9                                                
           10 TH14-C9              PIC S9(3)V9(2) USAGE COMP-3.                 
      *    *************************************************************        
      *                       C10                                               
           10 TH14-C10             PIC S9(3)V9(2) USAGE COMP-3.                 
      *    *************************************************************        
      *                       C11                                               
           10 TH14-C11             PIC S9(3)V9(2) USAGE COMP-3.                 
      *    *************************************************************        
      *                       C12                                               
           10 TH14-C12             PIC S9(3)V9(2) USAGE COMP-3.                 
      *    *************************************************************        
      *                       FLAGTAXE                                          
           10 TH14-FLAGTAXE        PIC X(1).                                    
      *    *************************************************************        
      *                       MNTTAXE                                           
           10 TH14-MNTTAXE         PIC S9(7)V9(2) USAGE COMP-3.                 
      *    *************************************************************        
      *                       DCRESYST                                          
           10 TH14-DCRESYST        PIC X(26).                                   
      *    *************************************************************        
      *                       DMAJSYST                                          
           10 TH14-DMAJSYST        PIC X(26).                                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      *   LISTE DES FLAGS DE LA TABLE RVTH1400                                  
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTH1499-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTH1499-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH14-NCODIC-F            PIC S9(4) COMP.                         
      *--                                                                       
           10  TH14-NCODIC-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH14-COPCO-F             PIC S9(4) COMP.                         
      *--                                                                       
           10  TH14-COPCO-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH14-DEFFET-F            PIC S9(4) COMP.                         
      *--                                                                       
           10  TH14-DEFFET-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH14-PTC-F               PIC S9(4) COMP.                         
      *--                                                                       
           10  TH14-PTC-F               PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH14-PCF-F               PIC S9(4) COMP.                         
      *--                                                                       
           10  TH14-PCF-F               PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH14-PCFCALC-F           PIC S9(4) COMP.                         
      *--                                                                       
           10  TH14-PCFCALC-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH14-FORCE-F             PIC S9(4) COMP.                         
      *--                                                                       
           10  TH14-FORCE-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH14-C1-F                PIC S9(4) COMP.                         
      *--                                                                       
           10  TH14-C1-F                PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH14-C2-F                PIC S9(4) COMP.                         
      *--                                                                       
           10  TH14-C2-F                PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH14-C3-F                PIC S9(4) COMP.                         
      *--                                                                       
           10  TH14-C3-F                PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH14-C4-F                PIC S9(4) COMP.                         
      *--                                                                       
           10  TH14-C4-F                PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH14-C5-F                PIC S9(4) COMP.                         
      *--                                                                       
           10  TH14-C5-F                PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH14-C6-F                PIC S9(4) COMP.                         
      *--                                                                       
           10  TH14-C6-F                PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH14-C7-F                PIC S9(4) COMP.                         
      *--                                                                       
           10  TH14-C7-F                PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH14-C8-F                PIC S9(4) COMP.                         
      *--                                                                       
           10  TH14-C8-F                PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH14-C9-F                PIC S9(4) COMP.                         
      *--                                                                       
           10  TH14-C9-F                PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH14-C10-F               PIC S9(4) COMP.                         
      *--                                                                       
           10  TH14-C10-F               PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH14-C11-F               PIC S9(4) COMP.                         
      *--                                                                       
           10  TH14-C11-F               PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH14-C11-F               PIC S9(4) COMP.                         
      *--                                                                       
           10  TH14-C11-F               PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH14-C12-F               PIC S9(4) COMP.                         
      *--                                                                       
           10  TH14-C12-F               PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH14-FLAGTAXE-F          PIC S9(4) COMP.                         
      *--                                                                       
           10  TH14-FLAGTAXE-F          PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH14-MNTTAXE-F           PIC S9(4) COMP.                         
      *--                                                                       
           10  TH14-MNTTAXE-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH14-DCRESYST-F          PIC S9(4) COMP.                         
      *--                                                                       
           10  TH14-DCRESYST-F          PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH14-DMAJSYST-F          PIC S9(4) COMP.                         
      *--                                                                       
           10  TH14-DMAJSYST-F          PIC S9(4) COMP-5.                       
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 13      *        
      ******************************************************************        
                                                                                
