      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGES MQ INTERROGATION STATUT VENTE : REPONSE DU LOCAL        
      *   TAILLE : 134                                                          
      *   12/00 : CONTROLE DES PRODUITS DESTOCKES                               
      *****************************************************************         
      * VERSION POUR MBS49                                                      
      *                                                                         
           10  WS-MESSAGE-RECU REDEFINES COMM-MQ12-MESSAGE.                     
      *---                                                                      
           17  MESR-ENTETE.                                                     
               20   MESR-TYPE     PIC    X(3).                                  
               20   MESR-NSOCMSG  PIC    X(3).                                  
               20   MESR-NLIEUMSG PIC    X(3).                                  
               20   MESR-NSOCDST  PIC    X(3).                                  
               20   MESR-NLIEUDST PIC    X(3).                                  
               20   MESR-NORD     PIC    9(8).                                  
               20   MESR-LPROG    PIC    X(10).                                 
               20   MESR-DJOUR    PIC    X(8).                                  
               20   MESR-WSID     PIC    X(10).                                 
               20   MESR-USER     PIC    X(10).                                 
               20   MESR-CHRONO   PIC    9(7).                                  
               20   MESR-NBRMSG   PIC    9(7).                                  
               20   MESR-FILLER   PIC    X(30).                                 
           17  MESR-STATUT.                                                     
               20   MESR-NORDRE    PIC    X(8).                                 
               20   MESR-WUTIL     PIC    X.                                    
               20   MESR-NTERMINAL PIC    X(10).                                
               20   MESR-NTRANS    PIC    S9(08).                               
12/00          20   MESR-NBDESTO   PIC    S9(08).                               
                                                                                
