      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE LIBCR LIBELLE CRITERES POUR 1 ECS      *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01EH.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01EH.                                                            
      *}                                                                        
           05  LIBCR-CTABLEG2    PIC X(15).                                     
           05  LIBCR-CTABLEG2-REDEF REDEFINES LIBCR-CTABLEG2.                   
               10  LIBCR-ECS             PIC X(05).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  LIBCR-ECS-N          REDEFINES LIBCR-ECS                     
                                         PIC 9(05).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  LIBCR-WTABLEG     PIC X(80).                                     
           05  LIBCR-WTABLEG-REDEF  REDEFINES LIBCR-WTABLEG.                    
               10  LIBCR-LIB1            PIC X(20).                             
               10  LIBCR-LIB2            PIC X(20).                             
               10  LIBCR-LIB3            PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01EH-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01EH-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LIBCR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  LIBCR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LIBCR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  LIBCR-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
