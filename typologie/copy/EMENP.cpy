      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Menus NCG+ Define Profiles                                      00000020
      ***************************************************************** 00000030
       01   EMENPI.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
           02 MLIGNEI OCCURS   15 TIMES .                               00000140
             03 MXD OCCURS   4 TIMES .                                  00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MXL   COMP PIC S9(4).                                 00000160
      *--                                                                       
               04 MXL COMP-5 PIC S9(4).                                         
      *}                                                                        
               04 MXF   PIC X.                                          00000170
               04 FILLER     PIC X(4).                                  00000180
               04 MXI   PIC X.                                          00000190
             03 MZUSERD OCCURS   4 TIMES .                              00000200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MZUSERL    COMP PIC S9(4).                            00000210
      *--                                                                       
               04 MZUSERL COMP-5 PIC S9(4).                                     
      *}                                                                        
               04 MZUSERF    PIC X.                                     00000220
               04 FILLER     PIC X(4).                                  00000230
               04 MZUSERI    PIC X(7).                                  00000240
             03 MNEWND OCCURS   4 TIMES .                               00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MNEWNL     COMP PIC S9(4).                            00000260
      *--                                                                       
               04 MNEWNL COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MNEWNF     PIC X.                                     00000270
               04 FILLER     PIC X(4).                                  00000280
               04 MNEWNI     PIC X(7).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLIBERRI  PIC X(79).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCODTRAI  PIC X(4).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MZONCMDI  PIC X(15).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCICSI    PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNETNAMI  PIC X(8).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MSCREENI  PIC X(4).                                       00000530
      ***************************************************************** 00000540
      * Menus NCG+ Define Profiles                                      00000550
      ***************************************************************** 00000560
       01   EMENPO REDEFINES EMENPI.                                    00000570
           02 FILLER    PIC X(12).                                      00000580
           02 FILLER    PIC X(2).                                       00000590
           02 MDATJOUA  PIC X.                                          00000600
           02 MDATJOUC  PIC X.                                          00000610
           02 MDATJOUP  PIC X.                                          00000620
           02 MDATJOUH  PIC X.                                          00000630
           02 MDATJOUV  PIC X.                                          00000640
           02 MDATJOUO  PIC X(10).                                      00000650
           02 FILLER    PIC X(2).                                       00000660
           02 MTIMJOUA  PIC X.                                          00000670
           02 MTIMJOUC  PIC X.                                          00000680
           02 MTIMJOUP  PIC X.                                          00000690
           02 MTIMJOUH  PIC X.                                          00000700
           02 MTIMJOUV  PIC X.                                          00000710
           02 MTIMJOUO  PIC X(5).                                       00000720
           02 MLIGNEO OCCURS   15 TIMES .                               00000730
             03 DFHMS1 OCCURS   4 TIMES .                               00000740
               04 FILLER     PIC X(2).                                  00000750
               04 MXA   PIC X.                                          00000760
               04 MXC   PIC X.                                          00000770
               04 MXP   PIC X.                                          00000780
               04 MXH   PIC X.                                          00000790
               04 MXV   PIC X.                                          00000800
               04 MXO   PIC X.                                          00000810
             03 DFHMS2 OCCURS   4 TIMES .                               00000820
               04 FILLER     PIC X(2).                                  00000830
               04 MZUSERA    PIC X.                                     00000840
               04 MZUSERC    PIC X.                                     00000850
               04 MZUSERP    PIC X.                                     00000860
               04 MZUSERH    PIC X.                                     00000870
               04 MZUSERV    PIC X.                                     00000880
               04 MZUSERO    PIC X(7).                                  00000890
             03 DFHMS3 OCCURS   4 TIMES .                               00000900
               04 FILLER     PIC X(2).                                  00000910
               04 MNEWNA     PIC X.                                     00000920
               04 MNEWNC     PIC X.                                     00000930
               04 MNEWNP     PIC X.                                     00000940
               04 MNEWNH     PIC X.                                     00000950
               04 MNEWNV     PIC X.                                     00000960
               04 MNEWNO     PIC X(7).                                  00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MLIBERRA  PIC X.                                          00000990
           02 MLIBERRC  PIC X.                                          00001000
           02 MLIBERRP  PIC X.                                          00001010
           02 MLIBERRH  PIC X.                                          00001020
           02 MLIBERRV  PIC X.                                          00001030
           02 MLIBERRO  PIC X(79).                                      00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MCODTRAA  PIC X.                                          00001060
           02 MCODTRAC  PIC X.                                          00001070
           02 MCODTRAP  PIC X.                                          00001080
           02 MCODTRAH  PIC X.                                          00001090
           02 MCODTRAV  PIC X.                                          00001100
           02 MCODTRAO  PIC X(4).                                       00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MZONCMDA  PIC X.                                          00001130
           02 MZONCMDC  PIC X.                                          00001140
           02 MZONCMDP  PIC X.                                          00001150
           02 MZONCMDH  PIC X.                                          00001160
           02 MZONCMDV  PIC X.                                          00001170
           02 MZONCMDO  PIC X(15).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MCICSA    PIC X.                                          00001200
           02 MCICSC    PIC X.                                          00001210
           02 MCICSP    PIC X.                                          00001220
           02 MCICSH    PIC X.                                          00001230
           02 MCICSV    PIC X.                                          00001240
           02 MCICSO    PIC X(5).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MNETNAMA  PIC X.                                          00001270
           02 MNETNAMC  PIC X.                                          00001280
           02 MNETNAMP  PIC X.                                          00001290
           02 MNETNAMH  PIC X.                                          00001300
           02 MNETNAMV  PIC X.                                          00001310
           02 MNETNAMO  PIC X(8).                                       00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MSCREENA  PIC X.                                          00001340
           02 MSCREENC  PIC X.                                          00001350
           02 MSCREENP  PIC X.                                          00001360
           02 MSCREENH  PIC X.                                          00001370
           02 MSCREENV  PIC X.                                          00001380
           02 MSCREENO  PIC X(4).                                       00001390
                                                                                
