      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FIFMG FRAIS MAGASINAGE EXCEPTIONELS    *        
      *----------------------------------------------------------------*        
       01  RVGA01TQ.                                                            
           05  FIFMG-CTABLEG2    PIC X(15).                                     
           05  FIFMG-CTABLEG2-REDEF REDEFINES FIFMG-CTABLEG2.                   
               10  FIFMG-NSOC            PIC X(03).                             
               10  FIFMG-NLIEU           PIC X(03).                             
               10  FIFMG-CFAM            PIC X(05).                             
           05  FIFMG-WTABLEG     PIC X(80).                                     
           05  FIFMG-WTABLEG-REDEF  REDEFINES FIFMG-WTABLEG.                    
               10  FIFMG-WACTIF          PIC X(01).                             
               10  FIFMG-VALEUR          PIC X(06).                             
               10  FIFMG-VALEUR-N       REDEFINES FIFMG-VALEUR                  
                                         PIC 9(04)V9(02).                       
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01TQ-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FIFMG-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FIFMG-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FIFMG-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FIFMG-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
