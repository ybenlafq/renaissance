      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      *             RELEVE DE PRIX                                   *  00030000
      *       CONSULTATION AU CODIC                                   * 00040001
      ****************************************************************  00050000
       01  TS-LONG              PIC S9(2) COMP-3 VALUE 78.              00080001
       01  TS-DONNEES.                                                  00090000
              10 TS-NPAGE       PIC 9(02).                              00100000
              10 TS-DTRTREL     PIC X(08).                              00120100
              10 TS-ZP          PIC X(02).                              00120200
              10 TS-PRELEVE     PIC S9(7)V99 COMP-3.                    00131000
              10 TS-HC          PIC X.                                  00132000
              10 TS-LENSCONC    PIC X(15).                              00140000
              10 TS-DRELEVE     PIC X(04).                              00141000
              10 TS-LEMPCONC    PIC X(15).                              00150000
              10 TS-LCOMMENT    PIC X(20).                              00160001
              10 TS-MODIF       PIC X.                                  00170001
              10 TS-NCONC       PIC X(4).                               00180001
                                                                                
