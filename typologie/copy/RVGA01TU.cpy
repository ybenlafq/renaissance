      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE RXTYP TYPES DE CONCURRENTS             *        
      *----------------------------------------------------------------*        
       01  RVGA01T.                                                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  RXTYP-CTABLEG2    PIC X(15).                                     
           05  RXTYP-CTABLEG2-REDEF REDEFINES RXTYP-CTABLEG2.                   
               10  RXTYP-NTYPCONC        PIC X(02).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  RXTYP-NTYPCONC-N     REDEFINES RXTYP-NTYPCONC                
                                         PIC 9(02).                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  RXTYP-WTABLEG     PIC X(80).                                     
           05  RXTYP-WTABLEG-REDEF  REDEFINES RXTYP-WTABLEG.                    
               10  RXTYP-LTYPCONC        PIC X(20).                             
               10  RXTYP-CTYPENS         PIC X(05).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01T-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RXTYP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  RXTYP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RXTYP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  RXTYP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
