      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *===============================================================* 00010000
      *                                                               * 00020000
      *      TS ASSOCIE A LA TRANSACTION GA37                         * 00030000
      *            GESTION DE L'ECOTAXE                               * 00040000
      *                                                               * 00050000
      *===============================================================* 00120000
                                                                        00130000
       01 TS-GA37.                                                      00140000
          05 TS-GA37-LONG              PIC S9(5) COMP-3     VALUE +57.  00150011
          05 TS-GA37-DONNEES.                                           00160000
             10 TS-GA37-CFAM           PIC  X(5).                       00160111
             10 TS-GA37-CECO           PIC  X(3).                       00160211
             10 TS-GA37-LCECO          PIC  X(10).                      00160311
             10 TS-GA37-WACHVEN        PIC  X.                          00160411
             10 TS-GA37-CPAYS          PIC  X(2).                       00160511
      *      ANCIENNES DONNEES                                          00161011
             10 TS-GA37-DONNEES-O.                                      00170000
                15 TS-GA37-DEFFET-O    PIC  X(8).                       00210000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         15 TS-GA37-MONTANT-O   PIC  S9(5)V99 COMP.              00230005
      *--                                                                       
                15 TS-GA37-MONTANT-O   PIC  S9(5)V99 COMP-5.                    
      *}                                                                        
      *      NOUVELLES DONNEES                                          00230111
             10 TS-GA37-DONNEES-N.                                      00231000
                15 TS-GA37-DEFFET-N    PIC  X(8).                       00235000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         15 TS-GA37-MONTANT-N   PIC  S9(5)V99 COMP .             00237005
      *--                                                                       
                15 TS-GA37-MONTANT-N   PIC  S9(5)V99 COMP-5 .                   
      *}                                                                        
      *      FLAG DES LIGNES SUPPRIMEES                                 00237111
             10 TS-GA37-FLAG-SUPP      PIC  X    VALUE '0'.             00238002
                88 TS-GA37-NON-SUPPRESSION       VALUE '0'.             00239006
                88 TS-GA37-SUPPRESSION           VALUE '1'.             00240006
      *===============================================================* 00280000
                                                                                
