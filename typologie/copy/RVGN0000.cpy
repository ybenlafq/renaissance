      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00030000
      *   COPY DE LA TABLE RVGN0000                                     00040001
      **********************************************************        00050000
      *   LISTE DES HOST VARIABLES DE LA VUE RVGN0000                   00060001
      **********************************************************        00070000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGN0000.                                                    00080001
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGN0000.                                                            
      *}                                                                        
           05  GN00-NCONCN     PIC X(4).                                00090002
           05  GN00-LCONCN     PIC X(20).                               00100002
           05  GN00-LIENCONCN  PIC X(1).                                00110002
           05  GN00-DSYST PIC S9(13) COMP-3.                            00160001
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************        00170000
      *   LISTE DES FLAGS DE LA TABLE RVGN0000                          00180001
      **********************************************************        00190000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGN0000-FLAGS.                                              00200001
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGN0000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN00-NCONCN-F                                            00210001
      *        PIC S9(4) COMP.                                          00211000
      *--                                                                       
           05  GN00-NCONCN-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN00-LCONCN-F                                            00220001
      *        PIC S9(4) COMP.                                          00221000
      *--                                                                       
           05  GN00-LCONCN-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN00-LIENCONCN-F                                         00230001
      *        PIC S9(4) COMP.                                          00231000
      *--                                                                       
           05  GN00-LIENCONCN-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN00-DSYST-F                                             00280001
      *        PIC S9(4) COMP.                                          00290000
      *                                                                         
      *--                                                                       
           05  GN00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
