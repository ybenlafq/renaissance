      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVBS3100                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVBS3100                 00000060
      *   CLE UNIQUE : CPROFASS                                         00000070
      *---------------------------------------------------------        00000080
      *                                                                 00000090
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBS3100.                                                    00000100
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBS3100.                                                            
      *}                                                                        
           10 BS31-CPROFASS        PIC X(5).                            00000110
           10 BS31-NSEQENT         PIC S9(3) COMP-3.                    00000120
           10 BS31-NSEQENTORIG     PIC S9(3) COMP-3.                    00000130
           10 BS31-CPROFLIEN       PIC X(5).                            00000140
           10 BS31-LCOMMENT        PIC X(30).                           00000150
           10 BS31-QDELTACLI       PIC S9(3)V99 COMP-3.                 00000160
           10 BS31-LGNSEQ          PIC X(1).                            00000170
           10 BS31-LVNSEQ          PIC S9(3) COMP-3.                    00000180
           10 BS31-WCONTUNI        PIC X(1).                            00000190
           10 BS31-WCONTFAM        PIC X(1).                            00000200
           10 BS31-LGNENTITE2      PIC X(1).                            00000210
           10 BS31-LVNENTITE2      PIC X(7).                            00000220
           10 BS31-LGNSEQENTREF    PIC X(1).                            00000230
           10 BS31-LVNSEQENTREF    PIC S9(3) COMP-3.                    00000240
           10 BS31-LGDEFFET        PIC X.                               00000250
           10 BS31-LVDEFFET        PIC X(16).                           00000260
           10 BS31-DSYST           PIC S9(13) COMP-3.                   00000270
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00000280
      *---------------------------------------------------------        00000290
      *   LISTE DES FLAGS DE LA TABLE RVBS3000                          00000300
      *---------------------------------------------------------        00000310
      *                                                                 00000320
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBS3100-FLAGS.                                              00000330
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBS3100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS31-CPROFASS-F      PIC S9(4) COMP.                      00000340
      *--                                                                       
           10 BS31-CPROFASS-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS31-NSEQENT-F       PIC S9(4) COMP.                      00000350
      *--                                                                       
           10 BS31-NSEQENT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS31-NSEQENTORIG-F   PIC S9(4) COMP.                      00000360
      *--                                                                       
           10 BS31-NSEQENTORIG-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS31-CPROFLIEN-F     PIC S9(4) COMP.                      00000370
      *--                                                                       
           10 BS31-CPROFLIEN-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS31-QDELTACLI-F     PIC S9(4) COMP.                      00000380
      *--                                                                       
           10 BS31-QDELTACLI-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS31-LGNSEQ-F        PIC S9(4) COMP.                      00000390
      *--                                                                       
           10 BS31-LGNSEQ-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS31-LVNSEQ-F        PIC S9(4) COMP.                      00000400
      *--                                                                       
           10 BS31-LVNSEQ-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS31-LGNENTITE2-F    PIC S9(4) COMP.                      00000410
      *--                                                                       
           10 BS31-LGNENTITE2-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS31-LVNENTITE2-F    PIC S9(4) COMP.                      00000420
      *--                                                                       
           10 BS31-LVNENTITE2-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS31-LGNSEQENTREF-F  PIC S9(4) COMP.                      00000430
      *--                                                                       
           10 BS31-LGNSEQENTREF-F  PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS31-LVNSEQENTREF-F  PIC S9(4) COMP.                      00000440
      *--                                                                       
           10 BS31-LVNSEQENTREF-F  PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS31-LGDEFFET-F      PIC S9(4) COMP.                      00000450
      *--                                                                       
           10 BS31-LGDEFFET-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS31-LVDEFFET-F      PIC S9(4) COMP.                      00000460
      *--                                                                       
           10 BS31-LVDEFFET-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS31-DSYST-F         PIC S9(4) COMP.                      00000470
      *                                                                         
      *                                                                         
      *                                                                         
      *                                                                         
      *                                                                         
      *                                                                         
      *--                                                                       
           10 BS31-DSYST-F         PIC S9(4) COMP-5.                            
                                                                        00000480
                                                                        00000490
                                                                        00000500
                                                                        00000510
                                                                                
                                                                        00000520
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
