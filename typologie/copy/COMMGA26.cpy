      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
           02 COMM-GA26-APPLI REDEFINES COMM-GA00-APPLI.                        
              05 COMM-GA26-WFONC                PIC  X(3).                      
              05 COMM-GA26-CETAT                PIC  X(10).                     
              05 COMM-GA26-LETAT                PIC  X(54).                     
              05 COMM-GA26-TYPE                 PIC  X.                         
              05 COMM-GA26-CFAM                 PIC  X(5).                      
              05 COMM-GA26-TABFAM.                                              
                 10 COMM-GA26-DFAM OCCURS 46    PIC  X(5).                      
              05 COMM-GA26-PAGENC               PIC  S9(2) COMP-3.              
              05 COMM-GA26-SAUVECR.                                             
                 10 COMM-GA26-LIGNE OCCURS 11.                                  
                    15 COMM-GA26-LFAM           PIC  X(5).                      
                    15 COMM-GA26-BORNE OCCURS 10 PIC S9(5) COMP-3.              
                    15 COMM-GA26-NSEQ           PIC  9(5).                      
              05 COMM-S-A  OCCURS 366           PIC  X.                         
              05 COMM-GA26-FILLER               PIC  X(2613).                   
                                                                                
