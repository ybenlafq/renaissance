      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                                 00000010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-BS00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.          00000020
      *--                                                                       
       01  COMM-BS00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                
      *}                                                                        
      *                                                                 00000030
       01  Z-COMMAREA.                                                  00000040
                                                                        00000050
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00000060
           02 FILLER-COM-AIDA      PIC X(100).                          00000070
                                                                        00000080
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00000090
           02 COMM-CICS-APPLID     PIC X(08).                           00000100
           02 COMM-CICS-NETNAM     PIC X(08).                           00000110
           02 COMM-CICS-TRANSA     PIC X(04).                           00000120
                                                                        00000130
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00000140
           02 COMM-DATE-SIECLE     PIC X(02).                           00000150
           02 COMM-DATE-ANNEE      PIC X(02).                           00000160
           02 COMM-DATE-MOIS       PIC X(02).                           00000170
           02 COMM-DATE-JOUR       PIC 99.                              00000180
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00000190
           02 COMM-DATE-QNTA       PIC 999.                             00000200
           02 COMM-DATE-QNT0       PIC 99999.                           00000210
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00000220
           02 COMM-DATE-BISX       PIC 9.                               00000230
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00000240
           02 COMM-DATE-JSM        PIC 9.                               00000250
      *   LIBELLES DU JOUR COURT - LONG                                 00000260
           02 COMM-DATE-JSM-LC     PIC X(03).                           00000270
           02 COMM-DATE-JSM-LL     PIC X(08).                           00000280
      *   LIBELLES DU MOIS COURT - LONG                                 00000290
           02 COMM-DATE-MOIS-LC    PIC X(03).                           00000300
           02 COMM-DATE-MOIS-LL    PIC X(08).                           00000310
      *   DIFFERENTES FORMES DE DATE                                    00000320
           02 COMM-DATE-SSAAMMJJ   PIC X(08).                           00000330
           02 COMM-DATE-AAMMJJ     PIC X(06).                           00000340
           02 COMM-DATE-JJMMSSAA   PIC X(08).                           00000350
           02 COMM-DATE-JJMMAA     PIC X(06).                           00000360
           02 COMM-DATE-JJ-MM-AA   PIC X(08).                           00000370
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00000380
      *   DATE DU LENDEMAIN                                             00000390
           02 COMM-DATE-DEMAIN-SAMJ PIC X(08).                          00000400
           02 COMM-DATE-DEMAIN-JMA PIC X(08).                           00000410
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00000420
           02 COMM-DATE-WEEK.                                           00000430
              05 COMM-DATE-SEMSS   PIC 99.                              00000440
              05 COMM-DATE-SEMAA   PIC 99.                              00000450
              05 COMM-DATE-SEMNU   PIC 99.                              00000460
           02 COMM-DATE-FILLER     PIC X(08).                           00000470
      * ZONES RESERVEES TRAITEMENT DU SWAP                              00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00000490
      *                                                                         
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
                                                                        00000500
      *}                                                                        
      * ZONES RESERVEES APPLICATIVES ------------- LG : 3874            00000510
           02 COMM-BS00-APPLI.                                          00000520
              03 COMM-ENTETE.                                           00000530
                 05 COMM-CODLANG            PIC X(02).                  00000540
                 05 COMM-CODPIC             PIC X(02).                  00000550
                 05 COMM-CODESFONCTION      PIC X(12).                  00000560
                 05 COMM-DROIT              PIC X(01).                  00000570
              03 COMM-00-ENTETE.                                        00000580
                 05 COMM-00-NCOFFRE         PIC X(07).                  00000590
                 05 COMM-00-CMARQ           PIC X(05).                  00000600
                 05 COMM-00-LMARQ           PIC X(20).                  00000610
                 05 COMM-00-CFAM            PIC X(05).                  00000620
                 05 COMM-00-LCFAM           PIC X(20).                  00000630
                 05 COMM-00-CASSOR          PIC X(05).                  00000640
                 05 COMM-00-LASSOR          PIC X(20).                  00000650
                 05 COMM-00-CPROFASS        PIC X(05).                  00000660
                 05 COMM-00-LPROFASS        PIC X(20).                  00000670
                 05 COMM-00-ENTITE          PIC X(07).                  00000680
                 05 COMM-00-LFAMENT         PIC X(05).                  00000690
                 05 COMM-00-LMARQENT        PIC X(05).                  00000700
                 05 COMM-00-LIBENT          PIC X(20).                  00000710
                 05 COMM-00-CTYPENT         PIC X(02).                  00000720
                 05 COMM-00-WFONC           PIC X(03).                  00000730
              03 COMM-01-ENTETE.                                        00000740
                 05 COMM-01-INDTS           PIC S9(03) COMP-3.          00000750
                 05 COMM-01-INDMAX          PIC S9(03) COMP-3.          00000760
                 05 COMM-01-NUMPAGE         PIC  9(03).                 00000770
                 05 COMM-01-NBRACT          PIC  9.                     00000780
                 05 COMM-01-NBRCRE          PIC  9.                     00000790
              03 COMM-50-ENTETE.                                        00000800
                 05 COMM-50-OFFRE-ENC       PIC  9.                     00000810
                 05 COMM-50-OFFRE-TOT       PIC  9.                     00000820
                 05 COMM-50-TAB1.                                       00000830
                    10 COMM-50-POSTE-TAB1  OCCURS 2  PIC 9.             00000840
                 05 COMM-50-TAB2.                                       00000850
                    10 COMM-50-POSTE-TAB2  OCCURS 2.                    00000860
                       15 COMM-50-FONCTION        PIC X(03).            00000870
                       15 COMM-50-NCOFFRE         PIC X(07).            00000880
                       15 COMM-50-NCOFFRE-2       PIC X(07).            00000890
                       15 COMM-50-LCOFFRE         PIC X(20).            00000900
                       15 COMM-50-CMARQ           PIC X(05).            00000910
                       15 COMM-50-LMARQ           PIC X(20).            00000920
                       15 COMM-50-CFAM            PIC X(05).            00000930
                       15 COMM-50-CFAM-AV         PIC X(05).            00000940
                       15 COMM-50-LCFAM           PIC X(20).            00000950
                       15 COMM-50-LCFAM-AV        PIC X(20).            00000960
                       15 COMM-50-CPROFASS        PIC X(05).            00000970
                       15 COMM-50-CPROFASS-AV     PIC X(05).            00000980
                       15 COMM-50-CTYPENT         PIC X(02).            00000990
                       15 COMM-50-WREMAUT         PIC X(01).            00001000
                       15 COMM-50-CCALCUL         PIC X(05).            00001010
                       15 COMM-50-LPROFASS        PIC X(20).            00001020
                       15 COMM-50-LPROFASS-AV     PIC X(20).            00001030
                       15 COMM-50-CASSOR          PIC X(05).            00001040
                       15 COMM-50-LASSOR          PIC X(20).            00001050
                       15 COMM-50-CSTATUT         PIC X(01).            00001060
                       15 COMM-50-OBLIG2          PIC X(01).            00001070
                       15 COMM-50-OBLIG3          PIC X(01).            00001080
                       15 COMM-50-OBLIG4          PIC X(01).            00001090
                       15 COMM-50-CPT-MAJ1        PIC S9(03) COMP-3.    00001100
                       15 COMM-50-CPT-MAJ2        PIC S9(03) COMP-3.    00001110
                       15 COMM-50-CPT-MAJ3        PIC S9(03) COMP-3.    00001120
                       15 COMM-50-CPT-MAJ4        PIC S9(03) COMP-3.    00001130
                       15 COMM-50-CPT-ERR1        PIC S9(03) COMP-3.    00001140
                       15 COMM-50-CPT-ERR2        PIC S9(03) COMP-3.    00001150
                       15 COMM-50-CPT-ERR3        PIC S9(03) COMP-3.    00001160
                       15 COMM-50-CPT-ERR4        PIC S9(03) COMP-3.    00001170
                       15 COMM-50-CAFF            PIC X.                00001180
                          88 ATT-LIBRE        VALUE '0' '1' '2' .       00001190
                          88 CTRL-LIBRE       VALUE '0' .               00001200
                          88 CTRL-OBLIG       VALUE '1' .               00001210
                          88 CTRL-LOG-ENT2    VALUE 'O'.                00001220
              03 COMM-51-PAGE.                                          00001230
                 05 COMM-51-NBRATT           PIC  999.                  00001240
                 05 COMM-51-NUMPAGE          PIC  999  OCCURS 2.        00001250
                 05 COMM-51-LIGNE.                                      00001260
                   10 COMM-51-POSTE        OCCURS 2.                    00001270
      * ZONES NON SAISISSABLES D'UN SERVICE EN CRE / MAJ :              00001280
                      15 COMM-51-NCOFFRE-AP     PIC X(07).              00001290
                      15 COMM-51-DCREATE-AP     PIC X(08).              00001300
                      15 COMM-51-DMAJ-AP        PIC X(08).              00001310
                      15 COMM-51-CASSORTC-AP    PIC X(05).              00001320
                      15 COMM-51-LASSORTC-AP    PIC X(20).              00001330
                      15 COMM-51-DATEC-AP       PIC X(08).              00001340
      * ZONES MODIFIABLES EN MAJ / CRE :                                00001350
      *   LES CHAMPS SUFFIXES PAR -AV CONRIENNENT LES VAL INITIALES     00001360
      *   LES CHAMPS SUFFIXES PAR -AP CONRIENNENT LES NEW VALEURS       00001370
                      15 COMM-51-DATA-AV.                               00001380
                         20 COMM-51-LCOFFRE-AV  PIC X(20).              00001390
                         20 COMM-51-CMARQ-AV    PIC X(05).              00001400
                         20 COMM-51-LCMARQ-AV   PIC X(20).              00001410
                         20 COMM-51-CTVA-AV     PIC X(01).              00001420
                         20 COMM-51-NTVA-AV     PIC S999V99.            00001430
                         20 COMM-51-LCTVA-AV    PIC X(20).              00001440
                         20 COMM-51-CASSORTF-AV PIC X(05).              00001450
                         20 COMM-51-LASSORTF-AV PIC X(20).              00001460
                         20 COMM-51-DATEF-AV    PIC X(08).              00001470
                         20 COMM-51-LREFDARTY-AV PIC X(20).             00001480
                         20 COMM-51-LCOMMENT-AV PIC X(50).              00001490
                         20 COMM-51-CHEF-AV     PIC X(05).              00001500
                         20 COMM-51-LCHEF-AV    PIC X(20).              00001510
                         20 COMM-51-CCOMPT-AV   PIC X(03).              00001520
                         20 COMM-51-LCOMPT-AV   PIC X(20).              00001530
                         20 COMM-51-PRIX-AV     PIC S9(7)V99 COMP-3.    00001540
                         20 COMM-51-PRIX-F-AV   PIC X.                  00001550
                         20 COMM-51-PRIME-AV    PIC S9(7)V99 COMP-3.    00001560
                         20 COMM-51-PRIME-F-AV  PIC X.                  00001570
                         20 COMM-51-CODPROF-AV  PIC X(10).              00001580
                         20 COMM-51-FAMCLT-AV   PIC X(30).              00001590
NV01                     20 COMM-51-QTEMUL-AV   PIC X.                  00001600
NV01                     20 COMM-51-REMISE-AV   PIC X.                  00001610
NV01                     20 COMM-51-PRNTTCK-AV  PIC X.                  00001620
SR99                     20 COMM-51-CLTREQ-AV   PIC X.                  00001630
SR99                     20 COMM-51-CLIREQ-AV   PIC X.                  00001640
                      15 COMM-51-DATA-AP.                               00001650
                         20 COMM-51-LCOFFRE-AP  PIC X(20).              00001660
                         20 COMM-51-CMARQ-AP    PIC X(05).              00001670
                         20 COMM-51-LCMARQ-AP   PIC X(20).              00001680
                         20 COMM-51-CTVA-AP     PIC X(01).              00001690
                         20 COMM-51-NTVA-AP     PIC S999V99.            00001700
                         20 COMM-51-LCTVA-AP    PIC X(20).              00001710
                         20 COMM-51-CASSORTF-AP PIC X(05).              00001720
                         20 COMM-51-LASSORTF-AP PIC X(20).              00001730
                         20 COMM-51-DATEF-AP    PIC X(08).              00001740
                         20 COMM-51-LREFDARTY-AP PIC X(20).             00001750
                         20 COMM-51-LCOMMENT-AP PIC X(50).              00001760
                         20 COMM-51-CHEF-AP     PIC X(05).              00001770
                         20 COMM-51-LCHEF-AP    PIC X(20).              00001780
                         20 COMM-51-CCOMPT-AP   PIC X(03).              00001790
                         20 COMM-51-LCOMPT-AP   PIC X(20).              00001800
                         20 COMM-51-PRIX-AP     PIC S9(7)V99 COMP-3.    00001810
                         20 COMM-51-PRIX-F-AP   PIC X.                  00001820
                         20 COMM-51-PRIME-AP    PIC S9(7)V99 COMP-3.    00001830
                         20 COMM-51-PRIME-F-AP  PIC X.                  00001840
                         20 COMM-51-CODPROF-AP  PIC X(10).              00001850
                         20 COMM-51-FAMCLT-AP   PIC X(30).              00001860
NV01                     20 COMM-51-QTEMUL-AP   PIC X.                  00001870
NV01                     20 COMM-51-REMISE-AP   PIC X.                  00001880
NV01                     20 COMM-51-PRNTTCK-AP  PIC X.                  00001890
SR99                     20 COMM-51-CLTREQ-AP   PIC X.                  00001900
SR99                     20 COMM-51-CLIREQ-AP   PIC X.                  00001910
      *--- FLAG DE CONTROLE DE SAISIE + CODE ERREUR ASSOCIE             00001920
      *    VALEURS PERMISES : A CONTROLER    VALUE SPACES               00001930
      *                       SAISIE OK      VALUE '0'                  00001940
      *                       SAISIE KO      VALUE '1'                  00001950
                      15 COMM-51-FLAG-CTRL.                             00001960
                         20 COMM-51-FLAG-CMARQ      PIC X(01).          00001970
                         20 COMM-51-CDRET-CMARQ     PIC X(04).          00001980
                         20 COMM-51-FLAG-CFAM       PIC X(01).          00001990
                         20 COMM-51-CDRET-CFAM      PIC X(04).          00002000
                         20 COMM-51-FLAG-CPROFASS   PIC X(01).          00002010
                         20 COMM-51-CDRET-CPROFASS  PIC X(04).          00002020
                         20 COMM-51-FLAG-TVA        PIC X(01).          00002030
                         20 COMM-51-CDRET-TVA       PIC X(04).          00002040
                         20 COMM-51-FLAG-ASSORT     PIC X(01).          00002050
                         20 COMM-51-CDRET-ASSORT    PIC X(04).          00002060
                         20 COMM-51-FLAG-CHEF       PIC X(01).          00002070
                         20 COMM-51-CDRET-CHEF      PIC X(04).          00002080
                         20 COMM-51-FLAG-STATCOMP   PIC X(01).          00002090
                         20 COMM-51-CDRET-STATCOMP  PIC X(04).          00002100
                         20 COMM-51-FLAG-LCOFFRE    PIC X(01).          00002110
                         20 COMM-51-CDRET-LCOFFRE   PIC X(04).          00002120
                         20 COMM-51-FLAG-CODPROF    PIC X(01).          00002130
                         20 COMM-51-CDRET-CODPROF   PIC X(04).          00002140
      *--- FLAG D'AUTORISATION                                          00002150
      *    VALEURS PERMISES : CHAMP SAISISSABLE      : VALUE 'O'        00002160
      *                     : CHAMP  NON SAISISSABLE : VALUE 'N'        00002170
                      15 COMM-51-FLAG-AFF.                              00002180
                         20 COMM-51-MLCOFFRE-AFF   PIC X.               00002190
                         20 COMM-51-CMARQ-AFF      PIC X.               00002200
                         20 COMM-51-CFAM-AFF       PIC X.               00002210
                         20 COMM-51-CPROFASS-AFF   PIC X.               00002220
                         20 COMM-51-MCTXTVA-AFF    PIC X.               00002230
                         20 COMM-51-MCASSOR-AFF    PIC X.               00002240
                         20 COMM-51-MLREFDARTY-AFF PIC X.               00002250
                         20 COMM-51-MLCOMMENT-AFF  PIC X.               00002260
                         20 COMM-51-MCHEF-AFF      PIC X.               00002270
                         20 COMM-51-MCCOMPT-AFF    PIC X.               00002280
                         20 COMM-51-MPRIX-AFF      PIC X.               00002290
                         20 COMM-51-MPRIME-AFF     PIC X.               00002300
                         20 COMM-51-MCODPROF-AFF   PIC X.               00002310
NV01                     20 COMM-51-MQTEMUL-AFF    PIC X.               00002320
NV01                     20 COMM-51-MREMISE-AFF    PIC X.               00002330
NV01                     20 COMM-51-MPRNTTCK-AFF   PIC X.               00002340
              03 COMM-52-ENTETE.                                        00002350
                 05 COMM-52-TAB-IND-TS  OCCURS 2.                       00002360
                    10 COMM-52-INDTS          PIC S9(03) COMP-3.        00002370
                    10 COMM-52-INDMAX         PIC S9(03) COMP-3.        00002380
                    10 COMM-52-NUMPAGE        PIC  9(03).               00002390
              03 COMM-53-ENTETE.                                        00002400
                 05 COMM-53-TAB-IND-TS  OCCURS 2.                       00002410
                    10 COMM-53-INDICE-TAB     PIC S9(03) COMP-3.        00002420
                    10 COMM-53-INDMAX         PIC S9(03) COMP-3.        00002430
                    10 COMM-53-NUMPAGE        PIC  9(03).               00002440
                    10 COMM-53-SELECTION      PIC  X.                   00002450
              03 COMM-54-ENTETE.                                        00002460
                 05 COMM-54-TAB-IND-TS  OCCURS 2.                       00002470
                    10 COMM-54-INDTS          PIC S9(03) COMP-3.        00002480
                    10 COMM-54-INDMAX         PIC S9(03) COMP-3.        00002490
                    10 COMM-54-INDMAX-AV      PIC S9(03) COMP-3.        00002500
                    10 COMM-54-NUMPAGE        PIC  9(03).               00002510
              03 COMM-56-ENTETE.                                        00002520
                 05 COMM-56-TAB-IND-TS  OCCURS 2.                       00002530
                    10 COMM-56-INDTS          PIC S9(03) COMP-3.        00002540
                    10 COMM-56-INDTS-INIT     PIC S9(03) COMP-3.        00002550
                    10 COMM-56-INDMAX         PIC S9(03) COMP-3.        00002560
                    10 COMM-56-NUMPAGE        PIC  9(03).               00002570
                    10 COMM-56-CDOSS          PIC  XXXXX.               00002580
           02 COMM-BS00-FILLER    PIC X(0001).                          00002590
  RG01*    02 FILLER-COMM                     PIC X(1818).              00002600
  RG01     02 FILLER-COMM                     PIC X(1814).              00002610
                                                                                
