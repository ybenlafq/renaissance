      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MGD91 ALLEES ET POSITIONS LIMITES      *        
      *----------------------------------------------------------------*        
       01  RVGA01KE.                                                            
           05  MGD91-CTABLEG2    PIC X(15).                                     
           05  MGD91-CTABLEG2-REDEF REDEFINES MGD91-CTABLEG2.                   
               10  MGD91-NSOCDEP         PIC X(06).                             
               10  MGD91-NSOCDEP-N      REDEFINES MGD91-NSOCDEP                 
                                         PIC 9(06).                             
               10  MGD91-NUMERO          PIC X(02).                             
               10  MGD91-NUMERO-N       REDEFINES MGD91-NUMERO                  
                                         PIC 9(02).                             
           05  MGD91-WTABLEG     PIC X(80).                                     
           05  MGD91-WTABLEG-REDEF  REDEFINES MGD91-WTABLEG.                    
               10  MGD91-DROITE          PIC X(02).                             
               10  MGD91-GAUCHE          PIC X(02).                             
               10  MGD91-LIMITE          PIC X(03).                             
               10  MGD91-LIMITE-N       REDEFINES MGD91-LIMITE                  
                                         PIC 9(03).                             
               10  MGD91-ACTIF           PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01KE-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MGD91-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MGD91-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MGD91-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MGD91-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
