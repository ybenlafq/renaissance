      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * TS SPECIFIQUE TABLE RTGG09                                 *            
      *       TR : GG00  GESTION DES GAMMES MENU                   *            
      *       PG : TGG09 INT DES PRIX ET COMMISSIONS ARTICLE       *            
      **************************************************************            
      *                                                                         
      * ZONES RESERVEES APPLICATIVES -----------------------------              
      *                                                                         
       01  TS-LONG            PIC S9(4) COMP-3 VALUE 620.                       
       01  TS-DONNEES.                                                          
           05 TS-LIGNE   OCCURS 10.                                             
      * ZONES PRIX ARTICLE ET COMMISSION ANCIENS------------------              
              10  TS-NZONPRIX          PIC X(2).                                
              10  TS-PRIXAC            PIC S9(7)V99   COMP-3.                   
              10  TS-PMBUAC            PIC S9(5)V99   COMP-3.                   
              10  TS-PCOMMA            PIC S9(4)V99   COMP-3.                   
              10  TS-PCOMMVOLA         PIC S9(4)V99   COMP-3.                   
              10  TS-FMILAC            PIC S9(5)V99   COMP-3.                   
              10  TS-DATEAC            PIC 9(8).                                
      * ZONES PRIX ARTICLE ET COMMISSION NOUVEAUX-----------------              
              10  TS-PRIXN             PIC S9(7)V99   COMP-3.                   
              10  TS-LCOMM             PIC X(05).                               
              10  TS-PCOMMN            PIC S9(4)V99   COMP-3.                   
              10  TS-PCOMMVOLN         PIC S9(4)V99   COMP-3.                   
      * ZONES CALCULEES ------------------------------------------              
              10  TS-PMBUN             PIC S9(5)V99   COMP-3.                   
              10  TS-QTMBN             PIC S9(4)V9(3) COMP-3.                   
              10  TS-FMILN             PIC S9(5)V99   COMP-3.                   
              10  TS-EXIST-ZP          PIC X(1).                                
                                                                                
