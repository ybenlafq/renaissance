      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                               *         
       01  TS-GA50.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GA50-LONG                 PIC S9(03) COMP-3                    
                                           VALUE +40.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-GA50-DONNEES.                                                  
              03 TS-GA50-CPROG             PIC X(05).                           
              03 TS-GA50-CCHAMP            PIC X(08).                           
              03 TS-GA50-NSOCIETE          PIC X(03).                           
              03 TS-GA50-ATTRIBUTA         PIC X(01).                           
              03 TS-GA50-ATTRIBUTC         PIC X(01).                           
              03 TS-GA50-ATTRIBUTH         PIC X(01).                           
              03 TS-GA50-DEFAUT            PIC X(20).                           
              03 TS-GA50-CFONC             PIC X(01).                           
                                                                                
