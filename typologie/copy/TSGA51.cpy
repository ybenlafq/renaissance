      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   TS : TYPE DE DECLARATION                                    *         
      *****************************************************************         
      *                                                               *         
       01  TS-GA51.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GA51-LONG                 PIC S9(5) COMP-3                     
                                           VALUE +13.                           
           02 TS-GA51-DONNEES.                                                  
      *------------------------------TABLE TYPE DE DECLARATION                  
              03 TS-GA51-TOPMAJ               PIC X(1).                         
              03 TS-GA51-NCODIC               PIC X(7).                         
              03 TS-GA51-CTYPDCL              PIC X(5).                         
      *                                                                         
                                                                                
