      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
       01  Z-COMMAREA-MSE21.                                            00000020
           02 COMM-MSE21-ENTETE-RETOUR.                                 00000030
      *---- BOOLEEN TYPE D'ERREUR                                       00000040
              03 COMM-MSE21-TYPERR      PIC 9(01).                      00000050
                 88 MSE21-NO-ERROR        VALUE 0.                      00000060
                 88 MSE21-APPL-ERROR      VALUE 1.                      00000070
                 88 MSE21-DB2-ERROR       VALUE 2.                      00000080
              03 COMM-MSE21-FUNC-SQL    PIC X(08).                      00000090
              03 COMM-MSE21-TABLE-NAME  PIC X(08).                      00000100
              03 COMM-MSE21-SQLCODE     PIC S9(04).                     00000110
              03 COMM-MSE21-POSITION    PIC  9(03).                     00000120
           02 COMM-MSE21-MODE-ENTREE     PIC 9(01).                     00000130
              88 CAS-LISTE            VALUE 0.                          00000140
              88 CAS-AJOUT-SERV       VALUE 1.                          00000150
              88 CAS-CREA-PURE        VALUE 2.                          00000160
           02 COMM-MSE21-CODESFONCTION    PIC X(12).                    00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-MSE21-INDMAX           PIC S9(04) COMP.              00000180
      *--                                                                       
           02 COMM-MSE21-INDMAX           PIC S9(04) COMP-5.                    
      *}                                                                        
           02 COMM-MSE21-WFONC            PIC X(03).                    00000190
           02 COMM-MSE21-NCSERV           PIC X(05).                    00000200
           02 COMM-MSE21-LCSERV           PIC X(20).                    00000210
           02 COMM-MSE21-COPER            PIC X(05).                    00000220
           02 COMM-MSE21-LOPER            PIC X(20).                    00000230
           02 COMM-MSE21-CFAM             PIC X(05).                    00000240
           02 COMM-MSE21-LCFAM            PIC X(20).                    00000250
           02 COMM-MSE21-TYPEM            PIC X(01).                    00000260
           02 COMM-MSE21-TYPEO            PIC X(01).                    00000270
           02 COMM-MSE21-TYPEC            PIC X(01).                    00000280
           02 COMM-MSE21-CASSOR           PIC X(05).                    00000290
FV2        02 COMM-MSE21-FILLER           PIC X(3971).                  00000300
FV2   *    02 COMM-MSE21-FILLER           PIC X(3974).                  00000310
                                                                                
