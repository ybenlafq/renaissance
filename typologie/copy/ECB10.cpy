      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * MAJ DES PRIX ARTICLES ET PRIMES                                 00000020
      ***************************************************************** 00000030
       01   ECB10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(8).                                       00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGINEL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGINEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPAGINEF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGINEI  PIC X(7).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNCODICI  PIC X(7).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLREFI    PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCFAMI    PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLFAMI    PIC X(20).                                      00000330
           02 MDEPOTD OCCURS   3 TIMES .                                00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEPOTL      COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MDEPOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDEPOTF      PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MDEPOTI      PIC X(6).                                  00000380
           02 MCAPPROD OCCURS   3 TIMES .                               00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAPPROL     COMP PIC S9(4).                            00000400
      *--                                                                       
             03 MCAPPROL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCAPPROF     PIC X.                                     00000410
             03 FILLER  PIC X(4).                                       00000420
             03 MCAPPROI     PIC X(5).                                  00000430
           02 MCEXPOD OCCURS   3 TIMES .                                00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCEXPOL      COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MCEXPOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCEXPOF      PIC X.                                     00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MCEXPOI      PIC X(5).                                  00000480
           02 MSTATD OCCURS   3 TIMES .                                 00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTATL  COMP PIC S9(4).                                 00000500
      *--                                                                       
             03 MSTATL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSTATF  PIC X.                                          00000510
             03 FILLER  PIC X(4).                                       00000520
             03 MSTATI  PIC X(3).                                       00000530
           02 MSTODEPD OCCURS   3 TIMES .                               00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTODEPL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MSTODEPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTODEPF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MSTODEPI     PIC X(5).                                  00000580
           02 MCDED OCCURS   3 TIMES .                                  00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCDEL   COMP PIC S9(4).                                 00000600
      *--                                                                       
             03 MCDEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MCDEF   PIC X.                                          00000610
             03 FILLER  PIC X(4).                                       00000620
             03 MCDEI   PIC X(5).                                       00000630
           02 MCDEDD OCCURS   3 TIMES .                                 00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCDEDL  COMP PIC S9(4).                                 00000650
      *--                                                                       
             03 MCDEDL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCDEDF  PIC X.                                          00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MCDEDI  PIC X(8).                                       00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MCMARQI   PIC X(5).                                       00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MLMARQI   PIC X(20).                                      00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATUTL  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MSTATUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTATUTF  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MSTATUTI  PIC X(5).                                       00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSAPPL    COMP PIC S9(4).                                 00000810
      *--                                                                       
           02 MSAPPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSAPPF    PIC X.                                          00000820
           02 FILLER    PIC X(4).                                       00000830
           02 MSAPPI    PIC X.                                          00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSTATUTL      COMP PIC S9(4).                            00000850
      *--                                                                       
           02 MDSTATUTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDSTATUTF      PIC X.                                     00000860
           02 FILLER    PIC X(4).                                       00000870
           02 MDSTATUTI      PIC X(8).                                  00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPXREFL   COMP PIC S9(4).                                 00000890
      *--                                                                       
           02 MPXREFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPXREFF   PIC X.                                          00000900
           02 FILLER    PIC X(4).                                       00000910
           02 MPXREFI   PIC X(8).                                       00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPXREFDL  COMP PIC S9(4).                                 00000930
      *--                                                                       
           02 MPXREFDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPXREFDF  PIC X.                                          00000940
           02 FILLER    PIC X(4).                                       00000950
           02 MPXREFDI  PIC X(8).                                       00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMBURL    COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MMBURL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MMBURF    PIC X.                                          00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MMBURI    PIC X(8).                                       00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTMBURL   COMP PIC S9(4).                                 00001010
      *--                                                                       
           02 MTMBURL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTMBURF   PIC X.                                          00001020
           02 FILLER    PIC X(4).                                       00001030
           02 MTMBURI   PIC X(5).                                       00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEDMRL    COMP PIC S9(4).                                 00001050
      *--                                                                       
           02 MEDMRL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MEDMRF    PIC X.                                          00001060
           02 FILLER    PIC X(4).                                       00001070
           02 MEDMRI    PIC X(5).                                       00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPMADPRL  COMP PIC S9(4).                                 00001090
      *--                                                                       
           02 MPMADPRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPMADPRF  PIC X.                                          00001100
           02 FILLER    PIC X(4).                                       00001110
           02 MPMADPRI  PIC X(5).                                       00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPMREFL   COMP PIC S9(4).                                 00001130
      *--                                                                       
           02 MPMREFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPMREFF   PIC X.                                          00001140
           02 FILLER    PIC X(4).                                       00001150
           02 MPMREFI   PIC X(5).                                       00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPMREFDL  COMP PIC S9(4).                                 00001170
      *--                                                                       
           02 MPMREFDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPMREFDF  PIC X.                                          00001180
           02 FILLER    PIC X(4).                                       00001190
           02 MPMREFDI  PIC X(8).                                       00001200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOANATL   COMP PIC S9(4).                                 00001210
      *--                                                                       
           02 MOANATL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MOANATF   PIC X.                                          00001220
           02 FILLER    PIC X(4).                                       00001230
           02 MOANATI   PIC X.                                          00001240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOANATDL  COMP PIC S9(4).                                 00001250
      *--                                                                       
           02 MOANATDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MOANATDF  PIC X.                                          00001260
           02 FILLER    PIC X(4).                                       00001270
           02 MOANATDI  PIC X(8).                                       00001280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPXCESSL  COMP PIC S9(4).                                 00001290
      *--                                                                       
           02 MPXCESSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPXCESSF  PIC X.                                          00001300
           02 FILLER    PIC X(4).                                       00001310
           02 MPXCESSI  PIC X(8).                                       00001320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSRPL     COMP PIC S9(4).                                 00001330
      *--                                                                       
           02 MSRPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSRPF     PIC X.                                          00001340
           02 FILLER    PIC X(4).                                       00001350
           02 MSRPI     PIC X(8).                                       00001360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRAL     COMP PIC S9(4).                                 00001370
      *--                                                                       
           02 MPRAL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MPRAF     PIC X.                                          00001380
           02 FILLER    PIC X(4).                                       00001390
           02 MPRAI     PIC X(8).                                       00001400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRMPL    COMP PIC S9(4).                                 00001410
      *--                                                                       
           02 MPRMPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPRMPF    PIC X.                                          00001420
           02 FILLER    PIC X(4).                                       00001430
           02 MPRMPI    PIC X(8).                                       00001440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBRL    COMP PIC S9(4).                                 00001450
      *--                                                                       
           02 MLIBRL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIBRF    PIC X.                                          00001460
           02 FILLER    PIC X(4).                                       00001470
           02 MLIBRI    PIC X(20).                                      00001480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEDMSRL   COMP PIC S9(4).                                 00001490
      *--                                                                       
           02 MEDMSRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MEDMSRF   PIC X.                                          00001500
           02 FILLER    PIC X(4).                                       00001510
           02 MEDMSRI   PIC X(5).                                       00001520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSELNL    COMP PIC S9(4).                                 00001530
      *--                                                                       
           02 MSELNL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSELNF    PIC X.                                          00001540
           02 FILLER    PIC X(4).                                       00001550
           02 MSELNI    PIC X.                                          00001560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCONCNL  COMP PIC S9(4).                                 00001570
      *--                                                                       
           02 MNCONCNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCONCNF  PIC X.                                          00001580
           02 FILLER    PIC X(4).                                       00001590
           02 MNCONCNI  PIC X(4).                                       00001600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUNL   COMP PIC S9(4).                                 00001610
      *--                                                                       
           02 MLIEUNL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIEUNF   PIC X.                                          00001620
           02 FILLER    PIC X(4).                                       00001630
           02 MLIEUNI   PIC X(3).                                       00001640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPXNL     COMP PIC S9(4).                                 00001650
      *--                                                                       
           02 MPXNL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MPXNF     PIC X.                                          00001660
           02 FILLER    PIC X(4).                                       00001670
           02 MPXNI     PIC X(8).                                       00001680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPXDNL    COMP PIC S9(4).                                 00001690
      *--                                                                       
           02 MPXDNL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPXDNF    PIC X.                                          00001700
           02 FILLER    PIC X(4).                                       00001710
           02 MPXDNI    PIC X(8).                                       00001720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMBUNL    COMP PIC S9(4).                                 00001730
      *--                                                                       
           02 MMBUNL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MMBUNF    PIC X.                                          00001740
           02 FILLER    PIC X(4).                                       00001750
           02 MMBUNI    PIC X(8).                                       00001760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTMBUNL   COMP PIC S9(4).                                 00001770
      *--                                                                       
           02 MTMBUNL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTMBUNF   PIC X.                                          00001780
           02 FILLER    PIC X(4).                                       00001790
           02 MTMBUNI   PIC X(5).                                       00001800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEDMNL    COMP PIC S9(4).                                 00001810
      *--                                                                       
           02 MEDMNL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MEDMNF    PIC X.                                          00001820
           02 FILLER    PIC X(4).                                       00001830
           02 MEDMNI    PIC X(5).                                       00001840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPMADAPNL      COMP PIC S9(4).                            00001850
      *--                                                                       
           02 MPMADAPNL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPMADAPNF      PIC X.                                     00001860
           02 FILLER    PIC X(4).                                       00001870
           02 MPMADAPNI      PIC X(5).                                  00001880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPMNL     COMP PIC S9(4).                                 00001890
      *--                                                                       
           02 MPMNL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MPMNF     PIC X.                                          00001900
           02 FILLER    PIC X(4).                                       00001910
           02 MPMNI     PIC X(5).                                       00001920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPMDNL    COMP PIC S9(4).                                 00001930
      *--                                                                       
           02 MPMDNL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPMDNF    PIC X.                                          00001940
           02 FILLER    PIC X(4).                                       00001950
           02 MPMDNI    PIC X(8).                                       00001960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPXFNL    COMP PIC S9(4).                                 00001970
      *--                                                                       
           02 MPXFNL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPXFNF    PIC X.                                          00001980
           02 FILLER    PIC X(4).                                       00001990
           02 MPXFNI    PIC X(8).                                       00002000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBNL    COMP PIC S9(4).                                 00002010
      *--                                                                       
           02 MLIBNL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIBNF    PIC X.                                          00002020
           02 FILLER    PIC X(4).                                       00002030
           02 MLIBNI    PIC X(20).                                      00002040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEDMSNL   COMP PIC S9(4).                                 00002050
      *--                                                                       
           02 MEDMSNL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MEDMSNF   PIC X.                                          00002060
           02 FILLER    PIC X(4).                                       00002070
           02 MEDMSNI   PIC X(5).                                       00002080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPMFNL    COMP PIC S9(4).                                 00002090
      *--                                                                       
           02 MPMFNL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPMFNF    PIC X.                                          00002100
           02 FILLER    PIC X(4).                                       00002110
           02 MPMFNI    PIC X(8).                                       00002120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLOCL     COMP PIC S9(4).                                 00002130
      *--                                                                       
           02 MLOCL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MLOCF     PIC X.                                          00002140
           02 FILLER    PIC X(4).                                       00002150
           02 MLOCI     PIC X(3).                                       00002160
           02 MSELD OCCURS   5 TIMES .                                  00002170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00002180
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00002190
             03 FILLER  PIC X(4).                                       00002200
             03 MSELI   PIC X.                                          00002210
           02 MLIEUD OCCURS   5 TIMES .                                 00002220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEUL  COMP PIC S9(4).                                 00002230
      *--                                                                       
             03 MLIEUL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLIEUF  PIC X.                                          00002240
             03 FILLER  PIC X(4).                                       00002250
             03 MLIEUI  PIC X(3).                                       00002260
           02 MPXD OCCURS   5 TIMES .                                   00002270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPXL    COMP PIC S9(4).                                 00002280
      *--                                                                       
             03 MPXL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MPXF    PIC X.                                          00002290
             03 FILLER  PIC X(4).                                       00002300
             03 MPXI    PIC X(8).                                       00002310
           02 MPXDD OCCURS   5 TIMES .                                  00002320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPXDL   COMP PIC S9(4).                                 00002330
      *--                                                                       
             03 MPXDL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MPXDF   PIC X.                                          00002340
             03 FILLER  PIC X(4).                                       00002350
             03 MPXDI   PIC X(8).                                       00002360
           02 MMBUD OCCURS   5 TIMES .                                  00002370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMBUL   COMP PIC S9(4).                                 00002380
      *--                                                                       
             03 MMBUL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MMBUF   PIC X.                                          00002390
             03 FILLER  PIC X(4).                                       00002400
             03 MMBUI   PIC X(8).                                       00002410
           02 MTMBUD OCCURS   5 TIMES .                                 00002420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTMBUL  COMP PIC S9(4).                                 00002430
      *--                                                                       
             03 MTMBUL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTMBUF  PIC X.                                          00002440
             03 FILLER  PIC X(4).                                       00002450
             03 MTMBUI  PIC X(5).                                       00002460
           02 MEDMD OCCURS   5 TIMES .                                  00002470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MEDML   COMP PIC S9(4).                                 00002480
      *--                                                                       
             03 MEDML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MEDMF   PIC X.                                          00002490
             03 FILLER  PIC X(4).                                       00002500
             03 MEDMI   PIC X(5).                                       00002510
           02 MPMADAPD OCCURS   5 TIMES .                               00002520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPMADAPL     COMP PIC S9(4).                            00002530
      *--                                                                       
             03 MPMADAPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPMADAPF     PIC X.                                     00002540
             03 FILLER  PIC X(4).                                       00002550
             03 MPMADAPI     PIC X(5).                                  00002560
           02 MPMD OCCURS   5 TIMES .                                   00002570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPML    COMP PIC S9(4).                                 00002580
      *--                                                                       
             03 MPML COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MPMF    PIC X.                                          00002590
             03 FILLER  PIC X(4).                                       00002600
             03 MPMI    PIC X(5).                                       00002610
           02 MPMDD OCCURS   5 TIMES .                                  00002620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPMDL   COMP PIC S9(4).                                 00002630
      *--                                                                       
             03 MPMDL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MPMDF   PIC X.                                          00002640
             03 FILLER  PIC X(4).                                       00002650
             03 MPMDI   PIC X(8).                                       00002660
           02 MSTOLOCD OCCURS   5 TIMES .                               00002670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTOLOCL     COMP PIC S9(4).                            00002680
      *--                                                                       
             03 MSTOLOCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTOLOCF     PIC X.                                     00002690
             03 FILLER  PIC X(4).                                       00002700
             03 MSTOLOCI     PIC X(5).                                  00002710
           02 MGRPD OCCURS   5 TIMES .                                  00002720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MGRPL   COMP PIC S9(4).                                 00002730
      *--                                                                       
             03 MGRPL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MGRPF   PIC X.                                          00002740
             03 FILLER  PIC X(4).                                       00002750
             03 MGRPI   PIC X.                                          00002760
           02 MLLIEUD OCCURS   5 TIMES .                                00002770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLLIEUL      COMP PIC S9(4).                            00002780
      *--                                                                       
             03 MLLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLLIEUF      PIC X.                                     00002790
             03 FILLER  PIC X(4).                                       00002800
             03 MLLIEUI      PIC X(10).                                 00002810
           02 MPXFD OCCURS   5 TIMES .                                  00002820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPXFL   COMP PIC S9(4).                                 00002830
      *--                                                                       
             03 MPXFL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MPXFF   PIC X.                                          00002840
             03 FILLER  PIC X(4).                                       00002850
             03 MPXFI   PIC X(8).                                       00002860
           02 MLIBD OCCURS   5 TIMES .                                  00002870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBL   COMP PIC S9(4).                                 00002880
      *--                                                                       
             03 MLIBL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MLIBF   PIC X.                                          00002890
             03 FILLER  PIC X(4).                                       00002900
             03 MLIBI   PIC X(20).                                      00002910
           02 MEDMSD OCCURS   5 TIMES .                                 00002920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MEDMSL  COMP PIC S9(4).                                 00002930
      *--                                                                       
             03 MEDMSL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MEDMSF  PIC X.                                          00002940
             03 FILLER  PIC X(4).                                       00002950
             03 MEDMSI  PIC X(5).                                       00002960
           02 MADPD OCCURS   5 TIMES .                                  00002970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MADPL   COMP PIC S9(4).                                 00002980
      *--                                                                       
             03 MADPL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MADPF   PIC X.                                          00002990
             03 FILLER  PIC X(4).                                       00003000
             03 MADPI   PIC X(5).                                       00003010
           02 MPMFD OCCURS   5 TIMES .                                  00003020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPMFL   COMP PIC S9(4).                                 00003030
      *--                                                                       
             03 MPMFL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MPMFF   PIC X.                                          00003040
             03 FILLER  PIC X(4).                                       00003050
             03 MPMFI   PIC X(8).                                       00003060
           02 MV4SLOCD OCCURS   5 TIMES .                               00003070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MV4SLOCL     COMP PIC S9(4).                            00003080
      *--                                                                       
             03 MV4SLOCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MV4SLOCF     PIC X.                                     00003090
             03 FILLER  PIC X(4).                                       00003100
             03 MV4SLOCI     PIC X(5).                                  00003110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00003120
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00003130
           02 FILLER    PIC X(4).                                       00003140
           02 MLIBERRI  PIC X(79).                                      00003150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00003160
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00003170
           02 FILLER    PIC X(4).                                       00003180
           02 MCODTRAI  PIC X(4).                                       00003190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00003200
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00003210
           02 FILLER    PIC X(4).                                       00003220
           02 MCICSI    PIC X(5).                                       00003230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00003240
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00003250
           02 FILLER    PIC X(4).                                       00003260
           02 MNETNAMI  PIC X(8).                                       00003270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00003280
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00003290
           02 FILLER    PIC X(4).                                       00003300
           02 MSCREENI  PIC X(5).                                       00003310
      ***************************************************************** 00003320
      * MAJ DES PRIX ARTICLES ET PRIMES                                 00003330
      ***************************************************************** 00003340
       01   ECB10O REDEFINES ECB10I.                                    00003350
           02 FILLER    PIC X(12).                                      00003360
           02 FILLER    PIC X(2).                                       00003370
           02 MDATJOUA  PIC X.                                          00003380
           02 MDATJOUC  PIC X.                                          00003390
           02 MDATJOUP  PIC X.                                          00003400
           02 MDATJOUH  PIC X.                                          00003410
           02 MDATJOUV  PIC X.                                          00003420
           02 MDATJOUO  PIC X(8).                                       00003430
           02 FILLER    PIC X(2).                                       00003440
           02 MTIMJOUA  PIC X.                                          00003450
           02 MTIMJOUC  PIC X.                                          00003460
           02 MTIMJOUP  PIC X.                                          00003470
           02 MTIMJOUH  PIC X.                                          00003480
           02 MTIMJOUV  PIC X.                                          00003490
           02 MTIMJOUO  PIC X(5).                                       00003500
           02 FILLER    PIC X(2).                                       00003510
           02 MPAGINEA  PIC X.                                          00003520
           02 MPAGINEC  PIC X.                                          00003530
           02 MPAGINEP  PIC X.                                          00003540
           02 MPAGINEH  PIC X.                                          00003550
           02 MPAGINEV  PIC X.                                          00003560
           02 MPAGINEO  PIC X(7).                                       00003570
           02 FILLER    PIC X(2).                                       00003580
           02 MNCODICA  PIC X.                                          00003590
           02 MNCODICC  PIC X.                                          00003600
           02 MNCODICP  PIC X.                                          00003610
           02 MNCODICH  PIC X.                                          00003620
           02 MNCODICV  PIC X.                                          00003630
           02 MNCODICO  PIC X(7).                                       00003640
           02 FILLER    PIC X(2).                                       00003650
           02 MLREFA    PIC X.                                          00003660
           02 MLREFC    PIC X.                                          00003670
           02 MLREFP    PIC X.                                          00003680
           02 MLREFH    PIC X.                                          00003690
           02 MLREFV    PIC X.                                          00003700
           02 MLREFO    PIC X(20).                                      00003710
           02 FILLER    PIC X(2).                                       00003720
           02 MCFAMA    PIC X.                                          00003730
           02 MCFAMC    PIC X.                                          00003740
           02 MCFAMP    PIC X.                                          00003750
           02 MCFAMH    PIC X.                                          00003760
           02 MCFAMV    PIC X.                                          00003770
           02 MCFAMO    PIC X(5).                                       00003780
           02 FILLER    PIC X(2).                                       00003790
           02 MLFAMA    PIC X.                                          00003800
           02 MLFAMC    PIC X.                                          00003810
           02 MLFAMP    PIC X.                                          00003820
           02 MLFAMH    PIC X.                                          00003830
           02 MLFAMV    PIC X.                                          00003840
           02 MLFAMO    PIC X(20).                                      00003850
           02 DFHMS1 OCCURS   3 TIMES .                                 00003860
             03 FILLER       PIC X(2).                                  00003870
             03 MDEPOTA      PIC X.                                     00003880
             03 MDEPOTC PIC X.                                          00003890
             03 MDEPOTP PIC X.                                          00003900
             03 MDEPOTH PIC X.                                          00003910
             03 MDEPOTV PIC X.                                          00003920
             03 MDEPOTO      PIC X(6).                                  00003930
           02 DFHMS2 OCCURS   3 TIMES .                                 00003940
             03 FILLER       PIC X(2).                                  00003950
             03 MCAPPROA     PIC X.                                     00003960
             03 MCAPPROC     PIC X.                                     00003970
             03 MCAPPROP     PIC X.                                     00003980
             03 MCAPPROH     PIC X.                                     00003990
             03 MCAPPROV     PIC X.                                     00004000
             03 MCAPPROO     PIC X(5).                                  00004010
           02 DFHMS3 OCCURS   3 TIMES .                                 00004020
             03 FILLER       PIC X(2).                                  00004030
             03 MCEXPOA      PIC X.                                     00004040
             03 MCEXPOC PIC X.                                          00004050
             03 MCEXPOP PIC X.                                          00004060
             03 MCEXPOH PIC X.                                          00004070
             03 MCEXPOV PIC X.                                          00004080
             03 MCEXPOO      PIC X(5).                                  00004090
           02 DFHMS4 OCCURS   3 TIMES .                                 00004100
             03 FILLER       PIC X(2).                                  00004110
             03 MSTATA  PIC X.                                          00004120
             03 MSTATC  PIC X.                                          00004130
             03 MSTATP  PIC X.                                          00004140
             03 MSTATH  PIC X.                                          00004150
             03 MSTATV  PIC X.                                          00004160
             03 MSTATO  PIC X(3).                                       00004170
           02 DFHMS5 OCCURS   3 TIMES .                                 00004180
             03 FILLER       PIC X(2).                                  00004190
             03 MSTODEPA     PIC X.                                     00004200
             03 MSTODEPC     PIC X.                                     00004210
             03 MSTODEPP     PIC X.                                     00004220
             03 MSTODEPH     PIC X.                                     00004230
             03 MSTODEPV     PIC X.                                     00004240
             03 MSTODEPO     PIC X(5).                                  00004250
           02 DFHMS6 OCCURS   3 TIMES .                                 00004260
             03 FILLER       PIC X(2).                                  00004270
             03 MCDEA   PIC X.                                          00004280
             03 MCDEC   PIC X.                                          00004290
             03 MCDEP   PIC X.                                          00004300
             03 MCDEH   PIC X.                                          00004310
             03 MCDEV   PIC X.                                          00004320
             03 MCDEO   PIC X(5).                                       00004330
           02 DFHMS7 OCCURS   3 TIMES .                                 00004340
             03 FILLER       PIC X(2).                                  00004350
             03 MCDEDA  PIC X.                                          00004360
             03 MCDEDC  PIC X.                                          00004370
             03 MCDEDP  PIC X.                                          00004380
             03 MCDEDH  PIC X.                                          00004390
             03 MCDEDV  PIC X.                                          00004400
             03 MCDEDO  PIC X(8).                                       00004410
           02 FILLER    PIC X(2).                                       00004420
           02 MCMARQA   PIC X.                                          00004430
           02 MCMARQC   PIC X.                                          00004440
           02 MCMARQP   PIC X.                                          00004450
           02 MCMARQH   PIC X.                                          00004460
           02 MCMARQV   PIC X.                                          00004470
           02 MCMARQO   PIC X(5).                                       00004480
           02 FILLER    PIC X(2).                                       00004490
           02 MLMARQA   PIC X.                                          00004500
           02 MLMARQC   PIC X.                                          00004510
           02 MLMARQP   PIC X.                                          00004520
           02 MLMARQH   PIC X.                                          00004530
           02 MLMARQV   PIC X.                                          00004540
           02 MLMARQO   PIC X(20).                                      00004550
           02 FILLER    PIC X(2).                                       00004560
           02 MSTATUTA  PIC X.                                          00004570
           02 MSTATUTC  PIC X.                                          00004580
           02 MSTATUTP  PIC X.                                          00004590
           02 MSTATUTH  PIC X.                                          00004600
           02 MSTATUTV  PIC X.                                          00004610
           02 MSTATUTO  PIC X(5).                                       00004620
           02 FILLER    PIC X(2).                                       00004630
           02 MSAPPA    PIC X.                                          00004640
           02 MSAPPC    PIC X.                                          00004650
           02 MSAPPP    PIC X.                                          00004660
           02 MSAPPH    PIC X.                                          00004670
           02 MSAPPV    PIC X.                                          00004680
           02 MSAPPO    PIC X.                                          00004690
           02 FILLER    PIC X(2).                                       00004700
           02 MDSTATUTA      PIC X.                                     00004710
           02 MDSTATUTC PIC X.                                          00004720
           02 MDSTATUTP PIC X.                                          00004730
           02 MDSTATUTH PIC X.                                          00004740
           02 MDSTATUTV PIC X.                                          00004750
           02 MDSTATUTO      PIC X(8).                                  00004760
           02 FILLER    PIC X(2).                                       00004770
           02 MPXREFA   PIC X.                                          00004780
           02 MPXREFC   PIC X.                                          00004790
           02 MPXREFP   PIC X.                                          00004800
           02 MPXREFH   PIC X.                                          00004810
           02 MPXREFV   PIC X.                                          00004820
           02 MPXREFO   PIC X(8).                                       00004830
           02 FILLER    PIC X(2).                                       00004840
           02 MPXREFDA  PIC X.                                          00004850
           02 MPXREFDC  PIC X.                                          00004860
           02 MPXREFDP  PIC X.                                          00004870
           02 MPXREFDH  PIC X.                                          00004880
           02 MPXREFDV  PIC X.                                          00004890
           02 MPXREFDO  PIC X(8).                                       00004900
           02 FILLER    PIC X(2).                                       00004910
           02 MMBURA    PIC X.                                          00004920
           02 MMBURC    PIC X.                                          00004930
           02 MMBURP    PIC X.                                          00004940
           02 MMBURH    PIC X.                                          00004950
           02 MMBURV    PIC X.                                          00004960
           02 MMBURO    PIC X(8).                                       00004970
           02 FILLER    PIC X(2).                                       00004980
           02 MTMBURA   PIC X.                                          00004990
           02 MTMBURC   PIC X.                                          00005000
           02 MTMBURP   PIC X.                                          00005010
           02 MTMBURH   PIC X.                                          00005020
           02 MTMBURV   PIC X.                                          00005030
           02 MTMBURO   PIC X(5).                                       00005040
           02 FILLER    PIC X(2).                                       00005050
           02 MEDMRA    PIC X.                                          00005060
           02 MEDMRC    PIC X.                                          00005070
           02 MEDMRP    PIC X.                                          00005080
           02 MEDMRH    PIC X.                                          00005090
           02 MEDMRV    PIC X.                                          00005100
           02 MEDMRO    PIC X(5).                                       00005110
           02 FILLER    PIC X(2).                                       00005120
           02 MPMADPRA  PIC X.                                          00005130
           02 MPMADPRC  PIC X.                                          00005140
           02 MPMADPRP  PIC X.                                          00005150
           02 MPMADPRH  PIC X.                                          00005160
           02 MPMADPRV  PIC X.                                          00005170
           02 MPMADPRO  PIC X(5).                                       00005180
           02 FILLER    PIC X(2).                                       00005190
           02 MPMREFA   PIC X.                                          00005200
           02 MPMREFC   PIC X.                                          00005210
           02 MPMREFP   PIC X.                                          00005220
           02 MPMREFH   PIC X.                                          00005230
           02 MPMREFV   PIC X.                                          00005240
           02 MPMREFO   PIC X(5).                                       00005250
           02 FILLER    PIC X(2).                                       00005260
           02 MPMREFDA  PIC X.                                          00005270
           02 MPMREFDC  PIC X.                                          00005280
           02 MPMREFDP  PIC X.                                          00005290
           02 MPMREFDH  PIC X.                                          00005300
           02 MPMREFDV  PIC X.                                          00005310
           02 MPMREFDO  PIC X(8).                                       00005320
           02 FILLER    PIC X(2).                                       00005330
           02 MOANATA   PIC X.                                          00005340
           02 MOANATC   PIC X.                                          00005350
           02 MOANATP   PIC X.                                          00005360
           02 MOANATH   PIC X.                                          00005370
           02 MOANATV   PIC X.                                          00005380
           02 MOANATO   PIC X.                                          00005390
           02 FILLER    PIC X(2).                                       00005400
           02 MOANATDA  PIC X.                                          00005410
           02 MOANATDC  PIC X.                                          00005420
           02 MOANATDP  PIC X.                                          00005430
           02 MOANATDH  PIC X.                                          00005440
           02 MOANATDV  PIC X.                                          00005450
           02 MOANATDO  PIC X(8).                                       00005460
           02 FILLER    PIC X(2).                                       00005470
           02 MPXCESSA  PIC X.                                          00005480
           02 MPXCESSC  PIC X.                                          00005490
           02 MPXCESSP  PIC X.                                          00005500
           02 MPXCESSH  PIC X.                                          00005510
           02 MPXCESSV  PIC X.                                          00005520
           02 MPXCESSO  PIC X(8).                                       00005530
           02 FILLER    PIC X(2).                                       00005540
           02 MSRPA     PIC X.                                          00005550
           02 MSRPC     PIC X.                                          00005560
           02 MSRPP     PIC X.                                          00005570
           02 MSRPH     PIC X.                                          00005580
           02 MSRPV     PIC X.                                          00005590
           02 MSRPO     PIC X(8).                                       00005600
           02 FILLER    PIC X(2).                                       00005610
           02 MPRAA     PIC X.                                          00005620
           02 MPRAC     PIC X.                                          00005630
           02 MPRAP     PIC X.                                          00005640
           02 MPRAH     PIC X.                                          00005650
           02 MPRAV     PIC X.                                          00005660
           02 MPRAO     PIC X(8).                                       00005670
           02 FILLER    PIC X(2).                                       00005680
           02 MPRMPA    PIC X.                                          00005690
           02 MPRMPC    PIC X.                                          00005700
           02 MPRMPP    PIC X.                                          00005710
           02 MPRMPH    PIC X.                                          00005720
           02 MPRMPV    PIC X.                                          00005730
           02 MPRMPO    PIC X(8).                                       00005740
           02 FILLER    PIC X(2).                                       00005750
           02 MLIBRA    PIC X.                                          00005760
           02 MLIBRC    PIC X.                                          00005770
           02 MLIBRP    PIC X.                                          00005780
           02 MLIBRH    PIC X.                                          00005790
           02 MLIBRV    PIC X.                                          00005800
           02 MLIBRO    PIC X(20).                                      00005810
           02 FILLER    PIC X(2).                                       00005820
           02 MEDMSRA   PIC X.                                          00005830
           02 MEDMSRC   PIC X.                                          00005840
           02 MEDMSRP   PIC X.                                          00005850
           02 MEDMSRH   PIC X.                                          00005860
           02 MEDMSRV   PIC X.                                          00005870
           02 MEDMSRO   PIC X(5).                                       00005880
           02 FILLER    PIC X(2).                                       00005890
           02 MSELNA    PIC X.                                          00005900
           02 MSELNC    PIC X.                                          00005910
           02 MSELNP    PIC X.                                          00005920
           02 MSELNH    PIC X.                                          00005930
           02 MSELNV    PIC X.                                          00005940
           02 MSELNO    PIC X.                                          00005950
           02 FILLER    PIC X(2).                                       00005960
           02 MNCONCNA  PIC X.                                          00005970
           02 MNCONCNC  PIC X.                                          00005980
           02 MNCONCNP  PIC X.                                          00005990
           02 MNCONCNH  PIC X.                                          00006000
           02 MNCONCNV  PIC X.                                          00006010
           02 MNCONCNO  PIC X(4).                                       00006020
           02 FILLER    PIC X(2).                                       00006030
           02 MLIEUNA   PIC X.                                          00006040
           02 MLIEUNC   PIC X.                                          00006050
           02 MLIEUNP   PIC X.                                          00006060
           02 MLIEUNH   PIC X.                                          00006070
           02 MLIEUNV   PIC X.                                          00006080
           02 MLIEUNO   PIC X(3).                                       00006090
           02 FILLER    PIC X(2).                                       00006100
           02 MPXNA     PIC X.                                          00006110
           02 MPXNC     PIC X.                                          00006120
           02 MPXNP     PIC X.                                          00006130
           02 MPXNH     PIC X.                                          00006140
           02 MPXNV     PIC X.                                          00006150
           02 MPXNO     PIC X(8).                                       00006160
           02 FILLER    PIC X(2).                                       00006170
           02 MPXDNA    PIC X.                                          00006180
           02 MPXDNC    PIC X.                                          00006190
           02 MPXDNP    PIC X.                                          00006200
           02 MPXDNH    PIC X.                                          00006210
           02 MPXDNV    PIC X.                                          00006220
           02 MPXDNO    PIC X(8).                                       00006230
           02 FILLER    PIC X(2).                                       00006240
           02 MMBUNA    PIC X.                                          00006250
           02 MMBUNC    PIC X.                                          00006260
           02 MMBUNP    PIC X.                                          00006270
           02 MMBUNH    PIC X.                                          00006280
           02 MMBUNV    PIC X.                                          00006290
           02 MMBUNO    PIC X(8).                                       00006300
           02 FILLER    PIC X(2).                                       00006310
           02 MTMBUNA   PIC X.                                          00006320
           02 MTMBUNC   PIC X.                                          00006330
           02 MTMBUNP   PIC X.                                          00006340
           02 MTMBUNH   PIC X.                                          00006350
           02 MTMBUNV   PIC X.                                          00006360
           02 MTMBUNO   PIC X(5).                                       00006370
           02 FILLER    PIC X(2).                                       00006380
           02 MEDMNA    PIC X.                                          00006390
           02 MEDMNC    PIC X.                                          00006400
           02 MEDMNP    PIC X.                                          00006410
           02 MEDMNH    PIC X.                                          00006420
           02 MEDMNV    PIC X.                                          00006430
           02 MEDMNO    PIC X(5).                                       00006440
           02 FILLER    PIC X(2).                                       00006450
           02 MPMADAPNA      PIC X.                                     00006460
           02 MPMADAPNC PIC X.                                          00006470
           02 MPMADAPNP PIC X.                                          00006480
           02 MPMADAPNH PIC X.                                          00006490
           02 MPMADAPNV PIC X.                                          00006500
           02 MPMADAPNO      PIC X(5).                                  00006510
           02 FILLER    PIC X(2).                                       00006520
           02 MPMNA     PIC X.                                          00006530
           02 MPMNC     PIC X.                                          00006540
           02 MPMNP     PIC X.                                          00006550
           02 MPMNH     PIC X.                                          00006560
           02 MPMNV     PIC X.                                          00006570
           02 MPMNO     PIC X(5).                                       00006580
           02 FILLER    PIC X(2).                                       00006590
           02 MPMDNA    PIC X.                                          00006600
           02 MPMDNC    PIC X.                                          00006610
           02 MPMDNP    PIC X.                                          00006620
           02 MPMDNH    PIC X.                                          00006630
           02 MPMDNV    PIC X.                                          00006640
           02 MPMDNO    PIC X(8).                                       00006650
           02 FILLER    PIC X(2).                                       00006660
           02 MPXFNA    PIC X.                                          00006670
           02 MPXFNC    PIC X.                                          00006680
           02 MPXFNP    PIC X.                                          00006690
           02 MPXFNH    PIC X.                                          00006700
           02 MPXFNV    PIC X.                                          00006710
           02 MPXFNO    PIC X(8).                                       00006720
           02 FILLER    PIC X(2).                                       00006730
           02 MLIBNA    PIC X.                                          00006740
           02 MLIBNC    PIC X.                                          00006750
           02 MLIBNP    PIC X.                                          00006760
           02 MLIBNH    PIC X.                                          00006770
           02 MLIBNV    PIC X.                                          00006780
           02 MLIBNO    PIC X(20).                                      00006790
           02 FILLER    PIC X(2).                                       00006800
           02 MEDMSNA   PIC X.                                          00006810
           02 MEDMSNC   PIC X.                                          00006820
           02 MEDMSNP   PIC X.                                          00006830
           02 MEDMSNH   PIC X.                                          00006840
           02 MEDMSNV   PIC X.                                          00006850
           02 MEDMSNO   PIC X(5).                                       00006860
           02 FILLER    PIC X(2).                                       00006870
           02 MPMFNA    PIC X.                                          00006880
           02 MPMFNC    PIC X.                                          00006890
           02 MPMFNP    PIC X.                                          00006900
           02 MPMFNH    PIC X.                                          00006910
           02 MPMFNV    PIC X.                                          00006920
           02 MPMFNO    PIC X(8).                                       00006930
           02 FILLER    PIC X(2).                                       00006940
           02 MLOCA     PIC X.                                          00006950
           02 MLOCC     PIC X.                                          00006960
           02 MLOCP     PIC X.                                          00006970
           02 MLOCH     PIC X.                                          00006980
           02 MLOCV     PIC X.                                          00006990
           02 MLOCO     PIC X(3).                                       00007000
           02 DFHMS8 OCCURS   5 TIMES .                                 00007010
             03 FILLER       PIC X(2).                                  00007020
             03 MSELA   PIC X.                                          00007030
             03 MSELC   PIC X.                                          00007040
             03 MSELP   PIC X.                                          00007050
             03 MSELH   PIC X.                                          00007060
             03 MSELV   PIC X.                                          00007070
             03 MSELO   PIC X.                                          00007080
           02 DFHMS9 OCCURS   5 TIMES .                                 00007090
             03 FILLER       PIC X(2).                                  00007100
             03 MLIEUA  PIC X.                                          00007110
             03 MLIEUC  PIC X.                                          00007120
             03 MLIEUP  PIC X.                                          00007130
             03 MLIEUH  PIC X.                                          00007140
             03 MLIEUV  PIC X.                                          00007150
             03 MLIEUO  PIC X(3).                                       00007160
           02 DFHMS10 OCCURS   5 TIMES .                                00007170
             03 FILLER       PIC X(2).                                  00007180
             03 MPXA    PIC X.                                          00007190
             03 MPXC    PIC X.                                          00007200
             03 MPXP    PIC X.                                          00007210
             03 MPXH    PIC X.                                          00007220
             03 MPXV    PIC X.                                          00007230
             03 MPXO    PIC X(8).                                       00007240
           02 DFHMS11 OCCURS   5 TIMES .                                00007250
             03 FILLER       PIC X(2).                                  00007260
             03 MPXDA   PIC X.                                          00007270
             03 MPXDC   PIC X.                                          00007280
             03 MPXDP   PIC X.                                          00007290
             03 MPXDH   PIC X.                                          00007300
             03 MPXDV   PIC X.                                          00007310
             03 MPXDO   PIC X(8).                                       00007320
           02 DFHMS12 OCCURS   5 TIMES .                                00007330
             03 FILLER       PIC X(2).                                  00007340
             03 MMBUA   PIC X.                                          00007350
             03 MMBUC   PIC X.                                          00007360
             03 MMBUP   PIC X.                                          00007370
             03 MMBUH   PIC X.                                          00007380
             03 MMBUV   PIC X.                                          00007390
             03 MMBUO   PIC X(8).                                       00007400
           02 DFHMS13 OCCURS   5 TIMES .                                00007410
             03 FILLER       PIC X(2).                                  00007420
             03 MTMBUA  PIC X.                                          00007430
             03 MTMBUC  PIC X.                                          00007440
             03 MTMBUP  PIC X.                                          00007450
             03 MTMBUH  PIC X.                                          00007460
             03 MTMBUV  PIC X.                                          00007470
             03 MTMBUO  PIC X(5).                                       00007480
           02 DFHMS14 OCCURS   5 TIMES .                                00007490
             03 FILLER       PIC X(2).                                  00007500
             03 MEDMA   PIC X.                                          00007510
             03 MEDMC   PIC X.                                          00007520
             03 MEDMP   PIC X.                                          00007530
             03 MEDMH   PIC X.                                          00007540
             03 MEDMV   PIC X.                                          00007550
             03 MEDMO   PIC X(5).                                       00007560
           02 DFHMS15 OCCURS   5 TIMES .                                00007570
             03 FILLER       PIC X(2).                                  00007580
             03 MPMADAPA     PIC X.                                     00007590
             03 MPMADAPC     PIC X.                                     00007600
             03 MPMADAPP     PIC X.                                     00007610
             03 MPMADAPH     PIC X.                                     00007620
             03 MPMADAPV     PIC X.                                     00007630
             03 MPMADAPO     PIC X(5).                                  00007640
           02 DFHMS16 OCCURS   5 TIMES .                                00007650
             03 FILLER       PIC X(2).                                  00007660
             03 MPMA    PIC X.                                          00007670
             03 MPMC    PIC X.                                          00007680
             03 MPMP    PIC X.                                          00007690
             03 MPMH    PIC X.                                          00007700
             03 MPMV    PIC X.                                          00007710
             03 MPMO    PIC X(5).                                       00007720
           02 DFHMS17 OCCURS   5 TIMES .                                00007730
             03 FILLER       PIC X(2).                                  00007740
             03 MPMDA   PIC X.                                          00007750
             03 MPMDC   PIC X.                                          00007760
             03 MPMDP   PIC X.                                          00007770
             03 MPMDH   PIC X.                                          00007780
             03 MPMDV   PIC X.                                          00007790
             03 MPMDO   PIC X(8).                                       00007800
           02 DFHMS18 OCCURS   5 TIMES .                                00007810
             03 FILLER       PIC X(2).                                  00007820
             03 MSTOLOCA     PIC X.                                     00007830
             03 MSTOLOCC     PIC X.                                     00007840
             03 MSTOLOCP     PIC X.                                     00007850
             03 MSTOLOCH     PIC X.                                     00007860
             03 MSTOLOCV     PIC X.                                     00007870
             03 MSTOLOCO     PIC X(5).                                  00007880
           02 DFHMS19 OCCURS   5 TIMES .                                00007890
             03 FILLER       PIC X(2).                                  00007900
             03 MGRPA   PIC X.                                          00007910
             03 MGRPC   PIC X.                                          00007920
             03 MGRPP   PIC X.                                          00007930
             03 MGRPH   PIC X.                                          00007940
             03 MGRPV   PIC X.                                          00007950
             03 MGRPO   PIC X.                                          00007960
           02 DFHMS20 OCCURS   5 TIMES .                                00007970
             03 FILLER       PIC X(2).                                  00007980
             03 MLLIEUA      PIC X.                                     00007990
             03 MLLIEUC PIC X.                                          00008000
             03 MLLIEUP PIC X.                                          00008010
             03 MLLIEUH PIC X.                                          00008020
             03 MLLIEUV PIC X.                                          00008030
             03 MLLIEUO      PIC X(10).                                 00008040
           02 DFHMS21 OCCURS   5 TIMES .                                00008050
             03 FILLER       PIC X(2).                                  00008060
             03 MPXFA   PIC X.                                          00008070
             03 MPXFC   PIC X.                                          00008080
             03 MPXFP   PIC X.                                          00008090
             03 MPXFH   PIC X.                                          00008100
             03 MPXFV   PIC X.                                          00008110
             03 MPXFO   PIC X(8).                                       00008120
           02 DFHMS22 OCCURS   5 TIMES .                                00008130
             03 FILLER       PIC X(2).                                  00008140
             03 MLIBA   PIC X.                                          00008150
             03 MLIBC   PIC X.                                          00008160
             03 MLIBP   PIC X.                                          00008170
             03 MLIBH   PIC X.                                          00008180
             03 MLIBV   PIC X.                                          00008190
             03 MLIBO   PIC X(20).                                      00008200
           02 DFHMS23 OCCURS   5 TIMES .                                00008210
             03 FILLER       PIC X(2).                                  00008220
             03 MEDMSA  PIC X.                                          00008230
             03 MEDMSC  PIC X.                                          00008240
             03 MEDMSP  PIC X.                                          00008250
             03 MEDMSH  PIC X.                                          00008260
             03 MEDMSV  PIC X.                                          00008270
             03 MEDMSO  PIC X(5).                                       00008280
           02 DFHMS24 OCCURS   5 TIMES .                                00008290
             03 FILLER       PIC X(2).                                  00008300
             03 MADPA   PIC X.                                          00008310
             03 MADPC   PIC X.                                          00008320
             03 MADPP   PIC X.                                          00008330
             03 MADPH   PIC X.                                          00008340
             03 MADPV   PIC X.                                          00008350
             03 MADPO   PIC X(5).                                       00008360
           02 DFHMS25 OCCURS   5 TIMES .                                00008370
             03 FILLER       PIC X(2).                                  00008380
             03 MPMFA   PIC X.                                          00008390
             03 MPMFC   PIC X.                                          00008400
             03 MPMFP   PIC X.                                          00008410
             03 MPMFH   PIC X.                                          00008420
             03 MPMFV   PIC X.                                          00008430
             03 MPMFO   PIC X(8).                                       00008440
           02 DFHMS26 OCCURS   5 TIMES .                                00008450
             03 FILLER       PIC X(2).                                  00008460
             03 MV4SLOCA     PIC X.                                     00008470
             03 MV4SLOCC     PIC X.                                     00008480
             03 MV4SLOCP     PIC X.                                     00008490
             03 MV4SLOCH     PIC X.                                     00008500
             03 MV4SLOCV     PIC X.                                     00008510
             03 MV4SLOCO     PIC X(5).                                  00008520
           02 FILLER    PIC X(2).                                       00008530
           02 MLIBERRA  PIC X.                                          00008540
           02 MLIBERRC  PIC X.                                          00008550
           02 MLIBERRP  PIC X.                                          00008560
           02 MLIBERRH  PIC X.                                          00008570
           02 MLIBERRV  PIC X.                                          00008580
           02 MLIBERRO  PIC X(79).                                      00008590
           02 FILLER    PIC X(2).                                       00008600
           02 MCODTRAA  PIC X.                                          00008610
           02 MCODTRAC  PIC X.                                          00008620
           02 MCODTRAP  PIC X.                                          00008630
           02 MCODTRAH  PIC X.                                          00008640
           02 MCODTRAV  PIC X.                                          00008650
           02 MCODTRAO  PIC X(4).                                       00008660
           02 FILLER    PIC X(2).                                       00008670
           02 MCICSA    PIC X.                                          00008680
           02 MCICSC    PIC X.                                          00008690
           02 MCICSP    PIC X.                                          00008700
           02 MCICSH    PIC X.                                          00008710
           02 MCICSV    PIC X.                                          00008720
           02 MCICSO    PIC X(5).                                       00008730
           02 FILLER    PIC X(2).                                       00008740
           02 MNETNAMA  PIC X.                                          00008750
           02 MNETNAMC  PIC X.                                          00008760
           02 MNETNAMP  PIC X.                                          00008770
           02 MNETNAMH  PIC X.                                          00008780
           02 MNETNAMV  PIC X.                                          00008790
           02 MNETNAMO  PIC X(8).                                       00008800
           02 FILLER    PIC X(2).                                       00008810
           02 MSCREENA  PIC X.                                          00008820
           02 MSCREENC  PIC X.                                          00008830
           02 MSCREENP  PIC X.                                          00008840
           02 MSCREENH  PIC X.                                          00008850
           02 MSCREENV  PIC X.                                          00008860
           02 MSCREENO  PIC X(5).                                       00008870
                                                                                
