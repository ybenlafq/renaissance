      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRX015 AU 02/12/2004  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,03,PD,A,                          *        
      *                           18,20,BI,A,                          *        
      *                           38,20,BI,A,                          *        
      *                           58,20,BI,A,                          *        
      *                           78,05,BI,A,                          *        
      *                           83,07,BI,A,                          *        
      *                           90,20,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                           12,05,PD,A,                          *        
      *                           17,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRX015.                                                        
            05 NOMETAT-IRX015           PIC X(6) VALUE 'IRX015'.                
            05 RUPTURES-IRX015.                                                 
           10 IRX015-NSOCIETE           PIC X(03).                      007  003
           10 IRX015-CHEFPROD           PIC X(05).                      010  005
           10 IRX015-WSEQFAM            PIC S9(05)      COMP-3.         015  003
           10 IRX015-LVMARKET1          PIC X(20).                      018  020
           10 IRX015-LVMARKET2          PIC X(20).                      038  020
           10 IRX015-LVMARKET3          PIC X(20).                      058  020
           10 IRX015-MARQREF            PIC X(05).                      078  005
           10 IRX015-MARQGR             PIC X(07).                      083  007
           10 IRX015-LREFFOURN          PIC X(20).                      090  020
           10 IRX015-NZONPRIX           PIC X(02).                      110  002
           10 IRX015-PVCONC             PIC S9(07)V9(2) COMP-3.         112  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRX015-SEQUENCE           PIC S9(04) COMP.                117  002
      *--                                                                       
           10 IRX015-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRX015.                                                   
           10 IRX015-CFAM               PIC X(05).                      119  005
           10 IRX015-CMARQ              PIC X(05).                      124  005
           10 IRX015-COLIS              PIC X(30).                      129  030
           10 IRX015-CORIG              PIC X(01).                      159  001
           10 IRX015-DRELEVE            PIC X(04).                      160  004
           10 IRX015-ECART              PIC X(01).                      164  001
           10 IRX015-EDITCODE           PIC X(01).                      165  001
           10 IRX015-EDITD8             PIC X(01).                      166  001
           10 IRX015-EDITJOH            PIC X(01).                      167  001
           10 IRX015-FLAGEDITION        PIC X(01).                      168  001
           10 IRX015-FLAGPEX            PIC X(01).                      169  001
           10 IRX015-FOURNREF           PIC X(20).                      170  020
           10 IRX015-HISTORIQUE         PIC X(01).                      190  001
           10 IRX015-LCHEFPROD          PIC X(20).                      191  020
           10 IRX015-LCOMMENT           PIC X(19).                      211  019
           10 IRX015-LCONCPE            PIC X(20).                      230  020
           10 IRX015-LEMPCONC           PIC X(10).                      250  010
           10 IRX015-LENSCONC           PIC X(15).                      260  015
           10 IRX015-LFAM               PIC X(20).                      275  020
           10 IRX015-LIBELLE1           PIC X(26).                      295  026
           10 IRX015-LIBELLE2           PIC X(03).                      321  003
           10 IRX015-LIBELLE3           PIC X(05).                      324  005
           10 IRX015-LSTATCOMP          PIC X(03).                      329  003
           10 IRX015-NCODIC             PIC X(07).                      332  007
           10 IRX015-NCONCPE            PIC X(04).                      339  004
           10 IRX015-NMAGPE             PIC X(03).                      343  003
           10 IRX015-NONCODIC           PIC X(11).                      346  011
           10 IRX015-PEXPTTC            PIC S9(07)V9(2) COMP-3.         357  005
           10 IRX015-PVMAG              PIC S9(07)V9(2) COMP-3.         362  005
           10 IRX015-PVREF              PIC S9(07)V9(2) COMP-3.         367  005
           10 IRX015-SRP                PIC S9(07)V9(2) COMP-3.         372  005
           10 IRX015-DATEAU             PIC X(08).                      377  008
           10 IRX015-DATEDU             PIC X(08).                      385  008
           10 IRX015-DFINEFFETPE        PIC X(08).                      393  008
            05 FILLER                      PIC X(112).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IRX015-LONG           PIC S9(4)   COMP  VALUE +400.           
      *                                                                         
      *--                                                                       
        01  DSECT-IRX015-LONG           PIC S9(4) COMP-5  VALUE +400.           
                                                                                
      *}                                                                        
