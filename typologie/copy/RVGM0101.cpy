      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVGM0101                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGM0101                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGM0101.                                                    00000090
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGM0101.                                                            
      *}                                                                        
           02  GM01-DATETRAIT                                           00000100
               PIC X(0008).                                             00000110
           02  GM01-CHEFPROD                                            00000120
               PIC X(0005).                                             00000130
           02  GM01-CFAM                                                00000140
               PIC X(0005).                                             00000150
           02  GM01-WSEQFAM                                             00000160
               PIC X(0005).                                             00000170
           02  GM01-WTYPART                                             00000180
               PIC X(0001).                                             00000190
           02  GM01-NCODIC1                                             00000200
               PIC X(0007).                                             00000210
           02  GM01-NCODIC2                                             00000220
               PIC X(0007).                                             00000230
           02  GM01-CMARKETIN1                                          00000240
               PIC X(0005).                                             00000250
           02  GM01-CVMARKETIN1                                         00000260
               PIC X(0005).                                             00000270
           02  GM01-LVMARKETIN1                                         00000280
               PIC X(0020).                                             00000290
           02  GM01-CMARKETIN2                                          00000300
               PIC X(0005).                                             00000310
           02  GM01-CVMARKETIN2                                         00000320
               PIC X(0005).                                             00000330
           02  GM01-LVMARKETIN2                                         00000340
               PIC X(0020).                                             00000350
           02  GM01-CMARKETIN3                                          00000360
               PIC X(0005).                                             00000370
           02  GM01-CVMARKETIN3                                         00000380
               PIC X(0005).                                             00000390
           02  GM01-LVMARKETIN3                                         00000400
               PIC X(0020).                                             00000410
           02  GM01-QTRI                                                00000420
               PIC S9(7)V9(0002) COMP-3.                                00000430
           02  GM01-CMARQ                                               00000440
               PIC X(0005).                                             00000450
           02  GM01-LREFFOURN                                           00000460
               PIC X(0020).                                             00000470
           02  GM01-CFAMELE                                             00000480
               PIC X(0005).                                             00000490
           02  GM01-WLIBERTE                                            00000500
               PIC X(0001).                                             00000510
           02  GM01-CASSORT                                             00000520
               PIC X(0005).                                             00000530
           02  GM01-CAPPRO                                              00000540
               PIC X(0005).                                             00000550
           02  GM01-CEXPO                                               00000560
               PIC X(0005).                                             00000570
           02  GM01-STATCOMP                                            00000580
               PIC X(0003).                                             00000590
           02  GM01-WSENSVTE                                            00000600
               PIC X(0001).                                             00000610
           02  GM01-WSENSAPPRO                                          00000620
               PIC X(0001).                                             00000630
           02  GM01-DEFSTATUT                                           00000640
               PIC X(0008).                                             00000650
           02  GM01-WTLMELA                                             00000660
               PIC X(0001).                                             00000670
           02  GM01-CVDESCRIPT1                                         00000680
               PIC X(0005).                                             00000690
           02  GM01-CVDESCRIPT2                                         00000700
               PIC X(0005).                                             00000710
           02  GM01-CVDESCRIPT3                                         00000720
               PIC X(0005).                                             00000730
           02  GM01-QTXTVA                                              00000740
               PIC S9(3)V9(0002) COMP-3.                                00000750
           02  GM01-QDELAIAPPRO                                         00000760
               PIC S9(3) COMP-3.                                        00000770
           02  GM01-PRAR                                                00000780
               PIC S9(7)V9(0002) COMP-3.                                00000790
           02  GM01-PRMP                                                00000800
               PIC S9(7)V9(0002) COMP-3.                                00000810
           02  GM01-PMBUZ1                                              00000820
               PIC S9(7)V9(0002) COMP-3.                                00000830
           02  GM01-CZPRIXZ1                                            00000840
               PIC X(0002).                                             00000850
           02  GM01-PSTDTTCZ1                                           00000860
               PIC S9(7)V9(0002) COMP-3.                                00000870
           02  GM01-PCOMMZ1                                             00000880
               PIC S9(5) COMP-3.                                        00000890
           02  GM01-QNBREART                                            00000900
               PIC S9(7) COMP-3.                                        00000910
           02  GM01-QTYPLIEN                                            00000920
               PIC S9(3) COMP-3.                                        00000930
           02  GM01-QSTOCKDEP                                           00000940
               PIC S9(7) COMP-3.                                        00000950
           02  GM01-QSTOCKMAG                                           00000960
               PIC S9(7) COMP-3.                                        00000970
           02  GM01-DSYST                                               00000980
               PIC S9(13) COMP-3.                                       00000990
           02  GM01-PCOMMARTZ1                                          00001000
               PIC S9(5)V9(0002) COMP-3.                                00001010
           02  GM01-PCOMMVOLZ1                                          00001020
               PIC S9(5)V9(0002) COMP-3.                                00001030
           02  GM01-WSENSNF                                             00001040
               PIC X(0001).                                             00001050
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00001060
      *---------------------------------------------------------        00001070
      *   LISTE DES FLAGS DE LA TABLE RVGM0101                          00001080
      *---------------------------------------------------------        00001090
      *                                                                 00001100
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGM0101-FLAGS.                                              00001110
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGM0101-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-DATETRAIT-F                                         00001120
      *        PIC S9(4) COMP.                                          00001130
      *--                                                                       
           02  GM01-DATETRAIT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CHEFPROD-F                                          00001140
      *        PIC S9(4) COMP.                                          00001150
      *--                                                                       
           02  GM01-CHEFPROD-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CFAM-F                                              00001160
      *        PIC S9(4) COMP.                                          00001170
      *--                                                                       
           02  GM01-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-WSEQFAM-F                                           00001180
      *        PIC S9(4) COMP.                                          00001190
      *--                                                                       
           02  GM01-WSEQFAM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-WTYPART-F                                           00001200
      *        PIC S9(4) COMP.                                          00001210
      *--                                                                       
           02  GM01-WTYPART-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-NCODIC1-F                                           00001220
      *        PIC S9(4) COMP.                                          00001230
      *--                                                                       
           02  GM01-NCODIC1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-NCODIC2-F                                           00001240
      *        PIC S9(4) COMP.                                          00001250
      *--                                                                       
           02  GM01-NCODIC2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CMARKETIN1-F                                        00001260
      *        PIC S9(4) COMP.                                          00001270
      *--                                                                       
           02  GM01-CMARKETIN1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CVMARKETIN1-F                                       00001280
      *        PIC S9(4) COMP.                                          00001290
      *--                                                                       
           02  GM01-CVMARKETIN1-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-LVMARKETIN1-F                                       00001300
      *        PIC S9(4) COMP.                                          00001310
      *--                                                                       
           02  GM01-LVMARKETIN1-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CMARKETIN2-F                                        00001320
      *        PIC S9(4) COMP.                                          00001330
      *--                                                                       
           02  GM01-CMARKETIN2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CVMARKETIN2-F                                       00001340
      *        PIC S9(4) COMP.                                          00001350
      *--                                                                       
           02  GM01-CVMARKETIN2-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-LVMARKETIN2-F                                       00001360
      *        PIC S9(4) COMP.                                          00001370
      *--                                                                       
           02  GM01-LVMARKETIN2-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CMARKETIN3-F                                        00001380
      *        PIC S9(4) COMP.                                          00001390
      *--                                                                       
           02  GM01-CMARKETIN3-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CVMARKETIN3-F                                       00001400
      *        PIC S9(4) COMP.                                          00001410
      *--                                                                       
           02  GM01-CVMARKETIN3-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-LVMARKETIN3-F                                       00001420
      *        PIC S9(4) COMP.                                          00001430
      *--                                                                       
           02  GM01-LVMARKETIN3-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-QTRI-F                                              00001440
      *        PIC S9(4) COMP.                                          00001450
      *--                                                                       
           02  GM01-QTRI-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CMARQ-F                                             00001460
      *        PIC S9(4) COMP.                                          00001470
      *--                                                                       
           02  GM01-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-LREFFOURN-F                                         00001480
      *        PIC S9(4) COMP.                                          00001490
      *--                                                                       
           02  GM01-LREFFOURN-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CFAMELE-F                                           00001500
      *        PIC S9(4) COMP.                                          00001510
      *--                                                                       
           02  GM01-CFAMELE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-WLIBERTE-F                                          00001520
      *        PIC S9(4) COMP.                                          00001530
      *--                                                                       
           02  GM01-WLIBERTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CASSORT-F                                           00001540
      *        PIC S9(4) COMP.                                          00001550
      *--                                                                       
           02  GM01-CASSORT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CAPPRO-F                                            00001560
      *        PIC S9(4) COMP.                                          00001570
      *--                                                                       
           02  GM01-CAPPRO-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CEXPO-F                                             00001580
      *        PIC S9(4) COMP.                                          00001590
      *--                                                                       
           02  GM01-CEXPO-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-STATCOMP-F                                          00001600
      *        PIC S9(4) COMP.                                          00001610
      *--                                                                       
           02  GM01-STATCOMP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-WSENSVTE-F                                          00001620
      *        PIC S9(4) COMP.                                          00001630
      *--                                                                       
           02  GM01-WSENSVTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-WSENSAPPRO-F                                        00001640
      *        PIC S9(4) COMP.                                          00001650
      *--                                                                       
           02  GM01-WSENSAPPRO-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-DEFSTATUT-F                                         00001660
      *        PIC S9(4) COMP.                                          00001670
      *--                                                                       
           02  GM01-DEFSTATUT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-WTLMELA-F                                           00001680
      *        PIC S9(4) COMP.                                          00001690
      *--                                                                       
           02  GM01-WTLMELA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CVDESCRIPT1-F                                       00001700
      *        PIC S9(4) COMP.                                          00001710
      *--                                                                       
           02  GM01-CVDESCRIPT1-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CVDESCRIPT2-F                                       00001720
      *        PIC S9(4) COMP.                                          00001730
      *--                                                                       
           02  GM01-CVDESCRIPT2-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CVDESCRIPT3-F                                       00001740
      *        PIC S9(4) COMP.                                          00001750
      *--                                                                       
           02  GM01-CVDESCRIPT3-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-QTXTVA-F                                            00001760
      *        PIC S9(4) COMP.                                          00001770
      *--                                                                       
           02  GM01-QTXTVA-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-QDELAIAPPRO-F                                       00001780
      *        PIC S9(4) COMP.                                          00001790
      *--                                                                       
           02  GM01-QDELAIAPPRO-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-PRAR-F                                              00001800
      *        PIC S9(4) COMP.                                          00001810
      *--                                                                       
           02  GM01-PRAR-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-PRMP-F                                              00001820
      *        PIC S9(4) COMP.                                          00001830
      *--                                                                       
           02  GM01-PRMP-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-PMBUZ1-F                                            00001840
      *        PIC S9(4) COMP.                                          00001850
      *--                                                                       
           02  GM01-PMBUZ1-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CZPRIXZ1-F                                          00001860
      *        PIC S9(4) COMP.                                          00001870
      *--                                                                       
           02  GM01-CZPRIXZ1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-PSTDTTCZ1-F                                         00001880
      *        PIC S9(4) COMP.                                          00001890
      *--                                                                       
           02  GM01-PSTDTTCZ1-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-PCOMMZ1-F                                           00001900
      *        PIC S9(4) COMP.                                          00001910
      *--                                                                       
           02  GM01-PCOMMZ1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-QNBREART-F                                          00001920
      *        PIC S9(4) COMP.                                          00001930
      *--                                                                       
           02  GM01-QNBREART-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-QTYPLIEN-F                                          00001940
      *        PIC S9(4) COMP.                                          00001950
      *--                                                                       
           02  GM01-QTYPLIEN-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-QSTOCKDEP-F                                         00001960
      *        PIC S9(4) COMP.                                          00001970
      *--                                                                       
           02  GM01-QSTOCKDEP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-QSTOCKMAG-F                                         00001980
      *        PIC S9(4) COMP.                                          00001990
      *--                                                                       
           02  GM01-QSTOCKMAG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-DSYST-F                                             00002000
      *        PIC S9(4) COMP.                                          00002010
      *--                                                                       
           02  GM01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-PCOMMARTZ1-F                                        00002020
      *        PIC S9(4) COMP.                                          00002030
      *--                                                                       
           02  GM01-PCOMMARTZ1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-PCOMMVOLZ1-F                                        00002040
      *        PIC S9(4) COMP.                                          00002050
      *--                                                                       
           02  GM01-PCOMMVOLZ1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-WSENSNF-F                                           00002060
      *        PIC S9(4) COMP.                                          00002070
      *--                                                                       
           02  GM01-WSENSNF-F                                                   
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                            00002080
                                                                                
