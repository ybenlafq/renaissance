      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE NASL1 RAPPROCHEMENT NASL CAISSE        *        
      *----------------------------------------------------------------*        
       01  RVGA01VN.                                                            
           05  NASL1-CTABLEG2    PIC X(15).                                     
           05  NASL1-CTABLEG2-REDEF REDEFINES NASL1-CTABLEG2.                   
               10  NASL1-SOCLIEU         PIC X(06).                             
               10  NASL1-SOCLIEU-N      REDEFINES NASL1-SOCLIEU                 
                                         PIC 9(06).                             
               10  NASL1-DATE            PIC X(08).                             
               10  NASL1-DATE-N         REDEFINES NASL1-DATE                    
                                         PIC 9(08).                             
               10  NASL1-NSEQ            PIC X(01).                             
               10  NASL1-NSEQ-N         REDEFINES NASL1-NSEQ                    
                                         PIC 9(01).                             
           05  NASL1-WTABLEG     PIC X(80).                                     
           05  NASL1-WTABLEG-REDEF  REDEFINES NASL1-WTABLEG.                    
               10  NASL1-INDICATE        PIC X(05).                             
               10  NASL1-LIEUGEST        PIC X(06).                             
               10  NASL1-LIEUGEST-N     REDEFINES NASL1-LIEUGEST                
                                         PIC 9(06).                             
               10  NASL1-DOMAINE         PIC X(05).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01VN-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NASL1-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  NASL1-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NASL1-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  NASL1-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
