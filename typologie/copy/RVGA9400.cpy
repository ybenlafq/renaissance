      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE CMTD00.RTGA94                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA9400.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA9400.                                                            
      *}                                                                        
           10 GA94-CPROG           PIC X(5).                                    
           10 GA94-CCHAMP          PIC X(15).                                   
           10 GA94-NSOCIETE        PIC X(3).                                    
           10 GA94-ATTRIBUT        PIC X(15).                                   
           10 GA94-DEFAUT          PIC X(20).                                   
           10 GA94-BORNEMIN        PIC X(20).                                   
           10 GA94-BORNEMAX        PIC X(20).                                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA9400-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA9400-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA94-CPROG-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 GA94-CPROG-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA94-CCHAMP-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA94-CCHAMP-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA94-NSOCIETE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA94-NSOCIETE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA94-ATTRIBUT-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA94-ATTRIBUT-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA94-DEFAUT-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA94-DEFAUT-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA94-BORNEMIN-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA94-BORNEMIN-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA94-BORNEMAX-F      PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 GA94-BORNEMAX-F      PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
