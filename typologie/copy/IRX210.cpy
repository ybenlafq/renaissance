      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRX210 AU 04/02/1999  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                           12,03,BI,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,05,BI,A,                          *        
      *                           25,04,BI,A,                          *        
      *                           29,08,BI,A,                          *        
      *                           37,06,BI,A,                          *        
      *                           43,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRX210.                                                        
            05 NOMETAT-IRX210           PIC X(6) VALUE 'IRX210'.                
            05 RUPTURES-IRX210.                                                 
           10 IRX210-NSOCIETE           PIC X(03).                      007  003
           10 IRX210-NZONPRIX           PIC X(02).                      010  002
           10 IRX210-NMAG               PIC X(03).                      012  003
           10 IRX210-RAYON              PIC X(05).                      015  005
           10 IRX210-CFAM               PIC X(05).                      020  005
           10 IRX210-NCONC              PIC X(04).                      025  004
           10 IRX210-DRELEVE            PIC X(08).                      029  008
           10 IRX210-CVENDEUR           PIC X(06).                      037  006
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRX210-SEQUENCE           PIC S9(04) COMP.                043  002
      *--                                                                       
           10 IRX210-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRX210.                                                   
           10 IRX210-LEMPCONC           PIC X(15).                      045  015
           10 IRX210-LENSCONC           PIC X(15).                      060  015
           10 IRX210-LMAG               PIC X(20).                      075  020
           10 IRX210-LZONPRIX           PIC X(20).                      095  020
           10 IRX210-ALIGNPART1         PIC S9(05)      COMP-3.         115  003
           10 IRX210-ALIGNPART2         PIC S9(05)      COMP-3.         118  003
           10 IRX210-ALIGNTOT1          PIC S9(05)      COMP-3.         121  003
           10 IRX210-ALIGNTOT2          PIC S9(05)      COMP-3.         124  003
           10 IRX210-INFSRP1            PIC S9(05)      COMP-3.         127  003
           10 IRX210-INFSRP2            PIC S9(03)      COMP-3.         130  002
           10 IRX210-NBETIQCONC         PIC S9(05)      COMP-3.         132  003
           10 IRX210-NBREFCONC          PIC S9(05)      COMP-3.         135  003
           10 IRX210-NBREFEXPO          PIC S9(05)      COMP-3.         138  003
           10 IRX210-PVINF1             PIC S9(05)      COMP-3.         141  003
           10 IRX210-PVINF2             PIC S9(05)      COMP-3.         144  003
           10 IRX210-TRI1               PIC S9(02)      COMP-3.         147  002
           10 IRX210-TRI2               PIC S9(09)      COMP-3.         149  005
           10 IRX210-DSAISIE-MAX        PIC X(08).                      154  008
           10 IRX210-DSAISIE-MIN        PIC X(08).                      162  008
            05 FILLER                      PIC X(343).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IRX210-LONG           PIC S9(4)   COMP  VALUE +169.           
      *                                                                         
      *--                                                                       
        01  DSECT-IRX210-LONG           PIC S9(4) COMP-5  VALUE +169.           
                                                                                
      *}                                                                        
