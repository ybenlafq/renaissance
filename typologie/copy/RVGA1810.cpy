      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *                                                                         
E0099 ******************************************************************        
      * DSA057 09/08/04 SUPPORT MAINTENANCE EVOLUTION                           
      *                 LIENS CFAM/CDES PAR OPCO                                
      ******************************************************************        
      * VUE RVGA1810 SUR RTGA18                                                 
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVGA1810.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVGA1810.                                                             
      *}                                                                        
           02 GA18-CFAM          PIC X(0005).                                   
           02 GA18-CDESCRIPTIF   PIC X(0005).                                   
           02 GA18-NSEQ          PIC S9(3) COMP-3.                              
           02 GA18-WDESC         PIC X(0001).                                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      * FLAGS                                                                   
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVGA1810-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVGA1810-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 GA18-CFAM-F        PIC S9(4) COMP.                                
      *--                                                                       
           02 GA18-CFAM-F        PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 GA18-CDESCRIPTIF-F PIC S9(4) COMP.                                
      *--                                                                       
           02 GA18-CDESCRIPTIF-F PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 GA18-NSEQ-F        PIC S9(4) COMP.                                
      *--                                                                       
           02 GA18-NSEQ-F        PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 GA18-WDESC-F       PIC S9(4) COMP.                                
      *                                                                         
      *--                                                                       
           02 GA18-WDESC-F       PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
