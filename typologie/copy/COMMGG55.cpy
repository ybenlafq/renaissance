      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
              03 COMMGG55  REDEFINES COMM-GG50-FILLER.                          
                 05  COMM-GG55-FILLER    PIC X(3452).                           
                 05  COMM-GG55-APPLI REDEFINES COMM-GG55-FILLER.                
                     10  COMM-GG55-NCODIC-CHGT  PIC  X(01).                     
                     10  COMM-GG55-VALID-PF5    PIC  X(01).                     
                     10  COMM-GG55-DATAS.                                       
                       15 COMM-GG55-NUMPAG       PIC  9(03).                    
                       15 COMM-GG55-NBRPAG       PIC  9(03).                    
                       15 COMM-GG55-MODIF-TS     PIC  9(01).                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                15 COMM-GG55-IND-RECH-TS  PIC S9(04) COMP.               
      *--                                                                       
                       15 COMM-GG55-IND-RECH-TS  PIC S9(04) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                15 COMM-GG55-IND-TS-MAX   PIC S9(04) COMP.               
      *--                                                                       
                       15 COMM-GG55-IND-TS-MAX   PIC S9(04) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                15 COMM-GG55-NUM-TS PIC S9(04) COMP OCCURS 10.           
      *--                                                                       
                       15 COMM-GG55-NUM-TS PIC S9(04) COMP-5 OCCURS 10.         
      *}                                                                        
                       15 COMM-GG55-STATUT       PIC  X(03).                    
                       15 COMM-GG55-DSTATUT      PIC  X(08).                    
                       15 COMM-GG55-LSTATCOMP    PIC  X(03).                    
                       15 COMM-GG55-CEXPO        PIC  X(03).                    
                       15 COMM-GG55-WSENSAPPRO   PIC  X(01).                    
                       15 COMM-GG55-CFAM         PIC  X(05).                    
                       15 COMM-GG55-LFAM         PIC  X(20).                    
E0072O                 15 COMM-TOP-CFAM-BS       PIC 9.                         
  "                       88 FAMILLE-NOT-BS            VALUE 0.                 
  "                       88 FAMILLE-BS                VALUE 1.                 
                       15 COMM-GG55-LREFFOU      PIC  X(20).                    
                       15 COMM-GG55-OASIEG       PIC  X(01).                    
                       15 COMM-GG55-DOASIEG      PIC  X(08).                    
                       15 COMM-GG55-NCODIC-SUIV  PIC  X(07).                    
                       15 COMM-GG55-CCODGROUP    PIC  X(01).                    
                       15 COMM-GG55-PRIMAC       PIC S9(7)V99 COMP-3.           
                       15 COMM-GG55-EDMMAX       PIC 99999.                     
                       15 COMM-GG55-QTAUXTVA     PIC S9V9(4) COMP-3.            
                       15 COMM-GG55-OAZP         PIC  X(01).                    
                       15 COMM-GG55-DDOAZP       PIC  X(08).                    
                       15 COMM-GG55-DFOAZP       PIC  X(08).                    
                       15 COMM-GG55-CMARQ        PIC  X(05).                    
                       15 COMM-GG55-LMARQ        PIC  X(20).                    
                       15 COMM-GG55-NSOCDEP      PIC  X(03).                    
                       15 COMM-GG55-NDEPOT       PIC  X(03).                    
                       15 COMM-GG55-STOCKD       PIC  X(06).                    
                       15 COMM-GG55-PCHT         PIC S9(07)V99 COMP-3.          
                       15 COMM-GG55-SRP          PIC S9(07)V99 COMP-3.          
E0072                  15 COMM-GG55-PRMP         PIC S9(07)V99 COMP-3.          
                       15 COMM-GG55-PRAR         PIC  X(08).                    
                       15 COMM-GG55-PBF          PIC  X(08).                    
                       15 COMM-GG55-DCDEFOU      PIC  X(10).                    
                       15 COMM-GG55-QCDEFOU      PIC  X(06).                    
                       15 COMM-GG55-REFERENTIEL.                                
                          20  COMM-GG55-PRIRAC  PIC S9(7)V99 COMP-3.            
                          20  COMM-GG55-WIMPAC  PIC X.                          
                          20  COMM-GG55-WMAXAC  PIC X.                          
                          20  COMM-GG55-DDREFA  PIC X(8).                       
                          20  COMM-GG55-DFREFA  PIC X(8).                       
                          20  COMM-GG55-INTREFA  PIC S9(7)V99 COMP-3.           
                          20  COMM-GG55-DINTREFA PIC X(08).                     
                          20  COMM-GG55-PRIRNO  PIC S9(7)V99 COMP-3.            
                          20  COMM-GG55-WIMPN   PIC X.                          
                          20  COMM-GG55-WMAXN   PIC X.                          
                          20  COMM-GG55-DDREFN  PIC X(8).                       
                          20  COMM-GG55-DFREFN  PIC X(8).                       
                          20  COMM-GG55-INTREFN  PIC S9(7)V99 COMP-3.           
                          20  COMM-GG55-DINTREFN PIC X(08).                     
                          20  COMM-GG55-PRIR2AC PIC S9(7)V99 COMP-3.            
                          20  COMM-GG55-WBLOCAC PIC X.                          
                          20  COMM-GG55-DDREF2A PIC X(8).                       
                          20  COMM-GG55-DFREF2A PIC X(8).                       
                          20  COMM-GG55-PRIR2NO PIC S9(7)V99 COMP-3.            
                          20  COMM-GG55-WBLOCN  PIC X.                          
                          20  COMM-GG55-DDREF2N PIC X(8).                       
                          20  COMM-GG55-DFREF2N PIC X(8).                       
E0072                     20  COMM-GG55-CTRL-PMINI PIC X(01).                   
  "                           88  EXIST-PAS-PCOMMINI         VALUE 'N'.         
  "                           88  EXIST-PCOMMINI             VALUE 'O'.         
  "                       20  COMM-GG55-PCOMMINIX  PIC X(10).                   
  "                       20  COMM-GG55-PCOMMINI9  PIC S9(7)V99 COMP-3.         
                       15  COMM-GG55-RESTE.                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                   20  COMM-GG55-NBR-GROUP  PIC S9(04) COMP.             
      *--                                                                       
                          20  COMM-GG55-NBR-GROUP  PIC S9(04) COMP-5.           
      *}                                                                        
                          20  COMM-GG55-TOP-COL1   PIC X OCCURS 10.             
                          20  COMM-GG55-TOP-COL2   PIC X OCCURS 10.             
                          20  COMM-GG55-MAJ-PRIX   PIC X(01).                   
                          20  COMM-GG55-MAJ-PRIME  PIC X(01).                   
                          20  COMM-GG55-MAJ-OA     PIC X(01).                   
                          20  COMM-GG55-SUPOAZP    PIC X(01).                   
                          20  COMM-GG55-DATEFFET   PIC X(08).                   
                          20  COMM-GG55-PVTTC      PIC 9(7)V99 COMP-3.          
                          20  COMM-GG55-PASSAGE-1  PIC X(01).                   
                          20  COMM-GG55-PASSAGE-2  PIC X(01).                   
                          20  COMM-GG55-PASSAGE-3  PIC X(01).                   
                          20  COMM-GG55-CTRL-PBF     PIC  X(01).                
                              88  PBF-ACTIF            VALUE 'O'.               
                              88  PBF-NON-ACTIF        VALUE 'N'.               
                     10  COMM-GG55-SIMI.                                        
                        15  COMM-GG55-ART-SIM     PIC  9(7) OCCURS 20.          
                        15  COMM-GG55-NBARTSIM    PIC  S9(03) COMP-3.           
                        15  COMM-GG55-CPTSIM      PIC  S9(03) COMP-3.           
      *                                                                         
E0072K               10  W-WCOPR1-MAX     PIC S9(4) COMP-3.                     
                     10  W-WCOPR1-TAB.                                          
                         15  W-WCOPR1-NBP         PIC S9(4) COMP-3.             
                         15  W-WCOPR1-POS         OCCURS 150                    
                                                  INDEXED W-ICOPR1.             
                             20 W-WCOPR1-MT       PIC 999V99.                   
                     10 COMM-GG55-TOP-CFAM-CBN    PIC 9.                        
                         88  COMM-GG55-CFAM-NOT-CBN     VALUE 0.                
                         88  COMM-GG55-CFAM-CBN         VALUE 1.                
                     10 COMM-GG55-CBGSM              PIC X.                     
                     10 COMM-GG55-UNUSED             PIC X(2147).               
                                                                                
