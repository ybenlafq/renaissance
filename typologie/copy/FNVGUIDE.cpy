      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      ******************************************************************        
      *               F I C H E   D E S C R I P T I V E                *        
      ******************************************************************        
      *                                                                *        
      *  PROJET      : INNOVENTE - CATALOGUE                           *        
      *  COPY        : FNVGUIDE                                        *        
      *  CREATION    : 07/06/2006                                      *        
      *  FONCTION    : DSECT DU FICHIER GENERE PAR LE BNV206 QUI       *        
      *                TRAITE LES MESSAGES DU GUIDE D'ACHAT            *        
      *                RECUS DE DACEM.COM                              *        
      *                                                                *        
      ******************************************************************        
      ******************************************************************        
      *                                                                *        
      * LONGUEUR DU FICHIER : 131                                      *        
      *                                                                *        
      ******************************************************************        
      ******************************************************************        
      *                                                                         
       01  :FNVGUIDE:-RECORD.                                                   
           03 :FNVGUIDE:-WACTION     PIC X(01).                                 
           03 :FNVGUIDE:-CLE.                                                   
              05 :FNVGUIDE:-REFPERE     PIC X(50).                              
              05 :FNVGUIDE:-MARQPERE    PIC X(50).                              
              05 :FNVGUIDE:-CODICPERE   PIC X(15).                              
              05 :FNVGUIDE:-CODICFILS   PIC X(15).                              
      *                                                                         
                                                                                
