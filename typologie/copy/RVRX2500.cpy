      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVRX2500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRX2500                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRX2500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRX2500.                                                            
      *}                                                                        
           02  RX25-NRELEVE                                                     
               PIC X(0007).                                                     
           02  RX25-NCODIC                                                      
               PIC X(0007).                                                     
           02  RX25-CFAM                                                        
               PIC X(0005).                                                     
           02  RX25-CMARQ                                                       
               PIC X(0005).                                                     
           02  RX25-LREFFOURN                                                   
               PIC X(0020).                                                     
           02  RX25-PRELEVE                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  RX25-QCONTENU                                                    
               PIC X(0003).                                                     
           02  RX25-QCOLREL                                                     
               PIC X(0003).                                                     
           02  RX25-LCOMMENT                                                    
               PIC X(0020).                                                     
           02  RX25-CVENDEUR                                                    
               PIC X(0006).                                                     
           02  RX25-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRX2500                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRX2500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRX2500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX25-NRELEVE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX25-NRELEVE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX25-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX25-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX25-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX25-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX25-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX25-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX25-LREFFOURN-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX25-LREFFOURN-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX25-PRELEVE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX25-PRELEVE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX25-QCONTENU-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX25-QCONTENU-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX25-QCOLREL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX25-QCOLREL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX25-LCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX25-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX25-CVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX25-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX25-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX25-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
