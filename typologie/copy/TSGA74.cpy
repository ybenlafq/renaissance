      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
        01  TS-GA74.                                                            
      *----------------------------------------  LONGUEUR TS      26            
            02 TS-GA74-LONG    PIC S9(5) COMP-3    VALUE +26.                   
      *-----------------------------------  DONNEES                             
            02 TS-GA74-DONNEES.                                                 
               05 TS-GA74-CDEST       PIC X(5).                                 
               05 TS-GA74-LDEST       PIC X(20).                                
      * -- FLAG POUR MAJ DES TABLES QUI PRENDRA COMME VALEUR S OU C             
      * S = SUPPRESSION DE L'ENREGISTREMENT                                     
      * C = CREATION D'UN NOUVEL ENREGISTREMENT                                 
               05 TS-GA74-FLAG        PIC X.                                    
                                                                                
