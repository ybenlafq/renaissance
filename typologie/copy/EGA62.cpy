      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA62   EGA62                                              00000020
      ***************************************************************** 00000030
       01   EGA62I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNCODICI  PIC X(7).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLREFI    PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCODF2L  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLCODF2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCODF2F  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLCODF2I  PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNFAMI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLFAMI    PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMARQL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNMARQF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNMARQI   PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLMARQI   PIC X(20).                                      00000450
           02 MWTABLEI OCCURS   14 TIMES .                              00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFOURNL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MCFOURNL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCFOURNF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MCFOURNI     PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLFOURNL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MLFOURNL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLFOURNF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MLFOURNI     PIC X(20).                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWTFOURL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MWTFOURL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWTFOURF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MWTFOURI     PIC X.                                     00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MZONCMDI  PIC X(15).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MLIBERRI  PIC X(58).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCODTRAI  PIC X(4).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCICSI    PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MNETNAMI  PIC X(8).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MSCREENI  PIC X(4).                                       00000820
      ***************************************************************** 00000830
      * SDF: EGA62   EGA62                                              00000840
      ***************************************************************** 00000850
       01   EGA62O REDEFINES EGA62I.                                    00000860
           02 FILLER    PIC X(12).                                      00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MDATJOUA  PIC X.                                          00000890
           02 MDATJOUC  PIC X.                                          00000900
           02 MDATJOUP  PIC X.                                          00000910
           02 MDATJOUH  PIC X.                                          00000920
           02 MDATJOUV  PIC X.                                          00000930
           02 MDATJOUO  PIC X(10).                                      00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MTIMJOUA  PIC X.                                          00000960
           02 MTIMJOUC  PIC X.                                          00000970
           02 MTIMJOUP  PIC X.                                          00000980
           02 MTIMJOUH  PIC X.                                          00000990
           02 MTIMJOUV  PIC X.                                          00001000
           02 MTIMJOUO  PIC X(5).                                       00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MWFONCA   PIC X.                                          00001030
           02 MWFONCC   PIC X.                                          00001040
           02 MWFONCP   PIC X.                                          00001050
           02 MWFONCH   PIC X.                                          00001060
           02 MWFONCV   PIC X.                                          00001070
           02 MWFONCO   PIC X(3).                                       00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MNCODICA  PIC X.                                          00001100
           02 MNCODICC  PIC X.                                          00001110
           02 MNCODICP  PIC X.                                          00001120
           02 MNCODICH  PIC X.                                          00001130
           02 MNCODICV  PIC X.                                          00001140
           02 MNCODICO  PIC X(7).                                       00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MLREFA    PIC X.                                          00001170
           02 MLREFC    PIC X.                                          00001180
           02 MLREFP    PIC X.                                          00001190
           02 MLREFH    PIC X.                                          00001200
           02 MLREFV    PIC X.                                          00001210
           02 MLREFO    PIC X(20).                                      00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MLCODF2A  PIC X.                                          00001240
           02 MLCODF2C  PIC X.                                          00001250
           02 MLCODF2P  PIC X.                                          00001260
           02 MLCODF2H  PIC X.                                          00001270
           02 MLCODF2V  PIC X.                                          00001280
           02 MLCODF2O  PIC X(20).                                      00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MNFAMA    PIC X.                                          00001310
           02 MNFAMC    PIC X.                                          00001320
           02 MNFAMP    PIC X.                                          00001330
           02 MNFAMH    PIC X.                                          00001340
           02 MNFAMV    PIC X.                                          00001350
           02 MNFAMO    PIC X(5).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MLFAMA    PIC X.                                          00001380
           02 MLFAMC    PIC X.                                          00001390
           02 MLFAMP    PIC X.                                          00001400
           02 MLFAMH    PIC X.                                          00001410
           02 MLFAMV    PIC X.                                          00001420
           02 MLFAMO    PIC X(20).                                      00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MNMARQA   PIC X.                                          00001450
           02 MNMARQC   PIC X.                                          00001460
           02 MNMARQP   PIC X.                                          00001470
           02 MNMARQH   PIC X.                                          00001480
           02 MNMARQV   PIC X.                                          00001490
           02 MNMARQO   PIC X(5).                                       00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MLMARQA   PIC X.                                          00001520
           02 MLMARQC   PIC X.                                          00001530
           02 MLMARQP   PIC X.                                          00001540
           02 MLMARQH   PIC X.                                          00001550
           02 MLMARQV   PIC X.                                          00001560
           02 MLMARQO   PIC X(20).                                      00001570
           02 MWTABLEO OCCURS   14 TIMES .                              00001580
             03 FILLER       PIC X(2).                                  00001590
             03 MCFOURNA     PIC X.                                     00001600
             03 MCFOURNC     PIC X.                                     00001610
             03 MCFOURNP     PIC X.                                     00001620
             03 MCFOURNH     PIC X.                                     00001630
             03 MCFOURNV     PIC X.                                     00001640
             03 MCFOURNO     PIC X(5).                                  00001650
             03 FILLER       PIC X(2).                                  00001660
             03 MLFOURNA     PIC X.                                     00001670
             03 MLFOURNC     PIC X.                                     00001680
             03 MLFOURNP     PIC X.                                     00001690
             03 MLFOURNH     PIC X.                                     00001700
             03 MLFOURNV     PIC X.                                     00001710
             03 MLFOURNO     PIC X(20).                                 00001720
             03 FILLER       PIC X(2).                                  00001730
             03 MWTFOURA     PIC X.                                     00001740
             03 MWTFOURC     PIC X.                                     00001750
             03 MWTFOURP     PIC X.                                     00001760
             03 MWTFOURH     PIC X.                                     00001770
             03 MWTFOURV     PIC X.                                     00001780
             03 MWTFOURO     PIC X.                                     00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MZONCMDA  PIC X.                                          00001810
           02 MZONCMDC  PIC X.                                          00001820
           02 MZONCMDP  PIC X.                                          00001830
           02 MZONCMDH  PIC X.                                          00001840
           02 MZONCMDV  PIC X.                                          00001850
           02 MZONCMDO  PIC X(15).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MLIBERRA  PIC X.                                          00001880
           02 MLIBERRC  PIC X.                                          00001890
           02 MLIBERRP  PIC X.                                          00001900
           02 MLIBERRH  PIC X.                                          00001910
           02 MLIBERRV  PIC X.                                          00001920
           02 MLIBERRO  PIC X(58).                                      00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MCODTRAA  PIC X.                                          00001950
           02 MCODTRAC  PIC X.                                          00001960
           02 MCODTRAP  PIC X.                                          00001970
           02 MCODTRAH  PIC X.                                          00001980
           02 MCODTRAV  PIC X.                                          00001990
           02 MCODTRAO  PIC X(4).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MCICSA    PIC X.                                          00002020
           02 MCICSC    PIC X.                                          00002030
           02 MCICSP    PIC X.                                          00002040
           02 MCICSH    PIC X.                                          00002050
           02 MCICSV    PIC X.                                          00002060
           02 MCICSO    PIC X(5).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MNETNAMA  PIC X.                                          00002090
           02 MNETNAMC  PIC X.                                          00002100
           02 MNETNAMP  PIC X.                                          00002110
           02 MNETNAMH  PIC X.                                          00002120
           02 MNETNAMV  PIC X.                                          00002130
           02 MNETNAMO  PIC X(8).                                       00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MSCREENA  PIC X.                                          00002160
           02 MSCREENC  PIC X.                                          00002170
           02 MSCREENP  PIC X.                                          00002180
           02 MSCREENH  PIC X.                                          00002190
           02 MSCREENV  PIC X.                                          00002200
           02 MSCREENO  PIC X(4).                                       00002210
                                                                                
