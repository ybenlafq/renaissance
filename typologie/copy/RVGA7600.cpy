      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVGA7600                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA7600                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
       01  RVGA7600.                                                    00000090
           02  GA76-CFAM                                                00000100
               PIC X(0005).                                             00000110
           02  GA76-CMARKETIN1                                          00000120
               PIC X(0005).                                             00000130
           02  GA76-CVMARKETIN1                                         00000140
               PIC X(0005).                                             00000150
           02  GA76-CMARKETIN2                                          00000160
               PIC X(0005).                                             00000170
           02  GA76-CVMARKETIN2                                         00000180
               PIC X(0005).                                             00000190
           02  GA76-NZONPRIX                                            00000200
               PIC X(0002).                                             00000210
           02  GA76-DEFFET                                              00000220
               PIC X(0008).                                             00000230
           02  GA76-WTYPCOMM                                            00000240
               PIC X(0001).                                             00000250
           02  GA76-PCOMMVOL                                            00000260
               PIC S9(5)V9(0002) COMP-3.                                00000270
           02  GA76-DSYST                                               00000280
               PIC S9(13) COMP-3.                                       00000290
      *                                                                 00000300
      *---------------------------------------------------------        00000310
      *   LISTE DES FLAGS DE LA TABLE RVGA7600                          00000320
      *---------------------------------------------------------        00000330
      *                                                                 00000340
       01  RVGA7600-FLAGS.                                              00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA76-CFAM-F                                              00000360
      *        PIC S9(4) COMP.                                          00000370
      *--                                                                       
           02  GA76-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA76-CMARKETIN1-F                                        00000380
      *        PIC S9(4) COMP.                                          00000390
      *--                                                                       
           02  GA76-CMARKETIN1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA76-CVMARKETIN1-F                                       00000400
      *        PIC S9(4) COMP.                                          00000410
      *--                                                                       
           02  GA76-CVMARKETIN1-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA76-CMARKETIN2-F                                        00000420
      *        PIC S9(4) COMP.                                          00000430
      *--                                                                       
           02  GA76-CMARKETIN2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA76-CVMARKETIN2-F                                       00000440
      *        PIC S9(4) COMP.                                          00000450
      *--                                                                       
           02  GA76-CVMARKETIN2-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA76-NZONPRIX-F                                          00000460
      *        PIC S9(4) COMP.                                          00000470
      *--                                                                       
           02  GA76-NZONPRIX-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA76-DEFFET-F                                            00000480
      *        PIC S9(4) COMP.                                          00000490
      *--                                                                       
           02  GA76-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA76-WTYPCOMM-F                                          00000500
      *        PIC S9(4) COMP.                                          00000510
      *--                                                                       
           02  GA76-WTYPCOMM-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA76-PCOMMVOL-F                                          00000520
      *        PIC S9(4) COMP.                                          00000530
      *--                                                                       
           02  GA76-PCOMMVOL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA76-DSYST-F                                             00000540
      *        PIC S9(4) COMP.                                          00000550
      *--                                                                       
           02  GA76-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00000560
                                                                                
