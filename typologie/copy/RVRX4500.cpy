      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRX4500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRX4500                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRX4500.                                                            
           02  RX45-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  RX45-NMAG                                                        
               PIC X(0003).                                                     
           02  RX45-CPROFIL                                                     
               PIC X(0005).                                                     
           02  RX45-LPROFIL                                                     
               PIC X(0020).                                                     
           02  RX45-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRX4500                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRX4500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX45-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX45-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX45-NMAG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX45-NMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX45-CPROFIL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX45-CPROFIL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX45-LPROFIL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX45-LPROFIL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX45-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX45-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
