      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA73   EGA73                                              00000020
      ***************************************************************** 00000030
       01   EGA73I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * NUMERO CHAMP                                                    00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMCHAMPL     COMP PIC S9(4).                            00000170
      *--                                                                       
           02 MNUMCHAMPL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNUMCHAMPF     PIC X.                                     00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MNUMCHAMPI     PIC X(3).                                  00000200
      * FONCTION                                                        00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCTL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MFONCTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MFONCTF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MFONCTI   PIC X(3).                                       00000250
      * LIBELLE FONCTION                                                00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFONCTL  COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MLFONCTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLFONCTF  PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MLFONCTI  PIC X(13).                                      00000300
      * NOM DE LA TABLE                                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTABLEL  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MCTABLEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTABLEF  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MCTABLEI  PIC X(6).                                       00000350
      * DATE DERNIERE MAJ                                               00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMAJL    COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MDMAJL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDMAJF    PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MDMAJI    PIC X(8).                                       00000400
      * NOM DE LA SOUS TABLE                                            00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSTABLEL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MCSTABLEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCSTABLEF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCSTABLEI      PIC X(5).                                  00000450
      * NOM CHAMP                                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHAMPL   COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MCHAMPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCHAMPF   PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MCHAMPI   PIC X(8).                                       00000500
      * LIBELLE CHAMP                                                   00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCHAMPL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MLCHAMPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCHAMPF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MLCHAMPI  PIC X(30).                                      00000550
      * LONGUEUR OCTETS                                                 00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQCHAMPL  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MQCHAMPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQCHAMPF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MQCHAMPI  PIC X(3).                                       00000600
      * TYPE CHAMP                                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWCHAMPL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MWCHAMPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWCHAMPF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MWCHAMPI  PIC X.                                          00000650
      * POSITION DANS LA CLE                                            00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPOSCLEL      COMP PIC S9(4).                            00000670
      *--                                                                       
           02 MWPOSCLEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWPOSCLEF      PIC X.                                     00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MWPOSCLEI      PIC X.                                     00000700
      * NB CHIFFRE APRES VIRG.                                          00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQDECL    COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MQDECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MQDECF    PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MQDECI    PIC X(3).                                       00000750
      * CONTROLES DIRECTS                                               00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWCTLDIRL      COMP PIC S9(4).                            00000770
      *--                                                                       
           02 MWCTLDIRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWCTLDIRF      PIC X.                                     00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MWCTLDIRI      PIC X.                                     00000800
      * CONTROLES INDIRECTS                                             00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWCTLINDL      COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MWCTLINDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWCTLINDF      PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MWCTLINDI      PIC X.                                     00000850
      * NUMERO CONTROLE 1                                               00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCTLIND1L     COMP PIC S9(4).                            00000870
      *--                                                                       
           02 MCCTLIND1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCCTLIND1F     PIC X.                                     00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCCTLIND1I     PIC X(3).                                  00000900
      * NUMERO CONTROLE 2                                               00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCTLIND2L     COMP PIC S9(4).                            00000920
      *--                                                                       
           02 MCCTLIND2L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCCTLIND2F     PIC X.                                     00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MCCTLIND2I     PIC X(3).                                  00000950
      * NUMERO CONTROLE 3                                               00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCTLIND3L     COMP PIC S9(4).                            00000970
      *--                                                                       
           02 MCCTLIND3L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCCTLIND3F     PIC X.                                     00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MCCTLIND3I     PIC X(3).                                  00001000
      * NUMERO CONTROLE 4                                               00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCTLIND4L     COMP PIC S9(4).                            00001020
      *--                                                                       
           02 MCCTLIND4L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCCTLIND4F     PIC X.                                     00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MCCTLIND4I     PIC X(3).                                  00001050
      * NUMERO CONTROLE 5                                               00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCTLIND5L     COMP PIC S9(4).                            00001070
      *--                                                                       
           02 MCCTLIND5L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCCTLIND5F     PIC X.                                     00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCCTLIND5I     PIC X(3).                                  00001100
      * NOM TABLE ASSOCIE                                               00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTABASSL      COMP PIC S9(4).                            00001120
      *--                                                                       
           02 MCTABASSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTABASSF      PIC X.                                     00001130
           02 FILLER    PIC X(4).                                       00001140
           02 MCTABASSI      PIC X(6).                                  00001150
      * NOM SOUS TABLE ASSOCIE                                          00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSTABASSL     COMP PIC S9(4).                            00001170
      *--                                                                       
           02 MCSTABASSL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCSTABASSF     PIC X.                                     00001180
           02 FILLER    PIC X(4).                                       00001190
           02 MCSTABASSI     PIC X(5).                                  00001200
      * ZONE CMD AIDA                                                   00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MZONCMDI  PIC X(15).                                      00001250
      * MESSAGE ERREUR                                                  00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MLIBERRI  PIC X(58).                                      00001300
      * CODE TRANSACTION                                                00001310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001320
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001330
           02 FILLER    PIC X(4).                                       00001340
           02 MCODTRAI  PIC X(4).                                       00001350
      * CICS DE TRAVAIL                                                 00001360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001370
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001380
           02 FILLER    PIC X(4).                                       00001390
           02 MCICSI    PIC X(5).                                       00001400
      * NETNAME                                                         00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MNETNAMI  PIC X(8).                                       00001450
      * CODE TERMINAL                                                   00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MSCREENI  PIC X(5).                                       00001500
      ***************************************************************** 00001510
      * SDF: EGA73   EGA73                                              00001520
      ***************************************************************** 00001530
       01   EGA73O REDEFINES EGA73I.                                    00001540
           02 FILLER    PIC X(12).                                      00001550
      * DATE DU JOUR                                                    00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MDATJOUA  PIC X.                                          00001580
           02 MDATJOUC  PIC X.                                          00001590
           02 MDATJOUP  PIC X.                                          00001600
           02 MDATJOUH  PIC X.                                          00001610
           02 MDATJOUV  PIC X.                                          00001620
           02 MDATJOUO  PIC X(10).                                      00001630
      * HEURE                                                           00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MTIMJOUA  PIC X.                                          00001660
           02 MTIMJOUC  PIC X.                                          00001670
           02 MTIMJOUP  PIC X.                                          00001680
           02 MTIMJOUH  PIC X.                                          00001690
           02 MTIMJOUV  PIC X.                                          00001700
           02 MTIMJOUO  PIC X(5).                                       00001710
      * NUMERO CHAMP                                                    00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MNUMCHAMPA     PIC X.                                     00001740
           02 MNUMCHAMPC     PIC X.                                     00001750
           02 MNUMCHAMPP     PIC X.                                     00001760
           02 MNUMCHAMPH     PIC X.                                     00001770
           02 MNUMCHAMPV     PIC X.                                     00001780
           02 MNUMCHAMPO     PIC X(3).                                  00001790
      * FONCTION                                                        00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MFONCTA   PIC X.                                          00001820
           02 MFONCTC   PIC X.                                          00001830
           02 MFONCTP   PIC X.                                          00001840
           02 MFONCTH   PIC X.                                          00001850
           02 MFONCTV   PIC X.                                          00001860
           02 MFONCTO   PIC X(3).                                       00001870
      * LIBELLE FONCTION                                                00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MLFONCTA  PIC X.                                          00001900
           02 MLFONCTC  PIC X.                                          00001910
           02 MLFONCTP  PIC X.                                          00001920
           02 MLFONCTH  PIC X.                                          00001930
           02 MLFONCTV  PIC X.                                          00001940
           02 MLFONCTO  PIC X(13).                                      00001950
      * NOM DE LA TABLE                                                 00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MCTABLEA  PIC X.                                          00001980
           02 MCTABLEC  PIC X.                                          00001990
           02 MCTABLEP  PIC X.                                          00002000
           02 MCTABLEH  PIC X.                                          00002010
           02 MCTABLEV  PIC X.                                          00002020
           02 MCTABLEO  PIC X(6).                                       00002030
      * DATE DERNIERE MAJ                                               00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MDMAJA    PIC X.                                          00002060
           02 MDMAJC    PIC X.                                          00002070
           02 MDMAJP    PIC X.                                          00002080
           02 MDMAJH    PIC X.                                          00002090
           02 MDMAJV    PIC X.                                          00002100
           02 MDMAJO    PIC X(8).                                       00002110
      * NOM DE LA SOUS TABLE                                            00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MCSTABLEA      PIC X.                                     00002140
           02 MCSTABLEC PIC X.                                          00002150
           02 MCSTABLEP PIC X.                                          00002160
           02 MCSTABLEH PIC X.                                          00002170
           02 MCSTABLEV PIC X.                                          00002180
           02 MCSTABLEO      PIC X(5).                                  00002190
      * NOM CHAMP                                                       00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MCHAMPA   PIC X.                                          00002220
           02 MCHAMPC   PIC X.                                          00002230
           02 MCHAMPP   PIC X.                                          00002240
           02 MCHAMPH   PIC X.                                          00002250
           02 MCHAMPV   PIC X.                                          00002260
           02 MCHAMPO   PIC X(8).                                       00002270
      * LIBELLE CHAMP                                                   00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MLCHAMPA  PIC X.                                          00002300
           02 MLCHAMPC  PIC X.                                          00002310
           02 MLCHAMPP  PIC X.                                          00002320
           02 MLCHAMPH  PIC X.                                          00002330
           02 MLCHAMPV  PIC X.                                          00002340
           02 MLCHAMPO  PIC X(30).                                      00002350
      * LONGUEUR OCTETS                                                 00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MQCHAMPA  PIC X.                                          00002380
           02 MQCHAMPC  PIC X.                                          00002390
           02 MQCHAMPP  PIC X.                                          00002400
           02 MQCHAMPH  PIC X.                                          00002410
           02 MQCHAMPV  PIC X.                                          00002420
           02 MQCHAMPO  PIC X(3).                                       00002430
      * TYPE CHAMP                                                      00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MWCHAMPA  PIC X.                                          00002460
           02 MWCHAMPC  PIC X.                                          00002470
           02 MWCHAMPP  PIC X.                                          00002480
           02 MWCHAMPH  PIC X.                                          00002490
           02 MWCHAMPV  PIC X.                                          00002500
           02 MWCHAMPO  PIC X.                                          00002510
      * POSITION DANS LA CLE                                            00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MWPOSCLEA      PIC X.                                     00002540
           02 MWPOSCLEC PIC X.                                          00002550
           02 MWPOSCLEP PIC X.                                          00002560
           02 MWPOSCLEH PIC X.                                          00002570
           02 MWPOSCLEV PIC X.                                          00002580
           02 MWPOSCLEO      PIC X.                                     00002590
      * NB CHIFFRE APRES VIRG.                                          00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MQDECA    PIC X.                                          00002620
           02 MQDECC    PIC X.                                          00002630
           02 MQDECP    PIC X.                                          00002640
           02 MQDECH    PIC X.                                          00002650
           02 MQDECV    PIC X.                                          00002660
           02 MQDECO    PIC X(3).                                       00002670
      * CONTROLES DIRECTS                                               00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MWCTLDIRA      PIC X.                                     00002700
           02 MWCTLDIRC PIC X.                                          00002710
           02 MWCTLDIRP PIC X.                                          00002720
           02 MWCTLDIRH PIC X.                                          00002730
           02 MWCTLDIRV PIC X.                                          00002740
           02 MWCTLDIRO      PIC X.                                     00002750
      * CONTROLES INDIRECTS                                             00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MWCTLINDA      PIC X.                                     00002780
           02 MWCTLINDC PIC X.                                          00002790
           02 MWCTLINDP PIC X.                                          00002800
           02 MWCTLINDH PIC X.                                          00002810
           02 MWCTLINDV PIC X.                                          00002820
           02 MWCTLINDO      PIC X.                                     00002830
      * NUMERO CONTROLE 1                                               00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MCCTLIND1A     PIC X.                                     00002860
           02 MCCTLIND1C     PIC X.                                     00002870
           02 MCCTLIND1P     PIC X.                                     00002880
           02 MCCTLIND1H     PIC X.                                     00002890
           02 MCCTLIND1V     PIC X.                                     00002900
           02 MCCTLIND1O     PIC X(3).                                  00002910
      * NUMERO CONTROLE 2                                               00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MCCTLIND2A     PIC X.                                     00002940
           02 MCCTLIND2C     PIC X.                                     00002950
           02 MCCTLIND2P     PIC X.                                     00002960
           02 MCCTLIND2H     PIC X.                                     00002970
           02 MCCTLIND2V     PIC X.                                     00002980
           02 MCCTLIND2O     PIC X(3).                                  00002990
      * NUMERO CONTROLE 3                                               00003000
           02 FILLER    PIC X(2).                                       00003010
           02 MCCTLIND3A     PIC X.                                     00003020
           02 MCCTLIND3C     PIC X.                                     00003030
           02 MCCTLIND3P     PIC X.                                     00003040
           02 MCCTLIND3H     PIC X.                                     00003050
           02 MCCTLIND3V     PIC X.                                     00003060
           02 MCCTLIND3O     PIC X(3).                                  00003070
      * NUMERO CONTROLE 4                                               00003080
           02 FILLER    PIC X(2).                                       00003090
           02 MCCTLIND4A     PIC X.                                     00003100
           02 MCCTLIND4C     PIC X.                                     00003110
           02 MCCTLIND4P     PIC X.                                     00003120
           02 MCCTLIND4H     PIC X.                                     00003130
           02 MCCTLIND4V     PIC X.                                     00003140
           02 MCCTLIND4O     PIC X(3).                                  00003150
      * NUMERO CONTROLE 5                                               00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MCCTLIND5A     PIC X.                                     00003180
           02 MCCTLIND5C     PIC X.                                     00003190
           02 MCCTLIND5P     PIC X.                                     00003200
           02 MCCTLIND5H     PIC X.                                     00003210
           02 MCCTLIND5V     PIC X.                                     00003220
           02 MCCTLIND5O     PIC X(3).                                  00003230
      * NOM TABLE ASSOCIE                                               00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MCTABASSA      PIC X.                                     00003260
           02 MCTABASSC PIC X.                                          00003270
           02 MCTABASSP PIC X.                                          00003280
           02 MCTABASSH PIC X.                                          00003290
           02 MCTABASSV PIC X.                                          00003300
           02 MCTABASSO      PIC X(6).                                  00003310
      * NOM SOUS TABLE ASSOCIE                                          00003320
           02 FILLER    PIC X(2).                                       00003330
           02 MCSTABASSA     PIC X.                                     00003340
           02 MCSTABASSC     PIC X.                                     00003350
           02 MCSTABASSP     PIC X.                                     00003360
           02 MCSTABASSH     PIC X.                                     00003370
           02 MCSTABASSV     PIC X.                                     00003380
           02 MCSTABASSO     PIC X(5).                                  00003390
      * ZONE CMD AIDA                                                   00003400
           02 FILLER    PIC X(2).                                       00003410
           02 MZONCMDA  PIC X.                                          00003420
           02 MZONCMDC  PIC X.                                          00003430
           02 MZONCMDP  PIC X.                                          00003440
           02 MZONCMDH  PIC X.                                          00003450
           02 MZONCMDV  PIC X.                                          00003460
           02 MZONCMDO  PIC X(15).                                      00003470
      * MESSAGE ERREUR                                                  00003480
           02 FILLER    PIC X(2).                                       00003490
           02 MLIBERRA  PIC X.                                          00003500
           02 MLIBERRC  PIC X.                                          00003510
           02 MLIBERRP  PIC X.                                          00003520
           02 MLIBERRH  PIC X.                                          00003530
           02 MLIBERRV  PIC X.                                          00003540
           02 MLIBERRO  PIC X(58).                                      00003550
      * CODE TRANSACTION                                                00003560
           02 FILLER    PIC X(2).                                       00003570
           02 MCODTRAA  PIC X.                                          00003580
           02 MCODTRAC  PIC X.                                          00003590
           02 MCODTRAP  PIC X.                                          00003600
           02 MCODTRAH  PIC X.                                          00003610
           02 MCODTRAV  PIC X.                                          00003620
           02 MCODTRAO  PIC X(4).                                       00003630
      * CICS DE TRAVAIL                                                 00003640
           02 FILLER    PIC X(2).                                       00003650
           02 MCICSA    PIC X.                                          00003660
           02 MCICSC    PIC X.                                          00003670
           02 MCICSP    PIC X.                                          00003680
           02 MCICSH    PIC X.                                          00003690
           02 MCICSV    PIC X.                                          00003700
           02 MCICSO    PIC X(5).                                       00003710
      * NETNAME                                                         00003720
           02 FILLER    PIC X(2).                                       00003730
           02 MNETNAMA  PIC X.                                          00003740
           02 MNETNAMC  PIC X.                                          00003750
           02 MNETNAMP  PIC X.                                          00003760
           02 MNETNAMH  PIC X.                                          00003770
           02 MNETNAMV  PIC X.                                          00003780
           02 MNETNAMO  PIC X(8).                                       00003790
      * CODE TERMINAL                                                   00003800
           02 FILLER    PIC X(2).                                       00003810
           02 MSCREENA  PIC X.                                          00003820
           02 MSCREENC  PIC X.                                          00003830
           02 MSCREENP  PIC X.                                          00003840
           02 MSCREENH  PIC X.                                          00003850
           02 MSCREENV  PIC X.                                          00003860
           02 MSCREENO  PIC X(5).                                       00003870
                                                                                
