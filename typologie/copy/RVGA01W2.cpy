      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE AV003 CACID/SOUS-LIEU  SECTION/SERV    *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
       01  RVGA01W2.                                                            
           05  AV003-CTABLEG2    PIC X(15).                                     
           05  AV003-CTABLEG2-REDEF REDEFINES AV003-CTABLEG2.                   
               10  AV003-NSOCIETE        PIC X(03).                             
               10  AV003-NLIEU           PIC X(03).                             
               10  AV003-CACID           PIC X(07).                             
           05  AV003-WTABLEG     PIC X(80).                                     
           05  AV003-WTABLEG-REDEF  REDEFINES AV003-WTABLEG.                    
               10  AV003-WFLAG           PIC X(01).                             
               10  AV003-NSECTION        PIC X(06).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01W2-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AV003-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  AV003-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AV003-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  AV003-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.
                                                                                
      *}                                                                        
