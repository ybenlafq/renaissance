      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MDCNS NMD : TABLE CONSO. AS400         *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01XC.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01XC.                                                            
      *}                                                                        
           05  MDCNS-CTABLEG2    PIC X(15).                                     
           05  MDCNS-CTABLEG2-REDEF REDEFINES MDCNS-CTABLEG2.                   
               10  MDCNS-NSOCP           PIC X(03).                             
               10  MDCNS-NLIEUP          PIC X(03).                             
               10  MDCNS-CFONC           PIC X(01).                             
               10  MDCNS-NSOCR           PIC X(03).                             
               10  MDCNS-NLIEUR          PIC X(03).                             
           05  MDCNS-WTABLEG     PIC X(80).                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01XC-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01XC-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MDCNS-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MDCNS-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MDCNS-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MDCNS-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
