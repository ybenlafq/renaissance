      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PRACT INNOVENTE - ACTIVATION SERVICE   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPRACT .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPRACT .                                                            
      *}                                                                        
           05  PRACT-CTABLEG2    PIC X(15).                                     
           05  PRACT-CTABLEG2-REDEF REDEFINES PRACT-CTABLEG2.                   
               10  PRACT-CACTIV          PIC X(15).                             
           05  PRACT-WTABLEG     PIC X(80).                                     
           05  PRACT-WTABLEG-REDEF  REDEFINES PRACT-WTABLEG.                    
               10  PRACT-WACTIF          PIC X(01).                             
               10  PRACT-PROTOC          PIC X(10).                             
               10  PRACT-CPRESTA         PIC X(17).                             
               10  PRACT-SUBTYPE         PIC X(10).                             
               10  PRACT-DOSSIER         PIC X(05).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPRACT-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPRACT-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRACT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PRACT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRACT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PRACT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
