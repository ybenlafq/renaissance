      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * TS SPECIFIQUE TABLE RTGA59                                 *    00020000
      *       TR : GA00                                            *    00030000
      *       PG : TGA17 DEF DES FAMILLES : CODES DESCRIPTIFS      *    00040000
      **************************************************************    00050000
      *                                                                 00060000
      * ZONES RESERVEES APPLICATIVES ----------------------------- 100  00070000
      *                                                                 00080000
      *------------------------------ LONGUEUR                          00090000
       01  TS-LONG              PIC S9(3) COMP-3 VALUE 700.             00100001
       01  TS-DONNEES.                                                  00110000
           05 TS-GA17-LIGNE   OCCURS 14.                                00120000
      *------------------------------ CODE DESCRIPTIF                   00130000
              10 TS-CDESCR      PIC X(05).                              00140000
      *------------------------------ LIBELLE DU CODE                   00150000
              10 TS-LDESCR      PIC X(20).                              00160000
      *------------------------------ CODE DESCRIPTIF                   00170000
              10 TS-CVDESCR     PIC X(05).                              00180000
      *------------------------------ LIBELLE DU CODE                   00190000
              10 TS-LVDESCR     PIC X(20).                              00200000
                                                                                
