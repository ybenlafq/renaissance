      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EQUIG EQUIPE GENERALITES               *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVGA01CS.                                                            
           05  EQUIG-CTABLEG2    PIC X(15).                                     
           05  EQUIG-CTABLEG2-REDEF REDEFINES EQUIG-CTABLEG2.                   
               10  EQUIG-CEQUIQ          PIC X(05).                             
           05  EQUIG-WTABLEG     PIC X(80).                                     
           05  EQUIG-WTABLEG-REDEF  REDEFINES EQUIG-WTABLEG.                    
               10  EQUIG-LEQUIP          PIC X(20).                             
               10  EQUIG-WEQPOU          PIC X(01).                             
               10  EQUIG-WMIXTE          PIC X(01).                             
               10  EQUIG-WTYPEQUI        PIC X(05).                             
               10  EQUIG-WSM             PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01CS-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EQUIG-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EQUIG-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EQUIG-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EQUIG-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
