      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : VISUALISATION DU REFERENTIEL / FILIALE (GN40B)         *         
      *   PAGINATION PAR PF10/PF11                                    *         
      *****************************************************************         
      *                                                               *         
       01  TS-GN40B.                                                            
      *----------------------------------  LONGUEUR TS                          
           02 TS-GN40B-LONG                PIC S9(03) COMP-3                    
                                           VALUE +70.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-GN40B-DONNEES.                                                 
      *----------------------------------  INDICATEUR OFFRE ACTIVE              
              03 TS-GN40B-WOA              PIC X(1).                            
      *----------------------------------  CODIC DE SUBSTITUTION                
              03 TS-GN40B-NCODICS          PIC X(07).                           
      *----------------------------------  DATE D'EFFET OFFRE ACTIVE            
              03 TS-GN40B-DEFFETOA         PIC X(8).                            
      *----------------------------------  PRIX DE REFERENCE                    
              03 TS-GN40B-PREFTTC1         PIC S9(5)V99.                        
      *----------------------------------  CODE IMPERATIF                       
              03 TS-GN40B-WACTIF1          PIC X(01).                           
      *----------------------------------  DATE EFFET PRIX DE REFERENCE         
              03 TS-GN40B-DEFFET1          PIC X(8).                            
      *----------------------------------  COMMENTAIRE PRIX DE REFERENCE        
              03 TS-GN40B-LCOMMENT1        PIC X(10).                           
      *----------------------------------  PRIX DE REFERENC 2                   
              03 TS-GN40B-PREFTTC2         PIC S9(5)V99.                        
      *----------------------------------  CODE IMPERATI 2                      
              03 TS-GN40B-WACTIF2          PIC X(01).                           
      *----------------------------------  DATE EFFET PRIX DE REF               
              03 TS-GN40B-DEFFET2          PIC X(8).                            
      *----------------------------------  COMMENTAIRE PRIX DE REF2             
              03 TS-GN40B-LCOMMENT2        PIC X(10).                           
                                                                                
