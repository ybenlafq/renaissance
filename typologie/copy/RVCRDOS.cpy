      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CRDOS RéFéRENCEMENT DES DOSSIERS       *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCRDOS.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCRDOS.                                                             
      *}                                                                        
           05  CRDOS-CTABLEG2    PIC X(15).                                     
           05  CRDOS-CTABLEG2-REDEF REDEFINES CRDOS-CTABLEG2.                   
               10  CRDOS-CDOSSIER        PIC X(05).                             
           05  CRDOS-WTABLEG     PIC X(80).                                     
           05  CRDOS-WTABLEG-REDEF  REDEFINES CRDOS-WTABLEG.                    
               10  CRDOS-WACTIF          PIC X(01).                             
               10  CRDOS-LDOSSIER        PIC X(20).                             
               10  CRDOS-CMARQ           PIC X(05).                             
               10  CRDOS-CTYPE           PIC X(05).                             
               10  CRDOS-CAPPLI          PIC X(05).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCRDOS-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCRDOS-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CRDOS-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CRDOS-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CRDOS-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CRDOS-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
