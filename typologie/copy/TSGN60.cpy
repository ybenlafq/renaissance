      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : CONCURRENT NATIONAL / LOCAL            (GN60)          *         
      *****************************************************************         
      *                                                               *         
       01  TS-GN60.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GN60-LONG                 PIC S9(03) COMP-3                    
                                           VALUE +66.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-GN60-DONNEES.                                                  
      *----------------------------------  ZONE DE PRIX                         
              03 TS-GN60-NZONPRIX          PIC X(3).                            
      *----------------------------------  CODE CONCURRENT LOCAL                
              03 TS-GN60-NCONCL            PIC X(4).                            
      *----------------------------------  LIBELLE CONCURRENT LOCAL             
              03 TS-GN60-LCONCL            PIC X(15).                           
      *----------------------------------  VILLE CONCURRENT                     
              03 TS-GN60-LVILLE            PIC X(18).                           
      *----------------------------------  CODE CONCURRENT                      
              03 TS-GN60-NCONCN            PIC X(05).                           
      *----------------------------------  LIBELLE CONCURRENT                   
              03 TS-GN60-LCONCN            PIC X(20).                           
      *----------------------------------  FLAG DE MODIFICATION                 
              03 TS-GN60-LIGNE-MODIFIEE    PIC X(01).                           
                                                                                
