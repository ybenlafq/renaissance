      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGA2500                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA2500                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA2500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA2500.                                                            
      *}                                                                        
           02  GA25-CMARKETING                                                  
               PIC X(0005).                                                     
           02  GA25-CVMARKETING                                                 
               PIC X(0005).                                                     
           02  GA25-CDESCRIPTIF                                                 
               PIC X(0005).                                                     
           02  GA25-CVDESCRIPT                                                  
               PIC X(0005).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGA2500                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA2500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA2500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA25-CMARKETING-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA25-CMARKETING-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA25-CVMARKETING-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA25-CVMARKETING-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA25-CDESCRIPTIF-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA25-CDESCRIPTIF-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA25-CVDESCRIPT-F                                                
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GA25-CVDESCRIPT-F                                                
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
