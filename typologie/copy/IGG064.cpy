      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGG064 AU 20/03/2007  *        
      *                                                                *        
      *          CRITERES DE TRI  07,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGG064.                                                        
            05 NOMETAT-IGG064           PIC X(6) VALUE 'IGG064'.                
            05 RUPTURES-IGG064.                                                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGG064-SEQUENCE           PIC S9(04) COMP.                007  002
      *--                                                                       
           10 IGG064-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGG064.                                                   
           10 IGG064-CMARQ              PIC X(05).                      009  005
           10 IGG064-LREFFOURN          PIC X(20).                      014  020
           10 IGG064-MLIBANO            PIC X(30).                      034  030
           10 IGG064-MORG               PIC X(02).                      064  002
           10 IGG064-NCDERET            PIC X(10).                      066  010
           10 IGG064-NCODIC             PIC X(07).                      076  007
           10 IGG064-NPOSTE             PIC X(05).                      083  005
           10 IGG064-NSOC               PIC X(03).                      088  003
           10 IGG064-MSTOCKDEB          PIC S9(09)     .                091  009
           10 IGG064-NMAJOR             PIC S9(07)V9(2).                100  009
           10 IGG064-NMINOR             PIC S9(07)V9(2).                109  009
            05 FILLER                      PIC X(395).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGG064-LONG           PIC S9(4)   COMP  VALUE +117.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGG064-LONG           PIC S9(4) COMP-5  VALUE +117.           
                                                                                
      *}                                                                        
