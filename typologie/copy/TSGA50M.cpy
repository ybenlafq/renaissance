      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ******************************************************************        
      * TS DE MENU                                                              
       01 TS-GA50-MENU.                                                         
      * LONGUEUR TS                                                             
           05 TS-GA50-MENU-LONG PIC S9(03) COMP-3 VALUE +22.                    
      * DONNEES                                                                 
           05 TS-GA50-MENU-DONNEES.                                             
               10 TS-GA50-MENU-NOPTION PIC 9(2).                                
               10 TS-GA50-MENU-LOPTION PIC X(20).                               
                                                                                
