      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
       01  Z-COMMAREA-MSE52.                                            00000020
           02 COMM-MSE52-ENTETE-RETOUR.                                 00000030
      *---- BOOLEEN TYPE D'ERREUR                                       00000040
              03 COMM-MSE52-TYPERR     PIC 9(01).                       00000050
                 88 MSE52-NO-ERROR       VALUE 0.                       00000060
                 88 MSE52-APPL-ERROR     VALUE 1.                       00000070
                 88 MSE52-DB2-ERROR      VALUE 2.                       00000080
              03 COMM-MSE52-FUNC-SQL   PIC X(08).                       00000090
              03 COMM-MSE52-TABLE-NAME PIC X(08).                       00000100
              03 COMM-MSE52-SQLCODE    PIC S9(04).                      00000110
              03 COMM-MSE52-POSITION   PIC  9(03).                      00000120
      *---- BOOLEEN CODE FONCTION                                       00000130
           02 COMM-MSE52-FONC      PIC 9(01).                           00000140
              88 MSE52-CTRL-DATA     VALUE 0.                           00000150
              88 MSE52-MAJ-DB2       VALUE 1.                           00000160
      *---- DONNEES EN ENTREE / SORTIE                                  00000170
           02 COMM-MSE52-ZINOUT.                                        00000180
              03 COMM-MSE52-CODESFONCTION   PIC X(12).                  00000190
              03 COMM-MSE52-WFONC           PIC X(03).                  00000200
              03 COMM-MSE52-NCSERV          PIC X(05).                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-MSE52-NBR-OCCURS      PIC S9(04)   COMP.          00000220
      *--                                                                       
              03 COMM-MSE52-NBR-OCCURS      PIC S9(04) COMP-5.                  
      *}                                                                        
              03 COMM-MSE52-TABLEAU.                                    00000230
                 05 COMM-MSE52-LIGNE       OCCURS 50.                   00000240
                    10 COMM-MSE52-MCCDESC   PIC X(05).                  00000250
                    10 COMM-MSE52-MLCDESC   PIC X(20).                  00000260
                    10 COMM-MSE52-MCVDESC   PIC X(05).                  00000270
                    10 COMM-MSE52-MLVDESC   PIC X(20).                  00000280
      *------------ CODE RETOUR POSITIONNEMENT DES ERREURS              00000290
                    10 COMM-MSE52-CVD-ERR   PIC X(04).                  00000300
                                                                        00000310
           02 COMM-MSE52-FILLER             PIC X(2249).                00000320
                                                                                
