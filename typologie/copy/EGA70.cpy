      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA70   EGA70                                              00000020
      ***************************************************************** 00000030
       01   EGA70I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * CODE FONCTIO                                                    00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODFCTL  COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MCODFCTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODFCTF  PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MCODFCTI  PIC X(3).                                       00000200
      * CODE TABLE                                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTABL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCODTABL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTABF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCODTABI  PIC X(6).                                       00000250
      * N� CONTROLE                                                     00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMCTRL  COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MNUMCTRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNUMCTRF  PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MNUMCTRI  PIC X(3).                                       00000300
      * ZONE CMD AID                                                    00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MZONCMDI  PIC X(15).                                      00000350
      * MESSAGE ERREA                                                   00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MLIBERRI  PIC X(74).                                      00000400
      * CODE TRANSACTION                                                00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCODTRAI  PIC X(4).                                       00000450
      * CICS DE TRAVAIL                                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MCICSI    PIC X(5).                                       00000500
      * NETNAME                                                         00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MNETNAMI  PIC X(8).                                       00000550
      * CODE TERMINAL                                                   00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MSCREENI  PIC X(5).                                       00000600
      ***************************************************************** 00000610
      * SDF: EGA70   EGA70                                              00000620
      ***************************************************************** 00000630
       01   EGA70O REDEFINES EGA70I.                                    00000640
           02 FILLER    PIC X(12).                                      00000650
      * DATE DU JOUR                                                    00000660
           02 FILLER    PIC X(2).                                       00000670
           02 MDATJOUA  PIC X.                                          00000680
           02 MDATJOUC  PIC X.                                          00000690
           02 MDATJOUP  PIC X.                                          00000700
           02 MDATJOUH  PIC X.                                          00000710
           02 MDATJOUV  PIC X.                                          00000720
           02 MDATJOUO  PIC X(10).                                      00000730
      * HEURE                                                           00000740
           02 FILLER    PIC X(2).                                       00000750
           02 MTIMJOUA  PIC X.                                          00000760
           02 MTIMJOUC  PIC X.                                          00000770
           02 MTIMJOUP  PIC X.                                          00000780
           02 MTIMJOUH  PIC X.                                          00000790
           02 MTIMJOUV  PIC X.                                          00000800
           02 MTIMJOUO  PIC X(5).                                       00000810
      * CODE FONCTIO                                                    00000820
           02 FILLER    PIC X(2).                                       00000830
           02 MCODFCTA  PIC X.                                          00000840
           02 MCODFCTC  PIC X.                                          00000850
           02 MCODFCTP  PIC X.                                          00000860
           02 MCODFCTH  PIC X.                                          00000870
           02 MCODFCTV  PIC X.                                          00000880
           02 MCODFCTO  PIC X(3).                                       00000890
      * CODE TABLE                                                      00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MCODTABA  PIC X.                                          00000920
           02 MCODTABC  PIC X.                                          00000930
           02 MCODTABP  PIC X.                                          00000940
           02 MCODTABH  PIC X.                                          00000950
           02 MCODTABV  PIC X.                                          00000960
           02 MCODTABO  PIC X(6).                                       00000970
      * N� CONTROLE                                                     00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MNUMCTRA  PIC X.                                          00001000
           02 MNUMCTRC  PIC X.                                          00001010
           02 MNUMCTRP  PIC X.                                          00001020
           02 MNUMCTRH  PIC X.                                          00001030
           02 MNUMCTRV  PIC X.                                          00001040
           02 MNUMCTRO  PIC X(3).                                       00001050
      * ZONE CMD AID                                                    00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MZONCMDA  PIC X.                                          00001080
           02 MZONCMDC  PIC X.                                          00001090
           02 MZONCMDP  PIC X.                                          00001100
           02 MZONCMDH  PIC X.                                          00001110
           02 MZONCMDV  PIC X.                                          00001120
           02 MZONCMDO  PIC X(15).                                      00001130
      * MESSAGE ERREA                                                   00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MLIBERRA  PIC X.                                          00001160
           02 MLIBERRC  PIC X.                                          00001170
           02 MLIBERRP  PIC X.                                          00001180
           02 MLIBERRH  PIC X.                                          00001190
           02 MLIBERRV  PIC X.                                          00001200
           02 MLIBERRO  PIC X(74).                                      00001210
      * CODE TRANSACTION                                                00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MCODTRAA  PIC X.                                          00001240
           02 MCODTRAC  PIC X.                                          00001250
           02 MCODTRAP  PIC X.                                          00001260
           02 MCODTRAH  PIC X.                                          00001270
           02 MCODTRAV  PIC X.                                          00001280
           02 MCODTRAO  PIC X(4).                                       00001290
      * CICS DE TRAVAIL                                                 00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MCICSA    PIC X.                                          00001320
           02 MCICSC    PIC X.                                          00001330
           02 MCICSP    PIC X.                                          00001340
           02 MCICSH    PIC X.                                          00001350
           02 MCICSV    PIC X.                                          00001360
           02 MCICSO    PIC X(5).                                       00001370
      * NETNAME                                                         00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MNETNAMA  PIC X.                                          00001400
           02 MNETNAMC  PIC X.                                          00001410
           02 MNETNAMP  PIC X.                                          00001420
           02 MNETNAMH  PIC X.                                          00001430
           02 MNETNAMV  PIC X.                                          00001440
           02 MNETNAMO  PIC X(8).                                       00001450
      * CODE TERMINAL                                                   00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MSCREENA  PIC X.                                          00001480
           02 MSCREENC  PIC X.                                          00001490
           02 MSCREENP  PIC X.                                          00001500
           02 MSCREENH  PIC X.                                          00001510
           02 MSCREENV  PIC X.                                          00001520
           02 MSCREENO  PIC X(5).                                       00001530
                                                                                
