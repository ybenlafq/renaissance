      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      *        IDENTIFICATION SERVICE - CODES DESCRIPTIFS             * 00000020
      ***************************************************************** 00000030
      *                                                               * 00000040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS-53-LONG          PIC S9(4) COMP VALUE +0380.              00000050
      *--                                                                       
       01  TS-53-LONG          PIC S9(4) COMP-5 VALUE +0380.                    
      *}                                                                        
       01  TS-53-DATA.                                                  00000060
           05 TS-53-LIGNE.                                              00000070
              10 TS-POSTE-TSBS53    OCCURS 2.                           00000080
                 15 TS-53-DATA-FIXE.                                    00000090
                    20 TS-53-NSEQENT             PIC XX.                00000100
                    20 TS-53-NSEQENTORIG         PIC XX.                00000110
                    20 TS-53-CPROFLIEN           PIC X(05).             00000120
                    20 TS-53-LPROFLIEN           PIC X(34).             00000130
                    20 TS-53-CTYPENT2            PIC X(02).             00000140
                    20 TS-53-CONTROLE2           PIC X(07).             00000150
                 15 TS-53-QDELTACLI              PIC S999V99 COMP-3.    00000160
                 15 TS-53-DATA-F.                                       00000170
                    20 TS-53-BLOCSEQ-F           PIC X.                 00000180
                    20 TS-53-WCONTUNI-F          PIC X.                 00000190
                    20 TS-53-WCONTFAM-F          PIC X.                 00000200
                    20 TS-53-NENTITE2-F          PIC X.                 00000210
                    20 TS-53-NSEQENTREF-F        PIC X.                 00000220
                    20 TS-53-DATES-F             PIC X.                 00000230
                 15 TS-53-DATA-AV.                                      00000240
                    20 TS-53-BLOCSEQ-AV          PIC XXX.               00000250
                    20 TS-53-NENTITE2-AV         PIC X(07).             00000260
                    20 TS-53-LENTITE2-AV         PIC X(34).             00000270
                    20 TS-53-NSEQENTREF-AV       PIC X(02).             00000280
                    20 TS-53-DEFFET-AV           PIC X(08).             00000290
                    20 TS-53-DFINEFFET-AV        PIC X(08).             00000300
                 15 TS-53-DATA-AP.                                      00000310
                    20 TS-53-BLOCSEQ-AP          PIC X(03).             00000320
                    20 TS-53-NENTITE2-AP         PIC X(07).             00000330
                    20 TS-53-LENTITE2-AP         PIC X(34).             00000340
                    20 TS-53-NSEQENTREF-AP       PIC X(02).             00000350
                    20 TS-53-DEFFET-AP           PIC X(08).             00000360
                    20 TS-53-DFINEFFET-AP        PIC X(08).             00000370
                 15 TS-53-FLAG-CTRL.                                    00000380
                    20 TS-53-CDRET-ENTITE        PIC X(04).             00000390
                 15 TS-53-FLAG-MAJ.                                     00000400
                    20 TS-53-WMAJTS              PIC X(01).             00000410
                                                                                
