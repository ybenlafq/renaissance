      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PPCTR CODE TYPES DE RUBRIQUES          *        
      *----------------------------------------------------------------*        
       01  RVGA01RT.                                                            
           05  PPCTR-CTABLEG2    PIC X(15).                                     
           05  PPCTR-CTABLEG2-REDEF REDEFINES PPCTR-CTABLEG2.                   
               10  PPCTR-CTYPRUB         PIC X(01).                             
           05  PPCTR-WTABLEG     PIC X(80).                                     
           05  PPCTR-WTABLEG-REDEF  REDEFINES PPCTR-WTABLEG.                    
               10  PPCTR-WACTIF          PIC X(01).                             
               10  PPCTR-LTYPRUB         PIC X(30).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01RT-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PPCTR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PPCTR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PPCTR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PPCTR-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
