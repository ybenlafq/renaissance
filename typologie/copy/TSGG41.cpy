      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
       01  TSGG41.                                                              
           05 TSGG41-DONNEES.                                                   
              10  TSGG41-PRIEX           PIC S9(7)V99 COMP-3.                   
              10  TSGG41-DDPRIEX         PIC X(08).                             
              10  TSGG41-DFPRIEX         PIC X(08).                             
              10  TSGG41-DDEXCEP         PIC X(08).                             
              10  TSGG41-DFEXCEP         PIC X(08).                             
              10  TSGG41-PMBUEX          PIC S9(5)V99 COMP-3.                   
              10  TSGG41-COMEX           PIC S9(4)V9 COMP-3.                    
              10  TSGG41-COMADAPT        PIC S9(4)V9 COMP-3.                    
              10  TSGG41-DDCOMEX         PIC X(08).                             
              10  TSGG41-DFCOMEX         PIC X(08).                             
              10  TSGG41-EDMEX           PIC S9(5)V99 COMP-3.                   
              10  TSGG41-NCONC           PIC X(04).                             
              10  TSGG41-LCONC           PIC X(12).                             
              10  TSGG41-LIEU OCCURS 20.                                        
                  15  TSGG41-NLIEU       PIC X(03).                             
              10  TSGG41-PRIEX-AV        PIC X(08).                             
              10  TSGG41-DDPRIEX-AV      PIC X(08).                             
              10  TSGG41-DFPRIEX-AV      PIC X(08).                             
              10  TSGG41-COMEX-AV        PIC X(05).                             
              10  TSGG41-DDCOMEX-AV      PIC X(08).                             
              10  TSGG41-DFCOMEX-AV      PIC X(08).                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       10  TSGG41-NBMAG-AV        PIC S9(4) COMP.                        
      *--                                                                       
              10  TSGG41-NBMAG-AV        PIC S9(4) COMP-5.                      
      *}                                                                        
              10  TSGG41-LIEU-AV         OCCURS 20.                             
                  15  TSGG41-NLIEU-AV    PIC X(03).                             
              10  TSGG41-TOP-PRIEX       PIC X.                                 
                  88  TSGG41-PRIEX-EXISTANT              VALUE 'O'.             
                  88  TSGG41-PRIEX-PAS-EXISTANT          VALUE 'N'.             
              10  TSGG41-TOP-PRIMEX      PIC X.                                 
                  88  TSGG41-PRIMEX-EXISTANT             VALUE 'O'.             
                  88  TSGG41-PRIMEX-PAS-EXISTANT         VALUE 'N'.             
              10  TSGG41-MAJ-PRIEX       PIC X.                                 
              10  TSGG41-CANNPRIEX       PIC X.                                 
              10  TSGG41-ANNUL-PRIEX     PIC X.                                 
              10  TSGG41-MAJ-COMEX       PIC X.                                 
              10  TSGG41-CANNCOMEX       PIC X.                                 
              10  TSGG41-ANNUL-COMEX     PIC X.                                 
                                                                                
