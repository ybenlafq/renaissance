      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                               *         
       01  TS-GG55.                                                             
           02 TS-GG55-LONG          PIC S9(03) COMP-3  VALUE +71.               
           02 TS-GG55-DONNEES.                                                  
              03 TS-GG55-COL1       PIC X(04).                                  
              03 TS-GG55-TOP-COL1   PIC X(01).                                  
              03 TS-GG55-ACT1       PIC X(01).                                  
              03 TS-GG55-PRIX       PIC X(08).                                  
              03 TS-GG55-WREF       PIC X(01).                                  
              03 TS-GG55-CDPRX      PIC X(03).                                  
              03 TS-GG55-DEFFET     PIC X(08).                                  
              03 TS-GG55-COMMENT    PIC X(20).                                  
              03 TS-GG55-COL2       PIC X(01).                                  
              03 TS-GG55-ACT2       PIC X(01).                                  
              03 TS-GG55-PCOMM      PIC X(06).                                  
              03 TS-GG55-DPCOMM     PIC X(08).                                  
              03 TS-GG55-STOCKM     PIC X(04).                                  
              03 TS-GG55-VTE4S      PIC X(04).                                  
              03 TS-GG55-WLIGNE     PIC X(01).                                  
                                                                                
