      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IBS030 AU 30/10/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,03,PD,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,05,BI,A,                          *        
      *                           25,05,BI,A,                          *        
      *                           30,05,PD,A,                          *        
      *                           35,20,BI,A,                          *        
      *                           55,05,BI,A,                          *        
      *                           60,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IBS030.                                                        
            05 NOMETAT-IBS030           PIC X(6) VALUE 'IBS030'.                
            05 RUPTURES-IBS030.                                                 
           10 IBS030-CHEFPROD           PIC X(05).                      007  005
           10 IBS030-WSEQFAM            PIC S9(05)      COMP-3.         012  003
           10 IBS030-CMARQ              PIC X(05).                      015  005
           10 IBS030-NSEQCTYPPREST      PIC X(05).                      020  005
           10 IBS030-CTYPPREST          PIC X(05).                      025  005
           10 IBS030-MONTANT            PIC S9(07)V9(2) COMP-3.         030  005
           10 IBS030-LPRESTATION        PIC X(20).                      035  020
           10 IBS030-CPRESTATION        PIC X(05).                      055  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IBS030-SEQUENCE           PIC S9(04) COMP.                060  002
      *--                                                                       
           10 IBS030-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IBS030.                                                   
           10 IBS030-CFAM               PIC X(05).                      062  005
           10 IBS030-LCHEFPROD          PIC X(20).                      067  020
           10 IBS030-LFAM               PIC X(20).                      087  020
           10 IBS030-LLIST1             PIC X(20).                      107  020
           10 IBS030-LLIST2             PIC X(20).                      127  020
           10 IBS030-LLIST3             PIC X(20).                      147  020
           10 IBS030-LMARQ              PIC X(20).                      167  020
           10 IBS030-LTYPPREST          PIC X(20).                      187  020
           10 IBS030-NLIST1             PIC X(07).                      207  007
           10 IBS030-NLIST2             PIC X(07).                      214  007
           10 IBS030-NLIST3             PIC X(07).                      221  007
           10 IBS030-WPRODMAJ           PIC X(01).                      228  001
           10 IBS030-MTPRIME            PIC S9(03)V9(2) COMP-3.         229  003
            05 FILLER                      PIC X(281).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IBS030-LONG           PIC S9(4)   COMP  VALUE +231.           
      *                                                                         
      *--                                                                       
        01  DSECT-IBS030-LONG           PIC S9(4) COMP-5  VALUE +231.           
                                                                                
      *}                                                                        
