      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVBS0400                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVBS0400                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVBS0400.                                                    00000090
           10 BS04-NLIST           PIC X(7).                            00000100
           10 BS04-CFAM            PIC X(5).                            00000110
           10 BS04-CDESCRIPTIF     PIC X(5).                            00000120
           10 BS04-CVDESCRIPT      PIC X(5).                            00000130
           10 BS04-DSYST           PIC S9(13)V USAGE COMP-3.            00000140
      *                                                                 00000150
      *---------------------------------------------------------        00000160
      *   LISTE DES FLAGS DE LA TABLE RVBS0400                          00000170
      *---------------------------------------------------------        00000180
      *                                                                 00000190
       01  RVBS0400-FLAGS.                                              00000200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS04-NLIST-F         PIC S9(4) COMP.                      00000210
      *--                                                                       
           10 BS04-NLIST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS04-CFAM-F          PIC S9(4) COMP.                      00000220
      *--                                                                       
           10 BS04-CFAM-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS04-CDESCRIPTIF-F   PIC S9(4) COMP.                      00000230
      *--                                                                       
           10 BS04-CDESCRIPTIF-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS04-CVDESCRIPT-F    PIC S9(4) COMP.                      00000240
      *--                                                                       
           10 BS04-CVDESCRIPT-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS04-DSYST-F         PIC S9(4) COMP.                      00000250
      *                                                                         
      *--                                                                       
           10 BS04-DSYST-F         PIC S9(4) COMP-5.                            
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
