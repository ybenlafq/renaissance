      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE VESPE CODE VENDEURS SPECIAUX           *        
      *----------------------------------------------------------------*        
            EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVGA01H5.                                                            
           05  VESPE-CTABLEG2    PIC X(15).                                     
           05  VESPE-CTABLEG2-REDEF REDEFINES VESPE-CTABLEG2.                   
               10  VESPE-CVESPE          PIC X(06).                             
           05  VESPE-WTABLEG     PIC X(80).                                     
           05  VESPE-WTABLEG-REDEF  REDEFINES VESPE-WTABLEG.                    
               10  VESPE-LVESPE          PIC X(30).                             
               10  VESPE-WFLAG           PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01H5-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  VESPE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  VESPE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  VESPE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  VESPE-WTABLEG-F   PIC S9(4) COMP-5.                              
            EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
