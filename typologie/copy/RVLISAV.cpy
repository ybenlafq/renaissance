      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE LISAV SAV DELIVERY CODE                *        
      *----------------------------------------------------------------*        
       01  RVLISAV .                                                            
           05  LISAV-CTABLEG2    PIC X(15).                                     
           05  LISAV-CTABLEG2-REDEF REDEFINES LISAV-CTABLEG2.                   
               10  LISAV-SAVTYP          PIC X(05).                             
           05  LISAV-WTABLEG     PIC X(80).                                     
           05  LISAV-WTABLEG-REDEF  REDEFINES LISAV-WTABLEG.                    
               10  LISAV-LIBSAVT         PIC X(17).                             
               10  LISAV-TRISEQ          PIC X(01).                             
               10  LISAV-TRISEQ-N       REDEFINES LISAV-TRISEQ                  
                                         PIC 9(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVLISAV-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LISAV-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  LISAV-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LISAV-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  LISAV-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
