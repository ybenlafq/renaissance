      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00010000
      * LONGUEUR 3509                                                   00010000
      *                                                                 00010000
              03 COMMGG51  REDEFINES COMM-GG50-FILLER.                  00970008
      * 3509 - 58 = 3451                                                00010000
                 05  COMM-GG51-FILLER    PIC X(3451).                   00970009
                 05  COMM-GG51-APPLI REDEFINES COMM-GG51-FILLER.        00970009
                     10  COMM-GG51-MAP.                                         
      *                                                                 00010000
      * ENTETE                                                          00010000
                             20  COMM-GG51-PCHT    PIC S9(7)V99 COMP-3.         
                             20  COMM-GG51-SRPHT   PIC S9(7)V99 COMP-3.         
                             20  COMM-GG51-WSENSAPP PIC X.                      
                             20  COMM-GG51-OANAT   PIC X.                       
                             20  COMM-GG51-OAZP    PIC X.                       
                             20  COMM-GG51-DDOAZP  PIC X(8).                    
                             20  COMM-GG51-DFOAZP  PIC X(8).                    
                             20  COMM-GG51-WACTSUP PIC X.                       
      *                                                                 00010000
      * ZONE DE REFERENCE                                               00010000
      *                   ACTUEL                                        00010000
                             20  COMM-GG51-PRIRAC  PIC S9(7)V99 COMP-3.         
                             20  COMM-GG51-WIMPAC  PIC X.                       
                             20  COMM-GG51-WMAXAC  PIC X.                       
                             20  COMM-GG51-DDREFA  PIC X(8).                    
                             20  COMM-GG51-DFREFA  PIC X(8).                    
      *                   FUTUR                                                 
                             20  COMM-GG51-PRIRNO  PIC S9(7)V99 COMP-3.         
                             20  COMM-GG51-WIMPN   PIC X.                       
                             20  COMM-GG51-WMAXN   PIC X.                       
                             20  COMM-GG51-DDREFN  PIC X(8).                    
                             20  COMM-GG51-DFREFN  PIC X(8).                    
      *                                                                 00010000
      * ZONE DE REFERENCE 2                                             00010000
      *                   ACTUEL                                        00010000
                             20  COMM-GG51-PRIR2AC PIC S9(7)V99 COMP-3.         
                             20  COMM-GG51-WBLOCAC PIC X.                       
                             20  COMM-GG51-DDREF2A PIC X(8).                    
                             20  COMM-GG51-DFREF2A PIC X(8).                    
      *                   FUTUR                                                 
                             20  COMM-GG51-PRIR2NO PIC S9(7)V99 COMP-3.         
                             20  COMM-GG51-WBLOCN  PIC X.                       
                             20  COMM-GG51-DDREF2N PIC X(8).                    
                             20  COMM-GG51-DFREF2N PIC X(8).                    
      *                                                                 00010000
      * ZONE DU MAGASIN                                                 00010000
      *                   ANCIEN                                                
                             20  COMM-GG51-NZONPMO PIC XX.                      
                             20  COMM-GG51-PRIMO   PIC S9(7)V99 COMP-3.         
                             20  COMM-GG51-PCOMMO  PIC S9(4)V9  COMP-3.         
                             20  COMM-GG51-INTFAMO PIC S9(4)V9  COMP-3.         
                             20  COMM-GG51-DDPRIMO PIC X(8).                    
                             20  COMM-GG51-PMBUMO  PIC S9(5)V99 COMP-3.         
                             20  COMM-GG51-QTMARMO PIC S9(4)V9  COMP-3.         
                             20  COMM-GG51-FMILMO  PIC S9(5)V99 COMP-3.         
                             20  COMM-GG51-LCOMMMO PIC X(15).                   
      *                   ACTUEL                                        00010000
                             20  COMM-GG51-NZONPMA PIC XX.                      
                             20  COMM-GG51-PRIMAC  PIC S9(7)V99 COMP-3.         
                             20  COMM-GG51-PCOMMAC PIC S9(4)V9  COMP-3.         
                             20  COMM-GG51-INTFAMA PIC S9(4)V9  COMP-3.         
                             20  COMM-GG51-DPRIMAC PIC X(8).                    
                             20  COMM-GG51-PMBUMAC PIC S9(5)V99 COMP-3.         
                             20  COMM-GG51-QTMARMA PIC S9(4)V9  COMP-3.         
                             20  COMM-GG51-FMILMAC PIC S9(5)V99 COMP-3.         
                             20  COMM-GG51-LCOMMMA PIC X(15).                   
                             20  COMM-GG51-WEXISTA PIC X.                       
      *                   FUTUR                                         00010000
                             20  COMM-GG51-NZONPMN PIC XX.                      
                             20  COMM-GG51-PRIMNO  PIC S9(7)V99 COMP-3.         
                             20  COMM-GG51-PCOMMNO PIC S9(4)V9  COMP-3.         
                             20  COMM-GG51-INTFAMN PIC S9(4)V9  COMP-3.         
                             20  COMM-GG51-DPRIMNO PIC X(8).                    
                             20  COMM-GG51-PMBUMNO PIC S9(5)V99 COMP-3.         
                             20  COMM-GG51-QTMARMN PIC S9(4)V9  COMP-3.         
                             20  COMM-GG51-FMILMNO PIC S9(5)V99 COMP-3.         
                             20  COMM-GG51-LCOMMMN PIC X(15).                   
      * PRIX ET PRIMES EXCEPTIONNELS                                    00010000
                             20  COMM-GG51-PAGE    PIC 99.                      
                             20  COMM-GG51-PAGETOT PIC 99.                      
                             20  COMM-GG51-NZONPEX PIC XX.                      
                             20  COMM-GG51-PRIEX   PIC S9(7)V99 COMP-3.         
                             20  COMM-GG51-COMEX   PIC S9(4)V9 COMP-3.          
                             20  COMM-GG51-DPRIEX  PIC X(8).                    
                             20  COMM-GG51-PMBUEX  PIC S9(5)V99 COMP-3.         
                             20  COMM-GG51-QTMAREX PIC S9(4)V9  COMP-3.         
                             20  COMM-GG51-FMILEX  PIC S9(5)V99 COMP-3.         
                             20  COMM-GG51-DFIEFEX PIC X(8).                    
                             20  COMM-GG51-NCONC   PIC X(4).                    
                             20  COMM-GG51-LCONC   PIC X(15).                   
                             20  COMM-GG51-LIEU OCCURS 10.                      
                                 30  COMM-GG51-NLIEU   PIC X(3).                
      *                                                                 00010000
                             20  COMM-GG51-NCODIS  PIC X(7).                    
                     10  COMM-GG51-SIMI.                                        
                             20  COMM-GG51-ART-SIM PIC 9(7) OCCURS 20.          
                             20  COMM-GG51-NBARTSIM PIC S9(3) COMP-3.           
                             20  COMM-GG51-CPTSIM   PIC S9(3) COMP-3.           
                     10  COMM-GG51-RESTE.                                       
                             20  COMM-GG51-QTAUXTVA PIC S9V9(4) COMP-3.         
                             20  COMM-GG51-FMILMAX  PIC 99999.                  
                             20  COMM-GG51-PGM      PIC X(5).                   
                             20  COMM-GG51-CCODGROUP PIC X.                     
                             20  COMM-GG51-TABLE-GROUP.                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                          25  COMM-GG51-NBR-GROUP PIC S9(4) COMP.        
      *--                                                                       
                                 25  COMM-GG51-NBR-GROUP PIC S9(4)              
                                                                 COMP-5.        
      *}                                                                        
                                 25  COMM-GG51-TAB-GROUP OCCURS 290.            
                                     30  COMM-GG51-TAB-NCODIC PIC 9(7).         
                                     30  COMM-GG51-TAB-QLIENS                   
                                              PIC S9(3) COMP-3.                 
                                     30  COMM-GG51-GRP-OU-ELT                   
                                              PIC X.                            
                             20  COMM-GG51-PASSAGE-1  PIC X.                    
                             20  COMM-GG51-PASSAGE-2  PIC X.                    
                             20  COMM-GG51-DATEFFET   PIC X(8).                 
                             20  COMM-GG51-PVTTC   PIC 9(7)V99 COMP-3.          
                             20  COMM-GG51-PRIX-MOD   PIC X.                    
                             20  COMM-GG51-DATE-MOD   PIC X.                    
                             20  COMM-GG51-COMM-MOD   PIC X.                    
                             20  COMM-GG51-LCOM-MOD   PIC X.                    
                             20  COMM-GG51-OFACT-MOD  PIC X.                    
                             20  COMM-GG51-MAJ-PRIX   PIC X.                    
                             20  COMM-GG51-MAJ-PRIXEX PIC X.                    
                             20  COMM-GG51-MAJ-COMEX  PIC X.                    
                                                                                
