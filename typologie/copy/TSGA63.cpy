      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00002000
      * TS SPECIFIQUE TABLE TSGA63 : CARACTERISTIQUES SPECIFIQUES  *    00002200
      *                              DE L ARTICLE                  *    00002300
      *       TR : GA50  IDENTIFICATION ARTICLE                    *    00002400
      *       PG : TGA63 SAISIE, MISE A JOUR DES DONNEES DE VENTE  *    00002500
      **************************************************************    00002600
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ------------------------------ 40  00009004
      *                                                                 00410100
      * PROGRAMME  TGA63 : SAISIE , MISE A JOUR DES DONNEES DE VENTE  * 00411000
      *                                                                 00412000
       01  TS-GA63.                                                     00420000
      *------------------------------ LONGUEUR                          00430000
           05 TS-GA63-LONG            PIC S9(3) COMP-3 VALUE 40.        00440003
      *------------------------------ DONNEES TS                        00440105
           05 TS-GA63-DONNEES.                                          00440200
      *------------------------------ DONNEES TABLE                     00440305
              10 TS-GA63-TABLE.                                         00440405
      *------------------------------ CODIC                             00521000
                 15 TS-GA63-NCODIC    PIC X(7).                         00522005
      *------------------------------ CODE CARACTERISTIQUE SPECIFIQUE   00523000
                 15 TS-GA63-CARACTSPE PIC X(5).                         00524005
      *------------------------------ CODE MISE A JOUR                  00561100
                 15 TS-GA63-CMAJ      PIC X(1).                         00561205
      *------------------------------ LIBELLE CARACTERISTIQUE SPE.      00561303
              10 TS-GA63-LCARACTSPE   PIC X(20).                        00561405
      *------------------------------ RESERVE                           00561500
              10 TS-GA63-FILLER       PIC X(7).                         00561605
      *                                                                 00562000
      ***************************************************************** 00740000
                                                                                
