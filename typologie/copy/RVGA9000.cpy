      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGA9000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA9000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGA9000.                                                            
           02  GA90-NCODIC                                                      
               PIC X(0007).                                                     
           02  GA90-NRUBRREM                                                    
               PIC X(0006).                                                     
           02  GA90-NRUBRVEN                                                    
               PIC X(0006).                                                     
           02  GA90-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGA9000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGA9000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA90-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA90-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA90-NRUBRREM-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA90-NRUBRREM-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA90-NRUBRVEN-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA90-NRUBRVEN-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA90-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA90-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
