      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PSGRC REGROUPEMENT DE CONTRATS         *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01PG.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01PG.                                                            
      *}                                                                        
           05  PSGRC-CTABLEG2    PIC X(15).                                     
           05  PSGRC-CTABLEG2-REDEF REDEFINES PSGRC-CTABLEG2.                   
               10  PSGRC-CGCPLT          PIC X(05).                             
               10  PSGRC-TGRP            PIC X(01).                             
           05  PSGRC-WTABLEG     PIC X(80).                                     
           05  PSGRC-WTABLEG-REDEF  REDEFINES PSGRC-WTABLEG.                    
               10  PSGRC-CGRP            PIC X(05).                             
               10  PSGRC-WACTIF          PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01PG-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01PG-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PSGRC-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PSGRC-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PSGRC-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PSGRC-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
