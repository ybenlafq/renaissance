      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TGA10 (TGA00 -> MENU)    TR: GA00  *    00002209
      *                 DEFINITION DES MAGASINS  PAR SOCIETE       *    00002309
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00009000
      *                                                                 00410100
      *            TRANSACTION GA10 : DEFINITION DES MAGASINS         * 00411009
      *                                                                 00412000
          02 COMM-GA10-APPLI REDEFINES COMM-GA00-APPLI.                 00420013
      *------------------------------ ZONE DONNEES TGA10                00510018
             03 COMM-GA10-DONNEES-TGA10.                                00520018
      *------------------------------ CODE FONCTION                     00521018
                04 COMM-GA10-FONCT          PIC XXX.                    00522018
      *------------------------------ LIBELLE FONCTION                  00530009
                04 COMM-GA10-LIBFCT            PIC X(13).               00540018
      *------------------------------ CODE SOCIETE                      00550009
                04 COMM-GA10-CODSOC         PIC XXX.                    00560018
      *------------------------------ LIBELLE SOCIETE                   00570009
                04 COMM-GA10-LIBSOC            PIC X(20).               00580018
      *------------------------------ TYPE DE LIEU                      00580019
                04 COMM-GA10-CTYPL             PIC X(1).                00580020
      *------------------------------ MAGASIN PCF                       00580030
                04 COMM-GA10-PCFMAG            PIC X(3).                00580040
      *------------------------------ POSTES MAGASIN                    00714109
                04 COMM-GA10-POSMAG         OCCURS 13.                  00714218
      *------------------------------ CODE MAGASIN                      00715009
                   05 COMM-GA10-CODMAG         PIC X(3).                00716018
      *------------------------------ LIBELLE MAGASIN                   00717009
                   05 COMM-GA10-LIBMAG         PIC X(20).               00718018
      *------------------------------ DATE OUVERTURE                    00718109
                   05 COMM-GA10-DATOUV.                                 00718218
                      06 COMM-GA10-DATOUV-JJ   PIC X(2).                00718318
                      06 COMM-GA10-DATOUV-MM   PIC X(2).                00718518
                      06 COMM-GA10-DATOUV-AA   PIC X(2).                00718718
      *------------------------------ DATE FERMETURE                    00718809
                   05 COMM-GA10-DATFER.                                 00718918
                      06 COMM-GA10-DATFER-JJ   PIC X(2).                00719018
                      06 COMM-GA10-DATFER-MM   PIC X(2).                00719218
                      06 COMM-GA10-DATFER-AA   PIC X(2).                00719418
      *------------------------------                                   00720009
                   05 COMM-GA10-FILLER         PIC X(2).                00729018
      *------------------------------ ZONE DE PRIX PRESTATION           00720009
                   05 COMM-GA10-ZPPREST        PIC X(2).                00729018
      *------------------------------ ZONE DE PRIX                      00720009
                   05 COMM-GA10-ZONPRI         PIC X(2).                00729018
      *------------------------------ GROUPE DE MAGASIN                 00729109
                   05 COMM-GA10-GRPMAG         PIC X(2).                00729218
      *------------------------------ CODE EXPO STANDARD                00729309
                   05 COMM-GA10-EXPSTD         PIC X(5).                00729418
      *------------------------------ DACEM                             00729419
                   05 COMM-GA10-WDACEM         PIC X(1).                00729420
      *------------------------------ CACID                             00729430
                   05 COMM-GA10-CACID          PIC X(4).                00729440
      *------------------------------ TABLE CODE MAGASIN PR PAGINATION  00729509
                04 COMM-GA10-TABMAG         OCCURS 100.                 00729618
      *------------------------------ 1ERS CODES MAGASIN DES PAGES      00729709
                   05 COMM-GA10-CODTAB         PIC X(3).                00729818
      *------------------------------ INDICATEUR ETAT FICHIER           00729915
                04 COMM-GA10-INDPAG            PIC 9(3).                00730018
      *------------------------------ DERNIER NUMERO MAG AFFICHE        00730309
                04 COMM-GA10-PAGSUI            PIC X(3).                00730418
      *------------------------------ ANCIEN NUMERO MAG AFFICHE         00730509
                04 COMM-GA10-PAGENC            PIC X(3).                00730618
      *------------------------------ NUMERO PAGE ECRAN                 00730710
                04 COMM-GA10-NUMPAG            PIC 9(3).                00730818
      *------------------------------ ZONE DONNEES TEX00                00730918
             03 COMM-GA10-DONNEES-TEX00.                                00731018
      *------------------------------ INDICATEUR MAJ TEX00 OK           00731117
                04 COMM-GA10-FMAJ-TEX          PIC 9.                   00731218
      *------------------------------ NUMERO CARTE TEX00 DE REFERENCE   00731317
                04 COMM-GA10-NUMCAR-TEX-REF    PIC 9(2).                00731418
      *------------------------------ ZONE PR TRANSFERT PARAMETRES      00731509
                04 COMM-GA10-PARAM-TEX.                                 00731618
      *------------------------------ ZONE CLE                          00731709
                   05 COMM-GA10-CLE-TEX.                                00731818
      *------------------------------ DATE                              00731909
                      06 COMM-GA10-DAT-TEX.                             00732018
      *------------------------------ DATE ANNEE                        00732109
                         08 COMM-GA10-AA-TEX  PIC X(2).                 00732218
      *------------------------------ DATE MOIS                         00732309
                         08 COMM-GA10-MM-TEX  PIC X(2).                 00732418
      *------------------------------ DATE JOUR                         00732509
                         08 COMM-GA10-JJ-TEX  PIC X(2).                 00732618
      *------------------------------ NOM DU PROGRAMME                  00732709
                      06 COMM-GA10-NOMPRG-TEX    PIC X(5).              00732818
      *------------------------------ NUMERO DE TACHE                   00732909
                      06 COMM-GA10-NUMTAC-TEX    PIC 9(2).              00733018
      *------------------------------ NUMERO DE CARTE                   00733109
                      06 COMM-GA10-NUMCAR-TEX    PIC 9(2).              00733218
      *------------------------------ CODE DE MISE A JOUR (G/I/D/R)     00733309
                   05 COMM-GA10-CODMAJ-TEX  PIC X.                      00733418
      *------------------------------ CODE RETOUR (00/99/98)            00733509
                   05 COMM-GA10-CODRET-TEX  PIC 99.                     00733618
      *------------------------------ LIBRE                             00733709
                   05 COMM-GA10-LIBRE1-TEX  PIC X(15).                  00733818
      *------------------------------ ZONE DE PARAMETRES                00733909
                   05 COMM-GA10-ZONPAR-TEX.                             00734018
      *------------------------------ CODE SOCIETE                      00734116
                      06 COMM-GA10-CODSOC-TEX    PIC 9(3).              00734218
      *------------------------------ NUMERO DE MAGASIN                 00734309
                      06 COMM-GA10-NUMMAG-TEX    PIC 9(3).              00734418
      *------------------------------ TYPE EXPO                         00734509
                      06 COMM-GA10-TYPEXP-TEX    PIC X(5).              00734618
      *------------------------------ LIBRE                             00734709
                      06 COMM-GA10-LIBRE2-TEX     PIC X(69).            00734818
      *------------------------------ LIBRE                             00734911
                   05 COMM-GA10-LIBRE3-TEX  PIC X(7).                   00735018
      *------------------------------ ZONE LIBRE                        00735100
             03 COMM-GA10-SWAP           PIC X OCCURS 200.              00736019
      *------------------------------ ZONE GROUPE DE MUTATION           00737019
             03 COMM-GA10-GRPMUT         PIC X(2) OCCURS 13.            00738019
      *------------------------------ TYPE DE MAGASIN                   00729430
             03 COMM-GA10-CTYPM          PIC X(3) OCCURS 13.            00729440
      *------------------------------ ZONE LIBRE                        00738020
             03 COMM-GA10-LIBRE          PIC X(2292).                   00738030
      ***************************************************************** 00740000
                                                                                
