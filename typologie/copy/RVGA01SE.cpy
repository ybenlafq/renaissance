      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PRDET RATTACHEMENT ETAT AU GROUPE      *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01SE.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01SE.                                                            
      *}                                                                        
           05  PRDET-CTABLEG2    PIC X(15).                                     
           05  PRDET-CTABLEG2-REDEF REDEFINES PRDET-CTABLEG2.                   
               10  PRDET-CETAT           PIC X(06).                             
           05  PRDET-WTABLEG     PIC X(80).                                     
           05  PRDET-WTABLEG-REDEF  REDEFINES PRDET-WTABLEG.                    
               10  PRDET-CGRPETAT        PIC X(05).                             
               10  PRDET-WACTIF          PIC X(01).                             
               10  PRDET-LETAT           PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01SE-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01SE-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRDET-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PRDET-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRDET-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PRDET-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
