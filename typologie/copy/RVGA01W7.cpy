      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PROST PROFIL SANS CALENDRIER           *        
      *----------------------------------------------------------------*        
       01  RVGA01W7.                                                            
           05  PROST-CTABLEG2    PIC X(15).                                     
           05  PROST-CTABLEG2-REDEF REDEFINES PROST-CTABLEG2.                   
               10  PROST-NSOCIETE        PIC X(03).                             
               10  PROST-PROFIL          PIC X(05).                             
           05  PROST-WTABLEG     PIC X(80).                                     
           05  PROST-WTABLEG-REDEF  REDEFINES PROST-WTABLEG.                    
               10  PROST-ACTIF           PIC X(01).                             
               10  PROST-NBJN            PIC X(01).                             
               10  PROST-NBJN-N         REDEFINES PROST-NBJN                    
                                         PIC 9(01).                             
               10  PROST-NBJM            PIC X(01).                             
               10  PROST-NBJM-N         REDEFINES PROST-NBJM                    
                                         PIC 9(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01W7-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PROST-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PROST-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PROST-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PROST-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
