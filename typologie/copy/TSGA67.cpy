      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ****************************************************************          
      *  TR GA50  : IDENTIFICATION ARTICLE                                      
      *  PG TGA67 : PRESTATIONS LIEES A UN ARTICLE                              
      ****************************************************************          
       01  TS-GA67.                                                             
         05  TS-GA67-LONG           PIC S9(4) COMP-3 VALUE 70.                  
         05  TS-GA67-DONNEES.                                                   
           10  TS-GA67-WPRODMAJ     PIC  X(1).                                  
           10  TS-GA67-WCALCUL      PIC  X(1).                                  
           10  TS-GA67-CPRESTATION  PIC  X(5).                                  
           10  TS-GA67-LPRESTATION  PIC  X(20).                                 
           10  TS-GA67-CFOURN       PIC  X(05).                                 
           10  TS-GA67-TYPE         PIC  X(01).                                 
           10  TS-GA67-MTPRIME-X.                                               
               15  TS-GA67-MTPRIME  PIC  S9(7)V99 COMP-3.                       
           10  TS-GA67-MTPREST-X.                                               
               15  TS-GA67-MTPREST  PIC  S9(7)V99 COMP-3.                       
           10  TS-GA67-WLIEN2       PIC  X(1).                                  
           10  TS-GA67-DEFFET2      PIC  X(8).                                  
           10  TS-GA67-WLIEN-A      PIC  X(1).                                  
           10  TS-GA67-DEFFET-A     PIC  X(8).                                  
           10  TS-GA67-WLIEN1       PIC  X(1).                                  
           10  TS-GA67-DEFFET1      PIC  X(8).                                  
                                                                                
