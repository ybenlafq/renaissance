      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CETAT CODES ETAT                       *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01BI.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01BI.                                                            
      *}                                                                        
           05  CETAT-CTABLEG2    PIC X(15).                                     
           05  CETAT-CTABLEG2-REDEF REDEFINES CETAT-CTABLEG2.                   
               10  CETAT-CETAT           PIC X(10).                             
           05  CETAT-WTABLEG     PIC X(80).                                     
           05  CETAT-WTABLEG-REDEF  REDEFINES CETAT-WTABLEG.                    
               10  CETAT-LIBELLE         PIC X(54).                             
               10  CETAT-CTYPE           PIC X(01).                             
               10  CETAT-WFLAGMGI        PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01BI-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01BI-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CETAT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CETAT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CETAT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CETAT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
