      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GCASI SIGNATURE DES UTILISATEURS GCA   *        
      *----------------------------------------------------------------*        
       01  RVGA01N2.                                                            
           05  GCASI-CTABLEG2    PIC X(15).                                     
           05  GCASI-CTABLEG2-REDEF REDEFINES GCASI-CTABLEG2.                   
               10  GCASI-SIGN            PIC X(07).                             
           05  GCASI-WTABLEG     PIC X(80).                                     
           05  GCASI-WTABLEG-REDEF  REDEFINES GCASI-WTABLEG.                    
               10  GCASI-USER            PIC X(25).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01N2-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GCASI-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GCASI-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GCASI-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GCASI-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
