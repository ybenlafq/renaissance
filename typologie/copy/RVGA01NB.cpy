           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FGAFR AFFEC.SERV.DES FACTURES RECUES   *        
      *----------------------------------------------------------------*        
       01  RVGA01NB.                                                            
           05  FGAFR-CTABLEG2    PIC X(15).                                     
           05  FGAFR-CTABLEG2-REDEF REDEFINES FGAFR-CTABLEG2.                   
               10  FGAFR-NSOC            PIC X(03).                             
           EXEC SQL END DECLARE SECTION END-EXEC.
               10  FGAFR-NSOC-N         REDEFINES FGAFR-NSOC                    
                                         PIC 9(03).                             
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  FGAFR-NATURE          PIC X(05).                             
               10  FGAFR-CVENT           PIC X(05).                             
           05  FGAFR-WTABLEG     PIC X(80).                                     
           05  FGAFR-WTABLEG-REDEF  REDEFINES FGAFR-WTABLEG.                    
               10  FGAFR-WACTIF          PIC X(01).                             
               10  FGAFR-SERVDEST        PIC X(05).                             
               10  FGAFR-WTRANSPO        PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01NB-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGAFR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FGAFR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGAFR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FGAFR-WTABLEG-F   PIC S9(4) COMP-5.                              
      *}
           EXEC SQL END DECLARE SECTION END-EXEC.
 
                                                                                
