      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESE00   ESE00                                              00000020
      ***************************************************************** 00000030
       01   ESE00I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPSERVML    COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MCTYPSERVML COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCTYPSERVMF    PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCTYPSERVMI    PIC X.                                     00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCSERV1L      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNCSERV1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCSERV1F      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCSERV1I      PIC X(5).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPSERVOL    COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCTYPSERVOL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCTYPSERVOF    PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCTYPSERVOI    PIC X.                                     00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAM1L   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAM1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAM1F   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAM1I   PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAM1L   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLFAM1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLFAM1F   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLFAM1I   PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPSERVCL    COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MCTYPSERVCL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCTYPSERVCF    PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCTYPSERVCI    PIC X.                                     00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPER1L  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCOPER1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCOPER1F  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCOPER1I  PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLOPER1L  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLOPER1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLOPER1F  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLOPER1I  PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCASSORTC1L    COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MCASSORTC1L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCASSORTC1F    PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCASSORTC1I    PIC X(5).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLASSORTC1L    COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MLASSORTC1L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLASSORTC1F    PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLASSORTC1I    PIC X(20).                                 00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MPAGEI    PIC X(3).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MPAGEMAXI      PIC X(3).                                  00000650
           02 MLIGNEI OCCURS   10 TIMES .                               00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCFAMI  PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOPERL      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MCOPERL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCOPERF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MCOPERI      PIC X(5).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSEGMENTL    COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MSEGMENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSEGMENTF    PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MSEGMENTI    PIC X(20).                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCSERVL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MNCSERVL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCSERVF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MNCSERVI     PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCSERVL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MLCSERVL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLCSERVF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MLCSERVI     PIC X(20).                                 00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCASSORTCL   COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MCASSORTCL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCASSORTCF   PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MCASSORTCI   PIC X(5).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTL   COMP PIC S9(4).                                 00000910
      *--                                                                       
             03 MACTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MACTF   PIC X.                                          00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MACTI   PIC X(3).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MZONCMDI  PIC X(15).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MLIBERRI  PIC X(58).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCODTRAI  PIC X(4).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCICSI    PIC X(5).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MNETNAMI  PIC X(8).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MSCREENI  PIC X(4).                                       00001180
      ***************************************************************** 00001190
      * SDF: ESE00   ESE00                                              00001200
      ***************************************************************** 00001210
       01   ESE00O REDEFINES ESE00I.                                    00001220
           02 FILLER    PIC X(12).                                      00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MDATJOUA  PIC X.                                          00001250
           02 MDATJOUC  PIC X.                                          00001260
           02 MDATJOUP  PIC X.                                          00001270
           02 MDATJOUH  PIC X.                                          00001280
           02 MDATJOUV  PIC X.                                          00001290
           02 MDATJOUO  PIC X(10).                                      00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MTIMJOUA  PIC X.                                          00001320
           02 MTIMJOUC  PIC X.                                          00001330
           02 MTIMJOUP  PIC X.                                          00001340
           02 MTIMJOUH  PIC X.                                          00001350
           02 MTIMJOUV  PIC X.                                          00001360
           02 MTIMJOUO  PIC X(5).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MWFONCA   PIC X.                                          00001390
           02 MWFONCC   PIC X.                                          00001400
           02 MWFONCP   PIC X.                                          00001410
           02 MWFONCH   PIC X.                                          00001420
           02 MWFONCV   PIC X.                                          00001430
           02 MWFONCO   PIC X(3).                                       00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MCTYPSERVMA    PIC X.                                     00001460
           02 MCTYPSERVMC    PIC X.                                     00001470
           02 MCTYPSERVMP    PIC X.                                     00001480
           02 MCTYPSERVMH    PIC X.                                     00001490
           02 MCTYPSERVMV    PIC X.                                     00001500
           02 MCTYPSERVMO    PIC X.                                     00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MNCSERV1A      PIC X.                                     00001530
           02 MNCSERV1C PIC X.                                          00001540
           02 MNCSERV1P PIC X.                                          00001550
           02 MNCSERV1H PIC X.                                          00001560
           02 MNCSERV1V PIC X.                                          00001570
           02 MNCSERV1O      PIC X(5).                                  00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MCTYPSERVOA    PIC X.                                     00001600
           02 MCTYPSERVOC    PIC X.                                     00001610
           02 MCTYPSERVOP    PIC X.                                     00001620
           02 MCTYPSERVOH    PIC X.                                     00001630
           02 MCTYPSERVOV    PIC X.                                     00001640
           02 MCTYPSERVOO    PIC X.                                     00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MCFAM1A   PIC X.                                          00001670
           02 MCFAM1C   PIC X.                                          00001680
           02 MCFAM1P   PIC X.                                          00001690
           02 MCFAM1H   PIC X.                                          00001700
           02 MCFAM1V   PIC X.                                          00001710
           02 MCFAM1O   PIC X(5).                                       00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MLFAM1A   PIC X.                                          00001740
           02 MLFAM1C   PIC X.                                          00001750
           02 MLFAM1P   PIC X.                                          00001760
           02 MLFAM1H   PIC X.                                          00001770
           02 MLFAM1V   PIC X.                                          00001780
           02 MLFAM1O   PIC X(20).                                      00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MCTYPSERVCA    PIC X.                                     00001810
           02 MCTYPSERVCC    PIC X.                                     00001820
           02 MCTYPSERVCP    PIC X.                                     00001830
           02 MCTYPSERVCH    PIC X.                                     00001840
           02 MCTYPSERVCV    PIC X.                                     00001850
           02 MCTYPSERVCO    PIC X.                                     00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MCOPER1A  PIC X.                                          00001880
           02 MCOPER1C  PIC X.                                          00001890
           02 MCOPER1P  PIC X.                                          00001900
           02 MCOPER1H  PIC X.                                          00001910
           02 MCOPER1V  PIC X.                                          00001920
           02 MCOPER1O  PIC X(5).                                       00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MLOPER1A  PIC X.                                          00001950
           02 MLOPER1C  PIC X.                                          00001960
           02 MLOPER1P  PIC X.                                          00001970
           02 MLOPER1H  PIC X.                                          00001980
           02 MLOPER1V  PIC X.                                          00001990
           02 MLOPER1O  PIC X(20).                                      00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MCASSORTC1A    PIC X.                                     00002020
           02 MCASSORTC1C    PIC X.                                     00002030
           02 MCASSORTC1P    PIC X.                                     00002040
           02 MCASSORTC1H    PIC X.                                     00002050
           02 MCASSORTC1V    PIC X.                                     00002060
           02 MCASSORTC1O    PIC X(5).                                  00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MLASSORTC1A    PIC X.                                     00002090
           02 MLASSORTC1C    PIC X.                                     00002100
           02 MLASSORTC1P    PIC X.                                     00002110
           02 MLASSORTC1H    PIC X.                                     00002120
           02 MLASSORTC1V    PIC X.                                     00002130
           02 MLASSORTC1O    PIC X(20).                                 00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MPAGEA    PIC X.                                          00002160
           02 MPAGEC    PIC X.                                          00002170
           02 MPAGEP    PIC X.                                          00002180
           02 MPAGEH    PIC X.                                          00002190
           02 MPAGEV    PIC X.                                          00002200
           02 MPAGEO    PIC X(3).                                       00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MPAGEMAXA      PIC X.                                     00002230
           02 MPAGEMAXC PIC X.                                          00002240
           02 MPAGEMAXP PIC X.                                          00002250
           02 MPAGEMAXH PIC X.                                          00002260
           02 MPAGEMAXV PIC X.                                          00002270
           02 MPAGEMAXO      PIC X(3).                                  00002280
           02 MLIGNEO OCCURS   10 TIMES .                               00002290
             03 FILLER       PIC X(2).                                  00002300
             03 MCFAMA  PIC X.                                          00002310
             03 MCFAMC  PIC X.                                          00002320
             03 MCFAMP  PIC X.                                          00002330
             03 MCFAMH  PIC X.                                          00002340
             03 MCFAMV  PIC X.                                          00002350
             03 MCFAMO  PIC X(5).                                       00002360
             03 FILLER       PIC X(2).                                  00002370
             03 MCOPERA      PIC X.                                     00002380
             03 MCOPERC PIC X.                                          00002390
             03 MCOPERP PIC X.                                          00002400
             03 MCOPERH PIC X.                                          00002410
             03 MCOPERV PIC X.                                          00002420
             03 MCOPERO      PIC X(5).                                  00002430
             03 FILLER       PIC X(2).                                  00002440
             03 MSEGMENTA    PIC X.                                     00002450
             03 MSEGMENTC    PIC X.                                     00002460
             03 MSEGMENTP    PIC X.                                     00002470
             03 MSEGMENTH    PIC X.                                     00002480
             03 MSEGMENTV    PIC X.                                     00002490
             03 MSEGMENTO    PIC X(20).                                 00002500
             03 FILLER       PIC X(2).                                  00002510
             03 MNCSERVA     PIC X.                                     00002520
             03 MNCSERVC     PIC X.                                     00002530
             03 MNCSERVP     PIC X.                                     00002540
             03 MNCSERVH     PIC X.                                     00002550
             03 MNCSERVV     PIC X.                                     00002560
             03 MNCSERVO     PIC X(5).                                  00002570
             03 FILLER       PIC X(2).                                  00002580
             03 MLCSERVA     PIC X.                                     00002590
             03 MLCSERVC     PIC X.                                     00002600
             03 MLCSERVP     PIC X.                                     00002610
             03 MLCSERVH     PIC X.                                     00002620
             03 MLCSERVV     PIC X.                                     00002630
             03 MLCSERVO     PIC X(20).                                 00002640
             03 FILLER       PIC X(2).                                  00002650
             03 MCASSORTCA   PIC X.                                     00002660
             03 MCASSORTCC   PIC X.                                     00002670
             03 MCASSORTCP   PIC X.                                     00002680
             03 MCASSORTCH   PIC X.                                     00002690
             03 MCASSORTCV   PIC X.                                     00002700
             03 MCASSORTCO   PIC X(5).                                  00002710
             03 FILLER       PIC X(2).                                  00002720
             03 MACTA   PIC X.                                          00002730
             03 MACTC   PIC X.                                          00002740
             03 MACTP   PIC X.                                          00002750
             03 MACTH   PIC X.                                          00002760
             03 MACTV   PIC X.                                          00002770
             03 MACTO   PIC X(3).                                       00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MZONCMDA  PIC X.                                          00002800
           02 MZONCMDC  PIC X.                                          00002810
           02 MZONCMDP  PIC X.                                          00002820
           02 MZONCMDH  PIC X.                                          00002830
           02 MZONCMDV  PIC X.                                          00002840
           02 MZONCMDO  PIC X(15).                                      00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MLIBERRA  PIC X.                                          00002870
           02 MLIBERRC  PIC X.                                          00002880
           02 MLIBERRP  PIC X.                                          00002890
           02 MLIBERRH  PIC X.                                          00002900
           02 MLIBERRV  PIC X.                                          00002910
           02 MLIBERRO  PIC X(58).                                      00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MCODTRAA  PIC X.                                          00002940
           02 MCODTRAC  PIC X.                                          00002950
           02 MCODTRAP  PIC X.                                          00002960
           02 MCODTRAH  PIC X.                                          00002970
           02 MCODTRAV  PIC X.                                          00002980
           02 MCODTRAO  PIC X(4).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MCICSA    PIC X.                                          00003010
           02 MCICSC    PIC X.                                          00003020
           02 MCICSP    PIC X.                                          00003030
           02 MCICSH    PIC X.                                          00003040
           02 MCICSV    PIC X.                                          00003050
           02 MCICSO    PIC X(5).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MNETNAMA  PIC X.                                          00003080
           02 MNETNAMC  PIC X.                                          00003090
           02 MNETNAMP  PIC X.                                          00003100
           02 MNETNAMH  PIC X.                                          00003110
           02 MNETNAMV  PIC X.                                          00003120
           02 MNETNAMO  PIC X(8).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MSCREENA  PIC X.                                          00003150
           02 MSCREENC  PIC X.                                          00003160
           02 MSCREENP  PIC X.                                          00003170
           02 MSCREENH  PIC X.                                          00003180
           02 MSCREENV  PIC X.                                          00003190
           02 MSCREENO  PIC X(4).                                       00003200
                                                                                
