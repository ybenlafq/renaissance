      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
000010*                                                                         
000020******************************************************************        
000030* PREPARATION DE LA TRACE SQL POUR LE MODELE NV1000                       
000040******************************************************************        
000050*                                                                         
000060 CLEF-NV1000             SECTION.                                         
000070*                                                                         
000080     MOVE 'RVNV1000'       TO   TABLE-NAME.                               
000090     MOVE 'NV1000'         TO   MODEL-NAME.                               
000100*                                                                         
000110 FIN-CLEF-NV1000. EXIT.                                                   
000120          EJECT                                                           
                                                                                
