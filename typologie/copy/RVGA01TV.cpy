      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TFM04 LIBELLE DEVISE ISSU RTFM04       *        
      *----------------------------------------------------------------*        
       01  RVGA01TV.                                                            
           05  TFM04-CTABLEG2    PIC X(15).                                     
           05  TFM04-CTABLEG2-REDEF REDEFINES TFM04-CTABLEG2.                   
               10  TFM04-CLE             PIC X(12).                             
           05  TFM04-WTABLEG     PIC X(80).                                     
           05  TFM04-WTABLEG-REDEF  REDEFINES TFM04-WTABLEG.                    
               10  TFM04-DEBZONES        PIC X(16).                             
               10  TFM04-SUITZONE        PIC X(17).                             
               10  TFM04-NBDECIM         PIC S9(01)       COMP-3.               
               10  TFM04-PSEURO          PIC X(01).                             
               10  TFM04-ARRONDI         PIC S9(01)V9(02)     COMP-3.           
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01TV-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TFM04-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TFM04-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TFM04-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TFM04-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
