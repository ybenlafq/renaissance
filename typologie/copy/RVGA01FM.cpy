      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE OPER  CODE OPERATION DE REGUL          *        
      *----------------------------------------------------------------*        
       01  RVGA01FM.                                                            
           05  OPER-CTABLEG2     PIC X(15).                                     
           05  OPER-CTABLEG2-REDEF  REDEFINES OPER-CTABLEG2.                    
               10  OPER-COPER            PIC X(10).                             
           05  OPER-WTABLEG      PIC X(80).                                     
           05  OPER-WTABLEG-REDEF   REDEFINES OPER-WTABLEG.                     
               10  OPER-LOPER            PIC X(20).                             
               10  OPER-WACTIVE          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01FM-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  OPER-CTABLEG2-F   PIC S9(4)  COMP.                               
      *--                                                                       
           05  OPER-CTABLEG2-F   PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  OPER-WTABLEG-F    PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  OPER-WTABLEG-F    PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
