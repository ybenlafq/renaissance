      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TPROD PROFIL TOURNEE / DETAILS         *        
      *----------------------------------------------------------------*        
       01  RVGA01HN.                                                            
           05  TPROD-CTABLEG2    PIC X(15).                                     
           05  TPROD-CTABLEG2-REDEF REDEFINES TPROD-CTABLEG2.                   
               10  TPROD-CPROF           PIC X(05).                             
               10  TPROD-CMODDEL         PIC X(03).                             
               10  TPROD-CPLAGE          PIC X(02).                             
               10  TPROD-CEQUIP          PIC X(05).                             
           05  TPROD-WTABLEG     PIC X(80).                                     
           05  TPROD-WTABLEG-REDEF  REDEFINES TPROD-WTABLEG.                    
               10  TPROD-WACTIF          PIC X(01).                             
               10  TPROD-HEURE           PIC X(04).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01HN-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TPROD-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TPROD-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TPROD-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TPROD-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
