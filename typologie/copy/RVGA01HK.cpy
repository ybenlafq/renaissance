      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TPLAN PLAN CONSTITUTION TOURNEE        *        
      *----------------------------------------------------------------*        
       01  RVGA01HK.                                                            
           05  TPLAN-CTABLEG2    PIC X(15).                                     
           05  TPLAN-CTABLEG2-REDEF REDEFINES TPLAN-CTABLEG2.                   
               10  TPLAN-CPLAN           PIC X(05).                             
           05  TPLAN-WTABLEG     PIC X(80).                                     
           05  TPLAN-WTABLEG-REDEF  REDEFINES TPLAN-WTABLEG.                    
               10  TPLAN-LPLAN           PIC X(20).                             
               10  TPLAN-A               PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01HK-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TPLAN-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TPLAN-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TPLAN-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TPLAN-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
