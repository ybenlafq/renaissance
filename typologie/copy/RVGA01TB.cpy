      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GMPRI MAG PRINCIPAL PAR ZONE DE PRIX   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01TB.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01TB.                                                            
      *}                                                                        
           05  GMPRI-CTABLEG2    PIC X(15).                                     
           05  GMPRI-CTABLEG2-REDEF REDEFINES GMPRI-CTABLEG2.                   
               10  GMPRI-NZONPRI         PIC X(02).                             
           05  GMPRI-WTABLEG     PIC X(80).                                     
           05  GMPRI-WTABLEG-REDEF  REDEFINES GMPRI-WTABLEG.                    
               10  GMPRI-LIEU            PIC X(03).                             
               10  GMPRI-WFLAG           PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01TB-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01TB-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GMPRI-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GMPRI-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GMPRI-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GMPRI-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
