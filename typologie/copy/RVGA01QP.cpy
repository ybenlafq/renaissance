      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PPSTA PERIODES TRAITEES PAR LES STAT   *        
      *----------------------------------------------------------------*        
       01  RVGA01QP.                                                            
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  PPSTA-CTABLEG2    PIC X(15).                                     
           05  PPSTA-CTABLEG2-REDEF REDEFINES PPSTA-CTABLEG2.                   
               10  PPSTA-NSOCIETE        PIC X(03).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  PPSTA-NSOCIETE-N     REDEFINES PPSTA-NSOCIETE                
                                         PIC 9(03).                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  PPSTA-WTABLEG     PIC X(80).                                     
           05  PPSTA-WTABLEG-REDEF  REDEFINES PPSTA-WTABLEG.                    
               10  PPSTA-DMOIS           PIC X(06).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  PPSTA-DMOIS-N        REDEFINES PPSTA-DMOIS                   
                                         PIC 9(06).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01QP-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PPSTA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PPSTA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PPSTA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PPSTA-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
