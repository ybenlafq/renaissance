      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************00000010
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *00000020
      ******************************************************************00000030
      *                                                                *00000040
      *  PROJET     :                                                   00000050
      *  PROGRAMME  : MEG40                                            *00000060
      *  TITRE      : COMMAREA POUR MODULE D'EDITION GENERALISEE       *00000070
      *  LONGUEUR   : 4096 OCTETS                                      *00000100
      *                                                                *00000110
      ******************************************************************00000120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-MEG4-LONG-COMMAREA         PIC S9(04) COMP VALUE +4096.         
      *--                                                                       
       01  COMM-MEG4-LONG-COMMAREA         PIC S9(04) COMP-5 VALUE              
                                                                  +4096.        
      *}                                                                        
       01  COMM-MEG4-APPLI.                                             00000130
           05  COMM-MEG4-CODE-ETAT         PIC X(10).                   00000140
      *                                  * CODE ETAT                    00000140
      *                                                                 00000140
           05  COMM-MEG4-IMPRIMANTE        PIC X(10).                   00000140
      *                                  * CODE IMPRIMANTE HOST         00000140
      *                                                                 00000140
           05  COMM-MEG4-NB-EXEMPL         PIC 9(03).                   00000140
      *                                  * NOMBRE D'EXEMPLAIRES         00000140
      *                                                                 00000140
           05  COMM-MEG4-LG-LIGNE          PIC 9(03).                   00000140
      *                                  * LONGUEUR DE LA LIGNE         00000140
      *                                                                 00000140
           05  COMM-MEG4-NB-LIGNE          PIC 9(03).                   00000140
      *                                  * NOMBRE DE LIGNES PAR PAGE    00000140
      *                                                                 00000140
           05  COMM-MEG4-SOC-EMET          PIC X(03).                   00000140
      *                                  * SOCIETE EMETTRICE            00000140
      *                                                                 00000140
           05  COMM-MEG4-LIEU-EMET         PIC X(03).                   00000140
      *                                  * LIEU EMETTEUR                00000140
      *                                                                 00000140
           05  COMM-MEG4-SOC-DEST          PIC X(03).                   00000140
      *                                  * SOCIETE DESTINATAIRE         00000140
      *                                                                 00000140
           05  COMM-MEG4-LIEU-DEST         PIC X(03).                   00000140
      *                                  * LIEU DESTINATAIRE            00000140
      *                                                                 00000140
           05  COMM-MEG4-DATE-TRAIT        PIC X(08).                   00000140
      *                                  * DATE DE TRAITEMENT (SSAAMMJJ)00000140
      *                                                                 00000140
           05  COMM-MEG4-ACID              PIC X(08).                   00000140
      *                                  * IDENTIFIANT                  00000140
      *                                                                 00000140
           05  COMM-MEG4-ZONES-RETOUR.                                  00000220
      *                                  * ZONES RETOUR                 00000140
               10  COMM-MEG4-CODRET        PIC X(02).                   00000230
      *                                  * CODE RETOUR                  00000140
      *                                                                 00000140
               10  COMM-MEG4-MESSAGE       PIC X(80).                   00000231
      *                                  * MESSAGE D'ERREUR             00000140
      *                                                                 00000140
           05 COMM-MEG4-FILLER             PIC X(3957).                 00000240
      *                                  * FILLER                       00000140
      *                                                                 00000140
                                                                                
