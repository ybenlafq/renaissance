      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRX300 AU 22/09/1998  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,03,PD,A,                          *        
      *                           18,20,BI,A,                          *        
      *                           38,20,BI,A,                          *        
      *                           58,20,BI,A,                          *        
      *                           78,05,BI,A,                          *        
      *                           83,20,BI,A,                          *        
      *                           03,07,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                           12,05,PD,A,                          *        
      *                           17,08,BI,A,                          *        
      *                           25,04,BI,A,                          *        
      *                           29,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRX300.                                                        
            05 NOMETAT-IRX300           PIC X(6) VALUE 'IRX300'.                
            05 RUPTURES-IRX300.                                                 
           10 IRX300-NSOCIETE           PIC X(03).                      007  003
           10 IRX300-CHEFPROD           PIC X(05).                      010  005
           10 IRX300-WSEQFAM            PIC S9(05)      COMP-3.         015  003
           10 IRX300-LVMARKET1          PIC X(20).                      018  020
           10 IRX300-LVMARKET2          PIC X(20).                      038  020
           10 IRX300-LVMARKET3          PIC X(20).                      058  020
           10 IRX300-CMARQ              PIC X(05).                      078  005
           10 IRX300-LREFFOURN          PIC X(20).                      083  020
           10 IRX300-NCODIC             PIC X(07).                      103  007
           10 IRX300-NZONPRIX           PIC X(02).                      110  002
           10 IRX300-PRELEVE            PIC S9(07)V9(2) COMP-3.         112  005
           10 IRX300-DRELEVE            PIC X(08).                      117  008
           10 IRX300-NCONC              PIC X(04).                      125  004
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRX300-SEQUENCE           PIC S9(04) COMP.                129  002
      *--                                                                       
           10 IRX300-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRX300.                                                   
           10 IRX300-CFAM               PIC X(05).                      131  005
           10 IRX300-DRELEVEJJMM        PIC X(04).                      136  004
           10 IRX300-LCHEFPROD          PIC X(20).                      140  020
           10 IRX300-LCOMMENT           PIC X(20).                      160  020
           10 IRX300-LEMPCONC           PIC X(15).                      180  015
           10 IRX300-LENSCONC           PIC X(15).                      195  015
           10 IRX300-LFAM               PIC X(20).                      210  020
           10 IRX300-LSTATCOMP          PIC X(03).                      230  003
           10 IRX300-TYPALIGN           PIC X(01).                      233  001
           10 IRX300-PVMAG              PIC S9(07)V9(2) COMP-3.         234  005
           10 IRX300-DATETRAITEE        PIC X(08).                      239  008
            05 FILLER                      PIC X(266).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IRX300-LONG           PIC S9(4)   COMP  VALUE +246.           
      *                                                                         
      *--                                                                       
        01  DSECT-IRX300-LONG           PIC S9(4) COMP-5  VALUE +246.           
                                                                                
      *}                                                                        
