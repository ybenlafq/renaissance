      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************                    
      *   COPY DE LA TABLE RVGN1500                                     00040000
      **********************************************************        00050000
      *   LISTE DES HOST VARIABLES DE LA VUE RVGN1500                   00060000
      **********************************************************        00070000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGN1500.                                                    00080000
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGN1500.                                                            
      *}                                                                        
           05  GN15-NSOCIETE   PIC X(3).                                00090000
           05  GN15-NZONPRIX   PIC X(2).                                00100000
           05  GN15-NLIEU      PIC X(3).                                00110000
           05  GN15-NCODIC     PIC X(7).                                00120000
           05  GN15-CREF       PIC X(1).                                00130000
           05  GN15-DEFFET     PIC X(8).                                00140000
           05  GN15-DFINEFFET  PIC X(8).                                00150000
           05  GN15-DSYST PIC S9(13) COMP-3.                            00160000
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************        00170000
      *   LISTE DES FLAGS DE LA TABLE RVGN1500                          00180000
      **********************************************************        00190000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGN1500-FLAGS.                                              00200000
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGN1500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN15-NSOCIETE-F                                          00210000
      *        PIC S9(4) COMP.                                          00211000
      *--                                                                       
           05  GN15-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN15-NZONPRIX-F                                          00220000
      *        PIC S9(4) COMP.                                          00221000
      *--                                                                       
           05  GN15-NZONPRIX-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN15-NLIEU-F                                             00230000
      *        PIC S9(4) COMP.                                          00231000
      *--                                                                       
           05  GN15-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN15-NCODIC-F                                            00240000
      *        PIC S9(4) COMP.                                          00241000
      *--                                                                       
           05  GN15-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN15-CREF-F                                              00250000
      *        PIC S9(4) COMP.                                          00251000
      *--                                                                       
           05  GN15-CREF-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN15-DEFFET-F                                            00260000
      *        PIC S9(4) COMP.                                          00261000
      *--                                                                       
           05  GN15-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN15-DFINEFFET-F                                         00270000
      *        PIC S9(4) COMP.                                          00271000
      *--                                                                       
           05  GN15-DFINEFFET-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN15-DSYST-F                                             00280000
      *        PIC S9(4) COMP.                                          00290000
      *                                                                         
      *--                                                                       
           05  GN15-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
