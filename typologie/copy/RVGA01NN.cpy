      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TVACO CALCUL TVA : COEFFICIENT TVA     *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01NN.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01NN.                                                            
      *}                                                                        
           05  TVACO-CTABLEG2    PIC X(15).                                     
           05  TVACO-CTABLEG2-REDEF REDEFINES TVACO-CTABLEG2.                   
               10  TVACO-CTAUXTVA        PIC X(05).                             
           05  TVACO-WTABLEG     PIC X(80).                                     
           05  TVACO-WTABLEG-REDEF  REDEFINES TVACO-WTABLEG.                    
               10  TVACO-WACTIF          PIC X(01).                             
               10  TVACO-PCOEFF          PIC S9(03)V9(04)     COMP-3.           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01NN-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01NN-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TVACO-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TVACO-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TVACO-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TVACO-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
