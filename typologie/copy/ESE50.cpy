      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESE00   ESE00                                              00000020
      ***************************************************************** 00000030
       01   ESE50I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCSERVL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNCSERVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCSERVF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNCSERVI  PIC X(5).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCSERVL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLCSERVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCSERVF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLCSERVI  PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPERL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCOPERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOPERF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCOPERI   PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLOPERL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLOPERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLOPERF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLOPERI   PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCFAMI    PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLFAMI    PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPTMAJ1L      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MCPTMAJ1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPTMAJ1F      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCPTMAJ1I      PIC X(3).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPTERR1L      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MCPTERR1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPTERR1F      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCPTERR1I      PIC X(3).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCASSORTL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MCASSORTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCASSORTF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCASSORTI      PIC X(5).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLASSORTL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MLASSORTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLASSORTF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLASSORTI      PIC X(10).                                 00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPTMAJ2L      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MCPTMAJ2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPTMAJ2F      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCPTMAJ2I      PIC X(3).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPTERR2L      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MCPTERR2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPTERR2F      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCPTERR2I      PIC X(3).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPTMAJ3L      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MCPTMAJ3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPTMAJ3F      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCPTMAJ3I      PIC X(3).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPTERR3L      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MCPTERR3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPTERR3F      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCPTERR3I      PIC X(3).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPTMAJ4L      COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MCPTMAJ4L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPTMAJ4F      PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCPTMAJ4I      PIC X(3).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPTERR4L      COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MCPTERR4L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPTERR4F      PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCPTERR4I      PIC X(3).                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPTMAJ5L      COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MCPTMAJ5L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPTMAJ5F      PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCPTMAJ5I      PIC X(3).                                  00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPTERR5L      COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MCPTERR5L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPTERR5F      PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCPTERR5I      PIC X(3).                                  00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPTMAJ7L      COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MCPTMAJ7L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPTMAJ7F      PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCPTMAJ7I      PIC X(3).                                  00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPTERR7L      COMP PIC S9(4).                            00000940
      *--                                                                       
           02 MCPTERR7L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPTERR7F      PIC X.                                     00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MCPTERR7I      PIC X(3).                                  00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MZONCMDI  PIC X(15).                                      00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MLIBERRI  PIC X(78).                                      00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MCODTRAI  PIC X(4).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MCICSI    PIC X(5).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MNETNAMI  PIC X(8).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MSCREENI  PIC X(4).                                       00001210
      ***************************************************************** 00001220
      * SDF: ESE00   ESE00                                              00001230
      ***************************************************************** 00001240
       01   ESE50O REDEFINES ESE50I.                                    00001250
           02 FILLER    PIC X(12).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MDATJOUA  PIC X.                                          00001280
           02 MDATJOUC  PIC X.                                          00001290
           02 MDATJOUP  PIC X.                                          00001300
           02 MDATJOUH  PIC X.                                          00001310
           02 MDATJOUV  PIC X.                                          00001320
           02 MDATJOUO  PIC X(10).                                      00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MTIMJOUA  PIC X.                                          00001350
           02 MTIMJOUC  PIC X.                                          00001360
           02 MTIMJOUP  PIC X.                                          00001370
           02 MTIMJOUH  PIC X.                                          00001380
           02 MTIMJOUV  PIC X.                                          00001390
           02 MTIMJOUO  PIC X(5).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MWFONCA   PIC X.                                          00001420
           02 MWFONCC   PIC X.                                          00001430
           02 MWFONCP   PIC X.                                          00001440
           02 MWFONCH   PIC X.                                          00001450
           02 MWFONCV   PIC X.                                          00001460
           02 MWFONCO   PIC X(3).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MNCSERVA  PIC X.                                          00001490
           02 MNCSERVC  PIC X.                                          00001500
           02 MNCSERVP  PIC X.                                          00001510
           02 MNCSERVH  PIC X.                                          00001520
           02 MNCSERVV  PIC X.                                          00001530
           02 MNCSERVO  PIC X(5).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MLCSERVA  PIC X.                                          00001560
           02 MLCSERVC  PIC X.                                          00001570
           02 MLCSERVP  PIC X.                                          00001580
           02 MLCSERVH  PIC X.                                          00001590
           02 MLCSERVV  PIC X.                                          00001600
           02 MLCSERVO  PIC X(20).                                      00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MCOPERA   PIC X.                                          00001630
           02 MCOPERC   PIC X.                                          00001640
           02 MCOPERP   PIC X.                                          00001650
           02 MCOPERH   PIC X.                                          00001660
           02 MCOPERV   PIC X.                                          00001670
           02 MCOPERO   PIC X(5).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MLOPERA   PIC X.                                          00001700
           02 MLOPERC   PIC X.                                          00001710
           02 MLOPERP   PIC X.                                          00001720
           02 MLOPERH   PIC X.                                          00001730
           02 MLOPERV   PIC X.                                          00001740
           02 MLOPERO   PIC X(20).                                      00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MCFAMA    PIC X.                                          00001770
           02 MCFAMC    PIC X.                                          00001780
           02 MCFAMP    PIC X.                                          00001790
           02 MCFAMH    PIC X.                                          00001800
           02 MCFAMV    PIC X.                                          00001810
           02 MCFAMO    PIC X(5).                                       00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MLFAMA    PIC X.                                          00001840
           02 MLFAMC    PIC X.                                          00001850
           02 MLFAMP    PIC X.                                          00001860
           02 MLFAMH    PIC X.                                          00001870
           02 MLFAMV    PIC X.                                          00001880
           02 MLFAMO    PIC X(20).                                      00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MCPTMAJ1A      PIC X.                                     00001910
           02 MCPTMAJ1C PIC X.                                          00001920
           02 MCPTMAJ1P PIC X.                                          00001930
           02 MCPTMAJ1H PIC X.                                          00001940
           02 MCPTMAJ1V PIC X.                                          00001950
           02 MCPTMAJ1O      PIC ZZ9.                                   00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MCPTERR1A      PIC X.                                     00001980
           02 MCPTERR1C PIC X.                                          00001990
           02 MCPTERR1P PIC X.                                          00002000
           02 MCPTERR1H PIC X.                                          00002010
           02 MCPTERR1V PIC X.                                          00002020
           02 MCPTERR1O      PIC X(3).                                  00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MCASSORTA      PIC X.                                     00002050
           02 MCASSORTC PIC X.                                          00002060
           02 MCASSORTP PIC X.                                          00002070
           02 MCASSORTH PIC X.                                          00002080
           02 MCASSORTV PIC X.                                          00002090
           02 MCASSORTO      PIC X(5).                                  00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MLASSORTA      PIC X.                                     00002120
           02 MLASSORTC PIC X.                                          00002130
           02 MLASSORTP PIC X.                                          00002140
           02 MLASSORTH PIC X.                                          00002150
           02 MLASSORTV PIC X.                                          00002160
           02 MLASSORTO      PIC X(10).                                 00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MCPTMAJ2A      PIC X.                                     00002190
           02 MCPTMAJ2C PIC X.                                          00002200
           02 MCPTMAJ2P PIC X.                                          00002210
           02 MCPTMAJ2H PIC X.                                          00002220
           02 MCPTMAJ2V PIC X.                                          00002230
           02 MCPTMAJ2O      PIC X(3).                                  00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MCPTERR2A      PIC X.                                     00002260
           02 MCPTERR2C PIC X.                                          00002270
           02 MCPTERR2P PIC X.                                          00002280
           02 MCPTERR2H PIC X.                                          00002290
           02 MCPTERR2V PIC X.                                          00002300
           02 MCPTERR2O      PIC X(3).                                  00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MCPTMAJ3A      PIC X.                                     00002330
           02 MCPTMAJ3C PIC X.                                          00002340
           02 MCPTMAJ3P PIC X.                                          00002350
           02 MCPTMAJ3H PIC X.                                          00002360
           02 MCPTMAJ3V PIC X.                                          00002370
           02 MCPTMAJ3O      PIC X(3).                                  00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MCPTERR3A      PIC X.                                     00002400
           02 MCPTERR3C PIC X.                                          00002410
           02 MCPTERR3P PIC X.                                          00002420
           02 MCPTERR3H PIC X.                                          00002430
           02 MCPTERR3V PIC X.                                          00002440
           02 MCPTERR3O      PIC X(3).                                  00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MCPTMAJ4A      PIC X.                                     00002470
           02 MCPTMAJ4C PIC X.                                          00002480
           02 MCPTMAJ4P PIC X.                                          00002490
           02 MCPTMAJ4H PIC X.                                          00002500
           02 MCPTMAJ4V PIC X.                                          00002510
           02 MCPTMAJ4O      PIC X(3).                                  00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MCPTERR4A      PIC X.                                     00002540
           02 MCPTERR4C PIC X.                                          00002550
           02 MCPTERR4P PIC X.                                          00002560
           02 MCPTERR4H PIC X.                                          00002570
           02 MCPTERR4V PIC X.                                          00002580
           02 MCPTERR4O      PIC X(3).                                  00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MCPTMAJ5A      PIC X.                                     00002610
           02 MCPTMAJ5C PIC X.                                          00002620
           02 MCPTMAJ5P PIC X.                                          00002630
           02 MCPTMAJ5H PIC X.                                          00002640
           02 MCPTMAJ5V PIC X.                                          00002650
           02 MCPTMAJ5O      PIC X(3).                                  00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MCPTERR5A      PIC X.                                     00002680
           02 MCPTERR5C PIC X.                                          00002690
           02 MCPTERR5P PIC X.                                          00002700
           02 MCPTERR5H PIC X.                                          00002710
           02 MCPTERR5V PIC X.                                          00002720
           02 MCPTERR5O      PIC X(3).                                  00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MCPTMAJ7A      PIC X.                                     00002750
           02 MCPTMAJ7C PIC X.                                          00002760
           02 MCPTMAJ7P PIC X.                                          00002770
           02 MCPTMAJ7H PIC X.                                          00002780
           02 MCPTMAJ7V PIC X.                                          00002790
           02 MCPTMAJ7O      PIC X(3).                                  00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MCPTERR7A      PIC X.                                     00002820
           02 MCPTERR7C PIC X.                                          00002830
           02 MCPTERR7P PIC X.                                          00002840
           02 MCPTERR7H PIC X.                                          00002850
           02 MCPTERR7V PIC X.                                          00002860
           02 MCPTERR7O      PIC X(3).                                  00002870
           02 FILLER    PIC X(2).                                       00002880
           02 MZONCMDA  PIC X.                                          00002890
           02 MZONCMDC  PIC X.                                          00002900
           02 MZONCMDP  PIC X.                                          00002910
           02 MZONCMDH  PIC X.                                          00002920
           02 MZONCMDV  PIC X.                                          00002930
           02 MZONCMDO  PIC X(15).                                      00002940
           02 FILLER    PIC X(2).                                       00002950
           02 MLIBERRA  PIC X.                                          00002960
           02 MLIBERRC  PIC X.                                          00002970
           02 MLIBERRP  PIC X.                                          00002980
           02 MLIBERRH  PIC X.                                          00002990
           02 MLIBERRV  PIC X.                                          00003000
           02 MLIBERRO  PIC X(78).                                      00003010
           02 FILLER    PIC X(2).                                       00003020
           02 MCODTRAA  PIC X.                                          00003030
           02 MCODTRAC  PIC X.                                          00003040
           02 MCODTRAP  PIC X.                                          00003050
           02 MCODTRAH  PIC X.                                          00003060
           02 MCODTRAV  PIC X.                                          00003070
           02 MCODTRAO  PIC X(4).                                       00003080
           02 FILLER    PIC X(2).                                       00003090
           02 MCICSA    PIC X.                                          00003100
           02 MCICSC    PIC X.                                          00003110
           02 MCICSP    PIC X.                                          00003120
           02 MCICSH    PIC X.                                          00003130
           02 MCICSV    PIC X.                                          00003140
           02 MCICSO    PIC X(5).                                       00003150
           02 FILLER    PIC X(2).                                       00003160
           02 MNETNAMA  PIC X.                                          00003170
           02 MNETNAMC  PIC X.                                          00003180
           02 MNETNAMP  PIC X.                                          00003190
           02 MNETNAMH  PIC X.                                          00003200
           02 MNETNAMV  PIC X.                                          00003210
           02 MNETNAMO  PIC X(8).                                       00003220
           02 FILLER    PIC X(2).                                       00003230
           02 MSCREENA  PIC X.                                          00003240
           02 MSCREENC  PIC X.                                          00003250
           02 MSCREENP  PIC X.                                          00003260
           02 MSCREENH  PIC X.                                          00003270
           02 MSCREENV  PIC X.                                          00003280
           02 MSCREENO  PIC X(4).                                       00003290
                                                                                
