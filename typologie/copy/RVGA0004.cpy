      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGA0004                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA0004                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA0004.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA0004.                                                            
      *}                                                                        
           02  GA00-NCODIC                                                      
               PIC X(0007).                                                     
           02  GA00-LREFFOURN                                                   
               PIC X(0020).                                                     
           02  GA00-CFAM                                                        
               PIC X(0005).                                                     
           02  GA00-CMARQ                                                       
               PIC X(0005).                                                     
           02  GA00-CASSORT                                                     
               PIC X(0005).                                                     
           02  GA00-DEFSTATUT                                                   
               PIC X(0008).                                                     
           02  GA00-DCREATION                                                   
               PIC X(0008).                                                     
           02  GA00-D1RECEPT                                                    
               PIC X(0008).                                                     
           02  GA00-DMAJ                                                        
               PIC X(0008).                                                     
           02  GA00-LREFDARTY                                                   
               PIC X(0020).                                                     
           02  GA00-LCOMMENT                                                    
               PIC X(0050).                                                     
           02  GA00-LSTATCOMP                                                   
               PIC X(0003).                                                     
           02  GA00-WDACEM                                                      
               PIC X(0001).                                                     
           02  GA00-WTLMELA                                                     
               PIC X(0001).                                                     
           02  GA00-PRAR                                                        
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GA00-CTAUXTVA                                                    
               PIC X(0005).                                                     
           02  GA00-CEXPO                                                       
               PIC X(0005).                                                     
           02  GA00-WSENSVTE                                                    
               PIC X(0001).                                                     
           02  GA00-CGESTVTE                                                    
               PIC X(0003).                                                     
           02  GA00-CGARANTIE                                                   
               PIC X(0005).                                                     
           02  GA00-CAPPRO                                                      
               PIC X(0005).                                                     
           02  GA00-WSENSAPPRO                                                  
               PIC X(0001).                                                     
           02  GA00-QDELAIAPPRO                                                 
               PIC X(0003).                                                     
           02  GA00-QCOLICDE                                                    
               PIC S9(3) COMP-3.                                                
           02  GA00-CHEFPROD                                                    
               PIC X(0005).                                                     
           02  GA00-CORIGPROD                                                   
               PIC X(0005).                                                     
           02  GA00-LEMBALLAGE                                                  
               PIC X(0050).                                                     
           02  GA00-CTYPCONDT                                                   
               PIC X(0005).                                                     
           02  GA00-QCONDT                                                      
               PIC S9(5) COMP-3.                                                
           02  GA00-QCOLIRECEPT                                                 
               PIC S9(5) COMP-3.                                                
           02  GA00-QCOLIDSTOCK                                                 
               PIC S9(5) COMP-3.                                                
           02  GA00-QCOLIVTE                                                    
               PIC S9(5) COMP-3.                                                
           02  GA00-QPOIDS                                                      
               PIC S9(7) COMP-3.                                                
           02  GA00-QLARGEUR                                                    
               PIC S9(3) COMP-3.                                                
           02  GA00-QPROFONDEUR                                                 
               PIC S9(3) COMP-3.                                                
           02  GA00-QHAUTEUR                                                    
               PIC S9(3) COMP-3.                                                
           02  GA00-CGARCONST                                                   
               PIC X(0005).                                                     
           02  GA00-CMODSTOCK1                                                  
               PIC X(0005).                                                     
           02  GA00-WMODSTOCK1                                                  
               PIC X(0001).                                                     
           02  GA00-CMODSTOCK2                                                  
               PIC X(0005).                                                     
           02  GA00-WMODSTOCK2                                                  
               PIC X(0001).                                                     
           02  GA00-CMODSTOCK3                                                  
               PIC X(0005).                                                     
           02  GA00-WMODSTOCK3                                                  
               PIC X(0001).                                                     
           02  GA00-CFETEMPL                                                    
               PIC X(0001).                                                     
           02  GA00-QNBRANMAIL                                                  
               PIC S9(3) COMP-3.                                                
           02  GA00-QNBNIVGERB                                                  
               PIC S9(3) COMP-3.                                                
           02  GA00-CZONEACCES                                                  
               PIC X(0001).                                                     
           02  GA00-CCONTENEUR                                                  
               PIC X(0001).                                                     
           02  GA00-CSPECIFSTK                                                  
               PIC X(0001).                                                     
           02  GA00-CCOTEHOLD                                                   
               PIC X(0001).                                                     
           02  GA00-CFETIQINFO                                                  
               PIC X(0001).                                                     
           02  GA00-CFETIQPRIX                                                  
               PIC X(0001).                                                     
           02  GA00-QNBPVSOL                                                    
               PIC S9(3) COMP-3.                                                
           02  GA00-QNBPRACK                                                    
               PIC S9(3) COMP-3.                                                
           02  GA00-WLCONF                                                      
               PIC X(0001).                                                     
           02  GA00-CTPSUP                                                      
               PIC X(0001).                                                     
           02  GA00-WRESFOURN                                                   
               PIC X(0001).                                                     
           02  GA00-CQUOTA                                                      
               PIC X(0005).                                                     
           02  GA00-QCONTENU                                                    
               PIC S9(5) COMP-3.                                                
           02  GA00-QGRATUITE                                                   
               PIC S9(5) COMP-3.                                                
           02  GA00-NEAN                                                        
               PIC X(0013).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGA0004                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA0004-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA0004-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-LREFFOURN-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-LREFFOURN-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-CASSORT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-CASSORT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-DEFSTATUT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-DEFSTATUT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-D1RECEPT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-D1RECEPT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-LREFDARTY-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-LREFDARTY-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-LCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-LSTATCOMP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-LSTATCOMP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-WDACEM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-WDACEM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-WTLMELA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-WTLMELA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-PRAR-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-PRAR-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-CTAUXTVA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-CTAUXTVA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-CEXPO-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-CEXPO-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-WSENSVTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-WSENSVTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-CGESTVTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-CGESTVTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-CGARANTIE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-CGARANTIE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-CAPPRO-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-CAPPRO-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-WSENSAPPRO-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-WSENSAPPRO-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-QDELAIAPPRO-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-QDELAIAPPRO-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-QCOLICDE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-QCOLICDE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-CHEFPROD-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-CHEFPROD-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-CORIGPROD-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-CORIGPROD-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-LEMBALLAGE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-LEMBALLAGE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-CTYPCONDT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-CTYPCONDT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-QCONDT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-QCONDT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-QCOLIRECEPT-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-QCOLIRECEPT-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-QCOLIDSTOCK-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-QCOLIDSTOCK-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-QCOLIVTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-QCOLIVTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-QPOIDS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-QPOIDS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-QLARGEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-QLARGEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-QPROFONDEUR-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-QPROFONDEUR-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-QHAUTEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-QHAUTEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-CGARCONST-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-CGARCONST-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-CMODSTOCK1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-CMODSTOCK1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-WMODSTOCK1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-WMODSTOCK1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-CMODSTOCK2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-CMODSTOCK2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-WMODSTOCK2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-WMODSTOCK2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-CMODSTOCK3-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-CMODSTOCK3-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-WMODSTOCK3-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-WMODSTOCK3-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-CFETEMPL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-CFETEMPL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-QNBRANMAIL-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-QNBRANMAIL-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-QNBNIVGERB-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-QNBNIVGERB-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-CZONEACCES-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-CZONEACCES-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-CCONTENEUR-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-CCONTENEUR-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-CSPECIFSTK-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-CSPECIFSTK-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-CCOTEHOLD-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-CCOTEHOLD-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-CFETIQINFO-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-CFETIQINFO-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-CFETIQPRIX-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-CFETIQPRIX-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-QNBPVSOL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-QNBPVSOL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-QNBPRACK-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-QNBPRACK-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-WLCONF-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-WLCONF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-CTPSUP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-CTPSUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-WRESFOURN-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-WRESFOURN-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-CQUOTA-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-CQUOTA-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-QCONTENU-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-QCONTENU-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-QGRATUITE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-QGRATUITE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA00-NEAN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA00-NEAN-F                                                      
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
