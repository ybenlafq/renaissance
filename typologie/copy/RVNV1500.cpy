      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RTNV15                        *        
      ******************************************************************        
       01  RVNV1500.                                                            
      *                       NCODIC                                            
           10 NV15-NCODIC          PIC X(7).                                    
      *                       NSOCIETE                                          
           10 NV15-NSOCIETE        PIC X(3).                                    
      *                       NLIEU                                             
           10 NV15-NLIEU           PIC X(3).                                    
      *                       CVENDABLE                                         
           10 NV15-CVENDABLE       PIC X(1).                                    
      *                       CASSORT                                           
           10 NV15-CASSORT         PIC X(5).                                    
      *                       LSTATCOMP                                         
           10 NV15-LSTATCOMP       PIC X(3).                                    
      *                       CAPPRO                                            
           10 NV15-CAPPRO          PIC X(5).                                    
      *                       WSENSAPPRO                                        
           10 NV15-WSENSAPPRO      PIC X(2).                                    
      *                       WOA                                               
           10 NV15-WOA             PIC X(2).                                    
      *                       DSYST                                             
           10 NV15-DSYST           PIC S9(13)V USAGE COMP-3.                    
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVNV1500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV15-NCODIC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 NV15-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV15-NSOCIETE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 NV15-NSOCIETE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV15-NLIEU-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 NV15-NLIEU-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV15-CVENDABLE-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 NV15-CVENDABLE-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV15-CASSORT-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 NV15-CASSORT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV15-LSTATCOMP-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 NV15-LSTATCOMP-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV15-CAPPRO-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 NV15-CAPPRO-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV15-WSENSAPPRO-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 NV15-WSENSAPPRO-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV15-WOA-F           PIC S9(4) COMP.                              
      *--                                                                       
           10 NV15-WOA-F           PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV15-DSYST-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 NV15-DSYST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 10      *        
      ******************************************************************        
                                                                                
