      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRX220 AU 04/02/1999  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,05,BI,A,                          *        
      *                           22,05,BI,A,                          *        
      *                           27,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRX220.                                                        
            05 NOMETAT-IRX220           PIC X(6) VALUE 'IRX220'.                
            05 RUPTURES-IRX220.                                                 
           10 IRX220-NSOCIETE           PIC X(03).                      007  003
           10 IRX220-NZONPRIX           PIC X(02).                      010  002
           10 IRX220-CHEFPRODUIT        PIC X(05).                      012  005
           10 IRX220-RAYON              PIC X(05).                      017  005
           10 IRX220-CFAM               PIC X(05).                      022  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRX220-SEQUENCE           PIC S9(04) COMP.                027  002
      *--                                                                       
           10 IRX220-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRX220.                                                   
           10 IRX220-LCHEFPROD          PIC X(20).                      029  020
           10 IRX220-LZONPRIX           PIC X(20).                      049  020
           10 IRX220-ALIGNPART1         PIC S9(05)      COMP-3.         069  003
           10 IRX220-ALIGNPART2         PIC S9(05)      COMP-3.         072  003
           10 IRX220-ALIGNTOT1          PIC S9(05)      COMP-3.         075  003
           10 IRX220-ALIGNTOT2          PIC S9(05)      COMP-3.         078  003
           10 IRX220-INFSRP1            PIC S9(05)      COMP-3.         081  003
           10 IRX220-INFSRP2            PIC S9(03)      COMP-3.         084  002
           10 IRX220-NBCONC             PIC S9(03)      COMP-3.         086  002
           10 IRX220-NBETIQCONC         PIC S9(05)      COMP-3.         088  003
           10 IRX220-NBREFCONC          PIC S9(05)      COMP-3.         091  003
           10 IRX220-NBREFEXPO          PIC S9(05)      COMP-3.         094  003
           10 IRX220-PVINF1             PIC S9(05)      COMP-3.         097  003
           10 IRX220-PVINF2             PIC S9(05)      COMP-3.         100  003
           10 IRX220-TRI                PIC S9(09)      COMP-3.         103  005
           10 IRX220-DSAISIE-MAX        PIC X(08).                      108  008
           10 IRX220-DSAISIE-MIN        PIC X(08).                      116  008
            05 FILLER                      PIC X(389).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IRX220-LONG           PIC S9(4)   COMP  VALUE +123.           
      *                                                                         
      *--                                                                       
        01  DSECT-IRX220-LONG           PIC S9(4) COMP-5  VALUE +123.           
                                                                                
      *}                                                                        
