      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVBS0100                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVBS0100                 00000060
      *   CLE UNIQUE : CTYPENT1 A NSEQENT                               00000070
      *---------------------------------------------------------        00000080
      *                                                                 00000090
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBS0100.                                                    00000100
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBS0100.                                                            
      *}                                                                        
           10 BS01-CTYPENT1        PIC X(2).                            00000110
           10 BS01-NENTITE1        PIC X(7).                            00000120
           10 BS01-NSEQENT         PIC S9(3)V USAGE COMP-3.             00000130
           10 BS01-CPROFLIEN       PIC X(5).                            00000140
           10 BS01-CLIEN           PIC X(2).                            00000150
           10 BS01-NBLOC           PIC S9(3)V USAGE COMP-3.             00000160
           10 BS01-NSEQ            PIC S9(3)V USAGE COMP-3.             00000170
           10 BS01-CTYPENT2        PIC X(2).                            00000180
           10 BS01-NENTITE2        PIC X(7).                            00000190
           10 BS01-QNBENT          PIC S9(3)V USAGE COMP-3.             00000200
           10 BS01-NENTMIN         PIC S9(3)V USAGE COMP-3.             00000210
           10 BS01-NENTMAX         PIC S9(3)V USAGE COMP-3.             00000220
           10 BS01-WPRIME          PIC X(1).                            00000230
           10 BS01-WPRIX           PIC X(1).                            00000240
           10 BS01-NSEQENTREF      PIC S9(3)V USAGE COMP-3.             00000250
           10 BS01-QDELTACLI       PIC S9(3)V9(2) USAGE COMP-3.         00000260
           10 BS01-DEFFET          PIC X(8).                            00000270
           10 BS01-DFINEFFET       PIC X(8).                            00000280
           10 BS01-WAUTO           PIC X(1).                            00000290
           10 BS01-DSYST           PIC S9(13)V USAGE COMP-3.            00000300
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00000310
      *---------------------------------------------------------        00000320
      *   LISTE DES FLAGS DE LA TABLE RVBS0100                          00000330
      *---------------------------------------------------------        00000340
      *                                                                 00000350
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBS0100-FLAGS.                                              00000360
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBS0100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS01-CTYPENT1-F      PIC S9(4) COMP.                      00000370
      *--                                                                       
           10 BS01-CTYPENT1-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS01-NENTITE1-F      PIC S9(4) COMP.                      00000380
      *--                                                                       
           10 BS01-NENTITE1-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS01-NSEQENT-F       PIC S9(4) COMP.                      00000390
      *--                                                                       
           10 BS01-NSEQENT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS01-CPROFLIEN-F     PIC S9(4) COMP.                      00000400
      *--                                                                       
           10 BS01-CPROFLIEN-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS01-CLIEN-F         PIC S9(4) COMP.                      00000410
      *--                                                                       
           10 BS01-CLIEN-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS01-NBLOC-F         PIC S9(4) COMP.                      00000420
      *--                                                                       
           10 BS01-NBLOC-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS01-NSEQ-F          PIC S9(4) COMP.                      00000430
      *--                                                                       
           10 BS01-NSEQ-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS01-CTYPENT2-F      PIC S9(4) COMP.                      00000440
      *--                                                                       
           10 BS01-CTYPENT2-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS01-NENTITE2-F      PIC S9(4) COMP.                      00000450
      *--                                                                       
           10 BS01-NENTITE2-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS01-QNBENT-F        PIC S9(4) COMP.                      00000460
      *--                                                                       
           10 BS01-QNBENT-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS01-NENTMIN-F       PIC S9(4) COMP.                      00000470
      *--                                                                       
           10 BS01-NENTMIN-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS01-NENTMAX-F       PIC S9(4) COMP.                      00000480
      *--                                                                       
           10 BS01-NENTMAX-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS01-WPRIME-F        PIC S9(4) COMP.                      00000490
      *--                                                                       
           10 BS01-WPRIME-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS01-WPRIX-F         PIC S9(4) COMP.                      00000500
      *--                                                                       
           10 BS01-WPRIX-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS01-NSEQENTREF-F    PIC S9(4) COMP.                      00000510
      *--                                                                       
           10 BS01-NSEQENTREF-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS01-QDELTACLI-F     PIC S9(4) COMP.                      00000520
      *--                                                                       
           10 BS01-QDELTACLI-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS01-DEFFET-F        PIC S9(4) COMP.                      00000530
      *--                                                                       
           10 BS01-DEFFET-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS01-DFINEFFET-F     PIC S9(4) COMP.                      00000540
      *--                                                                       
           10 BS01-DFINEFFET-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS01-WAUTO-F         PIC S9(4) COMP.                      00000550
      *--                                                                       
           10 BS01-WAUTO-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS01-DSYST-F         PIC S9(4) COMP.                      00000560
      *                                                                         
      *--                                                                       
           10 BS01-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
