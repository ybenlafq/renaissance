      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SLCOL COLONNES PAR EMPL./FONCTION      *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01ZN.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01ZN.                                                            
      *}                                                                        
           05  SLCOL-CTABLEG2    PIC X(15).                                     
           05  SLCOL-CTABLEG2-REDEF REDEFINES SLCOL-CTABLEG2.                   
               10  SLCOL-CPGM            PIC X(10).                             
               10  SLCOL-NCOL            PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  SLCOL-NCOL-N         REDEFINES SLCOL-NCOL                    
                                         PIC 9(02).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  SLCOL-WTABLEG     PIC X(80).                                     
           05  SLCOL-WTABLEG-REDEF  REDEFINES SLCOL-WTABLEG.                    
               10  SLCOL-CRSL            PIC X(05).                             
               10  SLCOL-TQTE            PIC X(01).                             
               10  SLCOL-MSGENT          PIC X(07).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01ZN-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01ZN-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLCOL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SLCOL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLCOL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SLCOL-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
