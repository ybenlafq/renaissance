      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : GESTION DES CONCURRENTS NATIONAUX      (GN57)          *         
      *****************************************************************         
      *                                                               *         
       01  TS-GN57.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GN57-LONG                 PIC S9(03) COMP-3                    
                                           VALUE +45.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-GN57-DONNEES.                                                  
      *----------------------------------  CODE SOCIETE                         
              03 TS-GN57-NSOC              PIC X(3).                            
      *----------------------------------  CODE CONCURRENT LOCAL                
              03 TS-GN57-NCONCL            PIC X(4).                            
      *----------------------------------  LIBELLE CONCURRENT LOCAL             
              03 TS-GN57-LCONCL            PIC X(15).                           
      *----------------------------------  CODE MAGASIN                         
              03 TS-GN57-NLIEU             PIC X(3).                            
      *----------------------------------  LIBELLE MAGASIN                      
              03 TS-GN57-LLIEU             PIC X(20).                           
                                                                                
