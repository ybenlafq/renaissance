      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
           EJECT                                                        00000100
      *==> DARTY ****************************************************** 00010001
      *          * COMMAREA TRAITEMENT DATE - PGM CICS TETDATC1       * 00020001
      ****************************************************** COMMDAT1 * 00030001
      *                                                                 00040001
PTSUP *01  COMM-DATC-LONG-COMMAREA PIC S9(4) COMP VALUE +163.           00050001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
PTADD *01  COMM-DATC-LONG-COMMAREA PIC S9(4) COMP VALUE +169.           00050001
      *--                                                                       
       01  COMM-DATC-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +169.                 
      *}                                                                        
      *                                                                 00060001
       01  Z-COMMAREA-TETDATC.                                          00070001
      *                                                                 00080001
          02 GFDATA                      PIC X.                         00090001
      *                                                                 00100001
          02 GFJJMMSSAA.                                                00110001
             05 GFJOUR                   PIC XX.                        00120001
             05 GFJJ REDEFINES GFJOUR    PIC 99.                        00130001
             05 GFMOIS                   PIC XX.                        00140001
             05 GFMM REDEFINES GFMOIS    PIC 99.                        00150001
             05 GFSIECLE                 PIC XX.                        00160001
             05 GFSS REDEFINES GFSIECLE  PIC 99.                        00170001
             05 GFANNEE                  PIC XX.                        00180001
             05 GFAA REDEFINES GFANNEE   PIC 99.                        00190001
      *                                                                 00200001
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00210003
          02 GFQNTA                      PIC 999.                       00220001
          02 GFQNT0                      PIC 9(5).                      00230001
      *                                                                 00240001
      *   RESULTATS SEMAINE                                             00250003
          02 GFSMN                       PIC 9.                         00260001
          02 GFSMN-LIB-C                 PIC X(3).                      00270001
PTSUP *   02 GFSMN-LIB-L                 PIC X(9).                      00280001
PTADD     02 GFSMN-LIB-L                 PIC X(12).                     00280001
      *                                                                 00290001
      *   LIBELLE MOIS                                                  00300003
          02 GFMOI-LIB-C                 PIC X(3).                      00310001
          02 GFMOI-LIB-L                 PIC X(9).                      00320001
      *                                                                 00330001
      * DIFFERENTES EXPRESSIONS DES DATES                               00340003
      *   FORME SSAAMMJJ                                                00350003
          02 GFSAMJ-0                    PIC X(8).                      00360003
      *   FORME AAMMJJ                                                  00370003
          02 GFAMJ-1                     PIC X(6).                      00380003
      *   FORME JJMMSSAA                                                00390003
          02 GFJMSA-2                    PIC X(8).                      00400003
      *   FORME JJMMAA                                                  00410003
          02 GFJMA-3                     PIC X(6).                      00420003
      *   FORME JJ/MM/AA                                                00430003
          02 GFJMA-4                     PIC X(8).                      00440003
      *   FORME JJ/MM/SSAA                                              00450003
          02 GFJMSA-5                    PIC X(10).                     00460003
      *                                                                 00470001
          02 GFVDAT                      PIC X.                         00480002
          02 GFBISS                      PIC 9.                         00490001
      *                                                                 00500001
          02 GFWEEK.                                                    00510001
             05 GFSEMSS                  PIC 99.                        00520001
             05 GFSEMAA                  PIC 99.                        00530001
             05 GFSEMNU                  PIC 99.                        00540001
      *                                                                 00550001
          02 GFAJOUX                     PIC XX.                        00560001
          02 GFAJOUP REDEFINES GFAJOUX   PIC 99.                        00560002
          02 FILLER                      PIC X(6).                      00560003
      *                                                                 00570001
          02 GF-MESS-ERR                 PIC X(60).                     00580001
PTADD**********AJOUT POUR INTERNALISATION *****                         00570001
      *                                                                 00570001
          02 GFCODE-LANGUE               PIC X(02).                     00580001
                                                                                
