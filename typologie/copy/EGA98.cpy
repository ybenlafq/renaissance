      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA77   EGA77                                              00000020
      ***************************************************************** 00000030
       01   EGA98I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFONCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFONCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCFONCI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSEQUENCEL    COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MNSEQUENCEL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNSEQUENCEF    PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNSEQUENCEI    PIC X(2).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBCOMPOL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MNBCOMPOL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBCOMPOF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNBCOMPOI      PIC X(2).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNCODICI  PIC X(7).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLREFFOURNI    PIC X(20).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCFAMI    PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLFAMI    PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCMARQI   PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLMARQI   PIC X(20).                                      00000570
           02 MTRD OCCURS   10 TIMES .                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTRL    COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MTRL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MTRF    PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MTRI    PIC X.                                          00000620
           02 MNEANCOMPOD OCCURS   10 TIMES .                           00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNEANCOMPOL  COMP PIC S9(4).                            00000640
      *--                                                                       
             03 MNEANCOMPOL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNEANCOMPOF  PIC X.                                     00000650
             03 FILLER  PIC X(4).                                       00000660
             03 MNEANCOMPOI  PIC X(13).                                 00000670
           02 MWAUTHENTD OCCURS   10 TIMES .                            00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWAUTHENTL   COMP PIC S9(4).                            00000690
      *--                                                                       
             03 MWAUTHENTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MWAUTHENTF   PIC X.                                     00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MWAUTHENTI   PIC X.                                     00000720
           02 MWACTIFD OCCURS   10 TIMES .                              00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWACTIFL     COMP PIC S9(4).                            00000740
      *--                                                                       
             03 MWACTIFL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWACTIFF     PIC X.                                     00000750
             03 FILLER  PIC X(4).                                       00000760
             03 MWACTIFI     PIC X.                                     00000770
           02 MLNEANCOMPOD OCCURS   10 TIMES .                          00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLNEANCOMPOL      COMP PIC S9(4).                       00000790
      *--                                                                       
             03 MLNEANCOMPOL COMP-5 PIC S9(4).                                  
      *}                                                                        
             03 MLNEANCOMPOF      PIC X.                                00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MLNEANCOMPOI      PIC X(15).                            00000820
           02 MDATED OCCURS   10 TIMES .                                00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEL  COMP PIC S9(4).                                 00000840
      *--                                                                       
             03 MDATEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDATEF  PIC X.                                          00000850
             03 FILLER  PIC X(4).                                       00000860
             03 MDATEI  PIC X(10).                                      00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOBSOLL  COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MNOBSOLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNOBSOLF  PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MNOBSOLI  PIC X(4).                                       00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MZONCMDI  PIC X(15).                                      00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MLIBERRI  PIC X(58).                                      00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MCODTRAI  PIC X(4).                                       00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001040
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MCICSI    PIC X(5).                                       00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001080
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001090
           02 FILLER    PIC X(4).                                       00001100
           02 MNETNAMI  PIC X(8).                                       00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001120
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001130
           02 FILLER    PIC X(4).                                       00001140
           02 MSCREENI  PIC X(4).                                       00001150
      ***************************************************************** 00001160
      * SDF: EGA77   EGA77                                              00001170
      ***************************************************************** 00001180
       01   EGA98O REDEFINES EGA98I.                                    00001190
           02 FILLER    PIC X(12).                                      00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MDATJOUA  PIC X.                                          00001220
           02 MDATJOUC  PIC X.                                          00001230
           02 MDATJOUP  PIC X.                                          00001240
           02 MDATJOUH  PIC X.                                          00001250
           02 MDATJOUV  PIC X.                                          00001260
           02 MDATJOUO  PIC X(10).                                      00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MTIMJOUA  PIC X.                                          00001290
           02 MTIMJOUC  PIC X.                                          00001300
           02 MTIMJOUP  PIC X.                                          00001310
           02 MTIMJOUH  PIC X.                                          00001320
           02 MTIMJOUV  PIC X.                                          00001330
           02 MTIMJOUO  PIC X(5).                                       00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MPAGEA    PIC X.                                          00001360
           02 MPAGEC    PIC X.                                          00001370
           02 MPAGEP    PIC X.                                          00001380
           02 MPAGEH    PIC X.                                          00001390
           02 MPAGEV    PIC X.                                          00001400
           02 MPAGEO    PIC X(3).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MPAGEMAXA      PIC X.                                     00001430
           02 MPAGEMAXC PIC X.                                          00001440
           02 MPAGEMAXP PIC X.                                          00001450
           02 MPAGEMAXH PIC X.                                          00001460
           02 MPAGEMAXV PIC X.                                          00001470
           02 MPAGEMAXO      PIC X(3).                                  00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MCFONCA   PIC X.                                          00001500
           02 MCFONCC   PIC X.                                          00001510
           02 MCFONCP   PIC X.                                          00001520
           02 MCFONCH   PIC X.                                          00001530
           02 MCFONCV   PIC X.                                          00001540
           02 MCFONCO   PIC X(3).                                       00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MNSEQUENCEA    PIC X.                                     00001570
           02 MNSEQUENCEC    PIC X.                                     00001580
           02 MNSEQUENCEP    PIC X.                                     00001590
           02 MNSEQUENCEH    PIC X.                                     00001600
           02 MNSEQUENCEV    PIC X.                                     00001610
           02 MNSEQUENCEO    PIC X(2).                                  00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MNBCOMPOA      PIC X.                                     00001640
           02 MNBCOMPOC PIC X.                                          00001650
           02 MNBCOMPOP PIC X.                                          00001660
           02 MNBCOMPOH PIC X.                                          00001670
           02 MNBCOMPOV PIC X.                                          00001680
           02 MNBCOMPOO      PIC X(2).                                  00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MNCODICA  PIC X.                                          00001710
           02 MNCODICC  PIC X.                                          00001720
           02 MNCODICP  PIC X.                                          00001730
           02 MNCODICH  PIC X.                                          00001740
           02 MNCODICV  PIC X.                                          00001750
           02 MNCODICO  PIC X(7).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MLREFFOURNA    PIC X.                                     00001780
           02 MLREFFOURNC    PIC X.                                     00001790
           02 MLREFFOURNP    PIC X.                                     00001800
           02 MLREFFOURNH    PIC X.                                     00001810
           02 MLREFFOURNV    PIC X.                                     00001820
           02 MLREFFOURNO    PIC X(20).                                 00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MCFAMA    PIC X.                                          00001850
           02 MCFAMC    PIC X.                                          00001860
           02 MCFAMP    PIC X.                                          00001870
           02 MCFAMH    PIC X.                                          00001880
           02 MCFAMV    PIC X.                                          00001890
           02 MCFAMO    PIC X(5).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MLFAMA    PIC X.                                          00001920
           02 MLFAMC    PIC X.                                          00001930
           02 MLFAMP    PIC X.                                          00001940
           02 MLFAMH    PIC X.                                          00001950
           02 MLFAMV    PIC X.                                          00001960
           02 MLFAMO    PIC X(20).                                      00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MCMARQA   PIC X.                                          00001990
           02 MCMARQC   PIC X.                                          00002000
           02 MCMARQP   PIC X.                                          00002010
           02 MCMARQH   PIC X.                                          00002020
           02 MCMARQV   PIC X.                                          00002030
           02 MCMARQO   PIC X(5).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MLMARQA   PIC X.                                          00002060
           02 MLMARQC   PIC X.                                          00002070
           02 MLMARQP   PIC X.                                          00002080
           02 MLMARQH   PIC X.                                          00002090
           02 MLMARQV   PIC X.                                          00002100
           02 MLMARQO   PIC X(20).                                      00002110
           02 DFHMS1 OCCURS   10 TIMES .                                00002120
             03 FILLER       PIC X(2).                                  00002130
             03 MTRA    PIC X.                                          00002140
             03 MTRC    PIC X.                                          00002150
             03 MTRP    PIC X.                                          00002160
             03 MTRH    PIC X.                                          00002170
             03 MTRV    PIC X.                                          00002180
             03 MTRO    PIC X.                                          00002190
           02 DFHMS2 OCCURS   10 TIMES .                                00002200
             03 FILLER       PIC X(2).                                  00002210
             03 MNEANCOMPOA  PIC X.                                     00002220
             03 MNEANCOMPOC  PIC X.                                     00002230
             03 MNEANCOMPOP  PIC X.                                     00002240
             03 MNEANCOMPOH  PIC X.                                     00002250
             03 MNEANCOMPOV  PIC X.                                     00002260
             03 MNEANCOMPOO  PIC X(13).                                 00002270
           02 DFHMS3 OCCURS   10 TIMES .                                00002280
             03 FILLER       PIC X(2).                                  00002290
             03 MWAUTHENTA   PIC X.                                     00002300
             03 MWAUTHENTC   PIC X.                                     00002310
             03 MWAUTHENTP   PIC X.                                     00002320
             03 MWAUTHENTH   PIC X.                                     00002330
             03 MWAUTHENTV   PIC X.                                     00002340
             03 MWAUTHENTO   PIC X.                                     00002350
           02 DFHMS4 OCCURS   10 TIMES .                                00002360
             03 FILLER       PIC X(2).                                  00002370
             03 MWACTIFA     PIC X.                                     00002380
             03 MWACTIFC     PIC X.                                     00002390
             03 MWACTIFP     PIC X.                                     00002400
             03 MWACTIFH     PIC X.                                     00002410
             03 MWACTIFV     PIC X.                                     00002420
             03 MWACTIFO     PIC X.                                     00002430
           02 DFHMS5 OCCURS   10 TIMES .                                00002440
             03 FILLER       PIC X(2).                                  00002450
             03 MLNEANCOMPOA      PIC X.                                00002460
             03 MLNEANCOMPOC PIC X.                                     00002470
             03 MLNEANCOMPOP PIC X.                                     00002480
             03 MLNEANCOMPOH PIC X.                                     00002490
             03 MLNEANCOMPOV PIC X.                                     00002500
             03 MLNEANCOMPOO      PIC X(15).                            00002510
           02 DFHMS6 OCCURS   10 TIMES .                                00002520
             03 FILLER       PIC X(2).                                  00002530
             03 MDATEA  PIC X.                                          00002540
             03 MDATEC  PIC X.                                          00002550
             03 MDATEP  PIC X.                                          00002560
             03 MDATEH  PIC X.                                          00002570
             03 MDATEV  PIC X.                                          00002580
             03 MDATEO  PIC X(10).                                      00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MNOBSOLA  PIC X.                                          00002610
           02 MNOBSOLC  PIC X.                                          00002620
           02 MNOBSOLP  PIC X.                                          00002630
           02 MNOBSOLH  PIC X.                                          00002640
           02 MNOBSOLV  PIC X.                                          00002650
           02 MNOBSOLO  PIC X(4).                                       00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MZONCMDA  PIC X.                                          00002680
           02 MZONCMDC  PIC X.                                          00002690
           02 MZONCMDP  PIC X.                                          00002700
           02 MZONCMDH  PIC X.                                          00002710
           02 MZONCMDV  PIC X.                                          00002720
           02 MZONCMDO  PIC X(15).                                      00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MLIBERRA  PIC X.                                          00002750
           02 MLIBERRC  PIC X.                                          00002760
           02 MLIBERRP  PIC X.                                          00002770
           02 MLIBERRH  PIC X.                                          00002780
           02 MLIBERRV  PIC X.                                          00002790
           02 MLIBERRO  PIC X(58).                                      00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MCODTRAA  PIC X.                                          00002820
           02 MCODTRAC  PIC X.                                          00002830
           02 MCODTRAP  PIC X.                                          00002840
           02 MCODTRAH  PIC X.                                          00002850
           02 MCODTRAV  PIC X.                                          00002860
           02 MCODTRAO  PIC X(4).                                       00002870
           02 FILLER    PIC X(2).                                       00002880
           02 MCICSA    PIC X.                                          00002890
           02 MCICSC    PIC X.                                          00002900
           02 MCICSP    PIC X.                                          00002910
           02 MCICSH    PIC X.                                          00002920
           02 MCICSV    PIC X.                                          00002930
           02 MCICSO    PIC X(5).                                       00002940
           02 FILLER    PIC X(2).                                       00002950
           02 MNETNAMA  PIC X.                                          00002960
           02 MNETNAMC  PIC X.                                          00002970
           02 MNETNAMP  PIC X.                                          00002980
           02 MNETNAMH  PIC X.                                          00002990
           02 MNETNAMV  PIC X.                                          00003000
           02 MNETNAMO  PIC X(8).                                       00003010
           02 FILLER    PIC X(2).                                       00003020
           02 MSCREENA  PIC X.                                          00003030
           02 MSCREENC  PIC X.                                          00003040
           02 MSCREENP  PIC X.                                          00003050
           02 MSCREENH  PIC X.                                          00003060
           02 MSCREENV  PIC X.                                          00003070
           02 MSCREENO  PIC X(4).                                       00003080
                                                                                
