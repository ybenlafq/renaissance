      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TAUTO TYPE AUTORISATION VENTES         *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVGA01G6.                                                            
           05  TAUTO-CTABLEG2    PIC X(15).                                     
           05  TAUTO-CTABLEG2-REDEF REDEFINES TAUTO-CTABLEG2.                   
               10  TAUTO-CAUTO           PIC X(05).                             
           05  TAUTO-WTABLEG     PIC X(80).                                     
           05  TAUTO-WTABLEG-REDEF  REDEFINES TAUTO-WTABLEG.                    
               10  TAUTO-LAUTO           PIC X(15).                             
               10  TAUTO-WAUTO           PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01G6-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TAUTO-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TAUTO-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TAUTO-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TAUTO-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
