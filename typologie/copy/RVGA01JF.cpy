      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE LDSTK TABLE DES LIBELLES DESTOCKAGE    *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVGA01JF.                                                            
           05  LDSTK-CTABLEG2    PIC X(15).                                     
           05  LDSTK-CTABLEG2-REDEF REDEFINES LDSTK-CTABLEG2.                   
               10  LDSTK-RUBRIQUE        PIC X(05).                             
           05  LDSTK-WTABLEG     PIC X(80).                                     
           05  LDSTK-WTABLEG-REDEF  REDEFINES LDSTK-WTABLEG.                    
               10  LDSTK-LRUBRIQ         PIC X(30).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01JF-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LDSTK-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  LDSTK-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LDSTK-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  LDSTK-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
