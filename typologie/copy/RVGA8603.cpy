      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGA8603                                             
      **********************************************************                
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA8603                         
      *---------------------------------------------------------                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA8603.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA8603.                                                            
      *}                                                                        
           02  GA86-NENTCDE                                                     
               PIC X(0005).                                                     
           02  GA86-WEDI                                                        
               PIC X(0001).                                                     
           02  GA86-DEFFETEDI                                                   
               PIC X(0008).                                                     
           02  GA86-WEAN                                                        
               PIC X(0001).                                                     
           02  GA86-WMULTIDEP                                                   
               PIC X(0001).                                                     
           02  GA86-WEDITCDE                                                    
               PIC X(0001).                                                     
           02  GA86-DSYST                                                       
               PIC S9(0013) COMP-3.                                             
           02  GA86-ORDERS                                                      
               PIC X(0001).                                                     
           02  GA86-ORDCHG                                                      
               PIC X(0001).                                                     
           02  GA86-DEFFETCAL                                                   
               PIC X(0008).                                                     
           02  GA86-WVALO                                                       
               PIC X(0001).                                                     
           02  GA86-DVALO                                                       
               PIC X(0008).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGA8603                                  
      *---------------------------------------------------------                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA8603-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA8603-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA86-NENTCDE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA86-NENTCDE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA86-WEDI-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA86-WEDI-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA86-DEFFETEDI-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA86-DEFFETEDI-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA86-WEAN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA86-WEAN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA86-WMULTIDEP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA86-WMULTIDEP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA86-WEDITCDE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA86-WEDITCDE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA86-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA86-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA86-ORDERS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA86-ORDERS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA86-ORDCHG-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA86-ORDCHG-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA86-DEFFETCAL-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA86-DEFFETCAL-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA86-WVALO-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA86-WVALO-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA86-DVALO-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA86-DVALO-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
