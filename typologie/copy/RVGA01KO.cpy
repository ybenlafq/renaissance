      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HEAUT ACCES CENTRE GHE                 *        
      *----------------------------------------------------------------*        
       01  RVGA01KO.                                                            
           05  HEAUT-CTABLEG2    PIC X(15).                                     
           05  HEAUT-CTABLEG2-REDEF REDEFINES HEAUT-CTABLEG2.                   
               10  HEAUT-CACID           PIC X(04).                             
               10  HEAUT-CLIEUHET        PIC X(05).                             
           05  HEAUT-WTABLEG     PIC X(80).                                     
           05  HEAUT-WTABLEG-REDEF  REDEFINES HEAUT-WTABLEG.                    
               10  HEAUT-WACTIF          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01KO-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HEAUT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HEAUT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HEAUT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HEAUT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
