      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE IFTIE PHYNANCE : TIERS GCT EXCEPTION   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01TL.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01TL.                                                            
      *}                                                                        
           05  IFTIE-CTABLEG2    PIC X(15).                                     
           05  IFTIE-CTABLEG2-REDEF REDEFINES IFTIE-CTABLEG2.                   
               10  IFTIE-NSOCIETE        PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  IFTIE-NSOCIETE-N     REDEFINES IFTIE-NSOCIETE                
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  IFTIE-NLIEU           PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  IFTIE-NLIEU-N        REDEFINES IFTIE-NLIEU                   
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  IFTIE-WTABLEG     PIC X(80).                                     
           05  IFTIE-WTABLEG-REDEF  REDEFINES IFTIE-WTABLEG.                    
               10  IFTIE-WACTIF          PIC X(01).                             
               10  IFTIE-NTIERSCV        PIC X(06).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01TL-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01TL-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IFTIE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  IFTIE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IFTIE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  IFTIE-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
