      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      *================================================================*        
      *       DESCRIPTION DU FICHIER FPR930 D'EXTRACTION DES           *        
      *       PRESTATIONS D'ASSURANCE AXA                              *        
      *       FICHIER EN SORTIE DE BPR930 ET EN ENTREE DE BPR931       *        
      *       LONGUEUR D'ENREGISTREMENT : 359 C.                       *        
      *================================================================*        
M06726* MA9157 : Changer ";" en ";" pour ACCESS                        *        
M9157 *        : ajout CMODDEL                                         *        
      *================================================================*        
      *                                                                         
       01  FILLER.                                                              
      *                                                                         
           05  FPR930A-ENREG.                                                   
               10  FILLER                PIC  X(01) VALUE ';'.                  
               10  FILLER                PIC  X(01) VALUE ' '.                  
               10  FPR930A-NSOCIETE      PIC  X(03).                            
               10  FILLER                PIC  X(03) VALUE ' ; '.                
               10  FPR930A-NLIEU         PIC  X(03).                            
               10  FILLER                PIC  X(03) VALUE ' ; '.                
               10  FPR930A-LLIEU         PIC  X(20).                            
               10  FILLER                PIC  X(03) VALUE ' ; '.                
               10  FPR930A-NVENTE        PIC  X(07).                            
               10  FILLER                PIC  X(03) VALUE ' ; '.                
               10  FPR930A-DDELIV        PIC  X(08).                            
               10  FILLER                PIC  X(03) VALUE ' ; '.                
               10  FPR930A-CENREG        PIC  X(05).                            
               10  FILLER                PIC  X(03) VALUE ' ; '.                
               10  FPR930A-LPRESTATION   PIC  X(20).                            
               10  FILLER                PIC  X(03) VALUE ' ; '.                
               10  FPR930A-NCODIC        PIC  X(07).                            
               10  FILLER                PIC  X(03) VALUE ' ; '.                
               10  FPR930A-LREFFOURN     PIC  X(20).                            
               10  FILLER                PIC  X(03) VALUE ' ; '.                
               10  FPR930A-QVENDUE       PIC  9(05).                            
               10  FILLER                PIC  X(02) VALUE ' ;'.                 
               10  FPR930A-PVUNITF       PIC  -----99,99.                       
               10  FILLER                PIC  X(01) VALUE ';'.                  
               10  FPR930A-PVTOTAL       PIC  -----99,99.                       
               10  FILLER                PIC  X(02) VALUE '; '.                 
               10  FPR930A-CTITRENOM     PIC  X(05).                            
               10  FILLER                PIC  X(03) VALUE ' ; '.                
               10  FPR930A-LNOM          PIC  X(25).                            
               10  FILLER                PIC  X(03) VALUE ' ; '.                
               10  FPR930A-LPRENOM       PIC  X(15).                            
               10  FILLER                PIC  X(03) VALUE ' ; '.                
               10  FPR930A-CVOIE         PIC  X(05).                            
               10  FILLER                PIC  X(03) VALUE ' ; '.                
               10  FPR930A-CTVOIE        PIC  X(04).                            
               10  FILLER                PIC  X(03) VALUE ' ; '.                
               10  FPR930A-LNOMVOIE      PIC  X(21).                            
               10  FILLER                PIC  X(03) VALUE ' ; '.                
               10  FPR930A-LCOMMUNE      PIC  X(32).                            
               10  FILLER                PIC  X(03) VALUE ' ; '.                
               10  FPR930A-CPOSTAL       PIC  X(05).                            
               10  FILLER                PIC  X(03) VALUE ' ; '.                
               10  FPR930A-CINSEE        PIC  X(05).                            
               10  FILLER                PIC  X(03) VALUE ' ; '.                
               10  FPR930A-WANNULATION   PIC  X(06).                            
               10  FILLER                PIC  X(03) VALUE ' ; '.                
               10  FPR930A-DANNULATION   PIC  X(08).                            
               10  FILLER                PIC  X(03) VALUE ' ; '.                
               10  FPR930A-DDEBPER       PIC  X(08).                            
               10  FILLER                PIC  X(03) VALUE ' ; '.                
               10  FPR930A-DFINPER       PIC  X(08).                            
               10  FILLER                PIC  X(03) VALUE ' ; '.                
m9157 *        10  FILLER                PIC  X(18).                            
M9157          10  FPR930A-CMODDEL       PIC  X(03).                            
               10  FILLER                PIC  X(03) VALUE ' ; '.                
               10  FILLER                PIC  X(12).                            
               10  FILLER                PIC  X(01) VALUE ' '.                  
               10  FPR930A-NFICHIER      PIC  X(02).                            
                                                                                
