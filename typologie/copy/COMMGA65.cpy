      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *            
      **************************************************************            
      *                                                                         
      * COM-GA65-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-GA65-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-GA65-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA ----------------------------------- 100          
           02 FILLER-COM-AIDA      PIC X(100).                                  
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020          
           02 COMM-CICS-APPLID     PIC X(8).                                    
           02 COMM-CICS-NETNAM     PIC X(8).                                    
           02 COMM-CICS-TRANSA     PIC X(4).                                    
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100          
           02 COMM-DATE-SIECLE     PIC XX.                                      
           02 COMM-DATE-ANNEE      PIC XX.                                      
           02 COMM-DATE-MOIS       PIC XX.                                      
           02 COMM-DATE-JOUR       PIC XX.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA       PIC 999.                                     
           02 COMM-DATE-QNT0       PIC 99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX       PIC 9.                                       
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM        PIC 9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC     PIC XXX.                                     
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC    PIC XXX.                                     
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                                    
           02 COMM-DATE-AAMMJJ     PIC X(6).                                    
           02 COMM-DATE-JJMMSSAA   PIC X(8).                                    
           02 COMM-DATE-JJMMAA     PIC X(6).                                    
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                                    
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-FILLER     PIC X(14).                                   
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724          
      *                                                              4          
           02 COMM-NSOCIETE        PIC    X(3).                                 
           02 COMM-DPARIS          PIC    X.                                    
      *                                                                         
           02 COMM-GA65-APPLI.                                                  
      *                              ZONES DE SAUVEGARDE----------- 74          
              03 COMM-ZONES-SAUV.                                               
                 04 COMM-PROTEGE        PIC    X.                               
                 04 COMM-TOP-BLOCAGE    PIC    X.                               
                 04 COMM-CFONC          PIC    X(3).                            
                 04 COMM-NCODIC         PIC    X(7).                            
                 04 COMM-CFAM           PIC    X(5).                            
                 04 COMM-CFAM-ORIG      PIC    X(5).                            
                 04 COMM-CASSORT-ORIG   PIC    X(5).                            
                 04 COMM-CAPPRO-ORIG    PIC    X(5).                            
                 04 COMM-CEXPO-ORIG     PIC    X(5).                            
                 04 COMM-CTYPCONDT-ORIG PIC    X(5).                            
                 04 COMM-QCONDT-ORIG    PIC    S9(5) COMP-3.                    
                 04 COMM-WMULTIFAM-ORIG PIC    X(1).                            
                 04 COMM-CPROFIL        PIC    X(10).                           
                 04 COMM-CPROFIL-ORIG   PIC    X(10).                           
                 04 COMM-DDMAJ-PROF     PIC    X(8).                            
                 04 COMM-WSENSVTE-ORIG  PIC    X(1).                            
                 04 COMM-WSENSAPPRO-ORIG  PIC  X(1).                            
      *                              INFO ARTICLE GA00------------ 300          
              03 COMM-INFO-ARTICLE.                                             
      **********************************************************                
      *          COPY DES VARIABLES DE LA TABLE RVGA00         *                
      *                LONGUEUR = 300                          *                
      **********************************************************                
                 04  COMM-GA00-NCODIC           PIC X(7).                       
                 04  COMM-GA00-LREFFOURN        PIC X(20).                      
                 04  COMM-GA00-CFAM             PIC X(5).                       
                 04  COMM-GA00-CMARQ            PIC X(5).                       
                 04  COMM-GA00-CASSORT          PIC X(5).                       
                 04  COMM-GA00-DEFSTATUT        PIC X(8).                       
                 04  COMM-GA00-DCREATION        PIC X(8).                       
                 04  COMM-GA00-D1RECEPT         PIC X(8).                       
                 04  COMM-GA00-DMAJ             PIC X(8).                       
                 04  COMM-GA00-LREFDARTY        PIC X(20).                      
                 04  COMM-GA00-LCOMMENT         PIC X(50).                      
                 04  COMM-GA00-LSTATCOMP        PIC X(3).                       
                 04  COMM-GA00-WDACEM           PIC X(1).                       
                 04  COMM-GA00-WTLMELA          PIC X(1).                       
                 04  COMM-GA00-PRAR             PIC S9(7)V9(2) COMP-3.          
                 04  COMM-GA00-CTAUXTVA         PIC X(5).                       
                 04  COMM-GA00-CEXPO            PIC X(5).                       
                 04  COMM-GA00-WSENSVTE         PIC X(1).                       
                 04  COMM-GA00-CGESTVTE         PIC X(3).                       
                 04  COMM-GA00-CGARANTIE        PIC X(5).                       
                 04  COMM-GA00-CAPPRO           PIC X(5).                       
                 04  COMM-GA00-WSENSAPPRO       PIC X(1).                       
                 04  COMM-GA00-QDELAIAPPRO      PIC X(3).                       
                 04  COMM-GA00-QCOLICDE         PIC S9(3) COMP-3.               
                 04  COMM-GA00-CHEFPROD         PIC X(5).                       
                 04  COMM-GA00-CORIGPROD        PIC X(5).                       
                 04  COMM-GA00-LEMBALLAGE       PIC X(50).                      
                 04  COMM-GA00-CTYPCONDT        PIC X(5).                       
                 04  COMM-GA00-QCONDT           PIC S9(5) COMP-3.               
                 04  COMM-GA00-QCOLIRECEPT      PIC S9(5) COMP-3.               
                 04  COMM-GA00-QCOLIDSTOCK      PIC S9(5) COMP-3.               
                 04  COMM-GA00-QCOLIVTE         PIC S9(5) COMP-3.               
                 04  COMM-GA00-QPOIDS           PIC S9(7) COMP-3.               
                 04  COMM-GA00-QLARGEUR         PIC S9(3) COMP-3.               
                 04  COMM-GA00-QPROFONDEUR      PIC S9(3) COMP-3.               
                 04  COMM-GA00-QHAUTEUR         PIC S9(3) COMP-3.               
                 04  COMM-GA00-CGARCONST        PIC X(5).                       
                 04  COMM-GA00-CMODSTOCK1       PIC X(5).                       
                 04  COMM-GA00-WMODSTOCK1       PIC X(1).                       
                 04  COMM-GA00-CMODSTOCK2       PIC X(5).                       
                 04  COMM-GA00-WMODSTOCK2       PIC X(1).                       
                 04  COMM-GA00-CMODSTOCK3       PIC X(5).                       
                 04  COMM-GA00-WMODSTOCK3       PIC X(1).                       
                 04  COMM-GA00-CFETEMPL         PIC X(1).                       
                 04  COMM-GA00-QNBRANMAIL       PIC S9(3) COMP-3.               
                 04  COMM-GA00-QNBNIVGERB       PIC S9(3) COMP-3.               
                 04  COMM-GA00-CZONEACCES       PIC X(1).                       
                 04  COMM-GA00-CCONTENEUR       PIC X(1).                       
                 04  COMM-GA00-CSPECIFSTK       PIC X(1).                       
                 04  COMM-GA00-CCOTEHOLD        PIC X(1).                       
                 04  COMM-GA00-CFETIQINFO       PIC X(1).                       
                 04  COMM-GA00-CFETIQPRIX       PIC X(1).                       
                 04  COMM-GA00-QNBPVSOL         PIC S9(3) COMP-3.               
                 04  COMM-GA00-QNBPRACK         PIC S9(3) COMP-3.               
                 04  COMM-GA00-WLCONF           PIC X(1).                       
      *                              INFO FAMILLE GA14------------- 52          
              03 COMM-INFO-FAMILLE.                                             
      **********************************************************                
      *        COPY DES VARIABLES DE LA TABLE RVGA1400         *                
      **********************************************************                
                 04  COMM-GA14-CFAM             PIC X(5).                       
                 04  COMM-GA14-LFAM             PIC X(20).                      
                 04  COMM-GA14-CMODSTOCK        PIC X(5).                       
                 04  COMM-GA14-CTAUXTVA         PIC X(5).                       
                 04  COMM-GA14-CGARANTIE        PIC X(5).                       
                 04  COMM-GA14-QBORNESOL        PIC S9(2)V9(3) COMP-3.          
                 04  COMM-GA14-QBORNEVRAC       PIC S9(2)V9(3) COMP-3.          
                 04  COMM-GA14-QNBPVSOL         PIC S9(3) COMP-3.               
                 04  COMM-GA14-WMULTIFAM        PIC X(1).                       
                 04  COMM-GA14-WSEQFAM          PIC S9(5) COMP-3.               
      *                              INFO VRAC -------------------- 87          
              03 COMM-INFO-VRAC.                                                
                 04 COMM-LMARQ         PIC    X(20).                            
                 04 COMM-LAPPRO        PIC    X(20).                            
                 04 COMM-LASSORT       PIC    X(20).                            
                 04 COMM-LEXPO         PIC    X(20).                            
                 04 COMM-LIAISON       PIC    X(5).                             
                 04 COMM-ZONEPV        PIC    X(2).                             
      *                              INFO CONTROLE ---------------- 13          
              03 COMM-INFO-CONTROLE.                                            
                 04 COMM-CONTROLE      PIC    X(1)   OCCURS 13.                 
      *                              INFO MISE A JOUR ------------- 13          
              03 COMM-INFO-MAJ.                                                 
                 04 COMM-MAJ           PIC    X(1)   OCCURS 13.                 
      *                              INFO TS ---------------------- 96          
              03 COMM-NOM-TS-66.                                                
                 04 COMM-DEB-NOM-TS-66 PIC    X(4).                             
                 04 COMM-FIN-NOM-TS-66 PIC    X(4).                             
              03 FILLER                PIC    X(227).                           
      *                              FILLER --------------------- 3096          
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGA66                    TR: GA66  *    00020001
      *           IDENTIFICATION ARTICLES-CODES DESCRIPTIFS.       *    00030000
      **************************************************************    00040000
           02 COMM-GA66-DONNEES.                                        00090001
              03 NUMERO-DE-PAGE           PIC 9(2).                        00101
              03 NOMBRE-DE-PAGE           PIC 9(2).                        00101
              03 COMM-GA66-FAMCLT         PIC X(30).                       00209
              03 COMM-GA66-DMAJ           PIC X(8).                        00209
              03 COMM-GA66-WPROFIL-COUL   PIC  X.                               
                 88 COMM-GA66-PROFIL-COULEUR   VALUE 'O'.                       
              03 COMM-GA66-FILLER         PIC X(3053).                  00270011
      ***************************************************************** 00280000
                                                                                
