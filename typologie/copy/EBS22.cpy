      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESE03   ESE03                                              00000020
      ***************************************************************** 00000030
       01   EBS22I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITREL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MTITREL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTITREF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MTITREI   PIC X(37).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSPRODUITL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MSPRODUITL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSPRODUITF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MSPRODUITI     PIC X.                                     00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSSERVICEL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MSSERVICEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSSERVICEF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSSERVICEI     PIC X.                                     00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOFFREL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MSOFFREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOFFREF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MSOFFREI  PIC X.                                          00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTITERL     COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MNENTITERL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNENTITERF     PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNENTITERI     PIC X(7).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMRL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCFAMRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAMRF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCFAMRI   PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQRL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCMARQRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMARQRF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCMARQRI  PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTITERL     COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MLENTITERL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLENTITERF     PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLENTITERI     PIC X(20).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MPAGEI    PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MPAGEMAXI      PIC X(3).                                  00000530
           02 MTABLIST1I OCCURS   16 TIMES .                            00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPENTL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MTYPENTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTYPENTF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MTYPENTI     PIC X(2).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNENTITEL    COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MNENTITEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNENTITEF    PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNENTITEI    PIC X(7).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCFAMI  PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCMARQI      PIC X(5).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLENTITEL    COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MLENTITEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLENTITEF    PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MLENTITEI    PIC X(20).                                 00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTAL   COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MSTAL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSTAF   PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MSTAI   PIC X(3).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIXL  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MPRIXL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MPRIXF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MPRIXI  PIC X(8).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBRLIENL    COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MNBRLIENL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNBRLIENF    PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MNBRLIENI    PIC X(6).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000870
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MSELI   PIC X.                                          00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MZONCMDI  PIC X(15).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLIBERRI  PIC X(58).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCODTRAI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCICSI    PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MNETNAMI  PIC X(8).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MSCREENI  PIC X(4).                                       00001140
      ***************************************************************** 00001150
      * SDF: ESE03   ESE03                                              00001160
      ***************************************************************** 00001170
       01   EBS22O REDEFINES EBS22I.                                    00001180
           02 FILLER    PIC X(12).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTIMJOUA  PIC X.                                          00001280
           02 MTIMJOUC  PIC X.                                          00001290
           02 MTIMJOUP  PIC X.                                          00001300
           02 MTIMJOUH  PIC X.                                          00001310
           02 MTIMJOUV  PIC X.                                          00001320
           02 MTIMJOUO  PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MTITREA   PIC X.                                          00001350
           02 MTITREC   PIC X.                                          00001360
           02 MTITREP   PIC X.                                          00001370
           02 MTITREH   PIC X.                                          00001380
           02 MTITREV   PIC X.                                          00001390
           02 MTITREO   PIC X(37).                                      00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MSPRODUITA     PIC X.                                     00001420
           02 MSPRODUITC     PIC X.                                     00001430
           02 MSPRODUITP     PIC X.                                     00001440
           02 MSPRODUITH     PIC X.                                     00001450
           02 MSPRODUITV     PIC X.                                     00001460
           02 MSPRODUITO     PIC X.                                     00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MSSERVICEA     PIC X.                                     00001490
           02 MSSERVICEC     PIC X.                                     00001500
           02 MSSERVICEP     PIC X.                                     00001510
           02 MSSERVICEH     PIC X.                                     00001520
           02 MSSERVICEV     PIC X.                                     00001530
           02 MSSERVICEO     PIC X.                                     00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MSOFFREA  PIC X.                                          00001560
           02 MSOFFREC  PIC X.                                          00001570
           02 MSOFFREP  PIC X.                                          00001580
           02 MSOFFREH  PIC X.                                          00001590
           02 MSOFFREV  PIC X.                                          00001600
           02 MSOFFREO  PIC X.                                          00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MNENTITERA     PIC X.                                     00001630
           02 MNENTITERC     PIC X.                                     00001640
           02 MNENTITERP     PIC X.                                     00001650
           02 MNENTITERH     PIC X.                                     00001660
           02 MNENTITERV     PIC X.                                     00001670
           02 MNENTITERO     PIC X(7).                                  00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MCFAMRA   PIC X.                                          00001700
           02 MCFAMRC   PIC X.                                          00001710
           02 MCFAMRP   PIC X.                                          00001720
           02 MCFAMRH   PIC X.                                          00001730
           02 MCFAMRV   PIC X.                                          00001740
           02 MCFAMRO   PIC X(5).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MCMARQRA  PIC X.                                          00001770
           02 MCMARQRC  PIC X.                                          00001780
           02 MCMARQRP  PIC X.                                          00001790
           02 MCMARQRH  PIC X.                                          00001800
           02 MCMARQRV  PIC X.                                          00001810
           02 MCMARQRO  PIC X(5).                                       00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MLENTITERA     PIC X.                                     00001840
           02 MLENTITERC     PIC X.                                     00001850
           02 MLENTITERP     PIC X.                                     00001860
           02 MLENTITERH     PIC X.                                     00001870
           02 MLENTITERV     PIC X.                                     00001880
           02 MLENTITERO     PIC X(20).                                 00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MPAGEA    PIC X.                                          00001910
           02 MPAGEC    PIC X.                                          00001920
           02 MPAGEP    PIC X.                                          00001930
           02 MPAGEH    PIC X.                                          00001940
           02 MPAGEV    PIC X.                                          00001950
           02 MPAGEO    PIC X(3).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MPAGEMAXA      PIC X.                                     00001980
           02 MPAGEMAXC PIC X.                                          00001990
           02 MPAGEMAXP PIC X.                                          00002000
           02 MPAGEMAXH PIC X.                                          00002010
           02 MPAGEMAXV PIC X.                                          00002020
           02 MPAGEMAXO      PIC X(3).                                  00002030
           02 MTABLIST1O OCCURS   16 TIMES .                            00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MTYPENTA     PIC X.                                     00002060
             03 MTYPENTC     PIC X.                                     00002070
             03 MTYPENTP     PIC X.                                     00002080
             03 MTYPENTH     PIC X.                                     00002090
             03 MTYPENTV     PIC X.                                     00002100
             03 MTYPENTO     PIC X(2).                                  00002110
             03 FILLER       PIC X(2).                                  00002120
             03 MNENTITEA    PIC X.                                     00002130
             03 MNENTITEC    PIC X.                                     00002140
             03 MNENTITEP    PIC X.                                     00002150
             03 MNENTITEH    PIC X.                                     00002160
             03 MNENTITEV    PIC X.                                     00002170
             03 MNENTITEO    PIC X(7).                                  00002180
             03 FILLER       PIC X(2).                                  00002190
             03 MCFAMA  PIC X.                                          00002200
             03 MCFAMC  PIC X.                                          00002210
             03 MCFAMP  PIC X.                                          00002220
             03 MCFAMH  PIC X.                                          00002230
             03 MCFAMV  PIC X.                                          00002240
             03 MCFAMO  PIC X(5).                                       00002250
             03 FILLER       PIC X(2).                                  00002260
             03 MCMARQA      PIC X.                                     00002270
             03 MCMARQC PIC X.                                          00002280
             03 MCMARQP PIC X.                                          00002290
             03 MCMARQH PIC X.                                          00002300
             03 MCMARQV PIC X.                                          00002310
             03 MCMARQO      PIC X(5).                                  00002320
             03 FILLER       PIC X(2).                                  00002330
             03 MLENTITEA    PIC X.                                     00002340
             03 MLENTITEC    PIC X.                                     00002350
             03 MLENTITEP    PIC X.                                     00002360
             03 MLENTITEH    PIC X.                                     00002370
             03 MLENTITEV    PIC X.                                     00002380
             03 MLENTITEO    PIC X(20).                                 00002390
             03 FILLER       PIC X(2).                                  00002400
             03 MSTAA   PIC X.                                          00002410
             03 MSTAC   PIC X.                                          00002420
             03 MSTAP   PIC X.                                          00002430
             03 MSTAH   PIC X.                                          00002440
             03 MSTAV   PIC X.                                          00002450
             03 MSTAO   PIC X(3).                                       00002460
             03 FILLER       PIC X(2).                                  00002470
             03 MPRIXA  PIC X.                                          00002480
             03 MPRIXC  PIC X.                                          00002490
             03 MPRIXP  PIC X.                                          00002500
             03 MPRIXH  PIC X.                                          00002510
             03 MPRIXV  PIC X.                                          00002520
             03 MPRIXO  PIC X(8).                                       00002530
             03 FILLER       PIC X(2).                                  00002540
             03 MNBRLIENA    PIC X.                                     00002550
             03 MNBRLIENC    PIC X.                                     00002560
             03 MNBRLIENP    PIC X.                                     00002570
             03 MNBRLIENH    PIC X.                                     00002580
             03 MNBRLIENV    PIC X.                                     00002590
             03 MNBRLIENO    PIC X(6).                                  00002600
             03 FILLER       PIC X(2).                                  00002610
             03 MSELA   PIC X.                                          00002620
             03 MSELC   PIC X.                                          00002630
             03 MSELP   PIC X.                                          00002640
             03 MSELH   PIC X.                                          00002650
             03 MSELV   PIC X.                                          00002660
             03 MSELO   PIC X.                                          00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MZONCMDA  PIC X.                                          00002690
           02 MZONCMDC  PIC X.                                          00002700
           02 MZONCMDP  PIC X.                                          00002710
           02 MZONCMDH  PIC X.                                          00002720
           02 MZONCMDV  PIC X.                                          00002730
           02 MZONCMDO  PIC X(15).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MLIBERRA  PIC X.                                          00002760
           02 MLIBERRC  PIC X.                                          00002770
           02 MLIBERRP  PIC X.                                          00002780
           02 MLIBERRH  PIC X.                                          00002790
           02 MLIBERRV  PIC X.                                          00002800
           02 MLIBERRO  PIC X(58).                                      00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MCODTRAA  PIC X.                                          00002830
           02 MCODTRAC  PIC X.                                          00002840
           02 MCODTRAP  PIC X.                                          00002850
           02 MCODTRAH  PIC X.                                          00002860
           02 MCODTRAV  PIC X.                                          00002870
           02 MCODTRAO  PIC X(4).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MCICSA    PIC X.                                          00002900
           02 MCICSC    PIC X.                                          00002910
           02 MCICSP    PIC X.                                          00002920
           02 MCICSH    PIC X.                                          00002930
           02 MCICSV    PIC X.                                          00002940
           02 MCICSO    PIC X(5).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MNETNAMA  PIC X.                                          00002970
           02 MNETNAMC  PIC X.                                          00002980
           02 MNETNAMP  PIC X.                                          00002990
           02 MNETNAMH  PIC X.                                          00003000
           02 MNETNAMV  PIC X.                                          00003010
           02 MNETNAMO  PIC X(8).                                       00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MSCREENA  PIC X.                                          00003040
           02 MSCREENC  PIC X.                                          00003050
           02 MSCREENP  PIC X.                                          00003060
           02 MSCREENH  PIC X.                                          00003070
           02 MSCREENV  PIC X.                                          00003080
           02 MSCREENO  PIC X(4).                                       00003090
                                                                                
