      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESE03   ESE03                                              00000020
      ***************************************************************** 00000030
       01   EBS13I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHEFPRODL     COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MCHEFPRODL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCHEFPRODF     PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCHEFPRODI     PIC X(5).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCHEFPRODL    COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MLCHEFPRODL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLCHEFPRODF    PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLCHEFPRODI    PIC X(20).                                 00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MWFONCI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MPAGEI    PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MPAGEMAXI      PIC X(3).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODEL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCODEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCODEF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCODEI    PIC X(5).                                       00000370
           02 MTABLISTI OCCURS   13 TIMES .                             00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPROFASSL   COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MCPROFASSL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCPROFASSF   PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MCPROFASSI   PIC X(5).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLPROFASSL   COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MLPROFASSL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLPROFASSF   PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MLPROFASSI   PIC X(20).                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOUNTL      COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MCOUNTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCOUNTF      PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MCOUNTI      PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWACTL  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MWACTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MWACTF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MWACTI  PIC X.                                          00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MZONCMDI  PIC X(15).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MLIBERRI  PIC X(58).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCODTRAI  PIC X(4).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCICSI    PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MNETNAMI  PIC X(8).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MSCREENI  PIC X(4).                                       00000780
      ***************************************************************** 00000790
      * SDF: ESE03   ESE03                                              00000800
      ***************************************************************** 00000810
       01   EBS13O REDEFINES EBS13I.                                    00000820
           02 FILLER    PIC X(12).                                      00000830
           02 FILLER    PIC X(2).                                       00000840
           02 MDATJOUA  PIC X.                                          00000850
           02 MDATJOUC  PIC X.                                          00000860
           02 MDATJOUP  PIC X.                                          00000870
           02 MDATJOUH  PIC X.                                          00000880
           02 MDATJOUV  PIC X.                                          00000890
           02 MDATJOUO  PIC X(10).                                      00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MTIMJOUA  PIC X.                                          00000920
           02 MTIMJOUC  PIC X.                                          00000930
           02 MTIMJOUP  PIC X.                                          00000940
           02 MTIMJOUH  PIC X.                                          00000950
           02 MTIMJOUV  PIC X.                                          00000960
           02 MTIMJOUO  PIC X(5).                                       00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MCHEFPRODA     PIC X.                                     00000990
           02 MCHEFPRODC     PIC X.                                     00001000
           02 MCHEFPRODP     PIC X.                                     00001010
           02 MCHEFPRODH     PIC X.                                     00001020
           02 MCHEFPRODV     PIC X.                                     00001030
           02 MCHEFPRODO     PIC X(5).                                  00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MLCHEFPRODA    PIC X.                                     00001060
           02 MLCHEFPRODC    PIC X.                                     00001070
           02 MLCHEFPRODP    PIC X.                                     00001080
           02 MLCHEFPRODH    PIC X.                                     00001090
           02 MLCHEFPRODV    PIC X.                                     00001100
           02 MLCHEFPRODO    PIC X(20).                                 00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MWFONCA   PIC X.                                          00001130
           02 MWFONCC   PIC X.                                          00001140
           02 MWFONCP   PIC X.                                          00001150
           02 MWFONCH   PIC X.                                          00001160
           02 MWFONCV   PIC X.                                          00001170
           02 MWFONCO   PIC X(3).                                       00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MPAGEA    PIC X.                                          00001200
           02 MPAGEC    PIC X.                                          00001210
           02 MPAGEP    PIC X.                                          00001220
           02 MPAGEH    PIC X.                                          00001230
           02 MPAGEV    PIC X.                                          00001240
           02 MPAGEO    PIC X(3).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MPAGEMAXA      PIC X.                                     00001270
           02 MPAGEMAXC PIC X.                                          00001280
           02 MPAGEMAXP PIC X.                                          00001290
           02 MPAGEMAXH PIC X.                                          00001300
           02 MPAGEMAXV PIC X.                                          00001310
           02 MPAGEMAXO      PIC X(3).                                  00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MCODEA    PIC X.                                          00001340
           02 MCODEC    PIC X.                                          00001350
           02 MCODEP    PIC X.                                          00001360
           02 MCODEH    PIC X.                                          00001370
           02 MCODEV    PIC X.                                          00001380
           02 MCODEO    PIC X(5).                                       00001390
           02 MTABLISTO OCCURS   13 TIMES .                             00001400
             03 FILLER       PIC X(2).                                  00001410
             03 MCPROFASSA   PIC X.                                     00001420
             03 MCPROFASSC   PIC X.                                     00001430
             03 MCPROFASSP   PIC X.                                     00001440
             03 MCPROFASSH   PIC X.                                     00001450
             03 MCPROFASSV   PIC X.                                     00001460
             03 MCPROFASSO   PIC X(5).                                  00001470
             03 FILLER       PIC X(2).                                  00001480
             03 MLPROFASSA   PIC X.                                     00001490
             03 MLPROFASSC   PIC X.                                     00001500
             03 MLPROFASSP   PIC X.                                     00001510
             03 MLPROFASSH   PIC X.                                     00001520
             03 MLPROFASSV   PIC X.                                     00001530
             03 MLPROFASSO   PIC X(20).                                 00001540
             03 FILLER       PIC X(2).                                  00001550
             03 MCOUNTA      PIC X.                                     00001560
             03 MCOUNTC PIC X.                                          00001570
             03 MCOUNTP PIC X.                                          00001580
             03 MCOUNTH PIC X.                                          00001590
             03 MCOUNTV PIC X.                                          00001600
             03 MCOUNTO      PIC X(5).                                  00001610
             03 FILLER       PIC X(2).                                  00001620
             03 MWACTA  PIC X.                                          00001630
             03 MWACTC  PIC X.                                          00001640
             03 MWACTP  PIC X.                                          00001650
             03 MWACTH  PIC X.                                          00001660
             03 MWACTV  PIC X.                                          00001670
             03 MWACTO  PIC X.                                          00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MZONCMDA  PIC X.                                          00001700
           02 MZONCMDC  PIC X.                                          00001710
           02 MZONCMDP  PIC X.                                          00001720
           02 MZONCMDH  PIC X.                                          00001730
           02 MZONCMDV  PIC X.                                          00001740
           02 MZONCMDO  PIC X(15).                                      00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MLIBERRA  PIC X.                                          00001770
           02 MLIBERRC  PIC X.                                          00001780
           02 MLIBERRP  PIC X.                                          00001790
           02 MLIBERRH  PIC X.                                          00001800
           02 MLIBERRV  PIC X.                                          00001810
           02 MLIBERRO  PIC X(58).                                      00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MCODTRAA  PIC X.                                          00001840
           02 MCODTRAC  PIC X.                                          00001850
           02 MCODTRAP  PIC X.                                          00001860
           02 MCODTRAH  PIC X.                                          00001870
           02 MCODTRAV  PIC X.                                          00001880
           02 MCODTRAO  PIC X(4).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MCICSA    PIC X.                                          00001910
           02 MCICSC    PIC X.                                          00001920
           02 MCICSP    PIC X.                                          00001930
           02 MCICSH    PIC X.                                          00001940
           02 MCICSV    PIC X.                                          00001950
           02 MCICSO    PIC X(5).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MNETNAMA  PIC X.                                          00001980
           02 MNETNAMC  PIC X.                                          00001990
           02 MNETNAMP  PIC X.                                          00002000
           02 MNETNAMH  PIC X.                                          00002010
           02 MNETNAMV  PIC X.                                          00002020
           02 MNETNAMO  PIC X(8).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MSCREENA  PIC X.                                          00002050
           02 MSCREENC  PIC X.                                          00002060
           02 MSCREENP  PIC X.                                          00002070
           02 MSCREENH  PIC X.                                          00002080
           02 MSCREENV  PIC X.                                          00002090
           02 MSCREENO  PIC X(4).                                       00002100
                                                                                
