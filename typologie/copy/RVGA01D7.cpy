      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE INVST STATUT DE DEPRECIATION / TYPE    *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01D7.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01D7.                                                            
      *}                                                                        
           05  INVST-CTABLEG2    PIC X(15).                                     
           05  INVST-CTABLEG2-REDEF REDEFINES INVST-CTABLEG2.                   
               10  INVST-TYPE            PIC X(03).                             
               10  INVST-STATUT          PIC X(01).                             
           05  INVST-WTABLEG     PIC X(80).                                     
           05  INVST-WTABLEG-REDEF  REDEFINES INVST-WTABLEG.                    
               10  INVST-FLAG            PIC X(01).                             
               10  INVST-COEFF           PIC X(05).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  INVST-COEFF-N        REDEFINES INVST-COEFF                   
                                         PIC 9(01)V9(04).                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  INVST-LIBELLE         PIC X(20).                             
               10  INVST-CONTROLE        PIC X(08).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01D7-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01D7-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  INVST-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  INVST-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  INVST-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  INVST-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
