      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVRX2200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRX2200                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRX2200.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRX2200.                                                            
      *}                                                                        
           02  RX22-NRELEVE                                                     
               PIC X(0007).                                                     
           02  RX22-NCONC                                                       
               PIC X(0004).                                                     
           02  RX22-NMAG                                                        
               PIC X(0003).                                                     
           02  RX22-WTYPREL                                                     
               PIC X(0001).                                                     
           02  RX22-DSUPPORT                                                    
               PIC X(0008).                                                     
           02  RX22-DRELEVE                                                     
               PIC X(0008).                                                     
           02  RX22-DHRELEVE                                                    
               PIC X(0002).                                                     
           02  RX22-D1SAIREL                                                    
               PIC X(0008).                                                     
           02  RX22-DCLOREL                                                     
               PIC X(0008).                                                     
           02  RX22-DTRTREL                                                     
               PIC X(0008).                                                     
           02  RX22-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRX2200                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRX2200-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRX2200-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX22-NRELEVE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX22-NRELEVE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX22-NCONC-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX22-NCONC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX22-NMAG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX22-NMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX22-WTYPREL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX22-WTYPREL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX22-DSUPPORT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX22-DSUPPORT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX22-DRELEVE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX22-DRELEVE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX22-DHRELEVE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX22-DHRELEVE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX22-D1SAIREL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX22-D1SAIREL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX22-DCLOREL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX22-DCLOREL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX22-DTRTREL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX22-DTRTREL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX22-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX22-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
