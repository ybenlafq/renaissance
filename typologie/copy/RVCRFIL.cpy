      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CRFIL SAV REF INTER-FILIALE ELA        *        
      *----------------------------------------------------------------*        
       01  RVCRFIL.                                                             
           05  CRFIL-CTABLEG2    PIC X(15).                                     
           05  CRFIL-CTABLEG2-REDEF REDEFINES CRFIL-CTABLEG2.                   
               10  CRFIL-CNSOC           PIC X(03).                             
               10  CRFIL-NLIEU           PIC X(03).                             
           05  CRFIL-WTABLEG     PIC X(80).                                     
           05  CRFIL-WTABLEG-REDEF  REDEFINES CRFIL-WTABLEG.                    
               10  CRFIL-SAVREF          PIC X(06).                             
               10  CRFIL-PRFDOS          PIC X(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVCRFIL-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CRFIL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CRFIL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CRFIL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CRFIL-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
