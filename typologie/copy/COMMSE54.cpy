      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
       01  Z-COMMAREA-MSE54.                                            00000020
           02 COMM-MSE54-ENTETE-RETOUR.                                 00000030
      *---- BOOLEEN TYPE D'ERREUR                                       00000040
              03 COMM-MSE54-TYPERR     PIC 9(01).                       00000050
                 88 MSE54-NO-ERROR       VALUE 0.                       00000060
                 88 MSE54-APPL-ERROR     VALUE 1.                       00000070
                 88 MSE54-DB2-ERROR      VALUE 2.                       00000080
              03 COMM-MSE54-FUNC-SQL   PIC X(08).                       00000090
              03 COMM-MSE54-TABLE-NAME PIC X(08).                       00000100
              03 COMM-MSE54-SQLCODE    PIC S9(04).                      00000110
              03 COMM-MSE54-POSITION   PIC  9(03).                      00000120
           02 COMM-MSE54-ZINOUT.                                        00000130
              03 COMM-MSE54-CODESFONCTION   PIC X(12).                  00000140
              03 COMM-MSE54-WFONC           PIC X(03).                  00000150
              03 COMM-MSE54-NCSERV          PIC X(05).                  00000160
              03 COMM-MSE54-CSIGNE          PIC X.                      00000170
              03 COMM-MSE54-DMAJ            PIC X(08).                  00000180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-MSE54-NBR-OCCURS      PIC S9(04)   COMP.          00000190
      *--                                                                       
              03 COMM-MSE54-NBR-OCCURS      PIC S9(04) COMP-5.                  
      *}                                                                        
              03 COMM-MSE54-TABLEAU.                                    00000200
                 05 COMM-MSE54-LIGNE       OCCURS 30.                   00000210
                    10 COMM-MSE54-PRMPN     PIC S9(7)V99 COMP-3.        00000220
                    10 COMM-MSE54-DPRMPN    PIC X(8).                   00000230
                    10 COMM-MSE54-NZONPRIX  PIC XX.                     00000240
                    10 COMM-MSE54-PRIXN     PIC S9(7)V99 COMP-3.        00000250
                    10 COMM-MSE54-PRIXN-F   PIC X.                      00000260
                    10 COMM-MSE54-INTERN    PIC S9(5)V99 COMP-3.        00000270
                    10 COMM-MSE54-INTERN-F  PIC X.                      00000280
                    10 COMM-MSE54-DATEN     PIC X(8).                   00000290
                                                                        00000300
                                                                        00000310
      *------------ CODE RETOUR POSITIONNEMENT DES ERREURS              00000320
                    10 COMM-MSE54-CC-ERR    PIC X(04).                  00000330
                                                                        00000340
           02 COMM-MSE54-FILLER            PIC X(5805).                 00000350
                                                                                
