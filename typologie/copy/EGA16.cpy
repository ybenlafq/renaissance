      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA16   EGA16                                              00000020
      ***************************************************************** 00000030
       01   EGA16I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MFONCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCIETEI     PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCIETEL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MLSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLSOCIETEF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLSOCIETEI     PIC X(20).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNLIEUI   PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLLIEUI   PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPLL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCTYPLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCTYPLF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCTYPLI   PIC X.                                          00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCMPAD1L      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLCMPAD1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLCMPAD1F      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLCMPAD1I      PIC X(32).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCMPAD2L      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MLCMPAD2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLCMPAD2F      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLCMPAD2I      PIC X(32).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCMPAD3L      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MLCMPAD3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLCMPAD3F      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLCMPAD3I      PIC X(32).                                 00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTALL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MCPOSTALL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPOSTALF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCPOSTALI      PIC X(5).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMMUNEL     COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MLCOMMUNEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLCOMMUNEF     PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLCOMMUNEI     PIC X(32).                                 00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAYSL    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MPAYSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAYSF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MPAYSI    PIC X(2).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPAYSL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLPAYSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLPAYSF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLPAYSI   PIC X(20).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPFACTL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MTYPFACTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPFACTF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MTYPFACTI      PIC X(5).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCCAL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MNSOCCAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSOCCAF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNSOCCAI  PIC X(3).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPCONTL      COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MTYPCONTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPCONTF      PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MTYPCONTI      PIC X(5).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBTYPCONL    COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MLIBTYPCONL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLIBTYPCONF    PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLIBTYPCONI    PIC X(20).                                 00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MORDRECAL      COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MORDRECAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MORDRECAF      PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MORDRECAI      PIC X(3).                                  00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTESCPTEL      COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MTESCPTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTESCPTEF      PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MTESCPTEI      PIC X(5).                                  00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCONDREGLL     COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MCONDREGLL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCONDREGLF     PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCONDREGLI     PIC X(20).                                 00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MACID1L   COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MACID1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MACID1F   PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MACID1I   PIC X(4).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MACID2L   COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MACID2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MACID2F   PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MACID2I   PIC X(4).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MACID3L   COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MACID3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MACID3F   PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MACID3I   PIC X(4).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCFACL      COMP PIC S9(4).                            00001060
      *--                                                                       
           02 MNSOCFACL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCFACF      PIC X.                                     00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MNSOCFACI      PIC X(3).                                  00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUFACL     COMP PIC S9(4).                            00001100
      *--                                                                       
           02 MNLIEUFACL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNLIEUFACF     PIC X.                                     00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MNLIEUFACI     PIC X(3).                                  00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MACID4L   COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MACID4L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MACID4F   PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MACID4I   PIC X(4).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MACID5L   COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MACID5L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MACID5F   PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MACID5I   PIC X(4).                                       00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MACID6L   COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MACID6L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MACID6F   PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MACID6I   PIC X(4).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCCOMPTL    COMP PIC S9(4).                            00001260
      *--                                                                       
           02 MNSOCCOMPTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNSOCCOMPTF    PIC X.                                     00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MNSOCCOMPTI    PIC X(3).                                  00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUCOMPTL   COMP PIC S9(4).                            00001300
      *--                                                                       
           02 MNLIEUCOMPTL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNLIEUCOMPTF   PIC X.                                     00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MNLIEUCOMPTI   PIC X(3).                                  00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSECTANAL      COMP PIC S9(4).                            00001340
      *--                                                                       
           02 MSECTANAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSECTANAF      PIC X.                                     00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MSECTANAI      PIC X(6).                                  00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCUMCAL   COMP PIC S9(4).                                 00001380
      *--                                                                       
           02 MCUMCAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCUMCAF   PIC X.                                          00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MCUMCAI   PIC X.                                          00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCVALOL     COMP PIC S9(4).                            00001420
      *--                                                                       
           02 MNSOCVALOL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCVALOF     PIC X.                                     00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MNSOCVALOI     PIC X(3).                                  00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUVALOL    COMP PIC S9(4).                            00001460
      *--                                                                       
           02 MNLIEUVALOL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNLIEUVALOF    PIC X.                                     00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MNLIEUVALOI    PIC X(3).                                  00001490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWRETROL  COMP PIC S9(4).                                 00001500
      *--                                                                       
           02 MWRETROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWRETROF  PIC X.                                          00001510
           02 FILLER    PIC X(4).                                       00001520
           02 MWRETROI  PIC X.                                          00001530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTYPVALOL     COMP PIC S9(4).                            00001540
      *--                                                                       
           02 MWTYPVALOL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MWTYPVALOF     PIC X.                                     00001550
           02 FILLER    PIC X(4).                                       00001560
           02 MWTYPVALOI     PIC X.                                     00001570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFRAISL  COMP PIC S9(4).                                 00001580
      *--                                                                       
           02 MCFRAISL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCFRAISF  PIC X.                                          00001590
           02 FILLER    PIC X(4).                                       00001600
           02 MCFRAISI  PIC X(6).                                       00001610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIERSL   COMP PIC S9(4).                                 00001620
      *--                                                                       
           02 MTIERSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTIERSF   PIC X.                                          00001630
           02 FILLER    PIC X(4).                                       00001640
           02 MTIERSI   PIC X(6).                                       00001650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001660
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001670
           02 FILLER    PIC X(4).                                       00001680
           02 MZONCMDI  PIC X(15).                                      00001690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001700
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001710
           02 FILLER    PIC X(4).                                       00001720
           02 MLIBERRI  PIC X(58).                                      00001730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001740
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001750
           02 FILLER    PIC X(4).                                       00001760
           02 MCODTRAI  PIC X(4).                                       00001770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001780
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001790
           02 FILLER    PIC X(4).                                       00001800
           02 MCICSI    PIC X(5).                                       00001810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001820
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001830
           02 FILLER    PIC X(4).                                       00001840
           02 MNETNAMI  PIC X(8).                                       00001850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001860
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001870
           02 FILLER    PIC X(4).                                       00001880
           02 MSCREENI  PIC X(5).                                       00001890
      ***************************************************************** 00001900
      * SDF: EGA16   EGA16                                              00001910
      ***************************************************************** 00001920
       01   EGA16O REDEFINES EGA16I.                                    00001930
           02 FILLER    PIC X(12).                                      00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MDATJOUA  PIC X.                                          00001960
           02 MDATJOUC  PIC X.                                          00001970
           02 MDATJOUP  PIC X.                                          00001980
           02 MDATJOUH  PIC X.                                          00001990
           02 MDATJOUV  PIC X.                                          00002000
           02 MDATJOUO  PIC X(10).                                      00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MTIMJOUA  PIC X.                                          00002030
           02 MTIMJOUC  PIC X.                                          00002040
           02 MTIMJOUP  PIC X.                                          00002050
           02 MTIMJOUH  PIC X.                                          00002060
           02 MTIMJOUV  PIC X.                                          00002070
           02 MTIMJOUO  PIC X(5).                                       00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MFONCA    PIC X.                                          00002100
           02 MFONCC    PIC X.                                          00002110
           02 MFONCP    PIC X.                                          00002120
           02 MFONCH    PIC X.                                          00002130
           02 MFONCV    PIC X.                                          00002140
           02 MFONCO    PIC X(3).                                       00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MNSOCIETEA     PIC X.                                     00002170
           02 MNSOCIETEC     PIC X.                                     00002180
           02 MNSOCIETEP     PIC X.                                     00002190
           02 MNSOCIETEH     PIC X.                                     00002200
           02 MNSOCIETEV     PIC X.                                     00002210
           02 MNSOCIETEO     PIC X(3).                                  00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MLSOCIETEA     PIC X.                                     00002240
           02 MLSOCIETEC     PIC X.                                     00002250
           02 MLSOCIETEP     PIC X.                                     00002260
           02 MLSOCIETEH     PIC X.                                     00002270
           02 MLSOCIETEV     PIC X.                                     00002280
           02 MLSOCIETEO     PIC X(20).                                 00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MNLIEUA   PIC X.                                          00002310
           02 MNLIEUC   PIC X.                                          00002320
           02 MNLIEUP   PIC X.                                          00002330
           02 MNLIEUH   PIC X.                                          00002340
           02 MNLIEUV   PIC X.                                          00002350
           02 MNLIEUO   PIC X(3).                                       00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MLLIEUA   PIC X.                                          00002380
           02 MLLIEUC   PIC X.                                          00002390
           02 MLLIEUP   PIC X.                                          00002400
           02 MLLIEUH   PIC X.                                          00002410
           02 MLLIEUV   PIC X.                                          00002420
           02 MLLIEUO   PIC X(20).                                      00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MCTYPLA   PIC X.                                          00002450
           02 MCTYPLC   PIC X.                                          00002460
           02 MCTYPLP   PIC X.                                          00002470
           02 MCTYPLH   PIC X.                                          00002480
           02 MCTYPLV   PIC X.                                          00002490
           02 MCTYPLO   PIC X.                                          00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MLCMPAD1A      PIC X.                                     00002520
           02 MLCMPAD1C PIC X.                                          00002530
           02 MLCMPAD1P PIC X.                                          00002540
           02 MLCMPAD1H PIC X.                                          00002550
           02 MLCMPAD1V PIC X.                                          00002560
           02 MLCMPAD1O      PIC X(32).                                 00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MLCMPAD2A      PIC X.                                     00002590
           02 MLCMPAD2C PIC X.                                          00002600
           02 MLCMPAD2P PIC X.                                          00002610
           02 MLCMPAD2H PIC X.                                          00002620
           02 MLCMPAD2V PIC X.                                          00002630
           02 MLCMPAD2O      PIC X(32).                                 00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MLCMPAD3A      PIC X.                                     00002660
           02 MLCMPAD3C PIC X.                                          00002670
           02 MLCMPAD3P PIC X.                                          00002680
           02 MLCMPAD3H PIC X.                                          00002690
           02 MLCMPAD3V PIC X.                                          00002700
           02 MLCMPAD3O      PIC X(32).                                 00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MCPOSTALA      PIC X.                                     00002730
           02 MCPOSTALC PIC X.                                          00002740
           02 MCPOSTALP PIC X.                                          00002750
           02 MCPOSTALH PIC X.                                          00002760
           02 MCPOSTALV PIC X.                                          00002770
           02 MCPOSTALO      PIC X(5).                                  00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MLCOMMUNEA     PIC X.                                     00002800
           02 MLCOMMUNEC     PIC X.                                     00002810
           02 MLCOMMUNEP     PIC X.                                     00002820
           02 MLCOMMUNEH     PIC X.                                     00002830
           02 MLCOMMUNEV     PIC X.                                     00002840
           02 MLCOMMUNEO     PIC X(32).                                 00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MPAYSA    PIC X.                                          00002870
           02 MPAYSC    PIC X.                                          00002880
           02 MPAYSP    PIC X.                                          00002890
           02 MPAYSH    PIC X.                                          00002900
           02 MPAYSV    PIC X.                                          00002910
           02 MPAYSO    PIC X(2).                                       00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MLPAYSA   PIC X.                                          00002940
           02 MLPAYSC   PIC X.                                          00002950
           02 MLPAYSP   PIC X.                                          00002960
           02 MLPAYSH   PIC X.                                          00002970
           02 MLPAYSV   PIC X.                                          00002980
           02 MLPAYSO   PIC X(20).                                      00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MTYPFACTA      PIC X.                                     00003010
           02 MTYPFACTC PIC X.                                          00003020
           02 MTYPFACTP PIC X.                                          00003030
           02 MTYPFACTH PIC X.                                          00003040
           02 MTYPFACTV PIC X.                                          00003050
           02 MTYPFACTO      PIC X(5).                                  00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MNSOCCAA  PIC X.                                          00003080
           02 MNSOCCAC  PIC X.                                          00003090
           02 MNSOCCAP  PIC X.                                          00003100
           02 MNSOCCAH  PIC X.                                          00003110
           02 MNSOCCAV  PIC X.                                          00003120
           02 MNSOCCAO  PIC X(3).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MTYPCONTA      PIC X.                                     00003150
           02 MTYPCONTC PIC X.                                          00003160
           02 MTYPCONTP PIC X.                                          00003170
           02 MTYPCONTH PIC X.                                          00003180
           02 MTYPCONTV PIC X.                                          00003190
           02 MTYPCONTO      PIC X(5).                                  00003200
           02 FILLER    PIC X(2).                                       00003210
           02 MLIBTYPCONA    PIC X.                                     00003220
           02 MLIBTYPCONC    PIC X.                                     00003230
           02 MLIBTYPCONP    PIC X.                                     00003240
           02 MLIBTYPCONH    PIC X.                                     00003250
           02 MLIBTYPCONV    PIC X.                                     00003260
           02 MLIBTYPCONO    PIC X(20).                                 00003270
           02 FILLER    PIC X(2).                                       00003280
           02 MORDRECAA      PIC X.                                     00003290
           02 MORDRECAC PIC X.                                          00003300
           02 MORDRECAP PIC X.                                          00003310
           02 MORDRECAH PIC X.                                          00003320
           02 MORDRECAV PIC X.                                          00003330
           02 MORDRECAO      PIC X(3).                                  00003340
           02 FILLER    PIC X(2).                                       00003350
           02 MTESCPTEA      PIC X.                                     00003360
           02 MTESCPTEC PIC X.                                          00003370
           02 MTESCPTEP PIC X.                                          00003380
           02 MTESCPTEH PIC X.                                          00003390
           02 MTESCPTEV PIC X.                                          00003400
           02 MTESCPTEO      PIC X(5).                                  00003410
           02 FILLER    PIC X(2).                                       00003420
           02 MCONDREGLA     PIC X.                                     00003430
           02 MCONDREGLC     PIC X.                                     00003440
           02 MCONDREGLP     PIC X.                                     00003450
           02 MCONDREGLH     PIC X.                                     00003460
           02 MCONDREGLV     PIC X.                                     00003470
           02 MCONDREGLO     PIC X(20).                                 00003480
           02 FILLER    PIC X(2).                                       00003490
           02 MACID1A   PIC X.                                          00003500
           02 MACID1C   PIC X.                                          00003510
           02 MACID1P   PIC X.                                          00003520
           02 MACID1H   PIC X.                                          00003530
           02 MACID1V   PIC X.                                          00003540
           02 MACID1O   PIC X(4).                                       00003550
           02 FILLER    PIC X(2).                                       00003560
           02 MACID2A   PIC X.                                          00003570
           02 MACID2C   PIC X.                                          00003580
           02 MACID2P   PIC X.                                          00003590
           02 MACID2H   PIC X.                                          00003600
           02 MACID2V   PIC X.                                          00003610
           02 MACID2O   PIC X(4).                                       00003620
           02 FILLER    PIC X(2).                                       00003630
           02 MACID3A   PIC X.                                          00003640
           02 MACID3C   PIC X.                                          00003650
           02 MACID3P   PIC X.                                          00003660
           02 MACID3H   PIC X.                                          00003670
           02 MACID3V   PIC X.                                          00003680
           02 MACID3O   PIC X(4).                                       00003690
           02 FILLER    PIC X(2).                                       00003700
           02 MNSOCFACA      PIC X.                                     00003710
           02 MNSOCFACC PIC X.                                          00003720
           02 MNSOCFACP PIC X.                                          00003730
           02 MNSOCFACH PIC X.                                          00003740
           02 MNSOCFACV PIC X.                                          00003750
           02 MNSOCFACO      PIC X(3).                                  00003760
           02 FILLER    PIC X(2).                                       00003770
           02 MNLIEUFACA     PIC X.                                     00003780
           02 MNLIEUFACC     PIC X.                                     00003790
           02 MNLIEUFACP     PIC X.                                     00003800
           02 MNLIEUFACH     PIC X.                                     00003810
           02 MNLIEUFACV     PIC X.                                     00003820
           02 MNLIEUFACO     PIC X(3).                                  00003830
           02 FILLER    PIC X(2).                                       00003840
           02 MACID4A   PIC X.                                          00003850
           02 MACID4C   PIC X.                                          00003860
           02 MACID4P   PIC X.                                          00003870
           02 MACID4H   PIC X.                                          00003880
           02 MACID4V   PIC X.                                          00003890
           02 MACID4O   PIC X(4).                                       00003900
           02 FILLER    PIC X(2).                                       00003910
           02 MACID5A   PIC X.                                          00003920
           02 MACID5C   PIC X.                                          00003930
           02 MACID5P   PIC X.                                          00003940
           02 MACID5H   PIC X.                                          00003950
           02 MACID5V   PIC X.                                          00003960
           02 MACID5O   PIC X(4).                                       00003970
           02 FILLER    PIC X(2).                                       00003980
           02 MACID6A   PIC X.                                          00003990
           02 MACID6C   PIC X.                                          00004000
           02 MACID6P   PIC X.                                          00004010
           02 MACID6H   PIC X.                                          00004020
           02 MACID6V   PIC X.                                          00004030
           02 MACID6O   PIC X(4).                                       00004040
           02 FILLER    PIC X(2).                                       00004050
           02 MNSOCCOMPTA    PIC X.                                     00004060
           02 MNSOCCOMPTC    PIC X.                                     00004070
           02 MNSOCCOMPTP    PIC X.                                     00004080
           02 MNSOCCOMPTH    PIC X.                                     00004090
           02 MNSOCCOMPTV    PIC X.                                     00004100
           02 MNSOCCOMPTO    PIC X(3).                                  00004110
           02 FILLER    PIC X(2).                                       00004120
           02 MNLIEUCOMPTA   PIC X.                                     00004130
           02 MNLIEUCOMPTC   PIC X.                                     00004140
           02 MNLIEUCOMPTP   PIC X.                                     00004150
           02 MNLIEUCOMPTH   PIC X.                                     00004160
           02 MNLIEUCOMPTV   PIC X.                                     00004170
           02 MNLIEUCOMPTO   PIC X(3).                                  00004180
           02 FILLER    PIC X(2).                                       00004190
           02 MSECTANAA      PIC X.                                     00004200
           02 MSECTANAC PIC X.                                          00004210
           02 MSECTANAP PIC X.                                          00004220
           02 MSECTANAH PIC X.                                          00004230
           02 MSECTANAV PIC X.                                          00004240
           02 MSECTANAO      PIC X(6).                                  00004250
           02 FILLER    PIC X(2).                                       00004260
           02 MCUMCAA   PIC X.                                          00004270
           02 MCUMCAC   PIC X.                                          00004280
           02 MCUMCAP   PIC X.                                          00004290
           02 MCUMCAH   PIC X.                                          00004300
           02 MCUMCAV   PIC X.                                          00004310
           02 MCUMCAO   PIC X.                                          00004320
           02 FILLER    PIC X(2).                                       00004330
           02 MNSOCVALOA     PIC X.                                     00004340
           02 MNSOCVALOC     PIC X.                                     00004350
           02 MNSOCVALOP     PIC X.                                     00004360
           02 MNSOCVALOH     PIC X.                                     00004370
           02 MNSOCVALOV     PIC X.                                     00004380
           02 MNSOCVALOO     PIC X(3).                                  00004390
           02 FILLER    PIC X(2).                                       00004400
           02 MNLIEUVALOA    PIC X.                                     00004410
           02 MNLIEUVALOC    PIC X.                                     00004420
           02 MNLIEUVALOP    PIC X.                                     00004430
           02 MNLIEUVALOH    PIC X.                                     00004440
           02 MNLIEUVALOV    PIC X.                                     00004450
           02 MNLIEUVALOO    PIC X(3).                                  00004460
           02 FILLER    PIC X(2).                                       00004470
           02 MWRETROA  PIC X.                                          00004480
           02 MWRETROC  PIC X.                                          00004490
           02 MWRETROP  PIC X.                                          00004500
           02 MWRETROH  PIC X.                                          00004510
           02 MWRETROV  PIC X.                                          00004520
           02 MWRETROO  PIC X.                                          00004530
           02 FILLER    PIC X(2).                                       00004540
           02 MWTYPVALOA     PIC X.                                     00004550
           02 MWTYPVALOC     PIC X.                                     00004560
           02 MWTYPVALOP     PIC X.                                     00004570
           02 MWTYPVALOH     PIC X.                                     00004580
           02 MWTYPVALOV     PIC X.                                     00004590
           02 MWTYPVALOO     PIC X.                                     00004600
           02 FILLER    PIC X(2).                                       00004610
           02 MCFRAISA  PIC X.                                          00004620
           02 MCFRAISC  PIC X.                                          00004630
           02 MCFRAISP  PIC X.                                          00004640
           02 MCFRAISH  PIC X.                                          00004650
           02 MCFRAISV  PIC X.                                          00004660
           02 MCFRAISO  PIC X(6).                                       00004670
           02 FILLER    PIC X(2).                                       00004680
           02 MTIERSA   PIC X.                                          00004690
           02 MTIERSC   PIC X.                                          00004700
           02 MTIERSP   PIC X.                                          00004710
           02 MTIERSH   PIC X.                                          00004720
           02 MTIERSV   PIC X.                                          00004730
           02 MTIERSO   PIC X(6).                                       00004740
           02 FILLER    PIC X(2).                                       00004750
           02 MZONCMDA  PIC X.                                          00004760
           02 MZONCMDC  PIC X.                                          00004770
           02 MZONCMDP  PIC X.                                          00004780
           02 MZONCMDH  PIC X.                                          00004790
           02 MZONCMDV  PIC X.                                          00004800
           02 MZONCMDO  PIC X(15).                                      00004810
           02 FILLER    PIC X(2).                                       00004820
           02 MLIBERRA  PIC X.                                          00004830
           02 MLIBERRC  PIC X.                                          00004840
           02 MLIBERRP  PIC X.                                          00004850
           02 MLIBERRH  PIC X.                                          00004860
           02 MLIBERRV  PIC X.                                          00004870
           02 MLIBERRO  PIC X(58).                                      00004880
           02 FILLER    PIC X(2).                                       00004890
           02 MCODTRAA  PIC X.                                          00004900
           02 MCODTRAC  PIC X.                                          00004910
           02 MCODTRAP  PIC X.                                          00004920
           02 MCODTRAH  PIC X.                                          00004930
           02 MCODTRAV  PIC X.                                          00004940
           02 MCODTRAO  PIC X(4).                                       00004950
           02 FILLER    PIC X(2).                                       00004960
           02 MCICSA    PIC X.                                          00004970
           02 MCICSC    PIC X.                                          00004980
           02 MCICSP    PIC X.                                          00004990
           02 MCICSH    PIC X.                                          00005000
           02 MCICSV    PIC X.                                          00005010
           02 MCICSO    PIC X(5).                                       00005020
           02 FILLER    PIC X(2).                                       00005030
           02 MNETNAMA  PIC X.                                          00005040
           02 MNETNAMC  PIC X.                                          00005050
           02 MNETNAMP  PIC X.                                          00005060
           02 MNETNAMH  PIC X.                                          00005070
           02 MNETNAMV  PIC X.                                          00005080
           02 MNETNAMO  PIC X(8).                                       00005090
           02 FILLER    PIC X(2).                                       00005100
           02 MSCREENA  PIC X.                                          00005110
           02 MSCREENC  PIC X.                                          00005120
           02 MSCREENP  PIC X.                                          00005130
           02 MSCREENH  PIC X.                                          00005140
           02 MSCREENV  PIC X.                                          00005150
           02 MSCREENO  PIC X(5).                                       00005160
                                                                                
