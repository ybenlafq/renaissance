      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IHA040 AU 28/01/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,PD,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,07,BI,A,                          *        
      *                           22,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IHA040.                                                        
            05 NOMETAT-IHA040           PIC X(6) VALUE 'IHA040'.                
            05 RUPTURES-IHA040.                                                 
           10 IHA040-WSEQFAM            PIC S9(05)      COMP-3.         007  003
           10 IHA040-CMARQ              PIC X(05).                      010  005
           10 IHA040-NCODIC             PIC X(07).                      015  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IHA040-SEQUENCE           PIC S9(04) COMP.                022  002
      *--                                                                       
           10 IHA040-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IHA040.                                                   
           10 IHA040-CFAM               PIC X(05).                      024  005
           10 IHA040-DETZP1             PIC X(22).                      029  022
           10 IHA040-DETZP2             PIC X(22).                      051  022
           10 IHA040-DETZP3             PIC X(22).                      073  022
           10 IHA040-LREFFOURN          PIC X(20).                      095  020
           10 IHA040-NSOCIETE           PIC X(03).                      115  003
           10 IHA040-DACEMACTUEL        PIC 9(05)V9(2).                 118  007
           10 IHA040-PVANCIEN           PIC 9(05)V9(2).                 125  007
           10 IHA040-SEMAINEAU          PIC X(08).                      132  008
           10 IHA040-SEMAINEDU          PIC X(08).                      140  008
            05 FILLER                      PIC X(365).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IHA040-LONG           PIC S9(4)   COMP  VALUE +147.           
      *                                                                         
      *--                                                                       
        01  DSECT-IHA040-LONG           PIC S9(4) COMP-5  VALUE +147.           
                                                                                
      *}                                                                        
