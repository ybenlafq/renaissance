      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ******************************************************************00000010
      * TS MENU GENERAL NCG+                                            00000020
      ******************************************************************00000010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS-MENU-LONG PIC S9(4) COMP VALUE +100.                      00000050
      *--                                                                       
       01  TS-MENU-LONG PIC S9(4) COMP-5 VALUE +100.                            
      *}                                                                        
       01  TS-MENU.                                                     00000070
             03 TS-MENU-NIVEAU     PIC  9.                              00000303
             03 TS-MENU-NIV1       PIC XX.                              00000303
             03 TS-MENU-NIV2       PIC XX.                              00000303
                                                                                
