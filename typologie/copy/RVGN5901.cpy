      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *                                                                 00050000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGN5901.                                                    00060000
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGN5901.                                                            
      *}                                                                        
           05  GN59-NCODIC     PIC X(7).                                00070000
           05  GN59-CREF       PIC X(1).                                00080000
           05  GN59-DEFFET     PIC X(8).                                00090000
           05  GN59-PREFTTC    PIC S9(5)V99 COMP-3.                     00100000
           05  GN59-WIMPERATIF PIC X(1).                                00110000
           05  GN59-CIMPER     PIC X(1).                                00120000
           05  GN59-WBLOQUE    PIC X(1).                                00130000
           05  GN59-LCOMMENT   PIC X(10).                               00140000
           05  GN59-DSYST      PIC S9(13) COMP-3.                       00150000
           05  GN59-USERID     PIC X(8).                                00160000
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00180000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGN5901-FLAGS.                                              00190000
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGN5901-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN59-NCODIC-F     PIC S9(4) COMP.                        00200000
      *--                                                                       
           05  GN59-NCODIC-F     PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN59-CREF-F       PIC S9(4) COMP.                        00220000
      *--                                                                       
           05  GN59-CREF-F       PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN59-DEFFET-F     PIC S9(4) COMP.                        00222000
      *--                                                                       
           05  GN59-DEFFET-F     PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN59-PREFTTC-F    PIC S9(4) COMP.                        00224000
      *--                                                                       
           05  GN59-PREFTTC-F    PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN59-WIMPERATIF-F PIC S9(4) COMP.                        00226000
      *--                                                                       
           05  GN59-WIMPERATIF-F PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN59-CIMPER-F     PIC S9(4) COMP.                        00228000
      *--                                                                       
           05  GN59-CIMPER-F     PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN59-WBLOQUE-F    PIC S9(4) COMP.                        00230000
      *--                                                                       
           05  GN59-WBLOQUE-F    PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN59-LCOMMENT-F   PIC S9(4) COMP.                        00250000
      *--                                                                       
           05  GN59-LCOMMENT-F   PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN59-DSYST-F      PIC S9(4) COMP.                        00270000
      *--                                                                       
           05  GN59-DSYST-F      PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN59-USERID-F     PIC S9(4) COMP.                        00280000
      *                                                                         
      *--                                                                       
           05  GN59-USERID-F     PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
