      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA59   EGA59                                              00000020
      ***************************************************************** 00000030
       01   EGA59I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCTL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MFONCTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MFONCTF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MFONCTI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCIETEI     PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCIETEL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MLSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLSOCIETEF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLSOCIETEI     PIC X(20).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODICL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCODICL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCODICF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCODICI   PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLREFFOURNI    PIC X(20).                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCASSORTL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MCASSORTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCASSORTF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCASSORTI      PIC X(5).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLASSORTL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLASSORTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLASSORTF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLASSORTI      PIC X(20).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCFAMI    PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLFAMI    PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPROL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAPPROF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCAPPROI  PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAPPROL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLAPPROF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLAPPROI  PIC X(20).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCMARQI   PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLMARQI   PIC X(20).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPOL   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXPOF   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCEXPOI   PIC X(5).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLEXPOL   COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MLEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLEXPOF   PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLEXPOI   PIC X(20).                                      00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPBFL     COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MPBFL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MPBFF     PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MPBFI     PIC X(10).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRARL    COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MPRARL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPRARF    PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MPRARI    PIC X(10).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBDACL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MLIBDACL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBDACF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLIBDACI  PIC X(13).                                      00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRIDACL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MPRIDACL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPRIDACF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MPRIDACI  PIC X(9).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MPAGEI    PIC X(2).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETOTL      COMP PIC S9(4).                            00000940
      *--                                                                       
           02 MPAGETOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGETOTF      PIC X.                                     00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MPAGETOTI      PIC X(2).                                  00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MASTERL   COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MASTERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MASTERF   PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MASTERI   PIC X.                                          00001010
           02 MLIGNESI OCCURS   10 TIMES .                              00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNZONPRIXL   COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MNZONPRIXL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNZONPRIXF   PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MNZONPRIXI   PIC X(2).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPSTDTTCL    COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MPSTDTTCL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MPSTDTTCF    PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MPSTDTTCI    PIC X(9).                                  00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDIVTEL      COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MDIVTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDIVTEF      PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MDIVTEI      PIC X(8).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOMMENTL   COMP PIC S9(4).                            00001150
      *--                                                                       
             03 MLCOMMENTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLCOMMENTF   PIC X.                                     00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MLCOMMENTI   PIC X(10).                                 00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPCOMML      COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MPCOMML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPCOMMF      PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MPCOMMI      PIC X(6).                                  00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDICOMML     COMP PIC S9(4).                            00001230
      *--                                                                       
             03 MDICOMML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDICOMMF     PIC X.                                     00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MDICOMMI     PIC X(8).                                  00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MZONCMDI  PIC X(15).                                      00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MLIBERRI  PIC X(58).                                      00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MCODTRAI  PIC X(4).                                       00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MCICSI    PIC X(5).                                       00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MNETNAMI  PIC X(8).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MSCREENI  PIC X(4).                                       00001500
      ***************************************************************** 00001510
      * SDF: EGA59   EGA59                                              00001520
      ***************************************************************** 00001530
       01   EGA59O REDEFINES EGA59I.                                    00001540
           02 FILLER    PIC X(12).                                      00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MDATJOUA  PIC X.                                          00001570
           02 MDATJOUC  PIC X.                                          00001580
           02 MDATJOUP  PIC X.                                          00001590
           02 MDATJOUH  PIC X.                                          00001600
           02 MDATJOUV  PIC X.                                          00001610
           02 MDATJOUO  PIC X(10).                                      00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MTIMJOUA  PIC X.                                          00001640
           02 MTIMJOUC  PIC X.                                          00001650
           02 MTIMJOUP  PIC X.                                          00001660
           02 MTIMJOUH  PIC X.                                          00001670
           02 MTIMJOUV  PIC X.                                          00001680
           02 MTIMJOUO  PIC X(5).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MFONCTA   PIC X.                                          00001710
           02 MFONCTC   PIC X.                                          00001720
           02 MFONCTP   PIC X.                                          00001730
           02 MFONCTH   PIC X.                                          00001740
           02 MFONCTV   PIC X.                                          00001750
           02 MFONCTO   PIC X(3).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MNSOCIETEA     PIC X.                                     00001780
           02 MNSOCIETEC     PIC X.                                     00001790
           02 MNSOCIETEP     PIC X.                                     00001800
           02 MNSOCIETEH     PIC X.                                     00001810
           02 MNSOCIETEV     PIC X.                                     00001820
           02 MNSOCIETEO     PIC X(3).                                  00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MLSOCIETEA     PIC X.                                     00001850
           02 MLSOCIETEC     PIC X.                                     00001860
           02 MLSOCIETEP     PIC X.                                     00001870
           02 MLSOCIETEH     PIC X.                                     00001880
           02 MLSOCIETEV     PIC X.                                     00001890
           02 MLSOCIETEO     PIC X(20).                                 00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MCODICA   PIC X.                                          00001920
           02 MCODICC   PIC X.                                          00001930
           02 MCODICP   PIC X.                                          00001940
           02 MCODICH   PIC X.                                          00001950
           02 MCODICV   PIC X.                                          00001960
           02 MCODICO   PIC X(7).                                       00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MLREFFOURNA    PIC X.                                     00001990
           02 MLREFFOURNC    PIC X.                                     00002000
           02 MLREFFOURNP    PIC X.                                     00002010
           02 MLREFFOURNH    PIC X.                                     00002020
           02 MLREFFOURNV    PIC X.                                     00002030
           02 MLREFFOURNO    PIC X(20).                                 00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCASSORTA      PIC X.                                     00002060
           02 MCASSORTC PIC X.                                          00002070
           02 MCASSORTP PIC X.                                          00002080
           02 MCASSORTH PIC X.                                          00002090
           02 MCASSORTV PIC X.                                          00002100
           02 MCASSORTO      PIC X(5).                                  00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MLASSORTA      PIC X.                                     00002130
           02 MLASSORTC PIC X.                                          00002140
           02 MLASSORTP PIC X.                                          00002150
           02 MLASSORTH PIC X.                                          00002160
           02 MLASSORTV PIC X.                                          00002170
           02 MLASSORTO      PIC X(20).                                 00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MCFAMA    PIC X.                                          00002200
           02 MCFAMC    PIC X.                                          00002210
           02 MCFAMP    PIC X.                                          00002220
           02 MCFAMH    PIC X.                                          00002230
           02 MCFAMV    PIC X.                                          00002240
           02 MCFAMO    PIC X(5).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MLFAMA    PIC X.                                          00002270
           02 MLFAMC    PIC X.                                          00002280
           02 MLFAMP    PIC X.                                          00002290
           02 MLFAMH    PIC X.                                          00002300
           02 MLFAMV    PIC X.                                          00002310
           02 MLFAMO    PIC X(20).                                      00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MCAPPROA  PIC X.                                          00002340
           02 MCAPPROC  PIC X.                                          00002350
           02 MCAPPROP  PIC X.                                          00002360
           02 MCAPPROH  PIC X.                                          00002370
           02 MCAPPROV  PIC X.                                          00002380
           02 MCAPPROO  PIC X(5).                                       00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MLAPPROA  PIC X.                                          00002410
           02 MLAPPROC  PIC X.                                          00002420
           02 MLAPPROP  PIC X.                                          00002430
           02 MLAPPROH  PIC X.                                          00002440
           02 MLAPPROV  PIC X.                                          00002450
           02 MLAPPROO  PIC X(20).                                      00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MCMARQA   PIC X.                                          00002480
           02 MCMARQC   PIC X.                                          00002490
           02 MCMARQP   PIC X.                                          00002500
           02 MCMARQH   PIC X.                                          00002510
           02 MCMARQV   PIC X.                                          00002520
           02 MCMARQO   PIC X(5).                                       00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MLMARQA   PIC X.                                          00002550
           02 MLMARQC   PIC X.                                          00002560
           02 MLMARQP   PIC X.                                          00002570
           02 MLMARQH   PIC X.                                          00002580
           02 MLMARQV   PIC X.                                          00002590
           02 MLMARQO   PIC X(20).                                      00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MCEXPOA   PIC X.                                          00002620
           02 MCEXPOC   PIC X.                                          00002630
           02 MCEXPOP   PIC X.                                          00002640
           02 MCEXPOH   PIC X.                                          00002650
           02 MCEXPOV   PIC X.                                          00002660
           02 MCEXPOO   PIC X(5).                                       00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MLEXPOA   PIC X.                                          00002690
           02 MLEXPOC   PIC X.                                          00002700
           02 MLEXPOP   PIC X.                                          00002710
           02 MLEXPOH   PIC X.                                          00002720
           02 MLEXPOV   PIC X.                                          00002730
           02 MLEXPOO   PIC X(20).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MPBFA     PIC X.                                          00002760
           02 MPBFC     PIC X.                                          00002770
           02 MPBFP     PIC X.                                          00002780
           02 MPBFH     PIC X.                                          00002790
           02 MPBFV     PIC X.                                          00002800
           02 MPBFO     PIC Z(6)9,99.                                   00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MPRARA    PIC X.                                          00002830
           02 MPRARC    PIC X.                                          00002840
           02 MPRARP    PIC X.                                          00002850
           02 MPRARH    PIC X.                                          00002860
           02 MPRARV    PIC X.                                          00002870
           02 MPRARO    PIC Z(6)9,99.                                   00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MLIBDACA  PIC X.                                          00002900
           02 MLIBDACC  PIC X.                                          00002910
           02 MLIBDACP  PIC X.                                          00002920
           02 MLIBDACH  PIC X.                                          00002930
           02 MLIBDACV  PIC X.                                          00002940
           02 MLIBDACO  PIC X(13).                                      00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MPRIDACA  PIC X.                                          00002970
           02 MPRIDACC  PIC X.                                          00002980
           02 MPRIDACP  PIC X.                                          00002990
           02 MPRIDACH  PIC X.                                          00003000
           02 MPRIDACV  PIC X.                                          00003010
           02 MPRIDACO  PIC Z(5)9,99.                                   00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MPAGEA    PIC X.                                          00003040
           02 MPAGEC    PIC X.                                          00003050
           02 MPAGEP    PIC X.                                          00003060
           02 MPAGEH    PIC X.                                          00003070
           02 MPAGEV    PIC X.                                          00003080
           02 MPAGEO    PIC ZZ.                                         00003090
           02 FILLER    PIC X(2).                                       00003100
           02 MPAGETOTA      PIC X.                                     00003110
           02 MPAGETOTC PIC X.                                          00003120
           02 MPAGETOTP PIC X.                                          00003130
           02 MPAGETOTH PIC X.                                          00003140
           02 MPAGETOTV PIC X.                                          00003150
           02 MPAGETOTO      PIC ZZ.                                    00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MASTERA   PIC X.                                          00003180
           02 MASTERC   PIC X.                                          00003190
           02 MASTERP   PIC X.                                          00003200
           02 MASTERH   PIC X.                                          00003210
           02 MASTERV   PIC X.                                          00003220
           02 MASTERO   PIC X.                                          00003230
           02 MLIGNESO OCCURS   10 TIMES .                              00003240
             03 FILLER       PIC X(2).                                  00003250
             03 MNZONPRIXA   PIC X.                                     00003260
             03 MNZONPRIXC   PIC X.                                     00003270
             03 MNZONPRIXP   PIC X.                                     00003280
             03 MNZONPRIXH   PIC X.                                     00003290
             03 MNZONPRIXV   PIC X.                                     00003300
             03 MNZONPRIXO   PIC X(2).                                  00003310
             03 FILLER       PIC X(2).                                  00003320
             03 MPSTDTTCA    PIC X.                                     00003330
             03 MPSTDTTCC    PIC X.                                     00003340
             03 MPSTDTTCP    PIC X.                                     00003350
             03 MPSTDTTCH    PIC X.                                     00003360
             03 MPSTDTTCV    PIC X.                                     00003370
             03 MPSTDTTCO    PIC Z(5)9,99.                              00003380
             03 FILLER       PIC X(2).                                  00003390
             03 MDIVTEA      PIC X.                                     00003400
             03 MDIVTEC PIC X.                                          00003410
             03 MDIVTEP PIC X.                                          00003420
             03 MDIVTEH PIC X.                                          00003430
             03 MDIVTEV PIC X.                                          00003440
             03 MDIVTEO      PIC X(8).                                  00003450
             03 FILLER       PIC X(2).                                  00003460
             03 MLCOMMENTA   PIC X.                                     00003470
             03 MLCOMMENTC   PIC X.                                     00003480
             03 MLCOMMENTP   PIC X.                                     00003490
             03 MLCOMMENTH   PIC X.                                     00003500
             03 MLCOMMENTV   PIC X.                                     00003510
             03 MLCOMMENTO   PIC X(10).                                 00003520
             03 FILLER       PIC X(2).                                  00003530
             03 MPCOMMA      PIC X.                                     00003540
             03 MPCOMMC PIC X.                                          00003550
             03 MPCOMMP PIC X.                                          00003560
             03 MPCOMMH PIC X.                                          00003570
             03 MPCOMMV PIC X.                                          00003580
             03 MPCOMMO      PIC ZZZ9,9.                                00003590
             03 FILLER       PIC X(2).                                  00003600
             03 MDICOMMA     PIC X.                                     00003610
             03 MDICOMMC     PIC X.                                     00003620
             03 MDICOMMP     PIC X.                                     00003630
             03 MDICOMMH     PIC X.                                     00003640
             03 MDICOMMV     PIC X.                                     00003650
             03 MDICOMMO     PIC X(8).                                  00003660
           02 FILLER    PIC X(2).                                       00003670
           02 MZONCMDA  PIC X.                                          00003680
           02 MZONCMDC  PIC X.                                          00003690
           02 MZONCMDP  PIC X.                                          00003700
           02 MZONCMDH  PIC X.                                          00003710
           02 MZONCMDV  PIC X.                                          00003720
           02 MZONCMDO  PIC X(15).                                      00003730
           02 FILLER    PIC X(2).                                       00003740
           02 MLIBERRA  PIC X.                                          00003750
           02 MLIBERRC  PIC X.                                          00003760
           02 MLIBERRP  PIC X.                                          00003770
           02 MLIBERRH  PIC X.                                          00003780
           02 MLIBERRV  PIC X.                                          00003790
           02 MLIBERRO  PIC X(58).                                      00003800
           02 FILLER    PIC X(2).                                       00003810
           02 MCODTRAA  PIC X.                                          00003820
           02 MCODTRAC  PIC X.                                          00003830
           02 MCODTRAP  PIC X.                                          00003840
           02 MCODTRAH  PIC X.                                          00003850
           02 MCODTRAV  PIC X.                                          00003860
           02 MCODTRAO  PIC X(4).                                       00003870
           02 FILLER    PIC X(2).                                       00003880
           02 MCICSA    PIC X.                                          00003890
           02 MCICSC    PIC X.                                          00003900
           02 MCICSP    PIC X.                                          00003910
           02 MCICSH    PIC X.                                          00003920
           02 MCICSV    PIC X.                                          00003930
           02 MCICSO    PIC X(5).                                       00003940
           02 FILLER    PIC X(2).                                       00003950
           02 MNETNAMA  PIC X.                                          00003960
           02 MNETNAMC  PIC X.                                          00003970
           02 MNETNAMP  PIC X.                                          00003980
           02 MNETNAMH  PIC X.                                          00003990
           02 MNETNAMV  PIC X.                                          00004000
           02 MNETNAMO  PIC X(8).                                       00004010
           02 FILLER    PIC X(2).                                       00004020
           02 MSCREENA  PIC X.                                          00004030
           02 MSCREENC  PIC X.                                          00004040
           02 MSCREENP  PIC X.                                          00004050
           02 MSCREENH  PIC X.                                          00004060
           02 MSCREENV  PIC X.                                          00004070
           02 MSCREENO  PIC X(4).                                       00004080
                                                                                
