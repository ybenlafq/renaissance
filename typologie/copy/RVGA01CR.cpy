      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE ENTST ASSOCIATION SOCIETE ENTITE       *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01CR.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01CR.                                                            
      *}                                                                        
           05  ENTST-CTABLEG2    PIC X(15).                                     
           05  ENTST-CTABLEG2-REDEF REDEFINES ENTST-CTABLEG2.                   
               10  ENTST-NSOCGL          PIC X(05).                             
           05  ENTST-WTABLEG     PIC X(80).                                     
           05  ENTST-WTABLEG-REDEF  REDEFINES ENTST-WTABLEG.                    
               10  ENTST-NENTITE         PIC X(05).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01CR-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01CR-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ENTST-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  ENTST-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ENTST-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  ENTST-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
