      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE AUTOG SOCIETE GROUPE                   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01ST.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01ST.                                                            
      *}                                                                        
           05  AUTOG-CTABLEG2    PIC X(15).                                     
           05  AUTOG-CTABLEG2-REDEF REDEFINES AUTOG-CTABLEG2.                   
               10  AUTOG-NSOCGROU        PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  AUTOG-NSOCGROU-N     REDEFINES AUTOG-NSOCGROU                
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  AUTOG-WTABLEG     PIC X(80).                                     
           05  AUTOG-WTABLEG-REDEF  REDEFINES AUTOG-WTABLEG.                    
               10  AUTOG-WACTIF          PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01ST-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01ST-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AUTOG-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  AUTOG-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AUTOG-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  AUTOG-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
