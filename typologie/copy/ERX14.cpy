      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * RELEVE PRIX : PARAM. FREQUENCES                                 00000020
      ***************************************************************** 00000030
       01   ERX14I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(2).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(2).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000170
           02 FILLER    PIC X(2).                                       00000180
           02 MNPAGEI   PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(2).                                       00000220
           02 MNBPI     PIC X(2).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000250
           02 FILLER    PIC X(2).                                       00000260
           02 MWFONCI   PIC X(3).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPFREQL      COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MTYPFREQL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPFREQF      PIC X.                                     00000290
           02 FILLER    PIC X(2).                                       00000300
           02 MTYPFREQI      PIC X.                                     00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000330
           02 FILLER    PIC X(2).                                       00000340
           02 MNSOCI    PIC X(3).                                       00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMSEML  COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MNUMSEML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNUMSEMF  PIC X.                                          00000370
           02 FILLER    PIC X(2).                                       00000380
           02 MNUMSEMI  PIC X(6).                                       00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGAL   COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MNMAGAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNMAGAF   PIC X.                                          00000410
           02 FILLER    PIC X(2).                                       00000420
           02 MNMAGAI   PIC X(3).                                       00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBZPL   COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MLIBZPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIBZPF   PIC X.                                          00000450
           02 FILLER    PIC X(2).                                       00000460
           02 MLIBZPI   PIC X(15).                                      00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONPRIXL      COMP PIC S9(4).                            00000480
      *--                                                                       
           02 MZONPRIXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MZONPRIXF      PIC X.                                     00000490
           02 FILLER    PIC X(2).                                       00000500
           02 MZONPRIXI      PIC X(2).                                  00000510
           02 MTABI OCCURS   13 TIMES .                                 00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCONCL      COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MNCONCL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNCONCF      PIC X.                                     00000540
             03 FILLER  PIC X(2).                                       00000550
             03 MNCONCI      PIC X(4).                                  00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLENSCOL     COMP PIC S9(4).                            00000570
      *--                                                                       
             03 MLENSCOL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLENSCOF     PIC X.                                     00000580
             03 FILLER  PIC X(2).                                       00000590
             03 MLENSCOI     PIC X(15).                                 00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLEMPCOL     COMP PIC S9(4).                            00000610
      *--                                                                       
             03 MLEMPCOL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLEMPCOF     PIC X.                                     00000620
             03 FILLER  PIC X(2).                                       00000630
             03 MLEMPCOI     PIC X(14).                                 00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSEQL   COMP PIC S9(4).                                 00000650
      *--                                                                       
             03 MSEQL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSEQF   PIC X.                                          00000660
             03 FILLER  PIC X(2).                                       00000670
             03 MSEQI   PIC X.                                          00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCRAYONL     COMP PIC S9(4).                            00000690
      *--                                                                       
             03 MCRAYONL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCRAYONF     PIC X.                                     00000700
             03 FILLER  PIC X(2).                                       00000710
             03 MCRAYONI     PIC X(5).                                  00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMAGL  COMP PIC S9(4).                                 00000730
      *--                                                                       
             03 MNMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMAGF  PIC X.                                          00000740
             03 FILLER  PIC X(2).                                       00000750
             03 MNMAGI  PIC X(3).                                       00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMAGL  COMP PIC S9(4).                                 00000770
      *--                                                                       
             03 MLMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLMAGF  PIC X.                                          00000780
             03 FILLER  PIC X(2).                                       00000790
             03 MLMAGI  PIC X(10).                                      00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNFREQL      COMP PIC S9(4).                            00000810
      *--                                                                       
             03 MNFREQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNFREQF      PIC X.                                     00000820
             03 FILLER  PIC X(2).                                       00000830
             03 MNFREQI      PIC X.                                     00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MJOURL  COMP PIC S9(4).                                 00000850
      *--                                                                       
             03 MJOURL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MJOURF  PIC X.                                          00000860
             03 FILLER  PIC X(2).                                       00000870
             03 MJOURI  PIC X(3).                                       00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEL  COMP PIC S9(4).                                 00000890
      *--                                                                       
             03 MDATEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDATEF  PIC X.                                          00000900
             03 FILLER  PIC X(2).                                       00000910
             03 MDATEI  PIC X(10).                                      00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTIFL      COMP PIC S9(4).                            00000930
      *--                                                                       
             03 MACTIFL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MACTIFF      PIC X.                                     00000940
             03 FILLER  PIC X(2).                                       00000950
             03 MACTIFI      PIC X.                                     00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MEDITL  COMP PIC S9(4).                                 00000970
      *--                                                                       
             03 MEDITL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MEDITF  PIC X.                                          00000980
             03 FILLER  PIC X(2).                                       00000990
             03 MEDITI  PIC X.                                          00001000
      * ZONE CMD AIDA                                                   00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MLIBERRI  PIC X(79).                                      00001050
      * CODE TRANSACTION                                                00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MCODTRAI  PIC X(4).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MZONCMDI  PIC X(15).                                      00001140
      * CICS DE TRAVAIL                                                 00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001160
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MCICSI    PIC X(5).                                       00001190
      * NETNAME                                                         00001200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001210
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MNETNAMI  PIC X(8).                                       00001240
      * CODE TERMINAL                                                   00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MSCREENI  PIC X(4).                                       00001290
      ***************************************************************** 00001300
      * RELEVE PRIX : PARAM. FREQUENCES                                 00001310
      ***************************************************************** 00001320
       01   ERX14O REDEFINES ERX14I.                                    00001330
           02 FILLER    PIC X(12).                                      00001340
      * DATE DU JOUR                                                    00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MDATJOUA  PIC X.                                          00001370
           02 MDATJOUC  PIC X.                                          00001380
           02 MDATJOUH  PIC X.                                          00001390
           02 MDATJOUO  PIC X(10).                                      00001400
      * HEURE                                                           00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MTIMJOUA  PIC X.                                          00001430
           02 MTIMJOUC  PIC X.                                          00001440
           02 MTIMJOUH  PIC X.                                          00001450
           02 MTIMJOUO  PIC X(5).                                       00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MNPAGEA   PIC X.                                          00001480
           02 MNPAGEC   PIC X.                                          00001490
           02 MNPAGEH   PIC X.                                          00001500
           02 MNPAGEO   PIC ZZ.                                         00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MNBPA     PIC X.                                          00001530
           02 MNBPC     PIC X.                                          00001540
           02 MNBPH     PIC X.                                          00001550
           02 MNBPO     PIC ZZ.                                         00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MWFONCA   PIC X.                                          00001580
           02 MWFONCC   PIC X.                                          00001590
           02 MWFONCH   PIC X.                                          00001600
           02 MWFONCO   PIC X(3).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MTYPFREQA      PIC X.                                     00001630
           02 MTYPFREQC PIC X.                                          00001640
           02 MTYPFREQH PIC X.                                          00001650
           02 MTYPFREQO      PIC X.                                     00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MNSOCA    PIC X.                                          00001680
           02 MNSOCC    PIC X.                                          00001690
           02 MNSOCH    PIC X.                                          00001700
           02 MNSOCO    PIC X(3).                                       00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MNUMSEMA  PIC X.                                          00001730
           02 MNUMSEMC  PIC X.                                          00001740
           02 MNUMSEMH  PIC X.                                          00001750
           02 MNUMSEMO  PIC X(6).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MNMAGAA   PIC X.                                          00001780
           02 MNMAGAC   PIC X.                                          00001790
           02 MNMAGAH   PIC X.                                          00001800
           02 MNMAGAO   PIC X(3).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MLIBZPA   PIC X.                                          00001830
           02 MLIBZPC   PIC X.                                          00001840
           02 MLIBZPH   PIC X.                                          00001850
           02 MLIBZPO   PIC X(15).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MZONPRIXA      PIC X.                                     00001880
           02 MZONPRIXC PIC X.                                          00001890
           02 MZONPRIXH PIC X.                                          00001900
           02 MZONPRIXO      PIC X(2).                                  00001910
           02 MTABO OCCURS   13 TIMES .                                 00001920
             03 FILLER       PIC X(2).                                  00001930
             03 MNCONCA      PIC X.                                     00001940
             03 MNCONCC PIC X.                                          00001950
             03 MNCONCH PIC X.                                          00001960
             03 MNCONCO      PIC X(4).                                  00001970
             03 FILLER       PIC X(2).                                  00001980
             03 MLENSCOA     PIC X.                                     00001990
             03 MLENSCOC     PIC X.                                     00002000
             03 MLENSCOH     PIC X.                                     00002010
             03 MLENSCOO     PIC X(15).                                 00002020
             03 FILLER       PIC X(2).                                  00002030
             03 MLEMPCOA     PIC X.                                     00002040
             03 MLEMPCOC     PIC X.                                     00002050
             03 MLEMPCOH     PIC X.                                     00002060
             03 MLEMPCOO     PIC X(14).                                 00002070
             03 FILLER       PIC X(2).                                  00002080
             03 MSEQA   PIC X.                                          00002090
             03 MSEQC   PIC X.                                          00002100
             03 MSEQH   PIC X.                                          00002110
             03 MSEQO   PIC X.                                          00002120
             03 FILLER       PIC X(2).                                  00002130
             03 MCRAYONA     PIC X.                                     00002140
             03 MCRAYONC     PIC X.                                     00002150
             03 MCRAYONH     PIC X.                                     00002160
             03 MCRAYONO     PIC X(5).                                  00002170
             03 FILLER       PIC X(2).                                  00002180
             03 MNMAGA  PIC X.                                          00002190
             03 MNMAGC  PIC X.                                          00002200
             03 MNMAGH  PIC X.                                          00002210
             03 MNMAGO  PIC X(3).                                       00002220
             03 FILLER       PIC X(2).                                  00002230
             03 MLMAGA  PIC X.                                          00002240
             03 MLMAGC  PIC X.                                          00002250
             03 MLMAGH  PIC X.                                          00002260
             03 MLMAGO  PIC X(10).                                      00002270
             03 FILLER       PIC X(2).                                  00002280
             03 MNFREQA      PIC X.                                     00002290
             03 MNFREQC PIC X.                                          00002300
             03 MNFREQH PIC X.                                          00002310
             03 MNFREQO      PIC X.                                     00002320
             03 FILLER       PIC X(2).                                  00002330
             03 MJOURA  PIC X.                                          00002340
             03 MJOURC  PIC X.                                          00002350
             03 MJOURH  PIC X.                                          00002360
             03 MJOURO  PIC X(3).                                       00002370
             03 FILLER       PIC X(2).                                  00002380
             03 MDATEA  PIC X.                                          00002390
             03 MDATEC  PIC X.                                          00002400
             03 MDATEH  PIC X.                                          00002410
             03 MDATEO  PIC X(10).                                      00002420
             03 FILLER       PIC X(2).                                  00002430
             03 MACTIFA      PIC X.                                     00002440
             03 MACTIFC PIC X.                                          00002450
             03 MACTIFH PIC X.                                          00002460
             03 MACTIFO      PIC X.                                     00002470
             03 FILLER       PIC X(2).                                  00002480
             03 MEDITA  PIC X.                                          00002490
             03 MEDITC  PIC X.                                          00002500
             03 MEDITH  PIC X.                                          00002510
             03 MEDITO  PIC X.                                          00002520
      * ZONE CMD AIDA                                                   00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MLIBERRA  PIC X.                                          00002550
           02 MLIBERRC  PIC X.                                          00002560
           02 MLIBERRH  PIC X.                                          00002570
           02 MLIBERRO  PIC X(79).                                      00002580
      * CODE TRANSACTION                                                00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MCODTRAA  PIC X.                                          00002610
           02 MCODTRAC  PIC X.                                          00002620
           02 MCODTRAH  PIC X.                                          00002630
           02 MCODTRAO  PIC X(4).                                       00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MZONCMDA  PIC X.                                          00002660
           02 MZONCMDC  PIC X.                                          00002670
           02 MZONCMDH  PIC X.                                          00002680
           02 MZONCMDO  PIC X(15).                                      00002690
      * CICS DE TRAVAIL                                                 00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MCICSA    PIC X.                                          00002720
           02 MCICSC    PIC X.                                          00002730
           02 MCICSH    PIC X.                                          00002740
           02 MCICSO    PIC X(5).                                       00002750
      * NETNAME                                                         00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MNETNAMA  PIC X.                                          00002780
           02 MNETNAMC  PIC X.                                          00002790
           02 MNETNAMH  PIC X.                                          00002800
           02 MNETNAMO  PIC X(8).                                       00002810
      * CODE TERMINAL                                                   00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MSCREENA  PIC X.                                          00002840
           02 MSCREENC  PIC X.                                          00002850
           02 MSCREENH  PIC X.                                          00002860
           02 MSCREENO  PIC X(4).                                       00002870
                                                                                
