      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGA18                    TR: GA18  *    00020000
      *                  GESTION DES ENTITES DE COMMANDE           *    00030000
      **************************************************************    00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00050000
      *                                                                 00060000
          02 COMM-GA18-APPLI REDEFINES COMM-GA00-APPLI.                 00070000
      *------------------------------ CODE DEPOT                        00080000
             03 COMM-GA18-NDEPOT         PIC X(3).                      00090001
      *------------------------------ LIBELLE DEPOT                     00100000
             03 COMM-GA18-LDEPOT         PIC X(20).                     00110000
      *------------------------------ CODE SOCIETE                      00120000
             03 COMM-GA18-NSOCIETE       PIC X(3).                      00130000
      *------------------------------ LIBELLE SOCIETE                   00140000
             03 COMM-GA18-LSOCIETE       PIC X(20).                     00150000
      *------------------------------ TABLE GA28                        00160001
             03 COMM-GA18-DONNEES.                                      00170001
                04 COMM-GA18-PAGE        PIC 99.                        00180001
                04 COMM-GA18-PAGETOT     PIC 99.                        00190001
                04 COMM-GA18-GA28        OCCURS 20.                     00200001
                   05 COMM-GA28-DVALIDITE   PIC X(8).                   00210001
                   05 COMM-GA28-QSOLA1      PIC S9(3)V9(2) COMP-3.      00220001
                   05 COMM-GA28-QSOLA2      PIC S9(3)V9(2) COMP-3.      00230001
                   05 COMM-GA28-QRACKB1     PIC S9(3)V9(2) COMP-3.      00240001
                   05 COMM-GA28-QRACKB2     PIC S9(3)V9(2) COMP-3.      00250001
                   05 COMM-GA28-QRACKB3     PIC S9(3)V9(2) COMP-3.      00260001
                   05 COMM-GA28-QRACKB4     PIC S9(3)V9(2) COMP-3.      00270001
                   05 COMM-GA28-QVRACC1     PIC S9(3)V9(2) COMP-3.      00280001
      ***************************************************************** 00290000
                                                                                
