      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
                                                                                
      ******************************************************************        
      * ITSSC                                                                   
      * GESTION PRIX PRIMES                                                     
E0265 ******************************************************************        
      * DE02005 04/04/06 CODE INITIAL                                           
E0316 ******************************************************************        
      * DSA057 21/12/06 SUPPORT MAINTENANCE ARBITRAGES CB10                     
      *                 AFFICHAGE EDM ADAPTE AU LIEU EDM CALCUL                 
      *                 AFFICHAGE COMMENTAIRE PRIX REF                          
      ******************************************************************        
      * 4096                                                            00370000
       01 Z-COMMAREA.                                                   00350000
*******  ZONES COMMUNES D'APPEL A LA TRANSACTION                        00370000
      *  1024                                                           00370000
         02 COMM-CB10-COMMUN.                                                   
      *    ZONES STANDARD RESERVEES                                     00370000
      *    0372                                                         00370000
           05 COMM-RESERVE.                                                     
               10 FILLER-COM-AIDA              PIC X(100).                  0038
               10 COMM-CICS-APPLID             PIC X(8).                    0041
               10 COMM-CICS-NETNAM             PIC X(8).                    0042
               10 COMM-CICS-TRANSA             PIC X(4).                    0043
               10 COMM-DATE-SIECLE             PIC XX.                      0046
               10 COMM-DATE-ANNEE              PIC XX.                      0047
               10 COMM-DATE-MOIS               PIC XX.                      0048
               10 COMM-DATE-JOUR               PIC XX.                      0049
               10 COMM-DATE-QNTA               PIC 999.                     0051
               10 COMM-DATE-QNT0               PIC 99999.                   0052
               10 COMM-DATE-BISX               PIC 9.                       0054
               10 COMM-DATE-JSM                PIC 9.                       0056
               10 COMM-DATE-JSM-LC             PIC XXX.                     0058
               10 COMM-DATE-JSM-LL             PIC XXXXXXXX.                0059
               10 COMM-DATE-MOIS-LC            PIC XXX.                     0061
               10 COMM-DATE-MOIS-LL            PIC XXXXXXXX.                0062
               10 COMM-DATE-SSAAMMJJ           PIC X(8).                    0064
               10 COMM-DATE-AAMMJJ             PIC X(6).                    0065
               10 COMM-DATE-JJMMSSAA           PIC X(8).                    0066
               10 COMM-DATE-JJMMAA             PIC X(6).                    0067
               10 COMM-DATE-JJ-MM-AA           PIC X(8).                    0068
               10 COMM-DATE-JJ-MM-SSAA         PIC X(10).                   0069
               10 COMM-DATE-WEEK.                                           0069
                   15 COMM-DATE-SEMSS          PIC 99.                      0069
                   15 COMM-DATE-SEMAA          PIC 99.                      0069
                   15 COMM-DATE-SEMNU          PIC 99.                      0069
               10 COMM-DATE-FILLER             PIC X(08).                   0069
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10 COMM-SWAP-CURS               PIC S9(4) COMP VALUE -1.     0075
      *--                                                                       
               10 COMM-SWAP-CURS               PIC S9(4) COMP-5 VALUE           
                                                                     -1.        
      *}                                                                        
               10 COMM-SWAP-ATTR OCCURS 150    PIC X(1).                    0076
      *    FILLER                                                       00370000
      *    0652                                                         00370000
           05 FILLER                           PIC X(652).                      
*******  ZONES SPECIFIQUES TCB10                                        00370000
      *  1024                                                           00370000
         02 COMM-CB10-TCB10.                                                    
      *    COMMAREA SPECIFIQUE TCB10                                            
      *    0189                                                                 
E0316 *    0191                                                                 
           05 CS-CB10-DATA.                                                     
               10 CS-CB10-NCODIC              PIC X(7) VALUE SPACE.             
      *        0022                                                             
E0316 *        0024                                                             
               10 CS-CB10-GESTION.                                              
                   15 CP-CB10-STATUT          PIC S9(3) COMP-3 VALUE 0. .       
                       88 CC-CB10-AUCUN           VALUE  000.                   
                       88 CC-CB10-INIT            VALUE +010.                   
                       88 CC-CB10-OK              VALUE +100.                   
                       88 CC-CB10-MOD             VALUE +200.                   
                       88 CC-CB10-ANO             VALUE -100.                   
                   15 CP-CB10-SEL             PIC S9(3) COMP-3 VALUE 0. .       
                       88 CC-CB10-SEL-NONE        VALUE  000.                   
                       88 CC-CB10-SEL-EVAL        VALUE +100.                   
                   15 CP-CB10-ACT             PIC S9(3) COMP-3 VALUE 0. .       
                       88 CC-CB10-ACT-NONE        VALUE  000.                   
                       88 CC-CB10-ACT-EVAL        VALUE +100.                   
                   15 CP-CB10-PAGE            PIC S9(3) COMP-3.                 
                   15 CP-CB10-PAGEMAX         PIC S9(3) COMP-3.                 
                   15 CS-CB10-TS.                                               
                       20 TS-CB10-IDENT       PIC X(8).                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                20 TB-CB10-NB          PIC S9(4) COMP.                   
      *--                                                                       
                       20 TB-CB10-NB          PIC S9(4) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                20 TB-CB10-MAX         PIC S9(4) COMP.                   
      *--                                                                       
                       20 TB-CB10-MAX         PIC S9(4) COMP-5.                 
      *}                                                                        
E0316              15 CP-CB10-ZONE-DGRP       PIC S9(3) COMP-3 VALUE 0.         
      *        0160                                                             
               10 CS-CB10-ENR.                                                  
                   15 CS-CB10-LREFFOURN       PIC X(20).                        
                   15 CS-CB10-CFAM            PIC X(5).                         
                   15 CS-CB10-LFAM            PIC X(20).                        
                   15 CS-CB10-CMARQ           PIC X(5).                         
                   15 CS-CB10-LMARQ           PIC X(20).                        
                   15 CS-CB10-SAISIE.                                           
                       20 CS-CB10-SEL         PIC X(1).                         
                       20 CS-CB10-LIEU        PIC X(3).                         
                       20 CS-CB10-NCONC       PIC X(4).                         
                       20 CS-CB10-PX          PIC X(8).                         
                       20 CS-CB10-PXD         PIC X(8).                         
                       20 CS-CB10-PXF         PIC X(8).                         
                       20 CS-CB10-LIB         PIC X(20).                        
                       20 CS-CB10-OA          PIC X(1).                         
                       20 CS-CB10-OAD         PIC X(8).                         
                       20 CS-CB10-OAF         PIC X(8).                         
                       20 CS-CB10-PM          PIC X(5).                         
                       20 CS-CB10-PMD         PIC X(8).                         
                       20 CS-CB10-PMF         PIC X(8).                         
      *    0835                                                                 
E0316 *    0833                                                                 
E0316 *    05 FILLER                          PIC X(835).                       
E0316      05 FILLER                          PIC X(833).                       
*******  ZONES SPECIFIQUES MCB00                                        00370000
      *  1024                                                           00370000
         02 COMM-CB10-MCB00.                                                    
                                                                                
           EXEC SQL                                                             
             INCLUDE COMMCB00                                                   
           END-EXEC                                                            
                                                                                
*******  FILLER LIBRE                                                   00370000
      *  1024                                                           00370000
         02 FILLER                            PIC X(1024).                      
                                                                                
