      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HEGOP REGLES D OPERATIONS GHE          *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01M9.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01M9.                                                            
      *}                                                                        
           05  HEGOP-CTABLEG2    PIC X(15).                                     
           05  HEGOP-CTABLEG2-REDEF REDEFINES HEGOP-CTABLEG2.                   
               10  HEGOP-CPROG           PIC X(05).                             
               10  HEGOP-NSEQ            PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  HEGOP-NSEQ-N         REDEFINES HEGOP-NSEQ                    
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  HEGOP-WTABLEG     PIC X(80).                                     
           05  HEGOP-WTABLEG-REDEF  REDEFINES HEGOP-WTABLEG.                    
               10  HEGOP-WACTIF          PIC X(01).                             
               10  HEGOP-FLAGAVAR        PIC X(08).                             
               10  HEGOP-TYOPAV          PIC X(06).                             
               10  HEGOP-TYOPAR          PIC X(06).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01M9-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01M9-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HEGOP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HEGOP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HEGOP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HEGOP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
