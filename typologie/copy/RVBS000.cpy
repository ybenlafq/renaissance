      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE BS000 COMMISSIONS TYPE/NATURE          *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBS000.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBS000.                                                             
      *}                                                                        
           05  BS000-CTABLEG2    PIC X(15).                                     
           05  BS000-CTABLEG2-REDEF REDEFINES BS000-CTABLEG2.                   
               10  BS000-NENTITE         PIC X(05).                             
               10  BS000-TYPE            PIC X(05).                             
               10  BS000-CTYPCND         PIC X(05).                             
           05  BS000-WTABLEG     PIC X(80).                                     
           05  BS000-WTABLEG-REDEF  REDEFINES BS000-WTABLEG.                    
               10  BS000-LIBELLE         PIC X(20).                             
               10  BS000-CTYPOPER        PIC X(05).                             
               10  BS000-CNATOPER        PIC X(05).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBS000-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBS000-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BS000-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  BS000-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BS000-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  BS000-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
