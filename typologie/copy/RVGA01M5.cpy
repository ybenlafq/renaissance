      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EFGAR CODES GARANTIE / CENTRE TRAIT.   *        
      *----------------------------------------------------------------*        
       01  RVGA01M5.                                                            
           05  EFGAR-CTABLEG2    PIC X(15).                                     
           05  EFGAR-CTABLEG2-REDEF REDEFINES EFGAR-CTABLEG2.                   
               10  EFGAR-CTRAIT          PIC X(05).                             
               10  EFGAR-CGARANTI        PIC X(05).                             
           05  EFGAR-WTABLEG     PIC X(80).                                     
           05  EFGAR-WTABLEG-REDEF  REDEFINES EFGAR-WTABLEG.                    
               10  EFGAR-WACTIF          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01M5-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EFGAR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EFGAR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EFGAR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EFGAR-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
