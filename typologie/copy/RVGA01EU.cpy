      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE LPLGE PLAGE HORAIRE DE DELIVRANCE      *        
      *----------------------------------------------------------------*        
       01  RVGA01EU.                                                            
           05  LPLGE-CTABLEG2    PIC X(15).                                     
           05  LPLGE-CTABLEG2-REDEF REDEFINES LPLGE-CTABLEG2.                   
               10  LPLGE-CPLAGE          PIC X(02).                             
           05  LPLGE-WTABLEG     PIC X(80).                                     
           05  LPLGE-WTABLEG-REDEF  REDEFINES LPLGE-WTABLEG.                    
               10  LPLGE-LPLAGE          PIC X(08).                             
               10  LPLGE-HEURE           PIC X(04).                             
               10  LPLGE-HEURE-N        REDEFINES LPLGE-HEURE                   
                                         PIC 9(04).                             
               10  LPLGE-CPLOGI          PIC X(02).                             
               10  LPLGE-HDEBUT          PIC X(04).                             
               10  LPLGE-HFIN            PIC X(04).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01EU-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LPLGE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  LPLGE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LPLGE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  LPLGE-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
