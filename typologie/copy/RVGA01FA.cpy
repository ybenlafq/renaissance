      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MOPAI CODE MODE DE PAIEMENT            *        
      *----------------------------------------------------------------*        
       01  RVGA01FA.                                                            
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  MOPAI-CTABLEG2    PIC X(15).                                     
           05  MOPAI-CTABLEG2-REDEF REDEFINES MOPAI-CTABLEG2.                   
               10  MOPAI-CMODRGLT        PIC X(05).                             
           05  MOPAI-WTABLEG     PIC X(80).                                     
           05  MOPAI-WTABLEG-REDEF  REDEFINES MOPAI-WTABLEG.                    
               10  MOPAI-LMODRGLT        PIC X(20).                             
               10  MOPAI-WMTVTE          PIC X(10).                             
               10  MOPAI-C36SEQV         PIC X(04).                             
               10  MOPAI-RENDU           PIC X(05).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  MOPAI-RENDU-N        REDEFINES MOPAI-RENDU                   
                                         PIC 9(03)V9(02).                       
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  MOPAI-IWERC           PIC X(14).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01FA-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MOPAI-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MOPAI-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MOPAI-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MOPAI-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
