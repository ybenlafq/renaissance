      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE QCNBV QUOTA D EXTRACTION DES VENTES    *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01QO.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01QO.                                                            
      *}                                                                        
           05  QCNBV-CTABLEG2    PIC X(15).                                     
           05  QCNBV-CTABLEG2-REDEF REDEFINES QCNBV-CTABLEG2.                   
               10  QCNBV-NSOCLIEU       PIC X(06).                              
           05  QCNBV-WTABLEG     PIC X(80).                                     
           05  QCNBV-WTABLEG-REDEF  REDEFINES QCNBV-WTABLEG.                    
               10  QCNBV-WACTIF          PIC X(01).                             
               10  QCNBV-QUOTA           PIC X(04).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  QCNBV-QUOTA-N        REDEFINES QCNBV-QUOTA                   
                                         PIC 9(04).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  QCNBV-CPERIM          PIC X(05).                             
               10  QCNBV-GRVTE           PIC X(03).                             
               10  QCNBV-QUOLIV          PIC X(04).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  QCNBV-QUOLIV-N       REDEFINES QCNBV-QUOLIV                  
                                         PIC 9(04).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01QO-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01QO-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QCNBV-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  QCNBV-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QCNBV-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  QCNBV-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
