      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE IFSOC SOCIETE LIEU DES SAV             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01SZ.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01SZ.                                                            
      *}                                                                        
           05  IFSOC-CTABLEG2    PIC X(15).                                     
           05  IFSOC-CTABLEG2-REDEF REDEFINES IFSOC-CTABLEG2.                   
               10  IFSOC-NSOCIETE        PIC X(03).                             
               10  IFSOC-NLIEU           PIC X(03).                             
           05  IFSOC-WTABLEG     PIC X(80).                                     
           05  IFSOC-WTABLEG-REDEF  REDEFINES IFSOC-WTABLEG.                    
               10  IFSOC-LLIEU           PIC X(20).                             
               10  IFSOC-NSOCCPT         PIC X(03).                             
               10  IFSOC-NETAB           PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01SZ-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01SZ-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IFSOC-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  IFSOC-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IFSOC-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  IFSOC-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
