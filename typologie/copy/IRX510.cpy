      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRX510 AU 13/12/2004  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,03,PD,A,                          *        
      *                           23,20,BI,A,                          *        
      *                           43,20,BI,A,                          *        
      *                           63,20,BI,A,                          *        
      *                           83,20,BI,A,                          *        
      *                           03,02,BI,A,                          *        
      *                           05,05,PD,A,                          *        
      *                           10,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRX510.                                                        
            05 NOMETAT-IRX510           PIC X(6) VALUE 'IRX510'.                
            05 RUPTURES-IRX510.                                                 
           10 IRX510-NSOCIETE           PIC X(03).                      007  003
           10 IRX510-CHEFPROD           PIC X(05).                      010  005
           10 IRX510-CMARQ              PIC X(05).                      015  005
           10 IRX510-WSEQFAM            PIC S9(05)      COMP-3.         020  003
           10 IRX510-LVMARKET1          PIC X(20).                      023  020
           10 IRX510-LVMARKET2          PIC X(20).                      043  020
           10 IRX510-LVMARKET3          PIC X(20).                      063  020
           10 IRX510-LREFFOURN          PIC X(20).                      083  020
           10 IRX510-NZONPRIX           PIC X(02).                      103  002
           10 IRX510-PVCONC             PIC S9(07)V9(2) COMP-3.         105  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRX510-SEQUENCE           PIC S9(04) COMP.                110  002
      *--                                                                       
           10 IRX510-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRX510.                                                   
           10 IRX510-CFAM               PIC X(05).                      112  005
           10 IRX510-COLIS              PIC X(30).                      117  030
           10 IRX510-CORIG              PIC X(01).                      147  001
           10 IRX510-DRELEVE            PIC X(04).                      148  004
           10 IRX510-ECART              PIC X(01).                      152  001
           10 IRX510-EDITCODE           PIC X(01).                      153  001
           10 IRX510-EDITD8             PIC X(01).                      154  001
           10 IRX510-FLAGEDITION        PIC X(01).                      155  001
           10 IRX510-FLAGPEX            PIC X(01).                      156  001
           10 IRX510-FOURNREF           PIC X(20).                      157  020
           10 IRX510-HISTORIQUE         PIC X(01).                      177  001
           10 IRX510-LCHEFPROD          PIC X(20).                      178  020
           10 IRX510-LCOMMENT           PIC X(20).                      198  020
           10 IRX510-LCONCPE            PIC X(20).                      218  020
           10 IRX510-LEMPCONC           PIC X(10).                      238  010
           10 IRX510-LENSCONC           PIC X(15).                      248  015
           10 IRX510-LIBELLE1           PIC X(26).                      263  026
           10 IRX510-LIBELLE2           PIC X(03).                      289  003
           10 IRX510-LIBELLE3           PIC X(05).                      292  005
           10 IRX510-LMARQ              PIC X(20).                      297  020
           10 IRX510-LSTATCOMP          PIC X(03).                      317  003
           10 IRX510-MARQREF            PIC X(05).                      320  005
           10 IRX510-NCODIC             PIC X(07).                      325  007
           10 IRX510-NCONCPE            PIC X(04).                      332  004
           10 IRX510-NMAGPE             PIC X(03).                      336  003
           10 IRX510-NONCODIC           PIC X(11).                      339  011
           10 IRX510-PEXPTTC            PIC S9(07)V9(2) COMP-3.         350  005
           10 IRX510-PVMAG              PIC S9(07)V9(2) COMP-3.         355  005
           10 IRX510-PVREF              PIC S9(07)V9(2) COMP-3.         360  005
           10 IRX510-DATEAU             PIC X(08).                      365  008
           10 IRX510-DATEDU             PIC X(08).                      373  008
           10 IRX510-DFINEFFETPE        PIC X(08).                      381  008
            05 FILLER                      PIC X(124).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IRX510-LONG           PIC S9(4)   COMP  VALUE +388.           
      *                                                                         
      *--                                                                       
        01  DSECT-IRX510-LONG           PIC S9(4) COMP-5  VALUE +388.           
                                                                                
      *}                                                                        
