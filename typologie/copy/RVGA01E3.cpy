      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MAGTY TYPES DE MAGASIN                 *        
      *----------------------------------------------------------------*        
       01  RVGA01E3.                                                            
       EXEC SQL BEGIN DECLARE SECTION END-EXEC
           05  MAGTY-CTABLEG2    PIC X(15).                                     
           05  MAGTY-CTABLEG2-REDEF REDEFINES MAGTY-CTABLEG2.                   
               10  MAGTY-MAG             PIC X(03).                             
       EXEC SQL END DECLARE SECTION END-EXEC
               10  MAGTY-MAG-N          REDEFINES MAGTY-MAG                     
                                         PIC 9(03).                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC
           05  MAGTY-WTABLEG     PIC X(80).                                     
           05  MAGTY-WTABLEG-REDEF  REDEFINES MAGTY-WTABLEG.                    
               10  MAGTY-TYPE            PIC X(03).                             
               10  MAGTY-LIBELLE         PIC X(20).                             
               10  MAGTY-WVE             PIC X(01).                             
       EXEC SQL END DECLARE SECTION END-EXEC
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01E3-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MAGTY-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MAGTY-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MAGTY-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MAGTY-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
