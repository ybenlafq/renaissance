      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE       .RTGA55                               
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVGA5510.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVGA5510.                                                             
      *}                                                                        
           10 GA55-NCODIC          PIC X(07).                                   
           10 GA55-NENTCDE         PIC X(05).                                   
           10 GA55-WENTCDE         PIC X(01).                                   
           10 GA55-COPCO           PIC X(03).                                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVGA5510-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVGA5510-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA55-NCODIC-F          PIC S9(4) COMP.                            
      *--                                                                       
           10 GA55-NCODIC-F          PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA55-NENTCDE-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 GA55-NENTCDE-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA55-WENTCDE-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 GA55-WENTCDE-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA55-COPCO-F           PIC S9(4) COMP.                            
      *                                                                         
      *--                                                                       
           10 GA55-COPCO-F           PIC S9(4) COMP-5.                          
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
