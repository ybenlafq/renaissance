      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGA9900                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA9900                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA9900.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA9900.                                                            
      *}                                                                        
           02  GA99-CNOMPGRM                                                    
               PIC X(0005).                                                     
           02  GA99-NSEQERR                                                     
               PIC X(0004).                                                     
           02  GA99-CERRUT                                                      
               PIC X(0004).                                                     
           02  GA99-LIBERR                                                      
               PIC X(0053).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGA9900                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA9900-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA9900-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA99-CNOMPGRM-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA99-CNOMPGRM-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA99-NSEQERR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA99-NSEQERR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA99-CERRUT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA99-CERRUT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA99-LIBERR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GA99-LIBERR-F                                                    
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
