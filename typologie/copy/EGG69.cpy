      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGG69   EGG69                                              00000020
      ***************************************************************** 00000030
       01   EGG69I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPI     PIC X(2).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVISEL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MCDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEVISEF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCDEVISEI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEVISEL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLDEVISEF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLDEVISEI      PIC X(25).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNCODICI  PIC X(7).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLREFI    PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVALSTCKTOTL   COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MVALSTCKTOTL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MVALSTCKTOTF   PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MVALSTCKTOTI   PIC X(11).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCFAMI    PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLFAMI    PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVALSTCKTODL   COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MVALSTCKTODL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MVALSTCKTODF   PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MVALSTCKTODI   PIC X(11).                                 00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCMARQI   PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLMARQI   PIC X(20).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTCKDEBJTOL   COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MSTCKDEBJTOL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MSTCKDEBJTOF   PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MSTCKDEBJTOI   PIC X(9).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTCKDEBJTDL   COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MSTCKDEBJTDL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MSTCKDEBJTDF   PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MSTCKDEBJTDI   PIC X(9).                                  00000690
           02 MLIGNEI OCCURS   4 TIMES .                                00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCFILL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MSOCFILL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSOCFILF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MSOCFILI     PIC X(3).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDPRMPL      COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MDPRMPL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDPRMPF      PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MDPRMPI      PIC X(14).                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTOCKFL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MSTOCKFL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTOCKFF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MSTOCKFI     PIC X(9).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVSTOCKL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MVSTOCKL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MVSTOCKF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MVSTOCKI     PIC X(14).                                 00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTOCKCOURL  COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MSTOCKCOURL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MSTOCKCOURF  PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MSTOCKCOURI  PIC X(9).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTOCKDEBJL  COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MSTOCKDEBJL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MSTOCKDEBJF  PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MSTOCKDEBJI  PIC X(9).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRISTL      COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MPRISTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPRISTF      PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MPRISTI      PIC X(10).                                 00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPSUPPL      COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MPSUPPL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPSUPPF      PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MPSUPPI      PIC X(10).                                 00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNPRMPL      COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MNPRMPL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNPRMPF      PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MNPRMPI      PIC X(14).                                 00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 LIB1L   COMP PIC S9(4).                                 00001070
      *--                                                                       
             03 LIB1L COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 LIB1F   PIC X.                                          00001080
             03 FILLER  PIC X(4).                                       00001090
             03 LIB1I   PIC X(26).                                      00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVALDL  COMP PIC S9(4).                                 00001110
      *--                                                                       
             03 MVALDL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MVALDF  PIC X.                                          00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MVALDI  PIC X(14).                                      00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 LIB2L   COMP PIC S9(4).                                 00001150
      *--                                                                       
             03 LIB2L COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 LIB2F   PIC X.                                          00001160
             03 FILLER  PIC X(4).                                       00001170
             03 LIB2I   PIC X(19).                                      00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTOCKDEBDL  COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MSTOCKDEBDL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MSTOCKDEBDF  PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MSTOCKDEBDI  PIC X(9).                                  00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MZONCMDI  PIC X(15).                                      00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MLIBERRI  PIC X(58).                                      00001300
      * CODE TRANSACTION                                                00001310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001320
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001330
           02 FILLER    PIC X(4).                                       00001340
           02 MCODTRAI  PIC X(4).                                       00001350
      * CICS DE TRAVAIL                                                 00001360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001370
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001380
           02 FILLER    PIC X(4).                                       00001390
           02 MCICSI    PIC X(5).                                       00001400
      * NETNAME                                                         00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MNETNAMI  PIC X(8).                                       00001450
      * CODE TERMINAL                                                   00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MSCREENI  PIC X(5).                                       00001500
      ***************************************************************** 00001510
      * SDF: EGG69   EGG69                                              00001520
      ***************************************************************** 00001530
       01   EGG69O REDEFINES EGG69I.                                    00001540
           02 FILLER    PIC X(12).                                      00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MDATJOUA  PIC X.                                          00001570
           02 MDATJOUC  PIC X.                                          00001580
           02 MDATJOUP  PIC X.                                          00001590
           02 MDATJOUH  PIC X.                                          00001600
           02 MDATJOUV  PIC X.                                          00001610
           02 MDATJOUO  PIC X(10).                                      00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MTIMJOUA  PIC X.                                          00001640
           02 MTIMJOUC  PIC X.                                          00001650
           02 MTIMJOUP  PIC X.                                          00001660
           02 MTIMJOUH  PIC X.                                          00001670
           02 MTIMJOUV  PIC X.                                          00001680
           02 MTIMJOUO  PIC X(5).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MPAGEA    PIC X.                                          00001710
           02 MPAGEC    PIC X.                                          00001720
           02 MPAGEP    PIC X.                                          00001730
           02 MPAGEH    PIC X.                                          00001740
           02 MPAGEV    PIC X.                                          00001750
           02 MPAGEO    PIC X(2).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MNBPA     PIC X.                                          00001780
           02 MNBPC     PIC X.                                          00001790
           02 MNBPP     PIC X.                                          00001800
           02 MNBPH     PIC X.                                          00001810
           02 MNBPV     PIC X.                                          00001820
           02 MNBPO     PIC X(2).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MCDEVISEA      PIC X.                                     00001850
           02 MCDEVISEC PIC X.                                          00001860
           02 MCDEVISEP PIC X.                                          00001870
           02 MCDEVISEH PIC X.                                          00001880
           02 MCDEVISEV PIC X.                                          00001890
           02 MCDEVISEO      PIC X(3).                                  00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MLDEVISEA      PIC X.                                     00001920
           02 MLDEVISEC PIC X.                                          00001930
           02 MLDEVISEP PIC X.                                          00001940
           02 MLDEVISEH PIC X.                                          00001950
           02 MLDEVISEV PIC X.                                          00001960
           02 MLDEVISEO      PIC X(25).                                 00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MNCODICA  PIC X.                                          00001990
           02 MNCODICC  PIC X.                                          00002000
           02 MNCODICP  PIC X.                                          00002010
           02 MNCODICH  PIC X.                                          00002020
           02 MNCODICV  PIC X.                                          00002030
           02 MNCODICO  PIC X(7).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MLREFA    PIC X.                                          00002060
           02 MLREFC    PIC X.                                          00002070
           02 MLREFP    PIC X.                                          00002080
           02 MLREFH    PIC X.                                          00002090
           02 MLREFV    PIC X.                                          00002100
           02 MLREFO    PIC X(20).                                      00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MVALSTCKTOTA   PIC X.                                     00002130
           02 MVALSTCKTOTC   PIC X.                                     00002140
           02 MVALSTCKTOTP   PIC X.                                     00002150
           02 MVALSTCKTOTH   PIC X.                                     00002160
           02 MVALSTCKTOTV   PIC X.                                     00002170
           02 MVALSTCKTOTO   PIC X(11).                                 00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MCFAMA    PIC X.                                          00002200
           02 MCFAMC    PIC X.                                          00002210
           02 MCFAMP    PIC X.                                          00002220
           02 MCFAMH    PIC X.                                          00002230
           02 MCFAMV    PIC X.                                          00002240
           02 MCFAMO    PIC X(5).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MLFAMA    PIC X.                                          00002270
           02 MLFAMC    PIC X.                                          00002280
           02 MLFAMP    PIC X.                                          00002290
           02 MLFAMH    PIC X.                                          00002300
           02 MLFAMV    PIC X.                                          00002310
           02 MLFAMO    PIC X(20).                                      00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MVALSTCKTODA   PIC X.                                     00002340
           02 MVALSTCKTODC   PIC X.                                     00002350
           02 MVALSTCKTODP   PIC X.                                     00002360
           02 MVALSTCKTODH   PIC X.                                     00002370
           02 MVALSTCKTODV   PIC X.                                     00002380
           02 MVALSTCKTODO   PIC X(11).                                 00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MCMARQA   PIC X.                                          00002410
           02 MCMARQC   PIC X.                                          00002420
           02 MCMARQP   PIC X.                                          00002430
           02 MCMARQH   PIC X.                                          00002440
           02 MCMARQV   PIC X.                                          00002450
           02 MCMARQO   PIC X(5).                                       00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MLMARQA   PIC X.                                          00002480
           02 MLMARQC   PIC X.                                          00002490
           02 MLMARQP   PIC X.                                          00002500
           02 MLMARQH   PIC X.                                          00002510
           02 MLMARQV   PIC X.                                          00002520
           02 MLMARQO   PIC X(20).                                      00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MSTCKDEBJTOA   PIC X.                                     00002550
           02 MSTCKDEBJTOC   PIC X.                                     00002560
           02 MSTCKDEBJTOP   PIC X.                                     00002570
           02 MSTCKDEBJTOH   PIC X.                                     00002580
           02 MSTCKDEBJTOV   PIC X.                                     00002590
           02 MSTCKDEBJTOO   PIC X(9).                                  00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MSTCKDEBJTDA   PIC X.                                     00002620
           02 MSTCKDEBJTDC   PIC X.                                     00002630
           02 MSTCKDEBJTDP   PIC X.                                     00002640
           02 MSTCKDEBJTDH   PIC X.                                     00002650
           02 MSTCKDEBJTDV   PIC X.                                     00002660
           02 MSTCKDEBJTDO   PIC X(9).                                  00002670
           02 MLIGNEO OCCURS   4 TIMES .                                00002680
             03 FILLER       PIC X(2).                                  00002690
             03 MSOCFILA     PIC X.                                     00002700
             03 MSOCFILC     PIC X.                                     00002710
             03 MSOCFILP     PIC X.                                     00002720
             03 MSOCFILH     PIC X.                                     00002730
             03 MSOCFILV     PIC X.                                     00002740
             03 MSOCFILO     PIC X(3).                                  00002750
             03 FILLER       PIC X(2).                                  00002760
             03 MDPRMPA      PIC X.                                     00002770
             03 MDPRMPC PIC X.                                          00002780
             03 MDPRMPP PIC X.                                          00002790
             03 MDPRMPH PIC X.                                          00002800
             03 MDPRMPV PIC X.                                          00002810
             03 MDPRMPO      PIC X(14).                                 00002820
             03 FILLER       PIC X(2).                                  00002830
             03 MSTOCKFA     PIC X.                                     00002840
             03 MSTOCKFC     PIC X.                                     00002850
             03 MSTOCKFP     PIC X.                                     00002860
             03 MSTOCKFH     PIC X.                                     00002870
             03 MSTOCKFV     PIC X.                                     00002880
             03 MSTOCKFO     PIC X(9).                                  00002890
             03 FILLER       PIC X(2).                                  00002900
             03 MVSTOCKA     PIC X.                                     00002910
             03 MVSTOCKC     PIC X.                                     00002920
             03 MVSTOCKP     PIC X.                                     00002930
             03 MVSTOCKH     PIC X.                                     00002940
             03 MVSTOCKV     PIC X.                                     00002950
             03 MVSTOCKO     PIC X(14).                                 00002960
             03 FILLER       PIC X(2).                                  00002970
             03 MSTOCKCOURA  PIC X.                                     00002980
             03 MSTOCKCOURC  PIC X.                                     00002990
             03 MSTOCKCOURP  PIC X.                                     00003000
             03 MSTOCKCOURH  PIC X.                                     00003010
             03 MSTOCKCOURV  PIC X.                                     00003020
             03 MSTOCKCOURO  PIC X(9).                                  00003030
             03 FILLER       PIC X(2).                                  00003040
             03 MSTOCKDEBJA  PIC X.                                     00003050
             03 MSTOCKDEBJC  PIC X.                                     00003060
             03 MSTOCKDEBJP  PIC X.                                     00003070
             03 MSTOCKDEBJH  PIC X.                                     00003080
             03 MSTOCKDEBJV  PIC X.                                     00003090
             03 MSTOCKDEBJO  PIC X(9).                                  00003100
             03 FILLER       PIC X(2).                                  00003110
             03 MPRISTA      PIC X.                                     00003120
             03 MPRISTC PIC X.                                          00003130
             03 MPRISTP PIC X.                                          00003140
             03 MPRISTH PIC X.                                          00003150
             03 MPRISTV PIC X.                                          00003160
             03 MPRISTO      PIC X(10).                                 00003170
             03 FILLER       PIC X(2).                                  00003180
             03 MPSUPPA      PIC X.                                     00003190
             03 MPSUPPC PIC X.                                          00003200
             03 MPSUPPP PIC X.                                          00003210
             03 MPSUPPH PIC X.                                          00003220
             03 MPSUPPV PIC X.                                          00003230
             03 MPSUPPO      PIC X(10).                                 00003240
             03 FILLER       PIC X(2).                                  00003250
             03 MNPRMPA      PIC X.                                     00003260
             03 MNPRMPC PIC X.                                          00003270
             03 MNPRMPP PIC X.                                          00003280
             03 MNPRMPH PIC X.                                          00003290
             03 MNPRMPV PIC X.                                          00003300
             03 MNPRMPO      PIC X(14).                                 00003310
             03 FILLER       PIC X(2).                                  00003320
             03 LIB1A   PIC X.                                          00003330
             03 LIB1C   PIC X.                                          00003340
             03 LIB1P   PIC X.                                          00003350
             03 LIB1H   PIC X.                                          00003360
             03 LIB1V   PIC X.                                          00003370
             03 LIB1O   PIC X(26).                                      00003380
             03 FILLER       PIC X(2).                                  00003390
             03 MVALDA  PIC X.                                          00003400
             03 MVALDC  PIC X.                                          00003410
             03 MVALDP  PIC X.                                          00003420
             03 MVALDH  PIC X.                                          00003430
             03 MVALDV  PIC X.                                          00003440
             03 MVALDO  PIC X(14).                                      00003450
             03 FILLER       PIC X(2).                                  00003460
             03 LIB2A   PIC X.                                          00003470
             03 LIB2C   PIC X.                                          00003480
             03 LIB2P   PIC X.                                          00003490
             03 LIB2H   PIC X.                                          00003500
             03 LIB2V   PIC X.                                          00003510
             03 LIB2O   PIC X(19).                                      00003520
             03 FILLER       PIC X(2).                                  00003530
             03 MSTOCKDEBDA  PIC X.                                     00003540
             03 MSTOCKDEBDC  PIC X.                                     00003550
             03 MSTOCKDEBDP  PIC X.                                     00003560
             03 MSTOCKDEBDH  PIC X.                                     00003570
             03 MSTOCKDEBDV  PIC X.                                     00003580
             03 MSTOCKDEBDO  PIC X(9).                                  00003590
           02 FILLER    PIC X(2).                                       00003600
           02 MZONCMDA  PIC X.                                          00003610
           02 MZONCMDC  PIC X.                                          00003620
           02 MZONCMDP  PIC X.                                          00003630
           02 MZONCMDH  PIC X.                                          00003640
           02 MZONCMDV  PIC X.                                          00003650
           02 MZONCMDO  PIC X(15).                                      00003660
           02 FILLER    PIC X(2).                                       00003670
           02 MLIBERRA  PIC X.                                          00003680
           02 MLIBERRC  PIC X.                                          00003690
           02 MLIBERRP  PIC X.                                          00003700
           02 MLIBERRH  PIC X.                                          00003710
           02 MLIBERRV  PIC X.                                          00003720
           02 MLIBERRO  PIC X(58).                                      00003730
      * CODE TRANSACTION                                                00003740
           02 FILLER    PIC X(2).                                       00003750
           02 MCODTRAA  PIC X.                                          00003760
           02 MCODTRAC  PIC X.                                          00003770
           02 MCODTRAP  PIC X.                                          00003780
           02 MCODTRAH  PIC X.                                          00003790
           02 MCODTRAV  PIC X.                                          00003800
           02 MCODTRAO  PIC X(4).                                       00003810
      * CICS DE TRAVAIL                                                 00003820
           02 FILLER    PIC X(2).                                       00003830
           02 MCICSA    PIC X.                                          00003840
           02 MCICSC    PIC X.                                          00003850
           02 MCICSP    PIC X.                                          00003860
           02 MCICSH    PIC X.                                          00003870
           02 MCICSV    PIC X.                                          00003880
           02 MCICSO    PIC X(5).                                       00003890
      * NETNAME                                                         00003900
           02 FILLER    PIC X(2).                                       00003910
           02 MNETNAMA  PIC X.                                          00003920
           02 MNETNAMC  PIC X.                                          00003930
           02 MNETNAMP  PIC X.                                          00003940
           02 MNETNAMH  PIC X.                                          00003950
           02 MNETNAMV  PIC X.                                          00003960
           02 MNETNAMO  PIC X(8).                                       00003970
      * CODE TERMINAL                                                   00003980
           02 FILLER    PIC X(2).                                       00003990
           02 MSCREENA  PIC X.                                          00004000
           02 MSCREENC  PIC X.                                          00004010
           02 MSCREENP  PIC X.                                          00004020
           02 MSCREENH  PIC X.                                          00004030
           02 MSCREENV  PIC X.                                          00004040
           02 MSCREENO  PIC X(5).                                       00004050
                                                                                
