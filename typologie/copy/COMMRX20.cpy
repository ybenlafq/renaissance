      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA DE LA TRANSACTION RX20                            *    00010100
      **************************************************************    00110000
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00120000
      **************************************************************    00130000
      *                                                                 00140000
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00150000
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00160000
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00170000
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00180000
      *                                                                 00190000
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00200000
      * COMPRENANT :                                                    00210000
      * 1 - LES ZONES RESERVEES A AIDA                                  00220000
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00230000
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00240000
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00250000
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00260000
      *                                                                 00270000
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00280000
      * PAR AIDA                                                        00290000
      *                                                                 00300000
      *-------------------------------------------------------------    00310000
      *                                                                 00320000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-RX20-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00330000
      *--                                                                       
       01  COM-RX20-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00340000
       01  Z-COMMAREA.                                                  00350000
      *                                                                 00360000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00370000
          02 FILLER-COM-AIDA      PIC X(100).                           00380000
      *                                                                 00390000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00400000
          02 COMM-CICS-APPLID     PIC X(8).                             00410000
          02 COMM-CICS-NETNAM     PIC X(8).                             00420000
          02 COMM-CICS-TRANSA     PIC X(4).                             00430000
      *                                                                 00440000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00450000
          02 COMM-DATE-SIECLE     PIC XX.                               00460000
          02 COMM-DATE-ANNEE      PIC XX.                               00470000
          02 COMM-DATE-MOIS       PIC XX.                               00480000
          02 COMM-DATE-JOUR       PIC XX.                               00490000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00500000
          02 COMM-DATE-QNTA       PIC 999.                              00510000
          02 COMM-DATE-QNT0       PIC 99999.                            00520000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00530000
          02 COMM-DATE-BISX       PIC 9.                                00540000
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00550000
          02 COMM-DATE-JSM        PIC 9.                                00560000
      *   LIBELLES DU JOUR COURT - LONG                                 00570000
          02 COMM-DATE-JSM-LC     PIC XXX.                              00580000
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                         00590000
      *   LIBELLES DU MOIS COURT - LONG                                 00600000
          02 COMM-DATE-MOIS-LC    PIC XXX.                              00610000
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                         00620000
      *   DIFFERENTES FORMES DE DATE                                    00630000
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                             00640000
          02 COMM-DATE-AAMMJJ     PIC X(6).                             00650000
          02 COMM-DATE-JJMMSSAA   PIC X(8).                             00660000
          02 COMM-DATE-JJMMAA     PIC X(6).                             00670000
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                             00680000
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                            00690000
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00690010
          02 COMM-DATE-WEEK.                                            00690020
             05  COMM-DATE-SEMSS  PIC 99.                               00690030
             05  COMM-DATE-SEMAA  PIC 99.                               00690040
             05  COMM-DATE-SEMNU  PIC 99.                               00690050
          02 COMM-DATE-FILLER     PIC X(08).                            00690060
      *                                                                 00720000
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00730000
      *                                                                 00740000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.              00750000
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 1 PIC X(1).                          00760000
      *                                                                 00770000
      * ZONES MESSAGE DU MODULE MRX20                    --------- 78   00780000
      *                                                                 00790000
          02 COMM-MRX20-MESS           PIC X(78).                       00800000
      *                                                                 00810000
      * ZONES COMMUNES DONNEES SPECIFIQUES A RELEVE DE PRIX ------ 172  00810020
      *                                                                 00810030
          02 COMM-RX10-COMMUM.                                          00810040
              03 COMM-RX20-NSOC              PIC X(3).                  00810050
              03 COMM-RX20-NMAG              PIC X(3).                  00810060
              03 COMM-RX10-LMAG              PIC X(20).                 00810070
              03 COMM-RX20-SIEGE             PIC X(1).                  00810080
              03 COMM-RX20-NZON              PIC X(2).                  00810090
              03 COMM-RX20-NCON              PIC X(4).                  00810091
              03 COMM-RX10-LENSCONC          PIC X(15).                 00810092
              03 COMM-RX10-CPROFIL           PIC X(5).                  00810093
              03 COMM-RX20-FONC              PIC X(3).                  00810094
              03 COMM-RX10-TRANS-SUP         PIC X(5).                  00810095
              03 COMM-RX10-TRANS-GEN         PIC X(5).                  00810096
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-RX10-PAGE              PIC S9(4) COMP.            00810097
      *--                                                                       
              03 COMM-RX10-PAGE              PIC S9(4) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-RX10-NBP               PIC S9(4) COMP.            00810098
      *--                                                                       
              03 COMM-RX10-NBP               PIC S9(4) COMP-5.                  
      *}                                                                        
              03 COMM-RX10-SUITE             PIC X(100).                00810099
      *                                                                 00810100
      * ZONES DONNEES SPECIFIQUES A LA TRANSACTION RX20 ---------- 127  00820000
      *                                                                 00830000
          02 COMM-RX20-MENU.                                            00840000
      *                                                                 00850000
              03 COMM-RX20-NREL              PIC X(7).                  01230020
      *                                                                 01230030
              03 COMM-RX20-DDEB              PIC X(8).                  01230060
      *                                                                 01230070
              03 COMM-RX20-TYPE              PIC X(1).                  01230082
      *                                                                 01230083
              03 COMM-RX20-DDEBUT-SAMJ       PIC X(8).                  01230090
      *                                                                 01230110
              03 COMM-RX20-DREEDIT-SAMJ      PIC X(8).                  01230120
      *                                                                 01230121
              03 COMM-RX20-ACID           PIC X(4).                     01230122
              03 COMM-RX20-NLIEU          PIC X(3).                     01230123
              03 COMM-RX20-USER    PIC X(1).                            01230124
      *                                                                 01230125
              03 COMM-RX20-CON-MIN PIC X(4).                            01230126
              03 COMM-RX20-CON-MAX PIC X(4).                            01230127
              03 COMM-RX20-ZONP-MIN PIC X(2).                           01230128
              03 COMM-RX20-ZONP-MAX PIC X(2).                           01230129
              03 COMM-RX20-TYPE-MIN PIC X.                              01230130
              03 COMM-RX20-TYPE-MAX   PIC X.                            01230131
              03 COMM-RX20-RELEVE-MIN PIC X(7).                         01230132
              03 COMM-RX20-RELEVE-MAX PIC X(7).                         01230133
              03 COMM-RX20-MAG-MIN    PIC X(3).                         01230134
              03 COMM-RX20-MAG-MAX    PIC X(3).                         01230135
              03 COMM-RX20-WJRX021    PIC X.                            01230136
              03 COMM-RX20-WJRX025    PIC X.                            01230137
      *                                                                 01230140
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3515  01230200
          02 COMM-RX20-APPLI.                                           01240000
      *------------------------------ ZONE COMMUNE                      01250000
             03 COMM-RX20-FILLER         PIC X(3515).                   01260394
      *---------------------------------------------                    01260800
          02 COMM-RX21-APPLI REDEFINES COMM-RX20-APPLI.                 01260900
             03 COMM-RX21-PAGE           PIC 9(4) COMP-3.               01261000
             03 COMM-RX21-PAGE-MAX       PIC 9(4) COMP-3.               01261100
             03 COMM-RX21-VALIDE-TRT     PIC X.                         01261110
             03 COMM-RX21-ERREUR-TRT     PIC X.                         01261120
             03 COMM-RX21-FILLER         PIC X(3505).                   01261200
      *----------------------------------------------                   01261300
          02 COMM-RX22-APPLI REDEFINES COMM-RX20-APPLI.                 01261400
             03 COMM-RX22-PAGE           PIC 9(4) COMP-3.               01261500
             03 COMM-RX22-PAGE-MAX       PIC 9(4) COMP-3.               01261600
             03 COMM-RX22-FILLER         PIC X(3507).                   01261900
      *----------------------------------------------                   01262000
          02 COMM-RX23-APPLI REDEFINES COMM-RX20-APPLI.                 01262100
             03 COMM-RX23-PAGE           PIC 9(2) COMP-3.               01262200
             03 COMM-RX23-PAGE-MAX       PIC 9(2) COMP-3.               01262300
             03 COMM-RX23-NON-TRAITEMENT PIC X.                         01262310
             03 COMM-RX23-LIMITE         PIC S9(4) COMP-3.              01262320
             03 COMM-RX23-RELEVE-MIN     PIC X(8).                      01262330
             03 COMM-RX23-RELEVE-MAX     PIC X(8).                      01262340
             03 COMM-RX23-DSYST          PIC S9(13) COMP-3.             01262340
             03 COMM-RX23-FILLER         PIC X(3483).                   01262400
      *----------------------------------------------                   01262500
          02 COMM-RX24-APPLI REDEFINES COMM-RX20-APPLI.                 01262600
             03 COMM-RX24-PAGE           PIC 9(2) COMP-3.               01262700
             03 COMM-RX24-PAGE-MAX       PIC 9(2) COMP-3.               01262800
             03 COMM-RX24-FILLER         PIC X(3511).                   01262900
      *----------------------------------------------                   01263000
          02 COMM-RX25-APPLI REDEFINES COMM-RX20-APPLI.                 01263100
             03 COMM-RX25-PAGE           PIC 9(2) COMP-3.               01263200
             03 COMM-RX25-PAGE-MAX       PIC 9(2) COMP-3.               01263300
             03 COMM-RX25-FILLER         PIC X(3511).                   01263400
      ***************************************************************** 01270000
                                                                                
