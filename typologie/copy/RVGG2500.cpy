      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGG2500                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGG2500                         
      **********************************************************                
       01  RVGG2500.                                                            
           02  GG25-CHEFPROD                                                    
               PIC X(0005).                                                     
           02  GG25-CFAM                                                        
               PIC X(0005).                                                     
           02  GG25-QNBDEMTOT                                                   
               PIC S9(5) COMP-3.                                                
           02  GG25-QNBDEMSUP7J                                                 
               PIC S9(5) COMP-3.                                                
           02  GG25-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGG2500                                  
      **********************************************************                
       01  RVGG2500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG25-CHEFPROD-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG25-CHEFPROD-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG25-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG25-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG25-QNBDEMTOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG25-QNBDEMTOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG25-QNBDEMSUP7J-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG25-QNBDEMSUP7J-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG25-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GG25-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
      *}                                                                        
