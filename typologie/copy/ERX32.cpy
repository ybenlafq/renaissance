      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERX34   ERX34                                              00000020
      ***************************************************************** 00000030
       01   ERX32I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(5).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNMAGI    PIC X(3).                                       00000250
           02 MNRELEVD OCCURS   14 TIMES .                              00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNRELEVL     COMP PIC S9(4).                            00000270
      *--                                                                       
             03 MNRELEVL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNRELEVF     PIC X.                                     00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MNRELEVI     PIC X(7).                                  00000300
           02 MNCONCD OCCURS   14 TIMES .                               00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCONCL      COMP PIC S9(4).                            00000320
      *--                                                                       
             03 MNCONCL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNCONCF      PIC X.                                     00000330
             03 FILLER  PIC X(4).                                       00000340
             03 MNCONCI      PIC X(4).                                  00000350
           02 MNENSD OCCURS   14 TIMES .                                00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNENSL  COMP PIC S9(4).                                 00000370
      *--                                                                       
             03 MNENSL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNENSF  PIC X.                                          00000380
             03 FILLER  PIC X(4).                                       00000390
             03 MNENSI  PIC X(14).                                      00000400
           02 MNNBRD OCCURS   14 TIMES .                                00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNNBRL  COMP PIC S9(4).                                 00000420
      *--                                                                       
             03 MNNBRL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNNBRF  PIC X.                                          00000430
             03 FILLER  PIC X(4).                                       00000440
             03 MNNBRI  PIC X(6).                                       00000450
           02 MD1SAIRD OCCURS   14 TIMES .                              00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MD1SAIRL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MD1SAIRL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MD1SAIRF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MD1SAIRI     PIC X(10).                                 00000500
           02 MCETARED OCCURS   14 TIMES .                              00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCETAREL     COMP PIC S9(4).                            00000520
      *--                                                                       
             03 MCETAREL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCETAREF     PIC X.                                     00000530
             03 FILLER  PIC X(4).                                       00000540
             03 MCETAREI     PIC X.                                     00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MLIBERRI  PIC X(78).                                      00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MCODTRAI  PIC X(4).                                       00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MCICSI    PIC X(5).                                       00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MNETNAMI  PIC X(8).                                       00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MSCREENI  PIC X(4).                                       00000750
      ***************************************************************** 00000760
      * SDF: ERX34   ERX34                                              00000770
      ***************************************************************** 00000780
       01   ERX32O REDEFINES ERX32I.                                    00000790
           02 FILLER    PIC X(12).                                      00000800
           02 FILLER    PIC X(2).                                       00000810
           02 MDATJOUA  PIC X.                                          00000820
           02 MDATJOUC  PIC X.                                          00000830
           02 MDATJOUP  PIC X.                                          00000840
           02 MDATJOUH  PIC X.                                          00000850
           02 MDATJOUV  PIC X.                                          00000860
           02 MDATJOUO  PIC X(10).                                      00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MTIMJOUA  PIC X.                                          00000890
           02 MTIMJOUC  PIC X.                                          00000900
           02 MTIMJOUP  PIC X.                                          00000910
           02 MTIMJOUH  PIC X.                                          00000920
           02 MTIMJOUV  PIC X.                                          00000930
           02 MTIMJOUO  PIC X(5).                                       00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MNPAGEA   PIC X.                                          00000960
           02 MNPAGEC   PIC X.                                          00000970
           02 MNPAGEP   PIC X.                                          00000980
           02 MNPAGEH   PIC X.                                          00000990
           02 MNPAGEV   PIC X.                                          00001000
           02 MNPAGEO   PIC X(5).                                       00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MNSOCA    PIC X.                                          00001030
           02 MNSOCC    PIC X.                                          00001040
           02 MNSOCP    PIC X.                                          00001050
           02 MNSOCH    PIC X.                                          00001060
           02 MNSOCV    PIC X.                                          00001070
           02 MNSOCO    PIC X(3).                                       00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MNMAGA    PIC X.                                          00001100
           02 MNMAGC    PIC X.                                          00001110
           02 MNMAGP    PIC X.                                          00001120
           02 MNMAGH    PIC X.                                          00001130
           02 MNMAGV    PIC X.                                          00001140
           02 MNMAGO    PIC X(3).                                       00001150
           02 DFHMS1 OCCURS   14 TIMES .                                00001160
             03 FILLER       PIC X(2).                                  00001170
             03 MNRELEVA     PIC X.                                     00001180
             03 MNRELEVC     PIC X.                                     00001190
             03 MNRELEVP     PIC X.                                     00001200
             03 MNRELEVH     PIC X.                                     00001210
             03 MNRELEVV     PIC X.                                     00001220
             03 MNRELEVO     PIC X(7).                                  00001230
           02 DFHMS2 OCCURS   14 TIMES .                                00001240
             03 FILLER       PIC X(2).                                  00001250
             03 MNCONCA      PIC X.                                     00001260
             03 MNCONCC PIC X.                                          00001270
             03 MNCONCP PIC X.                                          00001280
             03 MNCONCH PIC X.                                          00001290
             03 MNCONCV PIC X.                                          00001300
             03 MNCONCO      PIC X(4).                                  00001310
           02 DFHMS3 OCCURS   14 TIMES .                                00001320
             03 FILLER       PIC X(2).                                  00001330
             03 MNENSA  PIC X.                                          00001340
             03 MNENSC  PIC X.                                          00001350
             03 MNENSP  PIC X.                                          00001360
             03 MNENSH  PIC X.                                          00001370
             03 MNENSV  PIC X.                                          00001380
             03 MNENSO  PIC X(14).                                      00001390
           02 DFHMS4 OCCURS   14 TIMES .                                00001400
             03 FILLER       PIC X(2).                                  00001410
             03 MNNBRA  PIC X.                                          00001420
             03 MNNBRC  PIC X.                                          00001430
             03 MNNBRP  PIC X.                                          00001440
             03 MNNBRH  PIC X.                                          00001450
             03 MNNBRV  PIC X.                                          00001460
             03 MNNBRO  PIC X(6).                                       00001470
           02 DFHMS5 OCCURS   14 TIMES .                                00001480
             03 FILLER       PIC X(2).                                  00001490
             03 MD1SAIRA     PIC X.                                     00001500
             03 MD1SAIRC     PIC X.                                     00001510
             03 MD1SAIRP     PIC X.                                     00001520
             03 MD1SAIRH     PIC X.                                     00001530
             03 MD1SAIRV     PIC X.                                     00001540
             03 MD1SAIRO     PIC X(10).                                 00001550
           02 DFHMS6 OCCURS   14 TIMES .                                00001560
             03 FILLER       PIC X(2).                                  00001570
             03 MCETAREA     PIC X.                                     00001580
             03 MCETAREC     PIC X.                                     00001590
             03 MCETAREP     PIC X.                                     00001600
             03 MCETAREH     PIC X.                                     00001610
             03 MCETAREV     PIC X.                                     00001620
             03 MCETAREO     PIC X.                                     00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MLIBERRA  PIC X.                                          00001650
           02 MLIBERRC  PIC X.                                          00001660
           02 MLIBERRP  PIC X.                                          00001670
           02 MLIBERRH  PIC X.                                          00001680
           02 MLIBERRV  PIC X.                                          00001690
           02 MLIBERRO  PIC X(78).                                      00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MCODTRAA  PIC X.                                          00001720
           02 MCODTRAC  PIC X.                                          00001730
           02 MCODTRAP  PIC X.                                          00001740
           02 MCODTRAH  PIC X.                                          00001750
           02 MCODTRAV  PIC X.                                          00001760
           02 MCODTRAO  PIC X(4).                                       00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MCICSA    PIC X.                                          00001790
           02 MCICSC    PIC X.                                          00001800
           02 MCICSP    PIC X.                                          00001810
           02 MCICSH    PIC X.                                          00001820
           02 MCICSV    PIC X.                                          00001830
           02 MCICSO    PIC X(5).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MNETNAMA  PIC X.                                          00001860
           02 MNETNAMC  PIC X.                                          00001870
           02 MNETNAMP  PIC X.                                          00001880
           02 MNETNAMH  PIC X.                                          00001890
           02 MNETNAMV  PIC X.                                          00001900
           02 MNETNAMO  PIC X(8).                                       00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MSCREENA  PIC X.                                          00001930
           02 MSCREENC  PIC X.                                          00001940
           02 MSCREENP  PIC X.                                          00001950
           02 MSCREENH  PIC X.                                          00001960
           02 MSCREENV  PIC X.                                          00001970
           02 MSCREENO  PIC X(4).                                       00001980
                                                                                
