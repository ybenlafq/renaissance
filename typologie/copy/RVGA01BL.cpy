      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CILOT ILOT LOGITRANS                   *        
      *----------------------------------------------------------------*        
       01  RVGA01BL.                                                            
           05  CILOT-CTABLEG2    PIC X(15).                                     
           05  CILOT-CTABLEG2-REDEF REDEFINES CILOT-CTABLEG2.                   
               10  CILOT-CINSEE          PIC X(05).                             
               10  CILOT-CINSEE-N       REDEFINES CILOT-CINSEE                  
                                         PIC 9(05).                             
           05  CILOT-WTABLEG     PIC X(80).                                     
           05  CILOT-WTABLEG-REDEF  REDEFINES CILOT-WTABLEG.                    
               10  CILOT-FLAG            PIC X(01).                             
               10  CILOT-COMMUNE         PIC X(32).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01BL-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CILOT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CILOT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CILOT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CILOT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
