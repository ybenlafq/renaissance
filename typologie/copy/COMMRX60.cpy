      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
000010*-----------------------------------------------------------------        
000020*                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
000030*01  COM-RX60-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-RX60-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
000040*                                                                         
000050 01  Z-COMMAREA.                                                          
000060*                                                                         
000070* ZONES RESERVEES A AIDA -----------------------------------------        
000080     02 FILLER-COM-AIDA      PIC X(100).                                  
000090*                                                                         
000100* ZONES RESERVEES EN PROVENANCE DE CICS --------------------------        
000110     02 COMM-CICS-APPLID     PIC X(8).                                    
000120     02 COMM-CICS-NETNAM     PIC X(8).                                    
000130     02 COMM-CICS-TRANSA     PIC X(4).                                    
000140*                                                                         
000150* ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------------        
000160     02 COMM-DATE-SIECLE     PIC XX.                                      
000170     02 COMM-DATE-ANNEE      PIC XX.                                      
000180     02 COMM-DATE-MOIS       PIC XX.                                      
000190     02 COMM-DATE-JOUR       PIC 99.                                      
000200*   QUANTIEMES CALENDAIRE ET STANDARD                                     
000210     02 COMM-DATE-QNTA       PIC 999.                                     
000220     02 COMM-DATE-QNT0       PIC 99999.                                   
000230*   ANNEE BISSEXTILE 1=OUI 0=NON                                          
000240     02 COMM-DATE-BISX       PIC 9.                                       
000250*    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
000260     02 COMM-DATE-JSM        PIC 9.                                       
000270*   LIBELLES DU JOUR COURT - LONG                                         
000280     02 COMM-DATE-JSM-LC     PIC XXX.                                     
000290     02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                
000300*   LIBELLES DU MOIS COURT - LONG                                         
000310     02 COMM-DATE-MOIS-LC    PIC XXX.                                     
000320     02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                
000330*   DIFFERENTES FORMES DE DATE                                            
000340     02 COMM-DATE-SSAAMMJJ   PIC X(8).                                    
000350     02 COMM-DATE-AAMMJJ     PIC X(6).                                    
000360     02 COMM-DATE-JJMMSSAA   PIC X(8).                                    
000370     02 COMM-DATE-JJMMAA     PIC X(6).                                    
000380     02 COMM-DATE-JJ-MM-AA   PIC X(8).                                    
000390     02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
000400*   TRAITEMENT DU NUMERO DE SEMAINE                                       
000410     02 COMM-DATE-WEEK.                                                   
000420        05 COMM-DATE-SEMSS   PIC 99.                                      
000430        05 COMM-DATE-SEMAA   PIC 99.                                      
000440        05 COMM-DATE-SEMNU   PIC 99.                                      
000450*                                                                         
000460     02 COMM-DATE-FILLER     PIC X(08).                                   
000470*                                                                         
000480* ZONES RESERVEES TRAITEMENT DU SWAP -----------------------------        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
000490*    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                     
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
000500     02 COMM-SWAP-ATTR OCCURS 180 PIC X(1).                               
000510*                                                                         
000520*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*        
000530*>>>>>>>>>>>>>>>>> ZONES RESERVEES APPLICATIVES <<<<<<<<<<<<<<<<<*        
000540*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*        
000550*                                                                         
000560*-------------------------------------------------------3694-----*        
000570* MENU GENERAL DE L'APPLICATION (PROGRAMME TRX60)                *        
000580*----------------------------------------------------------------*        
000590     02 COMM-RX60-APPLI.                                                  
000600*--------------------------------------------------429----------*         
000610        03 COMM-RX60-SIEGE             PIC X.                             
000620        03 COMM-RX60-NSOCIETE          PIC X(3).                          
000630        03 COMM-RX60-NCODIC            PIC X(7).                          
000640        03 COMM-RX60-NCONC             PIC X(4).                          
000650        03 COMM-RX60-LCONC             PIC X(15).                         
000660        03 COMM-RX60-EMPCONC           PIC X(15).                         
000670        03 COMM-RX60-NMAG              PIC X(3).                          
000680        03 COMM-RX60-LMAG              PIC X(20).                         
000690        03 COMM-RX60-CFAM              PIC X(5).                          
000700        03 COMM-RX60-LFAM              PIC X(20).                         
000710        03 COMM-RX60-CMARQ             PIC X(5).                          
000720        03 COMM-RX60-LMARQ             PIC X(20).                         
000730        03 COMM-RX60-LREFFOURN         PIC X(20).                         
000740        03 COMM-RX60-NZONPRIX          PIC X(2).                          
000750        03 COMM-RX60-NATURE-MAG        PIC X(4).                          
000760        03 COMM-RX60-DRELEVE1          PIC X(10).                 01720001
000770        03 COMM-RX60-DRELEVE2          PIC X(10).                 01720001
000780        03 COMM-RX60-DTRTREL1          PIC X(10).                 01720001
000790        03 COMM-RX60-DTRTREL2          PIC X(10).                 01720001
000800        03 COMM-RX60-LCOMMENT1         PIC X(17).                 01720001
000810        03 COMM-RX60-LCOMMENT2         PIC X(17).                 01720001
000820        03 COMM-RX60-PRIX1             PIC X(9).                  01720001
000830        03 COMM-RX60-PRIX2             PIC X(9).                  01720001
000840        03 COMM-RX60-DRELEVE1-INIT     PIC X(10).                 01720001
000850        03 COMM-RX60-DRELEVE2-INIT     PIC X(10).                 01720001
000860        03 COMM-RX60-DTRTREL1-INIT     PIC X(10).                 01720001
000870        03 COMM-RX60-DTRTREL2-INIT     PIC X(10).                 01720001
000880        03 COMM-RX60-LCOMMENT1-INIT    PIC X(17).                 01720001
000890        03 COMM-RX60-LCOMMENT2-INIT    PIC X(17).                 01720001
000900        03 COMM-RX60-PRIX1-INIT        PIC X(9).                  01720001
000910        03 COMM-RX60-PRIX2-INIT        PIC X(9).                  01720001
000920        03 COMM-RX60-MODIFICATION      PIC X.                             
000930        03 COMM-RX60-TAB-RELEVE.                                          
000940           04 COMM-RX60-TAB OCCURS 2.                                     
000950              05 COMM-RX60-TAB-RX25 OCCURS 5.                             
000960                 06 COMM-RX60-NRELEVE     PIC X(7).                       
000970                 06 COMM-RX60-NMAGREL     PIC X(3).                       
000980*-------------------------------------------------------          00743701
000990*-------------------------------------------------------          00743701
001000        03 COMM-RX60-LIBRE          PIC X(2877).                  01720001
001010*-------------------------------------------------------          00743701
001020***************************************************************** 02170001
                                                                                
