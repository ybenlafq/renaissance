      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PLAFD QUANTITE PLAFONNEE               *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01F7.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01F7.                                                            
      *}                                                                        
           05  PLAFD-CTABLEG2    PIC X(15).                                     
           05  PLAFD-CTABLEG2-REDEF REDEFINES PLAFD-CTABLEG2.                   
               10  PLAFD-SOCIETE         PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  PLAFD-SOCIETE-N      REDEFINES PLAFD-SOCIETE                 
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  PLAFD-WTABLEG     PIC X(80).                                     
           05  PLAFD-WTABLEG-REDEF  REDEFINES PLAFD-WTABLEG.                    
               10  PLAFD-QTEPLAFD        PIC X(05).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  PLAFD-QTEPLAFD-N     REDEFINES PLAFD-QTEPLAFD                
                                         PIC 9(05).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01F7-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01F7-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PLAFD-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PLAFD-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PLAFD-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PLAFD-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
