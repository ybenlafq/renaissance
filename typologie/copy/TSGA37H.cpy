      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *===============================================================* 00010000
      *                                                               * 00020000
      *      TS ASSOCIE A LA TRANSACTION GA37                         * 00030000
      *            GESTION DE L'ECOTAXE                               * 00040000
      *                HISTORIQUE                                     * 00050000
      *===============================================================* 00060000
                                                                        00070000
       01 TS-GA37H.                                                     00080000
          05 TS-GA37H-LONG             PIC S9(5) COMP-3     VALUE +39.  00090006
          05 TS-GA37H-DONNEES.                                          00100000
      * DONNEES DE L'ECRAN                                              00110000
             10 TS-GA37H-CFAM          PIC  X(5).                       00130003
             10 TS-GA37H-CFOURN        PIC  X(5).                       00140003
             10 TS-GA37H-LFOURN        PIC  X(10).                      00141006
             10 TS-GA37H-WACHVEN       PIC  X.                          00150003
      *--                                                    SSAAMMJJ   00151005
             10 TS-GA37H-DEFFET        PIC  X(8).                       00160003
             10 TS-GA37H-CPAYS         PIC  X(2).                       00170003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      10 TS-GA37H-MONTANT       PIC  S9(5)V9(2) COMP.            00180004
      *                                                                         
      *--                                                                       
             10 TS-GA37H-MONTANT       PIC  S9(5)V9(2) COMP-5.                  
                                                                        00181000
      *}                                                                        
      *===============================================================* 00190000
                                                                                
