      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF:GESTION DES CONCURRENTS NAT.                                00000020
      ***************************************************************** 00000030
       01   EGN55I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(5).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(5).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(5).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(5).                                       00000200
           02 MPAGEMAXI      PIC X(4).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCONCNL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCONCNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCONCNF  PIC X.                                          00000230
           02 FILLER    PIC X(5).                                       00000240
           02 MNCONCNI  PIC X(4).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCONCNL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLCONCNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCONCNF  PIC X.                                          00000270
           02 FILLER    PIC X(5).                                       00000280
           02 MLCONCNI  PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGE2L   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MPAGE2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGE2F   PIC X.                                          00000310
           02 FILLER    PIC X(5).                                       00000320
           02 MPAGE2I   PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAX2L     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MPAGEMAX2L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MPAGEMAX2F     PIC X.                                     00000350
           02 FILLER    PIC X(5).                                       00000360
           02 MPAGEMAX2I     PIC X(4).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MANCONCNL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MANCONCNL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MANCONCNF      PIC X.                                     00000390
           02 FILLER    PIC X(5).                                       00000400
           02 MANCONCNI      PIC X(4).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MALCONCNL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MALCONCNL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MALCONCNF      PIC X.                                     00000430
           02 FILLER    PIC X(5).                                       00000440
           02 MALCONCNI      PIC X(20).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000470
           02 FILLER    PIC X(5).                                       00000480
           02 MNSOCI    PIC X(3).                                       00000490
           02 MACTD OCCURS   15 TIMES .                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTL   COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MACTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MACTF   PIC X.                                          00000520
             03 FILLER  PIC X(5).                                       00000530
             03 MACTI   PIC X.                                          00000540
           02 MNCONCD OCCURS   15 TIMES .                               00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCONCL      COMP PIC S9(4).                            00000560
      *--                                                                       
             03 MNCONCL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNCONCF      PIC X.                                     00000570
             03 FILLER  PIC X(5).                                       00000580
             03 MNCONCI      PIC X(4).                                  00000590
           02 MLCONCD OCCURS   15 TIMES .                               00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCONCL      COMP PIC S9(4).                            00000610
      *--                                                                       
             03 MLCONCL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLCONCF      PIC X.                                     00000620
             03 FILLER  PIC X(5).                                       00000630
             03 MLCONCI      PIC X(20).                                 00000640
           02 MNLIEUD OCCURS   15 TIMES .                               00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000660
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000670
             03 FILLER  PIC X(5).                                       00000680
             03 MNLIEUI      PIC X(3).                                  00000690
           02 MLLIEUD OCCURS   15 TIMES .                               00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLLIEUL      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MLLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLLIEUF      PIC X.                                     00000720
             03 FILLER  PIC X(5).                                       00000730
             03 MLLIEUI      PIC X(20).                                 00000740
           02 MNCONCLD OCCURS   15 TIMES .                              00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCONCLL     COMP PIC S9(4).                            00000760
      *--                                                                       
             03 MNCONCLL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCONCLF     PIC X.                                     00000770
             03 FILLER  PIC X(5).                                       00000780
             03 MNCONCLI     PIC X(4).                                  00000790
           02 MLCONCLD OCCURS   15 TIMES .                              00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCONCLL     COMP PIC S9(4).                            00000810
      *--                                                                       
             03 MLCONCLL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLCONCLF     PIC X.                                     00000820
             03 FILLER  PIC X(5).                                       00000830
             03 MLCONCLI     PIC X(15).                                 00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000850
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000860
           02 FILLER    PIC X(5).                                       00000870
           02 MLIBERRI  PIC X(78).                                      00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000890
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000900
           02 FILLER    PIC X(5).                                       00000910
           02 MCODTRAI  PIC X(4).                                       00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000930
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000940
           02 FILLER    PIC X(5).                                       00000950
           02 MCICSI    PIC X(5).                                       00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000980
           02 FILLER    PIC X(5).                                       00000990
           02 MNETNAMI  PIC X(8).                                       00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001010
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001020
           02 FILLER    PIC X(5).                                       00001030
           02 MSCREENI  PIC X(4).                                       00001040
      ***************************************************************** 00001050
      * SDF:GESTION DES CONCURRENTS NAT.                                00001060
      ***************************************************************** 00001070
       01   EGN55O REDEFINES EGN55I.                                    00001080
           02 FILLER    PIC X(12).                                      00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MDATJOUA  PIC X.                                          00001110
           02 MDATJOUC  PIC X.                                          00001120
           02 MDATJOUP  PIC X.                                          00001130
           02 MDATJOUH  PIC X.                                          00001140
           02 MDATJOUV  PIC X.                                          00001150
           02 MDATJOUU  PIC X.                                          00001160
           02 MDATJOUO  PIC X(10).                                      00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MTIMJOUA  PIC X.                                          00001190
           02 MTIMJOUC  PIC X.                                          00001200
           02 MTIMJOUP  PIC X.                                          00001210
           02 MTIMJOUH  PIC X.                                          00001220
           02 MTIMJOUV  PIC X.                                          00001230
           02 MTIMJOUU  PIC X.                                          00001240
           02 MTIMJOUO  PIC X(5).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MPAGEA    PIC X.                                          00001270
           02 MPAGEC    PIC X.                                          00001280
           02 MPAGEP    PIC X.                                          00001290
           02 MPAGEH    PIC X.                                          00001300
           02 MPAGEV    PIC X.                                          00001310
           02 MPAGEU    PIC X.                                          00001320
           02 MPAGEO    PIC X(3).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MPAGEMAXA      PIC X.                                     00001350
           02 MPAGEMAXC PIC X.                                          00001360
           02 MPAGEMAXP PIC X.                                          00001370
           02 MPAGEMAXH PIC X.                                          00001380
           02 MPAGEMAXV PIC X.                                          00001390
           02 MPAGEMAXU PIC X.                                          00001400
           02 MPAGEMAXO      PIC X(4).                                  00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MNCONCNA  PIC X.                                          00001430
           02 MNCONCNC  PIC X.                                          00001440
           02 MNCONCNP  PIC X.                                          00001450
           02 MNCONCNH  PIC X.                                          00001460
           02 MNCONCNV  PIC X.                                          00001470
           02 MNCONCNU  PIC X.                                          00001480
           02 MNCONCNO  PIC X(4).                                       00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MLCONCNA  PIC X.                                          00001510
           02 MLCONCNC  PIC X.                                          00001520
           02 MLCONCNP  PIC X.                                          00001530
           02 MLCONCNH  PIC X.                                          00001540
           02 MLCONCNV  PIC X.                                          00001550
           02 MLCONCNU  PIC X.                                          00001560
           02 MLCONCNO  PIC X(20).                                      00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MPAGE2A   PIC X.                                          00001590
           02 MPAGE2C   PIC X.                                          00001600
           02 MPAGE2P   PIC X.                                          00001610
           02 MPAGE2H   PIC X.                                          00001620
           02 MPAGE2V   PIC X.                                          00001630
           02 MPAGE2U   PIC X.                                          00001640
           02 MPAGE2O   PIC X(3).                                       00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MPAGEMAX2A     PIC X.                                     00001670
           02 MPAGEMAX2C     PIC X.                                     00001680
           02 MPAGEMAX2P     PIC X.                                     00001690
           02 MPAGEMAX2H     PIC X.                                     00001700
           02 MPAGEMAX2V     PIC X.                                     00001710
           02 MPAGEMAX2U     PIC X.                                     00001720
           02 MPAGEMAX2O     PIC X(4).                                  00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MANCONCNA      PIC X.                                     00001750
           02 MANCONCNC PIC X.                                          00001760
           02 MANCONCNP PIC X.                                          00001770
           02 MANCONCNH PIC X.                                          00001780
           02 MANCONCNV PIC X.                                          00001790
           02 MANCONCNU PIC X.                                          00001800
           02 MANCONCNO      PIC X(4).                                  00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MALCONCNA      PIC X.                                     00001830
           02 MALCONCNC PIC X.                                          00001840
           02 MALCONCNP PIC X.                                          00001850
           02 MALCONCNH PIC X.                                          00001860
           02 MALCONCNV PIC X.                                          00001870
           02 MALCONCNU PIC X.                                          00001880
           02 MALCONCNO      PIC X(20).                                 00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MNSOCA    PIC X.                                          00001910
           02 MNSOCC    PIC X.                                          00001920
           02 MNSOCP    PIC X.                                          00001930
           02 MNSOCH    PIC X.                                          00001940
           02 MNSOCV    PIC X.                                          00001950
           02 MNSOCU    PIC X.                                          00001960
           02 MNSOCO    PIC X(3).                                       00001970
           02 DFHMS1 OCCURS   15 TIMES .                                00001980
             03 FILLER       PIC X(2).                                  00001990
             03 MACTA   PIC X.                                          00002000
             03 MACTC   PIC X.                                          00002010
             03 MACTP   PIC X.                                          00002020
             03 MACTH   PIC X.                                          00002030
             03 MACTV   PIC X.                                          00002040
             03 MACTU   PIC X.                                          00002050
             03 MACTO   PIC X.                                          00002060
           02 DFHMS2 OCCURS   15 TIMES .                                00002070
             03 FILLER       PIC X(2).                                  00002080
             03 MNCONCA      PIC X.                                     00002090
             03 MNCONCC PIC X.                                          00002100
             03 MNCONCP PIC X.                                          00002110
             03 MNCONCH PIC X.                                          00002120
             03 MNCONCV PIC X.                                          00002130
             03 MNCONCU PIC X.                                          00002140
             03 MNCONCO      PIC X(4).                                  00002150
           02 DFHMS3 OCCURS   15 TIMES .                                00002160
             03 FILLER       PIC X(2).                                  00002170
             03 MLCONCA      PIC X.                                     00002180
             03 MLCONCC PIC X.                                          00002190
             03 MLCONCP PIC X.                                          00002200
             03 MLCONCH PIC X.                                          00002210
             03 MLCONCV PIC X.                                          00002220
             03 MLCONCU PIC X.                                          00002230
             03 MLCONCO      PIC X(20).                                 00002240
           02 DFHMS4 OCCURS   15 TIMES .                                00002250
             03 FILLER       PIC X(2).                                  00002260
             03 MNLIEUA      PIC X.                                     00002270
             03 MNLIEUC PIC X.                                          00002280
             03 MNLIEUP PIC X.                                          00002290
             03 MNLIEUH PIC X.                                          00002300
             03 MNLIEUV PIC X.                                          00002310
             03 MNLIEUU PIC X.                                          00002320
             03 MNLIEUO      PIC X(3).                                  00002330
           02 DFHMS5 OCCURS   15 TIMES .                                00002340
             03 FILLER       PIC X(2).                                  00002350
             03 MLLIEUA      PIC X.                                     00002360
             03 MLLIEUC PIC X.                                          00002370
             03 MLLIEUP PIC X.                                          00002380
             03 MLLIEUH PIC X.                                          00002390
             03 MLLIEUV PIC X.                                          00002400
             03 MLLIEUU PIC X.                                          00002410
             03 MLLIEUO      PIC X(20).                                 00002420
           02 DFHMS6 OCCURS   15 TIMES .                                00002430
             03 FILLER       PIC X(2).                                  00002440
             03 MNCONCLA     PIC X.                                     00002450
             03 MNCONCLC     PIC X.                                     00002460
             03 MNCONCLP     PIC X.                                     00002470
             03 MNCONCLH     PIC X.                                     00002480
             03 MNCONCLV     PIC X.                                     00002490
             03 MNCONCLU     PIC X.                                     00002500
             03 MNCONCLO     PIC X(4).                                  00002510
           02 DFHMS7 OCCURS   15 TIMES .                                00002520
             03 FILLER       PIC X(2).                                  00002530
             03 MLCONCLA     PIC X.                                     00002540
             03 MLCONCLC     PIC X.                                     00002550
             03 MLCONCLP     PIC X.                                     00002560
             03 MLCONCLH     PIC X.                                     00002570
             03 MLCONCLV     PIC X.                                     00002580
             03 MLCONCLU     PIC X.                                     00002590
             03 MLCONCLO     PIC X(15).                                 00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MLIBERRA  PIC X.                                          00002620
           02 MLIBERRC  PIC X.                                          00002630
           02 MLIBERRP  PIC X.                                          00002640
           02 MLIBERRH  PIC X.                                          00002650
           02 MLIBERRV  PIC X.                                          00002660
           02 MLIBERRU  PIC X.                                          00002670
           02 MLIBERRO  PIC X(78).                                      00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MCODTRAA  PIC X.                                          00002700
           02 MCODTRAC  PIC X.                                          00002710
           02 MCODTRAP  PIC X.                                          00002720
           02 MCODTRAH  PIC X.                                          00002730
           02 MCODTRAV  PIC X.                                          00002740
           02 MCODTRAU  PIC X.                                          00002750
           02 MCODTRAO  PIC X(4).                                       00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MCICSA    PIC X.                                          00002780
           02 MCICSC    PIC X.                                          00002790
           02 MCICSP    PIC X.                                          00002800
           02 MCICSH    PIC X.                                          00002810
           02 MCICSV    PIC X.                                          00002820
           02 MCICSU    PIC X.                                          00002830
           02 MCICSO    PIC X(5).                                       00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MNETNAMA  PIC X.                                          00002860
           02 MNETNAMC  PIC X.                                          00002870
           02 MNETNAMP  PIC X.                                          00002880
           02 MNETNAMH  PIC X.                                          00002890
           02 MNETNAMV  PIC X.                                          00002900
           02 MNETNAMU  PIC X.                                          00002910
           02 MNETNAMO  PIC X(8).                                       00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MSCREENA  PIC X.                                          00002940
           02 MSCREENC  PIC X.                                          00002950
           02 MSCREENP  PIC X.                                          00002960
           02 MSCREENH  PIC X.                                          00002970
           02 MSCREENV  PIC X.                                          00002980
           02 MSCREENU  PIC X.                                          00002990
           02 MSCREENO  PIC X(4).                                       00003000
                                                                                
