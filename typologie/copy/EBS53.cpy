      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESE52   ESE52                                              00000020
      ***************************************************************** 00000030
       01   EBS53I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSELECTL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MLSELECTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLSELECTF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MLSELECTI      PIC X(17).                                 00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSELECTL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MSELECTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSELECTF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MSELECTI  PIC X.                                          00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MWFONCI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCOFFREL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MNCOFFREL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCOFFREF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCOFFREI      PIC X(7).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOFFREL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MLCOFFREL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLCOFFREF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLCOFFREI      PIC X(20).                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCFAMI    PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLFAMI    PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPROFASSL     COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MCPROFASSL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCPROFASSF     PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCPROFASSI     PIC X(5).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPROFASSL     COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MLPROFASSL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLPROFASSF     PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLPROFASSI     PIC X(20).                                 00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCMARQI   PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLMARQI   PIC X(20).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MPAGEI    PIC X(3).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MPAGEMAXI      PIC X(3).                                  00000650
           02 MWTABLEI OCCURS   10 TIMES .                              00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSEQENTL    COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MNSEQENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNSEQENTF    PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNSEQENTI    PIC X(2).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELLEL    COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MLIBELLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBELLEF    PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MLIBELLEI    PIC X(34).                                 00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MBLOCSEQL    COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MBLOCSEQL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MBLOCSEQF    PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MBLOCSEQI    PIC X(3).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSEQENTREFL      COMP PIC S9(4).                       00000790
      *--                                                                       
             03 MNSEQENTREFL COMP-5 PIC S9(4).                                  
      *}                                                                        
             03 MNSEQENTREFF      PIC X.                                00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MNSEQENTREFI      PIC X(2).                             00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNENTITE2L   COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MNENTITE2L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNENTITE2F   PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MNENTITE2I   PIC X(7).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MDEFFETI     PIC X(8).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDFINEFFETL  COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MDFINEFFETL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MDFINEFFETF  PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MDFINEFFETI  PIC X(8).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MZONCMDI  PIC X(15).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MLIBERRI  PIC X(58).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCODTRAI  PIC X(4).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCICSI    PIC X(5).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MNETNAMI  PIC X(8).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MSCREENI  PIC X(4).                                       00001180
      ***************************************************************** 00001190
      * SDF: ESE52   ESE52                                              00001200
      ***************************************************************** 00001210
       01   EBS53O REDEFINES EBS53I.                                    00001220
           02 FILLER    PIC X(12).                                      00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MDATJOUA  PIC X.                                          00001250
           02 MDATJOUC  PIC X.                                          00001260
           02 MDATJOUP  PIC X.                                          00001270
           02 MDATJOUH  PIC X.                                          00001280
           02 MDATJOUV  PIC X.                                          00001290
           02 MDATJOUO  PIC X(10).                                      00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MTIMJOUA  PIC X.                                          00001320
           02 MTIMJOUC  PIC X.                                          00001330
           02 MTIMJOUP  PIC X.                                          00001340
           02 MTIMJOUH  PIC X.                                          00001350
           02 MTIMJOUV  PIC X.                                          00001360
           02 MTIMJOUO  PIC X(5).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MLSELECTA      PIC X.                                     00001390
           02 MLSELECTC PIC X.                                          00001400
           02 MLSELECTP PIC X.                                          00001410
           02 MLSELECTH PIC X.                                          00001420
           02 MLSELECTV PIC X.                                          00001430
           02 MLSELECTO      PIC X(17).                                 00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MSELECTA  PIC X.                                          00001460
           02 MSELECTC  PIC X.                                          00001470
           02 MSELECTP  PIC X.                                          00001480
           02 MSELECTH  PIC X.                                          00001490
           02 MSELECTV  PIC X.                                          00001500
           02 MSELECTO  PIC X.                                          00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MWFONCA   PIC X.                                          00001530
           02 MWFONCC   PIC X.                                          00001540
           02 MWFONCP   PIC X.                                          00001550
           02 MWFONCH   PIC X.                                          00001560
           02 MWFONCV   PIC X.                                          00001570
           02 MWFONCO   PIC X(3).                                       00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MNCOFFREA      PIC X.                                     00001600
           02 MNCOFFREC PIC X.                                          00001610
           02 MNCOFFREP PIC X.                                          00001620
           02 MNCOFFREH PIC X.                                          00001630
           02 MNCOFFREV PIC X.                                          00001640
           02 MNCOFFREO      PIC X(7).                                  00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MLCOFFREA      PIC X.                                     00001670
           02 MLCOFFREC PIC X.                                          00001680
           02 MLCOFFREP PIC X.                                          00001690
           02 MLCOFFREH PIC X.                                          00001700
           02 MLCOFFREV PIC X.                                          00001710
           02 MLCOFFREO      PIC X(20).                                 00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MCFAMA    PIC X.                                          00001740
           02 MCFAMC    PIC X.                                          00001750
           02 MCFAMP    PIC X.                                          00001760
           02 MCFAMH    PIC X.                                          00001770
           02 MCFAMV    PIC X.                                          00001780
           02 MCFAMO    PIC X(5).                                       00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MLFAMA    PIC X.                                          00001810
           02 MLFAMC    PIC X.                                          00001820
           02 MLFAMP    PIC X.                                          00001830
           02 MLFAMH    PIC X.                                          00001840
           02 MLFAMV    PIC X.                                          00001850
           02 MLFAMO    PIC X(20).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MCPROFASSA     PIC X.                                     00001880
           02 MCPROFASSC     PIC X.                                     00001890
           02 MCPROFASSP     PIC X.                                     00001900
           02 MCPROFASSH     PIC X.                                     00001910
           02 MCPROFASSV     PIC X.                                     00001920
           02 MCPROFASSO     PIC X(5).                                  00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MLPROFASSA     PIC X.                                     00001950
           02 MLPROFASSC     PIC X.                                     00001960
           02 MLPROFASSP     PIC X.                                     00001970
           02 MLPROFASSH     PIC X.                                     00001980
           02 MLPROFASSV     PIC X.                                     00001990
           02 MLPROFASSO     PIC X(20).                                 00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MCMARQA   PIC X.                                          00002020
           02 MCMARQC   PIC X.                                          00002030
           02 MCMARQP   PIC X.                                          00002040
           02 MCMARQH   PIC X.                                          00002050
           02 MCMARQV   PIC X.                                          00002060
           02 MCMARQO   PIC X(5).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MLMARQA   PIC X.                                          00002090
           02 MLMARQC   PIC X.                                          00002100
           02 MLMARQP   PIC X.                                          00002110
           02 MLMARQH   PIC X.                                          00002120
           02 MLMARQV   PIC X.                                          00002130
           02 MLMARQO   PIC X(20).                                      00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MPAGEA    PIC X.                                          00002160
           02 MPAGEC    PIC X.                                          00002170
           02 MPAGEP    PIC X.                                          00002180
           02 MPAGEH    PIC X.                                          00002190
           02 MPAGEV    PIC X.                                          00002200
           02 MPAGEO    PIC X(3).                                       00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MPAGEMAXA      PIC X.                                     00002230
           02 MPAGEMAXC PIC X.                                          00002240
           02 MPAGEMAXP PIC X.                                          00002250
           02 MPAGEMAXH PIC X.                                          00002260
           02 MPAGEMAXV PIC X.                                          00002270
           02 MPAGEMAXO      PIC X(3).                                  00002280
           02 MWTABLEO OCCURS   10 TIMES .                              00002290
             03 FILLER       PIC X(2).                                  00002300
             03 MNSEQENTA    PIC X.                                     00002310
             03 MNSEQENTC    PIC X.                                     00002320
             03 MNSEQENTP    PIC X.                                     00002330
             03 MNSEQENTH    PIC X.                                     00002340
             03 MNSEQENTV    PIC X.                                     00002350
             03 MNSEQENTO    PIC X(2).                                  00002360
             03 FILLER       PIC X(2).                                  00002370
             03 MLIBELLEA    PIC X.                                     00002380
             03 MLIBELLEC    PIC X.                                     00002390
             03 MLIBELLEP    PIC X.                                     00002400
             03 MLIBELLEH    PIC X.                                     00002410
             03 MLIBELLEV    PIC X.                                     00002420
             03 MLIBELLEO    PIC X(34).                                 00002430
             03 FILLER       PIC X(2).                                  00002440
             03 MBLOCSEQA    PIC X.                                     00002450
             03 MBLOCSEQC    PIC X.                                     00002460
             03 MBLOCSEQP    PIC X.                                     00002470
             03 MBLOCSEQH    PIC X.                                     00002480
             03 MBLOCSEQV    PIC X.                                     00002490
             03 MBLOCSEQO    PIC X(3).                                  00002500
             03 FILLER       PIC X(2).                                  00002510
             03 MNSEQENTREFA      PIC X.                                00002520
             03 MNSEQENTREFC PIC X.                                     00002530
             03 MNSEQENTREFP PIC X.                                     00002540
             03 MNSEQENTREFH PIC X.                                     00002550
             03 MNSEQENTREFV PIC X.                                     00002560
             03 MNSEQENTREFO      PIC X(2).                             00002570
             03 FILLER       PIC X(2).                                  00002580
             03 MNENTITE2A   PIC X.                                     00002590
             03 MNENTITE2C   PIC X.                                     00002600
             03 MNENTITE2P   PIC X.                                     00002610
             03 MNENTITE2H   PIC X.                                     00002620
             03 MNENTITE2V   PIC X.                                     00002630
             03 MNENTITE2O   PIC X(7).                                  00002640
             03 FILLER       PIC X(2).                                  00002650
             03 MDEFFETA     PIC X.                                     00002660
             03 MDEFFETC     PIC X.                                     00002670
             03 MDEFFETP     PIC X.                                     00002680
             03 MDEFFETH     PIC X.                                     00002690
             03 MDEFFETV     PIC X.                                     00002700
             03 MDEFFETO     PIC X(8).                                  00002710
             03 FILLER       PIC X(2).                                  00002720
             03 MDFINEFFETA  PIC X.                                     00002730
             03 MDFINEFFETC  PIC X.                                     00002740
             03 MDFINEFFETP  PIC X.                                     00002750
             03 MDFINEFFETH  PIC X.                                     00002760
             03 MDFINEFFETV  PIC X.                                     00002770
             03 MDFINEFFETO  PIC X(8).                                  00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MZONCMDA  PIC X.                                          00002800
           02 MZONCMDC  PIC X.                                          00002810
           02 MZONCMDP  PIC X.                                          00002820
           02 MZONCMDH  PIC X.                                          00002830
           02 MZONCMDV  PIC X.                                          00002840
           02 MZONCMDO  PIC X(15).                                      00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MLIBERRA  PIC X.                                          00002870
           02 MLIBERRC  PIC X.                                          00002880
           02 MLIBERRP  PIC X.                                          00002890
           02 MLIBERRH  PIC X.                                          00002900
           02 MLIBERRV  PIC X.                                          00002910
           02 MLIBERRO  PIC X(58).                                      00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MCODTRAA  PIC X.                                          00002940
           02 MCODTRAC  PIC X.                                          00002950
           02 MCODTRAP  PIC X.                                          00002960
           02 MCODTRAH  PIC X.                                          00002970
           02 MCODTRAV  PIC X.                                          00002980
           02 MCODTRAO  PIC X(4).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MCICSA    PIC X.                                          00003010
           02 MCICSC    PIC X.                                          00003020
           02 MCICSP    PIC X.                                          00003030
           02 MCICSH    PIC X.                                          00003040
           02 MCICSV    PIC X.                                          00003050
           02 MCICSO    PIC X(5).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MNETNAMA  PIC X.                                          00003080
           02 MNETNAMC  PIC X.                                          00003090
           02 MNETNAMP  PIC X.                                          00003100
           02 MNETNAMH  PIC X.                                          00003110
           02 MNETNAMV  PIC X.                                          00003120
           02 MNETNAMO  PIC X(8).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MSCREENA  PIC X.                                          00003150
           02 MSCREENC  PIC X.                                          00003160
           02 MSCREENP  PIC X.                                          00003170
           02 MSCREENH  PIC X.                                          00003180
           02 MSCREENV  PIC X.                                          00003190
           02 MSCREENO  PIC X(4).                                       00003200
                                                                                
