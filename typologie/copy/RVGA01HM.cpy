      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TPRET TYPE DE PRET ARTICLE             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01HM.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01HM.                                                            
      *}                                                                        
           05  TPRET-CTABLEG2    PIC X(15).                                     
           05  TPRET-CTABLEG2-REDEF REDEFINES TPRET-CTABLEG2.                   
               10  TPRET-NSOCIETE        PIC X(03).                             
               10  TPRET-TYPPRET         PIC X(01).                             
           05  TPRET-WTABLEG     PIC X(80).                                     
           05  TPRET-WTABLEG-REDEF  REDEFINES TPRET-WTABLEG.                    
               10  TPRET-LTYPPRET        PIC X(20).                             
               10  TPRET-ASSOCIAT        PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01HM-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01HM-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TPRET-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TPRET-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TPRET-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TPRET-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
