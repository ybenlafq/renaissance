      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE LIBCO COMMENTAIRES L�GAUX              *        
      *----------------------------------------------------------------*        
       01  RVLIBCO.                                                             
           05  LIBCO-CTABLEG2    PIC X(15).                                     
           05  LIBCO-CTABLEG2-REDEF REDEFINES LIBCO-CTABLEG2.                   
               10  LIBCO-CETAT           PIC X(07).                             
               10  LIBCO-CEMP            PIC X(01).                             
               10  LIBCO-NSEQ            PIC X(02).                             
               10  LIBCO-NSEQ-N         REDEFINES LIBCO-NSEQ                    
                                         PIC 9(02).                             
           05  LIBCO-WTABLEG     PIC X(80).                                     
           05  LIBCO-WTABLEG-REDEF  REDEFINES LIBCO-WTABLEG.                    
               10  LIBCO-COMMENT1        PIC X(55).                             
               10  LIBCO-WACTIF          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVLIBCO-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LIBCO-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  LIBCO-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LIBCO-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  LIBCO-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
