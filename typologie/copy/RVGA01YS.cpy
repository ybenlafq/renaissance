      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MD400 SELECTION LIEU EXTERNE / ETAT    *        
      *----------------------------------------------------------------*        
       01  RVGA01YS.                                                            
           05  MD400-CTABLEG2    PIC X(15).                                     
           05  MD400-CTABLEG2-REDEF REDEFINES MD400-CTABLEG2.                   
               10  MD400-CETAT           PIC X(10).                             
               10  MD400-LIEU            PIC X(03).                             
           05  MD400-WTABLEG     PIC X(80).                                     
           05  MD400-WTABLEG-REDEF  REDEFINES MD400-WTABLEG.                    
               10  MD400-WFLAG           PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01YS-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MD400-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MD400-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MD400-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MD400-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
