      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * ecran de parametrage des liens                                  00000020
      ***************************************************************** 00000030
       01   EBS14I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPENT1L     COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MCTYPENT1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTYPENT1F     PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCTYPENT1I     PIC X(2).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPENT1L     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MLTYPENT1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLTYPENT1F     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLTYPENT1I     PIC X(20).                                 00000210
           02 MCOLONNED OCCURS   3 TIMES .                              00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOLONNEL    COMP PIC S9(4).                            00000230
      *--                                                                       
             03 MCOLONNEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCOLONNEF    PIC X.                                     00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MCOLONNEI    PIC X(13).                                 00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MWFONCI   PIC X(3).                                       00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTITE1L     COMP PIC S9(4).                            00000310
      *--                                                                       
           02 MNENTITE1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNENTITE1F     PIC X.                                     00000320
           02 FILLER    PIC X(4).                                       00000330
           02 MNENTITE1I     PIC X(7).                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTITE1L     COMP PIC S9(4).                            00000350
      *--                                                                       
           02 MLENTITE1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLENTITE1F     PIC X.                                     00000360
           02 FILLER    PIC X(4).                                       00000370
           02 MLENTITE1I     PIC X(20).                                 00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTA1L    COMP PIC S9(4).                                 00000390
      *--                                                                       
           02 MSTA1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSTA1F    PIC X.                                          00000400
           02 FILLER    PIC X(4).                                       00000410
           02 MSTA1I    PIC X(3).                                       00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQ1L  COMP PIC S9(4).                                 00000430
      *--                                                                       
           02 MCMARQ1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMARQ1F  PIC X.                                          00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MCMARQ1I  PIC X(5).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQ1L  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MLMARQ1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLMARQ1F  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MLMARQ1I  PIC X(20).                                      00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAM1L   COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MCFAM1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAM1F   PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MCFAM1I   PIC X(5).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAM1L   COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MLFAM1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLFAM1F   PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MLFAM1I   PIC X(20).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MPAGEI    PIC X(3).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000630
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MPAGEMAXI      PIC X(3).                                  00000660
           02 MTABI OCCURS   11 TIMES .                                 00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MAFFL   COMP PIC S9(4).                                 00000680
      *--                                                                       
             03 MAFFL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MAFFF   PIC X.                                          00000690
             03 FILLER  PIC X(4).                                       00000700
             03 MAFFI   PIC X.                                          00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCLIENL      COMP PIC S9(4).                            00000720
      *--                                                                       
             03 MCLIENL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCLIENF      PIC X.                                     00000730
             03 FILLER  PIC X(4).                                       00000740
             03 MCLIENI      PIC X(2).                                  00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSEQENTL    COMP PIC S9(4).                            00000760
      *--                                                                       
             03 MNSEQENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNSEQENTF    PIC X.                                     00000770
             03 FILLER  PIC X(4).                                       00000780
             03 MNSEQENTI    PIC S9(3).                                 00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSEQL  COMP PIC S9(4).                                 00000800
      *--                                                                       
             03 MNSEQL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSEQF  PIC X.                                          00000810
             03 FILLER  PIC X(4).                                       00000820
             03 MNSEQI  PIC X(3).                                       00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPENT2L   COMP PIC S9(4).                            00000840
      *--                                                                       
             03 MCTYPENT2L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCTYPENT2F   PIC X.                                     00000850
             03 FILLER  PIC X(4).                                       00000860
             03 MCTYPENT2I   PIC X(2).                                  00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNENTITE2L   COMP PIC S9(4).                            00000880
      *--                                                                       
             03 MNENTITE2L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNENTITE2F   PIC X.                                     00000890
             03 FILLER  PIC X(4).                                       00000900
             03 MNENTITE2I   PIC X(7).                                  00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAM2L      COMP PIC S9(4).                            00000920
      *--                                                                       
             03 MCFAM2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCFAM2F      PIC X.                                     00000930
             03 FILLER  PIC X(4).                                       00000940
             03 MCFAM2I      PIC X(5).                                  00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQ2L     COMP PIC S9(4).                            00000960
      *--                                                                       
             03 MCMARQ2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCMARQ2F     PIC X.                                     00000970
             03 FILLER  PIC X(4).                                       00000980
             03 MCMARQ2I     PIC X(5).                                  00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLENTITE2L   COMP PIC S9(4).                            00001000
      *--                                                                       
             03 MLENTITE2L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLENTITE2F   PIC X.                                     00001010
             03 FILLER  PIC X(4).                                       00001020
             03 MLENTITE2I   PIC X(20).                                 00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTA2L  COMP PIC S9(4).                                 00001040
      *--                                                                       
             03 MSTA2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSTA2F  PIC X.                                          00001050
             03 FILLER  PIC X(4).                                       00001060
             03 MSTA2I  PIC X(3).                                       00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00001080
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00001090
             03 FILLER  PIC X(4).                                       00001100
             03 MDEFFETI     PIC X(6).                                  00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDFINEFFETL  COMP PIC S9(4).                            00001120
      *--                                                                       
             03 MDFINEFFETL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MDFINEFFETF  PIC X.                                     00001130
             03 FILLER  PIC X(4).                                       00001140
             03 MDFINEFFETI  PIC X(6).                                  00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001160
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001170
           02 FILLER    PIC X(4).                                       00001180
           02 MZONCMDI  PIC X(15).                                      00001190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001200
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001210
           02 FILLER    PIC X(4).                                       00001220
           02 MLIBERRI  PIC X(58).                                      00001230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001240
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001250
           02 FILLER    PIC X(4).                                       00001260
           02 MCODTRAI  PIC X(4).                                       00001270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001280
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001290
           02 FILLER    PIC X(4).                                       00001300
           02 MCICSI    PIC X(5).                                       00001310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001320
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001330
           02 FILLER    PIC X(4).                                       00001340
           02 MNETNAMI  PIC X(8).                                       00001350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001360
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001370
           02 FILLER    PIC X(4).                                       00001380
           02 MSCREENI  PIC X(4).                                       00001390
      ***************************************************************** 00001400
      * ecran de parametrage des liens                                  00001410
      ***************************************************************** 00001420
       01   EBS14O REDEFINES EBS14I.                                    00001430
           02 FILLER    PIC X(12).                                      00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MDATJOUA  PIC X.                                          00001460
           02 MDATJOUC  PIC X.                                          00001470
           02 MDATJOUP  PIC X.                                          00001480
           02 MDATJOUH  PIC X.                                          00001490
           02 MDATJOUV  PIC X.                                          00001500
           02 MDATJOUO  PIC X(10).                                      00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MTIMJOUA  PIC X.                                          00001530
           02 MTIMJOUC  PIC X.                                          00001540
           02 MTIMJOUP  PIC X.                                          00001550
           02 MTIMJOUH  PIC X.                                          00001560
           02 MTIMJOUV  PIC X.                                          00001570
           02 MTIMJOUO  PIC X(5).                                       00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MCTYPENT1A     PIC X.                                     00001600
           02 MCTYPENT1C     PIC X.                                     00001610
           02 MCTYPENT1P     PIC X.                                     00001620
           02 MCTYPENT1H     PIC X.                                     00001630
           02 MCTYPENT1V     PIC X.                                     00001640
           02 MCTYPENT1O     PIC X(2).                                  00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MLTYPENT1A     PIC X.                                     00001670
           02 MLTYPENT1C     PIC X.                                     00001680
           02 MLTYPENT1P     PIC X.                                     00001690
           02 MLTYPENT1H     PIC X.                                     00001700
           02 MLTYPENT1V     PIC X.                                     00001710
           02 MLTYPENT1O     PIC X(20).                                 00001720
           02 DFHMS1 OCCURS   3 TIMES .                                 00001730
             03 FILLER       PIC X(2).                                  00001740
             03 MCOLONNEA    PIC X.                                     00001750
             03 MCOLONNEC    PIC X.                                     00001760
             03 MCOLONNEP    PIC X.                                     00001770
             03 MCOLONNEH    PIC X.                                     00001780
             03 MCOLONNEV    PIC X.                                     00001790
             03 MCOLONNEO    PIC X(13).                                 00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MWFONCA   PIC X.                                          00001820
           02 MWFONCC   PIC X.                                          00001830
           02 MWFONCP   PIC X.                                          00001840
           02 MWFONCH   PIC X.                                          00001850
           02 MWFONCV   PIC X.                                          00001860
           02 MWFONCO   PIC X(3).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MNENTITE1A     PIC X.                                     00001890
           02 MNENTITE1C     PIC X.                                     00001900
           02 MNENTITE1P     PIC X.                                     00001910
           02 MNENTITE1H     PIC X.                                     00001920
           02 MNENTITE1V     PIC X.                                     00001930
           02 MNENTITE1O     PIC X(7).                                  00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MLENTITE1A     PIC X.                                     00001960
           02 MLENTITE1C     PIC X.                                     00001970
           02 MLENTITE1P     PIC X.                                     00001980
           02 MLENTITE1H     PIC X.                                     00001990
           02 MLENTITE1V     PIC X.                                     00002000
           02 MLENTITE1O     PIC X(20).                                 00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MSTA1A    PIC X.                                          00002030
           02 MSTA1C    PIC X.                                          00002040
           02 MSTA1P    PIC X.                                          00002050
           02 MSTA1H    PIC X.                                          00002060
           02 MSTA1V    PIC X.                                          00002070
           02 MSTA1O    PIC X(3).                                       00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MCMARQ1A  PIC X.                                          00002100
           02 MCMARQ1C  PIC X.                                          00002110
           02 MCMARQ1P  PIC X.                                          00002120
           02 MCMARQ1H  PIC X.                                          00002130
           02 MCMARQ1V  PIC X.                                          00002140
           02 MCMARQ1O  PIC X(5).                                       00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MLMARQ1A  PIC X.                                          00002170
           02 MLMARQ1C  PIC X.                                          00002180
           02 MLMARQ1P  PIC X.                                          00002190
           02 MLMARQ1H  PIC X.                                          00002200
           02 MLMARQ1V  PIC X.                                          00002210
           02 MLMARQ1O  PIC X(20).                                      00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MCFAM1A   PIC X.                                          00002240
           02 MCFAM1C   PIC X.                                          00002250
           02 MCFAM1P   PIC X.                                          00002260
           02 MCFAM1H   PIC X.                                          00002270
           02 MCFAM1V   PIC X.                                          00002280
           02 MCFAM1O   PIC X(5).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MLFAM1A   PIC X.                                          00002310
           02 MLFAM1C   PIC X.                                          00002320
           02 MLFAM1P   PIC X.                                          00002330
           02 MLFAM1H   PIC X.                                          00002340
           02 MLFAM1V   PIC X.                                          00002350
           02 MLFAM1O   PIC X(20).                                      00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MPAGEA    PIC X.                                          00002380
           02 MPAGEC    PIC X.                                          00002390
           02 MPAGEP    PIC X.                                          00002400
           02 MPAGEH    PIC X.                                          00002410
           02 MPAGEV    PIC X.                                          00002420
           02 MPAGEO    PIC X(3).                                       00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MPAGEMAXA      PIC X.                                     00002450
           02 MPAGEMAXC PIC X.                                          00002460
           02 MPAGEMAXP PIC X.                                          00002470
           02 MPAGEMAXH PIC X.                                          00002480
           02 MPAGEMAXV PIC X.                                          00002490
           02 MPAGEMAXO      PIC X(3).                                  00002500
           02 MTABO OCCURS   11 TIMES .                                 00002510
             03 FILLER       PIC X(2).                                  00002520
             03 MAFFA   PIC X.                                          00002530
             03 MAFFC   PIC X.                                          00002540
             03 MAFFP   PIC X.                                          00002550
             03 MAFFH   PIC X.                                          00002560
             03 MAFFV   PIC X.                                          00002570
             03 MAFFO   PIC X.                                          00002580
             03 FILLER       PIC X(2).                                  00002590
             03 MCLIENA      PIC X.                                     00002600
             03 MCLIENC PIC X.                                          00002610
             03 MCLIENP PIC X.                                          00002620
             03 MCLIENH PIC X.                                          00002630
             03 MCLIENV PIC X.                                          00002640
             03 MCLIENO      PIC X(2).                                  00002650
             03 FILLER       PIC X(2).                                  00002660
             03 MNSEQENTA    PIC X.                                     00002670
             03 MNSEQENTC    PIC X.                                     00002680
             03 MNSEQENTP    PIC X.                                     00002690
             03 MNSEQENTH    PIC X.                                     00002700
             03 MNSEQENTV    PIC X.                                     00002710
             03 MNSEQENTO    PIC S9(3).                                 00002720
             03 FILLER       PIC X(2).                                  00002730
             03 MNSEQA  PIC X.                                          00002740
             03 MNSEQC  PIC X.                                          00002750
             03 MNSEQP  PIC X.                                          00002760
             03 MNSEQH  PIC X.                                          00002770
             03 MNSEQV  PIC X.                                          00002780
             03 MNSEQO  PIC X(3).                                       00002790
             03 FILLER       PIC X(2).                                  00002800
             03 MCTYPENT2A   PIC X.                                     00002810
             03 MCTYPENT2C   PIC X.                                     00002820
             03 MCTYPENT2P   PIC X.                                     00002830
             03 MCTYPENT2H   PIC X.                                     00002840
             03 MCTYPENT2V   PIC X.                                     00002850
             03 MCTYPENT2O   PIC X(2).                                  00002860
             03 FILLER       PIC X(2).                                  00002870
             03 MNENTITE2A   PIC X.                                     00002880
             03 MNENTITE2C   PIC X.                                     00002890
             03 MNENTITE2P   PIC X.                                     00002900
             03 MNENTITE2H   PIC X.                                     00002910
             03 MNENTITE2V   PIC X.                                     00002920
             03 MNENTITE2O   PIC X(7).                                  00002930
             03 FILLER       PIC X(2).                                  00002940
             03 MCFAM2A      PIC X.                                     00002950
             03 MCFAM2C PIC X.                                          00002960
             03 MCFAM2P PIC X.                                          00002970
             03 MCFAM2H PIC X.                                          00002980
             03 MCFAM2V PIC X.                                          00002990
             03 MCFAM2O      PIC X(5).                                  00003000
             03 FILLER       PIC X(2).                                  00003010
             03 MCMARQ2A     PIC X.                                     00003020
             03 MCMARQ2C     PIC X.                                     00003030
             03 MCMARQ2P     PIC X.                                     00003040
             03 MCMARQ2H     PIC X.                                     00003050
             03 MCMARQ2V     PIC X.                                     00003060
             03 MCMARQ2O     PIC X(5).                                  00003070
             03 FILLER       PIC X(2).                                  00003080
             03 MLENTITE2A   PIC X.                                     00003090
             03 MLENTITE2C   PIC X.                                     00003100
             03 MLENTITE2P   PIC X.                                     00003110
             03 MLENTITE2H   PIC X.                                     00003120
             03 MLENTITE2V   PIC X.                                     00003130
             03 MLENTITE2O   PIC X(20).                                 00003140
             03 FILLER       PIC X(2).                                  00003150
             03 MSTA2A  PIC X.                                          00003160
             03 MSTA2C  PIC X.                                          00003170
             03 MSTA2P  PIC X.                                          00003180
             03 MSTA2H  PIC X.                                          00003190
             03 MSTA2V  PIC X.                                          00003200
             03 MSTA2O  PIC X(3).                                       00003210
             03 FILLER       PIC X(2).                                  00003220
             03 MDEFFETA     PIC X.                                     00003230
             03 MDEFFETC     PIC X.                                     00003240
             03 MDEFFETP     PIC X.                                     00003250
             03 MDEFFETH     PIC X.                                     00003260
             03 MDEFFETV     PIC X.                                     00003270
             03 MDEFFETO     PIC X(6).                                  00003280
             03 FILLER       PIC X(2).                                  00003290
             03 MDFINEFFETA  PIC X.                                     00003300
             03 MDFINEFFETC  PIC X.                                     00003310
             03 MDFINEFFETP  PIC X.                                     00003320
             03 MDFINEFFETH  PIC X.                                     00003330
             03 MDFINEFFETV  PIC X.                                     00003340
             03 MDFINEFFETO  PIC X(6).                                  00003350
           02 FILLER    PIC X(2).                                       00003360
           02 MZONCMDA  PIC X.                                          00003370
           02 MZONCMDC  PIC X.                                          00003380
           02 MZONCMDP  PIC X.                                          00003390
           02 MZONCMDH  PIC X.                                          00003400
           02 MZONCMDV  PIC X.                                          00003410
           02 MZONCMDO  PIC X(15).                                      00003420
           02 FILLER    PIC X(2).                                       00003430
           02 MLIBERRA  PIC X.                                          00003440
           02 MLIBERRC  PIC X.                                          00003450
           02 MLIBERRP  PIC X.                                          00003460
           02 MLIBERRH  PIC X.                                          00003470
           02 MLIBERRV  PIC X.                                          00003480
           02 MLIBERRO  PIC X(58).                                      00003490
           02 FILLER    PIC X(2).                                       00003500
           02 MCODTRAA  PIC X.                                          00003510
           02 MCODTRAC  PIC X.                                          00003520
           02 MCODTRAP  PIC X.                                          00003530
           02 MCODTRAH  PIC X.                                          00003540
           02 MCODTRAV  PIC X.                                          00003550
           02 MCODTRAO  PIC X(4).                                       00003560
           02 FILLER    PIC X(2).                                       00003570
           02 MCICSA    PIC X.                                          00003580
           02 MCICSC    PIC X.                                          00003590
           02 MCICSP    PIC X.                                          00003600
           02 MCICSH    PIC X.                                          00003610
           02 MCICSV    PIC X.                                          00003620
           02 MCICSO    PIC X(5).                                       00003630
           02 FILLER    PIC X(2).                                       00003640
           02 MNETNAMA  PIC X.                                          00003650
           02 MNETNAMC  PIC X.                                          00003660
           02 MNETNAMP  PIC X.                                          00003670
           02 MNETNAMH  PIC X.                                          00003680
           02 MNETNAMV  PIC X.                                          00003690
           02 MNETNAMO  PIC X(8).                                       00003700
           02 FILLER    PIC X(2).                                       00003710
           02 MSCREENA  PIC X.                                          00003720
           02 MSCREENC  PIC X.                                          00003730
           02 MSCREENP  PIC X.                                          00003740
           02 MSCREENH  PIC X.                                          00003750
           02 MSCREENV  PIC X.                                          00003760
           02 MSCREENO  PIC X(4).                                       00003770
                                                                                
