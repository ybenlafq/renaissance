      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PR930 ASSURANCES/AIGUILLAGE FICHIER    *        
      *----------------------------------------------------------------*        
       01  RVPR930.                                                             
           05  PR930-CTABLEG2    PIC X(15).                                     
           05  PR930-CTABLEG2-REDEF REDEFINES PR930-CTABLEG2.                   
               10  PR930-NENTCDE         PIC X(05).                             
           05  PR930-WTABLEG     PIC X(80).                                     
           05  PR930-WTABLEG-REDEF  REDEFINES PR930-WTABLEG.                    
               10  PR930-NFICHIER        PIC X(02).                             
               10  PR930-CPROGRAM        PIC X(08).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVPR930-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PR930-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PR930-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PR930-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PR930-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
