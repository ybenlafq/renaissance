      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MFL10 NLG : AUTORISATION UTILISATEUR   *        
      *----------------------------------------------------------------*        
       01  RVGA01WW.                                                            
           05  MFL10-CTABLEG2    PIC X(15).                                     
           05  MFL10-CTABLEG2-REDEF REDEFINES MFL10-CTABLEG2.                   
               10  MFL10-CACID           PIC X(07).                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  MFL10-WTABLEG     PIC X(80).                                     
       EXEC SQL END DECLARE SECTION END-EXEC.
           05  MFL10-WTABLEG-REDEF  REDEFINES MFL10-WTABLEG.                    
               10  MFL10-NSOCCICS        PIC X(03).                             
               10  MFL10-NSOCCICS-N     REDEFINES MFL10-NSOCCICS                
                                         PIC 9(03).                             
               10  MFL10-NSOCIETE        PIC X(03).                             
               10  MFL10-NSOCIETE-N     REDEFINES MFL10-NSOCIETE                
                                         PIC 9(03).                             
               10  MFL10-NLIEU           PIC X(03).                             
               10  MFL10-NLIEU-N        REDEFINES MFL10-NLIEU                   
                                         PIC 9(03).                             
               10  MFL10-CFLGMAG         PIC X(05).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01WW-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MFL10-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MFL10-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MFL10-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
           05  MFL10-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.
                                                                                
      *}                                                                        
