      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA50   EGA50                                              00000020
      ***************************************************************** 00000030
       01   EGA50I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MCFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNCODICI  PIC X(7).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLREFFOURNI    PIC X(20).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCFAMI    PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLFAMI    PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCMARQI   PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLMARQI   PIC X(20).                                      00000410
           02 MNOPTIONI OCCURS   15 TIMES .                             00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNOPTL  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MNOPTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNOPTF  PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNOPTI  PIC X(2).                                       00000460
           02 MLOPTIONI OCCURS   15 TIMES .                             00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLOPTL  COMP PIC S9(4).                                 00000480
      *--                                                                       
             03 MLOPTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLOPTF  PIC X.                                          00000490
             03 FILLER  PIC X(4).                                       00000500
             03 MLOPTI  PIC X(20).                                      00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCASSORTL      COMP PIC S9(4).                            00000520
      *--                                                                       
           02 MCASSORTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCASSORTF      PIC X.                                     00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MCASSORTI      PIC X(5).                                  00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLASSORTL      COMP PIC S9(4).                            00000560
      *--                                                                       
           02 MLASSORTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLASSORTF      PIC X.                                     00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MLASSORTI      PIC X(20).                                 00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPROL  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MCAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAPPROF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MCAPPROI  PIC X(5).                                       00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAPPROL  COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MLAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLAPPROF  PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MLAPPROI  PIC X(20).                                      00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPOL   COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MCEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXPOF   PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MCEXPOI   PIC X(5).                                       00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLEXPOL   COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MLEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLEXPOF   PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MLEXPOI   PIC X(20).                                      00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPOTL    COMP PIC S9(4).                            00000760
      *--                                                                       
           02 MNSOCDEPOTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNSOCDEPOTF    PIC X.                                     00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MNSOCDEPOTI    PIC X(3).                                  00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MNDEPOTI  PIC X(3).                                       00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MZONCMDI  PIC X(15).                                      00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MLIBERRI  PIC X(78).                                      00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MCODTRAI  PIC X(4).                                       00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MCICSI    PIC X(5).                                       00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MNETNAMI  PIC X(8).                                       00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001040
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MSCREENI  PIC X(4).                                       00001070
      ***************************************************************** 00001080
      * SDF: EGA50   EGA50                                              00001090
      ***************************************************************** 00001100
       01   EGA50O REDEFINES EGA50I.                                    00001110
           02 FILLER    PIC X(12).                                      00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MDATJOUA  PIC X.                                          00001140
           02 MDATJOUC  PIC X.                                          00001150
           02 MDATJOUP  PIC X.                                          00001160
           02 MDATJOUH  PIC X.                                          00001170
           02 MDATJOUV  PIC X.                                          00001180
           02 MDATJOUO  PIC X(10).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MTIMJOUA  PIC X.                                          00001210
           02 MTIMJOUC  PIC X.                                          00001220
           02 MTIMJOUP  PIC X.                                          00001230
           02 MTIMJOUH  PIC X.                                          00001240
           02 MTIMJOUV  PIC X.                                          00001250
           02 MTIMJOUO  PIC X(5).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MCFONCA   PIC X.                                          00001280
           02 MCFONCC   PIC X.                                          00001290
           02 MCFONCP   PIC X.                                          00001300
           02 MCFONCH   PIC X.                                          00001310
           02 MCFONCV   PIC X.                                          00001320
           02 MCFONCO   PIC X(3).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MNCODICA  PIC X.                                          00001350
           02 MNCODICC  PIC X.                                          00001360
           02 MNCODICP  PIC X.                                          00001370
           02 MNCODICH  PIC X.                                          00001380
           02 MNCODICV  PIC X.                                          00001390
           02 MNCODICO  PIC X(7).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MLREFFOURNA    PIC X.                                     00001420
           02 MLREFFOURNC    PIC X.                                     00001430
           02 MLREFFOURNP    PIC X.                                     00001440
           02 MLREFFOURNH    PIC X.                                     00001450
           02 MLREFFOURNV    PIC X.                                     00001460
           02 MLREFFOURNO    PIC X(20).                                 00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MCFAMA    PIC X.                                          00001490
           02 MCFAMC    PIC X.                                          00001500
           02 MCFAMP    PIC X.                                          00001510
           02 MCFAMH    PIC X.                                          00001520
           02 MCFAMV    PIC X.                                          00001530
           02 MCFAMO    PIC X(5).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MLFAMA    PIC X.                                          00001560
           02 MLFAMC    PIC X.                                          00001570
           02 MLFAMP    PIC X.                                          00001580
           02 MLFAMH    PIC X.                                          00001590
           02 MLFAMV    PIC X.                                          00001600
           02 MLFAMO    PIC X(20).                                      00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MCMARQA   PIC X.                                          00001630
           02 MCMARQC   PIC X.                                          00001640
           02 MCMARQP   PIC X.                                          00001650
           02 MCMARQH   PIC X.                                          00001660
           02 MCMARQV   PIC X.                                          00001670
           02 MCMARQO   PIC X(5).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MLMARQA   PIC X.                                          00001700
           02 MLMARQC   PIC X.                                          00001710
           02 MLMARQP   PIC X.                                          00001720
           02 MLMARQH   PIC X.                                          00001730
           02 MLMARQV   PIC X.                                          00001740
           02 MLMARQO   PIC X(20).                                      00001750
           02 MNOPTIONO OCCURS   15 TIMES .                             00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MNOPTA  PIC X.                                          00001780
             03 MNOPTC  PIC X.                                          00001790
             03 MNOPTP  PIC X.                                          00001800
             03 MNOPTH  PIC X.                                          00001810
             03 MNOPTV  PIC X.                                          00001820
             03 MNOPTO  PIC X(2).                                       00001830
           02 MLOPTIONO OCCURS   15 TIMES .                             00001840
             03 FILLER       PIC X(2).                                  00001850
             03 MLOPTA  PIC X.                                          00001860
             03 MLOPTC  PIC X.                                          00001870
             03 MLOPTP  PIC X.                                          00001880
             03 MLOPTH  PIC X.                                          00001890
             03 MLOPTV  PIC X.                                          00001900
             03 MLOPTO  PIC X(20).                                      00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MCASSORTA      PIC X.                                     00001930
           02 MCASSORTC PIC X.                                          00001940
           02 MCASSORTP PIC X.                                          00001950
           02 MCASSORTH PIC X.                                          00001960
           02 MCASSORTV PIC X.                                          00001970
           02 MCASSORTO      PIC X(5).                                  00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MLASSORTA      PIC X.                                     00002000
           02 MLASSORTC PIC X.                                          00002010
           02 MLASSORTP PIC X.                                          00002020
           02 MLASSORTH PIC X.                                          00002030
           02 MLASSORTV PIC X.                                          00002040
           02 MLASSORTO      PIC X(20).                                 00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MCAPPROA  PIC X.                                          00002070
           02 MCAPPROC  PIC X.                                          00002080
           02 MCAPPROP  PIC X.                                          00002090
           02 MCAPPROH  PIC X.                                          00002100
           02 MCAPPROV  PIC X.                                          00002110
           02 MCAPPROO  PIC X(5).                                       00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MLAPPROA  PIC X.                                          00002140
           02 MLAPPROC  PIC X.                                          00002150
           02 MLAPPROP  PIC X.                                          00002160
           02 MLAPPROH  PIC X.                                          00002170
           02 MLAPPROV  PIC X.                                          00002180
           02 MLAPPROO  PIC X(20).                                      00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MCEXPOA   PIC X.                                          00002210
           02 MCEXPOC   PIC X.                                          00002220
           02 MCEXPOP   PIC X.                                          00002230
           02 MCEXPOH   PIC X.                                          00002240
           02 MCEXPOV   PIC X.                                          00002250
           02 MCEXPOO   PIC X(5).                                       00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MLEXPOA   PIC X.                                          00002280
           02 MLEXPOC   PIC X.                                          00002290
           02 MLEXPOP   PIC X.                                          00002300
           02 MLEXPOH   PIC X.                                          00002310
           02 MLEXPOV   PIC X.                                          00002320
           02 MLEXPOO   PIC X(20).                                      00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MNSOCDEPOTA    PIC X.                                     00002350
           02 MNSOCDEPOTC    PIC X.                                     00002360
           02 MNSOCDEPOTP    PIC X.                                     00002370
           02 MNSOCDEPOTH    PIC X.                                     00002380
           02 MNSOCDEPOTV    PIC X.                                     00002390
           02 MNSOCDEPOTO    PIC X(3).                                  00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNDEPOTA  PIC X.                                          00002420
           02 MNDEPOTC  PIC X.                                          00002430
           02 MNDEPOTP  PIC X.                                          00002440
           02 MNDEPOTH  PIC X.                                          00002450
           02 MNDEPOTV  PIC X.                                          00002460
           02 MNDEPOTO  PIC X(3).                                       00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MZONCMDA  PIC X.                                          00002490
           02 MZONCMDC  PIC X.                                          00002500
           02 MZONCMDP  PIC X.                                          00002510
           02 MZONCMDH  PIC X.                                          00002520
           02 MZONCMDV  PIC X.                                          00002530
           02 MZONCMDO  PIC X(15).                                      00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MLIBERRA  PIC X.                                          00002560
           02 MLIBERRC  PIC X.                                          00002570
           02 MLIBERRP  PIC X.                                          00002580
           02 MLIBERRH  PIC X.                                          00002590
           02 MLIBERRV  PIC X.                                          00002600
           02 MLIBERRO  PIC X(78).                                      00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MCODTRAA  PIC X.                                          00002630
           02 MCODTRAC  PIC X.                                          00002640
           02 MCODTRAP  PIC X.                                          00002650
           02 MCODTRAH  PIC X.                                          00002660
           02 MCODTRAV  PIC X.                                          00002670
           02 MCODTRAO  PIC X(4).                                       00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MCICSA    PIC X.                                          00002700
           02 MCICSC    PIC X.                                          00002710
           02 MCICSP    PIC X.                                          00002720
           02 MCICSH    PIC X.                                          00002730
           02 MCICSV    PIC X.                                          00002740
           02 MCICSO    PIC X(5).                                       00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MNETNAMA  PIC X.                                          00002770
           02 MNETNAMC  PIC X.                                          00002780
           02 MNETNAMP  PIC X.                                          00002790
           02 MNETNAMH  PIC X.                                          00002800
           02 MNETNAMV  PIC X.                                          00002810
           02 MNETNAMO  PIC X(8).                                       00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MSCREENA  PIC X.                                          00002840
           02 MSCREENC  PIC X.                                          00002850
           02 MSCREENP  PIC X.                                          00002860
           02 MSCREENH  PIC X.                                          00002870
           02 MSCREENV  PIC X.                                          00002880
           02 MSCREENO  PIC X(4).                                       00002890
                                                                                
