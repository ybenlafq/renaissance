      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:13 >
      
      *****************************************************************
      *   COPY MESSAGE MQHI
      *
      *
      *****************************************************************
      *
      *
       01  WS-MQ10.
         02 WS-QUEUE.
               10   MQ10-MSGID         PIC    X(24).
               10   MQ10-CORRELID      PIC    X(24).
         02 WS-CODRET                  PIC    X(02).
         02 MESSAGE80.
           03 MESSAGE80-ENTETE.
               05   MES80-TYPE         PIC    X(03).
               05   MES80-NSOCMSG      PIC    X(03).
               05   MES80-NLIEUMSG     PIC    X(03).
               05   MES80-NSOCDST      PIC    X(03).
               05   MES80-NLIEUDST     PIC    X(03).
               05   MES80-NORD         PIC    9(08).
               05   MES80-LPROG        PIC    X(10).
               05   MES80-DJOUR        PIC    X(08).
               05   MES80-WSID         PIC    X(10).
               05   MES80-USER         PIC    X(10).
               05   MES80-CHRONO       PIC    9(07).
               05   MES80-NBRMSG       PIC    9(07).
               05   MES80-FILLER       PIC    X(30).
         02 MESSAGE80-DATA.
               05   MES80-NSOC         PIC    X(03).
               05   MES80-NLIEU        PIC    X(03).
               05   MES80-NBCDE        PIC    X(07).
               05   MES80-DET.
      *--TABLE RTGV02 : 1 OCCURENCE = 467 CAR-----------
                    07   MES80-ENR-GV02-A        PIC   X(467).
                    07   MES80-ENR-GV02-B        PIC   X(467).
                    07   MES80-ENR-GV02-C        PIC   X(467).
                    07   MES80-ENR-GV02-D        PIC   X(467).
      
