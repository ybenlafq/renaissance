      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVTH0100                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTH0100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTH0100.                                                            
      *}                                                                        
      *    *************************************************************        
      *                       NCODIC                                            
           10 TH01-NCODIC          PIC X(7).                                    
      *    *************************************************************        
      *                       COPCO                                             
           10 TH01-COPCO           PIC X(3).                                    
      *    *************************************************************        
      *                       DEFFET                                            
           10 TH01-DEFFET          PIC X(8).                                    
      *    *************************************************************        
      *                       DFINEFFET                                         
           10 TH01-DFINEFFET       PIC X(8).                                    
      *    *************************************************************        
      *                       DCRESYST                                          
           10 TH01-DCRESYST        PIC X(26).                                   
      *    *************************************************************        
      *                       DMAJSYST                                          
           10 TH01-DMAJSYST        PIC X(26).                                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 10      *        
      ******************************************************************        
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVTH0000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTH0100-FLAG.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTH0100-FLAG.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 TH01-NCODIC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 TH01-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 TH01-COPCO-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 TH01-COPCO-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 TH01-DEFFET-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 TH01-DEFFET-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 TH01-DFINEFFET-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 TH01-DFINEFFET-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 TH01-DCRESYST-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 TH01-DCRESYST-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 TH01-DMAJSYST-F      PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 TH01-DMAJSYST-F      PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
