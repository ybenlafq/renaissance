 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FGPER PERIODE D IMPUTATION GROUPE      *        
      *----------------------------------------------------------------*        
       01  RVGA01L7.                                                            
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  FGPER-CTABLEG2    PIC X(15).                                     
           05  FGPER-CTABLEG2-REDEF REDEFINES FGPER-CTABLEG2.                   
               10  FGPER-EXERCICE        PIC X(04).                             
           EXEC SQL END DECLARE SECTION END-EXEC.
               10  FGPER-EXERCICE-N     REDEFINES FGPER-EXERCICE                
                                         PIC 9(04).                             
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  FGPER-PERIODE         PIC X(03).                             
           EXEC SQL END DECLARE SECTION END-EXEC.
               10  FGPER-PERIODE-N      REDEFINES FGPER-PERIODE                 
                                         PIC 9(03).                             
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  FGPER-WTABLEG     PIC X(80).                                     
           05  FGPER-WTABLEG-REDEF  REDEFINES FGPER-WTABLEG.                    
               10  FGPER-DDEBUT          PIC X(08).                             
           EXEC SQL END DECLARE SECTION END-EXEC.
               10  FGPER-DDEBUT-N       REDEFINES FGPER-DDEBUT                  
                                         PIC 9(08).                             
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  FGPER-DFIN            PIC X(08).                             
           EXEC SQL END DECLARE SECTION END-EXEC.
               10  FGPER-DFIN-N         REDEFINES FGPER-DFIN                    
                                         PIC 9(08).                             
               10  FGPER-LPERIODE        PIC X(20).                             
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  FGPER-DFSAISIE        PIC X(08).                             
           EXEC SQL END DECLARE SECTION END-EXEC.
               10  FGPER-DFSAISIE-N     REDEFINES FGPER-DFSAISIE                
                                         PIC 9(08).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVGA01L7-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGPER-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FGPER-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGPER-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FGPER-WTABLEG-F   PIC S9(4) COMP-5.                              
      *}
           EXEC SQL END DECLARE SECTION END-EXEC.
 
                                                                                
