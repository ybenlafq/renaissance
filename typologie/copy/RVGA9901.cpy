      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COPY DE LA VUE RVGA9901                                                 
      ******************************************************************        
E0076 * DSA057 01/04/04 SUPPORT EVOLUTION KROWN                                 
      ******************************************************************        
      * DSA053 23/11/04 CORR SUPPORT EVOLUTION KROWN CLANG A 2 CAR              
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVGA9901.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVGA9901.                                                             
      *}                                                                        
           02 GA99-CNOMPGRM   PIC X(05).                                        
           02 GA99-NSEQERR    PIC X(04).                                        
           02 GA99-CERRUT     PIC X(04).                                        
           02 GA99-LIBERR     PIC X(53).                                        
           02 GA99-CLANG      PIC X(02).                                        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVGA9901-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVGA9901-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 GA99-CNOMPGRM-F  PIC S9(4) COMP.                                  
      *--                                                                       
           02 GA99-CNOMPGRM-F  PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 GA99-NSEQERR-F   PIC S9(4) COMP.                                  
      *--                                                                       
           02 GA99-NSEQERR-F   PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 GA99-CERRUT-F    PIC S9(4) COMP.                                  
      *--                                                                       
           02 GA99-CERRUT-F    PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 GA99-LIBERR-F    PIC S9(4) COMP.                                  
      *--                                                                       
           02 GA99-LIBERR-F    PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 GA99-CLANG-F     PIC S9(4) COMP.                                  
      *                                                                         
      *--                                                                       
           02 GA99-CLANG-F     PIC S9(4) COMP-5.                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
