      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE BASIG SIGNATAIRE DES BONS D ACHAT      *        
      *----------------------------------------------------------------*        
       01  RVGA01KA.                                                            
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  BASIG-CTABLEG2    PIC X(15).                                     
           05  BASIG-CTABLEG2-REDEF REDEFINES BASIG-CTABLEG2.                   
               10  BASIG-NSIGNAT         PIC X(01).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  BASIG-NSIGNAT-N      REDEFINES BASIG-NSIGNAT                 
                                         PIC 9(01).                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  BASIG-WTABLEG     PIC X(80).                                     
           05  BASIG-WTABLEG-REDEF  REDEFINES BASIG-WTABLEG.                    
               10  BASIG-WACTIF          PIC X(01).                             
               10  BASIG-LSIGNAT         PIC X(15).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01KA-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BASIG-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  BASIG-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BASIG-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  BASIG-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
