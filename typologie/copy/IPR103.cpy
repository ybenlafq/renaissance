      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPR103 AU 01/03/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,01,BI,A,                          *        
      *                           21,05,BI,A,                          *        
      *                           26,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPR103.                                                        
            05 NOMETAT-IPR103           PIC X(6) VALUE 'IPR103'.                
            05 RUPTURES-IPR103.                                                 
           10 IPR103-NSOCIETE           PIC X(03).                      007  003
           10 IPR103-CTYPPREST          PIC X(05).                      010  005
           10 IPR103-NENTCDE            PIC X(05).                      015  005
           10 IPR103-WMAJOPT            PIC X(01).                      020  001
           10 IPR103-CPRESTATION        PIC X(05).                      021  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPR103-SEQUENCE           PIC S9(04) COMP.                026  002
      *--                                                                       
           10 IPR103-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPR103.                                                   
           10 IPR103-CDEVISE            PIC X(06).                      028  006
           10 IPR103-LMAJOPT            PIC X(16).                      034  016
           10 IPR103-LPRESTATION        PIC X(20).                      050  020
           10 IPR103-CA                 PIC S9(09)      COMP-3.         070  005
           10 IPR103-CA-MOIS            PIC S9(09)      COMP-3.         075  005
           10 IPR103-CA-7               PIC S9(09)      COMP-3.         080  005
           10 IPR103-NBPREST            PIC S9(06)      COMP-3.         085  004
           10 IPR103-NBPREST-MOIS       PIC S9(06)      COMP-3.         089  004
           10 IPR103-NBPREST-7          PIC S9(06)      COMP-3.         093  004
           10 IPR103-PRMP               PIC S9(06)      COMP-3.         097  004
           10 IPR103-VOLNET             PIC S9(06)      COMP-3.         101  004
           10 IPR103-VOLNET-7           PIC S9(06)      COMP-3.         105  004
           10 IPR103-DVENTE             PIC X(08).                      109  008
            05 FILLER                      PIC X(396).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPR103-LONG           PIC S9(4)   COMP  VALUE +116.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPR103-LONG           PIC S9(4) COMP-5  VALUE +116.           
                                                                                
      *}                                                                        
