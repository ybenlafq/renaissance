      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      *         MAQUETTE COMMAREA STANDARD AIDA COBOL2             *            
      **************************************************************            
      *                                                                         
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET                    
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR          
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU                  
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                            
      *                                                                         
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                              
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES               
      * PAR AIDA                                                                
      *                                                                         
      *-----------------------------------------------------------------        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-GA40-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-GA40-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA -----------------------------------------        
           02 FILLER-COM-AIDA      PIC X(100).                                  
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS --------------------------        
           02 COMM-CICS-APPLID     PIC X(8).                                    
           02 COMM-CICS-NETNAM     PIC X(8).                                    
           02 COMM-CICS-TRANSA     PIC X(4).                                    
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------------        
           02 COMM-DATE-SIECLE     PIC XX.                                      
           02 COMM-DATE-ANNEE      PIC XX.                                      
           02 COMM-DATE-MOIS       PIC XX.                                      
           02 COMM-DATE-JOUR       PIC 99.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA       PIC 999.                                     
           02 COMM-DATE-QNT0       PIC 99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX       PIC 9.                                       
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM        PIC 9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC     PIC XXX.                                     
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC    PIC XXX.                                     
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                                    
           02 COMM-DATE-AAMMJJ     PIC X(6).                                    
           02 COMM-DATE-JJMMSSAA   PIC X(8).                                    
           02 COMM-DATE-JJMMAA     PIC X(6).                                    
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                                    
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS   PIC 99.                                      
              05 COMM-DATE-SEMAA   PIC 99.                                      
              05 COMM-DATE-SEMNU   PIC 99.                                      
      *                                                                         
           02 COMM-DATE-FILLER     PIC X(08).                                   
      *                                                                         
      * ZONES RESERVEES TRAITEMENT DU SWAP -----------------------------        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                     
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 180 PIC X(1).                               
      *                                                                         
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*        
      *>>>>>>>>>>>>>>>>> ZONES RESERVEES APPLICATIVES <<<<<<<<<<<<<<<<<*        
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*        
      *                                                                         
      *----------------------------------------------------------------*        
      * MENU GENERAL DE L'APPLICATION (PROGRAMME TGA40)                *        
      *----------------------------------------------------------------*        
           02 COMM-GA40-MENU.                                                   
      *                                                                         
      * OPTION 1 ------------------------------------------------------*        
      *..............................................GARANTIE                   
              03 COMM-GA40-CGAR           PIC X(5).                             
      *..............................................FAMILLE                    
              03 COMM-GA40-CFAM           PIC X(5).                             
      *                                                                         
      *..............................................TARIF                      
              03 COMM-GA40-CTAR           PIC X(5).                             
      *                                                                         
      *..............................................HISTO                      
              03 COMM-GA40-HIST           PIC X(1).                             
      *                                                                         
      *..............................................TOP PASSAGE                
              03 COMM-GA40-CODE-PASSAGE   PIC X  VALUE ' '.                     
                 88 COMM-GA40-TOPPASS            VALUE '1'.                     
      *                                                                         
      * OPTION 2 ------------------------------------------------------*        
      *..............................................CODE SECTEUR               
              03 COMM-GA40-SECTEUR        PIC X(5).                             
      *..............................................CODE DEPARTEMENT           
              03 COMM-GA40-DEPARTEMENT    PIC X(2).                             
      *                                                                         
      * OPTION 3 ------------------------------------------------------*        
      *..............................................ZONES SAISIES              
              03 COMM-GA40-FONCTION       PIC X(3).                             
              03 COMM-GA40-LCOMMUNE       PIC X(32).                            
              03 COMM-GA40-LNOMVOIE       PIC X(21).                            
              03 COMM-GA40-CTVOIE         PIC X(4).                             
      *..............................................ZONES SELECTEES DB2        
              03 COMM-GA40-LNOMPVOIE      PIC X(42).                            
              03 COMM-GA40-NVOIE          PIC 9(4).                             
              03 COMM-GA40-LISTCINSEE     OCCURS 50.                            
                 05 COMM-GA40-CINSEE      PIC X(5).                             
                 05 COMM-GA40-CILOT       PIC X(8).                             
                 05 COMM-GA40-CPARITE     PIC X(1).                             
      *                                                                         
      * OPTION 4 ------------------------------------------------------*        
      *..............................................ZONES SAISIES              
              03 COMM-GA40-OPTION4.                                             
                 05 COMM-GA40-CFAMMGI        PIC X(04).                         
                 05 COMM-GA40-LFAMMGI        PIC X(20).                         
                 05 COMM-GA40-CPSEMGI        PIC X(02).                         
                 05 COMM-GA40-CTVAMGI        PIC X(01).                         
                 05 COMM-GA40-CTEMGI         PIC X(01).                         
                 05 COMM-GA40-CBBMGI         PIC X(01).                         
                 05 COMM-GA40-CGRPMGI        PIC X(01).                         
                 05 COMM-GA40-CLIVMGI        PIC X(01).                         
                 05 COMM-GA40-CPRVMGI        PIC X(02).                         
                 05 COMM-GA40-CCQCMGI        PIC X(03).                         
                 05 COMM-GA40-CGARMGI        PIC X(02).                         
                 05 COMM-GA40-CDECMGI        PIC X(01).                         
                 05 COMM-GA40-CETQMGI        PIC X(02).                         
                 05 COMM-GA40-QTVMGI         PIC X(02).                         
                 05 COMM-GA40-QEXPMGI        PIC X(03).                         
                 05 COMM-GA40-LFAMCLTMGI     PIC X(25).                         
                 05 COMM-GA40-CRUB1MGI       PIC X(08).                         
                 05 COMM-GA40-CRUB2MGI       PIC X(08).                         
                 05 COMM-GA40-CRUB3MGI       PIC X(08).                         
                 05 COMM-GA40-CRUB4MGI       PIC X(08).                         
                 05 COMM-GA40-CRUB5MGI       PIC X(08).                         
                 05 COMM-GA40-CRUB6MGI       PIC X(08).                         
                 05 COMM-GA40-CREMVMGI       PIC X(01).                         
      *                                                                         
      *  POUR OPTION 2                                                          
      *..............................................DATE D'EFFET               
              03 COMM-GA40-CGCPLT            PIC X(5).                          
              03 COMM-GA40-DEFFET            PIC X(8).                          
      *                                                                         
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*        
      *>>>>>>>>>>>>>>>>>>>>>> OCTETS DISPONIBLES <<<<<<<<<<<<<<<<<<<<<<*        
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*        
      *                                                                         
ADHY  *    02 COMM-GA40-APPLI.                                                  
ADHY  *       03 COMM-GA40-FILLER         PIC X(3124).                          
ADHY       02 COMM-GA40-HYBRIS       PIC 9     VALUE 0.                         
ADHY          88 HYBRIS-DEMARRE      VALUE 1.                                   
ADHY          88 HYBRIS-PAS-DEMARRE  VALUE 0.                                   
ADHY       02 COMM-GA40-APPLI.                                                  
ADHY          03 COMM-GA40-FILLER         PIC X(3123).                          
                                                                                
