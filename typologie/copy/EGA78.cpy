      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA56   EGA56                                              00000020
      ***************************************************************** 00000030
       01   EGA78I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPOTL    COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNSOCDEPOTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNSOCDEPOTF    PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNSOCDEPOTI    PIC X(3).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNDEPOTI  PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MWFONCI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCODICI  PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLREFI    PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNFAMI    PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLFAMI    PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMARQL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNMARQF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNMARQI   PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLMARQI   PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLEMBALL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLEMBALL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLEMBALF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLEMBALI  PIC X(50).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCONDCL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCCONDCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCCONDCF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCCONDCI  PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCONDCL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLCONDCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCONDCF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLCONDCI  PIC X(20).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQCONDCL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MQCONDCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQCONDCF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MQCONDCI  PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTRANCHEL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MTRANCHEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTRANCHEF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MTRANCHEI      PIC X.                                     00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQRECEPL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MQRECEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQRECEPF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MQRECEPI  PIC X(5).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQPOIDSL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MQPOIDSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQPOIDSF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MQPOIDSI  PIC X(7).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQPOIDSDEL     COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MQPOIDSDEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQPOIDSDEF     PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MQPOIDSDEI     PIC X(7).                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQDESTOL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MQDESTOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQDESTOF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MQDESTOI  PIC X(5).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATUTL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MSTATUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTATUTF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MSTATUTI  PIC X.                                          00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQLGCCL   COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MQLGCCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MQLGCCF   PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MQLGCCI   PIC X(3).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQLGCDEL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MQLGCDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQLGCDEF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MQLGCDEI  PIC X(5).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQVTECCL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MQVTECCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQVTECCF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MQVTECCI  PIC X(5).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQPROFCL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MQPROFCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQPROFCF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MQPROFCI  PIC X(3).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQPROFDEL      COMP PIC S9(4).                            00001060
      *--                                                                       
           02 MQPROFDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQPROFDEF      PIC X.                                     00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MQPROFDEI      PIC X(5).                                  00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQECOL    COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MQECOL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MQECOF    PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MQECOI    PIC X(5).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQHAUTCL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MQHAUTCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQHAUTCF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MQHAUTCI  PIC X(3).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQHAUTDEL      COMP PIC S9(4).                            00001180
      *--                                                                       
           02 MQHAUTDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQHAUTDEF      PIC X.                                     00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MQHAUTDEI      PIC X(5).                                  00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNATDANGERL    COMP PIC S9(4).                            00001220
      *--                                                                       
           02 MNATDANGERL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNATDANGERF    PIC X.                                     00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MNATDANGERI    PIC X(6).                                  00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQPRODBOXL     COMP PIC S9(4).                            00001260
      *--                                                                       
           02 MQPRODBOXL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQPRODBOXF     PIC X.                                     00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MQPRODBOXI     PIC X(4).                                  00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MALIML    COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MALIML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MALIMF    PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MALIMI    PIC X.                                          00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIQUIDEL      COMP PIC S9(4).                            00001340
      *--                                                                       
           02 MLIQUIDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIQUIDEF      PIC X.                                     00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MLIQUIDEI      PIC X.                                     00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPEREMPTL      COMP PIC S9(4).                            00001380
      *--                                                                       
           02 MPEREMPTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPEREMPTF      PIC X.                                     00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MPEREMPTI      PIC X.                                     00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCUNITVL  COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MCUNITVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCUNITVF  PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MCUNITVI  PIC X(3).                                       00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLUNITVL  COMP PIC S9(4).                                 00001460
      *--                                                                       
           02 MLUNITVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLUNITVF  PIC X.                                          00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MLUNITVI  PIC X(20).                                      00001490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDELAIACCL     COMP PIC S9(4).                            00001500
      *--                                                                       
           02 MDELAIACCL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDELAIACCF     PIC X.                                     00001510
           02 FILLER    PIC X(4).                                       00001520
           02 MDELAIACCI     PIC X(3).                                  00001530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCUNITRL  COMP PIC S9(4).                                 00001540
      *--                                                                       
           02 MCUNITRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCUNITRF  PIC X.                                          00001550
           02 FILLER    PIC X(4).                                       00001560
           02 MCUNITRI  PIC X(3).                                       00001570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLUNITRL  COMP PIC S9(4).                                 00001580
      *--                                                                       
           02 MLUNITRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLUNITRF  PIC X.                                          00001590
           02 FILLER    PIC X(4).                                       00001600
           02 MLUNITRI  PIC X(20).                                      00001610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001620
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001630
           02 FILLER    PIC X(4).                                       00001640
           02 MZONCMDI  PIC X(15).                                      00001650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001660
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001670
           02 FILLER    PIC X(4).                                       00001680
           02 MLIBERRI  PIC X(58).                                      00001690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001700
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001710
           02 FILLER    PIC X(4).                                       00001720
           02 MCODTRAI  PIC X(4).                                       00001730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001740
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001750
           02 FILLER    PIC X(4).                                       00001760
           02 MCICSI    PIC X(5).                                       00001770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001780
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001790
           02 FILLER    PIC X(4).                                       00001800
           02 MNETNAMI  PIC X(8).                                       00001810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001820
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001830
           02 FILLER    PIC X(4).                                       00001840
           02 MSCREENI  PIC X(4).                                       00001850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBCOLL  COMP PIC S9(4).                                 00001860
      *--                                                                       
           02 MQNBCOLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQNBCOLF  PIC X.                                          00001870
           02 FILLER    PIC X(4).                                       00001880
           02 MQNBCOLI  PIC X(5).                                       00001890
      ***************************************************************** 00001900
      * SDF: EGA56   EGA56                                              00001910
      ***************************************************************** 00001920
       01   EGA78O REDEFINES EGA78I.                                    00001930
           02 FILLER    PIC X(12).                                      00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MDATJOUA  PIC X.                                          00001960
           02 MDATJOUC  PIC X.                                          00001970
           02 MDATJOUP  PIC X.                                          00001980
           02 MDATJOUH  PIC X.                                          00001990
           02 MDATJOUV  PIC X.                                          00002000
           02 MDATJOUO  PIC X(10).                                      00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MTIMJOUA  PIC X.                                          00002030
           02 MTIMJOUC  PIC X.                                          00002040
           02 MTIMJOUP  PIC X.                                          00002050
           02 MTIMJOUH  PIC X.                                          00002060
           02 MTIMJOUV  PIC X.                                          00002070
           02 MTIMJOUO  PIC X(5).                                       00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MNSOCDEPOTA    PIC X.                                     00002100
           02 MNSOCDEPOTC    PIC X.                                     00002110
           02 MNSOCDEPOTP    PIC X.                                     00002120
           02 MNSOCDEPOTH    PIC X.                                     00002130
           02 MNSOCDEPOTV    PIC X.                                     00002140
           02 MNSOCDEPOTO    PIC X(3).                                  00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MNDEPOTA  PIC X.                                          00002170
           02 MNDEPOTC  PIC X.                                          00002180
           02 MNDEPOTP  PIC X.                                          00002190
           02 MNDEPOTH  PIC X.                                          00002200
           02 MNDEPOTV  PIC X.                                          00002210
           02 MNDEPOTO  PIC X(3).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MWFONCA   PIC X.                                          00002240
           02 MWFONCC   PIC X.                                          00002250
           02 MWFONCP   PIC X.                                          00002260
           02 MWFONCH   PIC X.                                          00002270
           02 MWFONCV   PIC X.                                          00002280
           02 MWFONCO   PIC X(3).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MNCODICA  PIC X.                                          00002310
           02 MNCODICC  PIC X.                                          00002320
           02 MNCODICP  PIC X.                                          00002330
           02 MNCODICH  PIC X.                                          00002340
           02 MNCODICV  PIC X.                                          00002350
           02 MNCODICO  PIC X(7).                                       00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MLREFA    PIC X.                                          00002380
           02 MLREFC    PIC X.                                          00002390
           02 MLREFP    PIC X.                                          00002400
           02 MLREFH    PIC X.                                          00002410
           02 MLREFV    PIC X.                                          00002420
           02 MLREFO    PIC X(20).                                      00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MNFAMA    PIC X.                                          00002450
           02 MNFAMC    PIC X.                                          00002460
           02 MNFAMP    PIC X.                                          00002470
           02 MNFAMH    PIC X.                                          00002480
           02 MNFAMV    PIC X.                                          00002490
           02 MNFAMO    PIC X(5).                                       00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MLFAMA    PIC X.                                          00002520
           02 MLFAMC    PIC X.                                          00002530
           02 MLFAMP    PIC X.                                          00002540
           02 MLFAMH    PIC X.                                          00002550
           02 MLFAMV    PIC X.                                          00002560
           02 MLFAMO    PIC X(20).                                      00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MNMARQA   PIC X.                                          00002590
           02 MNMARQC   PIC X.                                          00002600
           02 MNMARQP   PIC X.                                          00002610
           02 MNMARQH   PIC X.                                          00002620
           02 MNMARQV   PIC X.                                          00002630
           02 MNMARQO   PIC X(5).                                       00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MLMARQA   PIC X.                                          00002660
           02 MLMARQC   PIC X.                                          00002670
           02 MLMARQP   PIC X.                                          00002680
           02 MLMARQH   PIC X.                                          00002690
           02 MLMARQV   PIC X.                                          00002700
           02 MLMARQO   PIC X(20).                                      00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MLEMBALA  PIC X.                                          00002730
           02 MLEMBALC  PIC X.                                          00002740
           02 MLEMBALP  PIC X.                                          00002750
           02 MLEMBALH  PIC X.                                          00002760
           02 MLEMBALV  PIC X.                                          00002770
           02 MLEMBALO  PIC X(50).                                      00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MCCONDCA  PIC X.                                          00002800
           02 MCCONDCC  PIC X.                                          00002810
           02 MCCONDCP  PIC X.                                          00002820
           02 MCCONDCH  PIC X.                                          00002830
           02 MCCONDCV  PIC X.                                          00002840
           02 MCCONDCO  PIC X(5).                                       00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MLCONDCA  PIC X.                                          00002870
           02 MLCONDCC  PIC X.                                          00002880
           02 MLCONDCP  PIC X.                                          00002890
           02 MLCONDCH  PIC X.                                          00002900
           02 MLCONDCV  PIC X.                                          00002910
           02 MLCONDCO  PIC X(20).                                      00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MQCONDCA  PIC X.                                          00002940
           02 MQCONDCC  PIC X.                                          00002950
           02 MQCONDCP  PIC X.                                          00002960
           02 MQCONDCH  PIC X.                                          00002970
           02 MQCONDCV  PIC X.                                          00002980
           02 MQCONDCO  PIC X(5).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MTRANCHEA      PIC X.                                     00003010
           02 MTRANCHEC PIC X.                                          00003020
           02 MTRANCHEP PIC X.                                          00003030
           02 MTRANCHEH PIC X.                                          00003040
           02 MTRANCHEV PIC X.                                          00003050
           02 MTRANCHEO      PIC X.                                     00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MQRECEPA  PIC X.                                          00003080
           02 MQRECEPC  PIC X.                                          00003090
           02 MQRECEPP  PIC X.                                          00003100
           02 MQRECEPH  PIC X.                                          00003110
           02 MQRECEPV  PIC X.                                          00003120
           02 MQRECEPO  PIC X(5).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MQPOIDSA  PIC X.                                          00003150
           02 MQPOIDSC  PIC X.                                          00003160
           02 MQPOIDSP  PIC X.                                          00003170
           02 MQPOIDSH  PIC X.                                          00003180
           02 MQPOIDSV  PIC X.                                          00003190
           02 MQPOIDSO  PIC X(7).                                       00003200
           02 FILLER    PIC X(2).                                       00003210
           02 MQPOIDSDEA     PIC X.                                     00003220
           02 MQPOIDSDEC     PIC X.                                     00003230
           02 MQPOIDSDEP     PIC X.                                     00003240
           02 MQPOIDSDEH     PIC X.                                     00003250
           02 MQPOIDSDEV     PIC X.                                     00003260
           02 MQPOIDSDEO     PIC X(7).                                  00003270
           02 FILLER    PIC X(2).                                       00003280
           02 MQDESTOA  PIC X.                                          00003290
           02 MQDESTOC  PIC X.                                          00003300
           02 MQDESTOP  PIC X.                                          00003310
           02 MQDESTOH  PIC X.                                          00003320
           02 MQDESTOV  PIC X.                                          00003330
           02 MQDESTOO  PIC X(5).                                       00003340
           02 FILLER    PIC X(2).                                       00003350
           02 MSTATUTA  PIC X.                                          00003360
           02 MSTATUTC  PIC X.                                          00003370
           02 MSTATUTP  PIC X.                                          00003380
           02 MSTATUTH  PIC X.                                          00003390
           02 MSTATUTV  PIC X.                                          00003400
           02 MSTATUTO  PIC X.                                          00003410
           02 FILLER    PIC X(2).                                       00003420
           02 MQLGCCA   PIC X.                                          00003430
           02 MQLGCCC   PIC X.                                          00003440
           02 MQLGCCP   PIC X.                                          00003450
           02 MQLGCCH   PIC X.                                          00003460
           02 MQLGCCV   PIC X.                                          00003470
           02 MQLGCCO   PIC X(3).                                       00003480
           02 FILLER    PIC X(2).                                       00003490
           02 MQLGCDEA  PIC X.                                          00003500
           02 MQLGCDEC  PIC X.                                          00003510
           02 MQLGCDEP  PIC X.                                          00003520
           02 MQLGCDEH  PIC X.                                          00003530
           02 MQLGCDEV  PIC X.                                          00003540
           02 MQLGCDEO  PIC X(5).                                       00003550
           02 FILLER    PIC X(2).                                       00003560
           02 MQVTECCA  PIC X.                                          00003570
           02 MQVTECCC  PIC X.                                          00003580
           02 MQVTECCP  PIC X.                                          00003590
           02 MQVTECCH  PIC X.                                          00003600
           02 MQVTECCV  PIC X.                                          00003610
           02 MQVTECCO  PIC X(5).                                       00003620
           02 FILLER    PIC X(2).                                       00003630
           02 MQPROFCA  PIC X.                                          00003640
           02 MQPROFCC  PIC X.                                          00003650
           02 MQPROFCP  PIC X.                                          00003660
           02 MQPROFCH  PIC X.                                          00003670
           02 MQPROFCV  PIC X.                                          00003680
           02 MQPROFCO  PIC X(3).                                       00003690
           02 FILLER    PIC X(2).                                       00003700
           02 MQPROFDEA      PIC X.                                     00003710
           02 MQPROFDEC PIC X.                                          00003720
           02 MQPROFDEP PIC X.                                          00003730
           02 MQPROFDEH PIC X.                                          00003740
           02 MQPROFDEV PIC X.                                          00003750
           02 MQPROFDEO      PIC X(5).                                  00003760
           02 FILLER    PIC X(2).                                       00003770
           02 MQECOA    PIC X.                                          00003780
           02 MQECOC    PIC X.                                          00003790
           02 MQECOP    PIC X.                                          00003800
           02 MQECOH    PIC X.                                          00003810
           02 MQECOV    PIC X.                                          00003820
           02 MQECOO    PIC X(5).                                       00003830
           02 FILLER    PIC X(2).                                       00003840
           02 MQHAUTCA  PIC X.                                          00003850
           02 MQHAUTCC  PIC X.                                          00003860
           02 MQHAUTCP  PIC X.                                          00003870
           02 MQHAUTCH  PIC X.                                          00003880
           02 MQHAUTCV  PIC X.                                          00003890
           02 MQHAUTCO  PIC X(3).                                       00003900
           02 FILLER    PIC X(2).                                       00003910
           02 MQHAUTDEA      PIC X.                                     00003920
           02 MQHAUTDEC PIC X.                                          00003930
           02 MQHAUTDEP PIC X.                                          00003940
           02 MQHAUTDEH PIC X.                                          00003950
           02 MQHAUTDEV PIC X.                                          00003960
           02 MQHAUTDEO      PIC X(5).                                  00003970
           02 FILLER    PIC X(2).                                       00003980
           02 MNATDANGERA    PIC X.                                     00003990
           02 MNATDANGERC    PIC X.                                     00004000
           02 MNATDANGERP    PIC X.                                     00004010
           02 MNATDANGERH    PIC X.                                     00004020
           02 MNATDANGERV    PIC X.                                     00004030
           02 MNATDANGERO    PIC X(6).                                  00004040
           02 FILLER    PIC X(2).                                       00004050
           02 MQPRODBOXA     PIC X.                                     00004060
           02 MQPRODBOXC     PIC X.                                     00004070
           02 MQPRODBOXP     PIC X.                                     00004080
           02 MQPRODBOXH     PIC X.                                     00004090
           02 MQPRODBOXV     PIC X.                                     00004100
           02 MQPRODBOXO     PIC X(4).                                  00004110
           02 FILLER    PIC X(2).                                       00004120
           02 MALIMA    PIC X.                                          00004130
           02 MALIMC    PIC X.                                          00004140
           02 MALIMP    PIC X.                                          00004150
           02 MALIMH    PIC X.                                          00004160
           02 MALIMV    PIC X.                                          00004170
           02 MALIMO    PIC X.                                          00004180
           02 FILLER    PIC X(2).                                       00004190
           02 MLIQUIDEA      PIC X.                                     00004200
           02 MLIQUIDEC PIC X.                                          00004210
           02 MLIQUIDEP PIC X.                                          00004220
           02 MLIQUIDEH PIC X.                                          00004230
           02 MLIQUIDEV PIC X.                                          00004240
           02 MLIQUIDEO      PIC X.                                     00004250
           02 FILLER    PIC X(2).                                       00004260
           02 MPEREMPTA      PIC X.                                     00004270
           02 MPEREMPTC PIC X.                                          00004280
           02 MPEREMPTP PIC X.                                          00004290
           02 MPEREMPTH PIC X.                                          00004300
           02 MPEREMPTV PIC X.                                          00004310
           02 MPEREMPTO      PIC X.                                     00004320
           02 FILLER    PIC X(2).                                       00004330
           02 MCUNITVA  PIC X.                                          00004340
           02 MCUNITVC  PIC X.                                          00004350
           02 MCUNITVP  PIC X.                                          00004360
           02 MCUNITVH  PIC X.                                          00004370
           02 MCUNITVV  PIC X.                                          00004380
           02 MCUNITVO  PIC X(3).                                       00004390
           02 FILLER    PIC X(2).                                       00004400
           02 MLUNITVA  PIC X.                                          00004410
           02 MLUNITVC  PIC X.                                          00004420
           02 MLUNITVP  PIC X.                                          00004430
           02 MLUNITVH  PIC X.                                          00004440
           02 MLUNITVV  PIC X.                                          00004450
           02 MLUNITVO  PIC X(20).                                      00004460
           02 FILLER    PIC X(2).                                       00004470
           02 MDELAIACCA     PIC X.                                     00004480
           02 MDELAIACCC     PIC X.                                     00004490
           02 MDELAIACCP     PIC X.                                     00004500
           02 MDELAIACCH     PIC X.                                     00004510
           02 MDELAIACCV     PIC X.                                     00004520
           02 MDELAIACCO     PIC X(3).                                  00004530
           02 FILLER    PIC X(2).                                       00004540
           02 MCUNITRA  PIC X.                                          00004550
           02 MCUNITRC  PIC X.                                          00004560
           02 MCUNITRP  PIC X.                                          00004570
           02 MCUNITRH  PIC X.                                          00004580
           02 MCUNITRV  PIC X.                                          00004590
           02 MCUNITRO  PIC X(3).                                       00004600
           02 FILLER    PIC X(2).                                       00004610
           02 MLUNITRA  PIC X.                                          00004620
           02 MLUNITRC  PIC X.                                          00004630
           02 MLUNITRP  PIC X.                                          00004640
           02 MLUNITRH  PIC X.                                          00004650
           02 MLUNITRV  PIC X.                                          00004660
           02 MLUNITRO  PIC X(20).                                      00004670
           02 FILLER    PIC X(2).                                       00004680
           02 MZONCMDA  PIC X.                                          00004690
           02 MZONCMDC  PIC X.                                          00004700
           02 MZONCMDP  PIC X.                                          00004710
           02 MZONCMDH  PIC X.                                          00004720
           02 MZONCMDV  PIC X.                                          00004730
           02 MZONCMDO  PIC X(15).                                      00004740
           02 FILLER    PIC X(2).                                       00004750
           02 MLIBERRA  PIC X.                                          00004760
           02 MLIBERRC  PIC X.                                          00004770
           02 MLIBERRP  PIC X.                                          00004780
           02 MLIBERRH  PIC X.                                          00004790
           02 MLIBERRV  PIC X.                                          00004800
           02 MLIBERRO  PIC X(58).                                      00004810
           02 FILLER    PIC X(2).                                       00004820
           02 MCODTRAA  PIC X.                                          00004830
           02 MCODTRAC  PIC X.                                          00004840
           02 MCODTRAP  PIC X.                                          00004850
           02 MCODTRAH  PIC X.                                          00004860
           02 MCODTRAV  PIC X.                                          00004870
           02 MCODTRAO  PIC X(4).                                       00004880
           02 FILLER    PIC X(2).                                       00004890
           02 MCICSA    PIC X.                                          00004900
           02 MCICSC    PIC X.                                          00004910
           02 MCICSP    PIC X.                                          00004920
           02 MCICSH    PIC X.                                          00004930
           02 MCICSV    PIC X.                                          00004940
           02 MCICSO    PIC X(5).                                       00004950
           02 FILLER    PIC X(2).                                       00004960
           02 MNETNAMA  PIC X.                                          00004970
           02 MNETNAMC  PIC X.                                          00004980
           02 MNETNAMP  PIC X.                                          00004990
           02 MNETNAMH  PIC X.                                          00005000
           02 MNETNAMV  PIC X.                                          00005010
           02 MNETNAMO  PIC X(8).                                       00005020
           02 FILLER    PIC X(2).                                       00005030
           02 MSCREENA  PIC X.                                          00005040
           02 MSCREENC  PIC X.                                          00005050
           02 MSCREENP  PIC X.                                          00005060
           02 MSCREENH  PIC X.                                          00005070
           02 MSCREENV  PIC X.                                          00005080
           02 MSCREENO  PIC X(4).                                       00005090
           02 FILLER    PIC X(2).                                       00005100
           02 MQNBCOLA  PIC X.                                          00005110
           02 MQNBCOLC  PIC X.                                          00005120
           02 MQNBCOLP  PIC X.                                          00005130
           02 MQNBCOLH  PIC X.                                          00005140
           02 MQNBCOLV  PIC X.                                          00005150
           02 MQNBCOLO  PIC X(5).                                       00005160
                                                                                
