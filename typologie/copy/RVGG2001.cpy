      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGG2001                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGG2001                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGG2001.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGG2001.                                                            
      *}                                                                        
           02  GG20-NCODIC                                                      
               PIC X(0007).                                                     
           02  GG20-NCONC                                                       
               PIC X(0004).                                                     
           02  GG20-PEXPTTC                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GG20-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GG20-NLIEU                                                       
               PIC X(0003).                                                     
           02  GG20-DEFFET                                                      
               PIC X(0008).                                                     
           02  GG20-DFINEFFET                                                   
               PIC X(0008).                                                     
           02  GG20-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GG20-WFINEFFET                                                   
               PIC X(0001).                                                     
           02  GG20-CORIG                                                       
               PIC X(0001).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGG2001                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGG2001-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGG2001-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG20-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG20-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG20-NCONC-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG20-NCONC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG20-PEXPTTC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG20-PEXPTTC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG20-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG20-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG20-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG20-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG20-DEFFET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG20-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG20-DFINEFFET-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG20-DFINEFFET-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG20-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG20-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG20-WFINEFFET-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG20-WFINEFFET-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG20-CORIG-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GG20-CORIG-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
