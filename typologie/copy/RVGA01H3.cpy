      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE VALLI LIBELLE DES LIEUX DE VALO        *        
      *----------------------------------------------------------------*        
       01  RVGA01H3.                                                            
           05  VALLI-CTABLEG2    PIC X(15).                                     
           05  VALLI-CTABLEG2-REDEF REDEFINES VALLI-CTABLEG2.                   
               10  VALLI-SOCLIE          PIC X(06).                             
           05  VALLI-WTABLEG     PIC X(80).                                     
           05  VALLI-WTABLEG-REDEF  REDEFINES VALLI-WTABLEG.                    
               10  VALLI-LIBELLIE        PIC X(20).                             
               10  VALLI-TYPELIEU        PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01H3-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  VALLI-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  VALLI-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  VALLI-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  VALLI-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
