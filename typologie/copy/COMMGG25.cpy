      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00340000
       01  Z-COMMAREA.                                                  00350000
      *                                                                 00360000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00370000
          02 FILLER-COM-AIDA      PIC X(100).                           00380000
      *                                                                 00390000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00400000
          02 COMM-CICS-APPLID     PIC X(8).                             00410000
          02 COMM-CICS-NETNAM     PIC X(8).                             00420000
          02 COMM-CICS-TRANSA     PIC X(4).                             00430000
      *                                                                 00440000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00450000
          02 COMM-DATE-SIECLE     PIC XX.                               00460000
          02 COMM-DATE-ANNEE      PIC XX.                               00470000
          02 COMM-DATE-MOIS       PIC XX.                               00480000
          02 COMM-DATE-JOUR       PIC XX.                               00490000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00500000
          02 COMM-DATE-QNTA       PIC 999.                              00510000
          02 COMM-DATE-QNT0       PIC 99999.                            00520000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00530000
          02 COMM-DATE-BISX       PIC 9.                                00540000
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00550000
          02 COMM-DATE-JSM        PIC 9.                                00560000
      *   LIBELLES DU JOUR COURT - LONG                                 00570000
          02 COMM-DATE-JSM-LC     PIC XXX.                              00580000
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                         00590000
      *   LIBELLES DU MOIS COURT - LONG                                 00600000
          02 COMM-DATE-MOIS-LC    PIC XXX.                              00610000
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                         00620000
      *   DIFFERENTES FORMES DE DATE                                    00630000
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                             00640000
          02 COMM-DATE-AAMMJJ     PIC X(6).                             00650000
          02 COMM-DATE-JJMMSSAA   PIC X(8).                             00660000
          02 COMM-DATE-JJMMAA     PIC X(6).                             00670000
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                             00680000
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                            00690000
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00690010
          02 COMM-DATE-WEEK.                                            00690020
             05  COMM-DATE-SEMSS  PIC 99.                               00690030
             05  COMM-DATE-SEMAA  PIC 99.                               00690040
             05  COMM-DATE-SEMNU  PIC 99.                               00690050
          02 COMM-DATE-FILLER     PIC X(08).                            00690060
      *                                                                 00720000
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00730000
      *                                                                 00740000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.              00750000
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                        00760000
      *                                                                 00810000
      * ZONES ECRAN GG25                                                        
          02 COMM-GG25-APPLI.                                                   
             03 COMM-GG25-FONCTION           PIC X(3).                  00860000
             03 COMM-GG25-ACID               PIC X(4).                  00860000
             03 COMM-GG25-SIEGE              PIC X(1).                  00860000
             03 COMM-GG25-NSOCIETE           PIC X(3).                  00860000
             03 COMM-GG25-NZONPRIX           PIC X(2).                  01090100
E0072        03 COMM-GG25-ZP-MAITRE          PIC X(2).                  01090100
             03 COMM-GG25-LZONPRIX           PIC X(20).                 01090100
             03 COMM-GG25-CFAM1              PIC X(5).                  00860000
             03 COMM-GG25-LFAM               PIC X(20).                 00860000
             03 COMM-GG25-NUMPAG             PIC  9(03).                00742901
             03 COMM-GG25-NBRPAG             PIC  9(03).                00743112
             03 COMM-GG25-MODIF-TS           PIC  X(01).                00743514
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GG25-IND-RECH-TS        PIC S9(04) COMP.           00745918
      *--                                                                       
             03 COMM-GG25-IND-RECH-TS        PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GG25-IND-RECH-ENC       PIC S9(04) COMP.           00745918
      *--                                                                       
             03 COMM-GG25-IND-RECH-ENC       PIC S9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GG25-IND-TS-MAX         PIC S9(04) COMP.           00747001
      *--                                                                       
             03 COMM-GG25-IND-TS-MAX         PIC S9(04) COMP-5.                 
      *}                                                                        
E0072        03 COMM-GG25-ZP-DUPCOEFF        PIC X(01).                 00747001
  "          03 COMM-GG25-ZP-DUPTOPE         PIC X(01).                 00747001
  "          03 COMM-GG25-LIBELLE1           PIC X(35).                 00747001
  "          03 COMM-GG25-LIBELLE2           PIC X(22).                 00747001
  "          03 COMM-GG25-LIBELLE3           PIC X(22).                 00747001
  "          03 COMM-GG25-LIBELLE4           PIC X(05).                 00747001
  "          03 COMM-GG25-LIBELLE5           PIC X(05).                 00747001
             03 COMM-GG25-DATAS OCCURS 12.                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         10  COMM-GG25-NUM-TS         PIC S9(04) COMP.                   
      *--                                                                       
                10  COMM-GG25-NUM-TS         PIC S9(04) COMP-5.                 
      *}                                                                        
                10  COMM-GG25-CFAM           PIC X(05).                         
                10  COMM-GG25-LCOMMENT       PIC X(20).                         
                10  COMM-GG25-TOPMAJ         PIC X(01).                         
                10  COMM-GG25-POURCENT       PIC X(05).                         
                10  COMM-GG25-DEF            PIC X(08).                         
E0072           10  COMM-GG25-DUPCOEFF       PIC X(01).                         
  "             10  COMM-GG25-DUPTOPE        PIC X(01).                         
             03 COMM-GG25-FILLER             PIC X(3044).                       
                                                                                
