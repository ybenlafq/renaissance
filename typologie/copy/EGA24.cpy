      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA24   EGA24                                              00000020
      ***************************************************************** 00000030
       01   EGA24I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOPAGL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNOPAGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNOPAGF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNOPAGI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMILLL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MCFAMILLL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCFAMILLF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCFAMILLI      PIC X(4).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMLIBL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MCFAMLIBL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCFAMLIBF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCFAMLIBI      PIC X(20).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARKET1L     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCMARKET1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCMARKET1F     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCMARKET1I     PIC X(4).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARKETL1L    COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MCMARKETL1L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCMARKETL1F    PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCMARKETL1I    PIC X(20).                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARKET2L     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MCMARKET2L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCMARKET2F     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCMARKET2I     PIC X(4).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARKETL2L    COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MCMARKETL2L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCMARKETL2F    PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCMARKETL2I    PIC X(20).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIMBRICLIBL    COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MIMBRICLIBL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MIMBRICLIBF    PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MIMBRICLIBI    PIC X(20).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARKET3L     COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MCMARKET3L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCMARKET3F     PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCMARKET3I     PIC X(4).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARKETL3L    COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MCMARKETL3L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCMARKETL3F    PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCMARKETL3I    PIC X(20).                                 00000530
           02 MPOSTEI OCCURS   14 TIMES .                               00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNOLIGL      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNOLIGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNOLIGF      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNOLIGI      PIC X(2).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MAGRELIBL    COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MAGRELIBL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MAGRELIBF    PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MAGRELIBI    PIC X(20).                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MZONCMDI  PIC X(15).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLIBERRI  PIC X(58).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCODTRAI  PIC X(4).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCICSI    PIC X(5).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MNETNAMI  PIC X(8).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MSCREENI  PIC X(4).                                       00000860
      ***************************************************************** 00000870
      * SDF: EGA24   EGA24                                              00000880
      ***************************************************************** 00000890
       01   EGA24O REDEFINES EGA24I.                                    00000900
           02 FILLER    PIC X(12).                                      00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MDATJOUA  PIC X.                                          00000930
           02 MDATJOUC  PIC X.                                          00000940
           02 MDATJOUP  PIC X.                                          00000950
           02 MDATJOUH  PIC X.                                          00000960
           02 MDATJOUV  PIC X.                                          00000970
           02 MDATJOUO  PIC X(10).                                      00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MTIMJOUA  PIC X.                                          00001000
           02 MTIMJOUC  PIC X.                                          00001010
           02 MTIMJOUP  PIC X.                                          00001020
           02 MTIMJOUH  PIC X.                                          00001030
           02 MTIMJOUV  PIC X.                                          00001040
           02 MTIMJOUO  PIC X(5).                                       00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MNOPAGA   PIC X.                                          00001070
           02 MNOPAGC   PIC X.                                          00001080
           02 MNOPAGP   PIC X.                                          00001090
           02 MNOPAGH   PIC X.                                          00001100
           02 MNOPAGV   PIC X.                                          00001110
           02 MNOPAGO   PIC X(3).                                       00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MCFAMILLA      PIC X.                                     00001140
           02 MCFAMILLC PIC X.                                          00001150
           02 MCFAMILLP PIC X.                                          00001160
           02 MCFAMILLH PIC X.                                          00001170
           02 MCFAMILLV PIC X.                                          00001180
           02 MCFAMILLO      PIC X(4).                                  00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MCFAMLIBA      PIC X.                                     00001210
           02 MCFAMLIBC PIC X.                                          00001220
           02 MCFAMLIBP PIC X.                                          00001230
           02 MCFAMLIBH PIC X.                                          00001240
           02 MCFAMLIBV PIC X.                                          00001250
           02 MCFAMLIBO      PIC X(20).                                 00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MCMARKET1A     PIC X.                                     00001280
           02 MCMARKET1C     PIC X.                                     00001290
           02 MCMARKET1P     PIC X.                                     00001300
           02 MCMARKET1H     PIC X.                                     00001310
           02 MCMARKET1V     PIC X.                                     00001320
           02 MCMARKET1O     PIC X(4).                                  00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MCMARKETL1A    PIC X.                                     00001350
           02 MCMARKETL1C    PIC X.                                     00001360
           02 MCMARKETL1P    PIC X.                                     00001370
           02 MCMARKETL1H    PIC X.                                     00001380
           02 MCMARKETL1V    PIC X.                                     00001390
           02 MCMARKETL1O    PIC X(20).                                 00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MCMARKET2A     PIC X.                                     00001420
           02 MCMARKET2C     PIC X.                                     00001430
           02 MCMARKET2P     PIC X.                                     00001440
           02 MCMARKET2H     PIC X.                                     00001450
           02 MCMARKET2V     PIC X.                                     00001460
           02 MCMARKET2O     PIC X(4).                                  00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MCMARKETL2A    PIC X.                                     00001490
           02 MCMARKETL2C    PIC X.                                     00001500
           02 MCMARKETL2P    PIC X.                                     00001510
           02 MCMARKETL2H    PIC X.                                     00001520
           02 MCMARKETL2V    PIC X.                                     00001530
           02 MCMARKETL2O    PIC X(20).                                 00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MIMBRICLIBA    PIC X.                                     00001560
           02 MIMBRICLIBC    PIC X.                                     00001570
           02 MIMBRICLIBP    PIC X.                                     00001580
           02 MIMBRICLIBH    PIC X.                                     00001590
           02 MIMBRICLIBV    PIC X.                                     00001600
           02 MIMBRICLIBO    PIC X(20).                                 00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MCMARKET3A     PIC X.                                     00001630
           02 MCMARKET3C     PIC X.                                     00001640
           02 MCMARKET3P     PIC X.                                     00001650
           02 MCMARKET3H     PIC X.                                     00001660
           02 MCMARKET3V     PIC X.                                     00001670
           02 MCMARKET3O     PIC X(4).                                  00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MCMARKETL3A    PIC X.                                     00001700
           02 MCMARKETL3C    PIC X.                                     00001710
           02 MCMARKETL3P    PIC X.                                     00001720
           02 MCMARKETL3H    PIC X.                                     00001730
           02 MCMARKETL3V    PIC X.                                     00001740
           02 MCMARKETL3O    PIC X(20).                                 00001750
           02 MPOSTEO OCCURS   14 TIMES .                               00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MNOLIGA      PIC X.                                     00001780
             03 MNOLIGC PIC X.                                          00001790
             03 MNOLIGP PIC X.                                          00001800
             03 MNOLIGH PIC X.                                          00001810
             03 MNOLIGV PIC X.                                          00001820
             03 MNOLIGO      PIC X(2).                                  00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MAGRELIBA    PIC X.                                     00001850
             03 MAGRELIBC    PIC X.                                     00001860
             03 MAGRELIBP    PIC X.                                     00001870
             03 MAGRELIBH    PIC X.                                     00001880
             03 MAGRELIBV    PIC X.                                     00001890
             03 MAGRELIBO    PIC X(20).                                 00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MZONCMDA  PIC X.                                          00001920
           02 MZONCMDC  PIC X.                                          00001930
           02 MZONCMDP  PIC X.                                          00001940
           02 MZONCMDH  PIC X.                                          00001950
           02 MZONCMDV  PIC X.                                          00001960
           02 MZONCMDO  PIC X(15).                                      00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MLIBERRA  PIC X.                                          00001990
           02 MLIBERRC  PIC X.                                          00002000
           02 MLIBERRP  PIC X.                                          00002010
           02 MLIBERRH  PIC X.                                          00002020
           02 MLIBERRV  PIC X.                                          00002030
           02 MLIBERRO  PIC X(58).                                      00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCODTRAA  PIC X.                                          00002060
           02 MCODTRAC  PIC X.                                          00002070
           02 MCODTRAP  PIC X.                                          00002080
           02 MCODTRAH  PIC X.                                          00002090
           02 MCODTRAV  PIC X.                                          00002100
           02 MCODTRAO  PIC X(4).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MCICSA    PIC X.                                          00002130
           02 MCICSC    PIC X.                                          00002140
           02 MCICSP    PIC X.                                          00002150
           02 MCICSH    PIC X.                                          00002160
           02 MCICSV    PIC X.                                          00002170
           02 MCICSO    PIC X(5).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MNETNAMA  PIC X.                                          00002200
           02 MNETNAMC  PIC X.                                          00002210
           02 MNETNAMP  PIC X.                                          00002220
           02 MNETNAMH  PIC X.                                          00002230
           02 MNETNAMV  PIC X.                                          00002240
           02 MNETNAMO  PIC X(8).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MSCREENA  PIC X.                                          00002270
           02 MSCREENC  PIC X.                                          00002280
           02 MSCREENP  PIC X.                                          00002290
           02 MSCREENH  PIC X.                                          00002300
           02 MSCREENV  PIC X.                                          00002310
           02 MSCREENO  PIC X(4).                                       00002320
                                                                                
