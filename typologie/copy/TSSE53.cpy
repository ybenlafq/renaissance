      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      *        IDENTIFICATION SERVICE - CODES CONTROLES               * 00000020
      ***************************************************************** 00000030
      *                                                               * 00000040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS-53-LONG          PIC S9(4) COMP VALUE +190.               00000050
      *--                                                                       
       01  TS-53-LONG          PIC S9(4) COMP-5 VALUE +190.                     
      *}                                                                        
       01  TS-53-DATA.                                                  00000060
           05 TS-53-LIGNE.                                              00000070
              10 TS-POSTE-TSSE53    OCCURS 2.                           00000080
                 15 TS-53-DATA-AV.                                      00000090
                    20 TS-53-CDOSS-AV            PIC X(05).             00000100
                    20 TS-53-LDOSS-AV            PIC X(20).             00000110
                    20 TS-53-DATDEB-AV           PIC X(08).             00000120
                    20 TS-53-DATFIN-AV           PIC X(08).             00000130
                    20 TS-53-NSEQ-AV             PIC X(03).             00000140
                    20 TS-53-SUPP-AV             PIC X.                 00000150
                 15 TS-53-DATA-AP.                                      00000160
                    20 TS-53-CDOSS-AP            PIC X(05).             00000170
                    20 TS-53-LDOSS-AP            PIC X(20).             00000180
                    20 TS-53-DATDEB-AP           PIC X(08).             00000190
                    20 TS-53-DATFIN-AP           PIC X(08).             00000200
                    20 TS-53-NSEQ-AP             PIC X(03).             00000210
                    20 TS-53-SUPP-AP             PIC X.                 00000220
                 15 TS-53-FLAG-CTRL.                                    00000230
                    20 TS-53-CDRET-CDOSS         PIC X(04).             00000240
                 15 TS-53-FLAG-MAJ.                                     00000250
                    20 TS-53-WMAJTS              PIC X(01).             00000260
                                                                                
