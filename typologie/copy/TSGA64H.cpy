      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      * TS DONNEES REFERENCEMENT ET VENDABILITE EN MODE HISTO ACTIF             
      *****************************************************************         
      * DE02011 06/11/13 CREATION - PROJET THEMIS                               
      *****************************************************************         
       01 TS-GA64H.                                                             
      *    LONGUEUR TS                                                          
           05 TS-GA64H-LONG PIC S9(5) COMP-3 VALUE +100.                        
           05 TS-GA64H-DONNEES.                                                 
               10 TS-GA64H-NCODIC        PIC X(7).                              
               10 TS-GA64H-OPCO          PIC X(3).                              
               10 TS-GA64H-DMAJ          PIC X(8).                              
               10 TS-GA64H-COPCO         PIC X(15).                             
               10 TS-GA64H-LOPCO         PIC X(20).                             
               10 TS-GA64H-REF           PIC X(1).                              
               10 TS-GA64H-DTDEB         PIC X(8).                              
               10 TS-GA64H-DTFIN         PIC X(8).                              
               10 FILLER                PIC X(30).                              
                                                                                
