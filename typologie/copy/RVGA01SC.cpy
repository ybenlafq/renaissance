      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE AUTO2 REGROUPEMENTS PAR UTILISATEUR    *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01SC.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01SC.                                                            
      *}                                                                        
           05  AUTO2-CTABLEG2    PIC X(15).                                     
           05  AUTO2-CTABLEG2-REDEF REDEFINES AUTO2-CTABLEG2.                   
               10  AUTO2-CACID           PIC X(08).                             
               10  AUTO2-CDOMAPPL        PIC X(07).                             
           05  AUTO2-WTABLEG     PIC X(80).                                     
           05  AUTO2-WTABLEG-REDEF  REDEFINES AUTO2-WTABLEG.                    
               10  AUTO2-WACTIF          PIC X(01).                             
               10  AUTO2-LCOMMENT        PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01SC-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01SC-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AUTO2-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  AUTO2-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AUTO2-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  AUTO2-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
