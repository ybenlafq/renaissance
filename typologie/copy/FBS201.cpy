      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************                00000010
      *   DESCRIPTION DU FICHIER FBS201    LG = 140                     00000020
      **************************************************                00000030
       01  W-ENR-FBS201.                                                00000040
1          02 FBS201-OFFRE          PIC X(07).                          00000050
8          02 FBS201-NCODIC         PIC X(07).                          00000060
           02 FBS201-INFOS.                                             00000070
            03 FBS201-INFOS-FBS200.                                     00000080
15            05 FBS201-LREFFOURN      PIC X(20).                       00000090
35            05 FBS201-CFAM           PIC X(05).                       00000100
40            05 FBS201-CMARQ          PIC X(05).                       00000110
45            05 FBS201-LSTATCOMP      PIC X(03).                       00000120
48            05 FBS201-CHEFPROD       PIC X(05).                       00000130
53            05 FBS201-CTAUXTVA       PIC X(05).                       00000140
58            05 FBS201-DEFSTATUT      PIC X(08).                       00000150
66            05 FBS201-WSEQED         PIC X(02).                       00000160
68            05 FBS201-NAGREGATED     PIC X(02).                       00000170
70            05 FBS201-LAGREGATED     PIC X(20).                       00000180
90            05 FBS201-CPROFASS       PIC X(05).                       00000190
95            05 FBS201-200-FILLER     PIC X(13).                       00000200
108         03 FBS201-NSEQENT        PIC S9(3) COMP-3.                  00000210
110         03 FBS201-NENTMIN        PIC S9(3) COMP-3.                  00000220
112         03 FBS201-NENTMAX        PIC S9(3) COMP-3.                  00000230
114         03 FBS201-WPRIME         PIC X(01).                         00000240
115         03 FBS201-WPRIX          PIC X(01).                         00000250
116         03 FBS201-TYPE           PIC X(02).                         00000260
118         03 FBS201-MONTANT        PIC S9(5)V99 COMP-3.               00000270
122         03 FBS201-MONTANT-MAX    PIC S9(5)V99 COMP-3.               00000280
126         03 FBS201-REFI           PIC S9(5)V99 COMP-3.               00000290
130         03 FILLER                PIC X(11) VALUE SPACES.            00000300
       01  ST-FBS201                PIC 99              VALUE ZERO.     00000310
                                                                                
