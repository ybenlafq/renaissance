      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPR446 AU 24/11/1998  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,01,BI,A,                          *        
      *                           21,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPR446.                                                        
            05 NOMETAT-IPR446           PIC X(6) VALUE 'IPR446'.                
            05 RUPTURES-IPR446.                                                 
           10 IPR446-NSOCIETE           PIC X(03).                      007  003
           10 IPR446-CTYPPREST          PIC X(05).                      010  005
           10 IPR446-COPER              PIC X(05).                      015  005
           10 IPR446-WPRODMAJ           PIC X(01).                      020  001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPR446-SEQUENCE           PIC S9(04) COMP.                021  002
      *--                                                                       
           10 IPR446-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPR446.                                                   
           10 IPR446-TX-CONCRETISATION  PIC X(01).                      023  001
           10 IPR446-WMAJEUR            PIC X(01).                      024  001
           10 IPR446-WOPTIONS           PIC X(01).                      025  001
           10 IPR446-PPRIME             PIC S9(05)V9(2) COMP-3.         026  004
           10 IPR446-PVTOTAL            PIC S9(07)V9(2) COMP-3.         030  005
           10 IPR446-QVENDUES           PIC S9(06)      COMP-3.         035  004
           10 IPR446-DDEBUT             PIC X(08).                      039  008
           10 IPR446-DFIN               PIC X(08).                      047  008
            05 FILLER                      PIC X(458).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPR446-LONG           PIC S9(4)   COMP  VALUE +054.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPR446-LONG           PIC S9(4) COMP-5  VALUE +054.           
                                                                                
      *}                                                                        
