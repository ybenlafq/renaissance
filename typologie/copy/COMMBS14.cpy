      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                        00000010
                                                                                
      *--  ZONES RESERVEES APPLICATIVES OPTION 1 ----------------- 3855 00000020
                                                                        00000030
           02 COMM-BS14-APPLI REDEFINES COMM-BS10-FILLER.               00000040
              03 COMM-14.                                               00000050
                 05 COMM-14-ENTETE.                                     00000060
                    10 COMM-14-NENTITE1     PIC X(7).                   00000070
                    10 COMM-14-LENTITE1     PIC X(20).                  00000080
                    10 COMM-14-STA1         PIC X(3).                   00000090
                    10 COMM-14-CFAM1        PIC X(5).                   00000100
                    10 COMM-14-LFAM1        PIC X(20).                  00000110
                    10 COMM-14-CMARQ1       PIC X(5).                   00000120
                    10 COMM-14-LMARQ1       PIC X(20).                  00000130
                    10 COMM-14-NSEQENT-PREV PIC S9(3) COMP-3.           00000140
                    10 COMM-14-NSEQENT      PIC S9(3) COMP-3.           00000150
      *-- GESTION ETAT ECRAN                                            00000160
                 05 COMM-14-ETAT            PIC 9(1).                   00000170
      *-- GESTION COMPTEUR-MAJ                                          00000180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-14-NB-MAJ          PIC S9(4) COMP.             00000190
      *--                                                                       
                 05 COMM-14-NB-MAJ          PIC S9(4) COMP-5.                   
      *}                                                                        
      *-- GESTION TS POSITION                                           00000200
                 05 COMM-14-POSITION-TS.                                00000210
      *                LIGNE                                            00000220
                    10 COMM-14-INDTS           PIC S9(5) COMP-3.        00000230
                    10 COMM-14-INDMAX          PIC S9(5) COMP-3.        00000240
      *                COLONNE                                          00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *             10 COMM-14-COLONNE         PIC S9 COMP.             00000260
      *--                                                                       
                    10 COMM-14-COLONNE         PIC S9 COMP-5.                   
      *}                                                                        
                    10 COMM-14-CPTINDMAX.                               00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                20 COMM-14-T-INDMAX     PIC S9(5) COMP OCCURS 3. 00000280
      *--                                                                       
                       20 COMM-14-T-INDMAX     PIC S9(5) COMP-5 OCCURS          
                                                                      3.        
      *}                                                                        
              03 COMM-BS14-FILLER           PIC X(3000).                00000290
                                                                                
