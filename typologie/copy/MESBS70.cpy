      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00010000
      *   COPY MESSAGE MQ REMONTEE ENCAISSEMENT                         00020000
      *   VERSION AVEC ZONES NMD POUR MAGASIN B&S                       00030000
      ***************************************************************** 00040000
      *                                                                 00050000
      *                                                                 00060000
       01  WS-MQ10.                                                     00070000
         02 WS-QUEUE.                                                   00080000
               10   MQ10-MSGID     PIC    X(24).                        00090003
               10   MQ10-CORRELID  PIC    X(24).                        00091000
         02 WS-CODRET              PIC    XX.                           00100000
         02  MESSAGE70.                                                 00110000
           03 MESSAGE70-ENTETE.                                         00120000
               05   MES70-TYPE     PIC    X(3).                         00130000
               05   MES70-NSOCMSG  PIC    X(3).                         00140000
               05   MES70-NLIEUMSG PIC    X(3).                         00150000
               05   MES70-NSOCDST  PIC    X(3).                         00160000
               05   MES70-NLIEUDST PIC    X(3).                         00170000
               05   MES70-NORD     PIC    9(8).                         00180000
               05   MES70-LPROG    PIC    X(10).                        00190000
               05   MES70-DJOUR    PIC    X(8).                         00200000
               05   MES70-WSID     PIC    X(10).                        00210000
               05   MES70-USER     PIC    X(10).                        00220000
               05   MES70-CHRONO   PIC    9(7).                         00230000
               05   MES70-NBRMSG   PIC    9(7).                         00240000
               05   MES70-FILLER   PIC    X(30).                        00250000
      *--DATA MESSAGE = 545 CAR                                         00260000
         03 MESSAGE70-DATA.                                             00270000
      *--SOCIETE DE DEMANDE D'ENCAISSEMENT                              00280000
           05  MES70-NSOCD         PIC    X(3).                         00290000
      *--LIEU    DE DEMANDE D'ENCAISSEMENT                              00300000
           05  MES70-NLIEUD        PIC    X(3).                         00310000
      *--NOMBRE DE VENTES A ENCAISSER                                   00320000
           05  MES70-NBVTES        PIC    9(1).                         00330000
      *--NOMBRE DE MODE DE PAIEMENT A ENCAISSER                         00340000
           05  MES70-NBMODP        PIC    9(2).                         00350000
      *--MONTANT AU COMPTANT                                            00360000
           05  MES70-PCOMPT        PIC   S9(7)V99 COMP-3.               00370000
      *--DEVISE DE REFERENCE                                            00380000
           05  MES70-CDEVISE       PIC    X(3).                         00390000
      *--VENTES A ENCAISSER                                             00400000
           05  MES70-VENTES OCCURS 5.                                   00410000
      *--SOCIETE DE LA VENTE                                            00420000
               10 MES70-NSOCV      PIC    X(3).                         00430000
      *--LIEU DE LA VENTE                                               00440000
               10 MES70-NLIEUV     PIC    X(3).                         00450000
      *--NO DE VENTE                                                    00460000
               10 MES70-NVENTE     PIC    X(7).                         00470000
      *--TYPE DE VENTE                                                  00480000
               10 MES70-TYPVTE     PIC    X(1).                         00490000
      *--DATE DE VENTE                                                  00491000
               10 MES70-DVENTE     PIC    X(8).                         00492000
      *--SOCIETE D'ENCAISSEMENT DE LA VENTE                             00493000
           05  MES70-NSOCP         PIC    X(3).                         00494000
      *--LIEU    D'ENCAISSEMENT DE LA VENTE                             00495000
           05  MES70-NLIEUP        PIC    X(3).                         00496000
      *--MODE DE PAIEMENT D'ENCAISSEMENT DE LA VENTE                    00497000
           05  MES70-MOPAI OCCURS 10.                                   00498000
               10 MES70-CMOPAI     PIC    X(5).                         00499000
               10 MES70-PMOPAI     PIC   S9(7)V99 COMP-3.               00500000
               10 MES70-CDEV       PIC    X(3).                         00510000
               10 MES70-DESC       PIC    X(20).                        00520000
      *--DUPLI OK=O OU KO=N                                             00530000
           05  MES70-DUP-OK        PIC    X(1).                         00540000
      *--ENCAISSEMENT OK=O OU KO=N                                      00550000
           05  MES70-ENC-OK        PIC    X(1).                         00560000
      *--LIBELLE DUPLI OU ENCAISSEMENT OK/KO                            00570000
           05  MES70-MSG           PIC    X(80).                        00580000
      *--------------FIN MESSAGE-------------------------------------*  00590000
                                                                                
