      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GBQLI AUTORISATION MAJ QUOTAS          *        
      *----------------------------------------------------------------*        
       01  RVGA01RL.                                                            
           05  GBQLI-CTABLEG2    PIC X(15).                                     
           05  GBQLI-CTABLEG2-REDEF REDEFINES GBQLI-CTABLEG2.                   
               10  GBQLI-NSOCDEP         PIC S9(07)       COMP-3.               
               10  GBQLI-CSELART         PIC X(05).                             
               10  GBQLI-CNATART         PIC X(06).                             
           05  GBQLI-WTABLEG     PIC X(80).                                     
           05  GBQLI-WTABLEG-REDEF  REDEFINES GBQLI-WTABLEG.                    
               10  GBQLI-AUTOMAJ         PIC X(01).                             
               10  GBQLI-LCOMPENS        PIC X(40).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01RL-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GBQLI-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GBQLI-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GBQLI-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GBQLI-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
