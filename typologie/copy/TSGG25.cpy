      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                               *         
       01  TS-GG25.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GG25-LONG                 PIC S9(03) COMP-3  VALUE +42.        
      *----------------------------------  DONNEES  TS                          
           02 TS-GG25-DONNEES.                                                  
      *----------------------------------  CODE ACTION                          
              03 TS-GG25-ACT               PIC X(01).                           
      *----------------------------------  CODE FAMILLE                         
              03 TS-GG25-CFAM              PIC X(05).                           
      *----------------------------------  POURCENTAGE TRANSPO                  
              03 TS-GG25-POURCENT          PIC X(05).                           
      *----------------------------------  DATE D'EFFET                         
              03 TS-GG25-DEF               PIC X(08).                           
      *----------------------------------  ZONE DUPLICATION COEFF               
              03 TS-GG25-DUPCOEFF          PIC X(01).                           
      *----------------------------------  COMMENTAIRE                          
              03 TS-GG25-LCOMMENT          PIC X(20).                           
      *----------------------------------  TOP DE MAJ RTGA75                    
              03 TS-GG25-TOPMAJ            PIC X(01).                           
      *----------------------------------  TOP DE DUPLICATION "TOPMAJ"          
              03 TS-GG25-DUPTOPE           PIC X(01).                           
                                                                                
