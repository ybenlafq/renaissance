      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVGM0601                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGM0601                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGM0601.                                                    00000090
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGM0601.                                                            
      *}                                                                        
           02  GM06-NCODIC1                                             00000100
               PIC X(0007).                                             00000110
           02  GM06-NCODIC2                                             00000120
               PIC X(0007).                                             00000130
           02  GM06-CZPRIX                                              00000140
               PIC X(0002).                                             00000150
           02  GM06-CFAM                                                00000160
               PIC X(0005).                                             00000170
           02  GM06-WTYPART                                             00000180
               PIC X(0001).                                             00000190
           02  GM06-PSTDTTC                                             00000200
               PIC S9(7)V9(0002) COMP-3.                                00000210
           02  GM06-PCOMM                                               00000220
               PIC S9(5) COMP-3.                                        00000230
           02  GM06-QNBREART                                            00000240
               PIC S9(7) COMP-3.                                        00000250
           02  GM06-QSTOCKZONE                                          00000260
               PIC S9(7) COMP-3.                                        00000270
           02  GM06-DSYST                                               00000280
               PIC S9(13) COMP-3.                                       00000290
           02  GM06-PCOMMART                                            00000300
               PIC S9(5)V9(0002) COMP-3.                                00000310
           02  GM06-PCOMMVOL                                            00000320
               PIC S9(5)V9(0002) COMP-3.                                00000330
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00000340
      *---------------------------------------------------------        00000350
      *   LISTE DES FLAGS DE LA TABLE RVGM0601                          00000360
      *---------------------------------------------------------        00000370
      *                                                                 00000380
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGM0601-FLAGS.                                              00000390
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGM0601-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM06-NCODIC1-F                                           00000400
      *        PIC S9(4) COMP.                                          00000410
      *--                                                                       
           02  GM06-NCODIC1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM06-NCODIC2-F                                           00000420
      *        PIC S9(4) COMP.                                          00000430
      *--                                                                       
           02  GM06-NCODIC2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM06-CZPRIX-F                                            00000440
      *        PIC S9(4) COMP.                                          00000450
      *--                                                                       
           02  GM06-CZPRIX-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM06-CFAM-F                                              00000460
      *        PIC S9(4) COMP.                                          00000470
      *--                                                                       
           02  GM06-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM06-WTYPART-F                                           00000480
      *        PIC S9(4) COMP.                                          00000490
      *--                                                                       
           02  GM06-WTYPART-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM06-PSTDTTC-F                                           00000500
      *        PIC S9(4) COMP.                                          00000510
      *--                                                                       
           02  GM06-PSTDTTC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM06-PCOMM-F                                             00000520
      *        PIC S9(4) COMP.                                          00000530
      *--                                                                       
           02  GM06-PCOMM-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM06-QNBREART-F                                          00000540
      *        PIC S9(4) COMP.                                          00000550
      *--                                                                       
           02  GM06-QNBREART-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM06-QSTOCKZONE-F                                        00000560
      *        PIC S9(4) COMP.                                          00000570
      *--                                                                       
           02  GM06-QSTOCKZONE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM06-DSYST-F                                             00000580
      *        PIC S9(4) COMP.                                          00000590
      *--                                                                       
           02  GM06-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM06-PCOMMART-F                                          00000600
      *        PIC S9(4) COMP.                                          00000610
      *--                                                                       
           02  GM06-PCOMMART-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM06-PCOMMVOL-F                                          00000620
      *        PIC S9(4) COMP.                                          00000630
      *--                                                                       
           02  GM06-PCOMMVOL-F                                                  
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                            00000640
                                                                                
