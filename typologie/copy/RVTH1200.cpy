      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVTH1200                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTH1200.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTH1200.                                                            
      *}                                                                        
      *    *************************************************************        
      *                       NCODIC                                            
           10 TH12-NCODIC          PIC X(7).                                    
      *    *************************************************************        
      *                       COPCO                                             
           10 TH12-COPCO           PIC X(3).                                    
      *    *************************************************************        
      *                       DEFFET                                            
           10 TH12-DEFFET          PIC X(8).                                    
      *    *************************************************************        
      *                       PCF                                               
           10 TH12-PCF             PIC S9(7)V9(6) USAGE COMP-3.                 
      *    *************************************************************        
      *                       FORCE                                             
           10 TH12-FORCE           PIC X(1).                                    
      *    *************************************************************        
      *                       DCRESYST                                          
           10 TH12-DCRESYST        PIC X(26).                                   
      *    *************************************************************        
      *                       DMAJSYST                                          
           10 TH12-DMAJSYST        PIC X(26).                                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 7       *        
      ******************************************************************        
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVTH0000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTH1200-FLAG.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTH1200-FLAG.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 TH12-NCODIC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 TH12-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 TH12-COPCO-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 TH12-COPCO-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 TH12-DEFFET-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 TH12-DEFFET-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 TH12-PCF-F           PIC S9(4) COMP.                              
      *--                                                                       
           10 TH12-PCF-F           PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 TH12-FORCE-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 TH12-FORCE-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 TH12-DCRESYST-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 TH12-DCRESYST-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 TH12-DMAJSYST-F      PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 TH12-DMAJSYST-F      PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
