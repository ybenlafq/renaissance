      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                                 00000010
      *-----------------------------------------------------------------00000130
      *    M O D I F I C A T I O N S / M A I N T E N A N C E S          00000140
      *-----------------------------------------------------------------00000150
BS27  *  DATE       : 09/08/2004                                        00000160
BS27  *  AUTEUR     : M.BRAULT                                          00000170
BS27  *  SIGNET     : BS27                                              00000180
BS27  *  OBJET      : AJOUT DE L'OPTION D'APPEL AU TBS27                00000190
      *-----------------------------------------------------------------00000150
MB2   *  DATE       : 09/08/2004                                        00000160
"     *  AUTEUR     : M.BRAULT                                          00000170
"     *  SIGNET     : MB2                                               00000180
"     *  OBJET      : ADAPTATION BS23 POUR LOT 4                        00000190
"     *               FLAG D'ACTIVATION DE LA FAMILLE DANS L'INTERFACE          
MB2   *               CATALOGUE DACEM (PARAMETRE FAMILLE GLB&S)                 
      *-----------------------------------------------------------------00000200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-BS20-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.          00000020
      *--                                                                       
       01  COMM-BS20-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                
      *}                                                                        
      *                                                                 00000030
       01  Z-COMMAREA.                                                  00000040
                                                                        00000050
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00000060
           02 FILLER-COM-AIDA      PIC X(100).                          00000070
                                                                        00000080
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00000090
           02 COMM-CICS-APPLID     PIC X(08).                           00000100
           02 COMM-CICS-NETNAM     PIC X(08).                           00000110
           02 COMM-CICS-TRANSA     PIC X(04).                           00000120
                                                                        00000130
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00000140
           02 COMM-DATE-SIECLE     PIC X(02).                           00000150
           02 COMM-DATE-ANNEE      PIC X(02).                           00000160
           02 COMM-DATE-MOIS       PIC X(02).                           00000170
           02 COMM-DATE-JOUR       PIC 99.                              00000180
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00000190
           02 COMM-DATE-QNTA       PIC 999.                             00000200
           02 COMM-DATE-QNT0       PIC 99999.                           00000210
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00000220
           02 COMM-DATE-BISX       PIC 9.                               00000230
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00000240
           02 COMM-DATE-JSM        PIC 9.                               00000250
      *   LIBELLES DU JOUR COURT - LONG                                 00000260
           02 COMM-DATE-JSM-LC     PIC X(03).                           00000270
           02 COMM-DATE-JSM-LL     PIC X(08).                           00000280
      *   LIBELLES DU MOIS COURT - LONG                                 00000290
           02 COMM-DATE-MOIS-LC    PIC X(03).                           00000300
           02 COMM-DATE-MOIS-LL    PIC X(08).                           00000310
      *   DIFFERENTES FORMES DE DATE                                    00000320
           02 COMM-DATE-SSAAMMJJ   PIC X(08).                           00000330
           02 COMM-DATE-AAMMJJ     PIC X(06).                           00000340
           02 COMM-DATE-JJMMSSAA   PIC X(08).                           00000350
           02 COMM-DATE-JJMMAA     PIC X(06).                           00000360
           02 COMM-DATE-JJ-MM-AA   PIC X(08).                           00000370
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00000380
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00000390
           02 COMM-DATE-WEEK.                                           00000400
              05 COMM-DATE-SEMSS   PIC 99.                              00000410
              05 COMM-DATE-SEMAA   PIC 99.                              00000420
              05 COMM-DATE-SEMNU   PIC 99.                              00000430
           02 COMM-DATE-FILLER     PIC X(08).                           00000440
      *   ZONES RESERVEES TRAITEMENT DU SWAP                            00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00000460
      *                                                                         
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
                                                                        00000470
      *}                                                                        
      *--  ZONES RESERVEES APPLICATIVES COMMUNES ----------------- 3874 00000480
                                                                        00000490
           02 COMM-BS20-APPLI.                                          00000500
              03 COMM-ENTETE.                                           00000510
                 05 COMM-CODLANG            PIC X(02).                  00000520
                 05 COMM-CODPIC             PIC X(02).                  00000530
                 05 COMM-CODESFONCTION      PIC X(12).                  00000540
              03 COMM-20-ENTETE.                                        00000550
                 05 COMM-20-WFONC           PIC X(03).                  00000560
                 05 COMM-20-CHEFPROD        PIC X(05).                          
                 05 COMM-20-LCHEFPROD       PIC X(20).                          
                 05 COMM-20-CTYPENT         PIC X(02).                          
                 05 COMM-20-LTYPENT         PIC X(20).                          
                 05 COMM-20-CTABLE          PIC X(06).                          
           02 COMM-BS20-FILLER              PIC X(3802).                00000620
                                                                        00000010
      *--  ZONES RESERVEES APPLICATIVES OPTION 1 -----------------      00000020
           02 COMM-BS22       REDEFINES COMM-BS20-FILLER.               00000040
              03 COMM-22.                                               00000050
                 05 COMM-22-INDTS           PIC S9(5) COMP-3.           00000070
                 05 COMM-22-INDMAX          PIC S9(5) COMP-3.           00000080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-22-NLIGNE          PIC S9(3) COMP.             00000080
      *--                                                                       
                 05 COMM-22-NLIGNE          PIC S9(3) COMP-5.                   
      *}                                                                        
                 05 COMM-22-TYPENTR         PIC X(1).                   00000080
                    88 COMM-22-PAS-TYPE            VALUE ' '.                   
                    88 COMM-22-TYPE-PRODUIT        VALUE 'P'.                   
                    88 COMM-22-TYPE-SERVICE        VALUE 'S'.                   
                    88 COMM-22-TYPE-OFFRE          VALUE 'O'.                   
                 05 COMM-22-TYPENTR-PR      PIC X(1).                   00000080
                 05 COMM-22-TYPENTITER      PIC X(2).                   00000080
                 05 COMM-22-NENTITER        PIC X(7).                   00000080
                 05 COMM-22-CFAMR           PIC X(5).                   00000080
                 05 COMM-22-CMARQR          PIC X(5).                   00000080
                 05 COMM-22-LENTITER        PIC X(20).                  00000080
                 05 COMM-22-STA             PIC X(3).                   00000080
                 05 COMM-22-PRIX            PIC X(9).                   00000080
                 05 COMM-22-CSELECTION      PIC X(4).                   00000080
                 05 COMM-22-CFAMS           PIC X(5).                   00000080
                 05 COMM-22-CMARQS          PIC X(5).                   00000080
                 05 COMM-22-LENTITES        PIC X(20).                  00000080
                 04 COMM-22-FILLER          PIC X(070).                    00000
                                                                        00000030
              03 COMM-21.                                                  00000
                 05 COMM-21-INDTS           PIC S9(5) COMP-3.           00000   
                 05 COMM-21-INDMAX          PIC S9(5) COMP-3.           00000   
                 05 COMM-21-INDDEB          PIC S9(5) COMP-3.           00000   
                 05 COMM-21-INDFIN          PIC S9(5) COMP-3.           00000   
                 05 COMM-21-CFAM            PIC X(05).                  00000   
                 05 COMM-21-LFAM            PIC X(20).                  00000   
                 05 COMM-BS21-FILLER        PIC X(100).                    00000
                                                                        00000030
              03 COMM-23.                                               00000050
                 05 COMM-23-NENTITER        PIC X(7).                   00000080
                 05 COMM-23-TYPENTITER      PIC X(2).                   00000080
                 05 COMM-23-CFAMR           PIC X(5).                   00000080
                 05 COMM-23-CMARQR          PIC X(5).                   00000080
                 05 COMM-23-LENTITER        PIC X(20).                  00000080
                 05 COMM-23-STA             PIC X(3).                   00000080
                 05 COMM-23-PRIX            PIC X(9).                   00000080
                 05 COMM-23-INDTS           PIC S9(5) COMP-3.           00000070
                 05 COMM-23-L-INDMAX.                                   00000080
                    15 COMM-23-INDMAX       PIC S9(5) COMP-3 OCCURS 4.     00000
                 05 COMM-23-BS05-MAX        PIC S9(5) COMP-3.           00000080
                 05 COMM-23-L-ACTMAX.                                   00000080
                    15 COMM-23-ACTMAX       PIC S9(5) COMP-3 OCCURS 4.     00000
      *--        LIEN FAMILLE                                           00000020
                 05 COMM-23-BLOC-FAM.                                   00000080
                    15 COMM-23-LFAM         PIC X(01)        OCCURS 4.     00000
                 05 COMM-23-BS05-A-MAX      PIC S9(5) COMP-3.           00000080
                 05 COMM-23-ONGLET          PIC 9(1).                   00000080
                 05 COMM-23-MAJ-EN-COURS    PIC X(01).                          
                    88 MAJ-EN-COURS VALUE '1'.                                  
                 05 COMM-23-NLISTORIG       PIC X(5).                   00000080
                 05 COMM-23-PGM-RETOUR      PIC X(5).                   00000080
                 05 COMM-23-LIEN-FAMILLE    PIC X(01).                          
                    88 LIEN-FAMILLE VALUE '1'.                                  
                 05 COMM-23-SUPPRESSION     PIC X(01).                          
                    88 CONFIRMER-SUPPRESSION  VALUE '1'.                        
MB2              05 COMM-23-ACTIF-INTERFACE PIC X(1).                           
"     *          05 COMM-23-FILLER          PIC X(098).                 00000450
MB2              05 COMM-23-FILLER          PIC X(097).                 00000450
              03 COMM-24.                                               00050   
                 05 COMM-24-NENTITER        PIC X(7).                   00080   
                 05 COMM-24-TYPENTITER      PIC X(2).                   00000080
                 05 COMM-24-CFAMR           PIC X(5).                   00000080
                 05 COMM-24-CMARQR          PIC X(5).                   00000080
                 05 COMM-24-LENTITER        PIC X(20).                  00000080
                 05 COMM-24-STA             PIC X(3).                   00000080
                 05 COMM-24-PRIX            PIC X(9).                   00000080
                 05 COMM-24-INDTS           PIC S9(5) COMP-3.           00070   
                 05 COMM-24-INDMAX          PIC S9(5) COMP-3.           00000   
                 05 COMM-24-INDMIN          PIC S9(5) COMP-3.           00000   
                 05 COMM-24-INDTS-FAM       PIC S9(5) COMP-3.           00070   
                 05 COMM-24-NLIGNE          PIC S9(3) COMP-3.           00000080
                 05 COMM-24-ECRAN           PIC X(01).                          
                    88 COMM-24-ECRAN-FAMILLE  VALUE 'F'.                        
                    88 COMM-24-ECRAN-ENTITE   VALUE 'E'.                        
                 05 COMM-24-RETOUR          PIC X(01).                          
                    88 COMM-24-RETOUR-FAMILLE VALUE 'F'.                        
                 05 COMM-24-FILLEUR         PIC X(099).                 00080   
              03 COMM-25.                                               00050   
                 05 COMM-25-INDTS           PIC S9(5) COMP-3.           00070   
                 05 COMM-25-L-INDMAX.                                   00000080
                    15 COMM-25-INDMAX       PIC S9(5) COMP-3 OCCURS 2.     00000
                 05 COMM-BS25-NSOC          PIC X(03).                  00070   
                 05 COMM-BS25-NMAG          PIC X(03).                  00000   
                 05 COMM-BS25-LMAG          PIC X(20).                  00000   
                 05 COMM-BS25-AFFICHAGE     PIC 9(01).                  00000   
                 05 COMM-BS25-AFFICH        PIC 9(01) .                 00000   
                    88 COMM-25-AFFICH-DL      VALUE 1.                          
                    88 COMM-25-AFFICH-TOUT    VALUE 2.                          
      * POUR DEFINIR LE POSITIONNEMENT SUR L"ECRAN PF11-COMPLEMENT      00000010
                 05 COMM-25-ECRAN           PIC X(01).                          
                    88 COMM-25-ECRAN-PF11     VALUE 'E'.                        
                 05 COMM-25-FILLEUR         PIC X(100).                 00000   
              03 COMM-26.                                               00050   
                 05 COMM-26-INDTS           PIC S9(5) COMP-3.           00070   
                 05 COMM-26-INDMAX          PIC S9(5) COMP-3 .          00000080
                 05 COMM-26-CTYPENT2        PIC X(02).                  00070   
                 05 COMM-26-MLENTITE2       PIC X(20).                  00000   
                 05 COMM-26-NBLOC           PIC S9(3) COMP-3.           00000   
                 05 COMM-26-CQUALIF         PIC X(05).                  00000   
                 05 COMM-26-LQUALIF         PIC X(20).                  00000   
                 05 COMM-26-NLIST           PIC X(07).                  00000   
      * POUR DEFINIR LE POSITIONNEMENT SUR L"ECRAN PF11-COMPLEMENT      00000010
                 05 COMM-26-ECRAN           PIC X(01).                          
                    88 COMM-26-ECRAN-PF11     VALUE 'E'.                        
                 05 COMM-26-FILLEUR         PIC X(100).                 00000   
      *       03 COMM-BS21-FILLER           PIC X(2637).                00000450
              03 COMM-BS21-FILLER           PIC X(2833).                00000450
BS27       02 COMM-BS27       REDEFINES COMM-BS20-FILLER.               00000040
BS27          03 COMM-27.                                                       
BS27             05 COMM-27-INDTS           PIC S9(5) COMP-3.           00070   
BS27             05 COMM-27-INDMAX          PIC S9(5) COMP-3.           00000080
BS27             05 COMM-27-I               PIC S9(5) COMP-3.                   
BS27             05 COMM-27-IMAX            PIC S9(5) COMP-3.                   
BS27             05 COMM-27-T-R1-R2.                                            
BS27                15 COMM-27-T     OCCURS 10.                                 
BS27                   20 COMM-27-T-CR1     PIC X(5).                           
BS27                   20 COMM-27-T-J       PIC S9(5) COMP-3.                   
BS27                   20 COMM-27-T-JMAX    PIC S9(5) COMP-3.                   
BS27                   20 COMM-27-TT OCCURS 20.                                 
BS27                      25 COMM-27-T-ITEM PIC S9(5) COMP-3.                   
BS27                      25 COMM-27-T-CR2  PIC X(8).                           
BS27          03 COMM-BS27-FILLER           PIC X(1480).                00000620
                                                                                
