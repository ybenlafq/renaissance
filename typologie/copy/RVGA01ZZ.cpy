      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE XCTRL SPECIFICITE TRANS OU PG / SOC    *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01ZZ.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01ZZ.                                                            
      *}                                                                        
           05  XCTRL-CTABLEG2    PIC X(15).                                     
           05  XCTRL-CTABLEG2-REDEF REDEFINES XCTRL-CTABLEG2.                   
               10  XCTRL-SOCZP           PIC X(05).                             
               10  XCTRL-TRANS           PIC X(06).                             
               10  XCTRL-CTRL            PIC X(04).                             
           05  XCTRL-WTABLEG     PIC X(80).                                     
           05  XCTRL-WTABLEG-REDEF  REDEFINES XCTRL-WTABLEG.                    
               10  XCTRL-WFLAG           PIC X(01).                             
               10  XCTRL-WPARAM          PIC X(10).                             
               10  XCTRL-LIBELLE         PIC X(30).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01ZZ-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01ZZ-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  XCTRL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  XCTRL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  XCTRL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  XCTRL-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
