      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SGCTA SIGA/GCT: TYPES DE CONTRATS      *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01UT.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01UT.                                                            
      *}                                                                        
           05  SGCTA-CTABLEG2    PIC X(15).                                     
           05  SGCTA-CTABLEG2-REDEF REDEFINES SGCTA-CTABLEG2.                   
               10  SGCTA-TYPCON          PIC X(02).                             
               10  SGCTA-NATCON          PIC X(02).                             
           05  SGCTA-WTABLEG     PIC X(80).                                     
           05  SGCTA-WTABLEG-REDEF  REDEFINES SGCTA-WTABLEG.                    
               10  SGCTA-WACTIF          PIC X(01).                             
               10  SGCTA-TYPCTA          PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01UT-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01UT-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SGCTA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SGCTA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SGCTA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SGCTA-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
