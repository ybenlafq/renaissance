      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HEPAF CODES PANNE / FAMILLE GHE        *        
      *----------------------------------------------------------------*        
       01  RVGA01K0.                                                            
           05  HEPAF-CTABLEG2    PIC X(15).                                     
           05  HEPAF-CTABLEG2-REDEF REDEFINES HEPAF-CTABLEG2.                   
               10  HEPAF-CFAM            PIC X(05).                             
               10  HEPAF-CPANNE          PIC X(05).                             
           05  HEPAF-WTABLEG     PIC X(80).                                     
           05  HEPAF-WTABLEG-REDEF  REDEFINES HEPAF-WTABLEG.                    
               10  HEPAF-WACTIF          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01K0-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HEPAF-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HEPAF-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HEPAF-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HEPAF-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
