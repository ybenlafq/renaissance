      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVPR5500                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVPR5500                 00000060
      *   CLE UNIQUE : CPRESTATION A NENTCDE                            00000070
      *---------------------------------------------------------        00000080
      *                                                                 00000090
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPR5500.                                                    00000100
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPR5500.                                                            
      *}                                                                        
           10 PR55-CPRESTATION     PIC X(5).                            00000110
           10 PR55-NENTCDE         PIC X(5).                            00000120
           10 PR55-WENTCDE         PIC X(1).                            00000130
           10 PR55-DSYST           PIC S9(13)V USAGE COMP-3.            00000140
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00000150
      *---------------------------------------------------------        00000160
      *   LISTE DES FLAGS DE LA TABLE RVPR5500                          00000170
      *---------------------------------------------------------        00000180
      *                                                                 00000190
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPR5500-FLAGS.                                              00000200
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPR5500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR55-CPRESTATION-F   PIC S9(4) COMP.                      00000210
      *--                                                                       
           10 PR55-CPRESTATION-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR55-NENTCDE-F       PIC S9(4) COMP.                      00000220
      *--                                                                       
           10 PR55-NENTCDE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR55-WENTCDE-F       PIC S9(4) COMP.                      00000230
      *--                                                                       
           10 PR55-WENTCDE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR55-DSYST-F         PIC S9(4) COMP.                      00000240
      *                                                                         
      *--                                                                       
           10 PR55-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
