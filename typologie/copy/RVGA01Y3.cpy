      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SLDOC NMD/SL : TYPES DE DOCUMENTS      *        
      *----------------------------------------------------------------*        
       01  RVGA01Y3.                                                            
           05  SLDOC-CTABLEG2    PIC X(15).                                     
           05  SLDOC-CTABLEG2-REDEF REDEFINES SLDOC-CTABLEG2.                   
               10  SLDOC-CTYPD           PIC X(02).                             
           05  SLDOC-WTABLEG     PIC X(80).                                     
           05  SLDOC-WTABLEG-REDEF  REDEFINES SLDOC-WTABLEG.                    
               10  SLDOC-LTYPD           PIC X(20).                             
               10  SLDOC-BMIN            PIC X(09).                             
               10  SLDOC-BMIN-N         REDEFINES SLDOC-BMIN                    
                                         PIC 9(09).                             
               10  SLDOC-BMAX            PIC X(09).                             
               10  SLDOC-BMAX-N         REDEFINES SLDOC-BMAX                    
                                         PIC 9(09).                             
               10  SLDOC-CPGM            PIC X(10).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01Y3-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLDOC-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SLDOC-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLDOC-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SLDOC-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
