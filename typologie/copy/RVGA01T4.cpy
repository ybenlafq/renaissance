      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EFLID LIEUX HS AUTORISÚS DANS TEF10    *        
      *----------------------------------------------------------------*        
       01  RVGA01T4.                                                            
           05  EFLID-CTABLEG2    PIC X(15).                                     
           05  EFLID-CTABLEG2-REDEF REDEFINES EFLID-CTABLEG2.                   
               10  EFLID-NSOCORIG        PIC X(03).                             
               10  EFLID-NSOCORIG-N     REDEFINES EFLID-NSOCORIG                
                                         PIC 9(03).                             
               10  EFLID-NLIEUORI        PIC X(03).                             
           05  EFLID-WTABLEG     PIC X(80).                                     
           05  EFLID-WTABLEG-REDEF  REDEFINES EFLID-WTABLEG.                    
               10  EFLID-WACTIF          PIC X(01).                             
               10  EFLID-LIBELLE         PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01T4-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EFLID-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EFLID-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EFLID-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EFLID-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
