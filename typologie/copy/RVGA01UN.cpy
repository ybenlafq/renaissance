      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE NEMET PARAMETRAGE DES CODES ETAT       *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01UN.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01UN.                                                            
      *}                                                                        
           05  NEMET-CTABLEG2    PIC X(15).                                     
           05  NEMET-CTABLEG2-REDEF REDEFINES NEMET-CTABLEG2.                   
               10  NEMET-CETAT           PIC X(10).                             
           05  NEMET-WTABLEG     PIC X(80).                                     
           05  NEMET-WTABLEG-REDEF  REDEFINES NEMET-WTABLEG.                    
               10  NEMET-DDEBUT          PIC X(08).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  NEMET-DDEBUT-N       REDEFINES NEMET-DDEBUT                  
                                         PIC 9(08).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  NEMET-DFIN            PIC X(08).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  NEMET-DFIN-N         REDEFINES NEMET-DFIN                    
                                         PIC 9(08).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01UN-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01UN-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NEMET-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  NEMET-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NEMET-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  NEMET-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
