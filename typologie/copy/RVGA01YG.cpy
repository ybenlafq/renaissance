      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PRDEP ASSOCIATION PROFIL DEL/PERIM     *        
      *----------------------------------------------------------------*        
       01  RVGA01YG.                                                            
           05  PRDEP-CTABLEG2    PIC X(15).                                     
           05  PRDEP-CTABLEG2-REDEF REDEFINES PRDEP-CTABLEG2.                   
               10  PRDEP-CPRODEL         PIC X(05).                             
               10  PRDEP-CPERIM          PIC X(05).                             
           05  PRDEP-WTABLEG     PIC X(80).                                     
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01YG-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRDEP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PRDEP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRDEP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PRDEP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
