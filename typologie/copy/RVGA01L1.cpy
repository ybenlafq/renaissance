           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FGNAT NATURES DE FACTURATION           *        
      *----------------------------------------------------------------*        
       01  RVGA01L1.                                                            
           05  FGNAT-CTABLEG2    PIC X(15).                                     
           05  FGNAT-CTABLEG2-REDEF REDEFINES FGNAT-CTABLEG2.                   
               10  FGNAT-NATURE          PIC X(05).                             
           05  FGNAT-WTABLEG     PIC X(80).                                     
           05  FGNAT-WTABLEG-REDEF  REDEFINES FGNAT-WTABLEG.                    
               10  FGNAT-WACTIF          PIC X(01).                             
               10  FGNAT-LIBELLE         PIC X(30).                             
               10  FGNAT-VENT            PIC X(01).                             
               10  FGNAT-WAUTEMET        PIC X(01).                             
               10  FGNAT-WAUTREC         PIC X(10).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01L1-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGNAT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FGNAT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGNAT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FGNAT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
           EXEC SQL END DECLARE SECTION END-EXEC.
 
      *}                                                                        
