      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      *        IDENTIFICATION SERVICE - DONNEES FOURNISSEURS          * 00000020
      ***************************************************************** 00000030
      *                                                               * 00000040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS-55-LONG          PIC S9(4) COMP VALUE +118.               00000050
      *--                                                                       
       01  TS-55-LONG          PIC S9(4) COMP-5 VALUE +118.                     
      *}                                                                        
       01  TS-55-DATA.                                                  00000060
           05 TS-55-LIGNE.                                              00000070
              10 TS-POSTE-TSSE55    OCCURS 2.                           00000080
                 15 TS-55-DATA-AV.                                      00000090
                    20 TS-55-NENTCDE-AV          PIC X(05).             00000100
                    20 TS-55-LENTCDE-AV          PIC X(20).             00000110
                    20 TS-55-WENTCDE-AV          PIC X(01).             00000120
                    20 TS-55-SUPP-AV             PIC X(01).             00000130
                 15 TS-55-DATA-AP.                                      00000140
                    20 TS-55-NENTCDE-AP          PIC X(05).             00000150
                    20 TS-55-LENTCDE-AP          PIC X(20).             00000160
                    20 TS-55-WENTCDE-AP          PIC X(01).             00000170
                    20 TS-55-SUPP-AP             PIC X(01).             00000180
                 15 TS-55-FLAG-CTRL.                                    00000190
                    20 TS-55-CDRET-NENTCDE       PIC X(04).             00000200
                 15 TS-55-FLAG-MAJ.                                     00000210
                    20 TS-55-WMAJTS              PIC X(01).             00000220
                                                                                
