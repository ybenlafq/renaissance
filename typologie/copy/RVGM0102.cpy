      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGM0102                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGM0102                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGM0102.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGM0102.                                                            
      *}                                                                        
           02  GM01-DATETRAIT                                                   
               PIC X(0008).                                                     
           02  GM01-CHEFPROD                                                    
               PIC X(0005).                                                     
           02  GM01-CFAM                                                        
               PIC X(0005).                                                     
           02  GM01-WSEQFAM                                                     
               PIC X(0005).                                                     
           02  GM01-WTYPART                                                     
               PIC X(0001).                                                     
           02  GM01-NCODIC1                                                     
               PIC X(0007).                                                     
           02  GM01-NCODIC2                                                     
               PIC X(0007).                                                     
           02  GM01-CMARKETIN1                                                  
               PIC X(0005).                                                     
           02  GM01-CVMARKETIN1                                                 
               PIC X(0005).                                                     
           02  GM01-LVMARKETIN1                                                 
               PIC X(0020).                                                     
           02  GM01-CMARKETIN2                                                  
               PIC X(0005).                                                     
           02  GM01-CVMARKETIN2                                                 
               PIC X(0005).                                                     
           02  GM01-LVMARKETIN2                                                 
               PIC X(0020).                                                     
           02  GM01-CMARKETIN3                                                  
               PIC X(0005).                                                     
           02  GM01-CVMARKETIN3                                                 
               PIC X(0005).                                                     
           02  GM01-LVMARKETIN3                                                 
               PIC X(0020).                                                     
           02  GM01-QTRI                                                        
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GM01-CMARQ                                                       
               PIC X(0005).                                                     
           02  GM01-LREFFOURN                                                   
               PIC X(0020).                                                     
           02  GM01-CFAMELE                                                     
               PIC X(0005).                                                     
           02  GM01-WLIBERTE                                                    
               PIC X(0001).                                                     
           02  GM01-CASSORT                                                     
               PIC X(0005).                                                     
           02  GM01-CAPPRO                                                      
               PIC X(0005).                                                     
           02  GM01-CEXPO                                                       
               PIC X(0005).                                                     
           02  GM01-STATCOMP                                                    
               PIC X(0003).                                                     
           02  GM01-WSENSVTE                                                    
               PIC X(0001).                                                     
           02  GM01-WSENSAPPRO                                                  
               PIC X(0001).                                                     
           02  GM01-DEFSTATUT                                                   
               PIC X(0008).                                                     
           02  GM01-WTLMELA                                                     
               PIC X(0001).                                                     
           02  GM01-CVDESCRIPT1                                                 
               PIC X(0005).                                                     
           02  GM01-CVDESCRIPT2                                                 
               PIC X(0005).                                                     
           02  GM01-CVDESCRIPT3                                                 
               PIC X(0005).                                                     
           02  GM01-QTXTVA                                                      
               PIC S9(3)V9(0002) COMP-3.                                        
           02  GM01-QDELAIAPPRO                                                 
               PIC S9(3) COMP-3.                                                
           02  GM01-PRAR                                                        
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GM01-PRMP                                                        
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GM01-PMBUZ1                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GM01-CZPRIXZ1                                                    
               PIC X(0002).                                                     
           02  GM01-PSTDTTCZ1                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GM01-PCOMMZ1                                                     
               PIC S9(5) COMP-3.                                                
           02  GM01-QNBREART                                                    
               PIC S9(7) COMP-3.                                                
           02  GM01-QTYPLIEN                                                    
               PIC S9(3) COMP-3.                                                
           02  GM01-QSTOCKDEP                                                   
               PIC S9(7) COMP-3.                                                
           02  GM01-QSTOCKMAG                                                   
               PIC S9(7) COMP-3.                                                
           02  GM01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GM01-PCOMMARTZ1                                                  
               PIC S9(5)V9(0002) COMP-3.                                        
           02  GM01-PCOMMVOLZ1                                                  
               PIC S9(5)V9(0002) COMP-3.                                        
           02  GM01-WSENSNF                                                     
               PIC X(0001).                                                     
           02  GM01-PSTDTTCZ2                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GM01-WIMPERATIF                                                  
               PIC X(0001).                                                     
           02  GM01-WBLOQUE                                                     
               PIC X(0001).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGM0102                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGM0102-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGM0102-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-DATETRAIT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-DATETRAIT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CHEFPROD-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-CHEFPROD-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-WSEQFAM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-WSEQFAM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-WTYPART-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-WTYPART-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-NCODIC1-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-NCODIC1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-NCODIC2-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-NCODIC2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CMARKETIN1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-CMARKETIN1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CVMARKETIN1-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-CVMARKETIN1-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-LVMARKETIN1-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-LVMARKETIN1-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CMARKETIN2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-CMARKETIN2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CVMARKETIN2-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-CVMARKETIN2-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-LVMARKETIN2-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-LVMARKETIN2-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CMARKETIN3-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-CMARKETIN3-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CVMARKETIN3-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-CVMARKETIN3-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-LVMARKETIN3-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-LVMARKETIN3-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-QTRI-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-QTRI-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-LREFFOURN-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-LREFFOURN-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CFAMELE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-CFAMELE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-WLIBERTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-WLIBERTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CASSORT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-CASSORT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CAPPRO-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-CAPPRO-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CEXPO-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-CEXPO-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-STATCOMP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-STATCOMP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-WSENSVTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-WSENSVTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-WSENSAPPRO-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-WSENSAPPRO-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-DEFSTATUT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-DEFSTATUT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-WTLMELA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-WTLMELA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CVDESCRIPT1-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-CVDESCRIPT1-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CVDESCRIPT2-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-CVDESCRIPT2-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CVDESCRIPT3-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-CVDESCRIPT3-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-QTXTVA-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-QTXTVA-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-QDELAIAPPRO-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-QDELAIAPPRO-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-PRAR-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-PRAR-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-PRMP-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-PRMP-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-PMBUZ1-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-PMBUZ1-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-CZPRIXZ1-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-CZPRIXZ1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-PSTDTTCZ1-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-PSTDTTCZ1-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-PCOMMZ1-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-PCOMMZ1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-QNBREART-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-QNBREART-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-QTYPLIEN-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-QTYPLIEN-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-QSTOCKDEP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-QSTOCKDEP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-QSTOCKMAG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-QSTOCKMAG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-PCOMMARTZ1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-PCOMMARTZ1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-PCOMMVOLZ1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-PCOMMVOLZ1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-WSENSNF-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-WSENSNF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-PSTDTTCZ2-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-PSTDTTCZ2-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-WIMPERATIF-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-WIMPERATIF-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM01-WBLOQUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM01-WBLOQUE-F                                                   
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
