      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HEGSB REGLES STOCK                     *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01K5.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01K5.                                                            
      *}                                                                        
           05  HEGSB-CTABLEG2    PIC X(15).                                     
           05  HEGSB-CTABLEG2-REDEF REDEFINES HEGSB-CTABLEG2.                   
               10  HEGSB-CPROG           PIC X(05).                             
               10  HEGSB-NSEQ            PIC X(03).                             
           05  HEGSB-WTABLEG     PIC X(80).                                     
           05  HEGSB-WTABLEG-REDEF  REDEFINES HEGSB-WTABLEG.                    
               10  HEGSB-WACTIF          PIC X(01).                             
               10  HEGSB-WCLEDATA        PIC X(16).                             
               10  HEGSB-ENVORIG         PIC X(14).                             
               10  HEGSB-ENVDEST         PIC X(14).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01K5-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01K5-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HEGSB-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HEGSB-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HEGSB-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HEGSB-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
