      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE NM005 NATURE OPER.- FACT. NETTING      *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVGA01V6.                                                            
           05  NM005-CTABLEG2    PIC X(15).                                     
           05  NM005-CTABLEG2-REDEF REDEFINES NM005-CTABLEG2.                   
               10  NM005-CAPPLI          PIC X(05).                             
               10  NM005-CNATOPER        PIC X(05).                             
               10  NM005-SIGNE           PIC X(01).                             
           05  NM005-WTABLEG     PIC X(80).                                     
           05  NM005-WTABLEG-REDEF  REDEFINES NM005-WTABLEG.                    
               10  NM005-CNATFACT        PIC X(05).                             
               10  NM005-CVENT           PIC X(05).                             
               10  NM005-CUMUL           PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01V6-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NM005-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  NM005-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NM005-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  NM005-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
