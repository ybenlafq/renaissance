      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * TS DE STOCKAGE DES DONNEES                                    *         
      * LONGUEUR TOTALE = XXX                                         *         
      *****************************************************************         
      *                                                                         
       01  TS42-DONNEES.                                                        
      *---                                                                      
         02  TS42-TABLE                  PIC X(4).                              
         02  TS42-NSOCIETE               PIC X(03).                             
         02  TS42-NLIEU                  PIC X(03).                             
         02  TS42-NVENTE                 PIC X(07).                             
         02  TS42-ADRESE.                                                       
               10   TS42-WTYPEADR        PIC X(01).                             
               10   TS42-CTITRENOM       PIC X(05).                             
               10   TS42-LNOM            PIC X(25).                             
               10   TS42-LPRENOM         PIC X(15).                             
               10   TS42-LBATIMENT       PIC X(03).                             
               10   TS42-LESCALIER       PIC X(03).                             
               10   TS42-LETAGE          PIC X(03).                             
               10   TS42-LPORTE          PIC X(03).                             
               10   TS42-LCOMPAD1        PIC X(32).                             
               10   TS42-CVOIE           PIC X(05).                             
               10   TS42-CTVOIE          PIC X(04).                             
               10   TS42-LNOMVOIE        PIC X(21).                             
               10   TS42-LCOMMUNE        PIC X(32).                             
               10   TS42-CPOSTAL         PIC X(05).                             
               10   TS42-TELDOM          PIC X(10).                             
               10   TS42-TELBUR          PIC X(10).                             
MH             10   TS42-WEXPTAXE        PIC X(01).                             
"              10   TS42-NORDRE          PIC X(05).                             
"              10   TS42-IDCLIENT        PIC X(08).                             
"              10   TS42-CPAYS           PIC X(03).                             
MH             10   TS42-NGSM            PIC X(15).                             
           05 TS42-REGLEMENT.                                                   
               10  TS42-NSEQ             PIC X(03).                             
               10  TS42-DSAISIE          PIC X(08).                             
               10  TS42-CMODPAIMT        PIC X(05).                             
               10  TS42-PREGLTVTE        PIC S9(7)V99 COMP-3.                   
               10  TS42-NREGLTVTE        PIC X(07).                             
               10  TS42-DREGLTVTE        PIC X(08).                             
               10  TS42-WREGLTVTE        PIC X(01).                             
               10  TS42-NSOCP            PIC X(03).                             
               10  TS42-NLIEUP           PIC X(03).                             
               10  TS42-NTRANS           PIC S9(8) COMP-3.                      
               10  TS42-CVENDEUR         PIC X(06).                             
               10  TS42-CFCRED           PIC X(05).                             
               10  TS42-NCREDI           PIC X(14).                             
           05 TS42-ARTICLES.                                                    
               10  TS42-CTYPENREG        PIC X(01).                             
               10  TS42-NCODICGRP        PIC X(07).                             
               10  TS42-NCODIC           PIC X(07).                             
               10  TS42-NSEQA            PIC 99.                                
               10  TS42-CMODDEL          PIC X(03).                             
               10  TS42-DDELIV           PIC X(08).                             
               10  TS42-CENREG           PIC X(05).                             
               10  TS42-QVENDUE          PIC S9(5)    COMP-3.                   
               10  TS42-PVUNIT           PIC S9(7)V99 COMP-3.                   
               10  TS42-PVUNITF          PIC S9(7)V99 COMP-3.                   
               10  TS42-PVTOTAL          PIC S9(7)V99 COMP-3.                   
               10  TS42-NLIGNE           PIC X(02).                             
               10  TS42-TAUXTVA          PIC S9(3)V99 COMP-3.                   
               10  TS42-QCONDT           PIC S9(5)    COMP-3.                   
               10  TS42-LCOMMENT         PIC X(35).                             
               10  TS42-DANNULATION      PIC X(08).                             
VEND           10  TS42-NVENDEUR         PIC X(07).                             
               10  TS42-PVCODIG          PIC S9(7)V99 COMP-3.                   
               10  TS42-QTCODIG          PIC S9(5) COMP-3.                      
               10  TS42-CTYPENT          PIC X(02).                             
               10  TS42-NLIEN            PIC S9(5) COMP-3.                      
               10  TS42-NACTVTE          PIC S9(5) COMP-3.                      
               10  TS42-NSEQNQ           PIC S9(5) COMP-3.                      
               10  TS42-NSEQREF          PIC S9(5) COMP-3.                      
               10  TS42-NSEQENS          PIC S9(5) COMP-3.                      
               10  TS42-MPRIMECLI        PIC S9(7)V9(2) COMP-3.                 
           05 TS42-DOSSIER05.                                                   
               10  TS42-NDOSSIER5        PIC S9(3) COMP-3.                      
               10  TS42-CDOSSIER         PIC X(05).                             
               10  TS42-CTYPENT5         PIC X(02).                             
               10  TS42-NENTITE          PIC X(07).                             
               10  TS42-NSEQENS5         PIC S9(3) COMP-3.                      
               10  TS42-CAGENT           PIC X(02).                             
               10  TS42-DCREATION        PIC X(08).                             
               10  TS42-DMODIF5          PIC X(08).                             
               10  TS42-DANNUL           PIC X(08).                             
           05 TS42-DOSSIER06.                                                   
               10  TS42-NDOSSIER6        PIC S9(3) COMP-3.                      
               10  TS42-NSEQNQ6          PIC S9(5) COMP-3.                      
               10  TS42-NSEQ6            PIC S9(3) COMP-3.                      
           05 TS42-DOSSIER08.                                                   
               10  TS42-NDOSSIER8        PIC S9(3) COMP-3.                      
               10  TS42-NSEQ8            PIC S9(3) COMP-3.                      
               10  TS42-CCTRL            PIC X(05).                             
               10  TS42-LGCTRL           PIC S9(3) COMP-3.                      
               10  TS42-VALCTRL          PIC X(50).                             
               10  TS42-CAUTOR           PIC X(05).                             
               10  TS42-CJUSTIF          PIC X(05).                             
               10  TS42-DMODIF8          PIC X(08).                             
           05 TS42-ENTETE.                                                      
               10  TS42-E-DVENTE         PIC X(8).                              
               10  TS42-E-PTTVENTE       PIC S9(7)V9(0002) COMP-3.              
               10  TS42-E-PVERSE         PIC S9(7)V9(0002) COMP-3.              
               10  TS42-E-PCOMPT         PIC S9(7)V9(0002) COMP-3.              
               10  TS42-E-PLIVR          PIC S9(7)V9(0002) COMP-3.              
               10  TS42-E-PDIFFERE       PIC S9(7)V9(0002) COMP-3.              
               10  TS42-E-PRFACT         PIC S9(7)V9(0002) COMP-3.              
               10  TS42-E-WFACTURE       PIC X(1).                              
               10  TS42-E-WEXPORT        PIC X(1).                              
               10  TS42-E-WDETAXEC       PIC X(1).                              
               10  TS42-E-WDETAXEHC      PIC X(1).                              
               10  TS42-E-DSTAT          PIC X(8).                              
               10  TS42-E-CFCRED         PIC X(5).                              
               10  TS42-E-NCREDI         PIC X(14).                             
               10  TS42-E-NAUTO          PIC X(20).                             
               10  TS42-E-TYPVTE         PIC X(1).                              
               10  TS42-E-NSEQNQ         PIC S9(5) COMP-3.                      
               10  TS42-E-NSEQENS        PIC S9(5) COMP-3.                      
               10  TS42-E-LCOMVTE1       PIC X(30).                             
               10  TS42-E-LCOMVTE2       PIC X(30).                             
               10  TS42-E-LCOMVTE3       PIC X(30).                             
               10  TS42-E-LCOMVTE4       PIC X(30).                             
                                                                                
