      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVBS0500                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVBS0500                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBS0500.                                                    00000090
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBS0500.                                                            
      *}                                                                        
           10 BS05-CTYPENT1        PIC X(2).                            00000100
           10 BS05-NENTITE1        PIC X(7).                            00000110
           10 BS05-NSEQENT         PIC S9(5)V USAGE COMP-3.             00000120
           10 BS05-NBLOC           PIC S9(3)V USAGE COMP-3.             00000130
           10 BS05-NSEQ            PIC S9(3)V USAGE COMP-3.             00000140
           10 BS05-CQUALIF         PIC X(5).                            00000150
           10 BS05-WPRESEL         PIC X(1).                            00000160
           10 BS05-CLIEN           PIC X(2).                            00000170
           10 BS05-CTYPENT2        PIC X(2).                            00000180
           10 BS05-NENTITE2        PIC X(7).                            00000190
           10 BS05-CORIG           PIC X(5).                            00000200
           10 BS05-WACTIF          PIC X(1).                            00000210
           10 BS05-CTYPENTREF      PIC X(2).                            00000220
           10 BS05-NENTITEREF      PIC X(7).                            00000230
           10 BS05-NSEQENTREF      PIC S9(5)V USAGE COMP-3.             00000240
           10 BS05-DSYST           PIC S9(13)V USAGE COMP-3.            00000250
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00000260
      *---------------------------------------------------------        00000270
      *   LISTE DES FLAGS DE LA TABLE RVBS0500                          00000280
      *---------------------------------------------------------        00000290
      *                                                                 00000300
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBS0500-FLAGS.                                              00000310
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBS0500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS05-CTYPENT1-F      PIC S9(4) COMP.                      00000320
      *--                                                                       
           10 BS05-CTYPENT1-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS05-NENTITE1-F      PIC S9(4) COMP.                      00000330
      *--                                                                       
           10 BS05-NENTITE1-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS05-NSEQENT-F       PIC S9(4) COMP.                      00000340
      *--                                                                       
           10 BS05-NSEQENT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS05-NBLOC-F         PIC S9(4) COMP.                      00000350
      *--                                                                       
           10 BS05-NBLOC-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS05-NSEQ-F          PIC S9(4) COMP.                      00000360
      *--                                                                       
           10 BS05-NSEQ-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS05-CQUALIF-F       PIC S9(4) COMP.                      00000370
      *--                                                                       
           10 BS05-CQUALIF-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS05-WPRESEL-F       PIC S9(4) COMP.                      00000380
      *--                                                                       
           10 BS05-WPRESEL-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS05-CLIEN-F         PIC S9(4) COMP.                      00000390
      *--                                                                       
           10 BS05-CLIEN-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS05-CTYPENT2-F      PIC S9(4) COMP.                      00000400
      *--                                                                       
           10 BS05-CTYPENT2-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS05-NENTITE2-F      PIC S9(4) COMP.                      00000410
      *--                                                                       
           10 BS05-NENTITE2-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS05-CORIG-F         PIC S9(4) COMP.                      00000420
      *--                                                                       
           10 BS05-CORIG-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS05-WACTIF-F        PIC S9(4) COMP.                      00000430
      *--                                                                       
           10 BS05-WACTIF-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS05-CTYPENTREF-F    PIC S9(4) COMP.                      00000440
      *--                                                                       
           10 BS05-CTYPENTREF-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS05-NENTITEREF-F    PIC S9(4) COMP.                      00000450
      *--                                                                       
           10 BS05-NENTITEREF-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS05-NSEQENTREF-F    PIC S9(4) COMP.                      00000460
      *--                                                                       
           10 BS05-NSEQENTREF-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS05-DSYST-F         PIC S9(4) COMP.                      00000470
      *                                                                         
      *--                                                                       
           10 BS05-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
