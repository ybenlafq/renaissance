      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      **************************************************************            
      * COMMAREA SPECIFIQUE PRG  TGA02                   TR: GA02  *            
      *                DEFINITION D'UN CODE MARKETING              *            
      **************************************************************            
          02 COMM-GA02-APPLI REDEFINES COMM-GA00-APPLI.                         
             03 COMM-GA02-FONCT          PIC X(3).                              
             03 COMM-GA02-CMARKETING     PIC X(5).                              
             03 COMM-GA02-LMARKETING     PIC X(20).                             
             03 COMM-GA02-CDESCRIPTIF    PIC X(5).                              
             03 COMM-GA02-LDESCRIPTIF    PIC X(20).                             
             03 COMM-GA02-TRAIT          PIC X(1).                              
                88 COMM-GA02-TRAIT-1E-FOIS  VALUE '0'.                          
                88 COMM-GA02-TRAIT-N-FOIS   VALUE '1'.                          
             03 COMM-GA02-NB-ELT         PIC 9(3).                              
             03 COMM-GA02-ELT-COURS      PIC 9(3).                              
             03 COMM-GA02-NB-LIGNE-ECRAN PIC 9(3).                              
             03 COMM-GA02-FILLER         PIC X(3661).                           
                                                                                
