      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * CONSULTATION AU CODIC GG59                                      00000020
      ***************************************************************** 00000030
       01   EGG57I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGETOTL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNPAGETOTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNPAGETOTF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGETOTI     PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCIETEI     PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCFAMI    PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLFAMI    PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPROL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAPPROF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCAPPROI  PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNLIEUI   PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNZONPRIXL     COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MNZONPRIXL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNZONPRIXF     PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNZONPRIXI     PIC X(2).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCMARQI   PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLMARQI   PIC X(20).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPOL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXPOF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCEXPOI   PIC X.                                          00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNCODICI  PIC X(7).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLREFFOURNI    PIC X(20).                                 00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMPACL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCOMPACL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCOMPACF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCOMPACI  PIC X(3).                                       00000690
           02 LIGNEI OCCURS   12 TIMES .                                00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMOUCHARDL   COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MMOUCHARDL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MMOUCHARDF   PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MMOUCHARDI   PIC X.                                     00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCONCL  COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MCONCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCONCF  PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCONCI  PIC X(4).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCONCL      COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MLCONCL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLCONCF      PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MLCONCI      PIC X(20).                                 00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MDEFFETI     PIC X(8).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFINEFFETL   COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MFINEFFETL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MFINEFFETF   PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MFINEFFETI   PIC X(8).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPEXPTTCL    COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MPEXPTTCL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MPEXPTTCF    PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MPEXPTTCI    PIC X(9).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPSTDTTCL    COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MPSTDTTCL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MPSTDTTCF    PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MPSTDTTCI    PIC X(9).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEL  COMP PIC S9(4).                                 00000990
      *--                                                                       
             03 MDATEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDATEF  PIC X.                                          00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MDATEI  PIC X(8).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MLIBERRI  PIC X(74).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCODTRAI  PIC X(4).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCICSI    PIC X(5).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MNETNAMI  PIC X(8).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MSCREENI  PIC X(4).                                       00001220
      ***************************************************************** 00001230
      * CONSULTATION AU CODIC GG59                                      00001240
      ***************************************************************** 00001250
       01   EGG57O REDEFINES EGG57I.                                    00001260
           02 FILLER    PIC X(12).                                      00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MDATJOUA  PIC X.                                          00001290
           02 MDATJOUC  PIC X.                                          00001300
           02 MDATJOUP  PIC X.                                          00001310
           02 MDATJOUH  PIC X.                                          00001320
           02 MDATJOUV  PIC X.                                          00001330
           02 MDATJOUO  PIC X(10).                                      00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MTIMJOUA  PIC X.                                          00001360
           02 MTIMJOUC  PIC X.                                          00001370
           02 MTIMJOUP  PIC X.                                          00001380
           02 MTIMJOUH  PIC X.                                          00001390
           02 MTIMJOUV  PIC X.                                          00001400
           02 MTIMJOUO  PIC X(5).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MNPAGEA   PIC X.                                          00001430
           02 MNPAGEC   PIC X.                                          00001440
           02 MNPAGEP   PIC X.                                          00001450
           02 MNPAGEH   PIC X.                                          00001460
           02 MNPAGEV   PIC X.                                          00001470
           02 MNPAGEO   PIC X(3).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MNPAGETOTA     PIC X.                                     00001500
           02 MNPAGETOTC     PIC X.                                     00001510
           02 MNPAGETOTP     PIC X.                                     00001520
           02 MNPAGETOTH     PIC X.                                     00001530
           02 MNPAGETOTV     PIC X.                                     00001540
           02 MNPAGETOTO     PIC X(3).                                  00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MNSOCIETEA     PIC X.                                     00001570
           02 MNSOCIETEC     PIC X.                                     00001580
           02 MNSOCIETEP     PIC X.                                     00001590
           02 MNSOCIETEH     PIC X.                                     00001600
           02 MNSOCIETEV     PIC X.                                     00001610
           02 MNSOCIETEO     PIC X(3).                                  00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MCFAMA    PIC X.                                          00001640
           02 MCFAMC    PIC X.                                          00001650
           02 MCFAMP    PIC X.                                          00001660
           02 MCFAMH    PIC X.                                          00001670
           02 MCFAMV    PIC X.                                          00001680
           02 MCFAMO    PIC X(5).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MLFAMA    PIC X.                                          00001710
           02 MLFAMC    PIC X.                                          00001720
           02 MLFAMP    PIC X.                                          00001730
           02 MLFAMH    PIC X.                                          00001740
           02 MLFAMV    PIC X.                                          00001750
           02 MLFAMO    PIC X(20).                                      00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MCAPPROA  PIC X.                                          00001780
           02 MCAPPROC  PIC X.                                          00001790
           02 MCAPPROP  PIC X.                                          00001800
           02 MCAPPROH  PIC X.                                          00001810
           02 MCAPPROV  PIC X.                                          00001820
           02 MCAPPROO  PIC X(3).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MNLIEUA   PIC X.                                          00001850
           02 MNLIEUC   PIC X.                                          00001860
           02 MNLIEUP   PIC X.                                          00001870
           02 MNLIEUH   PIC X.                                          00001880
           02 MNLIEUV   PIC X.                                          00001890
           02 MNLIEUO   PIC X(3).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MNZONPRIXA     PIC X.                                     00001920
           02 MNZONPRIXC     PIC X.                                     00001930
           02 MNZONPRIXP     PIC X.                                     00001940
           02 MNZONPRIXH     PIC X.                                     00001950
           02 MNZONPRIXV     PIC X.                                     00001960
           02 MNZONPRIXO     PIC X(2).                                  00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MCMARQA   PIC X.                                          00001990
           02 MCMARQC   PIC X.                                          00002000
           02 MCMARQP   PIC X.                                          00002010
           02 MCMARQH   PIC X.                                          00002020
           02 MCMARQV   PIC X.                                          00002030
           02 MCMARQO   PIC X(5).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MLMARQA   PIC X.                                          00002060
           02 MLMARQC   PIC X.                                          00002070
           02 MLMARQP   PIC X.                                          00002080
           02 MLMARQH   PIC X.                                          00002090
           02 MLMARQV   PIC X.                                          00002100
           02 MLMARQO   PIC X(20).                                      00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MCEXPOA   PIC X.                                          00002130
           02 MCEXPOC   PIC X.                                          00002140
           02 MCEXPOP   PIC X.                                          00002150
           02 MCEXPOH   PIC X.                                          00002160
           02 MCEXPOV   PIC X.                                          00002170
           02 MCEXPOO   PIC X.                                          00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MNCODICA  PIC X.                                          00002200
           02 MNCODICC  PIC X.                                          00002210
           02 MNCODICP  PIC X.                                          00002220
           02 MNCODICH  PIC X.                                          00002230
           02 MNCODICV  PIC X.                                          00002240
           02 MNCODICO  PIC X(7).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MLREFFOURNA    PIC X.                                     00002270
           02 MLREFFOURNC    PIC X.                                     00002280
           02 MLREFFOURNP    PIC X.                                     00002290
           02 MLREFFOURNH    PIC X.                                     00002300
           02 MLREFFOURNV    PIC X.                                     00002310
           02 MLREFFOURNO    PIC X(20).                                 00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MCOMPACA  PIC X.                                          00002340
           02 MCOMPACC  PIC X.                                          00002350
           02 MCOMPACP  PIC X.                                          00002360
           02 MCOMPACH  PIC X.                                          00002370
           02 MCOMPACV  PIC X.                                          00002380
           02 MCOMPACO  PIC X(3).                                       00002390
           02 LIGNEO OCCURS   12 TIMES .                                00002400
             03 FILLER       PIC X(2).                                  00002410
             03 MMOUCHARDA   PIC X.                                     00002420
             03 MMOUCHARDC   PIC X.                                     00002430
             03 MMOUCHARDP   PIC X.                                     00002440
             03 MMOUCHARDH   PIC X.                                     00002450
             03 MMOUCHARDV   PIC X.                                     00002460
             03 MMOUCHARDO   PIC X.                                     00002470
             03 FILLER       PIC X(2).                                  00002480
             03 MCONCA  PIC X.                                          00002490
             03 MCONCC  PIC X.                                          00002500
             03 MCONCP  PIC X.                                          00002510
             03 MCONCH  PIC X.                                          00002520
             03 MCONCV  PIC X.                                          00002530
             03 MCONCO  PIC X(4).                                       00002540
             03 FILLER       PIC X(2).                                  00002550
             03 MLCONCA      PIC X.                                     00002560
             03 MLCONCC PIC X.                                          00002570
             03 MLCONCP PIC X.                                          00002580
             03 MLCONCH PIC X.                                          00002590
             03 MLCONCV PIC X.                                          00002600
             03 MLCONCO      PIC X(20).                                 00002610
             03 FILLER       PIC X(2).                                  00002620
             03 MDEFFETA     PIC X.                                     00002630
             03 MDEFFETC     PIC X.                                     00002640
             03 MDEFFETP     PIC X.                                     00002650
             03 MDEFFETH     PIC X.                                     00002660
             03 MDEFFETV     PIC X.                                     00002670
             03 MDEFFETO     PIC X(8).                                  00002680
             03 FILLER       PIC X(2).                                  00002690
             03 MFINEFFETA   PIC X.                                     00002700
             03 MFINEFFETC   PIC X.                                     00002710
             03 MFINEFFETP   PIC X.                                     00002720
             03 MFINEFFETH   PIC X.                                     00002730
             03 MFINEFFETV   PIC X.                                     00002740
             03 MFINEFFETO   PIC X(8).                                  00002750
             03 FILLER       PIC X(2).                                  00002760
             03 MPEXPTTCA    PIC X.                                     00002770
             03 MPEXPTTCC    PIC X.                                     00002780
             03 MPEXPTTCP    PIC X.                                     00002790
             03 MPEXPTTCH    PIC X.                                     00002800
             03 MPEXPTTCV    PIC X.                                     00002810
             03 MPEXPTTCO    PIC X(9).                                  00002820
             03 FILLER       PIC X(2).                                  00002830
             03 MPSTDTTCA    PIC X.                                     00002840
             03 MPSTDTTCC    PIC X.                                     00002850
             03 MPSTDTTCP    PIC X.                                     00002860
             03 MPSTDTTCH    PIC X.                                     00002870
             03 MPSTDTTCV    PIC X.                                     00002880
             03 MPSTDTTCO    PIC X(9).                                  00002890
             03 FILLER       PIC X(2).                                  00002900
             03 MDATEA  PIC X.                                          00002910
             03 MDATEC  PIC X.                                          00002920
             03 MDATEP  PIC X.                                          00002930
             03 MDATEH  PIC X.                                          00002940
             03 MDATEV  PIC X.                                          00002950
             03 MDATEO  PIC X(8).                                       00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MLIBERRA  PIC X.                                          00002980
           02 MLIBERRC  PIC X.                                          00002990
           02 MLIBERRP  PIC X.                                          00003000
           02 MLIBERRH  PIC X.                                          00003010
           02 MLIBERRV  PIC X.                                          00003020
           02 MLIBERRO  PIC X(74).                                      00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MCODTRAA  PIC X.                                          00003050
           02 MCODTRAC  PIC X.                                          00003060
           02 MCODTRAP  PIC X.                                          00003070
           02 MCODTRAH  PIC X.                                          00003080
           02 MCODTRAV  PIC X.                                          00003090
           02 MCODTRAO  PIC X(4).                                       00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MCICSA    PIC X.                                          00003120
           02 MCICSC    PIC X.                                          00003130
           02 MCICSP    PIC X.                                          00003140
           02 MCICSH    PIC X.                                          00003150
           02 MCICSV    PIC X.                                          00003160
           02 MCICSO    PIC X(5).                                       00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MNETNAMA  PIC X.                                          00003190
           02 MNETNAMC  PIC X.                                          00003200
           02 MNETNAMP  PIC X.                                          00003210
           02 MNETNAMH  PIC X.                                          00003220
           02 MNETNAMV  PIC X.                                          00003230
           02 MNETNAMO  PIC X(8).                                       00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MSCREENA  PIC X.                                          00003260
           02 MSCREENC  PIC X.                                          00003270
           02 MSCREENP  PIC X.                                          00003280
           02 MSCREENH  PIC X.                                          00003290
           02 MSCREENV  PIC X.                                          00003300
           02 MSCREENO  PIC X(4).                                       00003310
                                                                                
