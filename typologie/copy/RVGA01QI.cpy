      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE QCGAA ENTITES RTGA10 D ANALYSE         *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01QI.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01QI.                                                            
      *}                                                                        
           05  QCGAA-CTABLEG2    PIC X(15).                                     
           05  QCGAA-CTABLEG2-REDEF REDEFINES QCGAA-CTABLEG2.                   
               10  QCGAA-CENTITE         PIC X(05).                             
               10  QCGAA-NSEQ            PIC X(02).                             
           05  QCGAA-WTABLEG     PIC X(80).                                     
           05  QCGAA-WTABLEG-REDEF  REDEFINES QCGAA-WTABLEG.                    
               10  QCGAA-WACTIF          PIC X(01).                             
               10  QCGAA-NCISOC          PIC X(03).                             
               10  QCGAA-TYPES           PIC X(04).                             
               10  QCGAA-CTCARTE         PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01QI-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01QI-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QCGAA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  QCGAA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QCGAA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  QCGAA-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
