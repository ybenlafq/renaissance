      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGG1500                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGG1500                         
      **********************************************************                
       01  RVGG1500.                                                            
           02  GG15-NDEMALI                                                     
               PIC X(0007).                                                     
           02  GG15-NCODIC                                                      
               PIC X(0007).                                                     
           02  GG15-PCONC                                                       
               PIC S9(7) COMP-3.                                                
           02  GG15-WDECISION                                                   
               PIC X(0002).                                                     
           02  GG15-DDECISION                                                   
               PIC X(0008).                                                     
           02  GG15-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGG1500                                  
      **********************************************************                
       01  RVGG1500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG15-NDEMALI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG15-NDEMALI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG15-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG15-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG15-PCONC-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG15-PCONC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG15-WDECISION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG15-WDECISION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG15-DDECISION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG15-DDECISION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG15-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GG15-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
      *}                                                                        
