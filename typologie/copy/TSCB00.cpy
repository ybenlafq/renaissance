      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
                                                                                
      ******************************************************************        
      * ITSSC                                                                   
      * TS GESTION PRIX PRIMES                                                  
E0265 ******************************************************************        
      * DE02005 04/04/06 CODE INITIAL                                           
E0499 ******************************************************************        
      * DE02005 01/10/09 SUPPORT EVOLUTION D004416-17                           
      *                  PRIX ET PRIME AUTO CODIC GROUPE                        
      ******************************************************************        
      * CONSTANTES RELATIVES AU TRAITEMENT                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB-STAT-INI                   PIC S9(4) COMP VALUE +000.           
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-STAT-INI                   PIC S9(4) COMP-5 VALUE +000.         
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-STAT-INI                   PIC S9(4) COMP-5 VALUE +000.         
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB-STAT-NEW                   PIC S9(4) COMP VALUE +100.           
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-STAT-NEW                   PIC S9(4) COMP-5 VALUE +100.         
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-STAT-NEW                   PIC S9(4) COMP-5 VALUE +100.         
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB-STAT-SUP                   PIC S9(4) COMP VALUE -100.           
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-STAT-SUP                   PIC S9(4) COMP-5 VALUE -100.         
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-STAT-SUP                   PIC S9(4) COMP-5 VALUE -100.         
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB-STAT-PP-SUP                PIC S9(4) COMP VALUE -101.           
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-STAT-PP-SUP                PIC S9(4) COMP-5 VALUE -101.         
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-STAT-PP-SUP                PIC S9(4) COMP-5 VALUE -101.         
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB-STAT-PX-SUP                PIC S9(4) COMP VALUE -102.           
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-STAT-PX-SUP                PIC S9(4) COMP-5 VALUE -102.         
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-STAT-PX-SUP                PIC S9(4) COMP-5 VALUE -102.         
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB-STAT-PM-SUP                PIC S9(4) COMP VALUE -103.           
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-STAT-PM-SUP                PIC S9(4) COMP-5 VALUE -103.         
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-STAT-PM-SUP                PIC S9(4) COMP-5 VALUE -103.         
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
E0499 *77 TC-CB-STAT-INC                   PIC S9(4) COMP VALUE -200.           
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-STAT-INC                   PIC S9(4) COMP-5 VALUE -200.         
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-STAT-INC                   PIC S9(4) COMP-5 VALUE -200.         
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB-STAT-SEL                   PIC S9(4) COMP VALUE +010.           
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-STAT-SEL                   PIC S9(4) COMP-5 VALUE +010.         
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-STAT-SEL                   PIC S9(4) COMP-5 VALUE +010.         
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-DPERM                      PIC X(8) VALUE '99999999'.           
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-DPERM                      PIC X(8) VALUE '99999999'.           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-DPERM-AFF                  PIC X(8) VALUE '99/99/99'.           
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-DPERM-AFF                  PIC X(8) VALUE '99/99/99'.           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB-LIEU                       PIC S9(4) COMP VALUE 0.              
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-LIEU                       PIC S9(4) COMP-5 VALUE 0.            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-LIEU                       PIC S9(4) COMP-5 VALUE 0.            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB-MONTANT                    PIC S9(4) COMP VALUE 1.              
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-MONTANT                    PIC S9(4) COMP-5 VALUE 1.            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-MONTANT                    PIC S9(4) COMP-5 VALUE 1.            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB-SOC                        PIC S9(4) COMP VALUE 00.             
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-SOC                        PIC S9(4) COMP-5 VALUE 00.           
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-SOC                        PIC S9(4) COMP-5 VALUE 00.           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB-PP-NUL                     PIC S9(4) COMP VALUE 00.             
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-PP-NUL                     PIC S9(4) COMP-5 VALUE 00.           
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-PP-NUL                     PIC S9(4) COMP-5 VALUE 00.           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB-PX-REF                     PIC S9(4) COMP VALUE 01.             
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-PX-REF                     PIC S9(4) COMP-5 VALUE 01.           
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-PX-REF                     PIC S9(4) COMP-5 VALUE 01.           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB-OA-REF                     PIC S9(4) COMP VALUE 02.             
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-OA-REF                     PIC S9(4) COMP-5 VALUE 02.           
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-OA-REF                     PIC S9(4) COMP-5 VALUE 02.           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB-PM-REF                     PIC S9(4) COMP VALUE 03.             
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-PM-REF                     PIC S9(4) COMP-5 VALUE 03.           
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-PM-REF                     PIC S9(4) COMP-5 VALUE 03.           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB-ZONE                       PIC S9(4) COMP VALUE 04.             
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-ZONE                       PIC S9(4) COMP-5 VALUE 04.           
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-ZONE                       PIC S9(4) COMP-5 VALUE 04.           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB-PX-LOC                     PIC S9(4) COMP VALUE 05.             
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-PX-LOC                     PIC S9(4) COMP-5 VALUE 05.           
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-PX-LOC                     PIC S9(4) COMP-5 VALUE 05.           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB-OA-LOC                     PIC S9(4) COMP VALUE 06.             
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-OA-LOC                     PIC S9(4) COMP-5 VALUE 06.           
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-OA-LOC                     PIC S9(4) COMP-5 VALUE 06.           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB-PM-LOC                     PIC S9(4) COMP VALUE 07.             
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-PM-LOC                     PIC S9(4) COMP-5 VALUE 07.           
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-PM-LOC                     PIC S9(4) COMP-5 VALUE 07.           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB-MAG                        PIC S9(4) COMP VALUE 08.             
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-MAG                        PIC S9(4) COMP-5 VALUE 08.           
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-MAG                        PIC S9(4) COMP-5 VALUE 08.           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB-PX-EXC                     PIC S9(4) COMP VALUE 09.             
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-PX-EXC                     PIC S9(4) COMP-5 VALUE 09.           
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-PX-EXC                     PIC S9(4) COMP-5 VALUE 09.           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB-PM-EXC                     PIC S9(4) COMP VALUE 10.             
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-PM-EXC                     PIC S9(4) COMP-5 VALUE 10.           
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-PM-EXC                     PIC S9(4) COMP-5 VALUE 10.           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB-ST-NUL                     PIC S9(4) COMP VALUE 11.             
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-ST-NUL                     PIC S9(4) COMP-5 VALUE 11.           
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-ST-NUL                     PIC S9(4) COMP-5 VALUE 11.           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB-ST-OK                      PIC S9(4) COMP VALUE 12.             
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 TC-CB-ST-OK                      PIC S9(4) COMP-5 VALUE 12.           
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 TC-CB-ST-OK                      PIC S9(4) COMP-5 VALUE 12.           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      * STATUTS                                                                 
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 WC-STATUT-OK                     PIC S9(3) COMP-3 VALUE +020.         
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 WC-STATUT-OK                     PIC S9(3) COMP-3 VALUE +020.         
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 WC-STATUT-MOD                    PIC S9(3) COMP-3 VALUE +001.         
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 WC-STATUT-MOD                    PIC S9(3) COMP-3 VALUE +001.         
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 WC-STATUT-NOK                    PIC S9(3) COMP-3 VALUE -100.         
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 WC-STATUT-NOK                    PIC S9(3) COMP-3 VALUE -100.         
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 WC-STATUT-WARN                   PIC S9(3) COMP-3 VALUE +100.         
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 WC-STATUT-WARN                   PIC S9(3) COMP-3 VALUE +100.         
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 WC-STATUT-PROT                   PIC S9(3) COMP-3 VALUE +010.         
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 WC-STATUT-PROT                   PIC S9(3) COMP-3 VALUE +010.         
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 WC-STATUT-MDNT                   PIC S9(3) COMP-3 VALUE +011.         
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 WC-STATUT-MDNT                   PIC S9(3) COMP-3 VALUE +011.         
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      * VARIABLES RELATIVES AU TRAITEMENT                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 WB-CB-PP-TMP                         PIC S9(4) COMP VALUE 0.          
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 WB-CB-PP-TMP                         PIC S9(4) COMP-5 VALUE 0.        
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 WB-CB-PP-TMP                         PIC S9(4) COMP-5 VALUE 0.        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 WB-SEL-POS                           PIC S9(4) COMP VALUE 0.          
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 WB-SEL-POS                           PIC S9(4) COMP-5 VALUE 0.        
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 WB-SEL-POS                           PIC S9(4) COMP-5 VALUE 0.        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 WB-SEL-NB                            PIC S9(4) COMP VALUE 0.          
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 WB-SEL-NB                            PIC S9(4) COMP-5 VALUE 0.        
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 WB-SEL-NB                            PIC S9(4) COMP-5 VALUE 0.        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 WB-LI-ZP                             PIC S9(4) COMP VALUE 0.          
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 WB-LI-ZP                             PIC S9(4) COMP-5 VALUE 0.        
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 WB-LI-ZP                             PIC S9(4) COMP-5 VALUE 0.        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 WB-LI-REF                            PIC S9(4) COMP VALUE 0.          
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 WB-LI-REF                            PIC S9(4) COMP-5 VALUE 0.        
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 WB-LI-REF                            PIC S9(4) COMP-5 VALUE 0.        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 WB-LI-LIEU                           PIC S9(4) COMP VALUE 0.          
      *--                                                                       
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 WB-LI-LIEU                           PIC S9(4) COMP-5 VALUE 0.        
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 WB-LI-LIEU                           PIC S9(4) COMP-5 VALUE 0.        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 WP-SEL-REF                         PIC S9(3) COMP-3 VALUE 0.          
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 WP-SEL-REF                         PIC S9(3) COMP-3 VALUE 0.          
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 WP-SEL-LIE                         PIC S9(3) COMP-3 VALUE 0.          
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 WP-SEL-LIE                         PIC S9(3) COMP-3 VALUE 0.          
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       01 WF-SEL-MOD      PIC 9 VALUE 0.                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *    88 WC-SEL-TOG        VALUE 0.                                        
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           88 WC-SEL-TOG        VALUE 0.                                        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *    88 WC-SEL-SEL        VALUE 1.                                        
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           88 WC-SEL-SEL        VALUE 1.                                        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *    88 WC-SEL-DES        VALUE 2.                                        
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           88 WC-SEL-DES        VALUE 2.                                        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       01 WF-SEL          PIC 9 VALUE 0.                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *    88 WC-SEL-NONE       VALUE 0.                                        
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           88 WC-SEL-NONE       VALUE 0.                                        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *    88 WC-SEL-REFSEL     VALUE 1.                                        
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           88 WC-SEL-REFSEL     VALUE 1.                                        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *    88 WC-SEL-LIEDES     VALUE 2.                                        
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           88 WC-SEL-LIEDES     VALUE 2.                                        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      * DOIT CORRESPONDRE A LA ZONE TS-CB-PP-LIEU                               
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 WS-CB-LIEU.                                                           
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 WS-CB-LIEU.                                                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 WB-CB-PP-TYPE                    PIC S9(4)      COMP.             
      *--                                                                       
           05 WB-CB-PP-TYPE                    PIC S9(4) COMP-5.                
      *}                                                                        
           05 WS-CB-PP-SOC                     PIC X(3).                        
           05 WS-CB-PP-ZONE                    PIC X(2).                        
           05 WS-CB-PP-MAG                     PIC X(3).                        
           05 WS-CB-PP-LLIEU                   PIC X(10).                       
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      * DOIT CORRESPONDRE A LA ZONE TS-CB-PP-DERO                               
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 WS-CB-DERO.                                                           
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 WS-CB-DERO.                                                           
      *}                                                                        
           05 WS-CB-PP-DERO-PREF               PIC X.                           
           05 WS-CB-PP-DERO-MINI               PIC X.                           
           05 WS-CB-PP-DERO-SRP                PIC X.                           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      * DOIT CORRESPONDRE A LA ZONE TS-CB-PP-PX ET TS-CB-PP-PM                  
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 WS-CB-PP.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 WS-CB-PP.                                                             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 WB-CB-PX-TYPE                    PIC S9(4)      COMP.             
      *--                                                                       
           05 WB-CB-PX-TYPE                    PIC S9(4) COMP-5.                
      *}                                                                        
           05 WS-CB-PP-LIB                     PIC X(20).                       
           05 WS-CB-PP-DEFFET                  PIC X(8).                        
           05 WS-CB-PP-DFINEFFET               PIC X(8).                        
           05 WP-CB-PP-MONTANT                 PIC S9(5)V9(2) COMP-3.           
           05 WP-CB-PP-ADAP                    PIC S9(5)V9(2) COMP-3.           
           05 WS-CB-PP-LCOMMENT                PIC X(10).                       
           05 WS-CB-PP-CORIG                   PIC X.                           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      * DOIT CORRESPONDRE A LA ZONE TS-CB-PP-STAT                               
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 WS-CB-STAT.                                                           
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 WS-CB-STAT.                                                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 WB-CB-ST-TYPE                    PIC S9(4)      COMP.             
      *--                                                                       
           05 WB-CB-ST-TYPE                    PIC S9(4) COMP-5.                
      *}                                                                        
           05 WP-CB-PP-STOCK                   PIC S9(5)      COMP-3.           
           05 WP-CB-PP-V4S                     PIC S9(5)      COMP-3.           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      * TAILLE DOIT CORRESPONDRE A LA ZONE TS-CB-PP-PX                          
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 WS-CB-PX-DEF.                                                         
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 WS-CB-PX-DEF.                                                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 WB-CB-PX-DEF-TYPE          PIC S9(4) COMP.                        
      *--                                                                       
           05 WB-CB-PX-DEF-TYPE          PIC S9(4) COMP-5.                      
      *}                                                                        
           05 WS-CB-PX-DEF-LIB           PIC X(20).                             
           05 WS-CB-PX-DEF-DEFFET        PIC X(8).                              
           05 WS-CB-PX-DEF-DFINEFFET     PIC X(8).                              
           05 WP-CB-PX-DEF-MONTANT       PIC S9(5)V9(2) COMP-3.                 
           05 WP-CB-PX-DEF-ADAP          PIC S9(5)V9(2) COMP-3.                 
           05 WS-CB-PX-DEF-LCOMMENT      PIC X(10).                             
           05 WS-CB-PX-DEF-CORIG         PIC X.                                 
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      * TAILLE DOIT CORRESPONDRE A LA ZONE TS-CB-PP-CALCUL                      
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *77 WS-CB-CALCUL-DEF                     PIC X(66).                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       77 WS-CB-CALCUL-DEF                     PIC X(66).                       
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      * TAILLE DOIT CORRESPONDRE A LA ZONE TS-CB-PP-PM                          
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 WS-CB-PM-DEF.                                                         
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 WS-CB-PM-DEF.                                                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 WB-CB-PM-DEF-TYPE          PIC S9(4) COMP.                        
      *--                                                                       
           05 WB-CB-PM-DEF-TYPE          PIC S9(4) COMP-5.                      
      *}                                                                        
           05 WS-CB-PM-DEF-LIB           PIC X(20).                             
           05 WS-CB-PM-DEF-DEFFET        PIC X(8).                              
           05 WS-CB-PM-DEF-DFINEFFET     PIC X(8).                              
           05 WP-CB-PM-DEF-MONTANT       PIC S9(5)V9(2) COMP-3.                 
           05 WP-CB-PM-DEF-ADAP          PIC S9(5)V9(2) COMP-3.                 
           05 WS-CB-PM-DEF-LCOMMENT      PIC X(10).                             
           05 WS-CB-PM-DEF-CORIG         PIC X.                                 
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      * TS STOCKAGE                                                             
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 TS-CB-DATA.                                                           
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 TS-CB-DATA.                                                           
      *}                                                                        
           02 TS-CB-GESTION.                                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB-PP-POS                   PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB-PP-POS                   PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB-LI-POS                   PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB-LI-POS                   PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB-ZP-POS                   PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB-ZP-POS                   PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB-DP-POS                   PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB-DP-POS                   PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB-CO-POS                   PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB-CO-POS                   PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
E0499 *        05 TB-CB-GR-POS                   PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB-GR-POS                   PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
E0499 *        05 TB-CB-GC-POS                   PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB-GC-POS                   PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
E0499 *        05 TB-CB-GP-POS                   PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB-GP-POS                   PIC S9(4) COMP-5.              
      *}                                                                        
************************************************************************        
      *    TS PRIX ET PRIMES                                                    
           02 TS-CB-PP-ENR.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB-PP-STATUT                PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB-PP-STATUT                PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB-PP-SEL                   PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB-PP-SEL                   PIC S9(4) COMP-5.              
      *}                                                                        
      *        LIEU                                                             
               05 TS-CB-PP-LIEU.                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB-PP-TYPE              PIC S9(4) COMP.                
      *--                                                                       
                   10 TB-CB-PP-TYPE              PIC S9(4) COMP-5.              
      *}                                                                        
                   10 TS-CB-PP-SOC               PIC X(3).                      
                   10 TS-CB-PP-ZONE              PIC X(2).                      
                   10 TS-CB-PP-MAG               PIC X(3).                      
                   10 TS-CB-PP-LLIEU             PIC X(10).                     
      *        LIEN                                                             
               05 TS-CB-PP-LIEN.                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB-PP-REF               PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB-PP-REF               PIC S9(4) COMP-5.              
      *}                                                                        
      *        DEROGATION                                                       
               05 TS-CB-PP-DERO.                                                
                   10 TS-CB-PP-DERO-PREF         PIC X.                         
                   10 TS-CB-PP-DERO-MINI         PIC X.                         
                   10 TS-CB-PP-DERO-SRP          PIC X.                         
      *        PRIX                                                             
               05 TS-CB-PP-PX.                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB-PX-TYPE              PIC S9(4) COMP.                
      *--                                                                       
                   10 TB-CB-PX-TYPE              PIC S9(4) COMP-5.              
      *}                                                                        
                   10 TS-CB-PX-LIB               PIC X(20).                     
                   10 TS-CB-PX-DEFFET            PIC X(8).                      
                   10 TS-CB-PX-DFINEFFET         PIC X(8).                      
                   10 TP-CB-PX-MONTANT           PIC S9(5)V9(2) COMP-3.         
                   10 TP-CB-PX-ADAP              PIC S9(5)V9(2) COMP-3.         
                   10 TS-CB-PX-LCOMMENT          PIC X(10).                     
                   10 TS-CB-PX-CORIG             PIC X.                         
      *        OFFRE ACTIVE                                                     
               05 TS-CB-PP-OA.                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB-OA-TYPE              PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB-OA-TYPE              PIC S9(4) COMP-5.              
      *}                                                                        
                   10 TS-CB-OA-WOA               PIC X(1).                      
                   10 TS-CB-OA-DEFFET            PIC X(8).                      
                   10 TS-CB-OA-DFINEFFET         PIC X(8).                      
      *        056                                                              
      *        INDICES ET PRIMES CALCULEES                                      
               05 TS-CB-PP-CALCUL.                                              
                   10 TP-CB-PX-MB                PIC S9(5)V9(4) COMP-3.         
                   10 TP-CB-PX-MB-TX             PIC S9(3)V9(4) COMP-3.         
                   10 TP-CB-PX-MB-CESS           PIC S9(5)V9(4) COMP-3.         
                   10 TP-CB-PX-MB-CESS-TX        PIC S9(3)V9(4) COMP-3.         
                   10 TP-CB-PX-EDM-MB            PIC S9(5)V9(4) COMP-3.         
                   10 TP-CB-PX-EDM-MB-TX         PIC S9(2)V9(4) COMP-3.         
                   10 TP-CB-PX-EDM-CA            PIC S9(5)V9(4) COMP-3.         
                   10 TP-CB-PX-EDM-CA-TX         PIC S9(2)V9(4) COMP-3.         
                   10 TP-CB-PX-PM-CALCUL         PIC S9(5)V9(4) COMP-3.         
                   10 TP-CB-PX-PM-AJUST          PIC S9(5)V9(4) COMP-3.         
                   10 TP-CB-PX-PM-ADAP           PIC S9(5)V9(4) COMP-3.         
                   10 TP-CB-PX-PM-PAS            PIC S9(5)V9(4) COMP-3.         
                   10 TP-CB-PX-EDM-ADAP          PIC S9(5)V9(4) COMP-3.         
                   10 TP-CB-PX-EDM-SAISI         PIC S9(5)V9(4) COMP-3.         
      *        PRIMES                                                           
               05 TS-CB-PP-PM.                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB-PM-TYPE              PIC S9(4) COMP.                
      *--                                                                       
                   10 TB-CB-PM-TYPE              PIC S9(4) COMP-5.              
      *}                                                                        
                   10 TS-CB-PM-LIB               PIC X(20).                     
                   10 TS-CB-PM-DEFFET            PIC X(8).                      
                   10 TS-CB-PM-DFINEFFET         PIC X(8).                      
                   10 TP-CB-PM-MONTANT           PIC S9(5)V9(2) COMP-3.         
                   10 TP-CB-PM-ADAP              PIC S9(5)V9(2) COMP-3.         
                   10 TS-CB-PM-LCOMMENT          PIC X(10).                     
                   10 TS-CB-PM-CORIG             PIC X.                         
      *        STATS                                                            
               05 TS-CB-PP-STAT.                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB-ST-TYPE              PIC S9(4) COMP.                
      *--                                                                       
                   10 TB-CB-ST-TYPE              PIC S9(4) COMP-5.              
      *}                                                                        
                   10 TP-CB-PP-STOCK             PIC S9(5)      COMP-3.         
                   10 TP-CB-PP-V4S               PIC S9(5)      COMP-3.         
************************************************************************        
      *    TS ZONES DE PRIX                                                     
           02 TS-CB-ZP-ENR.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB-ZP-STATUT            PIC S9(4)      COMP.               
      *--                                                                       
               05 TB-CB-ZP-STATUT            PIC S9(4) COMP-5.                  
      *}                                                                        
               05 TS-CB-ZP-ZONE              PIC X(2).                          
      *        OFFRE ACTIVE                                                     
               05 TS-CB-ZP-OA.                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB-ZP-OA-TYPE           PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB-ZP-OA-TYPE           PIC S9(4) COMP-5.              
      *}                                                                        
                   10 TS-CB-ZP-OA-WOA            PIC X(1).                      
                   10 TS-CB-ZP-OA-DEFFET         PIC X(8).                      
                   10 TS-CB-ZP-OA-DFINEFFET      PIC X(8).                      
               05 TS-CB-ZP-PARM.                                                
                   10 TS-CB-ZP-TABLE-MB          PIC X(5).                      
                   10 TS-CB-ZP-TABLE-CA          PIC X(5).                      
                   10 TP-CB-ZP-PRIME-VOL         PIC S9(5)V9(2) COMP-3.         
                   10 TP-CB-ZP-PRIME-MINI        PIC S9(5)V9(2) COMP-3.         
                   10 TP-CB-ZP-PRIME-MAXI        PIC S9(5)V9(2) COMP-3.         
               05 TS-CB-ZP-COEFF.                                               
                   10 TP-CB-ZP-COEFF-TRANS       PIC S9(1)V9(6) COMP-3.         
                   10 TP-CB-ZP-COEFF-FAM         PIC S9(1)V9(6) COMP-3.         
                   10 TP-CB-ZP-MAJORATION        PIC S9(5)V9(2) COMP-3.         
************************************************************************        
      *    TS LIEUX                                                             
           02 TS-CB-LI-ENR.                                                     
      *        LIEN                                                             
               05 TS-CB-LI-DEF.                                                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB-LI-TYPE              PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB-LI-TYPE              PIC S9(4) COMP-5.              
      *}                                                                        
                   10 TS-CB-LI-LIEU              PIC X(3).                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB-LI-STATUT            PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB-LI-STATUT            PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB-LI-SEL               PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB-LI-SEL               PIC S9(4) COMP-5.              
      *}                                                                        
      *        LIEN                                                             
               05 TS-CB-LI-LIEN.                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB-LI-REF               PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB-LI-REF               PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB-LI-PP-MIN            PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB-LI-PP-MIN            PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB-LI-PP-MAX            PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB-LI-PP-MAX            PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB-LI-MIN               PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB-LI-MIN               PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB-LI-MAX               PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB-LI-MAX               PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB-LI-PP-LIE-MIN        PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB-LI-PP-LIE-MIN        PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB-LI-PP-LIE-MAX        PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB-LI-PP-LIE-MAX        PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB-LI-ACT               PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB-LI-ACT               PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB-LI-ZP                PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB-LI-ZP                PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB-LI-COURS             PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB-LI-COURS             PIC S9(4) COMP-5.              
      *}                                                                        
************************************************************************        
      *    TS DEPOTS                                                            
           02 TS-CB-DP-ENR.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB-DP-STATUT            PIC S9(4)      COMP.               
      *--                                                                       
               05 TB-CB-DP-STATUT            PIC S9(4) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB-DP-PRIORITE          PIC S9(4)      COMP.               
      *--                                                                       
               05 TB-CB-DP-PRIORITE          PIC S9(4) COMP-5.                  
      *}                                                                        
               05 TS-CB-DP-SOC               PIC X(3).                          
               05 TS-CB-DP-LIEU              PIC X(3).                          
               05 TS-CB-DP-CAPPRO            PIC X(5).                          
               05 TS-CB-DP-CEXPO             PIC X(5).                          
               05 TS-CB-DP-LSTATCOMP         PIC X(3).                          
               05 TP-CB-DP-STOCK             PIC S9(5)      COMP-3.             
               05 TP-CB-DP-CDE               PIC S9(5)      COMP-3.             
               05 TS-CB-DP-CDED              PIC X(8).                          
************************************************************************        
      *    TS COPR1                                                             
           02 TS-CB-CO-ENR.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB-CO-STATUT            PIC S9(4)      COMP.               
      *--                                                                       
               05 TB-CB-CO-STATUT            PIC S9(4) COMP-5.                  
      *}                                                                        
               05 TS-CB-CO-SOC               PIC X(3).                          
               05 TP-CB-CO-PAS               PIC S9(3)V9(2)  COMP-3.            
E0499-******************************************************************        
      *    TS GROUPES                                                           
           02 TS-CB-GR-ENR.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB-GR-STATUT            PIC S9(4)      COMP.               
      *--                                                                       
               05 TB-CB-GR-STATUT            PIC S9(4) COMP-5.                  
      *}                                                                        
      *        PRODUIT                                                          
               05 TS-CB-GR-PRODUIT.                                             
                   10 TS-CB-GR-CFAM              PIC X(5).                      
                   10 TS-CB-GR-NCODIC            PIC X(7).                      
                   10 TS-CB-GR-CMARQ             PIC X(5).                      
                   10 TS-CB-GR-LREFFOURN         PIC X(20).                     
                   10 TS-CB-GR-CAPPRO            PIC X(5).                      
                   10 TS-CB-GR-LSTATCOMP         PIC X(3).                      
                   10 TS-CB-GR-INCLF             PIC X(1).                      
                   10 TP-CB-GR-TVA               PIC S9(3)V9(2) COMP-3.         
                   10 TP-CB-GR-COEFF-CESS        PIC S9(1)V9(6) COMP-3.         
                   10 TP-CB-GR-SEUIL-HT          PIC S9(5)V9(2) COMP-3.         
                   10 TP-CB-GR-SEUIL-TTC         PIC S9(5)V9(2) COMP-3.         
                   10 TP-CB-GR-REVIENT           PIC S9(5)V9(2) COMP-3.         
                   10 TP-CB-GR-QTYPLIEN          PIC S9(3) COMP-3.              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB-GR-LIEN              PIC S9(4) COMP.                
      *--                                                                       
                   10 TB-CB-GR-LIEN              PIC S9(4) COMP-5.              
      *}                                                                        
C0510              10 TS-CB-GR-NCODICLIE         PIC X(7).                      
      *    TS GROUPES INFO PRIX                                                 
           02 TS-CB-GP-ENR.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB-GP-STATUT            PIC S9(4)      COMP.               
      *--                                                                       
               05 TB-CB-GP-STATUT            PIC S9(4) COMP-5.                  
      *}                                                                        
               05 TS-CB-GP-NCODIC            PIC X(7).                          
               05 TS-CB-GP-LIEU              PIC X(3).                          
      *        PRIX                                                             
!!!!! *        ATTENTION ZONE LIEE DANS MCB00                                   
               05 TS-CB-GP.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB-GP-TYPE              PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB-GP-TYPE              PIC S9(4) COMP-5.              
      *}                                                                        
                   10 TS-CB-GP-DEFFET            PIC X(8).                      
                   10 TS-CB-GP-DFINEFFET         PIC X(8).                      
                   10 TS-CB-GP-TYPE              PIC X(3).                      
                   10 TP-CB-GP-MONTANT           PIC S9(5)V9(2) COMP-3.         
                   10 TS-CB-GP-FLAG              PIC X(1).                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB-GP-LIEN                  PIC S9(4) COMP.                
      *--                                                                       
               05 TB-CB-GP-LIEN                  PIC S9(4) COMP-5.              
      *}                                                                        
-E0499         05 TS-CB-GP-LIB                   PIC X(20).                     
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
