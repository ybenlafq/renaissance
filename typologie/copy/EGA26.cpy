      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA26   EGA26                                              00000020
      ***************************************************************** 00000030
       01   EGA26I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCETATL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCETATL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCETATF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCETATI   PIC X(10).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLETATL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLETATL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLETATF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLETATI   PIC X(54).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPEL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MTYPEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTYPEF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MTYPEI    PIC X.                                          00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPEL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLTYPEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLTYPEF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLTYPEI   PIC X(11).                                      00000370
           02 MSTRUCTUREI OCCURS   11 TIMES .                           00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MCFAMI  PIC X(5).                                       00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MBORNE1L     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MBORNE1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MBORNE1F     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MBORNE1I     PIC X(5).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MBORNE2L     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MBORNE2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MBORNE2F     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MBORNE2I     PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MBORNE3L     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MBORNE3L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MBORNE3F     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MBORNE3I     PIC X(5).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MBORNE4L     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MBORNE4L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MBORNE4F     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MBORNE4I     PIC X(5).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MBORNE5L     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MBORNE5L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MBORNE5F     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MBORNE5I     PIC X(5).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MBORNE6L     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MBORNE6L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MBORNE6F     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MBORNE6I     PIC X(5).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MBORNE7L     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MBORNE7L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MBORNE7F     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MBORNE7I     PIC X(5).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MBORNE8L     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MBORNE8L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MBORNE8F     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MBORNE8I     PIC X(5).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MBORNE9L     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MBORNE9L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MBORNE9F     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MBORNE9I     PIC X(5).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MBORNE10L    COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MBORNE10L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MBORNE10F    PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MBORNE10I    PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MZONCMDI  PIC X(15).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MLIBERRI  PIC X(58).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCODTRAI  PIC X(4).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCICSI    PIC X(5).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MNETNAMI  PIC X(8).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MSCREENI  PIC X(4).                                       00001060
      ***************************************************************** 00001070
      * SDF: EGA26   EGA26                                              00001080
      ***************************************************************** 00001090
       01   EGA26O REDEFINES EGA26I.                                    00001100
           02 FILLER    PIC X(12).                                      00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MDATJOUA  PIC X.                                          00001130
           02 MDATJOUC  PIC X.                                          00001140
           02 MDATJOUP  PIC X.                                          00001150
           02 MDATJOUH  PIC X.                                          00001160
           02 MDATJOUV  PIC X.                                          00001170
           02 MDATJOUO  PIC X(10).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MTIMJOUA  PIC X.                                          00001200
           02 MTIMJOUC  PIC X.                                          00001210
           02 MTIMJOUP  PIC X.                                          00001220
           02 MTIMJOUH  PIC X.                                          00001230
           02 MTIMJOUV  PIC X.                                          00001240
           02 MTIMJOUO  PIC X(5).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MWPAGEA   PIC X.                                          00001270
           02 MWPAGEC   PIC X.                                          00001280
           02 MWPAGEP   PIC X.                                          00001290
           02 MWPAGEH   PIC X.                                          00001300
           02 MWPAGEV   PIC X.                                          00001310
           02 MWPAGEO   PIC X(2).                                       00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MWFONCA   PIC X.                                          00001340
           02 MWFONCC   PIC X.                                          00001350
           02 MWFONCP   PIC X.                                          00001360
           02 MWFONCH   PIC X.                                          00001370
           02 MWFONCV   PIC X.                                          00001380
           02 MWFONCO   PIC X(3).                                       00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MCETATA   PIC X.                                          00001410
           02 MCETATC   PIC X.                                          00001420
           02 MCETATP   PIC X.                                          00001430
           02 MCETATH   PIC X.                                          00001440
           02 MCETATV   PIC X.                                          00001450
           02 MCETATO   PIC X(10).                                      00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MLETATA   PIC X.                                          00001480
           02 MLETATC   PIC X.                                          00001490
           02 MLETATP   PIC X.                                          00001500
           02 MLETATH   PIC X.                                          00001510
           02 MLETATV   PIC X.                                          00001520
           02 MLETATO   PIC X(54).                                      00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MTYPEA    PIC X.                                          00001550
           02 MTYPEC    PIC X.                                          00001560
           02 MTYPEP    PIC X.                                          00001570
           02 MTYPEH    PIC X.                                          00001580
           02 MTYPEV    PIC X.                                          00001590
           02 MTYPEO    PIC X.                                          00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MLTYPEA   PIC X.                                          00001620
           02 MLTYPEC   PIC X.                                          00001630
           02 MLTYPEP   PIC X.                                          00001640
           02 MLTYPEH   PIC X.                                          00001650
           02 MLTYPEV   PIC X.                                          00001660
           02 MLTYPEO   PIC X(11).                                      00001670
           02 MSTRUCTUREO OCCURS   11 TIMES .                           00001680
             03 FILLER       PIC X(2).                                  00001690
             03 MCFAMA  PIC X.                                          00001700
             03 MCFAMC  PIC X.                                          00001710
             03 MCFAMP  PIC X.                                          00001720
             03 MCFAMH  PIC X.                                          00001730
             03 MCFAMV  PIC X.                                          00001740
             03 MCFAMO  PIC X(5).                                       00001750
             03 FILLER       PIC X(2).                                  00001760
             03 MBORNE1A     PIC X.                                     00001770
             03 MBORNE1C     PIC X.                                     00001780
             03 MBORNE1P     PIC X.                                     00001790
             03 MBORNE1H     PIC X.                                     00001800
             03 MBORNE1V     PIC X.                                     00001810
             03 MBORNE1O     PIC X(5).                                  00001820
             03 FILLER       PIC X(2).                                  00001830
             03 MBORNE2A     PIC X.                                     00001840
             03 MBORNE2C     PIC X.                                     00001850
             03 MBORNE2P     PIC X.                                     00001860
             03 MBORNE2H     PIC X.                                     00001870
             03 MBORNE2V     PIC X.                                     00001880
             03 MBORNE2O     PIC X(5).                                  00001890
             03 FILLER       PIC X(2).                                  00001900
             03 MBORNE3A     PIC X.                                     00001910
             03 MBORNE3C     PIC X.                                     00001920
             03 MBORNE3P     PIC X.                                     00001930
             03 MBORNE3H     PIC X.                                     00001940
             03 MBORNE3V     PIC X.                                     00001950
             03 MBORNE3O     PIC X(5).                                  00001960
             03 FILLER       PIC X(2).                                  00001970
             03 MBORNE4A     PIC X.                                     00001980
             03 MBORNE4C     PIC X.                                     00001990
             03 MBORNE4P     PIC X.                                     00002000
             03 MBORNE4H     PIC X.                                     00002010
             03 MBORNE4V     PIC X.                                     00002020
             03 MBORNE4O     PIC X(5).                                  00002030
             03 FILLER       PIC X(2).                                  00002040
             03 MBORNE5A     PIC X.                                     00002050
             03 MBORNE5C     PIC X.                                     00002060
             03 MBORNE5P     PIC X.                                     00002070
             03 MBORNE5H     PIC X.                                     00002080
             03 MBORNE5V     PIC X.                                     00002090
             03 MBORNE5O     PIC X(5).                                  00002100
             03 FILLER       PIC X(2).                                  00002110
             03 MBORNE6A     PIC X.                                     00002120
             03 MBORNE6C     PIC X.                                     00002130
             03 MBORNE6P     PIC X.                                     00002140
             03 MBORNE6H     PIC X.                                     00002150
             03 MBORNE6V     PIC X.                                     00002160
             03 MBORNE6O     PIC X(5).                                  00002170
             03 FILLER       PIC X(2).                                  00002180
             03 MBORNE7A     PIC X.                                     00002190
             03 MBORNE7C     PIC X.                                     00002200
             03 MBORNE7P     PIC X.                                     00002210
             03 MBORNE7H     PIC X.                                     00002220
             03 MBORNE7V     PIC X.                                     00002230
             03 MBORNE7O     PIC X(5).                                  00002240
             03 FILLER       PIC X(2).                                  00002250
             03 MBORNE8A     PIC X.                                     00002260
             03 MBORNE8C     PIC X.                                     00002270
             03 MBORNE8P     PIC X.                                     00002280
             03 MBORNE8H     PIC X.                                     00002290
             03 MBORNE8V     PIC X.                                     00002300
             03 MBORNE8O     PIC X(5).                                  00002310
             03 FILLER       PIC X(2).                                  00002320
             03 MBORNE9A     PIC X.                                     00002330
             03 MBORNE9C     PIC X.                                     00002340
             03 MBORNE9P     PIC X.                                     00002350
             03 MBORNE9H     PIC X.                                     00002360
             03 MBORNE9V     PIC X.                                     00002370
             03 MBORNE9O     PIC X(5).                                  00002380
             03 FILLER       PIC X(2).                                  00002390
             03 MBORNE10A    PIC X.                                     00002400
             03 MBORNE10C    PIC X.                                     00002410
             03 MBORNE10P    PIC X.                                     00002420
             03 MBORNE10H    PIC X.                                     00002430
             03 MBORNE10V    PIC X.                                     00002440
             03 MBORNE10O    PIC X(5).                                  00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MZONCMDA  PIC X.                                          00002470
           02 MZONCMDC  PIC X.                                          00002480
           02 MZONCMDP  PIC X.                                          00002490
           02 MZONCMDH  PIC X.                                          00002500
           02 MZONCMDV  PIC X.                                          00002510
           02 MZONCMDO  PIC X(15).                                      00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MLIBERRA  PIC X.                                          00002540
           02 MLIBERRC  PIC X.                                          00002550
           02 MLIBERRP  PIC X.                                          00002560
           02 MLIBERRH  PIC X.                                          00002570
           02 MLIBERRV  PIC X.                                          00002580
           02 MLIBERRO  PIC X(58).                                      00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MCODTRAA  PIC X.                                          00002610
           02 MCODTRAC  PIC X.                                          00002620
           02 MCODTRAP  PIC X.                                          00002630
           02 MCODTRAH  PIC X.                                          00002640
           02 MCODTRAV  PIC X.                                          00002650
           02 MCODTRAO  PIC X(4).                                       00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MCICSA    PIC X.                                          00002680
           02 MCICSC    PIC X.                                          00002690
           02 MCICSP    PIC X.                                          00002700
           02 MCICSH    PIC X.                                          00002710
           02 MCICSV    PIC X.                                          00002720
           02 MCICSO    PIC X(5).                                       00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MNETNAMA  PIC X.                                          00002750
           02 MNETNAMC  PIC X.                                          00002760
           02 MNETNAMP  PIC X.                                          00002770
           02 MNETNAMH  PIC X.                                          00002780
           02 MNETNAMV  PIC X.                                          00002790
           02 MNETNAMO  PIC X(8).                                       00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MSCREENA  PIC X.                                          00002820
           02 MSCREENC  PIC X.                                          00002830
           02 MSCREENP  PIC X.                                          00002840
           02 MSCREENH  PIC X.                                          00002850
           02 MSCREENV  PIC X.                                          00002860
           02 MSCREENO  PIC X(4).                                       00002870
                                                                                
