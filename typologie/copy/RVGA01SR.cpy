      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EMDG  ATTRIBUTION GOULOTTES RAF EMD    *        
      *----------------------------------------------------------------*        
       01  RVGA01SR.                                                            
           05  EMDG-CTABLEG2     PIC X(15).                                     
           05  EMDG-CTABLEG2-REDEF  REDEFINES EMDG-CTABLEG2.                    
               10  EMDG-NSOCDEP          PIC X(06).                             
               10  EMDG-NSOCDEP-N       REDEFINES EMDG-NSOCDEP                  
                                         PIC 9(06).                             
           05  EMDG-WTABLEG      PIC X(80).                                     
           05  EMDG-WTABLEG-REDEF   REDEFINES EMDG-WTABLEG.                     
               10  EMDG-QNBPG            PIC X(05).                             
               10  EMDG-QNBPG-N         REDEFINES EMDG-QNBPG                    
                                         PIC 9(05).                             
               10  EMDG-QNBMG            PIC X(05).                             
               10  EMDG-QNBMG-N         REDEFINES EMDG-QNBMG                    
                                         PIC 9(05).                             
               10  EMDG-QNBGM            PIC X(05).                             
               10  EMDG-QNBGM-N         REDEFINES EMDG-QNBGM                    
                                         PIC 9(05).                             
               10  EMDG-QNBG             PIC X(05).                             
               10  EMDG-QNBG-N          REDEFINES EMDG-QNBG                     
                                         PIC 9(05).                             
               10  EMDG-QNBPMIN          PIC X(05).                             
               10  EMDG-QNBPMIN-N       REDEFINES EMDG-QNBPMIN                  
                                         PIC 9(05).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01SR-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EMDG-CTABLEG2-F   PIC S9(4)  COMP.                               
      *--                                                                       
           05  EMDG-CTABLEG2-F   PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EMDG-WTABLEG-F    PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EMDG-WTABLEG-F    PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
