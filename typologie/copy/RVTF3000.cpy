      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVTF3000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVTF3000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTF3000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTF3000.                                                            
      *}                                                                        
           02  TF30-LFICHIER                                                    
               PIC X(0008).                                                     
           02  TF30-CRITERE                                                     
               PIC X(0030).                                                     
           02  TF30-DCREATION                                                   
               PIC X(0008).                                                     
           02  TF30-NSEQ                                                        
               PIC S9(7) COMP-3.                                                
           02  TF30-DONNEES1                                                    
               PIC X(0128).                                                     
           02  TF30-DONNEES2                                                    
               PIC X(0128).                                                     
           02  TF30-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVTF3000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTF3000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTF3000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TF30-LFICHIER-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TF30-LFICHIER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TF30-CRITERE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TF30-CRITERE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TF30-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TF30-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TF30-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TF30-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TF30-DONNEE1-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TF30-DONNEE1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TF30-DONNEE2-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TF30-DONNEE2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TF30-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TF30-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
