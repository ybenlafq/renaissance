      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGA9300                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA9300                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA9300.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA9300.                                                            
      *}                                                                        
           02  GA93-CETAT                                                       
               PIC X(0010).                                                     
           02  GA93-WSEQPRO                                                     
               PIC S9(7) COMP-3.                                                
           02  GA93-AGRPR1                                                      
               PIC X(0005).                                                     
           02  GA93-AGRPR2                                                      
               PIC X(0005).                                                     
           02  GA93-AGRPR3                                                      
               PIC X(0005).                                                     
           02  GA93-AGRPR4                                                      
               PIC X(0005).                                                     
           02  GA93-AGRPR5                                                      
               PIC X(0005).                                                     
           02  GA93-AGRPR6                                                      
               PIC X(0005).                                                     
           02  GA93-AGRPR7                                                      
               PIC X(0005).                                                     
           02  GA93-AGRPR8                                                      
               PIC X(0005).                                                     
           02  GA93-AGRPR9                                                      
               PIC X(0005).                                                     
           02  GA93-AGRPR10                                                     
               PIC X(0005).                                                     
           02  GA93-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGA9300                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA9300-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA9300-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA93-CETAT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA93-CETAT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA93-WSEQPRO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA93-WSEQPRO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA93-AGRPR1-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA93-AGRPR1-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA93-AGRPR2-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA93-AGRPR2-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA93-AGRPR3-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA93-AGRPR3-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA93-AGRPR4-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA93-AGRPR4-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA93-AGRPR5-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA93-AGRPR5-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA93-AGRPR6-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA93-AGRPR6-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA93-AGRPR7-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA93-AGRPR7-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA93-AGRPR8-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA93-AGRPR8-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA93-AGRPR9-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA93-AGRPR9-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA93-AGRPR10-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA93-AGRPR10-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA93-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA93-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
