      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * INTERROGATION RELEVES PRIX                                      00000020
      ***************************************************************** 00000030
       01   ERX55I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(2).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(2).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000170
           02 FILLER    PIC X(2).                                       00000180
           02 MNPAGEI   PIC X(4).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(2).                                       00000220
           02 MNBPI     PIC X(4).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCL     COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MSOCL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSOCF     PIC X.                                          00000250
           02 FILLER    PIC X(2).                                       00000260
           02 MSOCI     PIC X(3).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIZPL     COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MIZPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MIZPF     PIC X.                                          00000290
           02 FILLER    PIC X(2).                                       00000300
           02 MIZPI     PIC X(2).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIMAGL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MIMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MIMAGF    PIC X.                                          00000330
           02 FILLER    PIC X(2).                                       00000340
           02 MIMAGI    PIC X(3).                                       00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MICONCL   COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MICONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MICONCF   PIC X.                                          00000370
           02 FILLER    PIC X(2).                                       00000380
           02 MICONCI   PIC X(4).                                       00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MILENSCOL      COMP PIC S9(4).                            00000400
      *--                                                                       
           02 MILENSCOL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MILENSCOF      PIC X.                                     00000410
           02 FILLER    PIC X(2).                                       00000420
           02 MILENSCOI      PIC X(15).                                 00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MICRAYONL      COMP PIC S9(4).                            00000440
      *--                                                                       
           02 MICRAYONL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MICRAYONF      PIC X.                                     00000450
           02 FILLER    PIC X(2).                                       00000460
           02 MICRAYONI      PIC X(5).                                  00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MICODICL  COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MICODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MICODICF  PIC X.                                          00000490
           02 FILLER    PIC X(2).                                       00000500
           02 MICODICI  PIC X(7).                                       00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIRELEVEL      COMP PIC S9(4).                            00000520
      *--                                                                       
           02 MIRELEVEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MIRELEVEF      PIC X.                                     00000530
           02 FILLER    PIC X(2).                                       00000540
           02 MIRELEVEI      PIC X(7).                                  00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MITYPEL   COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MITYPEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MITYPEF   PIC X.                                          00000570
           02 FILLER    PIC X(2).                                       00000580
           02 MITYPEI   PIC X(4).                                       00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIOPERL   COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MIOPERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MIOPERF   PIC X.                                          00000610
           02 FILLER    PIC X(2).                                       00000620
           02 MIOPERI   PIC X.                                          00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIDATEL   COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MIDATEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MIDATEF   PIC X.                                          00000650
           02 FILLER    PIC X(2).                                       00000660
           02 MIDATEI   PIC X(8).                                       00000670
           02 MTABI OCCURS   16 TIMES .                                 00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZPL    COMP PIC S9(4).                                 00000690
      *--                                                                       
             03 MZPL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MZPF    PIC X.                                          00000700
             03 FILLER  PIC X(2).                                       00000710
             03 MZPI    PIC X(2).                                       00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMAGL   COMP PIC S9(4).                                 00000730
      *--                                                                       
             03 MMAGL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MMAGF   PIC X.                                          00000740
             03 FILLER  PIC X(2).                                       00000750
             03 MMAGI   PIC X(3).                                       00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCONCL  COMP PIC S9(4).                                 00000770
      *--                                                                       
             03 MCONCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCONCF  PIC X.                                          00000780
             03 FILLER  PIC X(2).                                       00000790
             03 MCONCI  PIC X(4).                                       00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLENSCOL     COMP PIC S9(4).                            00000810
      *--                                                                       
             03 MLENSCOL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLENSCOF     PIC X.                                     00000820
             03 FILLER  PIC X(2).                                       00000830
             03 MLENSCOI     PIC X(15).                                 00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000850
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000860
             03 FILLER  PIC X(2).                                       00000870
             03 MCFAMI  PIC X(5).                                       00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODICL      COMP PIC S9(4).                            00000890
      *--                                                                       
             03 MCODICL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCODICF      PIC X.                                     00000900
             03 FILLER  PIC X(2).                                       00000910
             03 MCODICI      PIC X(7).                                  00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRELEVEL     COMP PIC S9(4).                            00000930
      *--                                                                       
             03 MRELEVEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MRELEVEF     PIC X.                                     00000940
             03 FILLER  PIC X(2).                                       00000950
             03 MRELEVEI     PIC X(7).                                  00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPEL  COMP PIC S9(4).                                 00000970
      *--                                                                       
             03 MTYPEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTYPEF  PIC X.                                          00000980
             03 FILLER  PIC X(2).                                       00000990
             03 MTYPEI  PIC X(4).                                       00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEL  COMP PIC S9(4).                                 00001010
      *--                                                                       
             03 MDATEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDATEF  PIC X.                                          00001020
             03 FILLER  PIC X(2).                                       00001030
             03 MDATEI  PIC X(8).                                       00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIXL  COMP PIC S9(4).                                 00001050
      *--                                                                       
             03 MPRIXL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MPRIXF  PIC X.                                          00001060
             03 FILLER  PIC X(2).                                       00001070
             03 MPRIXI  PIC X(9).                                       00001080
      * ZONE CMD AIDA                                                   00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MLIBERRI  PIC X(79).                                      00001130
      * CODE TRANSACTION                                                00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MCODTRAI  PIC X(4).                                       00001180
      * CICS DE TRAVAIL                                                 00001190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001200
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MCICSI    PIC X(5).                                       00001230
      * NETNAME                                                         00001240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001250
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MNETNAMI  PIC X(8).                                       00001280
      * CODE TERMINAL                                                   00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MSCREENI  PIC X(4).                                       00001330
      ***************************************************************** 00001340
      * INTERROGATION RELEVES PRIX                                      00001350
      ***************************************************************** 00001360
       01   ERX55O REDEFINES ERX55I.                                    00001370
           02 FILLER    PIC X(12).                                      00001380
      * DATE DU JOUR                                                    00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MDATJOUA  PIC X.                                          00001410
           02 MDATJOUC  PIC X.                                          00001420
           02 MDATJOUH  PIC X.                                          00001430
           02 MDATJOUO  PIC X(10).                                      00001440
      * HEURE                                                           00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MTIMJOUA  PIC X.                                          00001470
           02 MTIMJOUC  PIC X.                                          00001480
           02 MTIMJOUH  PIC X.                                          00001490
           02 MTIMJOUO  PIC X(5).                                       00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MNPAGEA   PIC X.                                          00001520
           02 MNPAGEC   PIC X.                                          00001530
           02 MNPAGEH   PIC X.                                          00001540
           02 MNPAGEO   PIC X(4).                                       00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MNBPA     PIC X.                                          00001570
           02 MNBPC     PIC X.                                          00001580
           02 MNBPH     PIC X.                                          00001590
           02 MNBPO     PIC X(4).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MSOCA     PIC X.                                          00001620
           02 MSOCC     PIC X.                                          00001630
           02 MSOCH     PIC X.                                          00001640
           02 MSOCO     PIC X(3).                                       00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MIZPA     PIC X.                                          00001670
           02 MIZPC     PIC X.                                          00001680
           02 MIZPH     PIC X.                                          00001690
           02 MIZPO     PIC X(2).                                       00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MIMAGA    PIC X.                                          00001720
           02 MIMAGC    PIC X.                                          00001730
           02 MIMAGH    PIC X.                                          00001740
           02 MIMAGO    PIC X(3).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MICONCA   PIC X.                                          00001770
           02 MICONCC   PIC X.                                          00001780
           02 MICONCH   PIC X.                                          00001790
           02 MICONCO   PIC X(4).                                       00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MILENSCOA      PIC X.                                     00001820
           02 MILENSCOC PIC X.                                          00001830
           02 MILENSCOH PIC X.                                          00001840
           02 MILENSCOO      PIC X(15).                                 00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MICRAYONA      PIC X.                                     00001870
           02 MICRAYONC PIC X.                                          00001880
           02 MICRAYONH PIC X.                                          00001890
           02 MICRAYONO      PIC X(5).                                  00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MICODICA  PIC X.                                          00001920
           02 MICODICC  PIC X.                                          00001930
           02 MICODICH  PIC X.                                          00001940
           02 MICODICO  PIC X(7).                                       00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MIRELEVEA      PIC X.                                     00001970
           02 MIRELEVEC PIC X.                                          00001980
           02 MIRELEVEH PIC X.                                          00001990
           02 MIRELEVEO      PIC X(7).                                  00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MITYPEA   PIC X.                                          00002020
           02 MITYPEC   PIC X.                                          00002030
           02 MITYPEH   PIC X.                                          00002040
           02 MITYPEO   PIC X(4).                                       00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MIOPERA   PIC X.                                          00002070
           02 MIOPERC   PIC X.                                          00002080
           02 MIOPERH   PIC X.                                          00002090
           02 MIOPERO   PIC X.                                          00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MIDATEA   PIC X.                                          00002120
           02 MIDATEC   PIC X.                                          00002130
           02 MIDATEH   PIC X.                                          00002140
           02 MIDATEO   PIC X(8).                                       00002150
           02 MTABO OCCURS   16 TIMES .                                 00002160
             03 FILLER       PIC X(2).                                  00002170
             03 MZPA    PIC X.                                          00002180
             03 MZPC    PIC X.                                          00002190
             03 MZPH    PIC X.                                          00002200
             03 MZPO    PIC X(2).                                       00002210
             03 FILLER       PIC X(2).                                  00002220
             03 MMAGA   PIC X.                                          00002230
             03 MMAGC   PIC X.                                          00002240
             03 MMAGH   PIC X.                                          00002250
             03 MMAGO   PIC X(3).                                       00002260
             03 FILLER       PIC X(2).                                  00002270
             03 MCONCA  PIC X.                                          00002280
             03 MCONCC  PIC X.                                          00002290
             03 MCONCH  PIC X.                                          00002300
             03 MCONCO  PIC X(4).                                       00002310
             03 FILLER       PIC X(2).                                  00002320
             03 MLENSCOA     PIC X.                                     00002330
             03 MLENSCOC     PIC X.                                     00002340
             03 MLENSCOH     PIC X.                                     00002350
             03 MLENSCOO     PIC X(15).                                 00002360
             03 FILLER       PIC X(2).                                  00002370
             03 MCFAMA  PIC X.                                          00002380
             03 MCFAMC  PIC X.                                          00002390
             03 MCFAMH  PIC X.                                          00002400
             03 MCFAMO  PIC X(5).                                       00002410
             03 FILLER       PIC X(2).                                  00002420
             03 MCODICA      PIC X.                                     00002430
             03 MCODICC PIC X.                                          00002440
             03 MCODICH PIC X.                                          00002450
             03 MCODICO      PIC X(7).                                  00002460
             03 FILLER       PIC X(2).                                  00002470
             03 MRELEVEA     PIC X.                                     00002480
             03 MRELEVEC     PIC X.                                     00002490
             03 MRELEVEH     PIC X.                                     00002500
             03 MRELEVEO     PIC X(7).                                  00002510
             03 FILLER       PIC X(2).                                  00002520
             03 MTYPEA  PIC X.                                          00002530
             03 MTYPEC  PIC X.                                          00002540
             03 MTYPEH  PIC X.                                          00002550
             03 MTYPEO  PIC X(4).                                       00002560
             03 FILLER       PIC X(2).                                  00002570
             03 MDATEA  PIC X.                                          00002580
             03 MDATEC  PIC X.                                          00002590
             03 MDATEH  PIC X.                                          00002600
             03 MDATEO  PIC X(8).                                       00002610
             03 FILLER       PIC X(2).                                  00002620
             03 MPRIXA  PIC X.                                          00002630
             03 MPRIXC  PIC X.                                          00002640
             03 MPRIXH  PIC X.                                          00002650
             03 MPRIXO  PIC X(9).                                       00002660
      * ZONE CMD AIDA                                                   00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MLIBERRA  PIC X.                                          00002690
           02 MLIBERRC  PIC X.                                          00002700
           02 MLIBERRH  PIC X.                                          00002710
           02 MLIBERRO  PIC X(79).                                      00002720
      * CODE TRANSACTION                                                00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MCODTRAA  PIC X.                                          00002750
           02 MCODTRAC  PIC X.                                          00002760
           02 MCODTRAH  PIC X.                                          00002770
           02 MCODTRAO  PIC X(4).                                       00002780
      * CICS DE TRAVAIL                                                 00002790
           02 FILLER    PIC X(2).                                       00002800
           02 MCICSA    PIC X.                                          00002810
           02 MCICSC    PIC X.                                          00002820
           02 MCICSH    PIC X.                                          00002830
           02 MCICSO    PIC X(5).                                       00002840
      * NETNAME                                                         00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MNETNAMA  PIC X.                                          00002870
           02 MNETNAMC  PIC X.                                          00002880
           02 MNETNAMH  PIC X.                                          00002890
           02 MNETNAMO  PIC X(8).                                       00002900
      * CODE TERMINAL                                                   00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MSCREENA  PIC X.                                          00002930
           02 MSCREENC  PIC X.                                          00002940
           02 MSCREENH  PIC X.                                          00002950
           02 MSCREENO  PIC X(4).                                       00002960
                                                                                
