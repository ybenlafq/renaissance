      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA55   EGA55                                              00000020
      ***************************************************************** 00000030
       01   EGA55I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNCODICI  PIC X(7).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLREFI    PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFAML    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNFAMF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNFAMI    PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLFAMI    PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMARQL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNMARQF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNMARQI   PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLMARQI   PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOURCINL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MSOURCINL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOURCINF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MSOURCINI      PIC X(5).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBSOURL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MLIBSOURL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBSOURF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLIBSOURI      PIC X(20).                                 00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCHEFPL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNCHEFPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCHEFPF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNCHEFPI  PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCHEFPL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLCHEFPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCHEFPF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLCHEFPI  PIC X(20).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNORIGNL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNORIGNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNORIGNF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNORIGNI  PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLORIGNL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLORIGNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLORIGNF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLORIGNI  PIC X(20).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCUSINEL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCUSINEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCUSINEF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCUSINEI  PIC X(5).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLUSINEL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MLUSINEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLUSINEF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLUSINEI  PIC X(20).                                      00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQDELAIL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MQDELAIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQDELAIF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MQDELAII  PIC X(3).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQUNITCL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MQUNITCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQUNITCF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MQUNITCI  PIC X(3).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWSTAVL   COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MWSTAVL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWSTAVF   PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MWSTAVI   PIC X.                                          00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBNDL   COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MLIBNDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIBNDF   PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MLIBNDI   PIC X(23).                                      00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCNOMDOUL      COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MCNOMDOUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCNOMDOUF      PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCNOMDOUI      PIC X(8).                                  00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOMDOUL      COMP PIC S9(4).                            00000940
      *--                                                                       
           02 MLNOMDOUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLNOMDOUF      PIC X.                                     00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MLNOMDOUI      PIC X(40).                                 00000970
           02 MTABLEI OCCURS   5 TIMES .                                00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLLDOUL      COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MLLDOUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLLDOUF      PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MLLDOUI      PIC X(50).                                 00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MZONCMDI  PIC X(15).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MLIBERRI  PIC X(58).                                      00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCODTRAI  PIC X(4).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCICSI    PIC X(5).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MNETNAMI  PIC X(8).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MSCREENI  PIC X(4).                                       00001260
      ***************************************************************** 00001270
      * SDF: EGA55   EGA55                                              00001280
      ***************************************************************** 00001290
       01   EGA55O REDEFINES EGA55I.                                    00001300
           02 FILLER    PIC X(12).                                      00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MDATJOUA  PIC X.                                          00001330
           02 MDATJOUC  PIC X.                                          00001340
           02 MDATJOUP  PIC X.                                          00001350
           02 MDATJOUH  PIC X.                                          00001360
           02 MDATJOUV  PIC X.                                          00001370
           02 MDATJOUO  PIC X(10).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MTIMJOUA  PIC X.                                          00001400
           02 MTIMJOUC  PIC X.                                          00001410
           02 MTIMJOUP  PIC X.                                          00001420
           02 MTIMJOUH  PIC X.                                          00001430
           02 MTIMJOUV  PIC X.                                          00001440
           02 MTIMJOUO  PIC X(5).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MWFONCA   PIC X.                                          00001470
           02 MWFONCC   PIC X.                                          00001480
           02 MWFONCP   PIC X.                                          00001490
           02 MWFONCH   PIC X.                                          00001500
           02 MWFONCV   PIC X.                                          00001510
           02 MWFONCO   PIC X(3).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MNCODICA  PIC X.                                          00001540
           02 MNCODICC  PIC X.                                          00001550
           02 MNCODICP  PIC X.                                          00001560
           02 MNCODICH  PIC X.                                          00001570
           02 MNCODICV  PIC X.                                          00001580
           02 MNCODICO  PIC X(7).                                       00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MLREFA    PIC X.                                          00001610
           02 MLREFC    PIC X.                                          00001620
           02 MLREFP    PIC X.                                          00001630
           02 MLREFH    PIC X.                                          00001640
           02 MLREFV    PIC X.                                          00001650
           02 MLREFO    PIC X(20).                                      00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MNFAMA    PIC X.                                          00001680
           02 MNFAMC    PIC X.                                          00001690
           02 MNFAMP    PIC X.                                          00001700
           02 MNFAMH    PIC X.                                          00001710
           02 MNFAMV    PIC X.                                          00001720
           02 MNFAMO    PIC X(5).                                       00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MLFAMA    PIC X.                                          00001750
           02 MLFAMC    PIC X.                                          00001760
           02 MLFAMP    PIC X.                                          00001770
           02 MLFAMH    PIC X.                                          00001780
           02 MLFAMV    PIC X.                                          00001790
           02 MLFAMO    PIC X(20).                                      00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MNMARQA   PIC X.                                          00001820
           02 MNMARQC   PIC X.                                          00001830
           02 MNMARQP   PIC X.                                          00001840
           02 MNMARQH   PIC X.                                          00001850
           02 MNMARQV   PIC X.                                          00001860
           02 MNMARQO   PIC X(5).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MLMARQA   PIC X.                                          00001890
           02 MLMARQC   PIC X.                                          00001900
           02 MLMARQP   PIC X.                                          00001910
           02 MLMARQH   PIC X.                                          00001920
           02 MLMARQV   PIC X.                                          00001930
           02 MLMARQO   PIC X(20).                                      00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MSOURCINA      PIC X.                                     00001960
           02 MSOURCINC PIC X.                                          00001970
           02 MSOURCINP PIC X.                                          00001980
           02 MSOURCINH PIC X.                                          00001990
           02 MSOURCINV PIC X.                                          00002000
           02 MSOURCINO      PIC X(5).                                  00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MLIBSOURA      PIC X.                                     00002030
           02 MLIBSOURC PIC X.                                          00002040
           02 MLIBSOURP PIC X.                                          00002050
           02 MLIBSOURH PIC X.                                          00002060
           02 MLIBSOURV PIC X.                                          00002070
           02 MLIBSOURO      PIC X(20).                                 00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MNCHEFPA  PIC X.                                          00002100
           02 MNCHEFPC  PIC X.                                          00002110
           02 MNCHEFPP  PIC X.                                          00002120
           02 MNCHEFPH  PIC X.                                          00002130
           02 MNCHEFPV  PIC X.                                          00002140
           02 MNCHEFPO  PIC X(5).                                       00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MLCHEFPA  PIC X.                                          00002170
           02 MLCHEFPC  PIC X.                                          00002180
           02 MLCHEFPP  PIC X.                                          00002190
           02 MLCHEFPH  PIC X.                                          00002200
           02 MLCHEFPV  PIC X.                                          00002210
           02 MLCHEFPO  PIC X(20).                                      00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MNORIGNA  PIC X.                                          00002240
           02 MNORIGNC  PIC X.                                          00002250
           02 MNORIGNP  PIC X.                                          00002260
           02 MNORIGNH  PIC X.                                          00002270
           02 MNORIGNV  PIC X.                                          00002280
           02 MNORIGNO  PIC X(5).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MLORIGNA  PIC X.                                          00002310
           02 MLORIGNC  PIC X.                                          00002320
           02 MLORIGNP  PIC X.                                          00002330
           02 MLORIGNH  PIC X.                                          00002340
           02 MLORIGNV  PIC X.                                          00002350
           02 MLORIGNO  PIC X(20).                                      00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MCUSINEA  PIC X.                                          00002380
           02 MCUSINEC  PIC X.                                          00002390
           02 MCUSINEP  PIC X.                                          00002400
           02 MCUSINEH  PIC X.                                          00002410
           02 MCUSINEV  PIC X.                                          00002420
           02 MCUSINEO  PIC X(5).                                       00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MLUSINEA  PIC X.                                          00002450
           02 MLUSINEC  PIC X.                                          00002460
           02 MLUSINEP  PIC X.                                          00002470
           02 MLUSINEH  PIC X.                                          00002480
           02 MLUSINEV  PIC X.                                          00002490
           02 MLUSINEO  PIC X(20).                                      00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MQDELAIA  PIC X.                                          00002520
           02 MQDELAIC  PIC X.                                          00002530
           02 MQDELAIP  PIC X.                                          00002540
           02 MQDELAIH  PIC X.                                          00002550
           02 MQDELAIV  PIC X.                                          00002560
           02 MQDELAIO  PIC X(3).                                       00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MQUNITCA  PIC X.                                          00002590
           02 MQUNITCC  PIC X.                                          00002600
           02 MQUNITCP  PIC X.                                          00002610
           02 MQUNITCH  PIC X.                                          00002620
           02 MQUNITCV  PIC X.                                          00002630
           02 MQUNITCO  PIC X(3).                                       00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MWSTAVA   PIC X.                                          00002660
           02 MWSTAVC   PIC X.                                          00002670
           02 MWSTAVP   PIC X.                                          00002680
           02 MWSTAVH   PIC X.                                          00002690
           02 MWSTAVV   PIC X.                                          00002700
           02 MWSTAVO   PIC X.                                          00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MLIBNDA   PIC X.                                          00002730
           02 MLIBNDC   PIC X.                                          00002740
           02 MLIBNDP   PIC X.                                          00002750
           02 MLIBNDH   PIC X.                                          00002760
           02 MLIBNDV   PIC X.                                          00002770
           02 MLIBNDO   PIC X(23).                                      00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MCNOMDOUA      PIC X.                                     00002800
           02 MCNOMDOUC PIC X.                                          00002810
           02 MCNOMDOUP PIC X.                                          00002820
           02 MCNOMDOUH PIC X.                                          00002830
           02 MCNOMDOUV PIC X.                                          00002840
           02 MCNOMDOUO      PIC X(8).                                  00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MLNOMDOUA      PIC X.                                     00002870
           02 MLNOMDOUC PIC X.                                          00002880
           02 MLNOMDOUP PIC X.                                          00002890
           02 MLNOMDOUH PIC X.                                          00002900
           02 MLNOMDOUV PIC X.                                          00002910
           02 MLNOMDOUO      PIC X(40).                                 00002920
           02 MTABLEO OCCURS   5 TIMES .                                00002930
             03 FILLER       PIC X(2).                                  00002940
             03 MLLDOUA      PIC X.                                     00002950
             03 MLLDOUC PIC X.                                          00002960
             03 MLLDOUP PIC X.                                          00002970
             03 MLLDOUH PIC X.                                          00002980
             03 MLLDOUV PIC X.                                          00002990
             03 MLLDOUO      PIC X(50).                                 00003000
           02 FILLER    PIC X(2).                                       00003010
           02 MZONCMDA  PIC X.                                          00003020
           02 MZONCMDC  PIC X.                                          00003030
           02 MZONCMDP  PIC X.                                          00003040
           02 MZONCMDH  PIC X.                                          00003050
           02 MZONCMDV  PIC X.                                          00003060
           02 MZONCMDO  PIC X(15).                                      00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MLIBERRA  PIC X.                                          00003090
           02 MLIBERRC  PIC X.                                          00003100
           02 MLIBERRP  PIC X.                                          00003110
           02 MLIBERRH  PIC X.                                          00003120
           02 MLIBERRV  PIC X.                                          00003130
           02 MLIBERRO  PIC X(58).                                      00003140
           02 FILLER    PIC X(2).                                       00003150
           02 MCODTRAA  PIC X.                                          00003160
           02 MCODTRAC  PIC X.                                          00003170
           02 MCODTRAP  PIC X.                                          00003180
           02 MCODTRAH  PIC X.                                          00003190
           02 MCODTRAV  PIC X.                                          00003200
           02 MCODTRAO  PIC X(4).                                       00003210
           02 FILLER    PIC X(2).                                       00003220
           02 MCICSA    PIC X.                                          00003230
           02 MCICSC    PIC X.                                          00003240
           02 MCICSP    PIC X.                                          00003250
           02 MCICSH    PIC X.                                          00003260
           02 MCICSV    PIC X.                                          00003270
           02 MCICSO    PIC X(5).                                       00003280
           02 FILLER    PIC X(2).                                       00003290
           02 MNETNAMA  PIC X.                                          00003300
           02 MNETNAMC  PIC X.                                          00003310
           02 MNETNAMP  PIC X.                                          00003320
           02 MNETNAMH  PIC X.                                          00003330
           02 MNETNAMV  PIC X.                                          00003340
           02 MNETNAMO  PIC X(8).                                       00003350
           02 FILLER    PIC X(2).                                       00003360
           02 MSCREENA  PIC X.                                          00003370
           02 MSCREENC  PIC X.                                          00003380
           02 MSCREENP  PIC X.                                          00003390
           02 MSCREENH  PIC X.                                          00003400
           02 MSCREENV  PIC X.                                          00003410
           02 MSCREENO  PIC X(4).                                       00003420
                                                                                
