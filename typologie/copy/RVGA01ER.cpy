      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE LIVRD ZONE DE LIVRAISON / DETAIL       *        
      *----------------------------------------------------------------*        
       01  RVGA01ER.                                                            
           05  LIVRD-CTABLEG2    PIC X(15).                                     
           05  LIVRD-CTABLEG2-REDEF REDEFINES LIVRD-CTABLEG2.                   
               10  LIVRD-CZONLIV         PIC X(05).                             
               10  LIVRD-CZONELE         PIC X(05).                             
           05  LIVRD-WTABLEG     PIC X(80).                                     
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01ER-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LIVRD-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  LIVRD-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LIVRD-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  LIVRD-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
