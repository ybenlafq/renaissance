      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA67   EGA67                                              00000020
      ***************************************************************** 00000030
       01   EGA67I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETOTL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGETOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGETOTF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGETOTI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MWFONCI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCODICI  PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLREFI    PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNFAMI    PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLFAMI    PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPREST1L      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MCPREST1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPREST1F      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCPREST1I      PIC X(5).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEN1L   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLIEN1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIEN1F   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLIEN1I   PIC X.                                          00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMARQL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNMARQF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNMARQI   PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLMARQI   PIC X(20).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFOURN1L      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MCFOURN1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCFOURN1F      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCFOURN1I      PIC X(5).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFOURN1L      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MLFOURN1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLFOURN1F      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLFOURN1I      PIC X(18).                                 00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODIC1L      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MNCODIC1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODIC1F      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNCODIC1I      PIC X(7).                                  00000690
           02 MLIGNEI OCCURS   10 TIMES .                               00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPRESTL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MCPRESTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCPRESTF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MCPRESTI     PIC X(5).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLPRESTL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MLPRESTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLPRESTF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MLPRESTI     PIC X(20).                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFOURNL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MCFOURNL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCFOURNF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MCFOURNI     PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPEL  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MTYPEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTYPEF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MTYPEI  PIC X.                                          00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMTPRESTL    COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MMTPRESTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MMTPRESTF    PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MMTPRESTI    PIC X(9).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMTPRIMEL    COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MMTPRIMEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MMTPRIMEF    PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MMTPRIMEI    PIC X(5).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIENAL      COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MLIENAL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLIENAF      PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MLIENAI      PIC X.                                     00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETAL    COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MDEFFETAL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDEFFETAF    PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MDEFFETAI    PIC X(10).                                 00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIENL  COMP PIC S9(4).                                 00001030
      *--                                                                       
             03 MLIENL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLIENF  PIC X.                                          00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MLIENI  PIC X.                                          00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MDEFFETI     PIC X(10).                                 00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MZONCMDI  PIC X(15).                                      00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MLIBERRI  PIC X(58).                                      00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MCODTRAI  PIC X(4).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MCICSI    PIC X(5).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MNETNAMI  PIC X(8).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MSCREENI  PIC X(4).                                       00001340
      ***************************************************************** 00001350
      * SDF: EGA67   EGA67                                              00001360
      ***************************************************************** 00001370
       01   EGA67O REDEFINES EGA67I.                                    00001380
           02 FILLER    PIC X(12).                                      00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MDATJOUA  PIC X.                                          00001410
           02 MDATJOUC  PIC X.                                          00001420
           02 MDATJOUP  PIC X.                                          00001430
           02 MDATJOUH  PIC X.                                          00001440
           02 MDATJOUV  PIC X.                                          00001450
           02 MDATJOUO  PIC X(10).                                      00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MTIMJOUA  PIC X.                                          00001480
           02 MTIMJOUC  PIC X.                                          00001490
           02 MTIMJOUP  PIC X.                                          00001500
           02 MTIMJOUH  PIC X.                                          00001510
           02 MTIMJOUV  PIC X.                                          00001520
           02 MTIMJOUO  PIC X(5).                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MPAGEA    PIC X.                                          00001550
           02 MPAGEC    PIC X.                                          00001560
           02 MPAGEP    PIC X.                                          00001570
           02 MPAGEH    PIC X.                                          00001580
           02 MPAGEV    PIC X.                                          00001590
           02 MPAGEO    PIC ZZ.                                         00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MPAGETOTA      PIC X.                                     00001620
           02 MPAGETOTC PIC X.                                          00001630
           02 MPAGETOTP PIC X.                                          00001640
           02 MPAGETOTH PIC X.                                          00001650
           02 MPAGETOTV PIC X.                                          00001660
           02 MPAGETOTO      PIC ZZ.                                    00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MWFONCA   PIC X.                                          00001690
           02 MWFONCC   PIC X.                                          00001700
           02 MWFONCP   PIC X.                                          00001710
           02 MWFONCH   PIC X.                                          00001720
           02 MWFONCV   PIC X.                                          00001730
           02 MWFONCO   PIC X(3).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MNCODICA  PIC X.                                          00001760
           02 MNCODICC  PIC X.                                          00001770
           02 MNCODICP  PIC X.                                          00001780
           02 MNCODICH  PIC X.                                          00001790
           02 MNCODICV  PIC X.                                          00001800
           02 MNCODICO  PIC X(7).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MLREFA    PIC X.                                          00001830
           02 MLREFC    PIC X.                                          00001840
           02 MLREFP    PIC X.                                          00001850
           02 MLREFH    PIC X.                                          00001860
           02 MLREFV    PIC X.                                          00001870
           02 MLREFO    PIC X(20).                                      00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MNFAMA    PIC X.                                          00001900
           02 MNFAMC    PIC X.                                          00001910
           02 MNFAMP    PIC X.                                          00001920
           02 MNFAMH    PIC X.                                          00001930
           02 MNFAMV    PIC X.                                          00001940
           02 MNFAMO    PIC X(5).                                       00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MLFAMA    PIC X.                                          00001970
           02 MLFAMC    PIC X.                                          00001980
           02 MLFAMP    PIC X.                                          00001990
           02 MLFAMH    PIC X.                                          00002000
           02 MLFAMV    PIC X.                                          00002010
           02 MLFAMO    PIC X(20).                                      00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MCPREST1A      PIC X.                                     00002040
           02 MCPREST1C PIC X.                                          00002050
           02 MCPREST1P PIC X.                                          00002060
           02 MCPREST1H PIC X.                                          00002070
           02 MCPREST1V PIC X.                                          00002080
           02 MCPREST1O      PIC X(5).                                  00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MLIEN1A   PIC X.                                          00002110
           02 MLIEN1C   PIC X.                                          00002120
           02 MLIEN1P   PIC X.                                          00002130
           02 MLIEN1H   PIC X.                                          00002140
           02 MLIEN1V   PIC X.                                          00002150
           02 MLIEN1O   PIC X.                                          00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MNMARQA   PIC X.                                          00002180
           02 MNMARQC   PIC X.                                          00002190
           02 MNMARQP   PIC X.                                          00002200
           02 MNMARQH   PIC X.                                          00002210
           02 MNMARQV   PIC X.                                          00002220
           02 MNMARQO   PIC X(5).                                       00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MLMARQA   PIC X.                                          00002250
           02 MLMARQC   PIC X.                                          00002260
           02 MLMARQP   PIC X.                                          00002270
           02 MLMARQH   PIC X.                                          00002280
           02 MLMARQV   PIC X.                                          00002290
           02 MLMARQO   PIC X(20).                                      00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MCFOURN1A      PIC X.                                     00002320
           02 MCFOURN1C PIC X.                                          00002330
           02 MCFOURN1P PIC X.                                          00002340
           02 MCFOURN1H PIC X.                                          00002350
           02 MCFOURN1V PIC X.                                          00002360
           02 MCFOURN1O      PIC X(5).                                  00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MLFOURN1A      PIC X.                                     00002390
           02 MLFOURN1C PIC X.                                          00002400
           02 MLFOURN1P PIC X.                                          00002410
           02 MLFOURN1H PIC X.                                          00002420
           02 MLFOURN1V PIC X.                                          00002430
           02 MLFOURN1O      PIC X(18).                                 00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MNCODIC1A      PIC X.                                     00002460
           02 MNCODIC1C PIC X.                                          00002470
           02 MNCODIC1P PIC X.                                          00002480
           02 MNCODIC1H PIC X.                                          00002490
           02 MNCODIC1V PIC X.                                          00002500
           02 MNCODIC1O      PIC X(7).                                  00002510
           02 MLIGNEO OCCURS   10 TIMES .                               00002520
             03 FILLER       PIC X(2).                                  00002530
             03 MCPRESTA     PIC X.                                     00002540
             03 MCPRESTC     PIC X.                                     00002550
             03 MCPRESTP     PIC X.                                     00002560
             03 MCPRESTH     PIC X.                                     00002570
             03 MCPRESTV     PIC X.                                     00002580
             03 MCPRESTO     PIC X(5).                                  00002590
             03 FILLER       PIC X(2).                                  00002600
             03 MLPRESTA     PIC X.                                     00002610
             03 MLPRESTC     PIC X.                                     00002620
             03 MLPRESTP     PIC X.                                     00002630
             03 MLPRESTH     PIC X.                                     00002640
             03 MLPRESTV     PIC X.                                     00002650
             03 MLPRESTO     PIC X(20).                                 00002660
             03 FILLER       PIC X(2).                                  00002670
             03 MCFOURNA     PIC X.                                     00002680
             03 MCFOURNC     PIC X.                                     00002690
             03 MCFOURNP     PIC X.                                     00002700
             03 MCFOURNH     PIC X.                                     00002710
             03 MCFOURNV     PIC X.                                     00002720
             03 MCFOURNO     PIC X(5).                                  00002730
             03 FILLER       PIC X(2).                                  00002740
             03 MTYPEA  PIC X.                                          00002750
             03 MTYPEC  PIC X.                                          00002760
             03 MTYPEP  PIC X.                                          00002770
             03 MTYPEH  PIC X.                                          00002780
             03 MTYPEV  PIC X.                                          00002790
             03 MTYPEO  PIC X.                                          00002800
             03 FILLER       PIC X(2).                                  00002810
             03 MMTPRESTA    PIC X.                                     00002820
             03 MMTPRESTC    PIC X.                                     00002830
             03 MMTPRESTP    PIC X.                                     00002840
             03 MMTPRESTH    PIC X.                                     00002850
             03 MMTPRESTV    PIC X.                                     00002860
             03 MMTPRESTO    PIC -(5)9,99.                              00002870
             03 FILLER       PIC X(2).                                  00002880
             03 MMTPRIMEA    PIC X.                                     00002890
             03 MMTPRIMEC    PIC X.                                     00002900
             03 MMTPRIMEP    PIC X.                                     00002910
             03 MMTPRIMEH    PIC X.                                     00002920
             03 MMTPRIMEV    PIC X.                                     00002930
             03 MMTPRIMEO    PIC 99,99.                                 00002940
             03 FILLER       PIC X(2).                                  00002950
             03 MLIENAA      PIC X.                                     00002960
             03 MLIENAC PIC X.                                          00002970
             03 MLIENAP PIC X.                                          00002980
             03 MLIENAH PIC X.                                          00002990
             03 MLIENAV PIC X.                                          00003000
             03 MLIENAO      PIC X.                                     00003010
             03 FILLER       PIC X(2).                                  00003020
             03 MDEFFETAA    PIC X.                                     00003030
             03 MDEFFETAC    PIC X.                                     00003040
             03 MDEFFETAP    PIC X.                                     00003050
             03 MDEFFETAH    PIC X.                                     00003060
             03 MDEFFETAV    PIC X.                                     00003070
             03 MDEFFETAO    PIC X(10).                                 00003080
             03 FILLER       PIC X(2).                                  00003090
             03 MLIENA  PIC X.                                          00003100
             03 MLIENC  PIC X.                                          00003110
             03 MLIENP  PIC X.                                          00003120
             03 MLIENH  PIC X.                                          00003130
             03 MLIENV  PIC X.                                          00003140
             03 MLIENO  PIC X.                                          00003150
             03 FILLER       PIC X(2).                                  00003160
             03 MDEFFETA     PIC X.                                     00003170
             03 MDEFFETC     PIC X.                                     00003180
             03 MDEFFETP     PIC X.                                     00003190
             03 MDEFFETH     PIC X.                                     00003200
             03 MDEFFETV     PIC X.                                     00003210
             03 MDEFFETO     PIC X(10).                                 00003220
           02 FILLER    PIC X(2).                                       00003230
           02 MZONCMDA  PIC X.                                          00003240
           02 MZONCMDC  PIC X.                                          00003250
           02 MZONCMDP  PIC X.                                          00003260
           02 MZONCMDH  PIC X.                                          00003270
           02 MZONCMDV  PIC X.                                          00003280
           02 MZONCMDO  PIC X(15).                                      00003290
           02 FILLER    PIC X(2).                                       00003300
           02 MLIBERRA  PIC X.                                          00003310
           02 MLIBERRC  PIC X.                                          00003320
           02 MLIBERRP  PIC X.                                          00003330
           02 MLIBERRH  PIC X.                                          00003340
           02 MLIBERRV  PIC X.                                          00003350
           02 MLIBERRO  PIC X(58).                                      00003360
           02 FILLER    PIC X(2).                                       00003370
           02 MCODTRAA  PIC X.                                          00003380
           02 MCODTRAC  PIC X.                                          00003390
           02 MCODTRAP  PIC X.                                          00003400
           02 MCODTRAH  PIC X.                                          00003410
           02 MCODTRAV  PIC X.                                          00003420
           02 MCODTRAO  PIC X(4).                                       00003430
           02 FILLER    PIC X(2).                                       00003440
           02 MCICSA    PIC X.                                          00003450
           02 MCICSC    PIC X.                                          00003460
           02 MCICSP    PIC X.                                          00003470
           02 MCICSH    PIC X.                                          00003480
           02 MCICSV    PIC X.                                          00003490
           02 MCICSO    PIC X(5).                                       00003500
           02 FILLER    PIC X(2).                                       00003510
           02 MNETNAMA  PIC X.                                          00003520
           02 MNETNAMC  PIC X.                                          00003530
           02 MNETNAMP  PIC X.                                          00003540
           02 MNETNAMH  PIC X.                                          00003550
           02 MNETNAMV  PIC X.                                          00003560
           02 MNETNAMO  PIC X(8).                                       00003570
           02 FILLER    PIC X(2).                                       00003580
           02 MSCREENA  PIC X.                                          00003590
           02 MSCREENC  PIC X.                                          00003600
           02 MSCREENP  PIC X.                                          00003610
           02 MSCREENH  PIC X.                                          00003620
           02 MSCREENV  PIC X.                                          00003630
           02 MSCREENO  PIC X(4).                                       00003640
                                                                                
