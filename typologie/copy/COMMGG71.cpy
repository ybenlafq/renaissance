      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00010000
              03 COMMGG71  REDEFINES COMM-GG50-FILLER.                  00970008
      *                                                                 00010000
                 05  COMM-GG71-FILLER    PIC X(3451).                   00970009
                 05  COMM-GG71-APPLI REDEFINES COMM-GG71-FILLER.        00970009
                     10  COMM-GG71-MAP.                                         
                             20  COMM-GG71-PCHT    PIC S9(7)V99 COMP-3.         
                             20  COMM-GG71-SRPHT   PIC S9(7)V99 COMP-3.         
                             20  COMM-GG71-CEFFIC  PIC X.                       
                             20  COMM-GG71-OAZP    PIC X.                       
                             20  COMM-GG71-DDOAZP  PIC X(8).                    
                             20  COMM-GG71-DFOAZP  PIC X(8).                    
                             20  COMM-GG71-WACTSUP PIC X.                       
      *                                                                 00010000
      * ZONE DE REFERENCE                                               00010000
      *                   ACTUEL                                        00010000
                             20  COMM-GG71-PRIRAC  PIC S9(7)V99 COMP-3.         
                             20  COMM-GG71-WIMPAC  PIC X.                       
                             20  COMM-GG71-WMAXAC  PIC X.                       
                             20  COMM-GG71-DDREFA  PIC X(8).                    
                             20  COMM-GG71-DFREFA  PIC X(8).                    
      *                   FUTUR                                                 
                             20  COMM-GG71-PRIRNO  PIC S9(7)V99 COMP-3.         
                             20  COMM-GG71-WIMPN   PIC X.                       
                             20  COMM-GG71-WMAXN   PIC X.                       
                             20  COMM-GG71-DDREFN  PIC X(8).                    
                             20  COMM-GG71-DFREFN  PIC X(8).                    
      *                                                                 00010000
      * ZONE DE REFERENCE 2                                             00010000
      *                   ACTUEL                                        00010000
                             20  COMM-GG71-PRIR2AC PIC S9(7)V99 COMP-3.         
                             20  COMM-GG71-WBLOCAC PIC X.                       
                             20  COMM-GG71-DDREF2A PIC X(8).                    
                             20  COMM-GG71-DFREF2A PIC X(8).                    
      *                   FUTUR                                                 
                             20  COMM-GG71-PRIR2NO PIC S9(7)V99 COMP-3.         
                             20  COMM-GG71-WBLOCN  PIC X.                       
                             20  COMM-GG71-DDREF2N PIC X(8).                    
                             20  COMM-GG71-DFREF2N PIC X(8).                    
      *                                                                 00010000
      * ZONE DU MAGASIN                                                 00010000
      *                   ANCIEN                                                
                             20  COMM-GG71-NZONPMO PIC XX.                      
                             20  COMM-GG71-PRIMO   PIC S9(7)V99 COMP-3.         
                             20  COMM-GG71-PCOMMO  PIC S9(4)V9  COMP-3.         
                             20  COMM-GG71-INTFAMO PIC S9(4)V9  COMP-3.         
                             20  COMM-GG71-DDPRIMO PIC X(8).                    
                             20  COMM-GG71-PMBUMO  PIC S9(5)V99 COMP-3.         
                             20  COMM-GG71-QTMARMO PIC S9(4)V9  COMP-3.         
                             20  COMM-GG71-FMILMO  PIC S9(5)V99 COMP-3.         
                             20  COMM-GG71-LCOMMMO PIC X(15).                   
      *                   ACTUEL                                        00010000
                             20  COMM-GG71-NZONPMA PIC XX.                      
                             20  COMM-GG71-PRIMAC  PIC S9(7)V99 COMP-3.         
                             20  COMM-GG71-PCOMMAC PIC S9(4)V9  COMP-3.         
                             20  COMM-GG71-INTFAMA PIC S9(4)V9  COMP-3.         
                             20  COMM-GG71-DPRIMAC PIC X(8).                    
                             20  COMM-GG71-PMBUMAC PIC S9(5)V99 COMP-3.         
                             20  COMM-GG71-QTMARMA PIC S9(4)V9  COMP-3.         
                             20  COMM-GG71-FMILMAC PIC S9(5)V99 COMP-3.         
                             20  COMM-GG71-LCOMMMA PIC X(15).                   
                             20  COMM-GG71-WEXISTA PIC X.                       
      *                   FUTUR                                         00010000
                             20  COMM-GG71-NZONPMN PIC XX.                      
                             20  COMM-GG71-PRIMNO  PIC S9(7)V99 COMP-3.         
                             20  COMM-GG71-PCOMMNO PIC S9(4)V9  COMP-3.         
                             20  COMM-GG71-INTFAMN PIC S9(4)V9  COMP-3.         
                             20  COMM-GG71-DPRIMNO PIC X(8).                    
                             20  COMM-GG71-PMBUMNO PIC S9(5)V99 COMP-3.         
                             20  COMM-GG71-QTMARMN PIC S9(4)V9  COMP-3.         
                             20  COMM-GG71-FMILMNO PIC S9(5)V99 COMP-3.         
                             20  COMM-GG71-LCOMMMN PIC X(15).                   
      *                                                                 00010000
      * PRIX ET PRIMES EXCEPTIONNELS A SAISIR                           00010000
      *                                                                 00010000
                             20  COMM-GG71-NZONPEX  PIC XX.                     
                             20  COMM-GG71-PRIEX    PIC S9(7)V99 COMP-3.        
                             20  COMM-GG71-COMEX    PIC S9(4)V9 COMP-3.         
                             20  COMM-GG71-DPRIEX   PIC X(8).                   
                             20  COMM-GG71-CORIG    PIC X(1).                   
                             20  COMM-GG71-DPRIEXA  PIC X(8).                   
                             20  COMM-GG71-PMBUEX   PIC S9(5)V99 COMP-3.        
                             20  COMM-GG71-QTMAREX  PIC S9(3)V9  COMP-3.        
                             20  COMM-GG71-FMILEX   PIC S9(3)V99 COMP-3.        
                             20  COMM-GG71-DFIEFEX  PIC X(8).                   
                             20  COMM-GG71-DFIEFEXA PIC X(8).                   
                             20  COMM-GG71-NCONC    PIC X(4).                   
                             20  COMM-GG71-LCONC    PIC X(15).                  
                             20  COMM-GG71-NLIEU    PIC X(3).                   
                             20  COMM-GG71-NCODIS  PIC X(7).                    
      * HA                                                              00010000
                     10  COMM-GG71-SIMI.                                        
                             20  COMM-GG71-ART-SIM PIC 9(7) OCCURS 20.          
                             20  COMM-GG71-NBARTSIM PIC S9(3) COMP-3.           
                             20  COMM-GG71-CPTSIM   PIC S9(3) COMP-3.           
                     10  COMM-GG71-RESTE.                                       
                             20  COMM-GG71-QTAUXTVA PIC S9V9(4) COMP-3.         
                             20  COMM-GG71-FMILMAX  PIC 999.                    
                             20  COMM-GG71-PGM      PIC X(5).                   
                             20  COMM-GG71-CCODGROUP PIC X.                     
                             20  COMM-GG71-TABLE-GROUP.                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                          25  COMM-GG71-NBR-GROUP PIC S9(4) COMP.        
      *--                                                                       
                                 25  COMM-GG71-NBR-GROUP PIC S9(4)              
                                                                 COMP-5.        
      *}                                                                        
                                 25  COMM-GG71-TAB-GROUP OCCURS 100.            
                                     30  COMM-GG71-TAB-NCODIC PIC 9(7).         
                                     30  COMM-GG71-TAB-QLIENS                   
                                              PIC S9(3) COMP-3.                 
                                     30  COMM-GG71-GRP-OU-ELT                   
                                              PIC X.                            
                             20  COMM-GG71-PASSAGE-1  PIC X.                    
                             20  COMM-GG71-PASSAGE-2  PIC X.                    
                             20  COMM-GG71-DATEFFET   PIC X(8).                 
                             20  COMM-GG71-PVTTC   PIC 9(7)V99 COMP-3.          
                             20  COMM-GG71-PRIX-MOD   PIC X.                    
                             20  COMM-GG71-DATE-MOD   PIC X.                    
                             20  COMM-GG71-COMM-MOD   PIC X.                    
                             20  COMM-GG71-LCOM-MOD   PIC X.                    
                             20  COMM-GG71-OFACT-MOD  PIC X.                    
                             20  COMM-GG71-MAJ-PRIX   PIC X.                    
                             20  COMM-GG71-MAJ-PRIXEX PIC X.                    
                             20  COMM-GG71-MAJ-COMEX  PIC X.                    
                             20  COMM-GG71-PRO-PRIEX  PIC X.                    
                             20  COMM-GG71-TRAITEMENT PIC X.                    
                             20  COMM-GG71-SIEGE      PIC X.                    
                             20  COMM-GG71-ANNULATION PIC X.                    
                          20  COMM-GG71-TS-PMBUEX  PIC S9(5)V99 COMP-3.         
                          20  COMM-GG71-TS-QTMAREX PIC S9(3)V99 COMP-3.         
                          20  COMM-GG71-TS-FMILEX  PIC S9(3)V99 COMP-3.         
                                                                                
