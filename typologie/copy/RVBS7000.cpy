      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVBS7000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVBS7000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBS7000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBS7000.                                                            
      *}                                                                        
           02  BS70-NSOC                                                        
               PIC X(0003).                                                     
           02  BS70-NLIEU                                                       
               PIC X(0003).                                                     
           02  BS70-DATEOP                                                      
               PIC X(0008).                                                     
           02  BS70-HEUREOP                                                     
               PIC X(0008).                                                     
           02  BS70-OPERATION                                                   
               PIC X(0003).                                                     
           02  BS70-ETAT                                                        
               PIC X(0001).                                                     
           02  BS70-CACID                                                       
               PIC X(0008).                                                     
           02  BS70-ECRAN                                                       
               PIC X(0010).                                                     
           02  BS70-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVBS7000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBS7000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBS7000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BS70-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BS70-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BS70-NETAB-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BS70-NETAB-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BS70-NFOUR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BS70-NFOUR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BS70-NAUX-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BS70-NAUX-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BS70-NFACTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BS70-NFACTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BS70-DFACTURE-FF                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BS70-DFACTURE-FF                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BS70-NJRN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BS70-NJRN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BS70-NEXER-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BS70-NEXER-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BS70-NPIECE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BS70-NPIECE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BS70-DFACTW4-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BS70-DFACTW4-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BS70-DCREAT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BS70-DCREAT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BS70-DMAJBAP-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BS70-DMAJBAP-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BS70-DFACTMAJ-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BS70-DFACTMAJ-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BS70-TYPECR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BS70-TYPECR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BS70-ORIGINE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BS70-ORIGINE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BS70-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BS70-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
