      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE RAMOT TABLE DES MOTIFS AVOIRS          *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01J5.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01J5.                                                            
      *}                                                                        
           05  RAMOT-CTABLEG2    PIC X(15).                                     
           05  RAMOT-CTABLEG2-REDEF REDEFINES RAMOT-CTABLEG2.                   
               10  RAMOT-NSOCIETE        PIC X(03).                             
               10  RAMOT-CMOTIFAV        PIC X(02).                             
           05  RAMOT-WTABLEG     PIC X(80).                                     
           05  RAMOT-WTABLEG-REDEF  REDEFINES RAMOT-WTABLEG.                    
               10  RAMOT-LMOTIFAV        PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01J5-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01J5-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RAMOT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  RAMOT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RAMOT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  RAMOT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
