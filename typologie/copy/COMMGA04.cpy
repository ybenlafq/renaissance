      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGA04                    TR: GA04  *    00020000
      *                  DEFINITION GROUPE FOURNISSEUR             *    00030005
      **************************************************************    00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00050000
      *                                                                 00060000
          02 COMM-GA04-APPLI REDEFINES COMM-GA00-APPLI.                 00070000
      *------------------------------ CODE FONCTION                     00080000
             03 COMM-GA04-FONCT          PIC X(3).                      00090000
      *------------------------------ NUMERO DE PAGE                    00100010
             03 NUMERO-DE-PAGE         PIC 9(2).                        00100110
      *------------------------------ COMPTEUR.                         00101009
             03 COMM-GA04-COMPT          PIC 9(2).                      00102009
      *------------------------------ CODE GROUPE FOURNISSEUR           00103009
             03 COMM-GA04-CGRPFOUR       PIC X(5).                      00110006
      *------------------------------ LIBELLE GROUPE FOURNISSEUR        00120001
             03 COMM-GA04-LGRPFOUR       PIC X(20).                     00130001
      *------------------------------ DERNIERE CLE LUE                  00140010
             03 SAVE-CLE-FIN             PIC X(5).                      00140310
      *------------------------------ ENTITE DE COMMANDE                00140410
             03 COMM-GA04-ENTITECDE .                                   00140503
      *------------------------------ LES 13 CODES ENTITE DE COMMANDE   00140607
                  04 COMM-GA04-ENTITE   OCCURS   13 .                   00141007
      *------------------------------ CODE ENTITE DE COMMANDE           00150001
                     05 COMM-GA04-NENTCDE PIC X(5).                     00151004
      *------------------------------ LIBELLE ENTITE DE COMMANDE        00170001
                     05 COMM-GA04-LENTCDE PIC X(20).                    00180003
      *------------------------------ TABLE POUR LA PAGINATION          00220004
             03 TABLE-PAGINATION     .                                  00230004
      *------------------------------ CLE DE LA PAGINATION              00240001
             04 SAVE-PAGINATION     OCCURS  30.                         00240102
      *------------------------------ PAGINATION                        00241002
                  05 SAVE-CLE-PAGINATION    PIC X(5).                   00250011
      *------------------------------ FILLER                            00262001
             03 COMM-GA04-FILLER         PIC X(3212).                   00270009
      ***************************************************************** 00280000
                                                                                
