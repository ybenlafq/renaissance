      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RVTH0800                           *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTH0800.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTH0800.                                                            
      *}                                                                        
      *    *************************************************************        
      *                       COPCO                                             
           10 TH08-COPCO           PIC X(3).                                    
      *    *************************************************************        
      *                       CPARAM                                            
           10 TH08-CPARAM          PIC X(10).                                   
      *    *************************************************************        
      *                       LPARAM                                            
           10 TH08-LPARAM          PIC X(20).                                   
      *    *************************************************************        
      *                       WFLAGPARAM                                        
           10 TH08-WFLAGPARAM      PIC X(1).                                    
      *    *************************************************************        
      *                       LVPARAM                                           
           10 TH08-LVPARAM         PIC X(50).                                   
      *    *************************************************************        
      *                       DMAJSYST                                          
           10 TH08-DMAJSYST        PIC X(26).                                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      ******************************************************************        
      *   LISTE DES FLAGS DE LA TABLE RVTH0800                                  
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTH0800-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTH0800-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH08-COPCO-F             PIC S9(4) COMP.                         
      *--                                                                       
           10  TH08-COPCO-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH08-CPARAM-F            PIC S9(4) COMP.                         
      *--                                                                       
           10  TH08-CPARAM-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH08-LPARAM-F            PIC S9(4) COMP.                         
      *--                                                                       
           10  TH08-LPARAM-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH08-WFLAGPARAM-F        PIC S9(4) COMP.                         
      *--                                                                       
           10  TH08-WFLAGPARAM-F        PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH08-LVPARAM-F           PIC S9(4) COMP.                         
      *--                                                                       
           10  TH08-LVPARAM-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH08-DMAJSYST-F          PIC S9(4) COMP.                         
      *--                                                                       
           10  TH08-DMAJSYST-F          PIC S9(4) COMP-5.                       
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 6       *        
      ******************************************************************        
                                                                                
