      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVBS2500                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVBS2500                 00000060
      *   CLE UNIQUE : CTYPENT A NSEQDOS                                00000070
      *---------------------------------------------------------        00000080
      *                                                                 00000090
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBS2500.                                                    00000100
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBS2500.                                                            
      *}                                                                        
           10 BS25-CTYPENT         PIC X(2).                            00000110
           10 BS25-NENTITE         PIC X(7).                            00000120
           10 BS25-NSEQDOS         PIC S9(3)V USAGE COMP-3.             00000130
           10 BS25-CDOSSIER        PIC X(5).                            00000140
           10 BS25-DEFFET          PIC X(8).                            00000150
           10 BS25-DFINEFFET       PIC X(8).                            00000160
           10 BS25-DSYST           PIC S9(13)V USAGE COMP-3.            00000170
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00000180
      *---------------------------------------------------------        00000190
      *   LISTE DES FLAGS DE LA TABLE RVBS2500                          00000200
      *---------------------------------------------------------        00000210
      *                                                                 00000220
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBS2500-FLAGS.                                              00000230
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBS2500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS25-CTYPENT-F       PIC S9(4) COMP.                      00000240
      *--                                                                       
           10 BS25-CTYPENT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS25-NENTITE-F       PIC S9(4) COMP.                      00000250
      *--                                                                       
           10 BS25-NENTITE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS25-NSEQDOS-F       PIC S9(4) COMP.                      00000260
      *--                                                                       
           10 BS25-NSEQDOS-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS25-CDOSSIER-F      PIC S9(4) COMP.                      00000270
      *--                                                                       
           10 BS25-CDOSSIER-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS25-DEFFET-F        PIC S9(4) COMP.                      00000280
      *--                                                                       
           10 BS25-DEFFET-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS25-DFINEFFET-F     PIC S9(4) COMP.                      00000290
      *--                                                                       
           10 BS25-DFINEFFET-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS25-DSYST-F         PIC S9(4) COMP.                      00000300
      *                                                                         
      *--                                                                       
           10 BS25-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
