      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE USINE USINE DE FABRICATION DES PROD.   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01WR.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01WR.                                                            
      *}                                                                        
           05  USINE-CTABLEG2    PIC X(15).                                     
           05  USINE-CTABLEG2-REDEF REDEFINES USINE-CTABLEG2.                   
               10  USINE-CUSINE          PIC X(05).                             
           05  USINE-WTABLEG     PIC X(80).                                     
           05  USINE-WTABLEG-REDEF  REDEFINES USINE-WTABLEG.                    
               10  USINE-LUSINE          PIC X(20).                             
               10  USINE-WACTIF          PIC X(01).                             
               10  USINE-NENTCDE         PIC X(05).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01WR-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01WR-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  USINE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  USINE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  USINE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  USINE-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
