      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESE00   ESE00                                              00000020
      ***************************************************************** 00000030
       01   ETH11I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCODICI  PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLREFFOURNI    PIC X(20).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLFAMI    PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MHISTOL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MHISTOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MHISTOF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MHISTOI   PIC X.                                          00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCMARQI   PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLMARQI   PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEL    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MDATEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDATEF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDATEI    PIC X(10).                                      00000530
           02 MDEFFETD OCCURS   10 TIMES .                              00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MDEFFETI     PIC X(10).                                 00000580
           02 MPTCD OCCURS   10 TIMES .                                 00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPTCL   COMP PIC S9(4).                                 00000600
      *--                                                                       
             03 MPTCL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MPTCF   PIC X.                                          00000610
             03 FILLER  PIC X(4).                                       00000620
             03 MPTCI   PIC X(14).                                      00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MZONCMDI  PIC X(15).                                      00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MLIBERRI  PIC X(58).                                      00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MCODTRAI  PIC X(4).                                       00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MCICSI    PIC X(5).                                       00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MNETNAMI  PIC X(8).                                       00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MSCREENI  PIC X(4).                                       00000870
      ***************************************************************** 00000880
      * SDF: ESE00   ESE00                                              00000890
      ***************************************************************** 00000900
       01   ETH11O REDEFINES ETH11I.                                    00000910
           02 FILLER    PIC X(12).                                      00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MDATJOUA  PIC X.                                          00000940
           02 MDATJOUC  PIC X.                                          00000950
           02 MDATJOUP  PIC X.                                          00000960
           02 MDATJOUH  PIC X.                                          00000970
           02 MDATJOUV  PIC X.                                          00000980
           02 MDATJOUO  PIC X(10).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MTIMJOUA  PIC X.                                          00001010
           02 MTIMJOUC  PIC X.                                          00001020
           02 MTIMJOUP  PIC X.                                          00001030
           02 MTIMJOUH  PIC X.                                          00001040
           02 MTIMJOUV  PIC X.                                          00001050
           02 MTIMJOUO  PIC X(5).                                       00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MPAGEA    PIC X.                                          00001080
           02 MPAGEC    PIC X.                                          00001090
           02 MPAGEP    PIC X.                                          00001100
           02 MPAGEH    PIC X.                                          00001110
           02 MPAGEV    PIC X.                                          00001120
           02 MPAGEO    PIC X(3).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MPAGEMAXA      PIC X.                                     00001150
           02 MPAGEMAXC PIC X.                                          00001160
           02 MPAGEMAXP PIC X.                                          00001170
           02 MPAGEMAXH PIC X.                                          00001180
           02 MPAGEMAXV PIC X.                                          00001190
           02 MPAGEMAXO      PIC X(3).                                  00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MNCODICA  PIC X.                                          00001220
           02 MNCODICC  PIC X.                                          00001230
           02 MNCODICP  PIC X.                                          00001240
           02 MNCODICH  PIC X.                                          00001250
           02 MNCODICV  PIC X.                                          00001260
           02 MNCODICO  PIC X(7).                                       00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MLREFFOURNA    PIC X.                                     00001290
           02 MLREFFOURNC    PIC X.                                     00001300
           02 MLREFFOURNP    PIC X.                                     00001310
           02 MLREFFOURNH    PIC X.                                     00001320
           02 MLREFFOURNV    PIC X.                                     00001330
           02 MLREFFOURNO    PIC X(20).                                 00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MCFAMA    PIC X.                                          00001360
           02 MCFAMC    PIC X.                                          00001370
           02 MCFAMP    PIC X.                                          00001380
           02 MCFAMH    PIC X.                                          00001390
           02 MCFAMV    PIC X.                                          00001400
           02 MCFAMO    PIC X(5).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MLFAMA    PIC X.                                          00001430
           02 MLFAMC    PIC X.                                          00001440
           02 MLFAMP    PIC X.                                          00001450
           02 MLFAMH    PIC X.                                          00001460
           02 MLFAMV    PIC X.                                          00001470
           02 MLFAMO    PIC X(20).                                      00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MHISTOA   PIC X.                                          00001500
           02 MHISTOC   PIC X.                                          00001510
           02 MHISTOP   PIC X.                                          00001520
           02 MHISTOH   PIC X.                                          00001530
           02 MHISTOV   PIC X.                                          00001540
           02 MHISTOO   PIC X.                                          00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MCMARQA   PIC X.                                          00001570
           02 MCMARQC   PIC X.                                          00001580
           02 MCMARQP   PIC X.                                          00001590
           02 MCMARQH   PIC X.                                          00001600
           02 MCMARQV   PIC X.                                          00001610
           02 MCMARQO   PIC X(5).                                       00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MLMARQA   PIC X.                                          00001640
           02 MLMARQC   PIC X.                                          00001650
           02 MLMARQP   PIC X.                                          00001660
           02 MLMARQH   PIC X.                                          00001670
           02 MLMARQV   PIC X.                                          00001680
           02 MLMARQO   PIC X(20).                                      00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MDATEA    PIC X.                                          00001710
           02 MDATEC    PIC X.                                          00001720
           02 MDATEP    PIC X.                                          00001730
           02 MDATEH    PIC X.                                          00001740
           02 MDATEV    PIC X.                                          00001750
           02 MDATEO    PIC X(10).                                      00001760
           02 DFHMS1 OCCURS   10 TIMES .                                00001770
             03 FILLER       PIC X(2).                                  00001780
             03 MDEFFETA     PIC X.                                     00001790
             03 MDEFFETC     PIC X.                                     00001800
             03 MDEFFETP     PIC X.                                     00001810
             03 MDEFFETH     PIC X.                                     00001820
             03 MDEFFETV     PIC X.                                     00001830
             03 MDEFFETO     PIC X(10).                                 00001840
           02 DFHMS2 OCCURS   10 TIMES .                                00001850
             03 FILLER       PIC X(2).                                  00001860
             03 MPTCA   PIC X.                                          00001870
             03 MPTCC   PIC X.                                          00001880
             03 MPTCP   PIC X.                                          00001890
             03 MPTCH   PIC X.                                          00001900
             03 MPTCV   PIC X.                                          00001910
             03 MPTCO   PIC X(14).                                      00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MZONCMDA  PIC X.                                          00001940
           02 MZONCMDC  PIC X.                                          00001950
           02 MZONCMDP  PIC X.                                          00001960
           02 MZONCMDH  PIC X.                                          00001970
           02 MZONCMDV  PIC X.                                          00001980
           02 MZONCMDO  PIC X(15).                                      00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MLIBERRA  PIC X.                                          00002010
           02 MLIBERRC  PIC X.                                          00002020
           02 MLIBERRP  PIC X.                                          00002030
           02 MLIBERRH  PIC X.                                          00002040
           02 MLIBERRV  PIC X.                                          00002050
           02 MLIBERRO  PIC X(58).                                      00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MCODTRAA  PIC X.                                          00002080
           02 MCODTRAC  PIC X.                                          00002090
           02 MCODTRAP  PIC X.                                          00002100
           02 MCODTRAH  PIC X.                                          00002110
           02 MCODTRAV  PIC X.                                          00002120
           02 MCODTRAO  PIC X(4).                                       00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MCICSA    PIC X.                                          00002150
           02 MCICSC    PIC X.                                          00002160
           02 MCICSP    PIC X.                                          00002170
           02 MCICSH    PIC X.                                          00002180
           02 MCICSV    PIC X.                                          00002190
           02 MCICSO    PIC X(5).                                       00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MNETNAMA  PIC X.                                          00002220
           02 MNETNAMC  PIC X.                                          00002230
           02 MNETNAMP  PIC X.                                          00002240
           02 MNETNAMH  PIC X.                                          00002250
           02 MNETNAMV  PIC X.                                          00002260
           02 MNETNAMO  PIC X(8).                                       00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MSCREENA  PIC X.                                          00002290
           02 MSCREENC  PIC X.                                          00002300
           02 MSCREENP  PIC X.                                          00002310
           02 MSCREENH  PIC X.                                          00002320
           02 MSCREENV  PIC X.                                          00002330
           02 MSCREENO  PIC X(4).                                       00002340
                                                                                
