      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                        00000010
                                                                                
      *--  ZONES RESERVEES APPLICATIVES OPTION 1 ----------------- 3855 00000020
                                                                        00000030
           02 COMM-BS11-BS12-BS13-APPLI REDEFINES COMM-BS10-FILLER.     00000040
              03 COMM-11.                                               00000050
                                                                        00000060
                 05 COMM-11-INDTS           PIC S9(5) COMP-3.           00000070
                 05 COMM-11-INDMAX          PIC S9(5) COMP-3.           00000080
                 05 COMM-11-VAL-ENTITE.                                 00000090
                    10 COMM-11-ENTITE          PIC X(7).                00000100
                    10 COMM-11-FAMENT          PIC X(5).                00000110
                    10 COMM-11-MARQENT         PIC X(5).                00000120
                    10 COMM-11-LIBENT          PIC X(20).               00000130
                 05 COMM-11-CCRIT1             PIC X(5).                00000140
                 05 COMM-11-CCRIT2             PIC X(5).                00000150
      *          TYPE DE LISTE GENERIQUE    O/N                         00000160
                 05 COMM-11-CTYPCRIT        PIC X(01).                  00000170
                                                                        00000180
                 05 COMM-11-CPROG-MENU      PIC X(05).                  00000190
                                                                        00000200
              03 COMM-12.                                               00000210
                                                                        00000220
                 05 COMM-12-NLIST           PIC X(7).                   00000230
                 05 COMM-12-LLIST           PIC X(20).                  00000240
                 05 COMM-12-DCREATION       PIC X(8).                   00000250
                 05 COMM-12-CTYPENT         PIC X(2).                   00000260
                 05 COMM-12-LTYPENT         PIC X(20).                  00000270
      *          TYPE DE LISTE GENERIQUE    O/N                         00000280
                 05 COMM-12-CTYPCRIT        PIC X(01).                  00000290
MB1   *          CF MAINTENANCE DU TBS12    O/N                         00000300
                 05 COMM-12-TRI             PIC X(01).                  00000310
                 05 COMM-12-CCRIT1          PIC X(05).                  00000320
                 05 COMM-12-CCRIT2          PIC X(05).                  00000330
                 05 COMM-12-NLISTORIG       PIC X(5).                   00000340
                 05 COMM-12-CTABLE          PIC X(6).                   00000350
                                                                        00000360
                 05 COMM-12-INDTS           PIC S9(5) COMP-3.           00000370
                 05 COMM-12-INDMAX          PIC S9(5) COMP-3.           00000380
                                                                        00000390
                 05 COMM-12-MAJ-EN-COURS    PIC X(01).                  00000400
                    88 MAJ-EN-COURS VALUE '1'.                          00000410
                                                                        00000420
              03 COMM-16.                                               00000430
                                                                        00000440
                 05 COMM-16-A-INDTS         PIC S9(5) COMP-3.           00000450
                 05 COMM-16-A-INDMAX        PIC S9(5) COMP-3.           00000460
                 05 COMM-16-B-INDTS         PIC S9(5) COMP-3.           00000470
                 05 COMM-16-B-INDMAX        PIC S9(5) COMP-3.           00000480
                                                                        00000490
                 05 COMM-16-DETAIL          PIC X(01).                  00000500
                                                                        00000510
                 05 COMM-16-MAJ-EN-COURS    PIC X(01).                  00000520
                    88 MAJ-EN-COURS-16 VALUE '1'.                       00000530
                 05 COMM-16-WFONC           PIC X(03).                  00000540
MB1           03 COMM-BS11-BS12-BS16-FILLER PIC X(3639).                00000550
                                                                                
