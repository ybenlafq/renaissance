      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGG13   EGG13                                              00000020
      ***************************************************************** 00000030
       01   EGG13I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHEFPL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCHEFPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCHEFPF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCHEFPI   PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCHEFPL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLCHEFPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCHEFPF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLCHEFPI  PIC X(20).                                      00000290
           02 MLISTEI OCCURS   14 TIMES .                               00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMAGL  COMP PIC S9(4).                                 00000310
      *--                                                                       
             03 MNMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMAGF  PIC X.                                          00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MNMAGI  PIC X(3).                                       00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MNCODICI     PIC X(7).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MCFAMI  PIC X(5).                                       00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCMARQI      PIC X(5).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MLREFI  PIC X(20).                                      00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRISTDL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MPRISTDL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPRISTDF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MPRISTDI     PIC X(5).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIEXPL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MPRIEXPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPRIEXPF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MPRIEXPI     PIC X(8).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCONCL      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MNCONCL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNCONCF      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNCONCI      PIC X(4).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MDEFFETI     PIC X(6).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDFINEFFL    COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MDFINEFFL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDFINEFFF    PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MDFINEFFI    PIC X(6).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MZONCMDI  PIC X(15).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MLIBERRI  PIC X(58).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCICSI    PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MNETNAMI  PIC X(8).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MSCREENI  PIC X(4).                                       00000940
      ***************************************************************** 00000950
      * SDF: EGG13   EGG13                                              00000960
      ***************************************************************** 00000970
       01   EGG13O REDEFINES EGG13I.                                    00000980
           02 FILLER    PIC X(12).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MDATJOUA  PIC X.                                          00001010
           02 MDATJOUC  PIC X.                                          00001020
           02 MDATJOUP  PIC X.                                          00001030
           02 MDATJOUH  PIC X.                                          00001040
           02 MDATJOUV  PIC X.                                          00001050
           02 MDATJOUO  PIC X(10).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MTIMJOUA  PIC X.                                          00001080
           02 MTIMJOUC  PIC X.                                          00001090
           02 MTIMJOUP  PIC X.                                          00001100
           02 MTIMJOUH  PIC X.                                          00001110
           02 MTIMJOUV  PIC X.                                          00001120
           02 MTIMJOUO  PIC X(5).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MWPAGEA   PIC X.                                          00001150
           02 MWPAGEC   PIC X.                                          00001160
           02 MWPAGEP   PIC X.                                          00001170
           02 MWPAGEH   PIC X.                                          00001180
           02 MWPAGEV   PIC X.                                          00001190
           02 MWPAGEO   PIC X(3).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MWFONCA   PIC X.                                          00001220
           02 MWFONCC   PIC X.                                          00001230
           02 MWFONCP   PIC X.                                          00001240
           02 MWFONCH   PIC X.                                          00001250
           02 MWFONCV   PIC X.                                          00001260
           02 MWFONCO   PIC X(3).                                       00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MCHEFPA   PIC X.                                          00001290
           02 MCHEFPC   PIC X.                                          00001300
           02 MCHEFPP   PIC X.                                          00001310
           02 MCHEFPH   PIC X.                                          00001320
           02 MCHEFPV   PIC X.                                          00001330
           02 MCHEFPO   PIC X(5).                                       00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MLCHEFPA  PIC X.                                          00001360
           02 MLCHEFPC  PIC X.                                          00001370
           02 MLCHEFPP  PIC X.                                          00001380
           02 MLCHEFPH  PIC X.                                          00001390
           02 MLCHEFPV  PIC X.                                          00001400
           02 MLCHEFPO  PIC X(20).                                      00001410
           02 MLISTEO OCCURS   14 TIMES .                               00001420
             03 FILLER       PIC X(2).                                  00001430
             03 MNMAGA  PIC X.                                          00001440
             03 MNMAGC  PIC X.                                          00001450
             03 MNMAGP  PIC X.                                          00001460
             03 MNMAGH  PIC X.                                          00001470
             03 MNMAGV  PIC X.                                          00001480
             03 MNMAGO  PIC X(3).                                       00001490
             03 FILLER       PIC X(2).                                  00001500
             03 MNCODICA     PIC X.                                     00001510
             03 MNCODICC     PIC X.                                     00001520
             03 MNCODICP     PIC X.                                     00001530
             03 MNCODICH     PIC X.                                     00001540
             03 MNCODICV     PIC X.                                     00001550
             03 MNCODICO     PIC X(7).                                  00001560
             03 FILLER       PIC X(2).                                  00001570
             03 MCFAMA  PIC X.                                          00001580
             03 MCFAMC  PIC X.                                          00001590
             03 MCFAMP  PIC X.                                          00001600
             03 MCFAMH  PIC X.                                          00001610
             03 MCFAMV  PIC X.                                          00001620
             03 MCFAMO  PIC X(5).                                       00001630
             03 FILLER       PIC X(2).                                  00001640
             03 MCMARQA      PIC X.                                     00001650
             03 MCMARQC PIC X.                                          00001660
             03 MCMARQP PIC X.                                          00001670
             03 MCMARQH PIC X.                                          00001680
             03 MCMARQV PIC X.                                          00001690
             03 MCMARQO      PIC X(5).                                  00001700
             03 FILLER       PIC X(2).                                  00001710
             03 MLREFA  PIC X.                                          00001720
             03 MLREFC  PIC X.                                          00001730
             03 MLREFP  PIC X.                                          00001740
             03 MLREFH  PIC X.                                          00001750
             03 MLREFV  PIC X.                                          00001760
             03 MLREFO  PIC X(20).                                      00001770
             03 FILLER       PIC X(2).                                  00001780
             03 MPRISTDA     PIC X.                                     00001790
             03 MPRISTDC     PIC X.                                     00001800
             03 MPRISTDP     PIC X.                                     00001810
             03 MPRISTDH     PIC X.                                     00001820
             03 MPRISTDV     PIC X.                                     00001830
             03 MPRISTDO     PIC X(5).                                  00001840
             03 FILLER       PIC X(2).                                  00001850
             03 MPRIEXPA     PIC X.                                     00001860
             03 MPRIEXPC     PIC X.                                     00001870
             03 MPRIEXPP     PIC X.                                     00001880
             03 MPRIEXPH     PIC X.                                     00001890
             03 MPRIEXPV     PIC X.                                     00001900
             03 MPRIEXPO     PIC X(8).                                  00001910
             03 FILLER       PIC X(2).                                  00001920
             03 MNCONCA      PIC X.                                     00001930
             03 MNCONCC PIC X.                                          00001940
             03 MNCONCP PIC X.                                          00001950
             03 MNCONCH PIC X.                                          00001960
             03 MNCONCV PIC X.                                          00001970
             03 MNCONCO      PIC X(4).                                  00001980
             03 FILLER       PIC X(2).                                  00001990
             03 MDEFFETA     PIC X.                                     00002000
             03 MDEFFETC     PIC X.                                     00002010
             03 MDEFFETP     PIC X.                                     00002020
             03 MDEFFETH     PIC X.                                     00002030
             03 MDEFFETV     PIC X.                                     00002040
             03 MDEFFETO     PIC X(6).                                  00002050
             03 FILLER       PIC X(2).                                  00002060
             03 MDFINEFFA    PIC X.                                     00002070
             03 MDFINEFFC    PIC X.                                     00002080
             03 MDFINEFFP    PIC X.                                     00002090
             03 MDFINEFFH    PIC X.                                     00002100
             03 MDFINEFFV    PIC X.                                     00002110
             03 MDFINEFFO    PIC X(6).                                  00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MZONCMDA  PIC X.                                          00002140
           02 MZONCMDC  PIC X.                                          00002150
           02 MZONCMDP  PIC X.                                          00002160
           02 MZONCMDH  PIC X.                                          00002170
           02 MZONCMDV  PIC X.                                          00002180
           02 MZONCMDO  PIC X(15).                                      00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLIBERRA  PIC X.                                          00002210
           02 MLIBERRC  PIC X.                                          00002220
           02 MLIBERRP  PIC X.                                          00002230
           02 MLIBERRH  PIC X.                                          00002240
           02 MLIBERRV  PIC X.                                          00002250
           02 MLIBERRO  PIC X(58).                                      00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCODTRAA  PIC X.                                          00002280
           02 MCODTRAC  PIC X.                                          00002290
           02 MCODTRAP  PIC X.                                          00002300
           02 MCODTRAH  PIC X.                                          00002310
           02 MCODTRAV  PIC X.                                          00002320
           02 MCODTRAO  PIC X(4).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCICSA    PIC X.                                          00002350
           02 MCICSC    PIC X.                                          00002360
           02 MCICSP    PIC X.                                          00002370
           02 MCICSH    PIC X.                                          00002380
           02 MCICSV    PIC X.                                          00002390
           02 MCICSO    PIC X(5).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNETNAMA  PIC X.                                          00002420
           02 MNETNAMC  PIC X.                                          00002430
           02 MNETNAMP  PIC X.                                          00002440
           02 MNETNAMH  PIC X.                                          00002450
           02 MNETNAMV  PIC X.                                          00002460
           02 MNETNAMO  PIC X(8).                                       00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MSCREENA  PIC X.                                          00002490
           02 MSCREENC  PIC X.                                          00002500
           02 MSCREENP  PIC X.                                          00002510
           02 MSCREENH  PIC X.                                          00002520
           02 MSCREENV  PIC X.                                          00002530
           02 MSCREENO  PIC X(4).                                       00002540
                                                                                
