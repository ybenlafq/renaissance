      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SPLAF PLAFON STOCK ENTREPOT POUR MAG   *        
      *----------------------------------------------------------------*        
       01  RVGA01G3.                                                            
           05  SPLAF-CTABLEG2    PIC X(15).                                     
           05  SPLAF-CTABLEG2-REDEF REDEFINES SPLAF-CTABLEG2.                   
               10  SPLAF-CLE             PIC X(05).                             
           05  SPLAF-WTABLEG     PIC X(80).                                     
           05  SPLAF-WTABLEG-REDEF  REDEFINES SPLAF-WTABLEG.                    
               10  SPLAF-QSTOCK          PIC X(06).                             
               10  SPLAF-QSTOCK-N       REDEFINES SPLAF-QSTOCK                  
                                         PIC S9(06).                            
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01G3-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SPLAF-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SPLAF-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SPLAF-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SPLAF-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
