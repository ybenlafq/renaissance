      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MD401 LISTE COPER POUR ETAT JMD401     *        
      *----------------------------------------------------------------*        
       01  RVGA01YT.                                                            
           05  MD401-CTABLEG2    PIC X(15).                                     
           05  MD401-CTABLEG2-REDEF REDEFINES MD401-CTABLEG2.                   
               10  MD401-COPER           PIC X(10).                             
           05  MD401-WTABLEG     PIC X(80).                                     
           05  MD401-WTABLEG-REDEF  REDEFINES MD401-WTABLEG.                    
               10  MD401-TOTALON         PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01YT-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MD401-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MD401-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MD401-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MD401-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
