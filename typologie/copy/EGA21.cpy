      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA21   EGA21                                              00000020
      ***************************************************************** 00000030
       01   EGA21I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODSSTL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MCODSSTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODSSTF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCODSSTI  PIC X(5).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLEDEBL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCLEDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCLEDEBF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCLEDEBI  PIC X(15).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLEFINL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCLEFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCLEFINF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCLEFINI  PIC X(15).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIMPRIML  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MIMPRIML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MIMPRIMF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MIMPRIMI  PIC X(4).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MZONCMDI  PIC X(15).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLIBERRI  PIC X(58).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCODTRAI  PIC X(4).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCICSI    PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNETNAMI  PIC X(8).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MSCREENI  PIC X(4).                                       00000530
      ***************************************************************** 00000540
      * SDF: EGA21   EGA21                                              00000550
      ***************************************************************** 00000560
       01   EGA21O REDEFINES EGA21I.                                    00000570
           02 FILLER    PIC X(12).                                      00000580
           02 FILLER    PIC X(2).                                       00000590
           02 MDATJOUA  PIC X.                                          00000600
           02 MDATJOUC  PIC X.                                          00000610
           02 MDATJOUP  PIC X.                                          00000620
           02 MDATJOUH  PIC X.                                          00000630
           02 MDATJOUV  PIC X.                                          00000640
           02 MDATJOUO  PIC X(10).                                      00000650
           02 FILLER    PIC X(2).                                       00000660
           02 MTIMJOUA  PIC X.                                          00000670
           02 MTIMJOUC  PIC X.                                          00000680
           02 MTIMJOUP  PIC X.                                          00000690
           02 MTIMJOUH  PIC X.                                          00000700
           02 MTIMJOUV  PIC X.                                          00000710
           02 MTIMJOUO  PIC X(5).                                       00000720
           02 FILLER    PIC X(2).                                       00000730
           02 MCODSSTA  PIC X.                                          00000740
           02 MCODSSTC  PIC X.                                          00000750
           02 MCODSSTP  PIC X.                                          00000760
           02 MCODSSTH  PIC X.                                          00000770
           02 MCODSSTV  PIC X.                                          00000780
           02 MCODSSTO  PIC X(5).                                       00000790
           02 FILLER    PIC X(2).                                       00000800
           02 MCLEDEBA  PIC X.                                          00000810
           02 MCLEDEBC  PIC X.                                          00000820
           02 MCLEDEBP  PIC X.                                          00000830
           02 MCLEDEBH  PIC X.                                          00000840
           02 MCLEDEBV  PIC X.                                          00000850
           02 MCLEDEBO  PIC X(15).                                      00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MCLEFINA  PIC X.                                          00000880
           02 MCLEFINC  PIC X.                                          00000890
           02 MCLEFINP  PIC X.                                          00000900
           02 MCLEFINH  PIC X.                                          00000910
           02 MCLEFINV  PIC X.                                          00000920
           02 MCLEFINO  PIC X(15).                                      00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MIMPRIMA  PIC X.                                          00000950
           02 MIMPRIMC  PIC X.                                          00000960
           02 MIMPRIMP  PIC X.                                          00000970
           02 MIMPRIMH  PIC X.                                          00000980
           02 MIMPRIMV  PIC X.                                          00000990
           02 MIMPRIMO  PIC X(4).                                       00001000
           02 FILLER    PIC X(2).                                       00001010
           02 MZONCMDA  PIC X.                                          00001020
           02 MZONCMDC  PIC X.                                          00001030
           02 MZONCMDP  PIC X.                                          00001040
           02 MZONCMDH  PIC X.                                          00001050
           02 MZONCMDV  PIC X.                                          00001060
           02 MZONCMDO  PIC X(15).                                      00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MLIBERRA  PIC X.                                          00001090
           02 MLIBERRC  PIC X.                                          00001100
           02 MLIBERRP  PIC X.                                          00001110
           02 MLIBERRH  PIC X.                                          00001120
           02 MLIBERRV  PIC X.                                          00001130
           02 MLIBERRO  PIC X(58).                                      00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MCODTRAA  PIC X.                                          00001160
           02 MCODTRAC  PIC X.                                          00001170
           02 MCODTRAP  PIC X.                                          00001180
           02 MCODTRAH  PIC X.                                          00001190
           02 MCODTRAV  PIC X.                                          00001200
           02 MCODTRAO  PIC X(4).                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MCICSA    PIC X.                                          00001230
           02 MCICSC    PIC X.                                          00001240
           02 MCICSP    PIC X.                                          00001250
           02 MCICSH    PIC X.                                          00001260
           02 MCICSV    PIC X.                                          00001270
           02 MCICSO    PIC X(5).                                       00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MNETNAMA  PIC X.                                          00001300
           02 MNETNAMC  PIC X.                                          00001310
           02 MNETNAMP  PIC X.                                          00001320
           02 MNETNAMH  PIC X.                                          00001330
           02 MNETNAMV  PIC X.                                          00001340
           02 MNETNAMO  PIC X(8).                                       00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MSCREENA  PIC X.                                          00001370
           02 MSCREENC  PIC X.                                          00001380
           02 MSCREENP  PIC X.                                          00001390
           02 MSCREENH  PIC X.                                          00001400
           02 MSCREENV  PIC X.                                          00001410
           02 MSCREENO  PIC X(4).                                       00001420
                                                                                
