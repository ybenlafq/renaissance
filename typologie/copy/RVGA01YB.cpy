      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PRFAC CODES FACTURATION GSM CT         *        
      *----------------------------------------------------------------*        
       01  RVGA01YB.                                                            
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  PRFAC-CTABLEG2    PIC X(15).                                     
           05  PRFAC-CTABLEG2-REDEF REDEFINES PRFAC-CTABLEG2.                   
               10  PRFAC-CODE            PIC X(03).                             
           05  PRFAC-WTABLEG     PIC X(80).                                     
       EXEC SQL END DECLARE SECTION END-EXEC.
           05  PRFAC-WTABLEG-REDEF  REDEFINES PRFAC-WTABLEG.                    
               10  PRFAC-LIBELLE         PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01YB-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRFAC-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PRFAC-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRFAC-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PRFAC-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
