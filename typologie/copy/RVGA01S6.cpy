      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GM070 ZONES D EDITION POUR IGM070      *        
      *----------------------------------------------------------------*        
       01  RVGA01S6.                                                            
           05  GM070-CTABLEG2    PIC X(15).                                     
           05  GM070-CTABLEG2-REDEF REDEFINES GM070-CTABLEG2.                   
               10  GM070-NZONE           PIC X(02).                             
               10  GM070-NZONE-N        REDEFINES GM070-NZONE                   
                                         PIC 9(02).                             
           05  GM070-WTABLEG     PIC X(80).                                     
           05  GM070-WTABLEG-REDEF  REDEFINES GM070-WTABLEG.                    
               10  GM070-LZONE           PIC X(20).                             
               10  GM070-NZONPRIX        PIC X(02).                             
               10  GM070-NZONPRIX-N     REDEFINES GM070-NZONPRIX                
                                         PIC 9(02).                             
               10  GM070-NLIEUP          PIC X(03).                             
               10  GM070-NLIEUS          PIC X(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01S6-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GM070-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GM070-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GM070-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GM070-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
