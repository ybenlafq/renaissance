      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT JRX001 AU 09/08/2004  *        
      *                                                                *        
      *          CRITERES DE TRI  07,02,BI,A,                          *        
      *                           09,03,BI,A,                          *        
      *                           12,04,BI,A,                          *        
      *                           16,07,BI,A,                          *        
      *                           23,25,BI,A,                          *        
      *                           48,03,PD,A,                          *        
      *                           51,30,BI,A,                          *        
      *                           81,05,BI,A,                          *        
      *                           86,07,BI,A,                          *        
      *                           93,01,BI,A,                          *        
      *                           94,06,BI,A,                          *        
      *                           00,20,BI,A,                          *        
      *                           20,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-JRX001.                                                        
            05 NOMETAT-JRX001           PIC X(6) VALUE 'JRX001'.                
            05 RUPTURES-JRX001.                                                 
           10 JRX001-NETAT              PIC X(02).                      007  002
           10 JRX001-NMAG               PIC X(03).                      009  003
           10 JRX001-NCONC              PIC X(04).                      012  004
           10 JRX001-NRELEVE            PIC X(07).                      016  007
           10 JRX001-WSAUT              PIC X(25).                      023  025
           10 JRX001-WSEQFAM            PIC S9(05)      COMP-3.         048  003
           10 JRX001-LVMARKETING        PIC X(30).                      051  030
           10 JRX001-CMARQGRP           PIC X(05).                      081  005
           10 JRX001-NCODICGRP          PIC X(07).                      086  007
           10 JRX001-GROUPE             PIC X(01).                      093  001
           10 JRX001-CMARQ              PIC X(06).                      094  006
           10 JRX001-LREFFOURN          PIC X(20).                      100  020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 JRX001-SEQUENCE           PIC S9(04) COMP.                120  002
      *--                                                                       
           10 JRX001-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-JRX001.                                                   
           10 JRX001-CFAM               PIC X(05).                      122  005
           10 JRX001-LEMPCONC           PIC X(15).                      127  015
           10 JRX001-LENSEIGN           PIC X(15).                      142  015
           10 JRX001-LFAM               PIC X(20).                      157  020
           10 JRX001-LMAG               PIC X(20).                      177  020
           10 JRX001-NCODIC             PIC X(07).                      197  007
           10 JRX001-NSOCIETE           PIC X(03).                      204  003
           10 JRX001-PSTDTTC            PIC S9(07)V9(2) COMP-3.         207  005
           10 JRX001-DSUPPORT           PIC X(08).                      212  008
            05 FILLER                      PIC X(293).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-JRX001-LONG           PIC S9(4)   COMP  VALUE +219.           
      *                                                                         
      *--                                                                       
        01  DSECT-JRX001-LONG           PIC S9(4) COMP-5  VALUE +219.           
                                                                                
      *}                                                                        
