      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-GG40.                                                             
           02 TS-LONG                   PIC S9(03) COMP-3  VALUE +69.           
           02 TS-DONNEES.                                                       
              03 TS-GG40-COL1              PIC X(03).                           
              03 TS-GG40-PRIX              PIC X(08).                           
              03 TS-GG40-DPRIX             PIC X(08).                           
              03 TS-GG40-CODEPRIX          PIC X(03).                           
              03 TS-GG40-LCOMMENT1         PIC X(17).                           
              03 TS-GG40-PCOMM             PIC X(05).                           
              03 TS-GG40-LCOMMENT2         PIC X(16).                           
              03 TS-GG40-DPCOMM            PIC X(08).                           
      *       FLAG DEBUT/FIN                                                    
              03 TS-GG40-WLIGNE            PIC X(01).                           
                                                                                
