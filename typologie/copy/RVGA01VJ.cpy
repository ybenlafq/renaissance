      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EPM60 VALEUR ET METHODE ARRONDI/FONC   *        
      *----------------------------------------------------------------*        
       01  RVGA01VJ.                                                            
           05  EPM60-CTABLEG2    PIC X(15).                                     
           05  EPM60-CTABLEG2-REDEF REDEFINES EPM60-CTABLEG2.                   
               10  EPM60-TPCALC          PIC X(05).                             
               10  EPM60-CDEV            PIC X(03).                             
           05  EPM60-WTABLEG     PIC X(80).                                     
           05  EPM60-WTABLEG-REDEF  REDEFINES EPM60-WTABLEG.                    
               10  EPM60-WARRON          PIC X(01).                             
               10  EPM60-QARRON          PIC X(10).                             
               10  EPM60-QARRON-N       REDEFINES EPM60-QARRON                  
                                         PIC 9(10).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01VJ-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EPM60-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EPM60-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EPM60-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EPM60-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
