      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FTSOC SOC/ETAB PAR UTILISATEUR         *        
      *----------------------------------------------------------------*        
       01  RVGA01SV.                                                            
           05  FTSOC-CTABLEG2    PIC X(15).                                     
           05  FTSOC-CTABLEG2-REDEF REDEFINES FTSOC-CTABLEG2.                   
               10  FTSOC-UTILIS          PIC X(07).                             
           05  FTSOC-WTABLEG     PIC X(80).                                     
           05  FTSOC-WTABLEG-REDEF  REDEFINES FTSOC-WTABLEG.                    
               10  FTSOC-SOCIETE         PIC X(05).                             
               10  FTSOC-ETABL           PIC X(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01SV-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FTSOC-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FTSOC-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FTSOC-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FTSOC-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
