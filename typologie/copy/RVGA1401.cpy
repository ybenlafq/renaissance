      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGA1401                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA1401                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA1401.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA1401.                                                            
      *}                                                                        
           02  GA14-CFAM                                                        
               PIC X(0005).                                                     
           02  GA14-LFAM                                                        
               PIC X(0020).                                                     
           02  GA14-CMODSTOCK                                                   
               PIC X(0005).                                                     
           02  GA14-CTAUXTVA                                                    
               PIC X(0005).                                                     
           02  GA14-CGARANTIE                                                   
               PIC X(0005).                                                     
           02  GA14-QBORNESOL                                                   
               PIC S9(2)V9(0003) COMP-3.                                        
           02  GA14-QBORNEVRAC                                                  
               PIC S9(2)V9(0003) COMP-3.                                        
           02  GA14-QNBPVSOL                                                    
               PIC S9(3) COMP-3.                                                
           02  GA14-WMULTIFAM                                                   
               PIC X(0001).                                                     
           02  GA14-WSEQFAM                                                     
               PIC S9(5) COMP-3.                                                
           02  GA14-CTYPENT                                                     
               PIC X(0002).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGA1401                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA1401-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA1401-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA14-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA14-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA14-LFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA14-LFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA14-CMODSTOCK-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA14-CMODSTOCK-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA14-CTAUXTVA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA14-CTAUXTVA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA14-CGARANTIE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA14-CGARANTIE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA14-QBORNESOL-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA14-QBORNESOL-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA14-QBORNEVRAC-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA14-QBORNEVRAC-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA14-QNBPVSOL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA14-QNBPVSOL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA14-WMULTIFAM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA14-WMULTIFAM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA14-WSEQFAM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA14-WSEQFAM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA14-CTYPENT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA14-CTYPENT-F                                                   
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
