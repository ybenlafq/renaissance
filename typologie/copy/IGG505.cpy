      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *            DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGG505      *        
      *                                                                *        
      *          CRITERES DE TRI  07,07,BI,A,                          *        
      *                           14,07,BI,A,                          *        
      *                           21,08,BI,A,                          *        
      *                           29,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGG505.                                                        
            05 NOMETAT-IGG505           PIC X(6) VALUE 'IGG505'.                
            05 RUPTURES-IGG505.                                                 
           10 IGG505-NORIGINE           PIC X(07).                      007  007
           10 IGG505-NCODIC             PIC X(07).                      014  007
           10 IGG505-DOPER              PIC X(08).                      021  008
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGG505-SEQUENCE           PIC S9(04) COMP.                029  002
      *--                                                                       
           10 IGG505-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGG505.                                                   
           10 IGG505-PRAREC             PIC S9(07)V9(2) COMP-3.         031  005
           10 IGG505-PRAVALO            PIC S9(07)V9(2) COMP-3.         036  005
           10 IGG505-QTEREC             PIC S9(11)      COMP-3.         041  006
            05 FILLER                      PIC X(466).                          
                                                                                
