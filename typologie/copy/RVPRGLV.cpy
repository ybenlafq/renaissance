      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PRGLV LIEN FLUX LM6 EXTRACTEUR         *        
      *----------------------------------------------------------------*        
          EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVPRGLV .                                                            
           05  PRGLV-CTABLEG2    PIC X(15).                                     
           05  PRGLV-CTABLEG2-REDEF REDEFINES PRGLV-CTABLEG2.                   
               10  PRGLV-CTYPMES         PIC X(03).                             
               10  PRGLV-NCLETECH        PIC X(10).                             
           05  PRGLV-WTABLEG     PIC X(80).                                     
           05  PRGLV-WTABLEG-REDEF  REDEFINES PRGLV-WTABLEG.                    
               10  PRGLV-CPROG           PIC X(05).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVPRGLV-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRGLV-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PRGLV-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRGLV-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PRGLV-WTABLEG-F   PIC S9(4) COMP-5.                              
          EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
