      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TRX43                                          *  00020001
      ****************************************************************  00050000
      *                                                                 00060000
      *------------------------------ LONGUEUR                          00070000
       01  TS-LONG              PIC S9(2) COMP-3 VALUE 72.              00080001
       01  TS-DONNEES.                                                  00090000
              10 TS-CFAM        PIC X(05).                              00090100
              10 TS-CMARQ       PIC X(05).                              00090200
              10 TS-LREFFOURN   PIC X(20).                              00100001
              10 TS-NCODIC      PIC 9(07).                              00122001
              10 TS-PRELEVE     PIC S9(7)V99 COMP-3.                    00123001
              10 TS-QCONTENU    PIC X(3).                               00123002
              10 TS-QCOLREL     PIC X(03).                              00123003
              10 TS-LCOMMENT    PIC X(20).                              00123004
                                                                                
