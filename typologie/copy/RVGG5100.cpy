      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGG5100                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGG5100                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGG5100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGG5100.                                                            
      *}                                                                        
           02  GG51-NCODIC                                                      
               PIC X(0007).                                                     
           02  GG51-DJOUR                                                       
               PIC X(0008).                                                     
           02  GG51-PRMP                                                        
               PIC S9(7)V9(0006) COMP-3.                                        
           02  GG51-PRMPRECYCL                                                  
               PIC S9(7)V9(0006) COMP-3.                                        
           02  GG51-WPRMPUTIL                                                   
               PIC X(0001).                                                     
           02  GG51-QSTOCKINIT                                                  
               PIC S9(9) COMP-3.                                                
           02  GG51-QSTOCKFINAL                                                 
               PIC S9(9) COMP-3.                                                
           02  GG51-WRECYCLSTCK                                                 
               PIC X(0001).                                                     
           02  GG51-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GG51-PCF                                                         
               PIC S9(7)V9(0006) COMP-3.                                        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGG5100                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGG5100-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGG5100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG51-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG51-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG51-DJOUR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG51-DJOUR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG51-PRMP-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG51-PRMP-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG51-PRMPRECYCL-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG51-PRMPRECYCL-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG51-WPRMPUTIL-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG51-WPRMPUTIL-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG51-QSTOCKINIT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG51-QSTOCKINIT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG51-QSTOCKFINAL-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG51-QSTOCKFINAL-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG51-WRECYCLSTCK-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG51-WRECYCLSTCK-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG51-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG51-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG51-PCF-F                                                       
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GG51-PCF-F                                                       
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
