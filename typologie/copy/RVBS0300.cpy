      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVBS0300                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVBS0300                 00000060
      *   CLE UNIQUE : CLIST   A NENTITE                                00000070
      *---------------------------------------------------------        00000080
      *                                                                 00000090
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBS0300.                                                    00000100
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBS0300.                                                            
      *}                                                                        
           10 BS03-NLIST           PIC X(7).                            00000110
           10 BS03-NENTITE         PIC X(7).                            00000120
           10 BS03-QNBENT          PIC S9(5)V USAGE COMP-3.             00000130
           10 BS03-WSELECTION      PIC X(1).                            00000140
           10 BS03-NSEQENT         PIC S9(3)V USAGE COMP-3.             00000150
           10 BS03-DSYST           PIC S9(13)V USAGE COMP-3.            00000160
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00000170
      *---------------------------------------------------------        00000180
      *   LISTE DES FLAGS DE LA TABLE RVBS0300                          00000190
      *---------------------------------------------------------        00000200
      *                                                                 00000210
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBS0300-FLAGS.                                              00000220
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBS0300-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS03-NLIST-F         PIC S9(4) COMP.                      00000230
      *--                                                                       
           10 BS03-NLIST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS03-NENTITE-F       PIC S9(4) COMP.                      00000240
      *--                                                                       
           10 BS03-NENTITE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS03-QNBENT-F        PIC S9(4) COMP.                      00000250
      *--                                                                       
           10 BS03-QNBENT-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS03-WSELECTION-F    PIC S9(4) COMP.                      00000260
      *--                                                                       
           10 BS03-WSELECTION-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS03-NSEQENT-F       PIC S9(4) COMP.                      00000270
      *--                                                                       
           10 BS03-NSEQENT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS03-DSYST-F         PIC S9(4) COMP.                      00000280
      *                                                                         
      *--                                                                       
           10 BS03-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
