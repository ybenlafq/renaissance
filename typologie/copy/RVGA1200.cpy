      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGA1200                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA1200                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA1200.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA1200.                                                            
      *}                                                                        
           02  GA12-CETAT                                                       
               PIC X(0010).                                                     
           02  GA12-CFAM                                                        
               PIC X(0005).                                                     
           02  GA12-CMARKETING                                                  
               PIC X(0005).                                                     
           02  GA12-WSEQED                                                      
               PIC X(0002).                                                     
           02  GA12-WIMBED                                                      
               PIC X(0001).                                                     
           02  GA12-WSAUT                                                       
               PIC X(0001).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGA1200                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA1200-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA1200-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA12-CETAT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA12-CETAT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA12-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA12-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA12-CMARKETING-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA12-CMARKETING-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA12-WSEQED-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA12-WSEQED-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA12-WIMBED-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA12-WIMBED-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA12-WSAUT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GA12-WSAUT-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
