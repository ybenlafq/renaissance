      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      * COMMAREA DE L'ECOUTEUR DE LA TABLE RTNV00 DE LA LOG INNOVENTE           
      *****************************************************************         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 COM-NV00-LONG-COMMAREA PIC S9(5) COMP VALUE +30000.                   
      *--                                                                       
       01 COM-NV00-LONG-COMMAREA PIC S9(5) COMP-5 VALUE +30000.                 
      *}                                                                        
      * ZONES APPLICATIVES NV00 GENERALES                                       
       01 COMM-NV00.                                                            
          02 COMM-NV00-DONNEES-LOG.                                             
             03 COMM-NV00-DTD                      PIC X(20).                   
             03 COMM-NV00-CLE                      PIC X(255).                  
             03 COMM-NV00-EVENEMENT                PIC X(01).                   
             03 COMM-NV00-TIMESTAMP                PIC X(26).                   
             03 FILLER                             PIC X(691).                  
          02 COMM-NV00-MODULE.                                                  
             03 COMM-NV00-NB-MSG                   PIC S9(3) COMP-3.            
             03 COMM-NV00-CODE-RETOUR              PIC X(05).                   
             03 COMM-NV00-RETOUR.                                               
                05 COMM-NV00-DONNEES-TAB.                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            07 COMM-NV00-TAB-MAX            PIC S9(3) COMP.              
      *--                                                                       
                   07 COMM-NV00-TAB-MAX            PIC S9(3) COMP-5.            
      *}                                                                        
                   07 COMM-NV00-TAB-MSG  OCCURS 50                              
                                         INDEXED BY COMM-NV00-I-TAB.            
                      09 COMM-NV00-TAB-DTD         PIC X(20).                   
                      09 COMM-NV00-TAB-ACTION      PIC X(01).                   
                      09 COMM-NV00-TAB-DONNEES-R   PIC X(550).                  
AD02  *         05 FILLER                          PIC X(253).                  
AD02            05 COMM-NV00-DONNEES-TS.                                        
                   07 COMM-NV00-TS-FLAG            PIC X(01).                   
                   07 COMM-NV00-TS-NB-PAGE         PIC S9(5) COMP-3.            
                   07 COMM-NV00-TS-NB-MSG          PIC S9(5) COMP-3.            
                05 FILLER                          PIC X(246).                  
      *                                                                         
      *****************************************************************         
                                                                                
