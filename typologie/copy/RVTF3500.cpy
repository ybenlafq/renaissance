      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVTF3500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVTF3500                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTF3500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTF3500.                                                            
      *}                                                                        
           02  TF35-LFICHIER                                                    
               PIC X(0008).                                                     
           02  TF35-CRITERE                                                     
               PIC X(0030).                                                     
           02  TF35-DCREATION                                                   
               PIC X(0008).                                                     
           02  TF35-NSEQ                                                        
               PIC S9(7) COMP-3.                                                
           02  TF35-NBLIGNE                                                     
               PIC S9(7) COMP-3.                                                
           02  TF35-CPROG                                                       
               PIC X(0008).                                                     
           02  TF35-WTYPETRT                                                    
               PIC X(0001).                                                     
           02  TF35-LSERVICE                                                    
               PIC X(0001).                                                     
           02  TF35-NMACHINE                                                    
               PIC X(0008).                                                     
           02  TF35-WZIP                                                        
               PIC X(0001).                                                     
           02  TF35-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVTF3500                                  
      *---------------------------------------------------------                
      *                                                                         
                                                                                
