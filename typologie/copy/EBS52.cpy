      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESE52   ESE52                                              00000020
      ***************************************************************** 00000030
       01   EBS52I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCOFFREL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNCOFFREL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCOFFREF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNCOFFREI      PIC X(7).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOFFREL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MLCOFFREL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLCOFFREF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLCOFFREI      PIC X(20).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCFAMI    PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLFAMI    PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPROFASSL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MCPROFASSL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCPROFASSF     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCPROFASSI     PIC X(5).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPROFASSL     COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLPROFASSL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLPROFASSF     PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLPROFASSI     PIC X(20).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCMARQI   PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLMARQI   PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MPAGEI    PIC X(3).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MPAGEMAXI      PIC X(3).                                  00000570
           02 MWTABLEI OCCURS   11 TIMES .                              00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCCDESCL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MCCDESCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCCDESCF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCCDESCI     PIC X(5).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCDESCL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MLCDESCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLCDESCF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MLCDESCI     PIC X(20).                                 00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCVDESCL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MCVDESCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCVDESCF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCVDESCI     PIC X(5).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLVDESCL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MLVDESCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLVDESCF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MLVDESCI     PIC X(20).                                 00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MZONCMDI  PIC X(15).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MLIBERRI  PIC X(58).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCODTRAI  PIC X(4).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCICSI    PIC X(5).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MNETNAMI  PIC X(8).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MSCREENI  PIC X(4).                                       00000980
      ***************************************************************** 00000990
      * SDF: ESE52   ESE52                                              00001000
      ***************************************************************** 00001010
       01   EBS52O REDEFINES EBS52I.                                    00001020
           02 FILLER    PIC X(12).                                      00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MDATJOUA  PIC X.                                          00001050
           02 MDATJOUC  PIC X.                                          00001060
           02 MDATJOUP  PIC X.                                          00001070
           02 MDATJOUH  PIC X.                                          00001080
           02 MDATJOUV  PIC X.                                          00001090
           02 MDATJOUO  PIC X(10).                                      00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MTIMJOUA  PIC X.                                          00001120
           02 MTIMJOUC  PIC X.                                          00001130
           02 MTIMJOUP  PIC X.                                          00001140
           02 MTIMJOUH  PIC X.                                          00001150
           02 MTIMJOUV  PIC X.                                          00001160
           02 MTIMJOUO  PIC X(5).                                       00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MWFONCA   PIC X.                                          00001190
           02 MWFONCC   PIC X.                                          00001200
           02 MWFONCP   PIC X.                                          00001210
           02 MWFONCH   PIC X.                                          00001220
           02 MWFONCV   PIC X.                                          00001230
           02 MWFONCO   PIC X(3).                                       00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MNCOFFREA      PIC X.                                     00001260
           02 MNCOFFREC PIC X.                                          00001270
           02 MNCOFFREP PIC X.                                          00001280
           02 MNCOFFREH PIC X.                                          00001290
           02 MNCOFFREV PIC X.                                          00001300
           02 MNCOFFREO      PIC X(7).                                  00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MLCOFFREA      PIC X.                                     00001330
           02 MLCOFFREC PIC X.                                          00001340
           02 MLCOFFREP PIC X.                                          00001350
           02 MLCOFFREH PIC X.                                          00001360
           02 MLCOFFREV PIC X.                                          00001370
           02 MLCOFFREO      PIC X(20).                                 00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MCFAMA    PIC X.                                          00001400
           02 MCFAMC    PIC X.                                          00001410
           02 MCFAMP    PIC X.                                          00001420
           02 MCFAMH    PIC X.                                          00001430
           02 MCFAMV    PIC X.                                          00001440
           02 MCFAMO    PIC X(5).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MLFAMA    PIC X.                                          00001470
           02 MLFAMC    PIC X.                                          00001480
           02 MLFAMP    PIC X.                                          00001490
           02 MLFAMH    PIC X.                                          00001500
           02 MLFAMV    PIC X.                                          00001510
           02 MLFAMO    PIC X(20).                                      00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MCPROFASSA     PIC X.                                     00001540
           02 MCPROFASSC     PIC X.                                     00001550
           02 MCPROFASSP     PIC X.                                     00001560
           02 MCPROFASSH     PIC X.                                     00001570
           02 MCPROFASSV     PIC X.                                     00001580
           02 MCPROFASSO     PIC X(5).                                  00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MLPROFASSA     PIC X.                                     00001610
           02 MLPROFASSC     PIC X.                                     00001620
           02 MLPROFASSP     PIC X.                                     00001630
           02 MLPROFASSH     PIC X.                                     00001640
           02 MLPROFASSV     PIC X.                                     00001650
           02 MLPROFASSO     PIC X(20).                                 00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MCMARQA   PIC X.                                          00001680
           02 MCMARQC   PIC X.                                          00001690
           02 MCMARQP   PIC X.                                          00001700
           02 MCMARQH   PIC X.                                          00001710
           02 MCMARQV   PIC X.                                          00001720
           02 MCMARQO   PIC X(5).                                       00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MLMARQA   PIC X.                                          00001750
           02 MLMARQC   PIC X.                                          00001760
           02 MLMARQP   PIC X.                                          00001770
           02 MLMARQH   PIC X.                                          00001780
           02 MLMARQV   PIC X.                                          00001790
           02 MLMARQO   PIC X(20).                                      00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MPAGEA    PIC X.                                          00001820
           02 MPAGEC    PIC X.                                          00001830
           02 MPAGEP    PIC X.                                          00001840
           02 MPAGEH    PIC X.                                          00001850
           02 MPAGEV    PIC X.                                          00001860
           02 MPAGEO    PIC X(3).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MPAGEMAXA      PIC X.                                     00001890
           02 MPAGEMAXC PIC X.                                          00001900
           02 MPAGEMAXP PIC X.                                          00001910
           02 MPAGEMAXH PIC X.                                          00001920
           02 MPAGEMAXV PIC X.                                          00001930
           02 MPAGEMAXO      PIC X(3).                                  00001940
           02 MWTABLEO OCCURS   11 TIMES .                              00001950
             03 FILLER       PIC X(2).                                  00001960
             03 MCCDESCA     PIC X.                                     00001970
             03 MCCDESCC     PIC X.                                     00001980
             03 MCCDESCP     PIC X.                                     00001990
             03 MCCDESCH     PIC X.                                     00002000
             03 MCCDESCV     PIC X.                                     00002010
             03 MCCDESCO     PIC X(5).                                  00002020
             03 FILLER       PIC X(2).                                  00002030
             03 MLCDESCA     PIC X.                                     00002040
             03 MLCDESCC     PIC X.                                     00002050
             03 MLCDESCP     PIC X.                                     00002060
             03 MLCDESCH     PIC X.                                     00002070
             03 MLCDESCV     PIC X.                                     00002080
             03 MLCDESCO     PIC X(20).                                 00002090
             03 FILLER       PIC X(2).                                  00002100
             03 MCVDESCA     PIC X.                                     00002110
             03 MCVDESCC     PIC X.                                     00002120
             03 MCVDESCP     PIC X.                                     00002130
             03 MCVDESCH     PIC X.                                     00002140
             03 MCVDESCV     PIC X.                                     00002150
             03 MCVDESCO     PIC X(5).                                  00002160
             03 FILLER       PIC X(2).                                  00002170
             03 MLVDESCA     PIC X.                                     00002180
             03 MLVDESCC     PIC X.                                     00002190
             03 MLVDESCP     PIC X.                                     00002200
             03 MLVDESCH     PIC X.                                     00002210
             03 MLVDESCV     PIC X.                                     00002220
             03 MLVDESCO     PIC X(20).                                 00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MZONCMDA  PIC X.                                          00002250
           02 MZONCMDC  PIC X.                                          00002260
           02 MZONCMDP  PIC X.                                          00002270
           02 MZONCMDH  PIC X.                                          00002280
           02 MZONCMDV  PIC X.                                          00002290
           02 MZONCMDO  PIC X(15).                                      00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLIBERRA  PIC X.                                          00002320
           02 MLIBERRC  PIC X.                                          00002330
           02 MLIBERRP  PIC X.                                          00002340
           02 MLIBERRH  PIC X.                                          00002350
           02 MLIBERRV  PIC X.                                          00002360
           02 MLIBERRO  PIC X(58).                                      00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MCODTRAA  PIC X.                                          00002390
           02 MCODTRAC  PIC X.                                          00002400
           02 MCODTRAP  PIC X.                                          00002410
           02 MCODTRAH  PIC X.                                          00002420
           02 MCODTRAV  PIC X.                                          00002430
           02 MCODTRAO  PIC X(4).                                       00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MCICSA    PIC X.                                          00002460
           02 MCICSC    PIC X.                                          00002470
           02 MCICSP    PIC X.                                          00002480
           02 MCICSH    PIC X.                                          00002490
           02 MCICSV    PIC X.                                          00002500
           02 MCICSO    PIC X(5).                                       00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MNETNAMA  PIC X.                                          00002530
           02 MNETNAMC  PIC X.                                          00002540
           02 MNETNAMP  PIC X.                                          00002550
           02 MNETNAMH  PIC X.                                          00002560
           02 MNETNAMV  PIC X.                                          00002570
           02 MNETNAMO  PIC X(8).                                       00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MSCREENA  PIC X.                                          00002600
           02 MSCREENC  PIC X.                                          00002610
           02 MSCREENP  PIC X.                                          00002620
           02 MSCREENH  PIC X.                                          00002630
           02 MSCREENV  PIC X.                                          00002640
           02 MSCREENO  PIC X(4).                                       00002650
                                                                                
