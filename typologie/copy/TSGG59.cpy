      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************          
      *       PRIX EXCEPTIONNELS                                     *          
      ****************************************************************          
       01  TS-GG59-LONG             PIC S9(4) COMP-3 VALUE +1190.               
       01  TS-GG59-RECORD.                                                      
           05 TS-GG59-LIGNE                OCCURS 14.                           
              10 TS-GG59-ZP         PIC X(2).                                   
              10 TS-GG59-NMAG       PIC X(3).                                   
              10 TS-GG59-NCODIC     PIC X(7).                                   
              10 TS-GG59-CHEFP      PIC X(7).                                   
              10 TS-GG59-CFAM       PIC X(5).                                   
              10 TS-GG59-CMARQ      PIC X(5).                                   
              10 TS-GG59-LREFFOURN  PIC X(20).                                  
              10 TS-GG59-NCONC      PIC X(4).                                   
              10 TS-GG59-MOUCHARD   PIC X(1).                                   
              10 TS-GG59-DEFFET     PIC X(8).                                   
              10 TS-GG59-DFINEFFET  PIC X(8).                                   
              10 TS-GG59-PEXPTTC    PIC S9(6)V99     COMP-3.                    
              10 TS-GG59-PSTDTTC    PIC S9(6)V99     COMP-3.                    
              10 TS-GG59-STATCOMP   PIC X(5).                                   
                                                                                
