      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE QCGAR CORRESPONDANCE GAR36/GAR. GROU   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01QA.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01QA.                                                            
      *}                                                                        
           05  QCGAR-CTABLEG2    PIC X(15).                                     
           05  QCGAR-CTABLEG2-REDEF REDEFINES QCGAR-CTABLEG2.                   
               10  QCGAR-NSOCIETE        PIC X(03).                             
               10  QCGAR-NLIEU           PIC X(03).                             
               10  QCGAR-G36             PIC X(03).                             
           05  QCGAR-WTABLEG     PIC X(80).                                     
           05  QCGAR-WTABLEG-REDEF  REDEFINES QCGAR-WTABLEG.                    
               10  QCGAR-WACTIF          PIC X(01).                             
               10  QCGAR-GAR             PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01QA-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01QA-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QCGAR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  QCGAR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QCGAR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  QCGAR-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
