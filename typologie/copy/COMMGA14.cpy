      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGA14                    TR: GA14  *    00020000
      *                  REFABRICATION DES STATISTIQUES            *    00030001
      **************************************************************    00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00050000
      *                                                                 00060000
          02 COMM-GA14-APPLI REDEFINES COMM-GA00-APPLI.                 00070000
      *------------------------------ NUMERO DE PAGE                    00080000
             03 COMM-GA14-NPAGE          PIC 99.                        00090000
      *------------------------------ DERNIERE CLE LUE                  00100000
             03 COMM-GA14-WCLELU         PIC X(5).                      00110000
      *------------------------------ SAUVEGARDE CLE PAGINATION         00120000
             03 COMM-GA14-WPAGINATION OCCURS 15.                        00130000
                04 COMM-GA14-WCLEPAG     PIC X(5).                      00140000
      *------------------------------ DEMANDE REFABRICATION STATS       00150000
             03 COMM-GA14-TFAMS       OCCURS 11.                        00160002
      *------------------------------ CODE FAMILLE                      00170000
                04 COMM-GA14-NFAMS       PIC X(5).                      00180000
      *------------------------------ CODE LIBELLE                      00190000
                04 COMM-GA14-LFAMS       PIC X(20).                     00200000
      *------------------------------ FILLER                            00210000
             03 COMM-GA14-FILLER         PIC X(2911).                   00220003
      ***************************************************************** 00230000
                                                                                
