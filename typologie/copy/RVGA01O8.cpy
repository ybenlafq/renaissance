      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FTEPU CODES EPURATION                  *        
      *----------------------------------------------------------------*        
       01  RVGA01O8.                                                            
           05  FTEPU-CTABLEG2    PIC X(15).                                     
           05  FTEPU-CTABLEG2-REDEF REDEFINES FTEPU-CTABLEG2.                   
               10  FTEPU-ENTITE          PIC X(05).                             
               10  FTEPU-CODE            PIC X(01).                             
           05  FTEPU-WTABLEG     PIC X(80).                                     
           05  FTEPU-WTABLEG-REDEF  REDEFINES FTEPU-WTABLEG.                    
               10  FTEPU-LIBELLE         PIC X(30).                             
               10  FTEPU-NBMOIS          PIC X(02).                             
               10  FTEPU-NBMOIS-N       REDEFINES FTEPU-NBMOIS                  
                                         PIC 9(02).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01O8-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FTEPU-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FTEPU-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FTEPU-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FTEPU-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
