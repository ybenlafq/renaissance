      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGG12   EGG12                                              00000020
      ***************************************************************** 00000030
       01   EGG12I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNCONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNCONCI   PIC X(4).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENSGNL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLENSGNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLENSGNF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLENSGNI  PIC X(15).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLEMPLCL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLEMPLCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLEMPLCF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLEMPLCI  PIC X(15).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNCODICI  PIC X(7).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLREFI    PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCASSORL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCASSORL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCASSORF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCASSORI  PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCFAMI    PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLFAMI    PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPROL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAPPROF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCAPPROI  PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCMARQI   PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLMARQI   PIC X(20).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPOL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXPOF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCEXPOI   PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRIXL    COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MPRIXL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPRIXF    PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MPRIXI    PIC X(9).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATDEBL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MDATDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATDEBF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MDATDEBI  PIC X(8).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATFINL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MDATFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATFINF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MDATFINI  PIC X(8).                                       00000770
           02 MMAGI OCCURS   8 TIMES .                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMAGL  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MNMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMAGF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MNMAGI  PIC X(3).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMAGL  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MLMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLMAGF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MLMAGI  PIC X(20).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRMPL    COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MPRMPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPRMPF    PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MPRMPI    PIC X(9).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPMBUL    COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MPMBUL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPMBUF    PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MPMBUI    PIC X(9).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPSRPTTCL      COMP PIC S9(4).                            00000950
      *--                                                                       
           02 MPSRPTTCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPSRPTTCF      PIC X.                                     00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MPSRPTTCI      PIC X(9).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTMBL    COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MQTMBL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MQTMBF    PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MQTMBI    PIC X(5).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MZONCMDI  PIC X(15).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MLIBERRI  PIC X(58).                                      00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCODTRAI  PIC X(4).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCICSI    PIC X(5).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MNETNAMI  PIC X(8).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MSCREENI  PIC X(4).                                       00001260
      ***************************************************************** 00001270
      * SDF: EGG12   EGG12                                              00001280
      ***************************************************************** 00001290
       01   EGG12O REDEFINES EGG12I.                                    00001300
           02 FILLER    PIC X(12).                                      00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MDATJOUA  PIC X.                                          00001330
           02 MDATJOUC  PIC X.                                          00001340
           02 MDATJOUP  PIC X.                                          00001350
           02 MDATJOUH  PIC X.                                          00001360
           02 MDATJOUV  PIC X.                                          00001370
           02 MDATJOUO  PIC X(10).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MTIMJOUA  PIC X.                                          00001400
           02 MTIMJOUC  PIC X.                                          00001410
           02 MTIMJOUP  PIC X.                                          00001420
           02 MTIMJOUH  PIC X.                                          00001430
           02 MTIMJOUV  PIC X.                                          00001440
           02 MTIMJOUO  PIC X(5).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MWFONCA   PIC X.                                          00001470
           02 MWFONCC   PIC X.                                          00001480
           02 MWFONCP   PIC X.                                          00001490
           02 MWFONCH   PIC X.                                          00001500
           02 MWFONCV   PIC X.                                          00001510
           02 MWFONCO   PIC X(3).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MNCONCA   PIC X.                                          00001540
           02 MNCONCC   PIC X.                                          00001550
           02 MNCONCP   PIC X.                                          00001560
           02 MNCONCH   PIC X.                                          00001570
           02 MNCONCV   PIC X.                                          00001580
           02 MNCONCO   PIC X(4).                                       00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MLENSGNA  PIC X.                                          00001610
           02 MLENSGNC  PIC X.                                          00001620
           02 MLENSGNP  PIC X.                                          00001630
           02 MLENSGNH  PIC X.                                          00001640
           02 MLENSGNV  PIC X.                                          00001650
           02 MLENSGNO  PIC X(15).                                      00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MLEMPLCA  PIC X.                                          00001680
           02 MLEMPLCC  PIC X.                                          00001690
           02 MLEMPLCP  PIC X.                                          00001700
           02 MLEMPLCH  PIC X.                                          00001710
           02 MLEMPLCV  PIC X.                                          00001720
           02 MLEMPLCO  PIC X(15).                                      00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MNCODICA  PIC X.                                          00001750
           02 MNCODICC  PIC X.                                          00001760
           02 MNCODICP  PIC X.                                          00001770
           02 MNCODICH  PIC X.                                          00001780
           02 MNCODICV  PIC X.                                          00001790
           02 MNCODICO  PIC X(7).                                       00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MLREFA    PIC X.                                          00001820
           02 MLREFC    PIC X.                                          00001830
           02 MLREFP    PIC X.                                          00001840
           02 MLREFH    PIC X.                                          00001850
           02 MLREFV    PIC X.                                          00001860
           02 MLREFO    PIC X(20).                                      00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MCASSORA  PIC X.                                          00001890
           02 MCASSORC  PIC X.                                          00001900
           02 MCASSORP  PIC X.                                          00001910
           02 MCASSORH  PIC X.                                          00001920
           02 MCASSORV  PIC X.                                          00001930
           02 MCASSORO  PIC X(5).                                       00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MCFAMA    PIC X.                                          00001960
           02 MCFAMC    PIC X.                                          00001970
           02 MCFAMP    PIC X.                                          00001980
           02 MCFAMH    PIC X.                                          00001990
           02 MCFAMV    PIC X.                                          00002000
           02 MCFAMO    PIC X(5).                                       00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MLFAMA    PIC X.                                          00002030
           02 MLFAMC    PIC X.                                          00002040
           02 MLFAMP    PIC X.                                          00002050
           02 MLFAMH    PIC X.                                          00002060
           02 MLFAMV    PIC X.                                          00002070
           02 MLFAMO    PIC X(20).                                      00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MCAPPROA  PIC X.                                          00002100
           02 MCAPPROC  PIC X.                                          00002110
           02 MCAPPROP  PIC X.                                          00002120
           02 MCAPPROH  PIC X.                                          00002130
           02 MCAPPROV  PIC X.                                          00002140
           02 MCAPPROO  PIC X(5).                                       00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCMARQA   PIC X.                                          00002170
           02 MCMARQC   PIC X.                                          00002180
           02 MCMARQP   PIC X.                                          00002190
           02 MCMARQH   PIC X.                                          00002200
           02 MCMARQV   PIC X.                                          00002210
           02 MCMARQO   PIC X(5).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MLMARQA   PIC X.                                          00002240
           02 MLMARQC   PIC X.                                          00002250
           02 MLMARQP   PIC X.                                          00002260
           02 MLMARQH   PIC X.                                          00002270
           02 MLMARQV   PIC X.                                          00002280
           02 MLMARQO   PIC X(20).                                      00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MCEXPOA   PIC X.                                          00002310
           02 MCEXPOC   PIC X.                                          00002320
           02 MCEXPOP   PIC X.                                          00002330
           02 MCEXPOH   PIC X.                                          00002340
           02 MCEXPOV   PIC X.                                          00002350
           02 MCEXPOO   PIC X(5).                                       00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MPRIXA    PIC X.                                          00002380
           02 MPRIXC    PIC X.                                          00002390
           02 MPRIXP    PIC X.                                          00002400
           02 MPRIXH    PIC X.                                          00002410
           02 MPRIXV    PIC X.                                          00002420
           02 MPRIXO    PIC X(9).                                       00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MDATDEBA  PIC X.                                          00002450
           02 MDATDEBC  PIC X.                                          00002460
           02 MDATDEBP  PIC X.                                          00002470
           02 MDATDEBH  PIC X.                                          00002480
           02 MDATDEBV  PIC X.                                          00002490
           02 MDATDEBO  PIC X(8).                                       00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MDATFINA  PIC X.                                          00002520
           02 MDATFINC  PIC X.                                          00002530
           02 MDATFINP  PIC X.                                          00002540
           02 MDATFINH  PIC X.                                          00002550
           02 MDATFINV  PIC X.                                          00002560
           02 MDATFINO  PIC X(8).                                       00002570
           02 MMAGO OCCURS   8 TIMES .                                  00002580
             03 FILLER       PIC X(2).                                  00002590
             03 MNMAGA  PIC X.                                          00002600
             03 MNMAGC  PIC X.                                          00002610
             03 MNMAGP  PIC X.                                          00002620
             03 MNMAGH  PIC X.                                          00002630
             03 MNMAGV  PIC X.                                          00002640
             03 MNMAGO  PIC X(3).                                       00002650
             03 FILLER       PIC X(2).                                  00002660
             03 MLMAGA  PIC X.                                          00002670
             03 MLMAGC  PIC X.                                          00002680
             03 MLMAGP  PIC X.                                          00002690
             03 MLMAGH  PIC X.                                          00002700
             03 MLMAGV  PIC X.                                          00002710
             03 MLMAGO  PIC X(20).                                      00002720
           02 FILLER    PIC X(2).                                       00002730
           02 MPRMPA    PIC X.                                          00002740
           02 MPRMPC    PIC X.                                          00002750
           02 MPRMPP    PIC X.                                          00002760
           02 MPRMPH    PIC X.                                          00002770
           02 MPRMPV    PIC X.                                          00002780
           02 MPRMPO    PIC X(9).                                       00002790
           02 FILLER    PIC X(2).                                       00002800
           02 MPMBUA    PIC X.                                          00002810
           02 MPMBUC    PIC X.                                          00002820
           02 MPMBUP    PIC X.                                          00002830
           02 MPMBUH    PIC X.                                          00002840
           02 MPMBUV    PIC X.                                          00002850
           02 MPMBUO    PIC X(9).                                       00002860
           02 FILLER    PIC X(2).                                       00002870
           02 MPSRPTTCA      PIC X.                                     00002880
           02 MPSRPTTCC PIC X.                                          00002890
           02 MPSRPTTCP PIC X.                                          00002900
           02 MPSRPTTCH PIC X.                                          00002910
           02 MPSRPTTCV PIC X.                                          00002920
           02 MPSRPTTCO      PIC X(9).                                  00002930
           02 FILLER    PIC X(2).                                       00002940
           02 MQTMBA    PIC X.                                          00002950
           02 MQTMBC    PIC X.                                          00002960
           02 MQTMBP    PIC X.                                          00002970
           02 MQTMBH    PIC X.                                          00002980
           02 MQTMBV    PIC X.                                          00002990
           02 MQTMBO    PIC X(5).                                       00003000
           02 FILLER    PIC X(2).                                       00003010
           02 MZONCMDA  PIC X.                                          00003020
           02 MZONCMDC  PIC X.                                          00003030
           02 MZONCMDP  PIC X.                                          00003040
           02 MZONCMDH  PIC X.                                          00003050
           02 MZONCMDV  PIC X.                                          00003060
           02 MZONCMDO  PIC X(15).                                      00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MLIBERRA  PIC X.                                          00003090
           02 MLIBERRC  PIC X.                                          00003100
           02 MLIBERRP  PIC X.                                          00003110
           02 MLIBERRH  PIC X.                                          00003120
           02 MLIBERRV  PIC X.                                          00003130
           02 MLIBERRO  PIC X(58).                                      00003140
           02 FILLER    PIC X(2).                                       00003150
           02 MCODTRAA  PIC X.                                          00003160
           02 MCODTRAC  PIC X.                                          00003170
           02 MCODTRAP  PIC X.                                          00003180
           02 MCODTRAH  PIC X.                                          00003190
           02 MCODTRAV  PIC X.                                          00003200
           02 MCODTRAO  PIC X(4).                                       00003210
           02 FILLER    PIC X(2).                                       00003220
           02 MCICSA    PIC X.                                          00003230
           02 MCICSC    PIC X.                                          00003240
           02 MCICSP    PIC X.                                          00003250
           02 MCICSH    PIC X.                                          00003260
           02 MCICSV    PIC X.                                          00003270
           02 MCICSO    PIC X(5).                                       00003280
           02 FILLER    PIC X(2).                                       00003290
           02 MNETNAMA  PIC X.                                          00003300
           02 MNETNAMC  PIC X.                                          00003310
           02 MNETNAMP  PIC X.                                          00003320
           02 MNETNAMH  PIC X.                                          00003330
           02 MNETNAMV  PIC X.                                          00003340
           02 MNETNAMO  PIC X(8).                                       00003350
           02 FILLER    PIC X(2).                                       00003360
           02 MSCREENA  PIC X.                                          00003370
           02 MSCREENC  PIC X.                                          00003380
           02 MSCREENP  PIC X.                                          00003390
           02 MSCREENH  PIC X.                                          00003400
           02 MSCREENV  PIC X.                                          00003410
           02 MSCREENO  PIC X(4).                                       00003420
                                                                                
