      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                        00000010
                                                                                
      *--  ZONES RESERVEES APPLICATIVES OPTION 1 ----------------- 3855 00000020
                                                                        00000030
           02 COMM-BS13-BS15-APPLI REDEFINES COMM-BS10-FILLER.          00000040
              03 COMM-13.                                               00000050
                                                                        00000060
                 05 COMM-13-CODE            PIC X(5).                   00000070
                 05 COMM-13-INDTS           PIC S9(5) COMP-3.           00000080
                 05 COMM-13-INDMAX          PIC S9(5) COMP-3.           00000090
                                                                        00000100
              03 COMM-15.                                               00000110
                 05 COMM-15-ENTETE.                                     00000120
                    10 COMM-15-CPROFASS     PIC X(5).                   00000130
                    10 COMM-15-LPROFASS     PIC X(20).                  00000140
                    10 COMM-15-CTYPENT      PIC X(5).                   00000150
                    10 COMM-15-LTYPENT      PIC X(20).                  00000160
                    10 COMM-15-CCALCUL      PIC X(05).                  00000170
                    10 COMM-15-LCALCUL      PIC X(20).                  00000180
                    10 COMM-15-PRESTA       PIC X(05).                  00000190
                    10 COMM-15-CTYPPREST    PIC X(05).                  00000200
                    10 COMM-15-CMARQ        PIC X(05).                  00000210
                    10 COMM-15-NSEQ         PIC X(03).                  00000220
      *-- GESTION TS POSITION                                           00000230
                 05 COMM-15-POSITION-TS.                                00000240
      *                LIGNE                                            00000250
                    10 COMM-15-INDTS           PIC S9(5) COMP-3.        00000260
                    10 COMM-15-INDMAX          PIC S9(5) COMP-3.        00000270
      *-- GESTION NUM MAX DE NSEQENT                                    00000280
                 05 COMM-15-NSEQENTMAX      PIC 9(3).                   00000290
              03 COMM-BS13-BS15-FILLER           PIC X(3689).           00000300
                                                                                
