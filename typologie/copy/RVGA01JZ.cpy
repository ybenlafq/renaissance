      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE COSAV COEFFICIENT INTERFACE SAV        *        
      *----------------------------------------------------------------*        
       01  RVGA01JZ.                                                            
           05  COSAV-CTABLEG2    PIC X(15).                                     
           05  COSAV-CTABLEG2-REDEF REDEFINES COSAV-CTABLEG2.                   
               10  COSAV-CCOEF           PIC X(05).                             
           05  COSAV-WTABLEG     PIC X(80).                                     
           05  COSAV-WTABLEG-REDEF  REDEFINES COSAV-WTABLEG.                    
               10  COSAV-LIBELLE         PIC X(20).                             
               10  COSAV-COEF            PIC S9(03)V9(02)     COMP-3.           
               10  COSAV-DEFFET          PIC X(08).                             
               10  COSAV-DEFFET-N       REDEFINES COSAV-DEFFET                  
                                         PIC 9(08).                             
               10  COSAV-ANCCOEF         PIC S9(03)V9(02)     COMP-3.           
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01JZ-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COSAV-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  COSAV-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COSAV-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  COSAV-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
