      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GESTION DES REGLES OPCO                                         00000020
      ***************************************************************** 00000030
       01   ETH18I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPCOL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MCOPCOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOPCOF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCOPCOI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLOPCOL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MLOPCOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLOPCOF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLOPCOI   PIC X(23).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWDATEEFL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MWDATEEFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWDATEEFF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MWDATEEFI      PIC X.                                     00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPCODUPL     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCOPCODUPL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCOPCODUPF     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCOPCODUPI     PIC X(3).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLOPCODUPL     COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MLOPCODUPL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLOPCODUPF     PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLOPCODUPI     PIC X(23).                                 00000330
           02 MLIGNEI OCCURS   12 TIMES .                               00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMPOSL     COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MCOMPOSL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCOMPOSF     PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MCOMPOSI     PIC X(3).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MIDREGLEL    COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MIDREGLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MIDREGLEF    PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MIDREGLEI    PIC X(10).                                 00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREGLEL     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MLREGLEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLREGLEF     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MLREGLEI     PIC X(20).                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWACTIFL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MWACTIFL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWACTIFF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MWACTIFI     PIC X.                                     00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWINCLUL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MWINCLUL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWINCLUF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MWINCLUI     PIC X.                                     00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSMTTL  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MSMTTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSMTTF  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MSMTTI  PIC X.                                          00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSPOURCL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MSPOURCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSPOURCF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MSPOURCI     PIC X.                                     00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMTTREFL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MMTTREFL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMTTREFF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MMTTREFI     PIC X(5).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MBMTTL  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MBMTTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MBMTTF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MBMTTI  PIC X.                                          00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MBPOURCL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MBPOURCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MBPOURCF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MBPOURCI     PIC X.                                     00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDELTAL      COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MDELTAL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDELTAF      PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MDELTAI      PIC X.                                     00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MDEFFETI     PIC X(8).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MLIBERRI  PIC X(78).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCODTRAI  PIC X(4).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCICSI    PIC X(5).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MNETNAMI  PIC X(8).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MSCREENI  PIC X(4).                                       00001020
      ***************************************************************** 00001030
      * GESTION DES REGLES OPCO                                         00001040
      ***************************************************************** 00001050
       01   ETH18O REDEFINES ETH18I.                                    00001060
           02 FILLER    PIC X(12).                                      00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MDATJOUA  PIC X.                                          00001090
           02 MDATJOUC  PIC X.                                          00001100
           02 MDATJOUP  PIC X.                                          00001110
           02 MDATJOUH  PIC X.                                          00001120
           02 MDATJOUV  PIC X.                                          00001130
           02 MDATJOUO  PIC X(10).                                      00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MTIMJOUA  PIC X.                                          00001160
           02 MTIMJOUC  PIC X.                                          00001170
           02 MTIMJOUP  PIC X.                                          00001180
           02 MTIMJOUH  PIC X.                                          00001190
           02 MTIMJOUV  PIC X.                                          00001200
           02 MTIMJOUO  PIC X(5).                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MCOPCOA   PIC X.                                          00001230
           02 MCOPCOC   PIC X.                                          00001240
           02 MCOPCOP   PIC X.                                          00001250
           02 MCOPCOH   PIC X.                                          00001260
           02 MCOPCOV   PIC X.                                          00001270
           02 MCOPCOO   PIC X(3).                                       00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MLOPCOA   PIC X.                                          00001300
           02 MLOPCOC   PIC X.                                          00001310
           02 MLOPCOP   PIC X.                                          00001320
           02 MLOPCOH   PIC X.                                          00001330
           02 MLOPCOV   PIC X.                                          00001340
           02 MLOPCOO   PIC X(23).                                      00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MWDATEEFA      PIC X.                                     00001370
           02 MWDATEEFC PIC X.                                          00001380
           02 MWDATEEFP PIC X.                                          00001390
           02 MWDATEEFH PIC X.                                          00001400
           02 MWDATEEFV PIC X.                                          00001410
           02 MWDATEEFO      PIC X.                                     00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MCOPCODUPA     PIC X.                                     00001440
           02 MCOPCODUPC     PIC X.                                     00001450
           02 MCOPCODUPP     PIC X.                                     00001460
           02 MCOPCODUPH     PIC X.                                     00001470
           02 MCOPCODUPV     PIC X.                                     00001480
           02 MCOPCODUPO     PIC X(3).                                  00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MLOPCODUPA     PIC X.                                     00001510
           02 MLOPCODUPC     PIC X.                                     00001520
           02 MLOPCODUPP     PIC X.                                     00001530
           02 MLOPCODUPH     PIC X.                                     00001540
           02 MLOPCODUPV     PIC X.                                     00001550
           02 MLOPCODUPO     PIC X(23).                                 00001560
           02 MLIGNEO OCCURS   12 TIMES .                               00001570
             03 FILLER       PIC X(2).                                  00001580
             03 MCOMPOSA     PIC X.                                     00001590
             03 MCOMPOSC     PIC X.                                     00001600
             03 MCOMPOSP     PIC X.                                     00001610
             03 MCOMPOSH     PIC X.                                     00001620
             03 MCOMPOSV     PIC X.                                     00001630
             03 MCOMPOSO     PIC X(3).                                  00001640
             03 FILLER       PIC X(2).                                  00001650
             03 MIDREGLEA    PIC X.                                     00001660
             03 MIDREGLEC    PIC X.                                     00001670
             03 MIDREGLEP    PIC X.                                     00001680
             03 MIDREGLEH    PIC X.                                     00001690
             03 MIDREGLEV    PIC X.                                     00001700
             03 MIDREGLEO    PIC X(10).                                 00001710
             03 FILLER       PIC X(2).                                  00001720
             03 MLREGLEA     PIC X.                                     00001730
             03 MLREGLEC     PIC X.                                     00001740
             03 MLREGLEP     PIC X.                                     00001750
             03 MLREGLEH     PIC X.                                     00001760
             03 MLREGLEV     PIC X.                                     00001770
             03 MLREGLEO     PIC X(20).                                 00001780
             03 FILLER       PIC X(2).                                  00001790
             03 MWACTIFA     PIC X.                                     00001800
             03 MWACTIFC     PIC X.                                     00001810
             03 MWACTIFP     PIC X.                                     00001820
             03 MWACTIFH     PIC X.                                     00001830
             03 MWACTIFV     PIC X.                                     00001840
             03 MWACTIFO     PIC X.                                     00001850
             03 FILLER       PIC X(2).                                  00001860
             03 MWINCLUA     PIC X.                                     00001870
             03 MWINCLUC     PIC X.                                     00001880
             03 MWINCLUP     PIC X.                                     00001890
             03 MWINCLUH     PIC X.                                     00001900
             03 MWINCLUV     PIC X.                                     00001910
             03 MWINCLUO     PIC X.                                     00001920
             03 FILLER       PIC X(2).                                  00001930
             03 MSMTTA  PIC X.                                          00001940
             03 MSMTTC  PIC X.                                          00001950
             03 MSMTTP  PIC X.                                          00001960
             03 MSMTTH  PIC X.                                          00001970
             03 MSMTTV  PIC X.                                          00001980
             03 MSMTTO  PIC X.                                          00001990
             03 FILLER       PIC X(2).                                  00002000
             03 MSPOURCA     PIC X.                                     00002010
             03 MSPOURCC     PIC X.                                     00002020
             03 MSPOURCP     PIC X.                                     00002030
             03 MSPOURCH     PIC X.                                     00002040
             03 MSPOURCV     PIC X.                                     00002050
             03 MSPOURCO     PIC X.                                     00002060
             03 FILLER       PIC X(2).                                  00002070
             03 MMTTREFA     PIC X.                                     00002080
             03 MMTTREFC     PIC X.                                     00002090
             03 MMTTREFP     PIC X.                                     00002100
             03 MMTTREFH     PIC X.                                     00002110
             03 MMTTREFV     PIC X.                                     00002120
             03 MMTTREFO     PIC X(5).                                  00002130
             03 FILLER       PIC X(2).                                  00002140
             03 MBMTTA  PIC X.                                          00002150
             03 MBMTTC  PIC X.                                          00002160
             03 MBMTTP  PIC X.                                          00002170
             03 MBMTTH  PIC X.                                          00002180
             03 MBMTTV  PIC X.                                          00002190
             03 MBMTTO  PIC X.                                          00002200
             03 FILLER       PIC X(2).                                  00002210
             03 MBPOURCA     PIC X.                                     00002220
             03 MBPOURCC     PIC X.                                     00002230
             03 MBPOURCP     PIC X.                                     00002240
             03 MBPOURCH     PIC X.                                     00002250
             03 MBPOURCV     PIC X.                                     00002260
             03 MBPOURCO     PIC X.                                     00002270
             03 FILLER       PIC X(2).                                  00002280
             03 MDELTAA      PIC X.                                     00002290
             03 MDELTAC PIC X.                                          00002300
             03 MDELTAP PIC X.                                          00002310
             03 MDELTAH PIC X.                                          00002320
             03 MDELTAV PIC X.                                          00002330
             03 MDELTAO      PIC X.                                     00002340
             03 FILLER       PIC X(2).                                  00002350
             03 MDEFFETA     PIC X.                                     00002360
             03 MDEFFETC     PIC X.                                     00002370
             03 MDEFFETP     PIC X.                                     00002380
             03 MDEFFETH     PIC X.                                     00002390
             03 MDEFFETV     PIC X.                                     00002400
             03 MDEFFETO     PIC X(8).                                  00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MLIBERRA  PIC X.                                          00002430
           02 MLIBERRC  PIC X.                                          00002440
           02 MLIBERRP  PIC X.                                          00002450
           02 MLIBERRH  PIC X.                                          00002460
           02 MLIBERRV  PIC X.                                          00002470
           02 MLIBERRO  PIC X(78).                                      00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MCODTRAA  PIC X.                                          00002500
           02 MCODTRAC  PIC X.                                          00002510
           02 MCODTRAP  PIC X.                                          00002520
           02 MCODTRAH  PIC X.                                          00002530
           02 MCODTRAV  PIC X.                                          00002540
           02 MCODTRAO  PIC X(4).                                       00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MCICSA    PIC X.                                          00002570
           02 MCICSC    PIC X.                                          00002580
           02 MCICSP    PIC X.                                          00002590
           02 MCICSH    PIC X.                                          00002600
           02 MCICSV    PIC X.                                          00002610
           02 MCICSO    PIC X(5).                                       00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MNETNAMA  PIC X.                                          00002640
           02 MNETNAMC  PIC X.                                          00002650
           02 MNETNAMP  PIC X.                                          00002660
           02 MNETNAMH  PIC X.                                          00002670
           02 MNETNAMV  PIC X.                                          00002680
           02 MNETNAMO  PIC X(8).                                       00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MSCREENA  PIC X.                                          00002710
           02 MSCREENC  PIC X.                                          00002720
           02 MSCREENP  PIC X.                                          00002730
           02 MSCREENH  PIC X.                                          00002740
           02 MSCREENV  PIC X.                                          00002750
           02 MSCREENO  PIC X(4).                                       00002760
                                                                                
