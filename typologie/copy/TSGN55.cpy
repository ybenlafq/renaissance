      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : GESTION DES CONCURRENTS NATIONAUX      (GN55)          *         
      *****************************************************************         
      *                                                               *         
       01  TS-GN55.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GN55-LONG                 PIC S9(03) COMP-3                    
                                           VALUE +25.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-GN55-DONNEES.                                                  
      *----------------------------------  CODE ACTION                          
              03 TS-GN55-ACT               PIC X.                               
      *----------------------------------  CODE CONCURRENT                      
              03 TS-GN55-NCONC             PIC X(4).                            
      *----------------------------------  LIBELLE CONCURRENT                   
              03 TS-GN55-LCONC             PIC X(20).                           
                                                                                
