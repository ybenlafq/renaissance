      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HCLIE TYPE DE LIEU EMETTEUR  CHS       *        
      *----------------------------------------------------------------*        
       01  RVGA01Q4.                                                            
           05  HCLIE-CTABLEG2    PIC X(15).                                     
           05  HCLIE-CTABLEG2-REDEF REDEFINES HCLIE-CTABLEG2.                   
               10  HCLIE-CTLIEU          PIC X(01).                             
           05  HCLIE-WTABLEG     PIC X(80).                                     
           05  HCLIE-WTABLEG-REDEF  REDEFINES HCLIE-WTABLEG.                    
               10  HCLIE-WACTIF          PIC X(01).                             
               10  HCLIE-LIBELLE         PIC X(20).                             
               10  HCLIE-CTYPLIEU        PIC X(01).                             
               10  HCLIE-CTYPSOC         PIC X(03).                             
               10  HCLIE-WCONTROL        PIC X(05).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01Q4-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HCLIE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HCLIE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HCLIE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HCLIE-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
