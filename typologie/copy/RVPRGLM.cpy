      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PRGLM LIEN FLUX LM7 EXTRACTEUR         *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVPRGLM .                                                            
           05  PRGLM-CTABLEG2    PIC X(15).                                     
           05  PRGLM-CTABLEG2-REDEF REDEFINES PRGLM-CTABLEG2.                   
               10  PRGLM-CTYPMES         PIC X(03).                             
               10  PRGLM-NCLETECH        PIC X(10).                             
           05  PRGLM-WTABLEG     PIC X(80).                                     
           05  PRGLM-WTABLEG-REDEF  REDEFINES PRGLM-WTABLEG.                    
               10  PRGLM-CPROG           PIC X(05).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVPRGLM-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRGLM-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PRGLM-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRGLM-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PRGLM-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
