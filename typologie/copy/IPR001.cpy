      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPR001 AU 08/11/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,07,BI,A,                          *        
      *                           24,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPR001.                                                        
            05 NOMETAT-IPR001           PIC X(6) VALUE 'IPR001'.                
            05 RUPTURES-IPR001.                                                 
           10 IPR001-CFAM               PIC X(05).                      007  005
           10 IPR001-CMARQ              PIC X(05).                      012  005
           10 IPR001-NCODIC             PIC X(07).                      017  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPR001-SEQUENCE           PIC S9(04) COMP.                024  002
      *--                                                                       
           10 IPR001-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPR001.                                                   
           10 IPR001-LREF               PIC X(20).                      026  020
           10 IPR001-NZONPRIX1          PIC X(02).                      046  002
           10 IPR001-NZONPRIX2          PIC X(02).                      048  002
           10 IPR001-NZONPRIX3          PIC X(02).                      050  002
           10 IPR001-NZONPRIX4          PIC X(02).                      052  002
           10 IPR001-NZONPRIX5          PIC X(02).                      054  002
           10 IPR001-NZONPRIX6          PIC X(02).                      056  002
           10 IPR001-PCOMM1             PIC S9(03)V9(1) COMP-3.         058  003
           10 IPR001-PCOMM2             PIC S9(03)V9(1) COMP-3.         061  003
           10 IPR001-PCOMM3             PIC S9(03)V9(1) COMP-3.         064  003
           10 IPR001-PCOMM4             PIC S9(03)V9(1) COMP-3.         067  003
           10 IPR001-PCOMM5             PIC S9(03)V9(1) COMP-3.         070  003
           10 IPR001-PCOMM6             PIC S9(03)V9(1) COMP-3.         073  003
           10 IPR001-PSTDTTC1           PIC S9(05)V9(2) COMP-3.         076  004
           10 IPR001-PSTDTTC2           PIC S9(05)V9(2) COMP-3.         080  004
           10 IPR001-PSTDTTC3           PIC S9(05)V9(2) COMP-3.         084  004
           10 IPR001-PSTDTTC4           PIC S9(05)V9(2) COMP-3.         088  004
           10 IPR001-PSTDTTC5           PIC S9(05)V9(2) COMP-3.         092  004
           10 IPR001-PSTDTTC6           PIC S9(05)V9(2) COMP-3.         096  004
            05 FILLER                      PIC X(413).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPR001-LONG           PIC S9(4)   COMP  VALUE +099.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPR001-LONG           PIC S9(4) COMP-5  VALUE +099.           
                                                                                
      *}                                                                        
