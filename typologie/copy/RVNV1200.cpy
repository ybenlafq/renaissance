      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RTNV12                        *        
      ******************************************************************        
       01  RVNV1200.                                                            
      *                       IDENTIFIANT MSG                                   
           10 NV12-IDENTIFIANT     PIC X(12).                                   
      *                       TYPE DE DTD                                       
           10 NV12-CDTD            PIC X(20).                                   
      *                       EMETTEUR DU MSG                                   
           10 NV12-CEMETTEUR       PIC X(20).                                   
      *                       DESTINATEUR DU MSG                                
           10 NV12-CDESTI          PIC X(20).                                   
      *                       CLE TRT                                           
           10 NV12-CLE             PIC X(60).                                   
      *                       ACTION                                            
           10 NV12-ACTION          PIC X(1).                                    
      *                       TIMESTAMP TRT                                     
           10 NV12-TIMESTAMP       PIC X(26).                                   
      *                       LIBELLE ANOMALIE                                  
           10 NV12-LIBANO          PIC X(100).                                  
      *                       NB REACTIVEE                                      
           10 NV12-QMSGREACT       PIC S9(7)V USAGE COMP-3.                     
      *                       ALERTE SVP                                        
           10 NV12-WSVP            PIC X(1).                                    
      *                       TIMESTAMP AR                                      
           10 NV12-DSYST           PIC X(26).                                   
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 9       *        
      ******************************************************************        
      *   LISTE DES FLAGS DE LA TABLE RVNV1100                                  
      **********************************************************                
       01  RVNV1200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV12-IDENTIFIANT-F         PIC S9(4) COMP.                        
      *--                                                                       
           10 NV12-IDENTIFIANT-F         PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV12-CDTD-F                PIC S9(4) COMP.                        
      *--                                                                       
           10 NV12-CDTD-F                PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV12-CEMETTEUR-F           PIC S9(4) COMP.                        
      *--                                                                       
           10 NV12-CEMETTEUR-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV12-CDESTI-F              PIC S9(4) COMP.                        
      *--                                                                       
           10 NV12-CDESTI-F              PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV12-CLE-F                 PIC S9(4) COMP.                        
      *--                                                                       
           10 NV12-CLE-F                 PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV12-ACTION-F              PIC S9(4) COMP.                        
      *--                                                                       
           10 NV12-ACTION-F              PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV12-TIMESTAMP-F           PIC S9(4) COMP.                        
      *--                                                                       
           10 NV12-TIMESTAMP-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV12-LIBANO-F              PIC S9(4) COMP.                        
      *--                                                                       
           10 NV12-LIBANO-F              PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV12-QMSGREACT-F           PIC S9(4) COMP.                        
      *--                                                                       
           10 NV12-QMSGREACT-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV12-WSVP-F                PIC S9(4) COMP.                        
      *--                                                                       
           10 NV12-WSVP-F                PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV12-DSYST-F               PIC S9(4) COMP.                        
      *                                                                         
      *--                                                                       
           10 NV12-DSYST-F               PIC S9(4) COMP-5.                      
                                                                                
      *}                                                                        
