      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PMODP MODES DE PAIEMENT A EDITER       *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01JW.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01JW.                                                            
      *}                                                                        
           05  PMODP-CTABLEG2    PIC X(15).                                     
           05  PMODP-CTABLEG2-REDEF REDEFINES PMODP-CTABLEG2.                   
               10  PMODP-CODETAT         PIC X(06).                             
               10  PMODP-WDARDAC         PIC X(03).                             
               10  PMODP-NSEQ            PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  PMODP-NSEQ-N         REDEFINES PMODP-NSEQ                    
                                         PIC 9(02).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  PMODP-WTABLEG     PIC X(80).                                     
           05  PMODP-WTABLEG-REDEF  REDEFINES PMODP-WTABLEG.                    
               10  PMODP-MODP            PIC X(01).                             
               10  PMODP-LIBMODP         PIC X(20).                             
               10  PMODP-WACTIF          PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01JW-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01JW-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PMODP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PMODP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PMODP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PMODP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
