      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FTENT ENTITES MODELE                   *        
      *----------------------------------------------------------------*        
       01  RVGA01O3.                                                            
           05  FTENT-CTABLEG2    PIC X(15).                                     
           05  FTENT-CTABLEG2-REDEF REDEFINES FTENT-CTABLEG2.                   
               10  FTENT-ENTITE          PIC X(05).                             
           05  FTENT-WTABLEG     PIC X(80).                                     
           05  FTENT-WTABLEG-REDEF  REDEFINES FTENT-WTABLEG.                    
               10  FTENT-LIBELLE         PIC X(30).                             
               10  FTENT-DEVISE          PIC X(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01O3-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FTENT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FTENT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FTENT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FTENT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
