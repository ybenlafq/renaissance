      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGA0603                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA0603                         
      *---------------------------------------------------------                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA0603.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA0603.                                                            
      *}                                                                        
           02  GA06-NENTCDE                                                     
               PIC X(0005).                                                     
           02  GA06-LENTCDE                                                     
               PIC X(0020).                                                     
           02  GA06-LENTCDEADR1                                                 
               PIC X(0032).                                                     
           02  GA06-LENTCDEADR2                                                 
               PIC X(0032).                                                     
           02  GA06-NENTCDEADR3                                                 
               PIC X(0005).                                                     
           02  GA06-LENTCDEADR4                                                 
               PIC X(0026).                                                     
           02  GA06-NENTCDETEL                                                  
               PIC X(0015).                                                     
           02  GA06-CENTCDETELX                                                 
               PIC X(0015).                                                     
           02  GA06-CDEVISE                                                     
               PIC X(0003).                                                     
           02  GA06-DEFFET                                                      
               PIC X(0008).                                                     
           02  GA06-CDEVISA                                                     
               PIC X(0003).                                                     
E0268      02  GA06-CPAYS                                                       
               PIC X(0002).                                                     
E0268      02  GA06-WIMPORT                                                     
               PIC X(0001).                                                     
E0268      02  GA06-CECO                                                        
               PIC X(0003).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGA0603                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA0602-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA0602-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA06-NENTCDE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA06-NENTCDE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA06-LENTCDE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA06-LENTCDE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA06-LENTCDEADR1-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA06-LENTCDEADR1-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA06-LENTCDEADR2-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA06-LENTCDEADR2-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA06-NENTCDEADR3-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA06-NENTCDEADR3-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA06-LENTCDEADR4-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA06-LENTCDEADR4-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA06-NENTCDETEL-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA06-NENTCDETEL-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA06-CENTCDETELX-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA06-CENTCDETELX-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA06-CDEVISE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA06-CDEVISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA06-DEFFET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA06-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA06-CDEVISA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA06-CDEVISA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
E0268 *    02  GA06-CPYAS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA06-CPYAS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
E0268 *    02  GA06-WIMPORT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA06-WIMPORT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
E0268 *    02  GA06-CECO-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA06-CECO-F                                                      
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
