      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGA2201                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA2201                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA2201.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA2201.                                                            
      *}                                                                        
           02  GA22-CMARQ                                                       
               PIC X(0005).                                                     
           02  GA22-LMARQ                                                       
               PIC X(0020).                                                     
           02  GA22-DEBEXER                                                     
               PIC X(0004).                                                     
           02  GA22-FINEXER                                                     
               PIC X(0004).                                                     
           02  GA22-DAVOIR                                                      
               PIC X(0008).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGA2201                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA2201-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA2201-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA22-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA22-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA22-LMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA22-LMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA22-DEBEXER-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA22-DEBEXER-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA22-FINEXER-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA22-FINEXER-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA22-DAVOIR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA22-DAVOIR-F                                                    
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
