      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TPSAV TYPES DE PRODUIT SAV             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01PI.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01PI.                                                            
      *}                                                                        
           05  TPSAV-CTABLEG2    PIC X(15).                                     
           05  TPSAV-CTABLEG2-REDEF REDEFINES TPSAV-CTABLEG2.                   
               10  TPSAV-CTPSAV          PIC X(05).                             
           05  TPSAV-WTABLEG     PIC X(80).                                     
           05  TPSAV-WTABLEG-REDEF  REDEFINES TPSAV-WTABLEG.                    
               10  TPSAV-LTPSAV          PIC X(20).                             
               10  TPSAV-WACTIF          PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01PI-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01PI-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TPSAV-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TPSAV-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TPSAV-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TPSAV-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
