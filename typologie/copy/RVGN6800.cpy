      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00030000
      *   COPY DE LA TABLE RVGN6800                                     00040001
      **********************************************************        00050000
      *   LISTE DES HOST VARIABLES DE LA VUE RVGN6800                   00060001
      **********************************************************        00070000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGN6800.                                                    00080001
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGN6800.                                                            
      *}                                                                        
           05  GN68-NCODIC     PIC X(7).                                00120001
           05  GN68-DEFFET     PIC X(8).                                00140001
           05  GN68-WOA        PIC X(1).                                00150001
           05  GN68-NCODICS    PIC X(7).                                00151001
           05  GN68-DSYST PIC S9(13) COMP-3.                            00160001
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************        00170000
      *   LISTE DES FLAGS DE LA TABLE RVGN6800                          00180001
      **********************************************************        00190000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGN6800-FLAGS.                                              00200001
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGN6800-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN68-NSOCIETE-F                                          00210001
      *        PIC S9(4) COMP.                                          00211000
      *--                                                                       
           05  GN68-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN68-DEFFET-F                                            00260001
      *        PIC S9(4) COMP.                                          00261000
      *--                                                                       
           05  GN68-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN68-WOA-F                                               00262002
      *        PIC S9(4) COMP.                                          00262101
      *--                                                                       
           05  GN68-WOA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN68-NCODICS-F                                           00263002
      *        PIC S9(4) COMP.                                          00264001
      *--                                                                       
           05  GN68-NCODICS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN68-DSYST-F                                             00280001
      *        PIC S9(4) COMP.                                          00290000
      *                                                                         
      *--                                                                       
           05  GN68-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
