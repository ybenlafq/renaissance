      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PPOPT CODES OPTIONS                    *        
      *----------------------------------------------------------------*        
       01  RVGA01RV.                                                            
           05  PPOPT-CTABLEG2    PIC X(15).                                     
           05  PPOPT-CTABLEG2-REDEF REDEFINES PPOPT-CTABLEG2.                   
               10  PPOPT-CPRIME          PIC X(05).                             
           05  PPOPT-WTABLEG     PIC X(80).                                     
           05  PPOPT-WTABLEG-REDEF  REDEFINES PPOPT-WTABLEG.                    
               10  PPOPT-WACTIF          PIC X(01).                             
               10  PPOPT-COPTION         PIC X(02).                             
               10  PPOPT-LOPTION         PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01RV-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PPOPT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PPOPT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PPOPT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PPOPT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
