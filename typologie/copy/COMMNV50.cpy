      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * ATTENTION CETTE COMM EST EN FAIT UTILISEE POUR                          
      * LE LINK NV50  < = > MXL01                                               
      * !! C EST UN REDIFINE DE LA COMM COMMXML   !!!                           
        01 NV50-DONNEES-PARSE  .                                                
          03 NV50-NOMPROG         PIC X(06).                                    
          03 NV50-DTD             PIC X(20).                                    
          03 NV50-CRETOUR         PIC X(02).                                    
          03 NV50-LRETOUR         PIC X(50).                                    
          03 NV50-ITEM-XSD        PIC 9(05) COMP-3.                             
          03 NV50-DONNEES-LG      PIC 9(07) COMP-3.                             
          03 DONNEES-PLAT         PIC X(32400).                                 
          03 NV50-DONNEES-OK   REDEFINES DONNEES-PLAT.                          
             05 NV50-ENTETE-OK.                                                 
                07 ENTETE-DRET-OK       PIC X(26).                              
      *       RESTE                     PIC X(32274) .                          
             05 NV50-LISTE-CLE-OK.                                              
                07 I-NV50-OK            PIC  9(3)  USAGE COMP-3.                
      *         CETTE ZONE DOIT ETRE EGALE � 100                                
                07 I-NV50-MAX-OK        PIC  9(3)  USAGE COMP-3.                
                07 NV50-TABLE-OK    OCCURS 500.                                 
                   10 NV50-SOURCE-OK       PIC X(15).                           
                   10 NV50-DEST-OK         PIC X(15).                           
                   10 NV50-DTD-OK          PIC X(20).                           
                   10 NV50-ID-OK           PIC X(12).                           
             05 FILLER                     PIC X(1270).                         
          03 NV50-DONNEES-KO   REDEFINES DONNEES-PLAT.                          
             10 NV50-ENTETE .                                                   
                15 ENTETE-IDENTIFIANT   PIC X(12).                              
                15 ENTETE-CEMETTEUR     PIC X(06).                              
                15 ENTETE-CDESTI        PIC X(06).                              
                15 ENTETE-CDTD          PIC X(20).                              
                15 ENTETE-CRETOUR       PIC X(02).                              
                15 ENTETE-LIBELLE       PIC X(100).                             
                15 ENTETE-TIMESTAMP     PIC X(26).                              
      *       RESTE                     PIC X(32234) .                          
             10 NV50-LISTE-CLE .                                                
                15 I-NV50               PIC  9(3)  USAGE COMP-3.                
      *         CETTE ZONE DOIT ETRE EGALE � 100                                
                15 I-NV50-MAX           PIC  9(3)  USAGE COMP-3.                
                15  FILLER PIC X.                                               
                  88  TABLEAU-DEPASSE          VALUE 'O'.                       
                15 NV50-TABLE-CLE  .                                            
                 18 NV50-TABLE       OCCURS 100.                                
                   20 NV50-CLE             PIC X(60).                           
                   20 NV50-ACTION          PIC X(1).                            
                   20 NV50-TIMESTAMP       PIC X(26).                           
                   20 NV50-EXCEPTION       PIC X(100).                          
                15 NV50-POS-ECHEC     PIC 9(05).                                
                15 NV50-FILLER        PIC X(23).                                
                                                                                
