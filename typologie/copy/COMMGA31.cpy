      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGA31                    TR: GA31  *    00020001
      *              CONSULTATION ARTICLES REFERENCES              *    00030000
      **************************************************************    00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3732  00050000
      *                                                                 00060000
      * MODIF DSA056 29/09/03 PROJET INTERNATIONALISATION                       
          02 COMM-GA31-APPLI REDEFINES COMM-GA00-APPLI.                 00070001
      *------------------------------ CODE FONCTION                     00080000
             03 COMM-GA31-FONCT          PIC X(3).                      00090001
      *------------------------------ NUMERO DE PAGE                    00100001
             03 NUMERO-DE-PAGE         PIC 9(3).                        00101001
      *------------------------------ CODE ARTICLE                      00103000
             03 COMM-GA31-NCODIC         PIC X(7).                      00110001
      *------------------------------ LIBELLE CODE ARTICLE              00120000
             03 COMM-GA31-LREF           PIC X(20).                     00130001
      *------------------------------ CODE FAMILLE                      00140100
             03 COMM-GA31-CFAM           PIC  X(5).                     00140201
      *------------------------------ LIBELLE FAMILLE                   00140500
             03 COMM-GA31-LFAM           PIC  X(20).                    00140601
      *------------------------------ CODE MARQUEE                      00201000
             03 COMM-GA31-CMARQ          PIC  X(5).                     00202001
      *------------------------------ LIBELLE MARQUEE                   00203000
             03 COMM-GA31-LMARQ          PIC  X(20).                    00204001
      *------------------------------ CODE ASSOR                        00205000
             03 COMM-GA31-CASSOR         PIC  X(5).                     00206001
      *------------------------------ LIBELLE ASSOR                     00207000
             03 COMM-GA31-LASSOR         PIC  X(20).                    00208001
      *------------------------------ CODE APPRO                        00209000
             03 COMM-GA31-CAPPRO         PIC  X(5).                     00209101
      *------------------------------ LIBELLE APPRO                     00209200
             03 COMM-GA31-LAPPRO         PIC  X(20).                    00209301
      *------------------------------ CODE EXPO                         00209400
             03 COMM-GA31-CEXPO          PIC  X(5).                     00209501
      *------------------------------ LIBELLE EXPO                      00209601
             03 COMM-GA31-LEXPO          PIC  X(20).                    00209701
      *------------------------------ DATE DE CREATION                  00209800
             03 COMM-GA31-DCRECC         PIC  X(8).                     00209901
      *------------------------------ DATE DE CREATION (SSAAMMJJ)       00210001
             03 COMM-GA31-DATE           PIC  X(8).                     00210101
      *------------------------------ DERNIERE CLE LUE                  00210201
             03 SAVE-CLE-FIN.                                           00210301
      *------------------------------ DERNIERE DATE CREATION LUE        00210201
                04 SAVE-CLE-FIN-DCREATION   PIC X(8).                   00210301
      *------------------------------ DERNIER CODIC LU                  00210201
                04 SAVE-CLE-FIN-NCODIC      PIC X(7).                   00210301
      *------------------------------ ARTICLE BAS DE L'ECRAN            00210401
             03 COMM-GA31-ARTICLE.                                      00210501
      *------------------------------ BAS DE L'ECRAN EGA31              00210601
                04 COMM-GA31-M156         OCCURS 11.                    00210701
      *------------------------------ CODE ARTICLE                      00210801
                   05 COMM-GA31-NCODIQ      PIC X(7).                   00210901
      *------------------------------ LIBELLE CODE ARTICLE              00211001
                   05 COMM-GA31-LREFCC     PIC X(20).                   00211101
      *------------------------------ CODE FAMILLE                      00211201
                   05 COMM-GA31-CFAMS       PIC  X(5).                  00211301
      *------------------------------ DONNEES RESUMES                   00211401
                   05 COMM-GA31-LRESUM      PIC  X(20).                 00211501
      *------------------------------ CODE MARQUEE                      00211601
                   05 COMM-GA31-CMARQS      PIC  X(5).                  00211701
      *------------------------------ CODE STATUT                       00211801
                   05 COMM-GA31-CSTACC     PIC  X(3).                   00211901
      *------------------------------ SENSIBILITE APPROVISIONNEMENT     00212001
                   05 COMM-GA31-WSAPPR      PIC  X(1).                  00212101
      *------------------------------ SENSIBILITE DE VENTE              00212201
                   05 COMM-GA31-WSVTEC      PIC  X(1).                  00212301
      *------------------------------ DATE DE PREMIERE REFERENCE        00212401
                   05 COMM-GA31-DRECCC      PIC  X(8).                  00212501
      *------------------------------ TABLE PAGINATION                  00241000
             03 TABLE-PAGINATION.                                       00250000
      *------------------------------ PAGINATION                        00241000
                04 SAVE-PAGINATION  OCCURS 140.                         00250000
      *------------------------------ CLE PAGINATION                    00241000
                   05 SAVE-CLE-PAGINATION.                              00250000
      *------------------------------                                   00241000
                     06 SAVE-CLE-DCREATION  PIC X(8).                   00250000
      *------------------------------                                   00241000
                     06 SAVE-CLE-NCODIC     PIC X(7).                   00250000
             03 COMM-GA31-INTER.                                                
                04 COMM-CODLANG             PIC X(2).                           
                04 COMM-CODPIC              PIC X(2).                           
      *------------------------------ FILLER                            00271001
      *      03 COMM-GA31-FILLER            PIC X(0663).                00272001
             03 COMM-GA31-FILLER            PIC X(0659).                        
      *------------------------------ STARTCODE                         00272002
             03 COMM-GA31-STARTCODE         PIC X(2).                   00272003
      ***************************************************************** 00280000
                                                                                
