      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRX515 AU 13/12/2004  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,03,PD,A,                          *        
      *                           23,20,BI,A,                          *        
      *                           43,20,BI,A,                          *        
      *                           63,20,BI,A,                          *        
      *                           83,20,BI,A,                          *        
      *                           03,02,BI,A,                          *        
      *                           05,05,PD,A,                          *        
      *                           10,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRX515.                                                        
            05 NOMETAT-IRX515           PIC X(6) VALUE 'IRX515'.                
            05 RUPTURES-IRX515.                                                 
           10 IRX515-NSOCIETE           PIC X(03).                      007  003
           10 IRX515-CHEFPROD           PIC X(05).                      010  005
           10 IRX515-CMARQ              PIC X(05).                      015  005
           10 IRX515-WSEQFAM            PIC S9(05)      COMP-3.         020  003
           10 IRX515-LVMARKET1          PIC X(20).                      023  020
           10 IRX515-LVMARKET2          PIC X(20).                      043  020
           10 IRX515-LVMARKET3          PIC X(20).                      063  020
           10 IRX515-LREFFOURN          PIC X(20).                      083  020
           10 IRX515-NZONPRIX           PIC X(02).                      103  002
           10 IRX515-PVCONC             PIC S9(07)V9(2) COMP-3.         105  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRX515-SEQUENCE           PIC S9(04) COMP.                110  002
      *--                                                                       
           10 IRX515-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRX515.                                                   
           10 IRX515-CFAM               PIC X(05).                      112  005
           10 IRX515-COLIS              PIC X(30).                      117  030
           10 IRX515-CORIG              PIC X(01).                      147  001
           10 IRX515-DRELEVE            PIC X(04).                      148  004
           10 IRX515-ECART              PIC X(01).                      152  001
           10 IRX515-EDITCODE           PIC X(01).                      153  001
           10 IRX515-EDITD8             PIC X(01).                      154  001
           10 IRX515-FLAGEDITION        PIC X(01).                      155  001
           10 IRX515-FLAGPEX            PIC X(01).                      156  001
           10 IRX515-FOURNREF           PIC X(20).                      157  020
           10 IRX515-HISTORIQUE         PIC X(01).                      177  001
           10 IRX515-LCHEFPROD          PIC X(20).                      178  020
           10 IRX515-LCOMMENT           PIC X(20).                      198  020
           10 IRX515-LCONCPE            PIC X(20).                      218  020
           10 IRX515-LEMPCONC           PIC X(10).                      238  010
           10 IRX515-LENSCONC           PIC X(15).                      248  015
           10 IRX515-LIBELLE1           PIC X(26).                      263  026
           10 IRX515-LIBELLE2           PIC X(03).                      289  003
           10 IRX515-LIBELLE3           PIC X(05).                      292  005
           10 IRX515-LMARQ              PIC X(20).                      297  020
           10 IRX515-LSTATCOMP          PIC X(03).                      317  003
           10 IRX515-MARQREF            PIC X(05).                      320  005
           10 IRX515-NCODIC             PIC X(07).                      325  007
           10 IRX515-NCONCPE            PIC X(04).                      332  004
           10 IRX515-NMAGPE             PIC X(03).                      336  003
           10 IRX515-NONCODIC           PIC X(11).                      339  011
           10 IRX515-PEXPTTC            PIC S9(07)V9(2) COMP-3.         350  005
           10 IRX515-PVMAG              PIC S9(07)V9(2) COMP-3.         355  005
           10 IRX515-PVREF              PIC S9(07)V9(2) COMP-3.         360  005
           10 IRX515-DATEAU             PIC X(08).                      365  008
           10 IRX515-DATEDU             PIC X(08).                      373  008
           10 IRX515-DFINEFFETPE        PIC X(08).                      381  008
            05 FILLER                      PIC X(124).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IRX515-LONG           PIC S9(4)   COMP  VALUE +388.           
      *                                                                         
      *--                                                                       
        01  DSECT-IRX515-LONG           PIC S9(4) COMP-5  VALUE +388.           
                                                                                
      *}                                                                        
