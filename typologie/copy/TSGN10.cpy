      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : MISE A JOUR DU REFERENTIEL NATIONAL    (GN10)          *         
      *        PAGINATION DES OFFRES ACTIVES                          *         
      *****************************************************************         
      *                                                               *         
       01  TS-GN10.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GN10-LONG                 PIC S9(03) COMP-3                    
                                           VALUE +65.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-GN10-DONNEES.                                                  
      *----------------------------------  CODE ACTION OA                       
              03 TS-GN10-ACT               PIC X.                               
      *----------------------------------  INDICATEUR OFFRE ACTIVE              
              03 TS-GN10-OA                PIC X.                               
      *----------------------------------  DATE DEBUT EFFET OFFRE ACIVE         
              03 TS-GN10-DEBOA             PIC X(8).                            
      *----------------------------------  DEBOA AVANT MODIF                    
              03 TS-GN10-DEBOA-AV          PIC X(8).                            
      *----------------------------------  CODIC DE SUBSTITUTION                
              03 TS-GN10-CODSUB            PIC X(7).                            
      *----------------------------------  BOOLEEN DE MODIF                     
              03 TS-GN10-MODIF             PIC X(01).                           
                                                                                
