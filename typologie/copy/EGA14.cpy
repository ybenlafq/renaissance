      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA14   EGA14                                              00000020
      ***************************************************************** 00000030
       01   EGA14I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
           02 M137I OCCURS   11 TIMES .                                 00000180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNFAMSL      COMP PIC S9(4).                            00000190
      *--                                                                       
             03 MNFAMSL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNFAMSF      PIC X.                                     00000200
             03 FILLER  PIC X(4).                                       00000210
             03 MNFAMSI      PIC X(5).                                  00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLFAMSL      COMP PIC S9(4).                            00000230
      *--                                                                       
             03 MLFAMSL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLFAMSF      PIC X.                                     00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MLFAMSI      PIC X(20).                                 00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MZONCMDI  PIC X(15).                                      00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000310
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000320
           02 FILLER    PIC X(4).                                       00000330
           02 MLIBERRI  PIC X(58).                                      00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000350
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000360
           02 FILLER    PIC X(4).                                       00000370
           02 MCODTRAI  PIC X(4).                                       00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000390
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000400
           02 FILLER    PIC X(4).                                       00000410
           02 MCICSI    PIC X(5).                                       00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000430
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MNETNAMI  PIC X(8).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MSCREENI  PIC X(4).                                       00000500
      ***************************************************************** 00000510
      * SDF: EGA14   EGA14                                              00000520
      ***************************************************************** 00000530
       01   EGA14O REDEFINES EGA14I.                                    00000540
           02 FILLER    PIC X(12).                                      00000550
           02 FILLER    PIC X(2).                                       00000560
           02 MDATJOUA  PIC X.                                          00000570
           02 MDATJOUC  PIC X.                                          00000580
           02 MDATJOUP  PIC X.                                          00000590
           02 MDATJOUH  PIC X.                                          00000600
           02 MDATJOUV  PIC X.                                          00000610
           02 MDATJOUO  PIC X(10).                                      00000620
           02 FILLER    PIC X(2).                                       00000630
           02 MTIMJOUA  PIC X.                                          00000640
           02 MTIMJOUC  PIC X.                                          00000650
           02 MTIMJOUP  PIC X.                                          00000660
           02 MTIMJOUH  PIC X.                                          00000670
           02 MTIMJOUV  PIC X.                                          00000680
           02 MTIMJOUO  PIC X(5).                                       00000690
           02 FILLER    PIC X(2).                                       00000700
           02 MPAGEA    PIC X.                                          00000710
           02 MPAGEC    PIC X.                                          00000720
           02 MPAGEP    PIC X.                                          00000730
           02 MPAGEH    PIC X.                                          00000740
           02 MPAGEV    PIC X.                                          00000750
           02 MPAGEO    PIC X(3).                                       00000760
           02 M137O OCCURS   11 TIMES .                                 00000770
             03 FILLER       PIC X(2).                                  00000780
             03 MNFAMSA      PIC X.                                     00000790
             03 MNFAMSC PIC X.                                          00000800
             03 MNFAMSP PIC X.                                          00000810
             03 MNFAMSH PIC X.                                          00000820
             03 MNFAMSV PIC X.                                          00000830
             03 MNFAMSO      PIC X(5).                                  00000840
             03 FILLER       PIC X(2).                                  00000850
             03 MLFAMSA      PIC X.                                     00000860
             03 MLFAMSC PIC X.                                          00000870
             03 MLFAMSP PIC X.                                          00000880
             03 MLFAMSH PIC X.                                          00000890
             03 MLFAMSV PIC X.                                          00000900
             03 MLFAMSO      PIC X(20).                                 00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MZONCMDA  PIC X.                                          00000930
           02 MZONCMDC  PIC X.                                          00000940
           02 MZONCMDP  PIC X.                                          00000950
           02 MZONCMDH  PIC X.                                          00000960
           02 MZONCMDV  PIC X.                                          00000970
           02 MZONCMDO  PIC X(15).                                      00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MLIBERRA  PIC X.                                          00001000
           02 MLIBERRC  PIC X.                                          00001010
           02 MLIBERRP  PIC X.                                          00001020
           02 MLIBERRH  PIC X.                                          00001030
           02 MLIBERRV  PIC X.                                          00001040
           02 MLIBERRO  PIC X(58).                                      00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MCODTRAA  PIC X.                                          00001070
           02 MCODTRAC  PIC X.                                          00001080
           02 MCODTRAP  PIC X.                                          00001090
           02 MCODTRAH  PIC X.                                          00001100
           02 MCODTRAV  PIC X.                                          00001110
           02 MCODTRAO  PIC X(4).                                       00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MCICSA    PIC X.                                          00001140
           02 MCICSC    PIC X.                                          00001150
           02 MCICSP    PIC X.                                          00001160
           02 MCICSH    PIC X.                                          00001170
           02 MCICSV    PIC X.                                          00001180
           02 MCICSO    PIC X(5).                                       00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MNETNAMA  PIC X.                                          00001210
           02 MNETNAMC  PIC X.                                          00001220
           02 MNETNAMP  PIC X.                                          00001230
           02 MNETNAMH  PIC X.                                          00001240
           02 MNETNAMV  PIC X.                                          00001250
           02 MNETNAMO  PIC X(8).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MSCREENA  PIC X.                                          00001280
           02 MSCREENC  PIC X.                                          00001290
           02 MSCREENP  PIC X.                                          00001300
           02 MSCREENH  PIC X.                                          00001310
           02 MSCREENV  PIC X.                                          00001320
           02 MSCREENO  PIC X(4).                                       00001330
                                                                                
