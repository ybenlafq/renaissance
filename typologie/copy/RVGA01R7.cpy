      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CRORG ORGANISME DE CREDIT              *        
      *----------------------------------------------------------------*        
       01  RVGA01R7.                                                            
           05  CRORG-CTABLEG2    PIC X(15).                                     
           05  CRORG-CTABLEG2-REDEF REDEFINES CRORG-CTABLEG2.                   
               10  CRORG-CCRED           PIC X(05).                             
           05  CRORG-WTABLEG     PIC X(80).                                     
           05  CRORG-WTABLEG-REDEF  REDEFINES CRORG-WTABLEG.                    
               10  CRORG-WACTIF          PIC X(01).                             
               10  CRORG-LCRED           PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01R7-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CRORG-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CRORG-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CRORG-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CRORG-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
