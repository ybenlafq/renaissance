      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRX310 AU 06/12/2004  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,15,BI,A,                          *        
      *                           28,15,BI,A,                          *        
      *                           43,04,BI,A,                          *        
      *                           47,03,PD,A,                          *        
      *                           50,05,BI,A,                          *        
      *                           55,20,BI,A,                          *        
      *                           75,07,BI,A,                          *        
      *                           82,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRX310.                                                        
            05 NOMETAT-IRX310           PIC X(6) VALUE 'IRX310'.                
            05 RUPTURES-IRX310.                                                 
           10 IRX310-NSOCIETE           PIC X(03).                      007  003
           10 IRX310-NMAG               PIC X(03).                      010  003
           10 IRX310-LENSCONC           PIC X(15).                      013  015
           10 IRX310-LEMPCONC           PIC X(15).                      028  015
           10 IRX310-NCONC              PIC X(04).                      043  004
           10 IRX310-WSEQFAM            PIC S9(05)      COMP-3.         047  003
           10 IRX310-CMARQ              PIC X(05).                      050  005
           10 IRX310-LREFFOURN          PIC X(20).                      055  020
           10 IRX310-NCODIC             PIC X(07).                      075  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRX310-SEQUENCE           PIC S9(04) COMP.                082  002
      *--                                                                       
           10 IRX310-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRX310.                                                   
           10 IRX310-AU                 PIC X(02).                      084  002
           10 IRX310-CFAM               PIC X(05).                      086  005
           10 IRX310-CORIG              PIC X(01).                      091  001
           10 IRX310-DATEAU             PIC X(10).                      092  010
           10 IRX310-DATEDU             PIC X(10).                      102  010
           10 IRX310-DDEBEFFET          PIC X(10).                      112  010
           10 IRX310-DFINEFFET          PIC X(10).                      122  010
           10 IRX310-DRELEVEJJMM        PIC X(04).                      132  004
           10 IRX310-LCOMMENT           PIC X(19).                      136  019
           10 IRX310-LCONCPE            PIC X(20).                      155  020
           10 IRX310-LMAG               PIC X(20).                      175  020
           10 IRX310-LSTATCOMP          PIC X(03).                      195  003
           10 IRX310-NCONCEXP           PIC X(04).                      198  004
           10 IRX310-NZONPRIX           PIC X(02).                      202  002
           10 IRX310-TYPALIGN           PIC X(01).                      204  001
           10 IRX310-WEDIT              PIC X(01).                      205  001
           10 IRX310-PEXPTTC            PIC S9(07)V9(2) COMP-3.         206  005
           10 IRX310-PRELEVE            PIC S9(07)V9(2) COMP-3.         211  005
           10 IRX310-PVMAG              PIC S9(07)V9(2) COMP-3.         216  005
            05 FILLER                      PIC X(292).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IRX310-LONG           PIC S9(4)   COMP  VALUE +220.           
      *                                                                         
      *--                                                                       
        01  DSECT-IRX310-LONG           PIC S9(4) COMP-5  VALUE +220.           
                                                                                
      *}                                                                        
