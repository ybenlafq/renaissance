      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVBS2600                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVBS2600                 00000060
      *   CLE UNIQUE : CTYPENT A NSEQENT                                00000070
      *---------------------------------------------------------        00000080
      *                                                                 00000090
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBS2600.                                                    00000100
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBS2600.                                                            
      *}                                                                        
           10 BS26-CTYPENT         PIC X(2).                            00000110
           10 BS26-NENTITE         PIC X(7).                            00000120
           10 BS26-NSEQDOS         PIC S9(3)V USAGE COMP-3.             00000130
           10 BS26-NSEQENT         PIC S9(3)V USAGE COMP-3.             00000140
           10 BS26-DSYST           PIC S9(13)V USAGE COMP-3.            00000150
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00000160
      *---------------------------------------------------------        00000170
      *   LISTE DES FLAGS DE LA TABLE RVBS2600                          00000180
      *---------------------------------------------------------        00000190
      *                                                                 00000200
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBS2600-FLAGS.                                              00000210
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBS2600-FLAGS.                                                      
      *}                                                                        
           10 BS26-CTYPENT-F       PIC X(2).                            00000220
           10 BS26-NENTITE-F       PIC X(7).                            00000230
           10 BS26-NSEQDOS-F       PIC S9(3)V USAGE COMP-3.             00000240
           10 BS26-NSEQENT-F       PIC S9(3)V USAGE COMP-3.             00000250
           10 BS26-DSYST-F         PIC S9(13)V USAGE COMP-3.            00000260
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
