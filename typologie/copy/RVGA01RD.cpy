      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PRTYP TYPE DE PRESTATION               *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01RD.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01RD.                                                            
      *}                                                                        
           05  PRTYP-CTABLEG2    PIC X(15).                                     
           05  PRTYP-CTABLEG2-REDEF REDEFINES PRTYP-CTABLEG2.                   
               10  PRTYP-CTPREST         PIC X(05).                             
           05  PRTYP-WTABLEG     PIC X(80).                                     
           05  PRTYP-WTABLEG-REDEF  REDEFINES PRTYP-WTABLEG.                    
               10  PRTYP-WACTIF          PIC X(01).                             
               10  PRTYP-LTPREST         PIC X(20).                             
               10  PRTYP-QMAJEUR         PIC X(04).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  PRTYP-QMAJEUR-N      REDEFINES PRTYP-QMAJEUR                 
                                         PIC 9(04).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  PRTYP-QOPTION         PIC X(04).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  PRTYP-QOPTION-N      REDEFINES PRTYP-QOPTION                 
                                         PIC 9(04).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01RD-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01RD-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRTYP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PRTYP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRTYP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PRTYP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
