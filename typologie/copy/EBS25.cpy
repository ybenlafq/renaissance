      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESE03   ESE03                                              00000020
      ***************************************************************** 00000030
       01   EBS25I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NSOCL     COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 NSOCL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 NSOCF     PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 NSOCI     PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NMAGL     COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 NMAGL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 NMAGF     PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 NMAGI     PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LMAGL     COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 LMAGL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 LMAGF     PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 LMAGI     PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPENT1L     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCTYPENT1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTYPENT1F     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCTYPENT1I     PIC X.                                     00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTITE1L     COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MNENTITE1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNENTITE1F     PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNENTITE1I     PIC X(7).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTITE1L     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLENTITE1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLENTITE1F     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLENTITE1I     PIC X(20).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MAFFICHAGL     COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MAFFICHAGL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MAFFICHAGF     PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MAFFICHAGI     PIC X.                                     00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CLIENL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 CLIENL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 CLIENF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 CLIENI    PIC X(2).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQ1L  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCMARQ1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMARQ1F  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCMARQ1I  PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MPAGEMAXI      PIC X(3).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAM1L   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCFAM1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAM1F   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCFAM1I   PIC X(5).                                       00000570
           02 MTABLISTI OCCURS   12 TIMES .                             00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPENTL    COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MCTYPENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCTYPENTF    PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCTYPENTI    PIC X(3).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTIFL      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MACTIFL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MACTIFF      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MACTIFI      PIC X.                                     00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 NENTITEL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 NENTITEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 NENTITEF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 NENTITEI     PIC X(7).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 LENTITEL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 LENTITEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 LENTITEF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 LENTITEI     PIC X(20).                                 00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 CFAML   COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 CFAML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 CFAMF   PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 CFAMI   PIC X(27).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 NMBRL   COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 NMBRL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 NMBRF   PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 NMBRI   PIC X(6).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 NPRIXL  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 NPRIXL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 NPRIXF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 NPRIXI  PIC X(9).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MZONCMDI  PIC X(15).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MLIBERRI  PIC X(58).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCODTRAI  PIC X(4).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCICSI    PIC X(5).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MNETNAMI  PIC X(8).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MSCREENI  PIC X(4).                                       00001100
      ***************************************************************** 00001110
      * SDF: ESE03   ESE03                                              00001120
      ***************************************************************** 00001130
       01   EBS25O REDEFINES EBS25I.                                    00001140
           02 FILLER    PIC X(12).                                      00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MDATJOUA  PIC X.                                          00001170
           02 MDATJOUC  PIC X.                                          00001180
           02 MDATJOUP  PIC X.                                          00001190
           02 MDATJOUH  PIC X.                                          00001200
           02 MDATJOUV  PIC X.                                          00001210
           02 MDATJOUO  PIC X(10).                                      00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MTIMJOUA  PIC X.                                          00001240
           02 MTIMJOUC  PIC X.                                          00001250
           02 MTIMJOUP  PIC X.                                          00001260
           02 MTIMJOUH  PIC X.                                          00001270
           02 MTIMJOUV  PIC X.                                          00001280
           02 MTIMJOUO  PIC X(5).                                       00001290
           02 FILLER    PIC X(2).                                       00001300
           02 NSOCA     PIC X.                                          00001310
           02 NSOCC     PIC X.                                          00001320
           02 NSOCP     PIC X.                                          00001330
           02 NSOCH     PIC X.                                          00001340
           02 NSOCV     PIC X.                                          00001350
           02 NSOCO     PIC X(3).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 NMAGA     PIC X.                                          00001380
           02 NMAGC     PIC X.                                          00001390
           02 NMAGP     PIC X.                                          00001400
           02 NMAGH     PIC X.                                          00001410
           02 NMAGV     PIC X.                                          00001420
           02 NMAGO     PIC X(3).                                       00001430
           02 FILLER    PIC X(2).                                       00001440
           02 LMAGA     PIC X.                                          00001450
           02 LMAGC     PIC X.                                          00001460
           02 LMAGP     PIC X.                                          00001470
           02 LMAGH     PIC X.                                          00001480
           02 LMAGV     PIC X.                                          00001490
           02 LMAGO     PIC X(20).                                      00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MCTYPENT1A     PIC X.                                     00001520
           02 MCTYPENT1C     PIC X.                                     00001530
           02 MCTYPENT1P     PIC X.                                     00001540
           02 MCTYPENT1H     PIC X.                                     00001550
           02 MCTYPENT1V     PIC X.                                     00001560
           02 MCTYPENT1O     PIC X.                                     00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MNENTITE1A     PIC X.                                     00001590
           02 MNENTITE1C     PIC X.                                     00001600
           02 MNENTITE1P     PIC X.                                     00001610
           02 MNENTITE1H     PIC X.                                     00001620
           02 MNENTITE1V     PIC X.                                     00001630
           02 MNENTITE1O     PIC X(7).                                  00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MLENTITE1A     PIC X.                                     00001660
           02 MLENTITE1C     PIC X.                                     00001670
           02 MLENTITE1P     PIC X.                                     00001680
           02 MLENTITE1H     PIC X.                                     00001690
           02 MLENTITE1V     PIC X.                                     00001700
           02 MLENTITE1O     PIC X(20).                                 00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MAFFICHAGA     PIC X.                                     00001730
           02 MAFFICHAGC     PIC X.                                     00001740
           02 MAFFICHAGP     PIC X.                                     00001750
           02 MAFFICHAGH     PIC X.                                     00001760
           02 MAFFICHAGV     PIC X.                                     00001770
           02 MAFFICHAGO     PIC X.                                     00001780
           02 FILLER    PIC X(2).                                       00001790
           02 CLIENA    PIC X.                                          00001800
           02 CLIENC    PIC X.                                          00001810
           02 CLIENP    PIC X.                                          00001820
           02 CLIENH    PIC X.                                          00001830
           02 CLIENV    PIC X.                                          00001840
           02 CLIENO    PIC X(2).                                       00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MCMARQ1A  PIC X.                                          00001870
           02 MCMARQ1C  PIC X.                                          00001880
           02 MCMARQ1P  PIC X.                                          00001890
           02 MCMARQ1H  PIC X.                                          00001900
           02 MCMARQ1V  PIC X.                                          00001910
           02 MCMARQ1O  PIC X(5).                                       00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MPAGEMAXA      PIC X.                                     00001940
           02 MPAGEMAXC PIC X.                                          00001950
           02 MPAGEMAXP PIC X.                                          00001960
           02 MPAGEMAXH PIC X.                                          00001970
           02 MPAGEMAXV PIC X.                                          00001980
           02 MPAGEMAXO      PIC X(3).                                  00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MCFAM1A   PIC X.                                          00002010
           02 MCFAM1C   PIC X.                                          00002020
           02 MCFAM1P   PIC X.                                          00002030
           02 MCFAM1H   PIC X.                                          00002040
           02 MCFAM1V   PIC X.                                          00002050
           02 MCFAM1O   PIC X(5).                                       00002060
           02 MTABLISTO OCCURS   12 TIMES .                             00002070
             03 FILLER       PIC X(2).                                  00002080
             03 MCTYPENTA    PIC X.                                     00002090
             03 MCTYPENTC    PIC X.                                     00002100
             03 MCTYPENTP    PIC X.                                     00002110
             03 MCTYPENTH    PIC X.                                     00002120
             03 MCTYPENTV    PIC X.                                     00002130
             03 MCTYPENTO    PIC X(3).                                  00002140
             03 FILLER       PIC X(2).                                  00002150
             03 MACTIFA      PIC X.                                     00002160
             03 MACTIFC PIC X.                                          00002170
             03 MACTIFP PIC X.                                          00002180
             03 MACTIFH PIC X.                                          00002190
             03 MACTIFV PIC X.                                          00002200
             03 MACTIFO      PIC X.                                     00002210
             03 FILLER       PIC X(2).                                  00002220
             03 NENTITEA     PIC X.                                     00002230
             03 NENTITEC     PIC X.                                     00002240
             03 NENTITEP     PIC X.                                     00002250
             03 NENTITEH     PIC X.                                     00002260
             03 NENTITEV     PIC X.                                     00002270
             03 NENTITEO     PIC X(7).                                  00002280
             03 FILLER       PIC X(2).                                  00002290
             03 LENTITEA     PIC X.                                     00002300
             03 LENTITEC     PIC X.                                     00002310
             03 LENTITEP     PIC X.                                     00002320
             03 LENTITEH     PIC X.                                     00002330
             03 LENTITEV     PIC X.                                     00002340
             03 LENTITEO     PIC X(20).                                 00002350
             03 FILLER       PIC X(2).                                  00002360
             03 CFAMA   PIC X.                                          00002370
             03 CFAMC   PIC X.                                          00002380
             03 CFAMP   PIC X.                                          00002390
             03 CFAMH   PIC X.                                          00002400
             03 CFAMV   PIC X.                                          00002410
             03 CFAMO   PIC X(27).                                      00002420
             03 FILLER       PIC X(2).                                  00002430
             03 NMBRA   PIC X.                                          00002440
             03 NMBRC   PIC X.                                          00002450
             03 NMBRP   PIC X.                                          00002460
             03 NMBRH   PIC X.                                          00002470
             03 NMBRV   PIC X.                                          00002480
             03 NMBRO   PIC X(6).                                       00002490
             03 FILLER       PIC X(2).                                  00002500
             03 NPRIXA  PIC X.                                          00002510
             03 NPRIXC  PIC X.                                          00002520
             03 NPRIXP  PIC X.                                          00002530
             03 NPRIXH  PIC X.                                          00002540
             03 NPRIXV  PIC X.                                          00002550
             03 NPRIXO  PIC X(9).                                       00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MZONCMDA  PIC X.                                          00002580
           02 MZONCMDC  PIC X.                                          00002590
           02 MZONCMDP  PIC X.                                          00002600
           02 MZONCMDH  PIC X.                                          00002610
           02 MZONCMDV  PIC X.                                          00002620
           02 MZONCMDO  PIC X(15).                                      00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MLIBERRA  PIC X.                                          00002650
           02 MLIBERRC  PIC X.                                          00002660
           02 MLIBERRP  PIC X.                                          00002670
           02 MLIBERRH  PIC X.                                          00002680
           02 MLIBERRV  PIC X.                                          00002690
           02 MLIBERRO  PIC X(58).                                      00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MCODTRAA  PIC X.                                          00002720
           02 MCODTRAC  PIC X.                                          00002730
           02 MCODTRAP  PIC X.                                          00002740
           02 MCODTRAH  PIC X.                                          00002750
           02 MCODTRAV  PIC X.                                          00002760
           02 MCODTRAO  PIC X(4).                                       00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MCICSA    PIC X.                                          00002790
           02 MCICSC    PIC X.                                          00002800
           02 MCICSP    PIC X.                                          00002810
           02 MCICSH    PIC X.                                          00002820
           02 MCICSV    PIC X.                                          00002830
           02 MCICSO    PIC X(5).                                       00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MNETNAMA  PIC X.                                          00002860
           02 MNETNAMC  PIC X.                                          00002870
           02 MNETNAMP  PIC X.                                          00002880
           02 MNETNAMH  PIC X.                                          00002890
           02 MNETNAMV  PIC X.                                          00002900
           02 MNETNAMO  PIC X(8).                                       00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MSCREENA  PIC X.                                          00002930
           02 MSCREENC  PIC X.                                          00002940
           02 MSCREENP  PIC X.                                          00002950
           02 MSCREENH  PIC X.                                          00002960
           02 MSCREENV  PIC X.                                          00002970
           02 MSCREENO  PIC X(4).                                       00002980
                                                                                
