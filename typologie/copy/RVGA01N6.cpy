      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE POLP5 DEFINITION POLICE PCL5           *        
      *----------------------------------------------------------------*        
       01  RVGA01N6.                                                            
           05  POLP5-CTABLEG2    PIC X(15).                                     
           05  POLP5-CTABLEG2-REDEF REDEFINES POLP5-CTABLEG2.                   
               10  POLP5-NPOLPR          PIC X(03).                             
               10  POLP5-NPOLPR-N       REDEFINES POLP5-NPOLPR                  
                                         PIC 9(03).                             
           05  POLP5-WTABLEG     PIC X(80).                                     
           05  POLP5-WTABLEG-REDEF  REDEFINES POLP5-WTABLEG.                    
               10  POLP5-P               PIC X(01).                             
               10  POLP5-P-N            REDEFINES POLP5-P                       
                                         PIC 9(01).                             
               10  POLP5-SIZE            PIC X(04).                             
               10  POLP5-SIZE-N         REDEFINES POLP5-SIZE                    
                                         PIC 9(04).                             
               10  POLP5-STYLE           PIC X(01).                             
               10  POLP5-STYLE-N        REDEFINES POLP5-STYLE                   
                                         PIC 9(01).                             
               10  POLP5-BOLD            PIC X(01).                             
               10  POLP5-BOLD-N         REDEFINES POLP5-BOLD                    
                                         PIC 9(01).                             
               10  POLP5-TYPO            PIC X(05).                             
               10  POLP5-TYPO-N         REDEFINES POLP5-TYPO                    
                                         PIC 9(05).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01N6-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  POLP5-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  POLP5-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  POLP5-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  POLP5-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
