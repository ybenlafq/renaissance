      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPR002 AU 08/11/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,07,BI,A,                          *        
      *                           24,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPR002.                                                        
            05 NOMETAT-IPR002           PIC X(6) VALUE 'IPR002'.                
            05 RUPTURES-IPR002.                                                 
           10 IPR002-CFAM               PIC X(05).                      007  005
           10 IPR002-CMARQ              PIC X(05).                      012  005
           10 IPR002-NCODIC             PIC X(07).                      017  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPR002-SEQUENCE           PIC S9(04) COMP.                024  002
      *--                                                                       
           10 IPR002-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPR002.                                                   
           10 IPR002-LREF               PIC X(20).                      026  020
           10 IPR002-NZONPRIX           PIC X(02).                      046  002
           10 IPR002-PCOMM              PIC S9(04)V9(2) COMP-3.         048  004
           10 IPR002-PSTDTTC            PIC S9(05)V9(2) COMP-3.         052  004
            05 FILLER                      PIC X(457).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPR002-LONG           PIC S9(4)   COMP  VALUE +055.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPR002-LONG           PIC S9(4) COMP-5  VALUE +055.           
                                                                                
      *}                                                                        
