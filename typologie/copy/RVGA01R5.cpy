      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FTDET PERIODES DE DETAXATION           *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01R5.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01R5.                                                            
      *}                                                                        
           05  FTDET-CTABLEG2    PIC X(15).                                     
           05  FTDET-CTABLEG2-REDEF REDEFINES FTDET-CTABLEG2.                   
               10  FTDET-NSOC            PIC X(05).                             
               10  FTDET-NEXER           PIC X(04).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  FTDET-NEXER-N        REDEFINES FTDET-NEXER                   
                                         PIC 9(04).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  FTDET-NPER            PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  FTDET-NPER-N         REDEFINES FTDET-NPER                    
                                         PIC 9(02).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  FTDET-WTABLEG     PIC X(80).                                     
           05  FTDET-WTABLEG-REDEF  REDEFINES FTDET-WTABLEG.                    
               10  FTDET-WACTIF          PIC X(01).                             
               10  FTDET-WOPTION         PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01R5-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01R5-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FTDET-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FTDET-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FTDET-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FTDET-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
