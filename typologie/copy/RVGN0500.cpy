      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      **********************************************************        00030000
      *   COPY DE LA TABLE RVGN0500                                     00040002
      **********************************************************        00050000
      *   LISTE DES HOST VARIABLES DE LA VUE RVGN0500                   00060002
      **********************************************************        00070000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGN0500.                                                    00080002
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGN0500.                                                            
      *}                                                                        
           05  GN05-NCONCN     PIC X(4).                                00090002
           05  GN05-NCONCE     PIC X(4).                                00100002
           05  GN05-DSYST PIC S9(13) COMP-3.                            00160002
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************        00170000
      *   LISTE DES FLAGS DE LA TABLE RVGN0500                          00180002
      **********************************************************        00190000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGN0500-FLAGS.                                              00200002
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGN0500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN05-NCONCN-F                                            00210002
      *        PIC S9(4) COMP.                                          00211000
      *--                                                                       
           05  GN05-NCONCN-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN05-NCONCE-F                                            00220002
      *        PIC S9(4) COMP.                                          00221000
      *--                                                                       
           05  GN05-NCONCE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN05-DSYST-F                                             00280002
      *        PIC S9(4) COMP.                                          00290000
      *                                                                         
      *--                                                                       
           05  GN05-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
