      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : GESTION DES ALIGNEMENTS NATIONAUX      (GN72)          *         
      *****************************************************************         
      *                                                               *         
       01  TS-GN72.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GN72-LONG                 PIC S9(03) COMP-3                    
                                           VALUE +55.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-GN72-DONNEES.                                                  
      *----------------------------------  CODE SOCIETE                         
              03 TS-GN72-NSOC              PIC X(03).                           
      *----------------------------------  LIGNE DE DONNEES                     
              03 TS-GN72-MLIGNE            PIC X(39).                           
      *----------------------------------  LIBELLE ENSEIGNE CONCURRENT          
              03 TS-GN72-LENSCONC          PIC X(15).                           
      *----------------------------------  CODE APPROVISIONNEMENT               
              03 TS-GN72-CAPPRO            PIC X(03).                           
      *----------------------------------  CODE EXPO                            
              03 TS-GN72-CEXPO             PIC X(02).                           
      *----------------------------------  STATUT COMPACT                       
              03 TS-GN72-LSTATCOMP         PIC X(03).                           
                                                                                
