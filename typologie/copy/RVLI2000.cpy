      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVLI2000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLI2000                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVLI2000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVLI2000.                                                            
      *}                                                                        
           02  LI20-NSOCO                                                       
               PIC X(0003).                                                     
           02  LI20-NLIEUO                                                      
               PIC X(0003).                                                     
           02  LI20-SSLIEUO                                                     
               PIC X(0005).                                                     
           02  LI20-NSOCD                                                       
               PIC X(0003).                                                     
           02  LI20-NLIEUD                                                      
               PIC X(0003).                                                     
           02  LI20-SSLIEUD                                                     
               PIC X(0005).                                                     
           02  LI20-CPARAM                                                      
               PIC X(0005).                                                     
           02  LI20-LVPARAM                                                     
               PIC X(0020).                                                     
           02  LI20-LVPARAM-REDEF  REDEFINES LI20-LVPARAM.                      
                   05  LI20-QDELAI          PIC X(01).                          
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
                   05  LI20-QDELAI-N       REDEFINES LI20-QDELAI                
                                            PIC 9(01).                          
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                   05  LI20-LVPARAM-X       PIC X(19).                          
           02  LI20-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVLI2000                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVLI2000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVLI2000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LI20-NSOCO-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LI20-NSOCO-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LI20-NLIEUO-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LI20-NLIEUO-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LI20-SSLIEUO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LI20-SSLIEUO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LI20-NSOCD-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LI20-NSOCD-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LI20-NLIEUD-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LI20-NLIEUD-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LI20-SSLIEUD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LI20-SSLIEUD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LI20-CPARAM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LI20-CPARAM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LI20-LVPARAM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LI20-LVPARAM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LI20-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  LI20-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
