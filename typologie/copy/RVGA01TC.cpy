      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SGSEC PAIE SIGAGIP/CS: CODES SECTION   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01TC.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01TC.                                                            
      *}                                                                        
           05  SGSEC-CTABLEG2    PIC X(15).                                     
           05  SGSEC-CTABLEG2-REDEF REDEFINES SGSEC-CTABLEG2.                   
               10  SGSEC-CSECTION        PIC X(03).                             
           05  SGSEC-WTABLEG     PIC X(80).                                     
           05  SGSEC-WTABLEG-REDEF  REDEFINES SGSEC-WTABLEG.                    
               10  SGSEC-WACTIF          PIC X(01).                             
               10  SGSEC-LCODE           PIC X(25).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01TC-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01TC-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SGSEC-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SGSEC-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SGSEC-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SGSEC-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
