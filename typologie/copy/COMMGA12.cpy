      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
           02 COMM-GA12-APPLI REDEFINES COMM-GA00-APPLI.                        
              05 COMM-GA12-FONCT                PIC  X(3).                      
              05 COMM-GA12-CETAT                PIC  X(10).                     
              05 COMM-GA12-NO-PAGE              PIC  S9(3)  COMP-3.             
              05 COMM-GA12-NO-PAGE-MAX          PIC  S9(3)  COMP-3.             
              05 COMM-GA12-WFLAGMGI             PIC  X(01).                     
              05 COMM-GA12-WFLAGGPE             PIC  X(01).                     
              05 COMM-GA12-FILLER               PIC  X(3705).                   
      *       05 COMM-GA12-FILLER               PIC  X(3706).                   
                                                                                
