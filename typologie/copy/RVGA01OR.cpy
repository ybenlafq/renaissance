      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HCSTA STATUTS A EDITER POUR HS         *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01OR.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01OR.                                                            
      *}                                                                        
           05  HCSTA-CTABLEG2    PIC X(15).                                     
           05  HCSTA-CTABLEG2-REDEF REDEFINES HCSTA-CTABLEG2.                   
               10  HCSTA-NOMPROG         PIC X(06).                             
               10  HCSTA-NSEQ            PIC X(02).                             
           05  HCSTA-WTABLEG     PIC X(80).                                     
           05  HCSTA-WTABLEG-REDEF  REDEFINES HCSTA-WTABLEG.                    
               10  HCSTA-CSTATUT         PIC X(01).                             
               10  HCSTA-WACTIF          PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01OR-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01OR-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HCSTA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HCSTA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HCSTA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HCSTA-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
