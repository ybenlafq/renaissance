      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EBS51   EBS51                                              00000020
      ***************************************************************** 00000030
       01   EBS51I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * FONCTION                                                        00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MWFONCI   PIC X(3).                                       00000200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCREDEBL      COMP PIC S9(4).                            00000210
      *--                                                                       
           02 MDCREDEBL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDCREDEBF      PIC X.                                     00000220
           02 FILLER    PIC X(4).                                       00000230
           02 MDCREDEBI      PIC X(8).                                  00000240
      * CODE SERVICE                                                    00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCOFFREL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MNCOFFREL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCOFFREF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCOFFREI      PIC X(7).                                  00000290
      * LIBELLE LONG CODE SERVICE                                       00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOFFREL      COMP PIC S9(4).                            00000310
      *--                                                                       
           02 MLCOFFREL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLCOFFREF      PIC X.                                     00000320
           02 FILLER    PIC X(4).                                       00000330
           02 MLCOFFREI      PIC X(20).                                 00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMAJL    COMP PIC S9(4).                                 00000350
      *--                                                                       
           02 MDMAJL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDMAJF    PIC X.                                          00000360
           02 FILLER    PIC X(4).                                       00000370
           02 MDMAJI    PIC X(8).                                       00000380
      * CODE OPERATEUR                                                  00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MCFAMI    PIC X(5).                                       00000430
      * LIBELLE CODE OPERATEUR                                          00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000450
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MLFAMI    PIC X(20).                                      00000480
      * CODE FAMILLE                                                    00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPROFASSL     COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MCPROFASSL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCPROFASSF     PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCPROFASSI     PIC X(5).                                  00000530
      * LIBELLE CODE FAMILLE                                            00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPROFASSL     COMP PIC S9(4).                            00000550
      *--                                                                       
           02 MLPROFASSL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLPROFASSF     PIC X.                                     00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MLPROFASSI     PIC X(20).                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCASSORTCL     COMP PIC S9(4).                            00000590
      *--                                                                       
           02 MCASSORTCL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCASSORTCF     PIC X.                                     00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MCASSORTCI     PIC X(5).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATECL   COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MDATECL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATECF   PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MDATECI   PIC X(8).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCMARQI   PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MLMARQI   PIC X(20).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCASSORTFL     COMP PIC S9(4).                            00000750
      *--                                                                       
           02 MCASSORTFL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCASSORTFF     PIC X.                                     00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCASSORTFI     PIC X(5).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEFL   COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MDATEFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATEFF   PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MDATEFI   PIC X(8).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFDARTYL    COMP PIC S9(4).                            00000830
      *--                                                                       
           02 MLREFDARTYL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFDARTYF    PIC X.                                     00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MLREFDARTYI    PIC X(20).                                 00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMMENTL     COMP PIC S9(4).                            00000870
      *--                                                                       
           02 MLCOMMENTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLCOMMENTF     PIC X.                                     00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MLCOMMENTI     PIC X(50).                                 00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHEFL    COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCHEFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCHEFF    PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCHEFI    PIC X(5).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCHEFL   COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLCHEFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLCHEFF   PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLCHEFI   PIC X(20).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCOMPTL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCCOMPTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCCOMPTF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCCOMPTI  PIC X(3).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMPTL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MLCOMPTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCOMPTF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MLCOMPTI  PIC X(20).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTVAL    COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCTVAL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCTVAF    PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCTVAI    PIC X.                                          00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTVAL    COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MLTVAL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLTVAF    PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MLTVAI    PIC X(17).                                      00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTEMULTL      COMP PIC S9(4).                            00001150
      *--                                                                       
           02 MQTEMULTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQTEMULTF      PIC X.                                     00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MQTEMULTI      PIC X.                                     00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRIXL    COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MPRIXL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPRIXF    PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MPRIXI    PIC X(8).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRIMEL   COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MPRIMEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPRIMEF   PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MPRIMEI   PIC X(5).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MREMISEL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MREMISEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MREMISEF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MREMISEI  PIC X.                                          00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTICKETL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MTICKETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTICKETF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MTICKETI  PIC X.                                          00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODPROFL      COMP PIC S9(4).                            00001350
      *--                                                                       
           02 MCODPROFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCODPROFF      PIC X.                                     00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MCODPROFI      PIC X(10).                                 00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFAMCLTL  COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MFAMCLTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MFAMCLTF  PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MFAMCLTI  PIC X(30).                                      00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIREQL  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MCLIREQL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCLIREQF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MCLIREQI  PIC X.                                          00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLTREQL  COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MCLTREQL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCLTREQF  PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MCLTREQI  PIC X.                                          00001500
      * ZONE CMD AIDA                                                   00001510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001520
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001530
           02 FILLER    PIC X(4).                                       00001540
           02 MZONCMDI  PIC X(15).                                      00001550
      * MESSAGE ERREUR                                                  00001560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001570
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001580
           02 FILLER    PIC X(4).                                       00001590
           02 MLIBERRI  PIC X(58).                                      00001600
      * CODE TRANSACTION                                                00001610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001620
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001630
           02 FILLER    PIC X(4).                                       00001640
           02 MCODTRAI  PIC X(4).                                       00001650
      * CICS DE TRAVAIL                                                 00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001670
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001680
           02 FILLER    PIC X(4).                                       00001690
           02 MCICSI    PIC X(5).                                       00001700
      * NETNAMEe                                                        00001710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001720
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001730
           02 FILLER    PIC X(4).                                       00001740
           02 MNETNAMI  PIC X(8).                                       00001750
      * CODE TERMINAL                                                   00001760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001770
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001780
           02 FILLER    PIC X(4).                                       00001790
           02 MSCREENI  PIC X(5).                                       00001800
      ***************************************************************** 00001810
      * SDF: EBS51   EBS51                                              00001820
      ***************************************************************** 00001830
       01   EBS51O REDEFINES EBS51I.                                    00001840
           02 FILLER    PIC X(12).                                      00001850
      * DATE DU JOUR                                                    00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MDATJOUA  PIC X.                                          00001880
           02 MDATJOUC  PIC X.                                          00001890
           02 MDATJOUP  PIC X.                                          00001900
           02 MDATJOUH  PIC X.                                          00001910
           02 MDATJOUV  PIC X.                                          00001920
           02 MDATJOUO  PIC X(10).                                      00001930
      * HEURE                                                           00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MTIMJOUA  PIC X.                                          00001960
           02 MTIMJOUC  PIC X.                                          00001970
           02 MTIMJOUP  PIC X.                                          00001980
           02 MTIMJOUH  PIC X.                                          00001990
           02 MTIMJOUV  PIC X.                                          00002000
           02 MTIMJOUO  PIC X(5).                                       00002010
      * FONCTION                                                        00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MWFONCA   PIC X.                                          00002040
           02 MWFONCC   PIC X.                                          00002050
           02 MWFONCP   PIC X.                                          00002060
           02 MWFONCH   PIC X.                                          00002070
           02 MWFONCV   PIC X.                                          00002080
           02 MWFONCO   PIC X(3).                                       00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MDCREDEBA      PIC X.                                     00002110
           02 MDCREDEBC PIC X.                                          00002120
           02 MDCREDEBP PIC X.                                          00002130
           02 MDCREDEBH PIC X.                                          00002140
           02 MDCREDEBV PIC X.                                          00002150
           02 MDCREDEBO      PIC X(8).                                  00002160
      * CODE SERVICE                                                    00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MNCOFFREA      PIC X.                                     00002190
           02 MNCOFFREC PIC X.                                          00002200
           02 MNCOFFREP PIC X.                                          00002210
           02 MNCOFFREH PIC X.                                          00002220
           02 MNCOFFREV PIC X.                                          00002230
           02 MNCOFFREO      PIC X(7).                                  00002240
      * LIBELLE LONG CODE SERVICE                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MLCOFFREA      PIC X.                                     00002270
           02 MLCOFFREC PIC X.                                          00002280
           02 MLCOFFREP PIC X.                                          00002290
           02 MLCOFFREH PIC X.                                          00002300
           02 MLCOFFREV PIC X.                                          00002310
           02 MLCOFFREO      PIC X(20).                                 00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MDMAJA    PIC X.                                          00002340
           02 MDMAJC    PIC X.                                          00002350
           02 MDMAJP    PIC X.                                          00002360
           02 MDMAJH    PIC X.                                          00002370
           02 MDMAJV    PIC X.                                          00002380
           02 MDMAJO    PIC X(8).                                       00002390
      * CODE OPERATEUR                                                  00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MCFAMA    PIC X.                                          00002420
           02 MCFAMC    PIC X.                                          00002430
           02 MCFAMP    PIC X.                                          00002440
           02 MCFAMH    PIC X.                                          00002450
           02 MCFAMV    PIC X.                                          00002460
           02 MCFAMO    PIC X(5).                                       00002470
      * LIBELLE CODE OPERATEUR                                          00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MLFAMA    PIC X.                                          00002500
           02 MLFAMC    PIC X.                                          00002510
           02 MLFAMP    PIC X.                                          00002520
           02 MLFAMH    PIC X.                                          00002530
           02 MLFAMV    PIC X.                                          00002540
           02 MLFAMO    PIC X(20).                                      00002550
      * CODE FAMILLE                                                    00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MCPROFASSA     PIC X.                                     00002580
           02 MCPROFASSC     PIC X.                                     00002590
           02 MCPROFASSP     PIC X.                                     00002600
           02 MCPROFASSH     PIC X.                                     00002610
           02 MCPROFASSV     PIC X.                                     00002620
           02 MCPROFASSO     PIC X(5).                                  00002630
      * LIBELLE CODE FAMILLE                                            00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MLPROFASSA     PIC X.                                     00002660
           02 MLPROFASSC     PIC X.                                     00002670
           02 MLPROFASSP     PIC X.                                     00002680
           02 MLPROFASSH     PIC X.                                     00002690
           02 MLPROFASSV     PIC X.                                     00002700
           02 MLPROFASSO     PIC X(20).                                 00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MCASSORTCA     PIC X.                                     00002730
           02 MCASSORTCC     PIC X.                                     00002740
           02 MCASSORTCP     PIC X.                                     00002750
           02 MCASSORTCH     PIC X.                                     00002760
           02 MCASSORTCV     PIC X.                                     00002770
           02 MCASSORTCO     PIC X(5).                                  00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MDATECA   PIC X.                                          00002800
           02 MDATECC   PIC X.                                          00002810
           02 MDATECP   PIC X.                                          00002820
           02 MDATECH   PIC X.                                          00002830
           02 MDATECV   PIC X.                                          00002840
           02 MDATECO   PIC X(8).                                       00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MCMARQA   PIC X.                                          00002870
           02 MCMARQC   PIC X.                                          00002880
           02 MCMARQP   PIC X.                                          00002890
           02 MCMARQH   PIC X.                                          00002900
           02 MCMARQV   PIC X.                                          00002910
           02 MCMARQO   PIC X(5).                                       00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MLMARQA   PIC X.                                          00002940
           02 MLMARQC   PIC X.                                          00002950
           02 MLMARQP   PIC X.                                          00002960
           02 MLMARQH   PIC X.                                          00002970
           02 MLMARQV   PIC X.                                          00002980
           02 MLMARQO   PIC X(20).                                      00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MCASSORTFA     PIC X.                                     00003010
           02 MCASSORTFC     PIC X.                                     00003020
           02 MCASSORTFP     PIC X.                                     00003030
           02 MCASSORTFH     PIC X.                                     00003040
           02 MCASSORTFV     PIC X.                                     00003050
           02 MCASSORTFO     PIC X(5).                                  00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MDATEFA   PIC X.                                          00003080
           02 MDATEFC   PIC X.                                          00003090
           02 MDATEFP   PIC X.                                          00003100
           02 MDATEFH   PIC X.                                          00003110
           02 MDATEFV   PIC X.                                          00003120
           02 MDATEFO   PIC X(8).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MLREFDARTYA    PIC X.                                     00003150
           02 MLREFDARTYC    PIC X.                                     00003160
           02 MLREFDARTYP    PIC X.                                     00003170
           02 MLREFDARTYH    PIC X.                                     00003180
           02 MLREFDARTYV    PIC X.                                     00003190
           02 MLREFDARTYO    PIC X(20).                                 00003200
           02 FILLER    PIC X(2).                                       00003210
           02 MLCOMMENTA     PIC X.                                     00003220
           02 MLCOMMENTC     PIC X.                                     00003230
           02 MLCOMMENTP     PIC X.                                     00003240
           02 MLCOMMENTH     PIC X.                                     00003250
           02 MLCOMMENTV     PIC X.                                     00003260
           02 MLCOMMENTO     PIC X(50).                                 00003270
           02 FILLER    PIC X(2).                                       00003280
           02 MCHEFA    PIC X.                                          00003290
           02 MCHEFC    PIC X.                                          00003300
           02 MCHEFP    PIC X.                                          00003310
           02 MCHEFH    PIC X.                                          00003320
           02 MCHEFV    PIC X.                                          00003330
           02 MCHEFO    PIC X(5).                                       00003340
           02 FILLER    PIC X(2).                                       00003350
           02 MLCHEFA   PIC X.                                          00003360
           02 MLCHEFC   PIC X.                                          00003370
           02 MLCHEFP   PIC X.                                          00003380
           02 MLCHEFH   PIC X.                                          00003390
           02 MLCHEFV   PIC X.                                          00003400
           02 MLCHEFO   PIC X(20).                                      00003410
           02 FILLER    PIC X(2).                                       00003420
           02 MCCOMPTA  PIC X.                                          00003430
           02 MCCOMPTC  PIC X.                                          00003440
           02 MCCOMPTP  PIC X.                                          00003450
           02 MCCOMPTH  PIC X.                                          00003460
           02 MCCOMPTV  PIC X.                                          00003470
           02 MCCOMPTO  PIC X(3).                                       00003480
           02 FILLER    PIC X(2).                                       00003490
           02 MLCOMPTA  PIC X.                                          00003500
           02 MLCOMPTC  PIC X.                                          00003510
           02 MLCOMPTP  PIC X.                                          00003520
           02 MLCOMPTH  PIC X.                                          00003530
           02 MLCOMPTV  PIC X.                                          00003540
           02 MLCOMPTO  PIC X(20).                                      00003550
           02 FILLER    PIC X(2).                                       00003560
           02 MCTVAA    PIC X.                                          00003570
           02 MCTVAC    PIC X.                                          00003580
           02 MCTVAP    PIC X.                                          00003590
           02 MCTVAH    PIC X.                                          00003600
           02 MCTVAV    PIC X.                                          00003610
           02 MCTVAO    PIC X.                                          00003620
           02 FILLER    PIC X(2).                                       00003630
           02 MLTVAA    PIC X.                                          00003640
           02 MLTVAC    PIC X.                                          00003650
           02 MLTVAP    PIC X.                                          00003660
           02 MLTVAH    PIC X.                                          00003670
           02 MLTVAV    PIC X.                                          00003680
           02 MLTVAO    PIC X(17).                                      00003690
           02 FILLER    PIC X(2).                                       00003700
           02 MQTEMULTA      PIC X.                                     00003710
           02 MQTEMULTC PIC X.                                          00003720
           02 MQTEMULTP PIC X.                                          00003730
           02 MQTEMULTH PIC X.                                          00003740
           02 MQTEMULTV PIC X.                                          00003750
           02 MQTEMULTO      PIC X.                                     00003760
           02 FILLER    PIC X(2).                                       00003770
           02 MPRIXA    PIC X.                                          00003780
           02 MPRIXC    PIC X.                                          00003790
           02 MPRIXP    PIC X.                                          00003800
           02 MPRIXH    PIC X.                                          00003810
           02 MPRIXV    PIC X.                                          00003820
           02 MPRIXO    PIC X(8).                                       00003830
           02 FILLER    PIC X(2).                                       00003840
           02 MPRIMEA   PIC X.                                          00003850
           02 MPRIMEC   PIC X.                                          00003860
           02 MPRIMEP   PIC X.                                          00003870
           02 MPRIMEH   PIC X.                                          00003880
           02 MPRIMEV   PIC X.                                          00003890
           02 MPRIMEO   PIC X(5).                                       00003900
           02 FILLER    PIC X(2).                                       00003910
           02 MREMISEA  PIC X.                                          00003920
           02 MREMISEC  PIC X.                                          00003930
           02 MREMISEP  PIC X.                                          00003940
           02 MREMISEH  PIC X.                                          00003950
           02 MREMISEV  PIC X.                                          00003960
           02 MREMISEO  PIC X.                                          00003970
           02 FILLER    PIC X(2).                                       00003980
           02 MTICKETA  PIC X.                                          00003990
           02 MTICKETC  PIC X.                                          00004000
           02 MTICKETP  PIC X.                                          00004010
           02 MTICKETH  PIC X.                                          00004020
           02 MTICKETV  PIC X.                                          00004030
           02 MTICKETO  PIC X.                                          00004040
           02 FILLER    PIC X(2).                                       00004050
           02 MCODPROFA      PIC X.                                     00004060
           02 MCODPROFC PIC X.                                          00004070
           02 MCODPROFP PIC X.                                          00004080
           02 MCODPROFH PIC X.                                          00004090
           02 MCODPROFV PIC X.                                          00004100
           02 MCODPROFO      PIC X(10).                                 00004110
           02 FILLER    PIC X(2).                                       00004120
           02 MFAMCLTA  PIC X.                                          00004130
           02 MFAMCLTC  PIC X.                                          00004140
           02 MFAMCLTP  PIC X.                                          00004150
           02 MFAMCLTH  PIC X.                                          00004160
           02 MFAMCLTV  PIC X.                                          00004170
           02 MFAMCLTO  PIC X(30).                                      00004180
           02 FILLER    PIC X(2).                                       00004190
           02 MCLIREQA  PIC X.                                          00004200
           02 MCLIREQC  PIC X.                                          00004210
           02 MCLIREQP  PIC X.                                          00004220
           02 MCLIREQH  PIC X.                                          00004230
           02 MCLIREQV  PIC X.                                          00004240
           02 MCLIREQO  PIC X.                                          00004250
           02 FILLER    PIC X(2).                                       00004260
           02 MCLTREQA  PIC X.                                          00004270
           02 MCLTREQC  PIC X.                                          00004280
           02 MCLTREQP  PIC X.                                          00004290
           02 MCLTREQH  PIC X.                                          00004300
           02 MCLTREQV  PIC X.                                          00004310
           02 MCLTREQO  PIC X.                                          00004320
      * ZONE CMD AIDA                                                   00004330
           02 FILLER    PIC X(2).                                       00004340
           02 MZONCMDA  PIC X.                                          00004350
           02 MZONCMDC  PIC X.                                          00004360
           02 MZONCMDP  PIC X.                                          00004370
           02 MZONCMDH  PIC X.                                          00004380
           02 MZONCMDV  PIC X.                                          00004390
           02 MZONCMDO  PIC X(15).                                      00004400
      * MESSAGE ERREUR                                                  00004410
           02 FILLER    PIC X(2).                                       00004420
           02 MLIBERRA  PIC X.                                          00004430
           02 MLIBERRC  PIC X.                                          00004440
           02 MLIBERRP  PIC X.                                          00004450
           02 MLIBERRH  PIC X.                                          00004460
           02 MLIBERRV  PIC X.                                          00004470
           02 MLIBERRO  PIC X(58).                                      00004480
      * CODE TRANSACTION                                                00004490
           02 FILLER    PIC X(2).                                       00004500
           02 MCODTRAA  PIC X.                                          00004510
           02 MCODTRAC  PIC X.                                          00004520
           02 MCODTRAP  PIC X.                                          00004530
           02 MCODTRAH  PIC X.                                          00004540
           02 MCODTRAV  PIC X.                                          00004550
           02 MCODTRAO  PIC X(4).                                       00004560
      * CICS DE TRAVAIL                                                 00004570
           02 FILLER    PIC X(2).                                       00004580
           02 MCICSA    PIC X.                                          00004590
           02 MCICSC    PIC X.                                          00004600
           02 MCICSP    PIC X.                                          00004610
           02 MCICSH    PIC X.                                          00004620
           02 MCICSV    PIC X.                                          00004630
           02 MCICSO    PIC X(5).                                       00004640
      * NETNAMEe                                                        00004650
           02 FILLER    PIC X(2).                                       00004660
           02 MNETNAMA  PIC X.                                          00004670
           02 MNETNAMC  PIC X.                                          00004680
           02 MNETNAMP  PIC X.                                          00004690
           02 MNETNAMH  PIC X.                                          00004700
           02 MNETNAMV  PIC X.                                          00004710
           02 MNETNAMO  PIC X(8).                                       00004720
      * CODE TERMINAL                                                   00004730
           02 FILLER    PIC X(2).                                       00004740
           02 MSCREENA  PIC X.                                          00004750
           02 MSCREENC  PIC X.                                          00004760
           02 MSCREENP  PIC X.                                          00004770
           02 MSCREENH  PIC X.                                          00004780
           02 MSCREENV  PIC X.                                          00004790
           02 MSCREENO  PIC X(5).                                       00004800
                                                                                
