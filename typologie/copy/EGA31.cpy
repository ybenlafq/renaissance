      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA31   EGA31                                              00000020
      ***************************************************************** 00000030
       01   EGA31I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCODICI  PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLREFI    PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCASSORL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCASSORL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCASSORF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCASSORI  PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLASSORL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLASSORL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLASSORF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLASSORI  PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCFAMI    PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLFAMI    PIC X(20).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPROL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAPPROF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCAPPROI  PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAPPROL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLAPPROF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLAPPROI  PIC X(20).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCMARQI   PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLMARQI   PIC X(20).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPOL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXPOF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCEXPOI   PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLEXPOL   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MLEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLEXPOF   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLEXPOI   PIC X(20).                                      00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCRECCL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MDCRECCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDCRECCF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MDCRECCI  PIC X(8).                                       00000730
           02 M156I OCCURS   10 TIMES .                                 00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODIQL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MNCODIQL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODIQF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MNCODIQI     PIC X(7).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAMSL      COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MCFAMSL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCFAMSF      PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MCFAMSI      PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQSL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MCMARQSL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCMARQSF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MCMARQSI     PIC X(5).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFCCL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MLREFCCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLREFCCF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MLREFCCI     PIC X(20).                                 00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSTACCL     COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MCSTACCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCSTACCF     PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MCSTACCI     PIC X(3).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSAPPRL     COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MWSAPPRL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWSAPPRF     PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MWSAPPRI     PIC X.                                     00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSVTECL     COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MWSVTECL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWSVTECF     PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MWSVTECI     PIC X.                                     00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLRESUML     COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MLRESUML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLRESUMF     PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MLRESUMI     PIC X(20).                                 00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDRECCCL     COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MDRECCCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDRECCCF     PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MDRECCCI     PIC X(8).                                  00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MZONCMDI  PIC X(15).                                      00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MLIBERRI  PIC X(58).                                      00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MCODTRAI  PIC X(4).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MCICSI    PIC X(5).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MNETNAMI  PIC X(8).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MSCREENI  PIC X(4).                                       00001340
      ***************************************************************** 00001350
      * SDF: EGA31   EGA31                                              00001360
      ***************************************************************** 00001370
       01   EGA31O REDEFINES EGA31I.                                    00001380
           02 FILLER    PIC X(12).                                      00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MDATJOUA  PIC X.                                          00001410
           02 MDATJOUC  PIC X.                                          00001420
           02 MDATJOUP  PIC X.                                          00001430
           02 MDATJOUH  PIC X.                                          00001440
           02 MDATJOUV  PIC X.                                          00001450
           02 MDATJOUO  PIC X(10).                                      00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MTIMJOUA  PIC X.                                          00001480
           02 MTIMJOUC  PIC X.                                          00001490
           02 MTIMJOUP  PIC X.                                          00001500
           02 MTIMJOUH  PIC X.                                          00001510
           02 MTIMJOUV  PIC X.                                          00001520
           02 MTIMJOUO  PIC X(5).                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MPAGEA    PIC X.                                          00001550
           02 MPAGEC    PIC X.                                          00001560
           02 MPAGEP    PIC X.                                          00001570
           02 MPAGEH    PIC X.                                          00001580
           02 MPAGEV    PIC X.                                          00001590
           02 MPAGEO    PIC X(3).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MWFONCA   PIC X.                                          00001620
           02 MWFONCC   PIC X.                                          00001630
           02 MWFONCP   PIC X.                                          00001640
           02 MWFONCH   PIC X.                                          00001650
           02 MWFONCV   PIC X.                                          00001660
           02 MWFONCO   PIC X(3).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MNCODICA  PIC X.                                          00001690
           02 MNCODICC  PIC X.                                          00001700
           02 MNCODICP  PIC X.                                          00001710
           02 MNCODICH  PIC X.                                          00001720
           02 MNCODICV  PIC X.                                          00001730
           02 MNCODICO  PIC X(7).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MLREFA    PIC X.                                          00001760
           02 MLREFC    PIC X.                                          00001770
           02 MLREFP    PIC X.                                          00001780
           02 MLREFH    PIC X.                                          00001790
           02 MLREFV    PIC X.                                          00001800
           02 MLREFO    PIC X(20).                                      00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MCASSORA  PIC X.                                          00001830
           02 MCASSORC  PIC X.                                          00001840
           02 MCASSORP  PIC X.                                          00001850
           02 MCASSORH  PIC X.                                          00001860
           02 MCASSORV  PIC X.                                          00001870
           02 MCASSORO  PIC X(5).                                       00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MLASSORA  PIC X.                                          00001900
           02 MLASSORC  PIC X.                                          00001910
           02 MLASSORP  PIC X.                                          00001920
           02 MLASSORH  PIC X.                                          00001930
           02 MLASSORV  PIC X.                                          00001940
           02 MLASSORO  PIC X(20).                                      00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MCFAMA    PIC X.                                          00001970
           02 MCFAMC    PIC X.                                          00001980
           02 MCFAMP    PIC X.                                          00001990
           02 MCFAMH    PIC X.                                          00002000
           02 MCFAMV    PIC X.                                          00002010
           02 MCFAMO    PIC X(5).                                       00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MLFAMA    PIC X.                                          00002040
           02 MLFAMC    PIC X.                                          00002050
           02 MLFAMP    PIC X.                                          00002060
           02 MLFAMH    PIC X.                                          00002070
           02 MLFAMV    PIC X.                                          00002080
           02 MLFAMO    PIC X(20).                                      00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MCAPPROA  PIC X.                                          00002110
           02 MCAPPROC  PIC X.                                          00002120
           02 MCAPPROP  PIC X.                                          00002130
           02 MCAPPROH  PIC X.                                          00002140
           02 MCAPPROV  PIC X.                                          00002150
           02 MCAPPROO  PIC X(5).                                       00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MLAPPROA  PIC X.                                          00002180
           02 MLAPPROC  PIC X.                                          00002190
           02 MLAPPROP  PIC X.                                          00002200
           02 MLAPPROH  PIC X.                                          00002210
           02 MLAPPROV  PIC X.                                          00002220
           02 MLAPPROO  PIC X(20).                                      00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MCMARQA   PIC X.                                          00002250
           02 MCMARQC   PIC X.                                          00002260
           02 MCMARQP   PIC X.                                          00002270
           02 MCMARQH   PIC X.                                          00002280
           02 MCMARQV   PIC X.                                          00002290
           02 MCMARQO   PIC X(5).                                       00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLMARQA   PIC X.                                          00002320
           02 MLMARQC   PIC X.                                          00002330
           02 MLMARQP   PIC X.                                          00002340
           02 MLMARQH   PIC X.                                          00002350
           02 MLMARQV   PIC X.                                          00002360
           02 MLMARQO   PIC X(20).                                      00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MCEXPOA   PIC X.                                          00002390
           02 MCEXPOC   PIC X.                                          00002400
           02 MCEXPOP   PIC X.                                          00002410
           02 MCEXPOH   PIC X.                                          00002420
           02 MCEXPOV   PIC X.                                          00002430
           02 MCEXPOO   PIC X(5).                                       00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MLEXPOA   PIC X.                                          00002460
           02 MLEXPOC   PIC X.                                          00002470
           02 MLEXPOP   PIC X.                                          00002480
           02 MLEXPOH   PIC X.                                          00002490
           02 MLEXPOV   PIC X.                                          00002500
           02 MLEXPOO   PIC X(20).                                      00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MDCRECCA  PIC X.                                          00002530
           02 MDCRECCC  PIC X.                                          00002540
           02 MDCRECCP  PIC X.                                          00002550
           02 MDCRECCH  PIC X.                                          00002560
           02 MDCRECCV  PIC X.                                          00002570
           02 MDCRECCO  PIC X(8).                                       00002580
           02 M156O OCCURS   10 TIMES .                                 00002590
             03 FILLER       PIC X(2).                                  00002600
             03 MNCODIQA     PIC X.                                     00002610
             03 MNCODIQC     PIC X.                                     00002620
             03 MNCODIQP     PIC X.                                     00002630
             03 MNCODIQH     PIC X.                                     00002640
             03 MNCODIQV     PIC X.                                     00002650
             03 MNCODIQO     PIC X(7).                                  00002660
             03 FILLER       PIC X(2).                                  00002670
             03 MCFAMSA      PIC X.                                     00002680
             03 MCFAMSC PIC X.                                          00002690
             03 MCFAMSP PIC X.                                          00002700
             03 MCFAMSH PIC X.                                          00002710
             03 MCFAMSV PIC X.                                          00002720
             03 MCFAMSO      PIC X(5).                                  00002730
             03 FILLER       PIC X(2).                                  00002740
             03 MCMARQSA     PIC X.                                     00002750
             03 MCMARQSC     PIC X.                                     00002760
             03 MCMARQSP     PIC X.                                     00002770
             03 MCMARQSH     PIC X.                                     00002780
             03 MCMARQSV     PIC X.                                     00002790
             03 MCMARQSO     PIC X(5).                                  00002800
             03 FILLER       PIC X(2).                                  00002810
             03 MLREFCCA     PIC X.                                     00002820
             03 MLREFCCC     PIC X.                                     00002830
             03 MLREFCCP     PIC X.                                     00002840
             03 MLREFCCH     PIC X.                                     00002850
             03 MLREFCCV     PIC X.                                     00002860
             03 MLREFCCO     PIC X(20).                                 00002870
             03 FILLER       PIC X(2).                                  00002880
             03 MCSTACCA     PIC X.                                     00002890
             03 MCSTACCC     PIC X.                                     00002900
             03 MCSTACCP     PIC X.                                     00002910
             03 MCSTACCH     PIC X.                                     00002920
             03 MCSTACCV     PIC X.                                     00002930
             03 MCSTACCO     PIC X(3).                                  00002940
             03 FILLER       PIC X(2).                                  00002950
             03 MWSAPPRA     PIC X.                                     00002960
             03 MWSAPPRC     PIC X.                                     00002970
             03 MWSAPPRP     PIC X.                                     00002980
             03 MWSAPPRH     PIC X.                                     00002990
             03 MWSAPPRV     PIC X.                                     00003000
             03 MWSAPPRO     PIC X.                                     00003010
             03 FILLER       PIC X(2).                                  00003020
             03 MWSVTECA     PIC X.                                     00003030
             03 MWSVTECC     PIC X.                                     00003040
             03 MWSVTECP     PIC X.                                     00003050
             03 MWSVTECH     PIC X.                                     00003060
             03 MWSVTECV     PIC X.                                     00003070
             03 MWSVTECO     PIC X.                                     00003080
             03 FILLER       PIC X(2).                                  00003090
             03 MLRESUMA     PIC X.                                     00003100
             03 MLRESUMC     PIC X.                                     00003110
             03 MLRESUMP     PIC X.                                     00003120
             03 MLRESUMH     PIC X.                                     00003130
             03 MLRESUMV     PIC X.                                     00003140
             03 MLRESUMO     PIC X(20).                                 00003150
             03 FILLER       PIC X(2).                                  00003160
             03 MDRECCCA     PIC X.                                     00003170
             03 MDRECCCC     PIC X.                                     00003180
             03 MDRECCCP     PIC X.                                     00003190
             03 MDRECCCH     PIC X.                                     00003200
             03 MDRECCCV     PIC X.                                     00003210
             03 MDRECCCO     PIC X(8).                                  00003220
           02 FILLER    PIC X(2).                                       00003230
           02 MZONCMDA  PIC X.                                          00003240
           02 MZONCMDC  PIC X.                                          00003250
           02 MZONCMDP  PIC X.                                          00003260
           02 MZONCMDH  PIC X.                                          00003270
           02 MZONCMDV  PIC X.                                          00003280
           02 MZONCMDO  PIC X(15).                                      00003290
           02 FILLER    PIC X(2).                                       00003300
           02 MLIBERRA  PIC X.                                          00003310
           02 MLIBERRC  PIC X.                                          00003320
           02 MLIBERRP  PIC X.                                          00003330
           02 MLIBERRH  PIC X.                                          00003340
           02 MLIBERRV  PIC X.                                          00003350
           02 MLIBERRO  PIC X(58).                                      00003360
           02 FILLER    PIC X(2).                                       00003370
           02 MCODTRAA  PIC X.                                          00003380
           02 MCODTRAC  PIC X.                                          00003390
           02 MCODTRAP  PIC X.                                          00003400
           02 MCODTRAH  PIC X.                                          00003410
           02 MCODTRAV  PIC X.                                          00003420
           02 MCODTRAO  PIC X(4).                                       00003430
           02 FILLER    PIC X(2).                                       00003440
           02 MCICSA    PIC X.                                          00003450
           02 MCICSC    PIC X.                                          00003460
           02 MCICSP    PIC X.                                          00003470
           02 MCICSH    PIC X.                                          00003480
           02 MCICSV    PIC X.                                          00003490
           02 MCICSO    PIC X(5).                                       00003500
           02 FILLER    PIC X(2).                                       00003510
           02 MNETNAMA  PIC X.                                          00003520
           02 MNETNAMC  PIC X.                                          00003530
           02 MNETNAMP  PIC X.                                          00003540
           02 MNETNAMH  PIC X.                                          00003550
           02 MNETNAMV  PIC X.                                          00003560
           02 MNETNAMO  PIC X(8).                                       00003570
           02 FILLER    PIC X(2).                                       00003580
           02 MSCREENA  PIC X.                                          00003590
           02 MSCREENC  PIC X.                                          00003600
           02 MSCREENP  PIC X.                                          00003610
           02 MSCREENH  PIC X.                                          00003620
           02 MSCREENV  PIC X.                                          00003630
           02 MSCREENO  PIC X(4).                                       00003640
                                                                                
