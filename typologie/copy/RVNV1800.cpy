      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVNV1800                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA VUE RVNV1800                           
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVNV1800.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVNV1800.                                                            
      *}                                                                        
      *---------------------  CODE ARTICLE                                      
           05  NV18-NCODIC          PIC X(07).                                  
      *---------------------  CODE SOCIETE                                      
           05  NV18-NSOCIETE        PIC X(03).                                  
      *---------------------  CODE LIEU                                         
           05  NV18-NLIEU           PIC X(03).                                  
      *---------------------  CODE ACTION                                       
           05  NV18-CTYPE           PIC X(02).                                  
      *---------------------  VALEUR MONTANT                                    
           05  NV18-PMONTANT        PIC S9(7)V9(2) USAGE COMP-3.                
      *---------------------  DSYST                                             
           05  NV18-DSYST           PIC S9(13)V USAGE COMP-3.                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVNV1800                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVNV1800-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVNV1800-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV18-NCODIC-F            PIC S9(4) COMP.                         
      *--                                                                       
           05  NV18-NCODIC-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV18-NSOCIETE-F          PIC S9(4) COMP.                         
      *--                                                                       
           05  NV18-NSOCIETE-F          PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV18-NLIEU-F             PIC S9(4) COMP.                         
      *--                                                                       
           05  NV18-NLIEU-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV18-CTYPE-F             PIC S9(4) COMP.                         
      *--                                                                       
           05  NV18-CTYPE-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV18-PMONTANT-F          PIC S9(4) COMP.                         
      *--                                                                       
           05  NV18-PMONTANT-F          PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV18-DSYST-F             PIC S9(4) COMP.                         
      *                                                                         
      *--                                                                       
           05  NV18-DSYST-F             PIC S9(4) COMP-5.                       
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
