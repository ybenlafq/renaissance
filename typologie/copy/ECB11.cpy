      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * MAJ PRIX/PRIMES DEPUIS RELEVE                                   00000020
      ***************************************************************** 00000030
       01   ECB11I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(8).                                       00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGINEL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGINEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPAGINEF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGINEI  PIC X(7).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFFETL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MDEFFETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEFFETF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MDEFFETI  PIC X(8).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFINL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDFINL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDFINF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDFINI    PIC X(8).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MGROUPEL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MGROUPEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MGROUPEF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MGROUPEI  PIC X(9).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNSOCI    PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNZONEL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNZONEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNZONEF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNZONEI   PIC X(2).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCHEFPL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCCHEFPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCCHEFPF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCCHEFPI  PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCHEFPL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLCHEFPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCHEFPF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLCHEFPI  PIC X(20).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCFAMI    PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLFAMI    PIC X(20).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCMARQI   PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNCODICI  PIC X(7).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLREFI    PIC X(18).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATUTL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MSTATUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTATUTF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MSTATUTI  PIC X(5).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPXNATL   COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MPXNATL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPXNATF   PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MPXNATI   PIC X(8).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPXLOCL   COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MPXLOCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPXLOCF   PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MPXLOCI   PIC X(8).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSRPL     COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MSRPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSRPF     PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MSRPI     PIC X(8).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOAL      COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MOAL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MOAF      PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MOAI      PIC X.                                          00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSAL      COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MSAL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MSAF      PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MSAI      PIC X.                                          00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTMBLOCL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MTMBLOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTMBLOCF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MTMBLOCI  PIC X(4).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTTLOCL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MSTTLOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTTLOCF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MSTTLOCI  PIC X(4).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTTL     COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MSTTL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSTTF     PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MSTTI     PIC X(4).                                       00001010
           02 MSELD OCCURS   15 TIMES .                                 00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00001030
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MSELI   PIC X.                                          00001060
           02 MZONE1D OCCURS   15 TIMES .                               00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZONE1L      COMP PIC S9(4).                            00001080
      *--                                                                       
             03 MZONE1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MZONE1F      PIC X.                                     00001090
             03 FILLER  PIC X(4).                                       00001100
             03 MZONE1I      PIC X(8).                                  00001110
           02 MZONE2D OCCURS   15 TIMES .                               00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZONE2L      COMP PIC S9(4).                            00001130
      *--                                                                       
             03 MZONE2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MZONE2F      PIC X.                                     00001140
             03 FILLER  PIC X(4).                                       00001150
             03 MZONE2I      PIC X(36).                                 00001160
           02 MSIGND OCCURS   15 TIMES .                                00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSIGNL  COMP PIC S9(4).                                 00001180
      *--                                                                       
             03 MSIGNL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSIGNF  PIC X.                                          00001190
             03 FILLER  PIC X(4).                                       00001200
             03 MSIGNI  PIC X.                                          00001210
           02 MNMAGD OCCURS   15 TIMES .                                00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMAGL  COMP PIC S9(4).                                 00001230
      *--                                                                       
             03 MNMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMAGF  PIC X.                                          00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MNMAGI  PIC X(3).                                       00001260
           02 MNCNCD OCCURS   15 TIMES .                                00001270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCNCL  COMP PIC S9(4).                                 00001280
      *--                                                                       
             03 MNCNCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNCNCF  PIC X.                                          00001290
             03 FILLER  PIC X(4).                                       00001300
             03 MNCNCI  PIC X(4).                                       00001310
           02 MPXMAGD OCCURS   15 TIMES .                               00001320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPXMAGL      COMP PIC S9(4).                            00001330
      *--                                                                       
             03 MPXMAGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPXMAGF      PIC X.                                     00001340
             03 FILLER  PIC X(4).                                       00001350
             03 MPXMAGI      PIC X(8).                                  00001360
           02 MDFINMAGD OCCURS   15 TIMES .                             00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDFINMAGL    COMP PIC S9(4).                            00001380
      *--                                                                       
             03 MDFINMAGL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDFINMAGF    PIC X.                                     00001390
             03 FILLER  PIC X(4).                                       00001400
             03 MDFINMAGI    PIC X(5).                                  00001410
           02 MSTTMAGD OCCURS   15 TIMES .                              00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTTMAGL     COMP PIC S9(4).                            00001430
      *--                                                                       
             03 MSTTMAGL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTTMAGF     PIC X.                                     00001440
             03 FILLER  PIC X(4).                                       00001450
             03 MSTTMAGI     PIC X(4).                                  00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MLIBERRI  PIC X(79).                                      00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MCODTRAI  PIC X(4).                                       00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MCICSI    PIC X(5).                                       00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001590
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001600
           02 FILLER    PIC X(4).                                       00001610
           02 MNETNAMI  PIC X(8).                                       00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001630
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001640
           02 FILLER    PIC X(4).                                       00001650
           02 MSCREENI  PIC X(5).                                       00001660
      ***************************************************************** 00001670
      * MAJ PRIX/PRIMES DEPUIS RELEVE                                   00001680
      ***************************************************************** 00001690
       01   ECB11O REDEFINES ECB11I.                                    00001700
           02 FILLER    PIC X(12).                                      00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MDATJOUA  PIC X.                                          00001730
           02 MDATJOUC  PIC X.                                          00001740
           02 MDATJOUP  PIC X.                                          00001750
           02 MDATJOUH  PIC X.                                          00001760
           02 MDATJOUV  PIC X.                                          00001770
           02 MDATJOUO  PIC X(8).                                       00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MTIMJOUA  PIC X.                                          00001800
           02 MTIMJOUC  PIC X.                                          00001810
           02 MTIMJOUP  PIC X.                                          00001820
           02 MTIMJOUH  PIC X.                                          00001830
           02 MTIMJOUV  PIC X.                                          00001840
           02 MTIMJOUO  PIC X(5).                                       00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MPAGINEA  PIC X.                                          00001870
           02 MPAGINEC  PIC X.                                          00001880
           02 MPAGINEP  PIC X.                                          00001890
           02 MPAGINEH  PIC X.                                          00001900
           02 MPAGINEV  PIC X.                                          00001910
           02 MPAGINEO  PIC X(7).                                       00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MDEFFETA  PIC X.                                          00001940
           02 MDEFFETC  PIC X.                                          00001950
           02 MDEFFETP  PIC X.                                          00001960
           02 MDEFFETH  PIC X.                                          00001970
           02 MDEFFETV  PIC X.                                          00001980
           02 MDEFFETO  PIC X(8).                                       00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MDFINA    PIC X.                                          00002010
           02 MDFINC    PIC X.                                          00002020
           02 MDFINP    PIC X.                                          00002030
           02 MDFINH    PIC X.                                          00002040
           02 MDFINV    PIC X.                                          00002050
           02 MDFINO    PIC X(8).                                       00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MGROUPEA  PIC X.                                          00002080
           02 MGROUPEC  PIC X.                                          00002090
           02 MGROUPEP  PIC X.                                          00002100
           02 MGROUPEH  PIC X.                                          00002110
           02 MGROUPEV  PIC X.                                          00002120
           02 MGROUPEO  PIC X(9).                                       00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MNSOCA    PIC X.                                          00002150
           02 MNSOCC    PIC X.                                          00002160
           02 MNSOCP    PIC X.                                          00002170
           02 MNSOCH    PIC X.                                          00002180
           02 MNSOCV    PIC X.                                          00002190
           02 MNSOCO    PIC X(3).                                       00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MNZONEA   PIC X.                                          00002220
           02 MNZONEC   PIC X.                                          00002230
           02 MNZONEP   PIC X.                                          00002240
           02 MNZONEH   PIC X.                                          00002250
           02 MNZONEV   PIC X.                                          00002260
           02 MNZONEO   PIC X(2).                                       00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MCCHEFPA  PIC X.                                          00002290
           02 MCCHEFPC  PIC X.                                          00002300
           02 MCCHEFPP  PIC X.                                          00002310
           02 MCCHEFPH  PIC X.                                          00002320
           02 MCCHEFPV  PIC X.                                          00002330
           02 MCCHEFPO  PIC X(5).                                       00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MLCHEFPA  PIC X.                                          00002360
           02 MLCHEFPC  PIC X.                                          00002370
           02 MLCHEFPP  PIC X.                                          00002380
           02 MLCHEFPH  PIC X.                                          00002390
           02 MLCHEFPV  PIC X.                                          00002400
           02 MLCHEFPO  PIC X(20).                                      00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MCFAMA    PIC X.                                          00002430
           02 MCFAMC    PIC X.                                          00002440
           02 MCFAMP    PIC X.                                          00002450
           02 MCFAMH    PIC X.                                          00002460
           02 MCFAMV    PIC X.                                          00002470
           02 MCFAMO    PIC X(5).                                       00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MLFAMA    PIC X.                                          00002500
           02 MLFAMC    PIC X.                                          00002510
           02 MLFAMP    PIC X.                                          00002520
           02 MLFAMH    PIC X.                                          00002530
           02 MLFAMV    PIC X.                                          00002540
           02 MLFAMO    PIC X(20).                                      00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MCMARQA   PIC X.                                          00002570
           02 MCMARQC   PIC X.                                          00002580
           02 MCMARQP   PIC X.                                          00002590
           02 MCMARQH   PIC X.                                          00002600
           02 MCMARQV   PIC X.                                          00002610
           02 MCMARQO   PIC X(5).                                       00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MNCODICA  PIC X.                                          00002640
           02 MNCODICC  PIC X.                                          00002650
           02 MNCODICP  PIC X.                                          00002660
           02 MNCODICH  PIC X.                                          00002670
           02 MNCODICV  PIC X.                                          00002680
           02 MNCODICO  PIC X(7).                                       00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MLREFA    PIC X.                                          00002710
           02 MLREFC    PIC X.                                          00002720
           02 MLREFP    PIC X.                                          00002730
           02 MLREFH    PIC X.                                          00002740
           02 MLREFV    PIC X.                                          00002750
           02 MLREFO    PIC X(18).                                      00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MSTATUTA  PIC X.                                          00002780
           02 MSTATUTC  PIC X.                                          00002790
           02 MSTATUTP  PIC X.                                          00002800
           02 MSTATUTH  PIC X.                                          00002810
           02 MSTATUTV  PIC X.                                          00002820
           02 MSTATUTO  PIC X(5).                                       00002830
           02 FILLER    PIC X(2).                                       00002840
           02 MPXNATA   PIC X.                                          00002850
           02 MPXNATC   PIC X.                                          00002860
           02 MPXNATP   PIC X.                                          00002870
           02 MPXNATH   PIC X.                                          00002880
           02 MPXNATV   PIC X.                                          00002890
           02 MPXNATO   PIC X(8).                                       00002900
           02 FILLER    PIC X(2).                                       00002910
           02 MPXLOCA   PIC X.                                          00002920
           02 MPXLOCC   PIC X.                                          00002930
           02 MPXLOCP   PIC X.                                          00002940
           02 MPXLOCH   PIC X.                                          00002950
           02 MPXLOCV   PIC X.                                          00002960
           02 MPXLOCO   PIC X(8).                                       00002970
           02 FILLER    PIC X(2).                                       00002980
           02 MSRPA     PIC X.                                          00002990
           02 MSRPC     PIC X.                                          00003000
           02 MSRPP     PIC X.                                          00003010
           02 MSRPH     PIC X.                                          00003020
           02 MSRPV     PIC X.                                          00003030
           02 MSRPO     PIC X(8).                                       00003040
           02 FILLER    PIC X(2).                                       00003050
           02 MOAA      PIC X.                                          00003060
           02 MOAC      PIC X.                                          00003070
           02 MOAP      PIC X.                                          00003080
           02 MOAH      PIC X.                                          00003090
           02 MOAV      PIC X.                                          00003100
           02 MOAO      PIC X.                                          00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MSAA      PIC X.                                          00003130
           02 MSAC      PIC X.                                          00003140
           02 MSAP      PIC X.                                          00003150
           02 MSAH      PIC X.                                          00003160
           02 MSAV      PIC X.                                          00003170
           02 MSAO      PIC X.                                          00003180
           02 FILLER    PIC X(2).                                       00003190
           02 MTMBLOCA  PIC X.                                          00003200
           02 MTMBLOCC  PIC X.                                          00003210
           02 MTMBLOCP  PIC X.                                          00003220
           02 MTMBLOCH  PIC X.                                          00003230
           02 MTMBLOCV  PIC X.                                          00003240
           02 MTMBLOCO  PIC X(4).                                       00003250
           02 FILLER    PIC X(2).                                       00003260
           02 MSTTLOCA  PIC X.                                          00003270
           02 MSTTLOCC  PIC X.                                          00003280
           02 MSTTLOCP  PIC X.                                          00003290
           02 MSTTLOCH  PIC X.                                          00003300
           02 MSTTLOCV  PIC X.                                          00003310
           02 MSTTLOCO  PIC X(4).                                       00003320
           02 FILLER    PIC X(2).                                       00003330
           02 MSTTA     PIC X.                                          00003340
           02 MSTTC     PIC X.                                          00003350
           02 MSTTP     PIC X.                                          00003360
           02 MSTTH     PIC X.                                          00003370
           02 MSTTV     PIC X.                                          00003380
           02 MSTTO     PIC X(4).                                       00003390
           02 DFHMS1 OCCURS   15 TIMES .                                00003400
             03 FILLER       PIC X(2).                                  00003410
             03 MSELA   PIC X.                                          00003420
             03 MSELC   PIC X.                                          00003430
             03 MSELP   PIC X.                                          00003440
             03 MSELH   PIC X.                                          00003450
             03 MSELV   PIC X.                                          00003460
             03 MSELO   PIC X.                                          00003470
           02 DFHMS2 OCCURS   15 TIMES .                                00003480
             03 FILLER       PIC X(2).                                  00003490
             03 MZONE1A      PIC X.                                     00003500
             03 MZONE1C PIC X.                                          00003510
             03 MZONE1P PIC X.                                          00003520
             03 MZONE1H PIC X.                                          00003530
             03 MZONE1V PIC X.                                          00003540
             03 MZONE1O      PIC X(8).                                  00003550
           02 DFHMS3 OCCURS   15 TIMES .                                00003560
             03 FILLER       PIC X(2).                                  00003570
             03 MZONE2A      PIC X.                                     00003580
             03 MZONE2C PIC X.                                          00003590
             03 MZONE2P PIC X.                                          00003600
             03 MZONE2H PIC X.                                          00003610
             03 MZONE2V PIC X.                                          00003620
             03 MZONE2O      PIC X(36).                                 00003630
           02 DFHMS4 OCCURS   15 TIMES .                                00003640
             03 FILLER       PIC X(2).                                  00003650
             03 MSIGNA  PIC X.                                          00003660
             03 MSIGNC  PIC X.                                          00003670
             03 MSIGNP  PIC X.                                          00003680
             03 MSIGNH  PIC X.                                          00003690
             03 MSIGNV  PIC X.                                          00003700
             03 MSIGNO  PIC X.                                          00003710
           02 DFHMS5 OCCURS   15 TIMES .                                00003720
             03 FILLER       PIC X(2).                                  00003730
             03 MNMAGA  PIC X.                                          00003740
             03 MNMAGC  PIC X.                                          00003750
             03 MNMAGP  PIC X.                                          00003760
             03 MNMAGH  PIC X.                                          00003770
             03 MNMAGV  PIC X.                                          00003780
             03 MNMAGO  PIC X(3).                                       00003790
           02 DFHMS6 OCCURS   15 TIMES .                                00003800
             03 FILLER       PIC X(2).                                  00003810
             03 MNCNCA  PIC X.                                          00003820
             03 MNCNCC  PIC X.                                          00003830
             03 MNCNCP  PIC X.                                          00003840
             03 MNCNCH  PIC X.                                          00003850
             03 MNCNCV  PIC X.                                          00003860
             03 MNCNCO  PIC X(4).                                       00003870
           02 DFHMS7 OCCURS   15 TIMES .                                00003880
             03 FILLER       PIC X(2).                                  00003890
             03 MPXMAGA      PIC X.                                     00003900
             03 MPXMAGC PIC X.                                          00003910
             03 MPXMAGP PIC X.                                          00003920
             03 MPXMAGH PIC X.                                          00003930
             03 MPXMAGV PIC X.                                          00003940
             03 MPXMAGO      PIC X(8).                                  00003950
           02 DFHMS8 OCCURS   15 TIMES .                                00003960
             03 FILLER       PIC X(2).                                  00003970
             03 MDFINMAGA    PIC X.                                     00003980
             03 MDFINMAGC    PIC X.                                     00003990
             03 MDFINMAGP    PIC X.                                     00004000
             03 MDFINMAGH    PIC X.                                     00004010
             03 MDFINMAGV    PIC X.                                     00004020
             03 MDFINMAGO    PIC X(5).                                  00004030
           02 DFHMS9 OCCURS   15 TIMES .                                00004040
             03 FILLER       PIC X(2).                                  00004050
             03 MSTTMAGA     PIC X.                                     00004060
             03 MSTTMAGC     PIC X.                                     00004070
             03 MSTTMAGP     PIC X.                                     00004080
             03 MSTTMAGH     PIC X.                                     00004090
             03 MSTTMAGV     PIC X.                                     00004100
             03 MSTTMAGO     PIC X(4).                                  00004110
           02 FILLER    PIC X(2).                                       00004120
           02 MLIBERRA  PIC X.                                          00004130
           02 MLIBERRC  PIC X.                                          00004140
           02 MLIBERRP  PIC X.                                          00004150
           02 MLIBERRH  PIC X.                                          00004160
           02 MLIBERRV  PIC X.                                          00004170
           02 MLIBERRO  PIC X(79).                                      00004180
           02 FILLER    PIC X(2).                                       00004190
           02 MCODTRAA  PIC X.                                          00004200
           02 MCODTRAC  PIC X.                                          00004210
           02 MCODTRAP  PIC X.                                          00004220
           02 MCODTRAH  PIC X.                                          00004230
           02 MCODTRAV  PIC X.                                          00004240
           02 MCODTRAO  PIC X(4).                                       00004250
           02 FILLER    PIC X(2).                                       00004260
           02 MCICSA    PIC X.                                          00004270
           02 MCICSC    PIC X.                                          00004280
           02 MCICSP    PIC X.                                          00004290
           02 MCICSH    PIC X.                                          00004300
           02 MCICSV    PIC X.                                          00004310
           02 MCICSO    PIC X(5).                                       00004320
           02 FILLER    PIC X(2).                                       00004330
           02 MNETNAMA  PIC X.                                          00004340
           02 MNETNAMC  PIC X.                                          00004350
           02 MNETNAMP  PIC X.                                          00004360
           02 MNETNAMH  PIC X.                                          00004370
           02 MNETNAMV  PIC X.                                          00004380
           02 MNETNAMO  PIC X(8).                                       00004390
           02 FILLER    PIC X(2).                                       00004400
           02 MSCREENA  PIC X.                                          00004410
           02 MSCREENC  PIC X.                                          00004420
           02 MSCREENP  PIC X.                                          00004430
           02 MSCREENH  PIC X.                                          00004440
           02 MSCREENV  PIC X.                                          00004450
           02 MSCREENO  PIC X(5).                                       00004460
                                                                                
