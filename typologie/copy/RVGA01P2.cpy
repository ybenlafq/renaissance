      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE QCFMS FAMILLES SAV 36 CARTES QUALITE   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01P2.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01P2.                                                            
      *}                                                                        
           05  QCFMS-CTABLEG2    PIC X(15).                                     
           05  QCFMS-CTABLEG2-REDEF REDEFINES QCFMS-CTABLEG2.                   
               10  QCFMS-CFAM            PIC X(05).                             
           05  QCFMS-WTABLEG     PIC X(80).                                     
           05  QCFMS-WTABLEG-REDEF  REDEFINES QCFMS-WTABLEG.                    
               10  QCFMS-WACTIF          PIC X(01).                             
               10  QCFMS-LFAM            PIC X(20).                             
               10  QCFMS-CRAYON          PIC X(05).                             
               10  QCFMS-CTPSAV          PIC X(05).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01P2-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01P2-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QCFMS-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  QCFMS-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QCFMS-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  QCFMS-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
