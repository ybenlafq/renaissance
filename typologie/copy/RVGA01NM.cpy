      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE DSPER PERIODES OBSOLESCENCE            *        
      *----------------------------------------------------------------*        
       01  RVGA01NM.                                                            
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  DSPER-CTABLEG2    PIC X(15).                                     
           05  DSPER-CTABLEG2-REDEF REDEFINES DSPER-CTABLEG2.                   
               10  DSPER-DSPER           PIC X(05).                             
           05  DSPER-WTABLEG     PIC X(80).                                     
           05  DSPER-WTABLEG-REDEF  REDEFINES DSPER-WTABLEG.                    
               10  DSPER-PERIOD          PIC X(06).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  DSPER-PERIOD-N       REDEFINES DSPER-PERIOD                  
                                         PIC 9(06).                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  DSPER-PTAUX2          PIC X(06).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  DSPER-PTAUX2-N       REDEFINES DSPER-PTAUX2                  
                                         PIC 9(06).                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  DSPER-DARRETE         PIC X(08).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  DSPER-DARRETE-N      REDEFINES DSPER-DARRETE                 
                                         PIC 9(08).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01NM-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  DSPER-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  DSPER-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  DSPER-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  DSPER-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
