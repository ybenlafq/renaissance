      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      *        IDENTIFICATION SERVICE - CODES CONTROLES               * 00000020
      ***************************************************************** 00000030
      *                                                               * 00000040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS-51-LONG          PIC S9(4) COMP VALUE +2586.              00000050
      *--                                                                       
       01  TS-51-LONG          PIC S9(4) COMP-5 VALUE +2586.                    
      *}                                                                        
       01      TS-51-DATA.                                              00000060
            10 TS-POSTE-TSSE51    OCCURS 2.                             00000080
               15 TS-51-DATA-AV.                                        00000090
                  20 TS-51-FCODGES-AV    OCCURS 30 TIMES.               00001520
                     25 TS-51-FGEST-AV PIC X.                           00001530
                  20 TS-51-VCODGES-AV    OCCURS 30 TIMES.               00001520
                     25 TS-51-VGEST-AV PIC X(20).                       00001530
               15 TS-51-DATA-AP.                                        00000160
                  20 TS-51-FCODGES-AP    OCCURS 30 TIMES.               00001520
                     25 TS-51-FGEST-AP PIC X.                           00001530
                  20 TS-51-VCODGES-AP    OCCURS 30 TIMES.               00001520
                     25 TS-51-VGEST-AP PIC X(20).                       00001530
               15 TS-51-FLAG-MAJ.                                       00000250
                  20 TS-51-CODGES-AFF    OCCURS 30 TIMES.               00002090
                     25 TS-51-CGEST-AFF PIC X.                          00002100
      *        TYPE INNOVENTE                                        *  00000040
BF1            15 TS-51-CTYPINN        PIC X(1).                                
      *        TYPE INNOVENTE JUSTE AVANT LA DERNIERE ACTION         *  00000040
BF1            15 TS-51-CTYPINN-JAV    PIC X(1).                                
      *        TYPE INNOVENTE EN ENTREE DE SE00                      *  00000040
BF1            15 TS-51-CTYPINN-AV     PIC X(1).                                
                                                                                
