      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : VISUALISATION DU REFERENTIEL / FILIALE (GN30)          *         
      *****************************************************************         
      *                                                               *         
       01  TS-GN30.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GN30-LONG                 PIC S9(03) COMP-3                    
                                           VALUE +83.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-GN30-DONNEES.                                                  
      *----------------------------------  ZONE DE PRIX                         
              03 TS-GN30-ZONPRIX           PIC X(2).                            
      *----------------------------------  CODE LIEU                            
              03 TS-GN30-NLIEU             PIC X(03).                           
      *----------------------------------  EXECPTION OFFRE ACTIVE               
              03 TS-GN30-WOAE              PIC X.                               
      *----------------------------------  PRIX DE VENTE                        
              03 TS-GN30-PSTDTTC           PIC S9(5)V99.                        
      *----------------------------------  DATE DEB EFFET PRIX DE VENTE         
              03 TS-GN30-DEFFETPV          PIC X(08).                           
      *----------------------------------  MARGE BRUTE                          
              03 TS-GN30-MARGE             PIC S9(4)V9.                         
      *----------------------------------  TAUX MARGE BRUTE                     
              03 TS-GN30-TAUXMB            PIC S9(3)V9.                         
      *----------------------------------  PRIME                                
              03 TS-GN30-PRIME             PIC S9(3)V99.                        
      *----------------------------------  DEBUT EFFET DE LA PRIME              
              03 TS-GN30-DEFFETP           PIC X(8).                            
      *----------------------------------  STOCK MAGASIN                        
              03 TS-GN30-QSTOCKM           PIC 9(4).                            
      *----------------------------------  VENTE 4S                             
              03 TS-GN30-VTE4S             PIC 9(4).                            
      *----------------------------------  CODE ORIGINE                         
              03 TS-GN30-CORIG             PIC X.                               
      *----------------------------------  COEFFICIENT DE TRANSPOSITION         
              03 TS-GN30-COEFTRAN          PIC S9V9(6).                         
      *----------------------------------  VARIABLE DE TRANSPOSITION            
              03 TS-GN30-VARBTRAN          PIC S9(2)V9(2).                      
      *----------------------------------  COEFFICIENT FAMILLE                  
              03 TS-GN30-COEFFAM           PIC S9(3)V9.                         
      *----------------------------------  COEFFICIENT DE TRANSPO               
              03 TS-GN30-INDPR             PIC X(01).                           
                                                                                
