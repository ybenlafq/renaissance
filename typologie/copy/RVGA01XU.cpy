      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FEURO DEVISE DE GESTION PAR PROG       *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01XU.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01XU.                                                            
      *}                                                                        
           05  FEURO-CTABLEG2    PIC X(15).                                     
           05  FEURO-CTABLEG2-REDEF REDEFINES FEURO-CTABLEG2.                   
               10  FEURO-NOMPROG         PIC X(08).                             
           05  FEURO-WTABLEG     PIC X(80).                                     
           05  FEURO-WTABLEG-REDEF  REDEFINES FEURO-WTABLEG.                    
               10  FEURO-WACTIF          PIC X(01).                             
               10  FEURO-DEVISE          PIC X(03).                             
               10  FEURO-DEFFET          PIC X(08).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  FEURO-DEFFET-N       REDEFINES FEURO-DEFFET                  
                                         PIC 9(08).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  FEURO-DEVANC          PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01XU-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01XU-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FEURO-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FEURO-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FEURO-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FEURO-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
