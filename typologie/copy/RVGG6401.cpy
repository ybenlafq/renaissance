      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVGG6401                      *        
      ******************************************************************        
       01  RVGG6401.                                                            
      *                       CDESAP                                            
           10 GG64-CDESAP          PIC X(10).                                   
      *                       POSTE                                             
           10 GG64-POSTE           PIC X(5).                                    
      *                       NSOC                                              
           10 GG64-NSOC            PIC X(3).                                    
      *                       DATE                                              
           10 GG64-DATE            PIC X(8).                                    
      *                       TYPE                                              
           10 GG64-TYPE            PIC X(2).                                    
      *                       NCODIC                                            
           10 GG64-NCODIC          PIC X(7).                                    
      *                       MINOR                                             
           10 GG64-MINOR           PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       MAJOR                                             
           10 GG64-MAJOR           PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       STOCKDEB                                          
           10 GG64-STOCKDEB        PIC S9(9)V USAGE COMP-3.                     
      *                       DSYST                                             
           10 GG64-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       DTRAIT                                            
           10 GG64-DTRAIT          PIC X(8).                                    
      *                       FLAGFRA                                           
           10 GG64-FLAGFRA         PIC X(1).                                    
      *                       ACID                                              
           10 GG64-ACID            PIC X(8).                                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 13      *        
      ******************************************************************        
                                                                                
