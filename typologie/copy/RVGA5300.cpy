      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGA5300                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA5300                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA5300.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA5300.                                                            
      *}                                                                        
           02  GA53-NCODIC                                                      
               PIC X(0007).                                                     
           02  GA53-CDESCRIPTIF                                                 
               PIC X(0005).                                                     
           02  GA53-CVDESCRIPT                                                  
               PIC X(0005).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGA5300                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA5300-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA5300-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA53-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA53-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA53-CDESCRIPTIF-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA53-CDESCRIPTIF-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA53-CVDESCRIPT-F                                                
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GA53-CVDESCRIPT-F                                                
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
