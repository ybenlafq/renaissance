      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TGA70 TGA71 TGA72 TGA73  TR: GA70  *    00002200
      *                  GESTION TABLE DES TABLES                  *    00002300
      **************************************************************    00002400
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00002500
      **************************************************************    00002600
      *                                                                 00002700
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00002800
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00002900
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00003000
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00003100
      *                                                                 00003200
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00003300
      * COMPRENANT :                                                    00003400
      * 1 - LES ZONES RESERVEES A AIDA                                  00003500
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00003600
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00003700
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00003800
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00003900
      *                                                                 00004000
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00004100
      * PAR AIDA                                                        00004200
      *                                                                 00004300
      *-------------------------------------------------------------    00004400
      *                                                                 00004500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-GA70-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00004600
      *--                                                                       
       01  COM-GA70-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00004700
       01  Z-COMMAREA.                                                  00004800
      *                                                                 00004900
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00005000
          02 FILLER-COM-AIDA      PIC X(100).                           00005100
      *                                                                 00005200
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00005300
          02 COMM-CICS-APPLID     PIC X(8).                             00005400
          02 COMM-CICS-NETNAM     PIC X(8).                             00005500
          02 COMM-CICS-TRANSA     PIC X(4).                             00005600
      *                                                                 00005700
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00005800
          02 COMM-DATE-SIECLE     PIC XX.                               00005900
          02 COMM-DATE-ANNEE      PIC XX.                               00006000
          02 COMM-DATE-MOIS       PIC XX.                               00006100
          02 COMM-DATE-JOUR       PIC XX.                               00006200
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00006300
          02 COMM-DATE-QNTA       PIC 999.                              00006400
          02 COMM-DATE-QNT0       PIC 99999.                            00006500
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00006600
          02 COMM-DATE-BISX       PIC 9.                                00006700
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00006800
          02 COMM-DATE-JSM        PIC 9.                                00006900
      *   LIBELLES DU JOUR COURT - LONG                                 00007000
          02 COMM-DATE-JSM-LC     PIC XXX.                              00007100
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                         00007200
      *   LIBELLES DU MOIS COURT - LONG                                 00007300
          02 COMM-DATE-MOIS-LC    PIC XXX.                              00007400
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                         00007500
      *   DIFFERENTES FORMES DE DATE                                    00007600
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                             00007700
          02 COMM-DATE-AAMMJJ     PIC X(6).                             00007800
          02 COMM-DATE-JJMMSSAA   PIC X(8).                             00007900
          02 COMM-DATE-JJMMAA     PIC X(6).                             00008000
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                             00008100
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                            00008200
      *   DIFFERENTES FORMES DE DATE                                    00008300
          02 COMM-DATE-FILLER     PIC X(14).                            00008400
      *                                                                 00008500
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00008600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.              00008700
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                        00008800
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00009000
      *                                                                 00410100
      *            TRANSACTION GA70 : GESTION DES TABLES              * 00411000
      *                                                                 00412000
          02 COMM-GA70-APPLI.                                           00420000
      *------------------------------ CODE FONCTION                     00510000
             03 COMM-GA70-FONCT          PIC XXX.                       00520007
      *------------------------------ CODE TABLE                        00530000
             03 COMM-GA70-CTABLE         PIC X(6).                      00540000
      *------------------------------ CODE SOUS-TABLE                   00550000
             03 COMM-GA70-CSTABLE        PIC X(5).                      00560000
      *                                                                 00560100
      *--------------------------------------  ZONE TABLE DES TABLES    00561000
      *                                                                 00562000
      *------------------------------ LIBELLE TABLE                     00570000
             03 COMM-GA70-LTABLE         PIC X(30).                     00580000
      *------------------------------ CODE RESPONSABLE                  00590000
             03 COMM-GA70-CRESP          PIC X(3).                      00600000
      *------------------------------ DATE DEBUT                        00610000
             03 COMM-GA70-DDEBUT.                                       00620000
                04 COMM-GA70-DDEBUT-SS   PIC X(2).                      00621000
                04 COMM-GA70-DDEBUT-AA   PIC X(2).                      00622000
                04 COMM-GA70-DDEBUT-MM   PIC X(2).                      00623000
                04 COMM-GA70-DDEBUT-JJ   PIC X(2).                      00624000
      *------------------------------ DATE FIN                          00630000
             03 COMM-GA70-DFIN.                                         00640000
                04 COMM-GA70-DFIN-SS     PIC X(2).                      00641000
                04 COMM-GA70-DFIN-AA     PIC X(2).                      00642000
                04 COMM-GA70-DFIN-MM     PIC X(2).                      00643000
                04 COMM-GA70-DFIN-JJ     PIC X(2).                      00644000
      *------------------------------ DATE MISE A JOUR                  00650011
             03 COMM-GA70-DMAJ.                                         00660000
                04 COMM-GA70-DMAJ-SS     PIC X(2).                      00661000
                04 COMM-GA70-DMAJ-AA     PIC X(2).                      00662000
                04 COMM-GA70-DMAJ-MM     PIC X(2).                      00663000
                04 COMM-GA70-DMAJ-JJ     PIC X(2).                      00664000
      *------------------------------ EXISTANCE DE LIGNES = '1'         00670011
             03 COMM-GA70-EXIST          PIC X.                         00680011
      *------------------------------ NUMERO DU CHAMPS                  00681011
             03 COMM-GA70-NUMCHAMP       PIC 9(3).                      00682011
      *------------------------------ POSTES TABLE                      00690000
             03 COMM-GA70-POSTE          OCCURS 06.                     00700002
      *------------------------------ NOM DU CHAMP                      00701000
                04 COMM-GA70-CHAMP       PIC X(8).                      00702000
      *------------------------------ LIBELLE DU CHAMP                  00703000
                04 COMM-GA70-LCHAMP      PIC X(30).                     00704000
      *------------------------------ LONGUEUR EN OCTETS DU CHAMP       00705000
                04 COMM-GA70-QCHAMP      PIC 9(3).                      00706000
      *------------------------------ TYPE DU CHAMP                     00707000
                04 COMM-GA70-WCHAMP      PIC X(1).                      00708000
      *------------------------------ NBRE CHIFFRES APRES VIRGULE       00709000
                04 COMM-GA70-QDEC        PIC 9(3).                      00709100
      *------------------------------ CONTROLE DIRECT                   00709200
                04 COMM-GA70-WCTLDIR     PIC X(1).                      00709300
      *------------------------------ CONTROLE INDIRECT                 00709400
                04 COMM-GA70-WCTLIND     PIC X(1).                      00709500
      *------------------------------ NUMEROS DE CONTROLE               00709600
                04 COMM-GA70-CCTLIND     PIC X(3) OCCURS 5.             00709708
      *------------------------------ NOM TABLE ASSOCIEE                00709800
                04 COMM-GA70-CTABASS     PIC X(6).                      00709900
      *------------------------------ NOM SOUS-TABLE ASSOCIEE           00710000
                04 COMM-GA70-CSTABASS    PIC X(5).                      00710100
      *------------------------------ POSITION DANS LA CLE              00710200
                04 COMM-GA70-WPOSCLE     PIC 9(1).                      00710300
      *------------------------------ ZONE LIBRE                        00710400
             03 COMM-GA70-ATTR           PIC X OCCURS 197.              00710512
      *                                                                 00711000
      *                                                                 00712000
      *--------------------------------------  ZONE TABLE DES BORNES    00713000
      *                                                                 00714000
      *------------------------------ POSTES CONTROLE                   00714100
             03 COMM-GA70-POSCTR         OCCURS 10.                     00714200
      *------------------------------ CODE CONTROLE                     00715000
                04 COMM-GA70-CODCTR         PIC X(3).                   00716000
      *------------------------------ TYPE DE CONTROLE                  00717000
                04 COMM-GA70-TYPCTR         PIC X(1).                   00718000
      *------------------------------ BORNE MAXIMALE                    00719000
                04 COMM-GA70-BORMAX         PIC X(30).                  00729000
      *------------------------------ BORNE MINIMALE                    00729100
                04 COMM-GA70-BORMIN         PIC X(30).                  00729200
      *------------------------------ TABLE CODE CONTROLE PR PAGINATION 00729300
             03 COMM-GA70-TABCTR         OCCURS 100.                    00729400
      *------------------------------ 1ERS CODES CONTROLE DES PAGES     00729500
                04 COMM-GA70-CODTAB         PIC X(3).                   00729600
      *------------------------------ INDICATEUR ETAT   FICHIER         00729710
             03 COMM-GA70-INDPAG            PIC 9(3).                   00729806
      *------------------------------ NUMERO DE PAGE                    00729909
             03 COMM-GA70-NUMPAG            PIC 9(3).                   00730009
      *------------------------------ LIBELLE FONCTION                  00730100
             03 COMM-GA70-LIBFCT            PIC X(13).                  00730200
      *------------------------------ DERNIER NUMERO CTR AFFICHE        00730304
             03 COMM-GA70-PAGSUI            PIC X(3).                   00730404
      *------------------------------ ANCIEN NUMERO CTR AFFICHE         00730504
             03 COMM-GA70-PAGENC            PIC X(3).                   00730604
      *------------------------------ ZONE LIBRE                        00730700
             03 COMM-GA70-LIBRE2         PIC X(1750).                   00731009
      ***************************************************************** 00740000
                                                                                
