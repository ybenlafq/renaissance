      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE BS004 REGLES DE LETTRAGE COMMISSIONS   *        
      *----------------------------------------------------------------*        
       01  RVBS004.                                                             
           05  BS004-CTABLEG2    PIC X(15).                                     
           05  BS004-CTABLEG2-REDEF REDEFINES BS004-CTABLEG2.                   
               10  BS004-TYPCND          PIC X(05).                             
           05  BS004-WTABLEG     PIC X(80).                                     
           05  BS004-WTABLEG-REDEF  REDEFINES BS004-WTABLEG.                    
               10  BS004-V1              PIC X(05).                             
               10  BS004-V2              PIC X(05).                             
               10  BS004-V3              PIC X(05).                             
               10  BS004-V4              PIC X(05).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVBS004-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BS004-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  BS004-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BS004-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  BS004-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
