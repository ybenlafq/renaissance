      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00010000
      **************************************************************    00020000
      * COMMAREA SPECIFIQUE PRG TGG62 (TGG60 -> MENU)    TR: GG60  *    00030000
      *                                                            *    00031000
      *                       RECYCLAGE DU PRMP                    *    00040000
      *                                                            *    00050000
      * ZONES RESERVEES APPLICATIVES ------------------------------*    00060003
      *                                                            *    00070000
      *        TRANSACTION GG60 : RECYCLAGE PRMP SUR PRA           *    00080000
      *                                                            *    00090000
      *------------------------------ ZONE DONNEES TGG60---3724----*    00110000
      *                                                                 00720200
           02 COMM-GG62-APPLI   REDEFINES  COMM-GG60-APPLI.             00721000
              03 COMM-GG62-NCODIC     PIC X(7).                         00740102
              03 COMM-GG62-NENTCDE    PIC X(5).                         00740202
              03 COMM-GG62-LENTCDE    PIC X(20).                        00740302
              03 COMM-GG62-NREC       PIC X(7).                         00740402
              03 COMM-GG62-LREFFOURN  PIC X(20).                        00740502
              03 COMM-GG62-LREF       PIC X(20).                        00740503
              03 COMM-GG62-DREC       PIC X(8).                         00740603
              03 COMM-GG62-CMARQ      PIC X(5).                         00740702
              03 COMM-GG62-LMARQ      PIC X(20).                        00740703
              03 COMM-GG62-NCDE       PIC X(7).                         00740803
              03 COMM-GG62-CFAM       PIC X(5).                         00740804
              03 COMM-GG62-LFAM       PIC X(20).                        00740805
              03 COMM-GG62-QREC       PIC S9(5)      COMP-3.            00740806
              03 COMM-GG62-PRA        PIC S9(7)V99   COMP-3.            00740807
              03 COMM-GG62-PRAANC     PIC S9(7)V9(2) COMP-3.            00740808
              03 COMM-GG62-PRMPANC    PIC S9(7)V9(6) COMP-3.            00740809
              03 COMM-GG62-DATREF     PIC X(08).                        00740810
              03 COMM-GG62-JOUR       PIC X(08).                        00740820
              03 COMM-GG62-MESS       PIC X(58).                        00740830
      *                                                                 00741100
      *------------------------------ ZONE DONNEES TGG62                00741200
      *                                                                 00741300
              03 COMM-GG62-TOPFOIS    PIC X.                            00741404
              03 COMM-GG62-PRMP       PIC S9(7)V9(6) COMP-3.            00741405
              03 COMM-GG62-CONSPRMP   PIC S9(7)V9(6) COMP-3.            00741406
              03 COMM-GG62-WPRMPUTIL  PIC X.                            00741407
              03 FILLER               PIC X(3470).                      00743004
      *                                                                 00750000
                                                                                
