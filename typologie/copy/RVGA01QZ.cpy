      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE RALIE LIEUX DEPOTS/SAV: DIFF DE MAG    *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01Q.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01Q.                                                             
      *}                                                                        
           05  RALIE-CTABLEG2    PIC X(15).                                     
           05  RALIE-CTABLEG2-REDEF REDEFINES RALIE-CTABLEG2.                   
               10  RALIE-NSOC            PIC X(03).                             
               10  RALIE-NLIEU           PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  RALIE-NLIEU-N        REDEFINES RALIE-NLIEU                   
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  RALIE-WTABLEG     PIC X(80).                                     
           05  RALIE-WTABLEG-REDEF  REDEFINES RALIE-WTABLEG.                    
               10  RALIE-WACTIF          PIC X(01).                             
               10  RALIE-WTYPE           PIC X(01).                             
               10  RALIE-LLIEU           PIC X(25).                             
               10  RALIE-SEC-SOC         PIC X(09).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  RALIE-SEC-SOC-N      REDEFINES RALIE-SEC-SOC                 
                                         PIC 9(09).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01Q-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01Q-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RALIE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  RALIE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RALIE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  RALIE-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
