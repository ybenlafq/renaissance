      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE REDEV TABLE DES REDEVANCES             *        
      *----------------------------------------------------------------*        
       01  RVGA01NO.                                                            
           05  REDEV-CTABLEG2    PIC X(15).                                     
           05  REDEV-CTABLEG2-REDEF REDEFINES REDEV-CTABLEG2.                   
               10  REDEV-CFAM            PIC X(05).                             
           05  REDEV-WTABLEG     PIC X(80).                                     
           05  REDEV-WTABLEG-REDEF  REDEFINES REDEV-WTABLEG.                    
               10  REDEV-CTYPE           PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01NO-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  REDEV-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  REDEV-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  REDEV-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  REDEV-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
