      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA56   EGA56                                              00000020
      ***************************************************************** 00000030
       01   EGA56I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPOTL    COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNSOCDEPOTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNSOCDEPOTF    PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNSOCDEPOTI    PIC X(3).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNDEPOTI  PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MWFONCI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCODICI  PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLREFI    PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNFAMI    PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLFAMI    PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMSTOFL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCMSTOFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMSTOFF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCMSTOFI  PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBPVFL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MQNBPVFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQNBPVFF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MQNBPVFI  PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMARQL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNMARQF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNMARQI   PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLMARQI   PIC X(20).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNEANL    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNEANL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNEANF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNEANI    PIC X(13).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBPVCL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MQNBPVCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQNBPVCF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MQNBPVCI  PIC X(3).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQRMAILL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MQRMAILL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQRMAILF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MQRMAILI  PIC X(2).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNGERBL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MQNGERBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQNGERBF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MQNGERBI  PIC X(2).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQBOXCARTL     COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MQBOXCARTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQBOXCARTF     PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MQBOXCARTI     PIC X(4).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQCARTCOUCL    COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MQCARTCOUCL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MQCARTCOUCF    PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MQCARTCOUCI    PIC X(2).                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQCOUCHPALL    COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MQCOUCHPALL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MQCOUCHPALF    PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MQCOUCHPALI    PIC X(2).                                  00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBPRCL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MQNBPRCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQNBPRCF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MQNBPRCI  PIC X(5).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQPRODCAML     COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MQPRODCAML COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQPRODCAMF     PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MQPRODCAMI     PIC X(5).                                  00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCONTL   COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MCCONTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCCONTF   PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MCCONTI   PIC X.                                          00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSPSTL   COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MCSPSTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCSPSTF   PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MCSPSTI   PIC X.                                          00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MZONCMDI  PIC X(15).                                      00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MLIBERRI  PIC X(58).                                      00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MCODTRAI  PIC X(4).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MCICSI    PIC X(5).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MNETNAMI  PIC X(8).                                       00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MSCREENI  PIC X(4).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCOTEL   COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MCCOTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCCOTEF   PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MCCOTEI   PIC X.                                          00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMSTOC1L      COMP PIC S9(4).                            00001300
      *--                                                                       
           02 MCMSTOC1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCMSTOC1F      PIC X.                                     00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MCMSTOC1I      PIC X(5).                                  00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWMSTOC1L      COMP PIC S9(4).                            00001340
      *--                                                                       
           02 MWMSTOC1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWMSTOC1F      PIC X.                                     00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MWMSTOC1I      PIC X.                                     00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCACCESL  COMP PIC S9(4).                                 00001380
      *--                                                                       
           02 MCACCESL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCACCESF  PIC X.                                          00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MCACCESI  PIC X.                                          00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMSTOC2L      COMP PIC S9(4).                            00001420
      *--                                                                       
           02 MCMSTOC2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCMSTOC2F      PIC X.                                     00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MCMSTOC2I      PIC X(5).                                  00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWMSTOC2L      COMP PIC S9(4).                            00001460
      *--                                                                       
           02 MWMSTOC2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWMSTOC2F      PIC X.                                     00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MWMSTOC2I      PIC X.                                     00001490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSYSMECL  COMP PIC S9(4).                                 00001500
      *--                                                                       
           02 MSYSMECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSYSMECF  PIC X.                                          00001510
           02 FILLER    PIC X(4).                                       00001520
           02 MSYSMECI  PIC X.                                          00001530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMSTOC3L      COMP PIC S9(4).                            00001540
      *--                                                                       
           02 MCMSTOC3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCMSTOC3F      PIC X.                                     00001550
           02 FILLER    PIC X(4).                                       00001560
           02 MCMSTOC3I      PIC X(5).                                  00001570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWMSTOC3L      COMP PIC S9(4).                            00001580
      *--                                                                       
           02 MWMSTOC3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWMSTOC3F      PIC X.                                     00001590
           02 FILLER    PIC X(4).                                       00001600
           02 MWMSTOC3I      PIC X.                                     00001610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLASSEL  COMP PIC S9(4).                                 00001620
      *--                                                                       
           02 MCLASSEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCLASSEF  PIC X.                                          00001630
           02 FILLER    PIC X(4).                                       00001640
           02 MCLASSEI  PIC X.                                          00001650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWCDOCKL  COMP PIC S9(4).                                 00001660
      *--                                                                       
           02 MWCDOCKL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWCDOCKF  PIC X.                                          00001670
           02 FILLER    PIC X(4).                                       00001680
           02 MWCDOCKI  PIC X.                                          00001690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCQUOTAL  COMP PIC S9(4).                                 00001700
      *--                                                                       
           02 MCQUOTAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCQUOTAF  PIC X.                                          00001710
           02 FILLER    PIC X(4).                                       00001720
           02 MCQUOTAI  PIC X(5).                                       00001730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLQUOTAL  COMP PIC S9(4).                                 00001740
      *--                                                                       
           02 MLQUOTAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLQUOTAF  PIC X.                                          00001750
           02 FILLER    PIC X(4).                                       00001760
           02 MLQUOTAI  PIC X(20).                                      00001770
      ***************************************************************** 00001780
      * SDF: EGA56   EGA56                                              00001790
      ***************************************************************** 00001800
       01   EGA56O REDEFINES EGA56I.                                    00001810
           02 FILLER    PIC X(12).                                      00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MDATJOUA  PIC X.                                          00001840
           02 MDATJOUC  PIC X.                                          00001850
           02 MDATJOUP  PIC X.                                          00001860
           02 MDATJOUH  PIC X.                                          00001870
           02 MDATJOUV  PIC X.                                          00001880
           02 MDATJOUO  PIC X(10).                                      00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MTIMJOUA  PIC X.                                          00001910
           02 MTIMJOUC  PIC X.                                          00001920
           02 MTIMJOUP  PIC X.                                          00001930
           02 MTIMJOUH  PIC X.                                          00001940
           02 MTIMJOUV  PIC X.                                          00001950
           02 MTIMJOUO  PIC X(5).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MNSOCDEPOTA    PIC X.                                     00001980
           02 MNSOCDEPOTC    PIC X.                                     00001990
           02 MNSOCDEPOTP    PIC X.                                     00002000
           02 MNSOCDEPOTH    PIC X.                                     00002010
           02 MNSOCDEPOTV    PIC X.                                     00002020
           02 MNSOCDEPOTO    PIC X(3).                                  00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MNDEPOTA  PIC X.                                          00002050
           02 MNDEPOTC  PIC X.                                          00002060
           02 MNDEPOTP  PIC X.                                          00002070
           02 MNDEPOTH  PIC X.                                          00002080
           02 MNDEPOTV  PIC X.                                          00002090
           02 MNDEPOTO  PIC X(3).                                       00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MWFONCA   PIC X.                                          00002120
           02 MWFONCC   PIC X.                                          00002130
           02 MWFONCP   PIC X.                                          00002140
           02 MWFONCH   PIC X.                                          00002150
           02 MWFONCV   PIC X.                                          00002160
           02 MWFONCO   PIC X(3).                                       00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MNCODICA  PIC X.                                          00002190
           02 MNCODICC  PIC X.                                          00002200
           02 MNCODICP  PIC X.                                          00002210
           02 MNCODICH  PIC X.                                          00002220
           02 MNCODICV  PIC X.                                          00002230
           02 MNCODICO  PIC X(7).                                       00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MLREFA    PIC X.                                          00002260
           02 MLREFC    PIC X.                                          00002270
           02 MLREFP    PIC X.                                          00002280
           02 MLREFH    PIC X.                                          00002290
           02 MLREFV    PIC X.                                          00002300
           02 MLREFO    PIC X(20).                                      00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MNFAMA    PIC X.                                          00002330
           02 MNFAMC    PIC X.                                          00002340
           02 MNFAMP    PIC X.                                          00002350
           02 MNFAMH    PIC X.                                          00002360
           02 MNFAMV    PIC X.                                          00002370
           02 MNFAMO    PIC X(5).                                       00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MLFAMA    PIC X.                                          00002400
           02 MLFAMC    PIC X.                                          00002410
           02 MLFAMP    PIC X.                                          00002420
           02 MLFAMH    PIC X.                                          00002430
           02 MLFAMV    PIC X.                                          00002440
           02 MLFAMO    PIC X(20).                                      00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MCMSTOFA  PIC X.                                          00002470
           02 MCMSTOFC  PIC X.                                          00002480
           02 MCMSTOFP  PIC X.                                          00002490
           02 MCMSTOFH  PIC X.                                          00002500
           02 MCMSTOFV  PIC X.                                          00002510
           02 MCMSTOFO  PIC X(5).                                       00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MQNBPVFA  PIC X.                                          00002540
           02 MQNBPVFC  PIC X.                                          00002550
           02 MQNBPVFP  PIC X.                                          00002560
           02 MQNBPVFH  PIC X.                                          00002570
           02 MQNBPVFV  PIC X.                                          00002580
           02 MQNBPVFO  PIC X(5).                                       00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MNMARQA   PIC X.                                          00002610
           02 MNMARQC   PIC X.                                          00002620
           02 MNMARQP   PIC X.                                          00002630
           02 MNMARQH   PIC X.                                          00002640
           02 MNMARQV   PIC X.                                          00002650
           02 MNMARQO   PIC X(5).                                       00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MLMARQA   PIC X.                                          00002680
           02 MLMARQC   PIC X.                                          00002690
           02 MLMARQP   PIC X.                                          00002700
           02 MLMARQH   PIC X.                                          00002710
           02 MLMARQV   PIC X.                                          00002720
           02 MLMARQO   PIC X(20).                                      00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MNEANA    PIC X.                                          00002750
           02 MNEANC    PIC X.                                          00002760
           02 MNEANP    PIC X.                                          00002770
           02 MNEANH    PIC X.                                          00002780
           02 MNEANV    PIC X.                                          00002790
           02 MNEANO    PIC X(13).                                      00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MQNBPVCA  PIC X.                                          00002820
           02 MQNBPVCC  PIC X.                                          00002830
           02 MQNBPVCP  PIC X.                                          00002840
           02 MQNBPVCH  PIC X.                                          00002850
           02 MQNBPVCV  PIC X.                                          00002860
           02 MQNBPVCO  PIC X(3).                                       00002870
           02 FILLER    PIC X(2).                                       00002880
           02 MQRMAILA  PIC X.                                          00002890
           02 MQRMAILC  PIC X.                                          00002900
           02 MQRMAILP  PIC X.                                          00002910
           02 MQRMAILH  PIC X.                                          00002920
           02 MQRMAILV  PIC X.                                          00002930
           02 MQRMAILO  PIC X(2).                                       00002940
           02 FILLER    PIC X(2).                                       00002950
           02 MQNGERBA  PIC X.                                          00002960
           02 MQNGERBC  PIC X.                                          00002970
           02 MQNGERBP  PIC X.                                          00002980
           02 MQNGERBH  PIC X.                                          00002990
           02 MQNGERBV  PIC X.                                          00003000
           02 MQNGERBO  PIC X(2).                                       00003010
           02 FILLER    PIC X(2).                                       00003020
           02 MQBOXCARTA     PIC X.                                     00003030
           02 MQBOXCARTC     PIC X.                                     00003040
           02 MQBOXCARTP     PIC X.                                     00003050
           02 MQBOXCARTH     PIC X.                                     00003060
           02 MQBOXCARTV     PIC X.                                     00003070
           02 MQBOXCARTO     PIC X(4).                                  00003080
           02 FILLER    PIC X(2).                                       00003090
           02 MQCARTCOUCA    PIC X.                                     00003100
           02 MQCARTCOUCC    PIC X.                                     00003110
           02 MQCARTCOUCP    PIC X.                                     00003120
           02 MQCARTCOUCH    PIC X.                                     00003130
           02 MQCARTCOUCV    PIC X.                                     00003140
           02 MQCARTCOUCO    PIC X(2).                                  00003150
           02 FILLER    PIC X(2).                                       00003160
           02 MQCOUCHPALA    PIC X.                                     00003170
           02 MQCOUCHPALC    PIC X.                                     00003180
           02 MQCOUCHPALP    PIC X.                                     00003190
           02 MQCOUCHPALH    PIC X.                                     00003200
           02 MQCOUCHPALV    PIC X.                                     00003210
           02 MQCOUCHPALO    PIC X(2).                                  00003220
           02 FILLER    PIC X(2).                                       00003230
           02 MQNBPRCA  PIC X.                                          00003240
           02 MQNBPRCC  PIC X.                                          00003250
           02 MQNBPRCP  PIC X.                                          00003260
           02 MQNBPRCH  PIC X.                                          00003270
           02 MQNBPRCV  PIC X.                                          00003280
           02 MQNBPRCO  PIC X(5).                                       00003290
           02 FILLER    PIC X(2).                                       00003300
           02 MQPRODCAMA     PIC X.                                     00003310
           02 MQPRODCAMC     PIC X.                                     00003320
           02 MQPRODCAMP     PIC X.                                     00003330
           02 MQPRODCAMH     PIC X.                                     00003340
           02 MQPRODCAMV     PIC X.                                     00003350
           02 MQPRODCAMO     PIC X(5).                                  00003360
           02 FILLER    PIC X(2).                                       00003370
           02 MCCONTA   PIC X.                                          00003380
           02 MCCONTC   PIC X.                                          00003390
           02 MCCONTP   PIC X.                                          00003400
           02 MCCONTH   PIC X.                                          00003410
           02 MCCONTV   PIC X.                                          00003420
           02 MCCONTO   PIC X.                                          00003430
           02 FILLER    PIC X(2).                                       00003440
           02 MCSPSTA   PIC X.                                          00003450
           02 MCSPSTC   PIC X.                                          00003460
           02 MCSPSTP   PIC X.                                          00003470
           02 MCSPSTH   PIC X.                                          00003480
           02 MCSPSTV   PIC X.                                          00003490
           02 MCSPSTO   PIC X.                                          00003500
           02 FILLER    PIC X(2).                                       00003510
           02 MZONCMDA  PIC X.                                          00003520
           02 MZONCMDC  PIC X.                                          00003530
           02 MZONCMDP  PIC X.                                          00003540
           02 MZONCMDH  PIC X.                                          00003550
           02 MZONCMDV  PIC X.                                          00003560
           02 MZONCMDO  PIC X(15).                                      00003570
           02 FILLER    PIC X(2).                                       00003580
           02 MLIBERRA  PIC X.                                          00003590
           02 MLIBERRC  PIC X.                                          00003600
           02 MLIBERRP  PIC X.                                          00003610
           02 MLIBERRH  PIC X.                                          00003620
           02 MLIBERRV  PIC X.                                          00003630
           02 MLIBERRO  PIC X(58).                                      00003640
           02 FILLER    PIC X(2).                                       00003650
           02 MCODTRAA  PIC X.                                          00003660
           02 MCODTRAC  PIC X.                                          00003670
           02 MCODTRAP  PIC X.                                          00003680
           02 MCODTRAH  PIC X.                                          00003690
           02 MCODTRAV  PIC X.                                          00003700
           02 MCODTRAO  PIC X(4).                                       00003710
           02 FILLER    PIC X(2).                                       00003720
           02 MCICSA    PIC X.                                          00003730
           02 MCICSC    PIC X.                                          00003740
           02 MCICSP    PIC X.                                          00003750
           02 MCICSH    PIC X.                                          00003760
           02 MCICSV    PIC X.                                          00003770
           02 MCICSO    PIC X(5).                                       00003780
           02 FILLER    PIC X(2).                                       00003790
           02 MNETNAMA  PIC X.                                          00003800
           02 MNETNAMC  PIC X.                                          00003810
           02 MNETNAMP  PIC X.                                          00003820
           02 MNETNAMH  PIC X.                                          00003830
           02 MNETNAMV  PIC X.                                          00003840
           02 MNETNAMO  PIC X(8).                                       00003850
           02 FILLER    PIC X(2).                                       00003860
           02 MSCREENA  PIC X.                                          00003870
           02 MSCREENC  PIC X.                                          00003880
           02 MSCREENP  PIC X.                                          00003890
           02 MSCREENH  PIC X.                                          00003900
           02 MSCREENV  PIC X.                                          00003910
           02 MSCREENO  PIC X(4).                                       00003920
           02 FILLER    PIC X(2).                                       00003930
           02 MCCOTEA   PIC X.                                          00003940
           02 MCCOTEC   PIC X.                                          00003950
           02 MCCOTEP   PIC X.                                          00003960
           02 MCCOTEH   PIC X.                                          00003970
           02 MCCOTEV   PIC X.                                          00003980
           02 MCCOTEO   PIC X.                                          00003990
           02 FILLER    PIC X(2).                                       00004000
           02 MCMSTOC1A      PIC X.                                     00004010
           02 MCMSTOC1C PIC X.                                          00004020
           02 MCMSTOC1P PIC X.                                          00004030
           02 MCMSTOC1H PIC X.                                          00004040
           02 MCMSTOC1V PIC X.                                          00004050
           02 MCMSTOC1O      PIC X(5).                                  00004060
           02 FILLER    PIC X(2).                                       00004070
           02 MWMSTOC1A      PIC X.                                     00004080
           02 MWMSTOC1C PIC X.                                          00004090
           02 MWMSTOC1P PIC X.                                          00004100
           02 MWMSTOC1H PIC X.                                          00004110
           02 MWMSTOC1V PIC X.                                          00004120
           02 MWMSTOC1O      PIC X.                                     00004130
           02 FILLER    PIC X(2).                                       00004140
           02 MCACCESA  PIC X.                                          00004150
           02 MCACCESC  PIC X.                                          00004160
           02 MCACCESP  PIC X.                                          00004170
           02 MCACCESH  PIC X.                                          00004180
           02 MCACCESV  PIC X.                                          00004190
           02 MCACCESO  PIC X.                                          00004200
           02 FILLER    PIC X(2).                                       00004210
           02 MCMSTOC2A      PIC X.                                     00004220
           02 MCMSTOC2C PIC X.                                          00004230
           02 MCMSTOC2P PIC X.                                          00004240
           02 MCMSTOC2H PIC X.                                          00004250
           02 MCMSTOC2V PIC X.                                          00004260
           02 MCMSTOC2O      PIC X(5).                                  00004270
           02 FILLER    PIC X(2).                                       00004280
           02 MWMSTOC2A      PIC X.                                     00004290
           02 MWMSTOC2C PIC X.                                          00004300
           02 MWMSTOC2P PIC X.                                          00004310
           02 MWMSTOC2H PIC X.                                          00004320
           02 MWMSTOC2V PIC X.                                          00004330
           02 MWMSTOC2O      PIC X.                                     00004340
           02 FILLER    PIC X(2).                                       00004350
           02 MSYSMECA  PIC X.                                          00004360
           02 MSYSMECC  PIC X.                                          00004370
           02 MSYSMECP  PIC X.                                          00004380
           02 MSYSMECH  PIC X.                                          00004390
           02 MSYSMECV  PIC X.                                          00004400
           02 MSYSMECO  PIC X.                                          00004410
           02 FILLER    PIC X(2).                                       00004420
           02 MCMSTOC3A      PIC X.                                     00004430
           02 MCMSTOC3C PIC X.                                          00004440
           02 MCMSTOC3P PIC X.                                          00004450
           02 MCMSTOC3H PIC X.                                          00004460
           02 MCMSTOC3V PIC X.                                          00004470
           02 MCMSTOC3O      PIC X(5).                                  00004480
           02 FILLER    PIC X(2).                                       00004490
           02 MWMSTOC3A      PIC X.                                     00004500
           02 MWMSTOC3C PIC X.                                          00004510
           02 MWMSTOC3P PIC X.                                          00004520
           02 MWMSTOC3H PIC X.                                          00004530
           02 MWMSTOC3V PIC X.                                          00004540
           02 MWMSTOC3O      PIC X.                                     00004550
           02 FILLER    PIC X(2).                                       00004560
           02 MCLASSEA  PIC X.                                          00004570
           02 MCLASSEC  PIC X.                                          00004580
           02 MCLASSEP  PIC X.                                          00004590
           02 MCLASSEH  PIC X.                                          00004600
           02 MCLASSEV  PIC X.                                          00004610
           02 MCLASSEO  PIC X.                                          00004620
           02 FILLER    PIC X(2).                                       00004630
           02 MWCDOCKA  PIC X.                                          00004640
           02 MWCDOCKC  PIC X.                                          00004650
           02 MWCDOCKP  PIC X.                                          00004660
           02 MWCDOCKH  PIC X.                                          00004670
           02 MWCDOCKV  PIC X.                                          00004680
           02 MWCDOCKO  PIC X.                                          00004690
           02 FILLER    PIC X(2).                                       00004700
           02 MCQUOTAA  PIC X.                                          00004710
           02 MCQUOTAC  PIC X.                                          00004720
           02 MCQUOTAP  PIC X.                                          00004730
           02 MCQUOTAH  PIC X.                                          00004740
           02 MCQUOTAV  PIC X.                                          00004750
           02 MCQUOTAO  PIC X(5).                                       00004760
           02 FILLER    PIC X(2).                                       00004770
           02 MLQUOTAA  PIC X.                                          00004780
           02 MLQUOTAC  PIC X.                                          00004790
           02 MLQUOTAP  PIC X.                                          00004800
           02 MLQUOTAH  PIC X.                                          00004810
           02 MLQUOTAV  PIC X.                                          00004820
           02 MLQUOTAO  PIC X(20).                                      00004830
                                                                                
