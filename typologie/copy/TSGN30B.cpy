      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : VISUALISATION DU REFERENTIEL / FILIALE (GN30B)         *         
      *   PAGINATION PAR PF10/PF11                                    *         
      ******************************************************************        
      * DSA057 07/09/04 SUPPORT MAINTENANCE CBN                                 
      *                 PB AFFICHAGE PRIME REF ET STOCK/VTE                     
      ******************************************************************        
       01 TS-GN30B.                                                             
      *    02 TS-GN30B-LONG                PIC S9(03) COMP-3 VALUE +70.         
           02 TS-GN30B-LONG                PIC S9(03) COMP-3 VALUE +91.         
           02 TS-GN30B-DONNEES.                                                 
               03 TS-GN30B-WOA              PIC X(1).                           
               03 TS-GN30B-NCODICS          PIC X(07).                          
               03 TS-GN30B-DEFFETOA         PIC X(8).                           
               03 TS-GN30B-PREFTTC1         PIC S9(5)V99.                       
               03 TS-GN30B-WACTIF1          PIC X(01).                          
               03 TS-GN30B-DEFFET1          PIC X(8).                           
               03 TS-GN30B-LCOMMENT1        PIC X(10).                          
               03 TS-GN30B-PREFTTC2         PIC S9(5)V99.                       
               03 TS-GN30B-WACTIF2          PIC X(01).                          
               03 TS-GN30B-DEFFET2          PIC X(8).                           
               03 TS-GN30B-LCOMMENT2        PIC X(10).                          
               03 TS-GN30B-PRR              PIC S9(3)V99.                       
               03 TS-GN30B-DDPRR            PIC X(08).                          
               03 TS-GN30B-CPRR             PIC X(10).                          
                                                                                
