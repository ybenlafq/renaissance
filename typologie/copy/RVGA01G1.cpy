      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SLIEU TABLE DES SOUS-LIEUX             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01G1.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01G1.                                                            
      *}                                                                        
           05  SLIEU-CTABLEG2    PIC X(15).                                     
           05  SLIEU-CTABLEG2-REDEF REDEFINES SLIEU-CTABLEG2.                   
               10  SLIEU-CSSLIEU         PIC X(03).                             
               10  SLIEU-CTYPLIEU        PIC X(01).                             
           05  SLIEU-WTABLEG     PIC X(80).                                     
           05  SLIEU-WTABLEG-REDEF  REDEFINES SLIEU-WTABLEG.                    
               10  SLIEU-LIBEL           PIC X(20).                             
               10  SLIEU-WACT            PIC X(01).                             
               10  SLIEU-WHS             PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01G1-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01G1-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLIEU-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SLIEU-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLIEU-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SLIEU-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
