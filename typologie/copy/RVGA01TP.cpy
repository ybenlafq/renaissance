      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PRTYL DELAI LIVRAISON / TYPE PREST     *        
      *----------------------------------------------------------------*        
       01  RVGA01TP.                                                            
           05  PRTYL-CTABLEG2    PIC X(15).                                     
           05  PRTYL-CTABLEG2-REDEF REDEFINES PRTYL-CTABLEG2.                   
               10  PRTYL-CTPREST         PIC X(05).                             
           05  PRTYL-WTABLEG     PIC X(80).                                     
           05  PRTYL-WTABLEG-REDEF  REDEFINES PRTYL-WTABLEG.                    
               10  PRTYL-WACTIF          PIC X(01).                             
               10  PRTYL-DELMIN          PIC X(03).                             
               10  PRTYL-DELMIN-N       REDEFINES PRTYL-DELMIN                  
                                         PIC 9(03).                             
               10  PRTYL-DELMAX          PIC X(03).                             
               10  PRTYL-DELMAX-N       REDEFINES PRTYL-DELMAX                  
                                         PIC 9(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01TP-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRTYL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PRTYL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRTYL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PRTYL-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
