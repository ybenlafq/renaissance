      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGN40   EGN40                                              00000020
      ***************************************************************** 00000030
       01   EGN40I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(5).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(5).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(5).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(5).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000230
           02 FILLER    PIC X(5).                                       00000240
           02 MNCODICI  PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOUL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLREFFOUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLREFFOUF      PIC X.                                     00000270
           02 FILLER    PIC X(5).                                       00000280
           02 MLREFFOUI      PIC X(20).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000310
           02 FILLER    PIC X(5).                                       00000320
           02 MCMARQI   PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(5).                                       00000360
           02 MCFAMI    PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWOAL     COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MWOAL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MWOAF     PIC X.                                          00000390
           02 FILLER    PIC X(5).                                       00000400
           02 MWOAI     PIC X.                                          00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSUBSL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MSUBSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSUBSF    PIC X.                                          00000430
           02 FILLER    PIC X(5).                                       00000440
           02 MSUBSI    PIC X(6).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICSL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MNCODICSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODICSF      PIC X.                                     00000470
           02 FILLER    PIC X(5).                                       00000480
           02 MNCODICSI      PIC X(7).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 DEFFETOAL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 DEFFETOAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 DEFFETOAF      PIC X.                                     00000510
           02 FILLER    PIC X(5).                                       00000520
           02 DEFFETOAI      PIC X(8).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 DFINEFOAL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 DFINEFOAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 DFINEFOAF      PIC X.                                     00000550
           02 FILLER    PIC X(5).                                       00000560
           02 DFINEFOAI      PIC X(8).                                  00000570
           02 MPREFTTCD OCCURS   2 TIMES .                              00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPREFTTCL    COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MPREFTTCL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MPREFTTCF    PIC X.                                     00000600
             03 FILLER  PIC X(5).                                       00000610
             03 MPREFTTCI    PIC X(8).                                  00000620
           02 MWACTIFD OCCURS   2 TIMES .                               00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWACTIFL     COMP PIC S9(4).                            00000640
      *--                                                                       
             03 MWACTIFL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWACTIFF     PIC X.                                     00000650
             03 FILLER  PIC X(5).                                       00000660
             03 MWACTIFI     PIC X.                                     00000670
           02 DEFFETD OCCURS   2 TIMES .                                00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 DEFFETL      COMP PIC S9(4).                            00000690
      *--                                                                       
             03 DEFFETL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 DEFFETF      PIC X.                                     00000700
             03 FILLER  PIC X(5).                                       00000710
             03 DEFFETI      PIC X(8).                                  00000720
           02 DFINEFFED OCCURS   2 TIMES .                              00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 DFINEFFEL    COMP PIC S9(4).                            00000740
      *--                                                                       
             03 DFINEFFEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 DFINEFFEF    PIC X.                                     00000750
             03 FILLER  PIC X(5).                                       00000760
             03 DFINEFFEI    PIC X(8).                                  00000770
           02 MOPARENTD OCCURS   2 TIMES .                              00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MOPARENTL    COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MOPARENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MOPARENTF    PIC X.                                     00000800
             03 FILLER  PIC X(5).                                       00000810
             03 MOPARENTI    PIC X.                                     00000820
           02 LCOMMENTD OCCURS   2 TIMES .                              00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 LCOMMENTL    COMP PIC S9(4).                            00000840
      *--                                                                       
             03 LCOMMENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 LCOMMENTF    PIC X.                                     00000850
             03 FILLER  PIC X(5).                                       00000860
             03 LCOMMENTI    PIC X(10).                                 00000870
           02 MFPARENTD OCCURS   2 TIMES .                              00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFPARENTL    COMP PIC S9(4).                            00000890
      *--                                                                       
             03 MFPARENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MFPARENTF    PIC X.                                     00000900
             03 FILLER  PIC X(5).                                       00000910
             03 MFPARENTI    PIC X.                                     00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LIBF10F11L     COMP PIC S9(4).                            00000930
      *--                                                                       
           02 LIBF10F11L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 LIBF10F11F     PIC X.                                     00000940
           02 FILLER    PIC X(5).                                       00000950
           02 LIBF10F11I     PIC X(33).                                 00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCL     COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MSOCL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSOCF     PIC X.                                          00000980
           02 FILLER    PIC X(5).                                       00000990
           02 MSOCI     PIC X(3).                                       00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZPL      COMP PIC S9(4).                                 00001010
      *--                                                                       
           02 MZPL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MZPF      PIC X.                                          00001020
           02 FILLER    PIC X(5).                                       00001030
           02 MZPI      PIC X(2).                                       00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUL    COMP PIC S9(4).                                 00001050
      *--                                                                       
           02 MLIEUL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIEUF    PIC X.                                          00001060
           02 FILLER    PIC X(5).                                       00001070
           02 MLIEUI    PIC X(3).                                       00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEROL    COMP PIC S9(4).                                 00001090
      *--                                                                       
           02 MDEROL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDEROF    PIC X.                                          00001100
           02 FILLER    PIC X(5).                                       00001110
           02 MDEROI    PIC X(4).                                       00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 DDEFFETDL      COMP PIC S9(4).                            00001130
      *--                                                                       
           02 DDEFFETDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 DDEFFETDF      PIC X.                                     00001140
           02 FILLER    PIC X(5).                                       00001150
           02 DDEFFETDI      PIC X(8).                                  00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 DFEFFETDL      COMP PIC S9(4).                            00001170
      *--                                                                       
           02 DFEFFETDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 DFEFFETDF      PIC X.                                     00001180
           02 FILLER    PIC X(5).                                       00001190
           02 DFEFFETDI      PIC X(8).                                  00001200
           02 MACTIOND OCCURS   10 TIMES .                              00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTIONL     COMP PIC S9(4).                            00001220
      *--                                                                       
             03 MACTIONL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MACTIONF     PIC X.                                     00001230
             03 FILLER  PIC X(5).                                       00001240
             03 MACTIONI     PIC X.                                     00001250
           02 MSOCIETED OCCURS   10 TIMES .                             00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCIETEL    COMP PIC S9(4).                            00001270
      *--                                                                       
             03 MSOCIETEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSOCIETEF    PIC X.                                     00001280
             03 FILLER  PIC X(5).                                       00001290
             03 MSOCIETEI    PIC X(3).                                  00001300
           02 MZONPRIXD OCCURS   10 TIMES .                             00001310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZONPRIXL    COMP PIC S9(4).                            00001320
      *--                                                                       
             03 MZONPRIXL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MZONPRIXF    PIC X.                                     00001330
             03 FILLER  PIC X(5).                                       00001340
             03 MZONPRIXI    PIC X(2).                                  00001350
           02 MNLIEUD OCCURS   10 TIMES .                               00001360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00001370
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00001380
             03 FILLER  PIC X(5).                                       00001390
             03 MNLIEUI      PIC X(3).                                  00001400
           02 MLREFD OCCURS   10 TIMES .                                00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00001420
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00001430
             03 FILLER  PIC X(5).                                       00001440
             03 MLREFI  PIC X(4).                                       00001450
           02 DDEFFETRD OCCURS   10 TIMES .                             00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 DDEFFETRL    COMP PIC S9(4).                            00001470
      *--                                                                       
             03 DDEFFETRL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 DDEFFETRF    PIC X.                                     00001480
             03 FILLER  PIC X(5).                                       00001490
             03 DDEFFETRI    PIC X(8).                                  00001500
           02 DFEFFETRD OCCURS   10 TIMES .                             00001510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 DFEFFETRL    COMP PIC S9(4).                            00001520
      *--                                                                       
             03 DFEFFETRL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 DFEFFETRF    PIC X.                                     00001530
             03 FILLER  PIC X(5).                                       00001540
             03 DFEFFETRI    PIC X(8).                                  00001550
           02 MPSTDTTCD OCCURS   10 TIMES .                             00001560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPSTDTTCL    COMP PIC S9(4).                            00001570
      *--                                                                       
             03 MPSTDTTCL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MPSTDTTCF    PIC X.                                     00001580
             03 FILLER  PIC X(5).                                       00001590
             03 MPSTDTTCI    PIC X(8).                                  00001600
           02 MCORIGD OCCURS   10 TIMES .                               00001610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCORIGL      COMP PIC S9(4).                            00001620
      *--                                                                       
             03 MCORIGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCORIGF      PIC X.                                     00001630
             03 FILLER  PIC X(5).                                       00001640
             03 MCORIGI      PIC X.                                     00001650
           02 DDEFFETSD OCCURS   10 TIMES .                             00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 DDEFFETSL    COMP PIC S9(4).                            00001670
      *--                                                                       
             03 DDEFFETSL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 DDEFFETSF    PIC X.                                     00001680
             03 FILLER  PIC X(5).                                       00001690
             03 DDEFFETSI    PIC X(8).                                  00001700
           02 DFEFFETSD OCCURS   10 TIMES .                             00001710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 DFEFFETSL    COMP PIC S9(4).                            00001720
      *--                                                                       
             03 DFEFFETSL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 DFEFFETSF    PIC X.                                     00001730
             03 FILLER  PIC X(5).                                       00001740
             03 DFEFFETSI    PIC X(8).                                  00001750
           02 MMARGED OCCURS   10 TIMES .                               00001760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMARGEL      COMP PIC S9(4).                            00001770
      *--                                                                       
             03 MMARGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MMARGEF      PIC X.                                     00001780
             03 FILLER  PIC X(5).                                       00001790
             03 MMARGEI      PIC X(7).                                  00001800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001810
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001820
           02 FILLER    PIC X(5).                                       00001830
           02 MLIBERRI  PIC X(78).                                      00001840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001850
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001860
           02 FILLER    PIC X(5).                                       00001870
           02 MCODTRAI  PIC X(4).                                       00001880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001890
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001900
           02 FILLER    PIC X(5).                                       00001910
           02 MCICSI    PIC X(5).                                       00001920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001930
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001940
           02 FILLER    PIC X(5).                                       00001950
           02 MNETNAMI  PIC X(8).                                       00001960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001970
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001980
           02 FILLER    PIC X(5).                                       00001990
           02 MSCREENI  PIC X(4).                                       00002000
      ***************************************************************** 00002010
      * SDF: EGN40   EGN40                                              00002020
      ***************************************************************** 00002030
       01   EGN40O REDEFINES EGN40I.                                    00002040
           02 FILLER    PIC X(12).                                      00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MDATJOUA  PIC X.                                          00002070
           02 MDATJOUC  PIC X.                                          00002080
           02 MDATJOUP  PIC X.                                          00002090
           02 MDATJOUH  PIC X.                                          00002100
           02 MDATJOUV  PIC X.                                          00002110
           02 MDATJOUU  PIC X.                                          00002120
           02 MDATJOUO  PIC X(10).                                      00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MTIMJOUA  PIC X.                                          00002150
           02 MTIMJOUC  PIC X.                                          00002160
           02 MTIMJOUP  PIC X.                                          00002170
           02 MTIMJOUH  PIC X.                                          00002180
           02 MTIMJOUV  PIC X.                                          00002190
           02 MTIMJOUU  PIC X.                                          00002200
           02 MTIMJOUO  PIC X(5).                                       00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MPAGEA    PIC X.                                          00002230
           02 MPAGEC    PIC X.                                          00002240
           02 MPAGEP    PIC X.                                          00002250
           02 MPAGEH    PIC X.                                          00002260
           02 MPAGEV    PIC X.                                          00002270
           02 MPAGEU    PIC X.                                          00002280
           02 MPAGEO    PIC X(3).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MPAGEMAXA      PIC X.                                     00002310
           02 MPAGEMAXC PIC X.                                          00002320
           02 MPAGEMAXP PIC X.                                          00002330
           02 MPAGEMAXH PIC X.                                          00002340
           02 MPAGEMAXV PIC X.                                          00002350
           02 MPAGEMAXU PIC X.                                          00002360
           02 MPAGEMAXO      PIC X(3).                                  00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MNCODICA  PIC X.                                          00002390
           02 MNCODICC  PIC X.                                          00002400
           02 MNCODICP  PIC X.                                          00002410
           02 MNCODICH  PIC X.                                          00002420
           02 MNCODICV  PIC X.                                          00002430
           02 MNCODICU  PIC X.                                          00002440
           02 MNCODICO  PIC X(7).                                       00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MLREFFOUA      PIC X.                                     00002470
           02 MLREFFOUC PIC X.                                          00002480
           02 MLREFFOUP PIC X.                                          00002490
           02 MLREFFOUH PIC X.                                          00002500
           02 MLREFFOUV PIC X.                                          00002510
           02 MLREFFOUU PIC X.                                          00002520
           02 MLREFFOUO      PIC X(20).                                 00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MCMARQA   PIC X.                                          00002550
           02 MCMARQC   PIC X.                                          00002560
           02 MCMARQP   PIC X.                                          00002570
           02 MCMARQH   PIC X.                                          00002580
           02 MCMARQV   PIC X.                                          00002590
           02 MCMARQU   PIC X.                                          00002600
           02 MCMARQO   PIC X(5).                                       00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MCFAMA    PIC X.                                          00002630
           02 MCFAMC    PIC X.                                          00002640
           02 MCFAMP    PIC X.                                          00002650
           02 MCFAMH    PIC X.                                          00002660
           02 MCFAMV    PIC X.                                          00002670
           02 MCFAMU    PIC X.                                          00002680
           02 MCFAMO    PIC X(5).                                       00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MWOAA     PIC X.                                          00002710
           02 MWOAC     PIC X.                                          00002720
           02 MWOAP     PIC X.                                          00002730
           02 MWOAH     PIC X.                                          00002740
           02 MWOAV     PIC X.                                          00002750
           02 MWOAU     PIC X.                                          00002760
           02 MWOAO     PIC X.                                          00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MSUBSA    PIC X.                                          00002790
           02 MSUBSC    PIC X.                                          00002800
           02 MSUBSP    PIC X.                                          00002810
           02 MSUBSH    PIC X.                                          00002820
           02 MSUBSV    PIC X.                                          00002830
           02 MSUBSU    PIC X.                                          00002840
           02 MSUBSO    PIC X(6).                                       00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MNCODICSA      PIC X.                                     00002870
           02 MNCODICSC PIC X.                                          00002880
           02 MNCODICSP PIC X.                                          00002890
           02 MNCODICSH PIC X.                                          00002900
           02 MNCODICSV PIC X.                                          00002910
           02 MNCODICSU PIC X.                                          00002920
           02 MNCODICSO      PIC X(7).                                  00002930
           02 FILLER    PIC X(2).                                       00002940
           02 DEFFETOAA      PIC X.                                     00002950
           02 DEFFETOAC PIC X.                                          00002960
           02 DEFFETOAP PIC X.                                          00002970
           02 DEFFETOAH PIC X.                                          00002980
           02 DEFFETOAV PIC X.                                          00002990
           02 DEFFETOAU PIC X.                                          00003000
           02 DEFFETOAO      PIC X(8).                                  00003010
           02 FILLER    PIC X(2).                                       00003020
           02 DFINEFOAA      PIC X.                                     00003030
           02 DFINEFOAC PIC X.                                          00003040
           02 DFINEFOAP PIC X.                                          00003050
           02 DFINEFOAH PIC X.                                          00003060
           02 DFINEFOAV PIC X.                                          00003070
           02 DFINEFOAU PIC X.                                          00003080
           02 DFINEFOAO      PIC X(8).                                  00003090
           02 DFHMS1 OCCURS   2 TIMES .                                 00003100
             03 FILLER       PIC X(2).                                  00003110
             03 MPREFTTCA    PIC X.                                     00003120
             03 MPREFTTCC    PIC X.                                     00003130
             03 MPREFTTCP    PIC X.                                     00003140
             03 MPREFTTCH    PIC X.                                     00003150
             03 MPREFTTCV    PIC X.                                     00003160
             03 MPREFTTCU    PIC X.                                     00003170
             03 MPREFTTCO    PIC X(8).                                  00003180
           02 DFHMS2 OCCURS   2 TIMES .                                 00003190
             03 FILLER       PIC X(2).                                  00003200
             03 MWACTIFA     PIC X.                                     00003210
             03 MWACTIFC     PIC X.                                     00003220
             03 MWACTIFP     PIC X.                                     00003230
             03 MWACTIFH     PIC X.                                     00003240
             03 MWACTIFV     PIC X.                                     00003250
             03 MWACTIFU     PIC X.                                     00003260
             03 MWACTIFO     PIC X.                                     00003270
           02 DFHMS3 OCCURS   2 TIMES .                                 00003280
             03 FILLER       PIC X(2).                                  00003290
             03 DEFFETA      PIC X.                                     00003300
             03 DEFFETC PIC X.                                          00003310
             03 DEFFETP PIC X.                                          00003320
             03 DEFFETH PIC X.                                          00003330
             03 DEFFETV PIC X.                                          00003340
             03 DEFFETU PIC X.                                          00003350
             03 DEFFETO      PIC X(8).                                  00003360
           02 DFHMS4 OCCURS   2 TIMES .                                 00003370
             03 FILLER       PIC X(2).                                  00003380
             03 DFINEFFEA    PIC X.                                     00003390
             03 DFINEFFEC    PIC X.                                     00003400
             03 DFINEFFEP    PIC X.                                     00003410
             03 DFINEFFEH    PIC X.                                     00003420
             03 DFINEFFEV    PIC X.                                     00003430
             03 DFINEFFEU    PIC X.                                     00003440
             03 DFINEFFEO    PIC X(8).                                  00003450
           02 DFHMS5 OCCURS   2 TIMES .                                 00003460
             03 FILLER       PIC X(2).                                  00003470
             03 MOPARENTA    PIC X.                                     00003480
             03 MOPARENTC    PIC X.                                     00003490
             03 MOPARENTP    PIC X.                                     00003500
             03 MOPARENTH    PIC X.                                     00003510
             03 MOPARENTV    PIC X.                                     00003520
             03 MOPARENTU    PIC X.                                     00003530
             03 MOPARENTO    PIC X.                                     00003540
           02 DFHMS6 OCCURS   2 TIMES .                                 00003550
             03 FILLER       PIC X(2).                                  00003560
             03 LCOMMENTA    PIC X.                                     00003570
             03 LCOMMENTC    PIC X.                                     00003580
             03 LCOMMENTP    PIC X.                                     00003590
             03 LCOMMENTH    PIC X.                                     00003600
             03 LCOMMENTV    PIC X.                                     00003610
             03 LCOMMENTU    PIC X.                                     00003620
             03 LCOMMENTO    PIC X(10).                                 00003630
           02 DFHMS7 OCCURS   2 TIMES .                                 00003640
             03 FILLER       PIC X(2).                                  00003650
             03 MFPARENTA    PIC X.                                     00003660
             03 MFPARENTC    PIC X.                                     00003670
             03 MFPARENTP    PIC X.                                     00003680
             03 MFPARENTH    PIC X.                                     00003690
             03 MFPARENTV    PIC X.                                     00003700
             03 MFPARENTU    PIC X.                                     00003710
             03 MFPARENTO    PIC X.                                     00003720
           02 FILLER    PIC X(2).                                       00003730
           02 LIBF10F11A     PIC X.                                     00003740
           02 LIBF10F11C     PIC X.                                     00003750
           02 LIBF10F11P     PIC X.                                     00003760
           02 LIBF10F11H     PIC X.                                     00003770
           02 LIBF10F11V     PIC X.                                     00003780
           02 LIBF10F11U     PIC X.                                     00003790
           02 LIBF10F11O     PIC X(33).                                 00003800
           02 FILLER    PIC X(2).                                       00003810
           02 MSOCA     PIC X.                                          00003820
           02 MSOCC     PIC X.                                          00003830
           02 MSOCP     PIC X.                                          00003840
           02 MSOCH     PIC X.                                          00003850
           02 MSOCV     PIC X.                                          00003860
           02 MSOCU     PIC X.                                          00003870
           02 MSOCO     PIC X(3).                                       00003880
           02 FILLER    PIC X(2).                                       00003890
           02 MZPA      PIC X.                                          00003900
           02 MZPC      PIC X.                                          00003910
           02 MZPP      PIC X.                                          00003920
           02 MZPH      PIC X.                                          00003930
           02 MZPV      PIC X.                                          00003940
           02 MZPU      PIC X.                                          00003950
           02 MZPO      PIC X(2).                                       00003960
           02 FILLER    PIC X(2).                                       00003970
           02 MLIEUA    PIC X.                                          00003980
           02 MLIEUC    PIC X.                                          00003990
           02 MLIEUP    PIC X.                                          00004000
           02 MLIEUH    PIC X.                                          00004010
           02 MLIEUV    PIC X.                                          00004020
           02 MLIEUU    PIC X.                                          00004030
           02 MLIEUO    PIC X(3).                                       00004040
           02 FILLER    PIC X(2).                                       00004050
           02 MDEROA    PIC X.                                          00004060
           02 MDEROC    PIC X.                                          00004070
           02 MDEROP    PIC X.                                          00004080
           02 MDEROH    PIC X.                                          00004090
           02 MDEROV    PIC X.                                          00004100
           02 MDEROU    PIC X.                                          00004110
           02 MDEROO    PIC X(4).                                       00004120
           02 FILLER    PIC X(2).                                       00004130
           02 DDEFFETDA      PIC X.                                     00004140
           02 DDEFFETDC PIC X.                                          00004150
           02 DDEFFETDP PIC X.                                          00004160
           02 DDEFFETDH PIC X.                                          00004170
           02 DDEFFETDV PIC X.                                          00004180
           02 DDEFFETDU PIC X.                                          00004190
           02 DDEFFETDO      PIC X(8).                                  00004200
           02 FILLER    PIC X(2).                                       00004210
           02 DFEFFETDA      PIC X.                                     00004220
           02 DFEFFETDC PIC X.                                          00004230
           02 DFEFFETDP PIC X.                                          00004240
           02 DFEFFETDH PIC X.                                          00004250
           02 DFEFFETDV PIC X.                                          00004260
           02 DFEFFETDU PIC X.                                          00004270
           02 DFEFFETDO      PIC X(8).                                  00004280
           02 DFHMS8 OCCURS   10 TIMES .                                00004290
             03 FILLER       PIC X(2).                                  00004300
             03 MACTIONA     PIC X.                                     00004310
             03 MACTIONC     PIC X.                                     00004320
             03 MACTIONP     PIC X.                                     00004330
             03 MACTIONH     PIC X.                                     00004340
             03 MACTIONV     PIC X.                                     00004350
             03 MACTIONU     PIC X.                                     00004360
             03 MACTIONO     PIC X.                                     00004370
           02 DFHMS9 OCCURS   10 TIMES .                                00004380
             03 FILLER       PIC X(2).                                  00004390
             03 MSOCIETEA    PIC X.                                     00004400
             03 MSOCIETEC    PIC X.                                     00004410
             03 MSOCIETEP    PIC X.                                     00004420
             03 MSOCIETEH    PIC X.                                     00004430
             03 MSOCIETEV    PIC X.                                     00004440
             03 MSOCIETEU    PIC X.                                     00004450
             03 MSOCIETEO    PIC X(3).                                  00004460
           02 DFHMS10 OCCURS   10 TIMES .                               00004470
             03 FILLER       PIC X(2).                                  00004480
             03 MZONPRIXA    PIC X.                                     00004490
             03 MZONPRIXC    PIC X.                                     00004500
             03 MZONPRIXP    PIC X.                                     00004510
             03 MZONPRIXH    PIC X.                                     00004520
             03 MZONPRIXV    PIC X.                                     00004530
             03 MZONPRIXU    PIC X.                                     00004540
             03 MZONPRIXO    PIC X(2).                                  00004550
           02 DFHMS11 OCCURS   10 TIMES .                               00004560
             03 FILLER       PIC X(2).                                  00004570
             03 MNLIEUA      PIC X.                                     00004580
             03 MNLIEUC PIC X.                                          00004590
             03 MNLIEUP PIC X.                                          00004600
             03 MNLIEUH PIC X.                                          00004610
             03 MNLIEUV PIC X.                                          00004620
             03 MNLIEUU PIC X.                                          00004630
             03 MNLIEUO      PIC X(3).                                  00004640
           02 DFHMS12 OCCURS   10 TIMES .                               00004650
             03 FILLER       PIC X(2).                                  00004660
             03 MLREFA  PIC X.                                          00004670
             03 MLREFC  PIC X.                                          00004680
             03 MLREFP  PIC X.                                          00004690
             03 MLREFH  PIC X.                                          00004700
             03 MLREFV  PIC X.                                          00004710
             03 MLREFU  PIC X.                                          00004720
             03 MLREFO  PIC X(4).                                       00004730
           02 DFHMS13 OCCURS   10 TIMES .                               00004740
             03 FILLER       PIC X(2).                                  00004750
             03 DDEFFETRA    PIC X.                                     00004760
             03 DDEFFETRC    PIC X.                                     00004770
             03 DDEFFETRP    PIC X.                                     00004780
             03 DDEFFETRH    PIC X.                                     00004790
             03 DDEFFETRV    PIC X.                                     00004800
             03 DDEFFETRU    PIC X.                                     00004810
             03 DDEFFETRO    PIC X(8).                                  00004820
           02 DFHMS14 OCCURS   10 TIMES .                               00004830
             03 FILLER       PIC X(2).                                  00004840
             03 DFEFFETRA    PIC X.                                     00004850
             03 DFEFFETRC    PIC X.                                     00004860
             03 DFEFFETRP    PIC X.                                     00004870
             03 DFEFFETRH    PIC X.                                     00004880
             03 DFEFFETRV    PIC X.                                     00004890
             03 DFEFFETRU    PIC X.                                     00004900
             03 DFEFFETRO    PIC X(8).                                  00004910
           02 DFHMS15 OCCURS   10 TIMES .                               00004920
             03 FILLER       PIC X(2).                                  00004930
             03 MPSTDTTCA    PIC X.                                     00004940
             03 MPSTDTTCC    PIC X.                                     00004950
             03 MPSTDTTCP    PIC X.                                     00004960
             03 MPSTDTTCH    PIC X.                                     00004970
             03 MPSTDTTCV    PIC X.                                     00004980
             03 MPSTDTTCU    PIC X.                                     00004990
             03 MPSTDTTCO    PIC X(8).                                  00005000
           02 DFHMS16 OCCURS   10 TIMES .                               00005010
             03 FILLER       PIC X(2).                                  00005020
             03 MCORIGA      PIC X.                                     00005030
             03 MCORIGC PIC X.                                          00005040
             03 MCORIGP PIC X.                                          00005050
             03 MCORIGH PIC X.                                          00005060
             03 MCORIGV PIC X.                                          00005070
             03 MCORIGU PIC X.                                          00005080
             03 MCORIGO      PIC X.                                     00005090
           02 DFHMS17 OCCURS   10 TIMES .                               00005100
             03 FILLER       PIC X(2).                                  00005110
             03 DDEFFETSA    PIC X.                                     00005120
             03 DDEFFETSC    PIC X.                                     00005130
             03 DDEFFETSP    PIC X.                                     00005140
             03 DDEFFETSH    PIC X.                                     00005150
             03 DDEFFETSV    PIC X.                                     00005160
             03 DDEFFETSU    PIC X.                                     00005170
             03 DDEFFETSO    PIC X(8).                                  00005180
           02 DFHMS18 OCCURS   10 TIMES .                               00005190
             03 FILLER       PIC X(2).                                  00005200
             03 DFEFFETSA    PIC X.                                     00005210
             03 DFEFFETSC    PIC X.                                     00005220
             03 DFEFFETSP    PIC X.                                     00005230
             03 DFEFFETSH    PIC X.                                     00005240
             03 DFEFFETSV    PIC X.                                     00005250
             03 DFEFFETSU    PIC X.                                     00005260
             03 DFEFFETSO    PIC X(8).                                  00005270
           02 DFHMS19 OCCURS   10 TIMES .                               00005280
             03 FILLER       PIC X(2).                                  00005290
             03 MMARGEA      PIC X.                                     00005300
             03 MMARGEC PIC X.                                          00005310
             03 MMARGEP PIC X.                                          00005320
             03 MMARGEH PIC X.                                          00005330
             03 MMARGEV PIC X.                                          00005340
             03 MMARGEU PIC X.                                          00005350
             03 MMARGEO      PIC X(7).                                  00005360
           02 FILLER    PIC X(2).                                       00005370
           02 MLIBERRA  PIC X.                                          00005380
           02 MLIBERRC  PIC X.                                          00005390
           02 MLIBERRP  PIC X.                                          00005400
           02 MLIBERRH  PIC X.                                          00005410
           02 MLIBERRV  PIC X.                                          00005420
           02 MLIBERRU  PIC X.                                          00005430
           02 MLIBERRO  PIC X(78).                                      00005440
           02 FILLER    PIC X(2).                                       00005450
           02 MCODTRAA  PIC X.                                          00005460
           02 MCODTRAC  PIC X.                                          00005470
           02 MCODTRAP  PIC X.                                          00005480
           02 MCODTRAH  PIC X.                                          00005490
           02 MCODTRAV  PIC X.                                          00005500
           02 MCODTRAU  PIC X.                                          00005510
           02 MCODTRAO  PIC X(4).                                       00005520
           02 FILLER    PIC X(2).                                       00005530
           02 MCICSA    PIC X.                                          00005540
           02 MCICSC    PIC X.                                          00005550
           02 MCICSP    PIC X.                                          00005560
           02 MCICSH    PIC X.                                          00005570
           02 MCICSV    PIC X.                                          00005580
           02 MCICSU    PIC X.                                          00005590
           02 MCICSO    PIC X(5).                                       00005600
           02 FILLER    PIC X(2).                                       00005610
           02 MNETNAMA  PIC X.                                          00005620
           02 MNETNAMC  PIC X.                                          00005630
           02 MNETNAMP  PIC X.                                          00005640
           02 MNETNAMH  PIC X.                                          00005650
           02 MNETNAMV  PIC X.                                          00005660
           02 MNETNAMU  PIC X.                                          00005670
           02 MNETNAMO  PIC X(8).                                       00005680
           02 FILLER    PIC X(2).                                       00005690
           02 MSCREENA  PIC X.                                          00005700
           02 MSCREENC  PIC X.                                          00005710
           02 MSCREENP  PIC X.                                          00005720
           02 MSCREENH  PIC X.                                          00005730
           02 MSCREENV  PIC X.                                          00005740
           02 MSCREENU  PIC X.                                          00005750
           02 MSCREENO  PIC X(4).                                       00005760
                                                                                
