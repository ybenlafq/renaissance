      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RVTH0900                           *        
      ******************************************************************        
       01  RVTH0900.                                                            
      *    *************************************************************        
      *                       COPCO                                             
           10 TH09-COPCO           PIC X(3).                                    
      *    *************************************************************        
      *                       COPCO                                             
           10 TH09-CCOMPOSANTE     PIC X(5).                                    
      *    *************************************************************        
      *                       DEFFET                                            
           10 TH09-DEFFET          PIC X(8).                                    
      *    *************************************************************        
      *                       WACTIF                                            
           10 TH09-WACTIF          PIC X(1).                                    
      *    *************************************************************        
      *                       WINCLUSCALC                                       
           10 TH09-WINCLUSCALC     PIC X(1).                                    
      *    *************************************************************        
      *                       IDREGLE                                           
           10 TH09-IDREGLE         PIC X(10).                                   
      *    *************************************************************        
      *                       LIBREGLE                                          
           10 TH09-LIBREGLE        PIC X(20).                                   
      *    *************************************************************        
      *                       WSVAL                                             
           10 TH09-WSVAL           PIC X(1).                                    
      *    *************************************************************        
      *                       WSPOURCENT                                        
           10 TH09-WSPOURCENT      PIC X(1).                                    
      *    *************************************************************        
      *                       MTTREF                                            
           10 TH09-MTTREF          PIC X(5).                                    
      *    *************************************************************        
      *                       WBVAL                                             
           10 TH09-WBVAL           PIC X(1).                                    
      *    *************************************************************        
      *                       WBPOURCENT                                        
           10 TH09-WBPOURCENT      PIC X(1).                                    
      *    *************************************************************        
      *                       WDELTA                                            
           10 TH09-WDELTA          PIC X(1).                                    
      *    *************************************************************        
      *                       DMAJSYST                                          
           10 TH09-DMAJSYST        PIC X(26).                                   
      *                                                                         
      ******************************************************************        
      *   LISTE DES FLAGS DE LA TABLE RVTH0900                                  
      ******************************************************************        
       01  RVTH0900-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH09-COPCO-F             PIC S9(4) COMP.                         
      *--                                                                       
           10  TH09-COPCO-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH09-CCOMPOSANTE-F       PIC S9(4) COMP.                         
      *--                                                                       
           10  TH09-CCOMPOSANTE-F       PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH09-DEFFET-F            PIC S9(4) COMP.                         
      *--                                                                       
           10  TH09-DEFFET-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH09-WACTIF-F            PIC S9(4) COMP.                         
      *--                                                                       
           10  TH09-WACTIF-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH09-WINCLUSCALC-F       PIC S9(4) COMP.                         
      *--                                                                       
           10  TH09-WINCLUSCALC-F       PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH09-IDREGLE-F           PIC S9(4) COMP.                         
      *--                                                                       
           10  TH09-IDREGLE-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH09-LIBREGLE-F          PIC S9(4) COMP.                         
      *--                                                                       
           10  TH09-LIBREGLE-F          PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH09-WSVAL-F             PIC S9(4) COMP.                         
      *--                                                                       
           10  TH09-WSVAL-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH09-WSPOURCENT-F        PIC S9(4) COMP.                         
      *--                                                                       
           10  TH09-WSPOURCENT-F        PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH09-MTTREF-F            PIC S9(4) COMP.                         
      *--                                                                       
           10  TH09-MTTREF-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH09-WBVAL-F             PIC S9(4) COMP.                         
      *--                                                                       
           10  TH09-WBVAL-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH09-WBPOURCENT-F        PIC S9(4) COMP.                         
      *--                                                                       
           10  TH09-WBPOURCENT-F        PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH09-WDELTA-F            PIC S9(4) COMP.                         
      *--                                                                       
           10  TH09-WDELTA-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH09-DMAJSYST-F          PIC S9(4) COMP.                         
      *--                                                                       
           10  TH09-DMAJSYST-F          PIC S9(4) COMP-5.                       
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 14      *        
      ******************************************************************        
                                                                                
