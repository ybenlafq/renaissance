      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PRDEG PROFIL DELIVRANCE GENERALITES    *        
      *----------------------------------------------------------------*        
       01  RVGA01GB.                                                            
           05  PRDEG-CTABLEG2    PIC X(15).                                     
           05  PRDEG-CTABLEG2-REDEF REDEFINES PRDEG-CTABLEG2.                   
               10  PRDEG-CPRODEL         PIC X(05).                             
           05  PRDEG-WTABLEG     PIC X(80).                                     
           05  PRDEG-WTABLEG-REDEF  REDEFINES PRDEG-WTABLEG.                    
               10  PRDEG-LPRODEL         PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01GB-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRDEG-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PRDEG-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRDEG-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PRDEG-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
