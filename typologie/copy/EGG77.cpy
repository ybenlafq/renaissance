      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * CONSULT. COMPOS. GROUPE/ELEMENT                                 00000020
      ***************************************************************** 00000030
       01   EGG77I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(5).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(5).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(5).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(5).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFFETCL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MDEFFETCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDEFFETCF      PIC X.                                     00000230
           02 FILLER    PIC X(5).                                       00000240
           02 MDEFFETCI      PIC X(8).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEPFL     COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MEPFL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MEPFF     PIC X.                                          00000270
           02 FILLER    PIC X(5).                                       00000280
           02 MEPFI     PIC X.                                          00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBZPL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLIBZPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIBZPF   PIC X.                                          00000310
           02 FILLER    PIC X(5).                                       00000320
           02 MLIBZPI   PIC X(7).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNZONPRIXL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNZONPRIXL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNZONPRIXF     PIC X.                                     00000350
           02 FILLER    PIC X(5).                                       00000360
           02 MNZONPRIXI     PIC X(2).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBNLIEUL     COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLIBNLIEUL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIBNLIEUF     PIC X.                                     00000390
           02 FILLER    PIC X(5).                                       00000400
           02 MLIBNLIEUI     PIC X(20).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000430
           02 FILLER    PIC X(5).                                       00000440
           02 MNLIEUI   PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000470
           02 FILLER    PIC X(5).                                       00000480
           02 MNCODICI  PIC X(7).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOUL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MLREFFOUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLREFFOUF      PIC X.                                     00000510
           02 FILLER    PIC X(5).                                       00000520
           02 MLREFFOUI      PIC X(20).                                 00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000550
           02 FILLER    PIC X(5).                                       00000560
           02 MCMARQI   PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000590
           02 FILLER    PIC X(5).                                       00000600
           02 MCFAMI    PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATCPL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MSTATCPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTATCPF  PIC X.                                          00000630
           02 FILLER    PIC X(5).                                       00000640
           02 MSTATCPI  PIC X(3).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFSTATL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MDEFSTATL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDEFSTATF      PIC X.                                     00000670
           02 FILLER    PIC X(5).                                       00000680
           02 MDEFSTATI      PIC X(8).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBVARL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MLIBVARL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBVARF  PIC X.                                          00000710
           02 FILLER    PIC X(5).                                       00000720
           02 MLIBVARI  PIC X(4).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBVAR1L      COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MLIBVAR1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBVAR1F      PIC X.                                     00000750
           02 FILLER    PIC X(5).                                       00000760
           02 MLIBVAR1I      PIC X(21).                                 00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBVAR2L      COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MLIBVAR2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBVAR2F      PIC X.                                     00000790
           02 FILLER    PIC X(5).                                       00000800
           02 MLIBVAR2I      PIC X(20).                                 00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBVAR3L      COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MLIBVAR3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBVAR3F      PIC X.                                     00000830
           02 FILLER    PIC X(5).                                       00000840
           02 MLIBVAR3I      PIC X(8).                                  00000850
           02 MCFAMGD OCCURS   15 TIMES .                               00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAMGL      COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MCFAMGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCFAMGF      PIC X.                                     00000880
             03 FILLER  PIC X(5).                                       00000890
             03 MCFAMGI      PIC X(5).                                  00000900
           02 MNCODICGD OCCURS   15 TIMES .                             00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICGL    COMP PIC S9(4).                            00000920
      *--                                                                       
             03 MNCODICGL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNCODICGF    PIC X.                                     00000930
             03 FILLER  PIC X(5).                                       00000940
             03 MNCODICGI    PIC X(7).                                  00000950
           02 MVAR1D OCCURS   15 TIMES .                                00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVAR1L  COMP PIC S9(4).                                 00000970
      *--                                                                       
             03 MVAR1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MVAR1F  PIC X.                                          00000980
             03 FILLER  PIC X(5).                                       00000990
             03 MVAR1I  PIC X(21).                                      00001000
           02 MNCODICED OCCURS   15 TIMES .                             00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICEL    COMP PIC S9(4).                            00001020
      *--                                                                       
             03 MNCODICEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNCODICEF    PIC X.                                     00001030
             03 FILLER  PIC X(5).                                       00001040
             03 MNCODICEI    PIC X(7).                                  00001050
           02 MCFAMED OCCURS   15 TIMES .                               00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAMEL      COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MCFAMEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCFAMEF      PIC X.                                     00001080
             03 FILLER  PIC X(5).                                       00001090
             03 MCFAMEI      PIC X(5).                                  00001100
           02 MVAR2D OCCURS   15 TIMES .                                00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVAR2L  COMP PIC S9(4).                                 00001120
      *--                                                                       
             03 MVAR2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MVAR2F  PIC X.                                          00001130
             03 FILLER  PIC X(5).                                       00001140
             03 MVAR2I  PIC X(21).                                      00001150
           02 MVAR3D OCCURS   15 TIMES .                                00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVAR3L  COMP PIC S9(4).                                 00001170
      *--                                                                       
             03 MVAR3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MVAR3F  PIC X.                                          00001180
             03 FILLER  PIC X(5).                                       00001190
             03 MVAR3I  PIC X(7).                                       00001200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001210
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001220
           02 FILLER    PIC X(5).                                       00001230
           02 MLIBERRI  PIC X(78).                                      00001240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001250
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001260
           02 FILLER    PIC X(5).                                       00001270
           02 MCODTRAI  PIC X(4).                                       00001280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001290
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001300
           02 FILLER    PIC X(5).                                       00001310
           02 MCICSI    PIC X(5).                                       00001320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001330
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001340
           02 FILLER    PIC X(5).                                       00001350
           02 MNETNAMI  PIC X(8).                                       00001360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001370
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001380
           02 FILLER    PIC X(5).                                       00001390
           02 MSCREENI  PIC X(4).                                       00001400
      ***************************************************************** 00001410
      * CONSULT. COMPOS. GROUPE/ELEMENT                                 00001420
      ***************************************************************** 00001430
       01   EGG77O REDEFINES EGG77I.                                    00001440
           02 FILLER    PIC X(12).                                      00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MDATJOUA  PIC X.                                          00001470
           02 MDATJOUC  PIC X.                                          00001480
           02 MDATJOUP  PIC X.                                          00001490
           02 MDATJOUH  PIC X.                                          00001500
           02 MDATJOUV  PIC X.                                          00001510
           02 MDATJOUU  PIC X.                                          00001520
           02 MDATJOUO  PIC X(10).                                      00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MTIMJOUA  PIC X.                                          00001550
           02 MTIMJOUC  PIC X.                                          00001560
           02 MTIMJOUP  PIC X.                                          00001570
           02 MTIMJOUH  PIC X.                                          00001580
           02 MTIMJOUV  PIC X.                                          00001590
           02 MTIMJOUU  PIC X.                                          00001600
           02 MTIMJOUO  PIC X(5).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MPAGEA    PIC X.                                          00001630
           02 MPAGEC    PIC X.                                          00001640
           02 MPAGEP    PIC X.                                          00001650
           02 MPAGEH    PIC X.                                          00001660
           02 MPAGEV    PIC X.                                          00001670
           02 MPAGEU    PIC X.                                          00001680
           02 MPAGEO    PIC X(3).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MPAGEMAXA      PIC X.                                     00001710
           02 MPAGEMAXC PIC X.                                          00001720
           02 MPAGEMAXP PIC X.                                          00001730
           02 MPAGEMAXH PIC X.                                          00001740
           02 MPAGEMAXV PIC X.                                          00001750
           02 MPAGEMAXU PIC X.                                          00001760
           02 MPAGEMAXO      PIC X(3).                                  00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MDEFFETCA      PIC X.                                     00001790
           02 MDEFFETCC PIC X.                                          00001800
           02 MDEFFETCP PIC X.                                          00001810
           02 MDEFFETCH PIC X.                                          00001820
           02 MDEFFETCV PIC X.                                          00001830
           02 MDEFFETCU PIC X.                                          00001840
           02 MDEFFETCO      PIC X(8).                                  00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MEPFA     PIC X.                                          00001870
           02 MEPFC     PIC X.                                          00001880
           02 MEPFP     PIC X.                                          00001890
           02 MEPFH     PIC X.                                          00001900
           02 MEPFV     PIC X.                                          00001910
           02 MEPFU     PIC X.                                          00001920
           02 MEPFO     PIC X.                                          00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MLIBZPA   PIC X.                                          00001950
           02 MLIBZPC   PIC X.                                          00001960
           02 MLIBZPP   PIC X.                                          00001970
           02 MLIBZPH   PIC X.                                          00001980
           02 MLIBZPV   PIC X.                                          00001990
           02 MLIBZPU   PIC X.                                          00002000
           02 MLIBZPO   PIC X(7).                                       00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MNZONPRIXA     PIC X.                                     00002030
           02 MNZONPRIXC     PIC X.                                     00002040
           02 MNZONPRIXP     PIC X.                                     00002050
           02 MNZONPRIXH     PIC X.                                     00002060
           02 MNZONPRIXV     PIC X.                                     00002070
           02 MNZONPRIXU     PIC X.                                     00002080
           02 MNZONPRIXO     PIC X(2).                                  00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MLIBNLIEUA     PIC X.                                     00002110
           02 MLIBNLIEUC     PIC X.                                     00002120
           02 MLIBNLIEUP     PIC X.                                     00002130
           02 MLIBNLIEUH     PIC X.                                     00002140
           02 MLIBNLIEUV     PIC X.                                     00002150
           02 MLIBNLIEUU     PIC X.                                     00002160
           02 MLIBNLIEUO     PIC X(20).                                 00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MNLIEUA   PIC X.                                          00002190
           02 MNLIEUC   PIC X.                                          00002200
           02 MNLIEUP   PIC X.                                          00002210
           02 MNLIEUH   PIC X.                                          00002220
           02 MNLIEUV   PIC X.                                          00002230
           02 MNLIEUU   PIC X.                                          00002240
           02 MNLIEUO   PIC X(3).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MNCODICA  PIC X.                                          00002270
           02 MNCODICC  PIC X.                                          00002280
           02 MNCODICP  PIC X.                                          00002290
           02 MNCODICH  PIC X.                                          00002300
           02 MNCODICV  PIC X.                                          00002310
           02 MNCODICU  PIC X.                                          00002320
           02 MNCODICO  PIC X(7).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MLREFFOUA      PIC X.                                     00002350
           02 MLREFFOUC PIC X.                                          00002360
           02 MLREFFOUP PIC X.                                          00002370
           02 MLREFFOUH PIC X.                                          00002380
           02 MLREFFOUV PIC X.                                          00002390
           02 MLREFFOUU PIC X.                                          00002400
           02 MLREFFOUO      PIC X(20).                                 00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MCMARQA   PIC X.                                          00002430
           02 MCMARQC   PIC X.                                          00002440
           02 MCMARQP   PIC X.                                          00002450
           02 MCMARQH   PIC X.                                          00002460
           02 MCMARQV   PIC X.                                          00002470
           02 MCMARQU   PIC X.                                          00002480
           02 MCMARQO   PIC X(5).                                       00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MCFAMA    PIC X.                                          00002510
           02 MCFAMC    PIC X.                                          00002520
           02 MCFAMP    PIC X.                                          00002530
           02 MCFAMH    PIC X.                                          00002540
           02 MCFAMV    PIC X.                                          00002550
           02 MCFAMU    PIC X.                                          00002560
           02 MCFAMO    PIC X(5).                                       00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MSTATCPA  PIC X.                                          00002590
           02 MSTATCPC  PIC X.                                          00002600
           02 MSTATCPP  PIC X.                                          00002610
           02 MSTATCPH  PIC X.                                          00002620
           02 MSTATCPV  PIC X.                                          00002630
           02 MSTATCPU  PIC X.                                          00002640
           02 MSTATCPO  PIC X(3).                                       00002650
           02 FILLER    PIC X(2).                                       00002660
           02 MDEFSTATA      PIC X.                                     00002670
           02 MDEFSTATC PIC X.                                          00002680
           02 MDEFSTATP PIC X.                                          00002690
           02 MDEFSTATH PIC X.                                          00002700
           02 MDEFSTATV PIC X.                                          00002710
           02 MDEFSTATU PIC X.                                          00002720
           02 MDEFSTATO      PIC X(8).                                  00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MLIBVARA  PIC X.                                          00002750
           02 MLIBVARC  PIC X.                                          00002760
           02 MLIBVARP  PIC X.                                          00002770
           02 MLIBVARH  PIC X.                                          00002780
           02 MLIBVARV  PIC X.                                          00002790
           02 MLIBVARU  PIC X.                                          00002800
           02 MLIBVARO  PIC X(4).                                       00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MLIBVAR1A      PIC X.                                     00002830
           02 MLIBVAR1C PIC X.                                          00002840
           02 MLIBVAR1P PIC X.                                          00002850
           02 MLIBVAR1H PIC X.                                          00002860
           02 MLIBVAR1V PIC X.                                          00002870
           02 MLIBVAR1U PIC X.                                          00002880
           02 MLIBVAR1O      PIC X(21).                                 00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MLIBVAR2A      PIC X.                                     00002910
           02 MLIBVAR2C PIC X.                                          00002920
           02 MLIBVAR2P PIC X.                                          00002930
           02 MLIBVAR2H PIC X.                                          00002940
           02 MLIBVAR2V PIC X.                                          00002950
           02 MLIBVAR2U PIC X.                                          00002960
           02 MLIBVAR2O      PIC X(20).                                 00002970
           02 FILLER    PIC X(2).                                       00002980
           02 MLIBVAR3A      PIC X.                                     00002990
           02 MLIBVAR3C PIC X.                                          00003000
           02 MLIBVAR3P PIC X.                                          00003010
           02 MLIBVAR3H PIC X.                                          00003020
           02 MLIBVAR3V PIC X.                                          00003030
           02 MLIBVAR3U PIC X.                                          00003040
           02 MLIBVAR3O      PIC X(8).                                  00003050
           02 DFHMS1 OCCURS   15 TIMES .                                00003060
             03 FILLER       PIC X(2).                                  00003070
             03 MCFAMGA      PIC X.                                     00003080
             03 MCFAMGC PIC X.                                          00003090
             03 MCFAMGP PIC X.                                          00003100
             03 MCFAMGH PIC X.                                          00003110
             03 MCFAMGV PIC X.                                          00003120
             03 MCFAMGU PIC X.                                          00003130
             03 MCFAMGO      PIC X(5).                                  00003140
           02 DFHMS2 OCCURS   15 TIMES .                                00003150
             03 FILLER       PIC X(2).                                  00003160
             03 MNCODICGA    PIC X.                                     00003170
             03 MNCODICGC    PIC X.                                     00003180
             03 MNCODICGP    PIC X.                                     00003190
             03 MNCODICGH    PIC X.                                     00003200
             03 MNCODICGV    PIC X.                                     00003210
             03 MNCODICGU    PIC X.                                     00003220
             03 MNCODICGO    PIC X(7).                                  00003230
           02 DFHMS3 OCCURS   15 TIMES .                                00003240
             03 FILLER       PIC X(2).                                  00003250
             03 MVAR1A  PIC X.                                          00003260
             03 MVAR1C  PIC X.                                          00003270
             03 MVAR1P  PIC X.                                          00003280
             03 MVAR1H  PIC X.                                          00003290
             03 MVAR1V  PIC X.                                          00003300
             03 MVAR1U  PIC X.                                          00003310
             03 MVAR1O  PIC X(21).                                      00003320
           02 DFHMS4 OCCURS   15 TIMES .                                00003330
             03 FILLER       PIC X(2).                                  00003340
             03 MNCODICEA    PIC X.                                     00003350
             03 MNCODICEC    PIC X.                                     00003360
             03 MNCODICEP    PIC X.                                     00003370
             03 MNCODICEH    PIC X.                                     00003380
             03 MNCODICEV    PIC X.                                     00003390
             03 MNCODICEU    PIC X.                                     00003400
             03 MNCODICEO    PIC X(7).                                  00003410
           02 DFHMS5 OCCURS   15 TIMES .                                00003420
             03 FILLER       PIC X(2).                                  00003430
             03 MCFAMEA      PIC X.                                     00003440
             03 MCFAMEC PIC X.                                          00003450
             03 MCFAMEP PIC X.                                          00003460
             03 MCFAMEH PIC X.                                          00003470
             03 MCFAMEV PIC X.                                          00003480
             03 MCFAMEU PIC X.                                          00003490
             03 MCFAMEO      PIC X(5).                                  00003500
           02 DFHMS6 OCCURS   15 TIMES .                                00003510
             03 FILLER       PIC X(2).                                  00003520
             03 MVAR2A  PIC X.                                          00003530
             03 MVAR2C  PIC X.                                          00003540
             03 MVAR2P  PIC X.                                          00003550
             03 MVAR2H  PIC X.                                          00003560
             03 MVAR2V  PIC X.                                          00003570
             03 MVAR2U  PIC X.                                          00003580
             03 MVAR2O  PIC X(21).                                      00003590
           02 DFHMS7 OCCURS   15 TIMES .                                00003600
             03 FILLER       PIC X(2).                                  00003610
             03 MVAR3A  PIC X.                                          00003620
             03 MVAR3C  PIC X.                                          00003630
             03 MVAR3P  PIC X.                                          00003640
             03 MVAR3H  PIC X.                                          00003650
             03 MVAR3V  PIC X.                                          00003660
             03 MVAR3U  PIC X.                                          00003670
             03 MVAR3O  PIC X(7).                                       00003680
           02 FILLER    PIC X(2).                                       00003690
           02 MLIBERRA  PIC X.                                          00003700
           02 MLIBERRC  PIC X.                                          00003710
           02 MLIBERRP  PIC X.                                          00003720
           02 MLIBERRH  PIC X.                                          00003730
           02 MLIBERRV  PIC X.                                          00003740
           02 MLIBERRU  PIC X.                                          00003750
           02 MLIBERRO  PIC X(78).                                      00003760
           02 FILLER    PIC X(2).                                       00003770
           02 MCODTRAA  PIC X.                                          00003780
           02 MCODTRAC  PIC X.                                          00003790
           02 MCODTRAP  PIC X.                                          00003800
           02 MCODTRAH  PIC X.                                          00003810
           02 MCODTRAV  PIC X.                                          00003820
           02 MCODTRAU  PIC X.                                          00003830
           02 MCODTRAO  PIC X(4).                                       00003840
           02 FILLER    PIC X(2).                                       00003850
           02 MCICSA    PIC X.                                          00003860
           02 MCICSC    PIC X.                                          00003870
           02 MCICSP    PIC X.                                          00003880
           02 MCICSH    PIC X.                                          00003890
           02 MCICSV    PIC X.                                          00003900
           02 MCICSU    PIC X.                                          00003910
           02 MCICSO    PIC X(5).                                       00003920
           02 FILLER    PIC X(2).                                       00003930
           02 MNETNAMA  PIC X.                                          00003940
           02 MNETNAMC  PIC X.                                          00003950
           02 MNETNAMP  PIC X.                                          00003960
           02 MNETNAMH  PIC X.                                          00003970
           02 MNETNAMV  PIC X.                                          00003980
           02 MNETNAMU  PIC X.                                          00003990
           02 MNETNAMO  PIC X(8).                                       00004000
           02 FILLER    PIC X(2).                                       00004010
           02 MSCREENA  PIC X.                                          00004020
           02 MSCREENC  PIC X.                                          00004030
           02 MSCREENP  PIC X.                                          00004040
           02 MSCREENH  PIC X.                                          00004050
           02 MSCREENV  PIC X.                                          00004060
           02 MSCREENU  PIC X.                                          00004070
           02 MSCREENO  PIC X(4).                                       00004080
                                                                                
