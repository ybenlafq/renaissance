      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
                                                                                
      ******************************************************************        
      * ITSSC                                                                   
      * COMMUNICATION GESTION PRIX PRIMES                                       
E0265 ******************************************************************        
      * DE02005 04/04/06 CODE INITIAL                                           
E0452 ******************************************************************        
      * DE02005 26/09/08 SUPPORT EVOLUTION D004238                              
      *                  APPEL A LA CARTE                                       
C0486 ******************************************************************        
      * DE02005 07/05/09 SUPPORT MAINTENANCE                                    
      *                  DEROGATIONS A LA SOCIETE                               
E0499 ******************************************************************        
      * DE02005 01/10/09 SUPPORT EVOLUTION D004416-17                           
      *                  PRIX ET PRIME AUTO CODIC GROUPE                        
      ******************************************************************        
E0619 * DE02001 09/03/12 SUPPORT EVOLUTION                                      
      *                  AJOUT DU DROIT UTILISATEUR POUR METTRE A JOUR          
      *                  UN PRIX EN-DESSOUS DU SEUIL                            
      ******************************************************************        
E0622 * DE02001 04/04/12 SUPPORT EVOLUTION                                      
      *                  CREATION D'UN PRIX NATIONAL OU LOCAL SUR TOUTES        
      *                  LES ZONES, APPLICABLE LE JOUR J                        
      ******************************************************************        
     Z* DE02005 20/06/14 ANNULATION E0619 ET E0622                              
      ******************************************************************        
      * COMMAREA                                                                
      * 1024                                                                    
       05 CS-CB-DATA.                                                           
E0398 **   0130                                                                 
E0398 *    0140                                                                 
C0486 *    0143                                                                 
           10 CS-CB-GESTION.                                                    
      *        0086                                                             
               15 CS-CB-STATUT              PIC S9(3) COMP-3 VALUE 0.           
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *            88 CC-CB-INIT                VALUE +010.                     
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                   88 CC-CB-INIT                VALUE +010.                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
E0398B*            88 CC-CB-INIT-PARM           VALUE +011.                     
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                   88 CC-CB-INIT-PARM           VALUE +011.                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
E0398B*            88 CC-CB-INIT-DATA           VALUE +012.                     
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                   88 CC-CB-INIT-DATA           VALUE +012.                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *            88 CC-CB-REINIT              VALUE +020.                     
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                   88 CC-CB-REINIT              VALUE +020.                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *            88 CC-CB-SAISIE              VALUE +100.                     
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                   88 CC-CB-SAISIE              VALUE +100.                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *            88 CC-CB-TRAITE              VALUE +110.                     
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                   88 CC-CB-TRAITE              VALUE +110.                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
E0452 *            88 CC-CB-CARTE               VALUE +200.                     
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                   88 CC-CB-CARTE               VALUE +200.                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *            88 CC-CB-OK                  VALUE  000.                     
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                   88 CC-CB-OK                  VALUE  000.                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *            88 CC-CB-NOK                 VALUE -100.                     
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                   88 CC-CB-NOK                 VALUE -100.                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *            88 CC-CB-WARN                VALUE  100.                     
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                   88 CC-CB-WARN                VALUE  100.                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
               15 CS-CB-NSEQERR             PIC X(4).                           
               15 CS-CB-LLIBRE              PIC X(80).                          
E0398 **       0044                                                             
E0398 *        0047                                                             
               15 CS-CB-CONTEXTE.                                               
E0398              20 CS-CB-SOC                 PIC X(3).                       
                   20 CS-CB-APPLID              PIC X(8).                       
                   20 CS-CB-TERMID              PIC X(4).                       
                   20 CS-CB-USERID              PIC X(8).                       
                   20 CS-CB-PROGID              PIC X(8).                       
                   20 CS-CB-NCODIC              PIC X(7).                       
                   20 CS-CB-DATE-JOUR           PIC X(8).                       
                   20 CS-CB-LOG                 PIC X(1).                       
E0452 *        0007                                                             
E0452-         15 CS-CB-CRT.                                                    
                   20 CS-CB-CRT-RCP-PRM         PIC X(1) VALUE SPACE.           
                   20 CS-CB-CRT-RCP-PRD         PIC X(1) VALUE SPACE.           
                   20 CS-CB-CRT-TRT-SS          PIC X(1) VALUE SPACE.           
                   20 CS-CB-CRT-TRT-PP          PIC X(1) VALUE SPACE.           
                   20 CS-CB-CRT-RCP-PP          PIC X(1) VALUE SPACE.           
-E0452             20 CS-CB-CRT-RCP-PXC         PIC X(1) VALUE SPACE.           
E0452 *        15 FILLER                    PIC X(7).                           
E0452          15 FILLER                    PIC X(1).                           
      **   0586                                                                 
E0316 **   0587                                                                 
E0390G**   0593                                                                 
E0398 *    0601                                                                 
C0486 *    0604                                                                 
E0622Z     10 CS-CB-ENR.                                                        
E0622Z*    10 CS-CB-ENR-GEN.                                                    
E0390G**       0019                                                             
E0390G*        0021                                                             
               15 CS-CB-DROIT.                                                  
E0390G             20 CS-CB-FNC              PIC S9(3) COMP-3 VALUE 0.          
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
-     *                88 CC-CB-FNC-INT             VALUE +000.                 
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                       88 CC-CB-FNC-INT             VALUE +000.                 
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
E0390G*                88 CC-CB-FNC-MAJ             VALUE +010.                 
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                       88 CC-CB-FNC-MAJ             VALUE +010.                 
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
                   20 CS-CB-DRT              PIC S9(3) COMP-3 VALUE 0.          
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *                88 CC-CB-DRT-NONE            VALUE +000.                 
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                       88 CC-CB-DRT-NONE            VALUE +000.                 
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
E0619Z*                88 CC-CB-DRT-SOC             VALUE +010.                 
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                       88 CC-CB-DRT-SOC             VALUE +010.                 
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
E0619Z*                88 CC-CB-DRT-SOC            VALUE +010 THRU +019.        
E0619Z*                88 CC-CB-DRT-SOC-BASE        VALUE +010.                 
E0619Z*                88 CC-CB-DRT-SOC-DRG         VALUE +011.                 
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
E0619Z*                88 CC-CB-DRT-ZONE            VALUE +020.                 
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                       88 CC-CB-DRT-ZONE            VALUE +020.                 
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
E0619Z*                88 CC-CB-DRT-ZONE           VALUE +020 THRU +029.        
E0619Z*                88 CC-CB-DRT-ZONE-BASE      VALUE +020.                  
E0619Z*                88 CC-CB-DRT-ZONE-DRG        VALUE +021.                 
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
E0619Z*                88 CC-CB-DRT-MAG             VALUE +030.                 
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                       88 CC-CB-DRT-MAG             VALUE +030.                 
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
E0619Z*                88 CC-CB-DRT-MAG            VALUE +030 THRU +039.        
E0619Z*                88 CC-CB-DRT-MAG-BASE        VALUE +030.                 
E0619Z*                88 CC-CB-DRT-MAG-DRG         VALUE +031.                 
                   20 CS-CB-SPX              PIC S9(3) COMP-3 VALUE 0.          
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *                88 CC-CB-SPX-NONE            VALUE +000.                 
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                       88 CC-CB-SPX-NONE            VALUE +000.                 
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *                88 CC-CB-SPX-PART            VALUE +010.                 
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                       88 CC-CB-SPX-PART            VALUE +010.                 
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *                88 CC-CB-SPX-TOTAL           VALUE +100.                 
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                       88 CC-CB-SPX-TOTAL           VALUE +100.                 
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
                   20 CS-CB-SOA              PIC S9(3) COMP-3 VALUE 0.          
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *                88 CC-CB-SOA-NONE            VALUE +000.                 
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                       88 CC-CB-SOA-NONE            VALUE +000.                 
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *                88 CC-CB-SOA-PART            VALUE +010.                 
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                       88 CC-CB-SOA-PART            VALUE +010.                 
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *                88 CC-CB-SOA-TOTAL           VALUE +100.                 
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                       88 CC-CB-SOA-TOTAL           VALUE +100.                 
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
                   20 CS-CB-SPM              PIC S9(3) COMP-3 VALUE 0.          
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *                88 CC-CB-SPM-NONE            VALUE +000.                 
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                       88 CC-CB-SPM-NONE            VALUE +000.                 
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *                88 CC-CB-SPM-PART            VALUE +010.                 
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                       88 CC-CB-SPM-PART            VALUE +010.                 
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *                88 CC-CB-SPM-TOTAL           VALUE +100.                 
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
                       88 CC-CB-SPM-TOTAL           VALUE +100.                 
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
                   20 CS-CB-SOC-REF             PIC X(3).                       
                   20 CS-CB-SOC-LOC             PIC X(3).                       
                   20 CS-CB-ZONE-LOC            PIC X(2).                       
                   20 CS-CB-MAG-LOC             PIC X(3).                       
E0622Z*    10 CS-CB-ENR.                                                        
      *        0054                                                             
               15 CS-CB-TS.                                                     
                   20 TS-CB-PP-IDENT            PIC X(8).                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            20 TB-CB-PP-MAX              PIC S9(4) COMP.                 
      *--                                                                       
                   20 TB-CB-PP-MAX              PIC S9(4) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            20 TB-CB-PP-ADD              PIC S9(4) COMP.                 
      *--                                                                       
                   20 TB-CB-PP-ADD              PIC S9(4) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            20 TB-CB-PP-NB               PIC S9(4) COMP.                 
      *--                                                                       
                   20 TB-CB-PP-NB               PIC S9(4) COMP-5.               
      *}                                                                        
                   20 TS-CB-LI-IDENT            PIC X(8).                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            20 TB-CB-LI-NB               PIC S9(4) COMP.                 
      *--                                                                       
                   20 TB-CB-LI-NB               PIC S9(4) COMP-5.               
      *}                                                                        
                   20 TS-CB-ZP-IDENT            PIC X(8).                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            20 TB-CB-ZP-NB               PIC S9(4) COMP.                 
      *--                                                                       
                   20 TB-CB-ZP-NB               PIC S9(4) COMP-5.               
      *}                                                                        
                   20 TS-CB-DP-IDENT            PIC X(8).                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            20 TB-CB-DP-NB               PIC S9(4) COMP.                 
      *--                                                                       
                   20 TB-CB-DP-NB               PIC S9(4) COMP-5.               
      *}                                                                        
                   20 TS-CB-CO-IDENT            PIC X(8).                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            20 TB-CB-CO-NB               PIC S9(4) COMP.                 
      *--                                                                       
                   20 TB-CB-CO-NB               PIC S9(4) COMP-5.               
      *}                                                                        
      **       0143                                                             
E0316 **       0144                                                             
E0398 *        0152                                                             
               15 CS-CB-DONNEE.                                                 
                   20 CS-CB-CFAM                PIC X(5).                       
E0398              20 CS-CB-CHEFPROD            PIC X(5).                       
E0316              20 CS-CB-INCLF               PIC X(1).                       
                   20 CS-CB-FTYPE               PIC X(1).                       
                   20 CS-CB-GROUPE              PIC X(1).                       
                   20 CS-CB-GRP                 PIC X(1).                       
                   20 CS-CB-ELT                 PIC X(1).                       
                   20 CS-CB-CAPPRO              PIC X(5).                       
                   20 CS-CB-DSTATUT             PIC X(8).                       
                   20 CS-CB-LSTATCOMP           PIC X(3).                       
                   20 CS-CB-WSENSAPPRO          PIC X(1).                       
                   20 CS-CB-WSENSVTE            PIC X(1).                       
                   20 CP-CB-TVA                 PIC S9(3)V9(2) COMP-3.          
                   20 CP-CB-PX-PRA              PIC S9(5)V9(2) COMP-3.          
                   20 CP-CB-PX-REVIENT          PIC S9(5)V9(2) COMP-3.          
                   20 CP-CB-PX-SEUIL-HT         PIC S9(5)V9(2) COMP-3.          
                   20 CP-CB-PX-SEUIL-TTC        PIC S9(5)V9(2) COMP-3.          
                   20 CP-CB-PX-COMM             PIC S9(5)V9(2) COMP-3.          
                   20 CP-CB-COEFF-CESS          PIC S9(1)V9(6) COMP-3.          
                   20 CP-CB-PX-CESS             PIC S9(5)V9(2) COMP-3.          
                   20 CS-CB-BLOQ-SRP            PIC X.                          
                   20 CS-CB-BLOQ-EDM            PIC X.                          
                   20 CP-CB-BLOQ-EDM            PIC S9(5)V9(2) COMP-3.          
      *            0048                                                         
                   20 CS-CB-PP-DATE.                                            
                       25 CS-CB-DEB             PIC X(8).                       
                       25 CS-CB-DEB-AFF         PIC X(8).                       
                       25 CS-CB-FIN             PIC X(8).                       
                       25 CS-CB-FIN-AFF         PIC X(8).                       
                       25 CS-CB-SUP             PIC X(8).                       
                       25 CS-CB-SUP-AFF         PIC X(8).                       
      *            0022                                                         
                   20 CS-CB-PX-PARM.                                            
                       25 CS-CB-TABLE-MB        PIC X(5).                       
                       25 CS-CB-TABLE-CA        PIC X(5).                       
                       25 CP-CB-PM-VOL          PIC S9(5)V9(2) COMP-3.          
                       25 CP-CB-PM-MINI         PIC S9(5)V9(2) COMP-3.          
                       25 CP-CB-PM-MAXI         PIC S9(5)V9(2) COMP-3.          
      *            0012                                                         
                   20 CS-CB-PX-COEFF.                                           
                       25 CP-CB-COEFF-TRANS     PIC S9(1)V9(6) COMP-3.          
                       25 CP-CB-COEFF-FAM       PIC S9(1)V9(6) COMP-3.          
                       25 CP-CB-MAJORATION      PIC S9(5)V9(2) COMP-3.          
      *        0050                                                             
               15 CS-CB-PX-ACTUEL.                                              
                   20 CP-CB-PX-REF              PIC S9(5)V9(2) COMP-3.          
                   20 CS-CB-PX-REF-DEFFET       PIC X(8).                       
                   20 CS-CB-PX-REF-WIMPER       PIC X(1).                       
                   20 CS-CB-PX-REF-CIMPER       PIC X(1).                       
                   20 CS-CB-PX-REF-WBLOQUE      PIC X(1).                       
                   20 CS-CB-PX-REF-LCOMMENT     PIC X(10).                      
                   20 CP-CB-PX-MINI             PIC S9(5)V9(2) COMP-3.          
                   20 CS-CB-PX-MINI-DEFFET      PIC X(8).                       
                   20 CS-CB-PX-MINI-WIMPER      PIC X(1).                       
                   20 CS-CB-PX-MINI-CIMPER      PIC X(1).                       
                   20 CS-CB-PX-MINI-WBLOQUE     PIC X(1).                       
                   20 CS-CB-PX-MINI-LCOMMENT    PIC X(10).                      
      *        0066                                                             
               15 CS-CB-CALCUL.                                                 
                   20 CP-CB-MB                  PIC S9(5)V9(4) COMP-3.          
                   20 CP-CB-MB-TX               PIC S9(3)V9(4) COMP-3.          
                   20 CP-CB-MB-CESS             PIC S9(5)V9(4) COMP-3.          
                   20 CP-CB-MB-CESS-TX          PIC S9(3)V9(4) COMP-3.          
                   20 CP-CB-EDM-MB              PIC S9(5)V9(4) COMP-3.          
                   20 CP-CB-EDM-MB-TX           PIC S9(2)V9(4) COMP-3.          
                   20 CP-CB-EDM-CA              PIC S9(5)V9(4) COMP-3.          
                   20 CP-CB-EDM-CA-TX           PIC S9(2)V9(4) COMP-3.          
                   20 CP-CB-PM-CALCUL           PIC S9(5)V9(4) COMP-3.          
                   20 CP-CB-PM-AJUST            PIC S9(5)V9(4) COMP-3.          
                   20 CP-CB-PM-ADAP             PIC S9(5)V9(4) COMP-3.          
                   20 CP-CB-PM-PAS              PIC S9(5)V9(4) COMP-3.          
                   20 CP-CB-EDM-ADAP            PIC S9(5)V9(4) COMP-3.          
                   20 CP-CB-EDM-SAISI           PIC S9(5)V9(4) COMP-3.          
      *        0028                                                             
               15 CS-CB-PM-ACTUEL.                                              
                   20 CS-CB-PM-TYPE             PIC X(1).                       
                   20 CP-CB-PM-REF              PIC S9(5)V9(2) COMP-3.          
                   20 CP-CB-PM-CAL              PIC S9(5)V9(2) COMP-3.          
                   20 CS-CB-PM-WCALCUL          PIC X(1).                       
                   20 CS-CB-PM-REF-DEFFET       PIC X(8).                       
                   20 CS-CB-PM-REF-LCOMMENT     PIC X(10).                      
      *        0026                                                             
               15 CS-CB-OA-ACTUEL.                                              
      *            0019                                                         
                   20 CS-CB-OA.                                                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                25 CB-CB-OA-TYPE             PIC S9(4) COMP.             
      *--                                                                       
                       25 CB-CB-OA-TYPE             PIC S9(4) COMP-5.           
      *}                                                                        
                       25 CS-CB-OA-NAT              PIC X(1).                   
                       25 CS-CB-OA-NAT-DEFFET       PIC X(8).                   
                       25 CS-CB-OA-NAT-DFINEFFET    PIC X(8).                   
                   20 CS-CB-OA-NAT-NCODICS          PIC X(7).                   
      *        0004                                                             
               15 CS-CB-PM-PARM.                                                
                   20 CP-CB-PM-MINI-SOC         PIC S9(5)V9(2) COMP-3.          
      *        0010                                                             
               15 CS-CB-SELECTION.                                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            20 CB-CB-SEL-REF       PIC S9(4) COMP   VALUE 0.             
      *--                                                                       
                   20 CB-CB-SEL-REF       PIC S9(4) COMP-5   VALUE 0.           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            20 CB-CB-SEL-REFM      PIC S9(4) COMP   VALUE 0.             
      *--                                                                       
                   20 CB-CB-SEL-REFM      PIC S9(4) COMP-5   VALUE 0.           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            20 CB-CB-SEL-LIE       PIC S9(4) COMP   VALUE 0.             
      *--                                                                       
                   20 CB-CB-SEL-LIE       PIC S9(4) COMP-5   VALUE 0.           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            20 CB-CB-SEL-LIEM      PIC S9(4) COMP   VALUE 0.             
      *--                                                                       
                   20 CB-CB-SEL-LIEM      PIC S9(4) COMP-5   VALUE 0.           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            20 CB-CB-SEL-SUP       PIC S9(4) COMP   VALUE 0.             
      *--                                                                       
                   20 CB-CB-SEL-SUP       PIC S9(4) COMP-5   VALUE 0.           
      *}                                                                        
E0390C**       0106                                                             
E0390C*        0110                                                             
               15 CS-CB-SAISIE.                                                 
                   20 CP-CB-SA-ACTION     PIC S9(3) COMP-3 VALUE 0.             
                   20 CS-CB-SA-LIEU       PIC X(3).                             
                   20 CS-CB-SA-ZONE       PIC X(2).                             
                   20 CS-CB-SA-MAG        PIC X(3).                             
                   20 CS-CB-SA-NCONC      PIC X(4).                             
                   20 CS-CB-SA-LCONC      PIC X(15).                            
                   20 CP-CB-SA-PX         PIC S9(5)V9(2) COMP-3 VALUE 0.        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
E0390C*            20 CB-CB-SA-PX-F       PIC S9(4) COMP VALUE 00.              
      *--                                                                       
                   20 CB-CB-SA-PX-F       PIC S9(4) COMP-5 VALUE 00.            
      *}                                                                        
                   20 CS-CB-SA-PXD        PIC X(8).                             
                   20 CS-CB-SA-PXF        PIC X(8).                             
                   20 CS-CB-SA-LIB        PIC X(20).                            
                   20 CS-CB-SA-OA         PIC X(1).                             
                   20 CS-CB-SA-OAD        PIC X(8).                             
                   20 CS-CB-SA-OAF        PIC X(8).                             
                   20 CP-CB-SA-PM         PIC S9(5)V9(2) COMP-3 VALUE 0.        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
E0390C*            20 CB-CB-SA-PM-F       PIC S9(4) COMP VALUE 00.              
      *--                                                                       
                   20 CB-CB-SA-PM-F       PIC S9(4) COMP-5 VALUE 00.            
      *}                                                                        
                   20 CS-CB-SA-PMD        PIC X(8).                             
                   20 CS-CB-SA-PMF        PIC X(8).                             
      *        0014                                                             
               15 CS-CB-SAISIE-S.                                               
                   20 CP-CB-SA-ACTION-S      PIC S9(3) COMP-3 VALUE 0.          
                   20 CP-CB-SA-SEL-S         PIC S9(3) COMP-3 VALUE 0.          
                   20 CP-CB-SA-LIEU-S        PIC S9(3) COMP-3 VALUE 0.          
                   20 CP-CB-SA-NCONC-S       PIC S9(3) COMP-3 VALUE 0.          
                   20 CP-CB-SA-PX-S          PIC S9(3) COMP-3 VALUE 0.          
                   20 CP-CB-SA-OA-S          PIC S9(3) COMP-3 VALUE 0.          
                   20 CP-CB-SA-PM-S          PIC S9(3) COMP-3 VALUE 0.          
      *        0066                                                             
               15 CS-CB-RESULT.                                                 
                   20 CP-CB-RE-MB                PIC S9(5)V9(4) COMP-3.         
                   20 CP-CB-RE-MB-TX             PIC S9(3)V9(4) COMP-3.         
                   20 CP-CB-RE-MB-CESS           PIC S9(5)V9(4) COMP-3.         
                   20 CP-CB-RE-MB-CESS-TX        PIC S9(3)V9(4) COMP-3.         
                   20 CP-CB-RE-EDM-MB            PIC S9(5)V9(4) COMP-3.         
                   20 CP-CB-RE-EDM-MB-TX         PIC S9(2)V9(4) COMP-3.         
                   20 CP-CB-RE-EDM-CA            PIC S9(5)V9(4) COMP-3.         
                   20 CP-CB-RE-EDM-CA-TX         PIC S9(2)V9(4) COMP-3.         
                   20 CP-CB-RE-PM-CALCUL         PIC S9(5)V9(4) COMP-3.         
                   20 CP-CB-RE-PM-AJUST          PIC S9(5)V9(4) COMP-3.         
                   20 CP-CB-RE-PM-ADAP           PIC S9(5)V9(4) COMP-3.         
                   20 CP-CB-RE-PM-PAS            PIC S9(5)V9(4) COMP-3.         
                   20 CP-CB-RE-EDM-ADAP          PIC S9(5)V9(4) COMP-3.         
                   20 CP-CB-RE-EDM-SAISI         PIC S9(5)V9(4) COMP-3.         
E0452-*        0002                                                             
               15 CS-CB-PARM.                                                   
                   20 CS-CB-BLOQ-SRP-INI        PIC X.                          
-E0452             20 CS-CB-BLOQ-EDM-INI        PIC X.                          
C0486-*        0003                                                             
               15 CS-CB-DERO-ACTUEL.                                            
                   20 CS-DERO-REF-PREF          PIC X.                          
                   20 CS-DERO-REF-MINI          PIC X.                          
-C0486             20 CS-DERO-REF-SRP           PIC X.                          
      **   0309                                                                 
E0316 **   0308                                                                 
E0390G**   0302                                                                 
E0398 **   0283                                                                 
E0452 *    0281                                                                 
C0486 *    0278                                                                 
E0499-*    GESTION GROUPE 60                                                    
           10 CS-CB-GR.                                                         
               15 CS-CB-GR-GESTION.                                             
                   20 CS-CB-GR-AUTO            PIC X(1).                        
               15 CS-CB-GR-CMPTR.                                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            20 CB-CB-GX-MAJ             PIC S9(4) COMP   VALUE 0.        
      *--                                                                       
                   20 CB-CB-GX-MAJ             PIC S9(4) COMP-5   VALUE         
                                                                      0.        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            20 CB-CB-GX-REJET           PIC S9(4) COMP   VALUE 0.        
      *--                                                                       
                   20 CB-CB-GX-REJET           PIC S9(4) COMP-5   VALUE         
                                                                      0.        
      *}                                                                        
               15 CS-CB-GR-TS.                                                  
                   20 TS-CB-HL-IDENT           PIC X(8).                        
                   20 TS-CB-GR-IDENT           PIC X(8).                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            20 TB-CB-GR-NB              PIC S9(4) COMP.                  
      *--                                                                       
                   20 TB-CB-GR-NB              PIC S9(4) COMP-5.                
      *}                                                                        
                   20 TS-CB-GC-IDENT           PIC X(8).                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            20 TB-CB-GC-NB              PIC S9(4) COMP.                  
      *--                                                                       
                   20 TB-CB-GC-NB              PIC S9(4) COMP-5.                
      *}                                                                        
                   20 TS-CB-GP-IDENT           PIC X(8).                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            20 TB-CB-GP-NB              PIC S9(4) COMP.                  
      *--                                                                       
                   20 TB-CB-GP-NB              PIC S9(4) COMP-5.                
      *}                                                                        
-E0499         15 FILLER                   PIC X(007).                          
E0622Z*    10 CS-CB-TGN10.                                                      
E0622Z*        15 CS-CB-INIT-TGN10          PIC S9(1) COMP-3 VALUE 0.           
E0622Z*            88 CC-CB-INIT-TGN10          VALUE +1.                       
E0622Z     10 FILLER                 PIC X(228).                                
E0622Z*    10 FILLER                 PIC X(227).                                
                                                                                
