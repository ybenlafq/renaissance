      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PPPRI CODES PRIMES NCG                 *        
      *----------------------------------------------------------------*        
       01  RVGA01QL.                                                            
           05  PPPRI-CTABLEG2    PIC X(15).                                     
           05  PPPRI-CTABLEG2-REDEF REDEFINES PPPRI-CTABLEG2.                   
               10  PPPRI-CPRIME          PIC X(05).                             
           05  PPPRI-WTABLEG     PIC X(80).                                     
           05  PPPRI-WTABLEG-REDEF  REDEFINES PPPRI-WTABLEG.                    
               10  PPPRI-WACTIF          PIC X(01).                             
               10  PPPRI-LPRIME          PIC X(30).                             
               10  PPPRI-CGRPRIME        PIC X(05).                             
               10  PPPRI-LNOMZONE        PIC X(10).                             
               10  PPPRI-CPRISIGA        PIC X(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01QL-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PPPRI-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PPPRI-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PPPRI-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PPPRI-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
