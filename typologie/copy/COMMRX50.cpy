      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *-----------------------------------------------------------------        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-RX50-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-RX50-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA -----------------------------------------        
           02 FILLER-COM-AIDA      PIC X(100).                                  
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS --------------------------        
           02 COMM-CICS-APPLID     PIC X(8).                                    
           02 COMM-CICS-NETNAM     PIC X(8).                                    
           02 COMM-CICS-TRANSA     PIC X(4).                                    
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------------        
           02 COMM-DATE-SIECLE     PIC XX.                                      
           02 COMM-DATE-ANNEE      PIC XX.                                      
           02 COMM-DATE-MOIS       PIC XX.                                      
           02 COMM-DATE-JOUR       PIC 99.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA       PIC 999.                                     
           02 COMM-DATE-QNT0       PIC 99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX       PIC 9.                                       
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM        PIC 9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC     PIC XXX.                                     
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC    PIC XXX.                                     
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                                    
           02 COMM-DATE-AAMMJJ     PIC X(6).                                    
           02 COMM-DATE-JJMMSSAA   PIC X(8).                                    
           02 COMM-DATE-JJMMAA     PIC X(6).                                    
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                                    
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS   PIC 99.                                      
              05 COMM-DATE-SEMAA   PIC 99.                                      
              05 COMM-DATE-SEMNU   PIC 99.                                      
      *                                                                         
           02 COMM-DATE-FILLER     PIC X(08).                                   
      *                                                                         
      * ZONES RESERVEES TRAITEMENT DU SWAP -----------------------------        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                     
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 180 PIC X(1).                               
      *                                                                         
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*        
      *>>>>>>>>>>>>>>>>> ZONES RESERVEES APPLICATIVES <<<<<<<<<<<<<<<<<*        
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*        
      *                                                                         
      *-------------------------------------------------------3694-----*        
      * MENU GENERAL DE L'APPLICATION (PROGRAMME TRX50)                *        
      *----------------------------------------------------------------*        
           02 COMM-RX50-APPLI.                                                  
      *--------------------------------------------------119----------*         
              03 COMM-RX50-SIEGE             PIC X.                             
              03 COMM-RX50-BBTE              PIC X.                             
              03 COMM-RX50-CENTRALISE        PIC X.                             
              03 COMM-RX50-TOUT              PIC X.                             
              03 COMM-RX50-PAGE              PIC 9(2).                          
              03 COMM-RX50-PAGETOT           PIC 9(2).                          
              03 COMM-RX50-DDEB              PIC X(8).                          
              03 COMM-RX50-NSOCIETE          PIC X(3).                          
              03 COMM-RX50-NCODIC            PIC X(7).                          
              03 COMM-RX50-NLIEU             PIC X(3).                          
              03 COMM-RX50-NZONPRIX          PIC X(2).                          
              03 COMM-RX50-PRIX              PIC X(9).                  01720001
              03 COMM-RX50-MESSAGE           PIC X(79).                 01720001
      *-------------------------------------------------------          00743701
           02 COMM-RX51-APPLI.                                                  
      *-------------------------------------------------126---          00743701
              03 COMM-RX51-NZONPRIX          PIC X(2).                          
              03 COMM-RX51-NCONC             PIC X(4).                          
              03 COMM-RX51-LENSCONC          PIC X(15).                         
              03 COMM-RX51-LEMPCONC          PIC X(15).                         
              03 COMM-RX51-PRIX              PIC X(9).                          
              03 COMM-RX51-PAGE              PIC 9(2).                          
              03 COMM-RX51-PAGE-MAX          PIC 9(2).                          
              03 COMM-RX51-CFAM              PIC X(05).                 05970001
              03 COMM-RX51-LFAM              PIC X(19).                 05980001
              03 COMM-RX51-CAPPRO            PIC X(03).                 05990001
              03 COMM-RX51-CMARQ             PIC X(05).                 06000001
              03 COMM-RX51-LMARQ             PIC X(19).                 06010001
              03 COMM-RX51-COMPAC            PIC X(03).                 05980001
              03 COMM-RX51-REF               PIC X(20).                 05990001
              03 COMM-RX51-COACT             PIC X(01).                 06000001
              03 COMM-RX51-CEXPO             PIC X(01).                 06010001
              03 COMM-RX51-OAM               PIC X(01).                 06010001
      *-------------------------------------------------------          00743701
           02 COMM-RX55-APPLI.                                                  
      *------------------------------------------------2093---                  
              03 COMM-RX55-SIEGE             PIC X.                             
              03 COMM-RX55-NZONPRIX          PIC X(2).                          
              03 COMM-RX55-NZPMAG            PIC X(2).                          
              03 COMM-RX55-NMAG              PIC X(3).                          
              03 COMM-RX55-NZPCONC           PIC X(2).                          
              03 COMM-RX55-NCONC             PIC X(4).                          
              03 COMM-RX55-LENSCONC          PIC X(15).                         
              03 COMM-RX55-CRAYON            PIC X(05).                         
              03 COMM-RX55-NCODIC            PIC X(07).                         
              03 COMM-RX55-NRELEVE           PIC X(07).                         
              03 COMM-RX55-TYPE              PIC X(04).                         
              03 COMM-RX55-OPER              PIC X(01).                         
              03 COMM-RX55-DATE              PIC X(08).                         
              03 COMM-RX55-DATE2             PIC X(08).                         
              03 COMM-RX55-DATEMIN           PIC X(08).                         
              03 COMM-RX55-DATEMIN2          PIC X(08).                         
              03 COMM-RX55-ENSFAM            PIC X(2000).                       
              03 COMM-RX55-PAGE              PIC 9(4).                          
              03 COMM-RX55-PAGETOT           PIC 9(4).                          
      *--------------------------------------------------RESTE 1356     00743701
           02 COMM-RX5X-LIBRE          PIC X(1356).                     01720001
      *-------------------------------------------------------          00743701
      ***************************************************************** 02170001
                                                                                
