      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE LICRD ACTIV. BAR. CR�DIT NIV. GROUPE   *        
      *----------------------------------------------------------------*        
       01  RVLICRD.                                                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  LICRD-CTABLEG2    PIC X(15).                                     
           05  LICRD-CTABLEG2-REDEF REDEFINES LICRD-CTABLEG2.                   
               10  LICRD-CBAREME         PIC X(07).                             
               10  LICRD-NSOC            PIC X(03).                             
               10  LICRD-NLIEU           PIC X(03).                             
               10  LICRD-NSEQ            PIC X(02).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  LICRD-NSEQ-N         REDEFINES LICRD-NSEQ                    
                                         PIC 9(02).                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  LICRD-WTABLEG     PIC X(80).                                     
           05  LICRD-WTABLEG-REDEF  REDEFINES LICRD-WTABLEG.                    
               10  LICRD-DEFFET          PIC X(08).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  LICRD-DEFFET-N       REDEFINES LICRD-DEFFET                  
                                         PIC 9(08).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVLICRD-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LICRD-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  LICRD-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LICRD-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  LICRD-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
