      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE IFTMT TYPES DE MONTANTS                *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01SK.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01SK.                                                            
      *}                                                                        
           05  IFTMT-CTABLEG2    PIC X(15).                                     
           05  IFTMT-CTABLEG2-REDEF REDEFINES IFTMT-CTABLEG2.                   
               10  IFTMT-CTYPMONT        PIC X(05).                             
           05  IFTMT-WTABLEG     PIC X(80).                                     
           05  IFTMT-WTABLEG-REDEF  REDEFINES IFTMT-WTABLEG.                    
               10  IFTMT-WACTIF          PIC X(01).                             
               10  IFTMT-LTYPMONT        PIC X(20).                             
               10  IFTMT-WDETAIL         PIC X(01).                             
               10  IFTMT-CMOTCLE         PIC X(10).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01SK-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01SK-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IFTMT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  IFTMT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IFTMT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  IFTMT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
