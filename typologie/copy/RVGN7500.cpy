      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      **********************************************************        00030000
      *   COPY DE LA TABLE RVGN7500                                     00040001
      **********************************************************        00050000
      *   LISTE DES HOST VARIABLES DE LA VUE RVGN7500                   00060001
      **********************************************************        00070000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGN7500.                                                    00080001
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGN7500.                                                            
      *}                                                                        
           05  GN75-NCODIC     PIC X(7).                                00090001
           05  GN75-DEFFET     PIC X(8).                                00100001
           05  GN75-TYPCOMM    PIC X(1).                                00110001
           05  GN75-PCOMMREF   PIC S9(5)V9(2) COMP-3.                           
           05  GN75-PCOMMCAL   PIC S9(5)V9(2) COMP-3.                           
           05  GN75-WCALCUL    PIC X(1).                                00130002
           05  GN75-LCOMMENT   PIC X(10).                               00150002
           05  GN75-DSYST      PIC S9(13) COMP-3.                       00160003
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************        00170000
      *   LISTE DES FLAGS DE LA TABLE RVGN7500                          00180001
      **********************************************************        00190000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGN7500-FLAGS.                                              00200001
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGN7500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN75-NCODIC-F                                            00210003
      *        PIC S9(4) COMP.                                          00211000
      *--                                                                       
           05  GN75-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN75-DEFFET-F                                            00220003
      *        PIC S9(4) COMP.                                          00221000
      *--                                                                       
           05  GN75-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN75-TYPCOMM-F                                           00230003
      *        PIC S9(4) COMP.                                          00231000
      *--                                                                       
           05  GN75-TYPCOMM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN75-PCOMMREF-F                                          00240003
      *        PIC S9(4) COMP.                                          00241000
      *--                                                                       
           05  GN75-PCOMMREF-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN75-PCOMMCAL-F                                          00250003
      *        PIC S9(4) COMP.                                          00251000
      *--                                                                       
           05  GN75-PCOMMCAL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN75-WCALCUL-F                                           00260003
      *        PIC S9(4) COMP.                                          00261000
      *--                                                                       
           05  GN75-WCALCUL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN75-LCOMMENT-F                                          00270003
      *        PIC S9(4) COMP.                                          00271000
      *--                                                                       
           05  GN75-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN75-DSYST-F                                             00280001
      *        PIC S9(4) COMP.                                          00290000
      *                                                                         
      *--                                                                       
           05  GN75-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
