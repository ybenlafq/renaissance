      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RTPR31                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPR3100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPR3100.                                                            
      *}                                                                        
           10 PR31-NCHRONO         PIC X(7).                                    
           10 PR31-NLIGNE          PIC X(3).                                    
           10 PR31-NVENTE          PIC X(7).                                    
           10 PR31-CTITRENOM       PIC X(5).                                    
           10 PR31-LNOM            PIC X(25).                                   
           10 PR31-LPRENOM         PIC X(15).                                   
           10 PR31-MTPRIME         PIC S9(7)V9(2) USAGE COMP-3.                 
           10 PR31-GSM             PIC X(11).                                   
           10 PR31-IMEI            PIC X(15).                                   
           10 PR31-ICCIDNSCE       PIC X(20).                                   
           10 PR31-INITIALES       PIC X(2).                                    
           10 PR31-WDOSSIER        PIC X(1).                                    
           10 PR31-WBONCDE         PIC X(1).                                    
           10 PR31-WPIDENT         PIC X(1).                                    
           10 PR31-WAR             PIC X(1).                                    
           10 PR31-DAR             PIC X(8).                                    
           10 PR31-MTRGLT          PIC S9(7)V9(2) USAGE COMP-3.                 
           10 PR31-DRGLT           PIC X(8).                                    
           10 PR31-DSYST           PIC S9(13)V USAGE COMP-3.                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 19      *        
      ******************************************************************        
                                                                                
