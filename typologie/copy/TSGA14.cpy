      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TRM31                                          *  00020001
      ****************************************************************  00030000
      *                                                                 00040000
      *------------------------------ LONGUEUR                          00050000
       01  TS-GA14-LONG         PIC S9(2) COMP-3 VALUE 29.              00060003
       01  TS-GA14-DONNEES.                                             00070000
              10 TS-GA14-CFAM      PIC X(05).                           00080001
              10 TS-GA14-LFAM      PIC X(20).                           00090003
              10 TS-GA14-WMULTIFAM PIC X(01).                           00100001
              10 TS-GA14-NUMPAGE   PIC S9(3) COMP-3.                    00110001
              10 TS-GA14-OK        PIC X.                               00120002
                                                                                
