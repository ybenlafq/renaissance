      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MEG36 MENU EDITIONS GENERALISEES 36    *        
      *----------------------------------------------------------------*        
       01  RVGA01E6.                                                            
           05  MEG36-CTABLEG2    PIC X(15).                                     
           05  MEG36-CTABLEG2-REDEF REDEFINES MEG36-CTABLEG2.                   
               10  MEG36-SOCIETE         PIC X(03).                             
               10  MEG36-SOCIETE-N      REDEFINES MEG36-SOCIETE                 
                                         PIC 9(03).                             
               10  MEG36-OPTION          PIC X(02).                             
               10  MEG36-OPTION-N       REDEFINES MEG36-OPTION                  
                                         PIC 9(02).                             
           05  MEG36-WTABLEG     PIC X(80).                                     
           05  MEG36-WTABLEG-REDEF  REDEFINES MEG36-WTABLEG.                    
               10  MEG36-ETAT            PIC X(08).                             
               10  MEG36-FORMAT          PIC X(01).                             
               10  MEG36-TITRE           PIC X(47).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01E6-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MEG36-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MEG36-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MEG36-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MEG36-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
