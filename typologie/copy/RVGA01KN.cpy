      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EFTRT CENTRE DE TRAITEMENTS GAF        *        
      *----------------------------------------------------------------*        
       01  RVGA01KN.                                                            
           05  EFTRT-CTABLEG2    PIC X(15).                                     
           05  EFTRT-CTABLEG2-REDEF REDEFINES EFTRT-CTABLEG2.                   
               10  EFTRT-CTRAIT          PIC X(05).                             
           05  EFTRT-WTABLEG     PIC X(80).                                     
           05  EFTRT-WTABLEG-REDEF  REDEFINES EFTRT-WTABLEG.                    
               10  EFTRT-WACTIF          PIC X(01).                             
               10  EFTRT-LTRAIT          PIC X(20).                             
               10  EFTRT-TIERSHE         PIC X(16).                             
               10  EFTRT-TOLERANC        PIC X(03).                             
               10  EFTRT-TOLERANC-N     REDEFINES EFTRT-TOLERANC                
                                         PIC 9(03).                             
               10  EFTRT-FLAGS           PIC X(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01KN-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EFTRT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EFTRT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EFTRT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EFTRT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
