      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES VENTES                               *        
      *  TRANSACTION: SANS (BATCH MQ)                                  *        
      *  TITRE      : COMMAREA DE L'APPLICATION GV45 GV46 BS47         *        
      *  LONGUEUR   : 200                                              *        
      *                                                                *        
      ******************************************************************        
      *                                                                 00230000
      *                                                                 00230000
       01  COMM-BS47-APPLI.                                             00260000
           02 COMM-BS47-NSOC            PIC X(03).                      00320000
           02 COMM-BS47-NLIEU           PIC X(03).                      00330000
           02 COMM-BS47-NVENTE          PIC X(07).                      00330000
           02 COMM-BS47-NBCDE           PIC X(07).                      00330000
           02 COMM-BS47-TYPVTE          PIC X(01).                      00330000
           02 COMM-BS47-WEXPORT         PIC X.                          00330000
           02 COMM-BS47-DJOUR           PIC X(8).                       00330000
MH1103     02 WS-CODRET7                PIC X(2).                       00340000
"          02 FILLER                    PIC X(168).                     00340000
                                                                                
