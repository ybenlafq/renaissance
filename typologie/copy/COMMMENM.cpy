      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************00000010
      * COMMAREA MENU GENERAL NCG+ MASTER USER                          00000020
      ******************************************************************00000010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-MENM-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00000050
      *--                                                                       
       01  COM-MENM-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
       01  Z-COMMAREA.                                                  00000070
      * ZONES RESERVEES AIDA ------------------------------------- 100  00000090
          02 FILLER-COM-AIDA      PIC  X(100).                          00000091
      * ZONES RESERVEES CICS ------------------------------------- 020  00000093
          02 COMM-CICS-APPLID     PIC  X(8).                            00000094
          02 COMM-CICS-NETNAM     PIC  X(8).                            00000095
          02 COMM-CICS-TRANSA     PIC  X(4).                            00000096
      * DATE DU JOUR --------------------------------------------- 100  00000098
          02 COMM-DATE-SIECLE     PIC  99.                              00000099
          02 COMM-DATE-ANNEE      PIC  99.                              00000100
          02 COMM-DATE-MOIS       PIC  99.                              00000110
          02 COMM-DATE-JOUR       PIC  99.                              00000120
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00000130
          02 COMM-DATE-QNTA       PIC  999.                             00000140
          02 COMM-DATE-QNT0       PIC  99999.                           00000150
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00000160
          02 COMM-DATE-BISX       PIC  9.                               00000170
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00000180
          02 COMM-DATE-JSM        PIC  9.                               00000190
      *   LIBELLES DU JOUR COURT - LONG                                 00000191
          02 COMM-DATE-JSM-LC     PIC  XXX.                             00000192
          02 COMM-DATE-JSM-LL     PIC  X(8).                            00000193
      *   LIBELLES DU MOIS COURT - LONG                                 00000194
          02 COMM-DATE-MOIS-LC    PIC  XXX.                             00000195
          02 COMM-DATE-MOIS-LL    PIC  X(8).                            00000196
      *   DIFFERENTES FORMES DE DATE                                    00000197
          02 COMM-DATE-SSAAMMJJ   PIC  X(8).                            00000198
          02 COMM-DATE-AAMMJJ     PIC  X(6).                            00000199
          02 COMM-DATE-JJMMSSAA   PIC  X(8).                            00000200
          02 COMM-DATE-JJMMAA     PIC  X(6).                            00000210
          02 COMM-DATE-JJ-MM-AA   PIC  X(8).                            00000220
          02 COMM-DATE-JJ-MM-SSAA PIC  X(10).                           00000230
      *   TRAITEMENT NUMERO DE SEMAINE                                  00000240
          02 COMM-DATE-WEEK.                                            00000250
             05 COMM-DATE-SEMSS   PIC  99.                              00000260
             05 COMM-DATE-SEMAA   PIC  99.                              00000270
             05 COMM-DATE-SEMNU   PIC  99.                              00000280
          02 COMM-DATE-FILLER     PIC  X(08).                           00000290
      * ATTRIBUTS BMS======================================== 4 + AAAA  00000292
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.              00000293
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-MENM-ZMAP       PIC  X(1).                            00000294
          02 COMM-MENM-REDEFINES  PIC  X(3000).                         00000302
      * ZONES APPLICATIVES PR35/6/7============================== BBBB  00000296
          02 COMM-MENM REDEFINES COMM-MENM-REDEFINES.                   00000302
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-MENT-PAGEA      PIC   S9(4) BINARY.                00000303
      *--                                                                       
             03 COMM-MENT-PAGEA      PIC   S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-MENT-NBP        PIC   S9(4) BINARY.                00000303
      *--                                                                       
             03 COMM-MENT-NBP        PIC   S9(4) COMP-5.                        
      *}                                                                        
             03 COMM-MENP-TAB-ZUSER.                                            
                04 COMM-MENP-TAB-ZU-FIL OCCURS 15.                              
                   05 COMM-MENP-ZUSER   PIC X(7) OCCURS 4.                      
             03 COMM-MEND-ZUSER      PIC   X(7).                        00000303
             03 COMM-MEND-MENUAFF    PIC   X(1).                        00000303
             03 COMM-MEND-NIV1A      PIC   X(2).                        00000303
             03 COMM-MEND-NIV2A      PIC   X(2).                        00000303
             03 COMM-MEND-TITRE      PIC   X(40).                       00000303
             03 COMM-MEND-TAB-MENU.                                             
                04 COMM-MEND-TAB-FILLER OCCURS 15.                              
                   05 COMM-MEND-NIV1    PIC X(2).                               
                   05 COMM-MEND-NIV2    PIC X(2).                               
                   05 COMM-MEND-NOPT    PIC X(2).                               
                   05 COMM-MEND-LOPT    PIC X(40).                              
                   05 COMM-MEND-TRAN    PIC X(4).                               
                                                                                
