      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EGV18 FORMULE DE CREDIT                *        
      *----------------------------------------------------------------*        
       01  RVGA01U5.                                                            
           05  EGV18-CTABLEG2    PIC X(15).                                     
           05  EGV18-CTABLEG2-REDEF REDEFINES EGV18-CTABLEG2.                   
               10  EGV18-COCRED          PIC X(01).                             
               10  EGV18-CFCRED          PIC X(05).                             
           05  EGV18-WTABLEG     PIC X(80).                                     
           05  EGV18-WTABLEG-REDEF  REDEFINES EGV18-WTABLEG.                    
               10  EGV18-WTPCRD          PIC X(05).                             
               10  EGV18-PCOMPT          PIC S9(07)V9(02)     COMP-3.           
               10  EGV18-LFRMLE          PIC X(30).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01U5-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGV18-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EGV18-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGV18-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EGV18-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
