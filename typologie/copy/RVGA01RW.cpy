      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PPRUB RUBRIQUES                        *        
      *----------------------------------------------------------------*        
       01  RVGA01RW.                                                            
           05  PPRUB-CTABLEG2    PIC X(15).                                     
           05  PPRUB-CTABLEG2-REDEF REDEFINES PPRUB-CTABLEG2.                   
               10  PPRUB-NRUB            PIC X(02).                             
           05  PPRUB-WTABLEG     PIC X(80).                                     
           05  PPRUB-WTABLEG-REDEF  REDEFINES PPRUB-WTABLEG.                    
               10  PPRUB-WACTIF          PIC X(01).                             
               10  PPRUB-LRUB            PIC X(30).                             
               10  PPRUB-CTYPRUB         PIC X(01).                             
               10  PPRUB-CTYPVALO        PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01RW-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PPRUB-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PPRUB-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PPRUB-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PPRUB-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
