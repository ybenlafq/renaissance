      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MDPAR NMD: PROG AS400 PAR MESSAGE MQ   *        
      *----------------------------------------------------------------*        
       01  RVGA01XA.                                                            
           05  MDPAR-CTABLEG2    PIC X(15).                                     
           05  MDPAR-CTABLEG2-REDEF REDEFINES MDPAR-CTABLEG2.                   
               10  MDPAR-CODMSG          PIC X(10).                             
           05  MDPAR-WTABLEG     PIC X(80).                                     
           05  MDPAR-WTABLEG-REDEF  REDEFINES MDPAR-WTABLEG.                    
               10  MDPAR-LMSG            PIC X(20).                             
               10  MDPAR-CPGM            PIC X(10).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01XA-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MDPAR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MDPAR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MDPAR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MDPAR-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
