      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGA4101                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA4101                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGA4101.                                                            
           02  GA41-CGCPLT                                                      
               PIC X(0005).                                                     
           02  GA41-DEFFET                                                      
               PIC X(0008).                                                     
           02  GA41-DUGSTD                                                      
               PIC S9(3) COMP-3.                                                
           02  GA41-DUGCPLT                                                     
               PIC S9(3) COMP-3.                                                
           02  GA41-PFRAIS                                                      
               PIC S9(3)V9(0002) COMP-3.                                        
           02  GA41-CTAUXTVA                                                    
               PIC X(0005).                                                     
           02  GA41-DSYST                                                       
               PIC S9(13) COMP-3.                                               
E0713      02  GA41-CMARQ                                                       
               PIC X(0005).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGA4101                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGA4101-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA41-CGCPLT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA41-CGCPLT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA41-DEFFET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA41-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA41-DUGSTD-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA41-DUGSTD-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA41-DUGCPLT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA41-DUGCPLT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA41-PFRAIS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA41-PFRAIS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA41-CTAUXTVA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA41-CTAUXTVA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA41-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA41-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
E0713 *    02  GA41-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA41-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
