      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE BS7000                       
      ******************************************************************        
      *                                                                         
       CLEF-BS7000             SECTION.                                         
      *                                                                         
           MOVE 'RVBS7000          '       TO   TABLE-NAME.                     
           MOVE 'BS7000'                   TO   MODEL-NAME.                     
      *                                                                         
       FIN-CLEF-BS7000. EXIT.                                                   
                EJECT                                                           
                                                                                
