      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGG01   EGG01                                              00000020
      ***************************************************************** 00000030
       01   EGG01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNCONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNCONCI   PIC X(4).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENSGNL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLENSGNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLENSGNF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLENSGNI  PIC X(15).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLEMPLCL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLEMPLCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLEMPLCF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLEMPLCI  PIC X(15).                                      00000290
           02 MNMAGD OCCURS   20 TIMES .                                00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMAGL  COMP PIC S9(4).                                 00000310
      *--                                                                       
             03 MNMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMAGF  PIC X.                                          00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MNMAGI  PIC X(3).                                       00000340
           02 MLIBMAGD OCCURS   20 TIMES .                              00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBMAGL     COMP PIC S9(4).                            00000360
      *--                                                                       
             03 MLIBMAGL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLIBMAGF     PIC X.                                     00000370
             03 FILLER  PIC X(4).                                       00000380
             03 MLIBMAGI     PIC X(20).                                 00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MZONCMDI  PIC X(15).                                      00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MLIBERRI  PIC X(58).                                      00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MCODTRAI  PIC X(4).                                       00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MCICSI    PIC X(5).                                       00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MNETNAMI  PIC X(8).                                       00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MSCREENI  PIC X(4).                                       00000630
      ***************************************************************** 00000640
      * SDF: EGG01   EGG01                                              00000650
      ***************************************************************** 00000660
       01   EGG01O REDEFINES EGG01I.                                    00000670
           02 FILLER    PIC X(12).                                      00000680
           02 FILLER    PIC X(2).                                       00000690
           02 MDATJOUA  PIC X.                                          00000700
           02 MDATJOUC  PIC X.                                          00000710
           02 MDATJOUP  PIC X.                                          00000720
           02 MDATJOUH  PIC X.                                          00000730
           02 MDATJOUV  PIC X.                                          00000740
           02 MDATJOUO  PIC X(10).                                      00000750
           02 FILLER    PIC X(2).                                       00000760
           02 MTIMJOUA  PIC X.                                          00000770
           02 MTIMJOUC  PIC X.                                          00000780
           02 MTIMJOUP  PIC X.                                          00000790
           02 MTIMJOUH  PIC X.                                          00000800
           02 MTIMJOUV  PIC X.                                          00000810
           02 MTIMJOUO  PIC X(5).                                       00000820
           02 FILLER    PIC X(2).                                       00000830
           02 MWFONCA   PIC X.                                          00000840
           02 MWFONCC   PIC X.                                          00000850
           02 MWFONCP   PIC X.                                          00000860
           02 MWFONCH   PIC X.                                          00000870
           02 MWFONCV   PIC X.                                          00000880
           02 MWFONCO   PIC X(3).                                       00000890
           02 FILLER    PIC X(2).                                       00000900
           02 MNCONCA   PIC X.                                          00000910
           02 MNCONCC   PIC X.                                          00000920
           02 MNCONCP   PIC X.                                          00000930
           02 MNCONCH   PIC X.                                          00000940
           02 MNCONCV   PIC X.                                          00000950
           02 MNCONCO   PIC X(4).                                       00000960
           02 FILLER    PIC X(2).                                       00000970
           02 MLENSGNA  PIC X.                                          00000980
           02 MLENSGNC  PIC X.                                          00000990
           02 MLENSGNP  PIC X.                                          00001000
           02 MLENSGNH  PIC X.                                          00001010
           02 MLENSGNV  PIC X.                                          00001020
           02 MLENSGNO  PIC X(15).                                      00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MLEMPLCA  PIC X.                                          00001050
           02 MLEMPLCC  PIC X.                                          00001060
           02 MLEMPLCP  PIC X.                                          00001070
           02 MLEMPLCH  PIC X.                                          00001080
           02 MLEMPLCV  PIC X.                                          00001090
           02 MLEMPLCO  PIC X(15).                                      00001100
           02 DFHMS1 OCCURS   20 TIMES .                                00001110
             03 FILLER       PIC X(2).                                  00001120
             03 MNMAGA  PIC X.                                          00001130
             03 MNMAGC  PIC X.                                          00001140
             03 MNMAGP  PIC X.                                          00001150
             03 MNMAGH  PIC X.                                          00001160
             03 MNMAGV  PIC X.                                          00001170
             03 MNMAGO  PIC X(3).                                       00001180
           02 DFHMS2 OCCURS   20 TIMES .                                00001190
             03 FILLER       PIC X(2).                                  00001200
             03 MLIBMAGA     PIC X.                                     00001210
             03 MLIBMAGC     PIC X.                                     00001220
             03 MLIBMAGP     PIC X.                                     00001230
             03 MLIBMAGH     PIC X.                                     00001240
             03 MLIBMAGV     PIC X.                                     00001250
             03 MLIBMAGO     PIC X(20).                                 00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MZONCMDA  PIC X.                                          00001280
           02 MZONCMDC  PIC X.                                          00001290
           02 MZONCMDP  PIC X.                                          00001300
           02 MZONCMDH  PIC X.                                          00001310
           02 MZONCMDV  PIC X.                                          00001320
           02 MZONCMDO  PIC X(15).                                      00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MLIBERRA  PIC X.                                          00001350
           02 MLIBERRC  PIC X.                                          00001360
           02 MLIBERRP  PIC X.                                          00001370
           02 MLIBERRH  PIC X.                                          00001380
           02 MLIBERRV  PIC X.                                          00001390
           02 MLIBERRO  PIC X(58).                                      00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MCODTRAA  PIC X.                                          00001420
           02 MCODTRAC  PIC X.                                          00001430
           02 MCODTRAP  PIC X.                                          00001440
           02 MCODTRAH  PIC X.                                          00001450
           02 MCODTRAV  PIC X.                                          00001460
           02 MCODTRAO  PIC X(4).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MCICSA    PIC X.                                          00001490
           02 MCICSC    PIC X.                                          00001500
           02 MCICSP    PIC X.                                          00001510
           02 MCICSH    PIC X.                                          00001520
           02 MCICSV    PIC X.                                          00001530
           02 MCICSO    PIC X(5).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MNETNAMA  PIC X.                                          00001560
           02 MNETNAMC  PIC X.                                          00001570
           02 MNETNAMP  PIC X.                                          00001580
           02 MNETNAMH  PIC X.                                          00001590
           02 MNETNAMV  PIC X.                                          00001600
           02 MNETNAMO  PIC X(8).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MSCREENA  PIC X.                                          00001630
           02 MSCREENC  PIC X.                                          00001640
           02 MSCREENP  PIC X.                                          00001650
           02 MSCREENH  PIC X.                                          00001660
           02 MSCREENV  PIC X.                                          00001670
           02 MSCREENO  PIC X(4).                                       00001680
                                                                                
