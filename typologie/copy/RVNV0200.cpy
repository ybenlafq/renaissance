      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVNV0200                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA VUE RVNV0200                           
      **********************************************************                
       01  RVNV0200.                                                            
      *---------------------  EMETTEUR                                          
           05  NV02-CEMETTEUR       PIC X(20).                                  
      *---------------------  DESTINATAIRE                                      
           05  NV02-CDESTI          PIC X(20).                                  
      *---------------------  DTD WEBSPHERE                                     
           05  NV02-CDTD            PIC X(20).                                  
      *---------------------  IDENTIFIANT DU MSG ENVOYE                         
           05  NV02-IDENTIFIANT     PIC X(12).                                  
      *---------------------  DATE D ENVOI DU MSG                               
           05  NV02-DENVOI          PIC X(08).                                  
      *---------------------  CODE RETOUR                                       
           05  NV02-CRETOUR         PIC X(02).                                  
      *---------------------  LIBELLE ANOMALIE                                  
           05  NV02-LIBANO          PIC X(20).                                  
      *---------------------  TIMESTAMP DE TRAITEMENT                           
           05  NV02-TIMESTAMP       PIC X(26).                                  
      *---------------------  NBRE DE MESSAGES REJETES                          
           05  NV02-QMSGREJET       PIC S9(7)V USAGE COMP-3.                    
      *---------------------  DSYST                                             
           05  NV02-DSYST           PIC S9(13)V USAGE COMP-3.                   
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVNV0200                                  
      **********************************************************                
       01  RVNV0200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV02-CEMETTEUR-F         PIC S9(4) COMP.                         
      *--                                                                       
           05  NV02-CEMETTEUR-F         PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV02-CDESTI-F            PIC S9(4) COMP.                         
      *--                                                                       
           05  NV02-CDESTI-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV02-CDTD-F              PIC S9(4) COMP.                         
      *--                                                                       
           05  NV02-CDTD-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV02-IDENTIFIANT-F       PIC S9(4) COMP.                         
      *--                                                                       
           05  NV02-IDENTIFIANT-F       PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV02-DENVOI-F            PIC S9(4) COMP.                         
      *--                                                                       
           05  NV02-DENVOI-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV02-CRETOUR-F           PIC S9(4) COMP.                         
      *--                                                                       
           05  NV02-CRETOUR-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV02-LIBANO-F            PIC S9(4) COMP.                         
      *--                                                                       
           05  NV02-LIBANO-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV02-TIMESTAMP-F         PIC S9(4) COMP.                         
      *--                                                                       
           05  NV02-TIMESTAMP-F         PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV02-QMSGREJET-F         PIC S9(4) COMP.                         
      *--                                                                       
           05  NV02-QMSGREJET-F         PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV02-DSYST-F             PIC S9(4) COMP.                         
      *                                                                         
      *--                                                                       
           05  NV02-DSYST-F             PIC S9(4) COMP-5.                       
                                                                                
      *}                                                                        
