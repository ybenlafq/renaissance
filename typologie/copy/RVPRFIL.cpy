      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PRFIL PARAMETRES PRTYP FILIALE         *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPRFIL.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPRFIL.                                                             
      *}                                                                        
           05  PRFIL-CTABLEG2    PIC X(15).                                     
           05  PRFIL-CTABLEG2-REDEF REDEFINES PRFIL-CTABLEG2.                   
               10  PRFIL-CTPREST         PIC X(05).                             
               10  PRFIL-CPARAM          PIC X(05).                             
               10  PRFIL-CLEPARAM        PIC X(05).                             
           05  PRFIL-WTABLEG     PIC X(80).                                     
           05  PRFIL-WTABLEG-REDEF  REDEFINES PRFIL-WTABLEG.                    
               10  PRFIL-LVPARAM         PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPRFIL-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPRFIL-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRFIL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PRFIL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRFIL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PRFIL-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
