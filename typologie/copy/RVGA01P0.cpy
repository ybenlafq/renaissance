      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HEASS ASSOCIATIONS CLIEUHET / CTRAIT   *        
      *----------------------------------------------------------------*        
       01  RVGA01P0.                                                            
           05  HEASS-CTABLEG2    PIC X(15).                                     
           05  HEASS-CTABLEG2-REDEF REDEFINES HEASS-CTABLEG2.                   
               10  HEASS-CLIEUHET        PIC X(05).                             
               10  HEASS-CTRAIT          PIC X(05).                             
           05  HEASS-WTABLEG     PIC X(80).                                     
           05  HEASS-WTABLEG-REDEF  REDEFINES HEASS-WTABLEG.                    
               10  HEASS-WACTIF          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01P0-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HEASS-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HEASS-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HEASS-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HEASS-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
