      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA DE LA TRANSACTION MP00                            *    00010100
      **************************************************************    00110000
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00120000
      **************************************************************    00130000
      *                                                                 00140000
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00150000
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00160000
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00170000
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00180000
      *                                                                 00190000
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00200000
      * COMPRENANT :                                                    00210000
      * 1 - LES ZONES RESERVEES A AIDA                                  00220000
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00230000
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00240000
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00250000
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00260000
      *                                                                 00270000
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00280000
      * PAR AIDA                                                        00290000
      *                                                                 00300000
      *-------------------------------------------------------------    00310000
      * 11/2002 MODIF PF01 AJOUT DE DEUX NOUVELLES ZONES                00320000
      *-------------------------------------------------------------    00310000
      *                                                                 00320000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-MP00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00330000
      *--                                                                       
       01  COM-MP00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00340000
       01  Z-COMMAREA.                                                  00350000
      *                                                                 00360000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00370000
          02 FILLER-COM-AIDA      PIC X(100).                           00380000
      *                                                                 00390000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00400000
          02 COMM-CICS-APPLID     PIC X(8).                             00410000
          02 COMM-CICS-NETNAM     PIC X(8).                             00420000
          02 COMM-CICS-TRANSA     PIC X(4).                             00430000
      *                                                                 00440000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00450000
          02 COMM-DATE-SIECLE     PIC XX.                               00460000
          02 COMM-DATE-ANNEE      PIC XX.                               00470000
          02 COMM-DATE-MOIS       PIC XX.                               00480000
          02 COMM-DATE-JOUR       PIC XX.                               00490000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00500000
          02 COMM-DATE-QNTA       PIC 999.                              00510000
          02 COMM-DATE-QNT0       PIC 99999.                            00520000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00530000
          02 COMM-DATE-BISX       PIC 9.                                00540000
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00550000
          02 COMM-DATE-JSM        PIC 9.                                00560000
      *   LIBELLES DU JOUR COURT - LONG                                 00570000
          02 COMM-DATE-JSM-LC     PIC XXX.                              00580000
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                         00590000
      *   LIBELLES DU MOIS COURT - LONG                                 00600000
          02 COMM-DATE-MOIS-LC    PIC XXX.                              00610000
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                         00620000
      *   DIFFERENTES FORMES DE DATE                                    00630000
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                             00640000
          02 COMM-DATE-AAMMJJ     PIC X(6).                             00650000
          02 COMM-DATE-JJMMSSAA   PIC X(8).                             00660000
          02 COMM-DATE-JJMMAA     PIC X(6).                             00670000
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                             00680000
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                            00690000
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00690010
          02 COMM-DATE-WEEK.                                            00690020
             05  COMM-DATE-SEMSS  PIC 99.                               00690030
             05  COMM-DATE-SEMAA  PIC 99.                               00690040
             05  COMM-DATE-SEMNU  PIC 99.                               00690050
          02 COMM-DATE-FILLER     PIC X(08).                            00690060
      *                                                                 00720000
      * ZONES RESERVEES TRAITEMENT DU SWAP -----------------------      00730000
      *                                                                 00740000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.              00750000
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 1 PIC X(1).                          00760000
      *                                                                 00770000
      * ZONES MESSAGE DU MODULE MMP00                    --------- 78   00780000
      *                                                                 00790000
          02 COMM-MP00-MESS           PIC X(78).                        00800000
      *                                                                 00810100
      * ZONES DONNEES SPECIFIQUES A LA TRANSACTION MP00 ----------      00820000
      *                                                                 00830000
          02 COMM-MP00-MENU.                                            00840000
      *                                                                 01230121
              03 COMM-MP00-ACID              PIC X(4).                  01230122
              03 COMM-MP00-NSOC              PIC X(3).                  00810050
      *       03 COMM-MP00-NMAG              PIC X(3).                  00810060
              03 COMM-MP00-FONC              PIC X(3).                  00810060
              03 COMM-MP00-CMOPAI            PIC X(5).                          
              03 COMM-MP00-CMPANC            PIC X(1).                          
              03 COMM-MP00-WTPRGT            PIC X(1).                          
              03 COMM-MP00-LMDPAI            PIC X(15).                         
              03 COMM-MP00-WINFCP            PIC X(1).                          
              03 COMM-MP00-WTCTRL            PIC X(1).                          
              03 COMM-MP00-WSEQED            PIC X(2).                          
              03 COMM-MP00-LMDPRB            PIC X(15).                         
              03 COMM-MP00-WDCMPT            PIC X(1).                          
              03 COMM-MP00-WMCTRL            PIC X(10).                         
              03 COMM-MP00-WCTFDC            PIC X(1).                          
              03 COMM-MP00-WRCOFF            PIC X(1).                          
              03 COMM-MP00-CMOPAC            PIC X(5).                          
              03 COMM-MP00-LVTIPA            PIC X(10).                         
              03 COMM-MP00-LRTIPA            PIC X(10).                         
              03 COMM-MP00-LUTIPA            PIC X(10).                         
PF01          03 COMM-MP00-LNBCHQ            PIC X(12).                         
PF01          03 COMM-MP00-FLAGS             PIC X(40).                         
      *                                                                 01230140
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3776  01230200
          02 COMM-MP00-APPLI.                                           01240000
      *------------------------------ ZONE COMMUNE                      01250000
             03 COMM-MP00-FILLER         PIC X(3695).                   01260394
      *---------------------------------------------                    01260800
          02 COMM-MP01-APPLI REDEFINES COMM-MP00-APPLI.                 01260900
             03 COMM-MP01-VALIDE-TRT     PIC X(1).                      01261000
             03 COMM-MP01-VALIDE-SUP     PIC 9(1).                      01261000
             03 COMM-MP01-FILLER         PIC X(3693).                   01261100
      *----------------------------------------------                   01261300
          02 COMM-MP03-APPLI REDEFINES COMM-MP00-APPLI.                 01261400
             03 COMM-MP03-PAGE           PIC 9(2).                      01261500
             03 COMM-MP03-PAGE-MAX       PIC 9(2).                      01261600
             03 COMM-MP03-FILLER         PIC X(3691).                   01261900
      *----------------------------------------------                   01262000
      *   02 COMM-MP20-APPLI REDEFINES COMM-MP00-APPLI.                 01262100
      *      03 COMM-MP20-PAGE           PIC 9(2) COMP-3.               01262200
      *      03 COMM-MP20-PAGE-MAX       PIC 9(2) COMP-3.               01262300
      ***************************************************************** 01270000
                                                                                
