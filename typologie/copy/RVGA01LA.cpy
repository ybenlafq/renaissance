      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MGCDK CODES MARKETING MGI              *        
      *----------------------------------------------------------------*        
       01  RVGA01LA.                                                            
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  MGCDK-CTABLEG2    PIC X(15).                                     
           05  MGCDK-CTABLEG2-REDEF REDEFINES MGCDK-CTABLEG2.                   
               10  MGCDK-CFAMMGI         PIC X(04).                             
               10  MGCDK-CMARKETI        PIC X(05).                             
           05  MGCDK-WTABLEG     PIC X(80).                                     
           05  MGCDK-WTABLEG-REDEF  REDEFINES MGCDK-WTABLEG.                    
               10  MGCDK-WACTIF          PIC X(01).                             
               10  MGCDK-NSEQ            PIC X(02).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  MGCDK-NSEQ-N         REDEFINES MGCDK-NSEQ                    
                                         PIC 9(02).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01LA-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MGCDK-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MGCDK-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MGCDK-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MGCDK-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
