      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PRDEF PROFIL DEL QUOTAS PAR DEFAUT     *        
      *----------------------------------------------------------------*        
       01  RVGA01YH.                                                            
           05  PRDEF-CTABLEG2    PIC X(15).                                     
           05  PRDEF-CTABLEG2-REDEF REDEFINES PRDEF-CTABLEG2.                   
               10  PRDEF-CPERIM          PIC X(05).                             
               10  PRDEF-SMN             PIC X(01).                             
               10  PRDEF-CTQUOTA         PIC X(01).                             
           05  PRDEF-WTABLEG     PIC X(80).                                     
           05  PRDEF-WTABLEG-REDEF  REDEFINES PRDEF-WTABLEG.                    
               10  PRDEF-CPRODEL         PIC X(05).                             
               10  PRDEF-QQUOTA          PIC X(05).                             
               10  PRDEF-QQUOTA-N       REDEFINES PRDEF-QQUOTA                  
                                         PIC 9(05).                             
               10  PRDEF-WILLIM          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01YH-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRDEF-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PRDEF-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRDEF-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PRDEF-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
