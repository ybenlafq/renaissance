           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FGTSV TRANSPO SERVICE PAR ETABLIS.     *        
      *----------------------------------------------------------------*        
       01  RVGA01OX.                                                            
           05  FGTSV-CTABLEG2    PIC X(15).                                     
           05  FGTSV-CTABLEG2-REDEF REDEFINES FGTSV-CTABLEG2.                   
               10  FGTSV-NSOCC           PIC X(03).                             
               10  FGTSV-NETAB           PIC X(03).                             
               10  FGTSV-SERVICE         PIC X(05).                             
               10  FGTSV-SENS            PIC X(01).                             
           05  FGTSV-WTABLEG     PIC X(80).                                     
           05  FGTSV-WTABLEG-REDEF  REDEFINES FGTSV-WTABLEG.                    
               10  FGTSV-WACTIF          PIC X(01).                             
               10  FGTSV-SERVCOR         PIC X(05).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01OX-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGTSV-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FGTSV-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGTSV-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FGTSV-WTABLEG-F   PIC S9(4) COMP-5.                              
      *}
           EXEC SQL END DECLARE SECTION END-EXEC.
 
                                                                                
