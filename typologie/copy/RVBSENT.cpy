      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE BSENT TYPES D ENTITES DE TYPOLOGIE     *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBSENT.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBSENT.                                                             
      *}                                                                        
           05  BSENT-CTABLEG2    PIC X(15).                                     
           05  BSENT-CTABLEG2-REDEF REDEFINES BSENT-CTABLEG2.                   
               10  BSENT-CTYPENT         PIC X(02).                             
           05  BSENT-WTABLEG     PIC X(80).                                     
           05  BSENT-WTABLEG-REDEF  REDEFINES BSENT-WTABLEG.                    
               10  BSENT-LTYPENT         PIC X(20).                             
               10  BSENT-CTABLE          PIC X(06).                             
               10  BSENT-WCFAM           PIC X(01).                             
               10  BSENT-NBLOC           PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  BSENT-NBLOC-N        REDEFINES BSENT-NBLOC                   
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  BSENT-CTRANS          PIC X(04).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBSENT-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBSENT-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BSENT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  BSENT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BSENT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  BSENT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
