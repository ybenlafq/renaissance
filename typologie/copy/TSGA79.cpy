      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *===============================================================* 00010000
      *                                                               * 00020000
      *      TS ASSOCIE A LA TRANSACTION GA79                         * 00030021
      *            TAXE ECO-MOBILIER                                  * 00040017
      *                                                               * 00050000
      *===============================================================* 00060000
                                                                        00070000
       01 TS-GA79.                                                      00080021
          05 TS-GA79-LONG              PIC S9(5) COMP-3     VALUE +111. 00090021
          05 TS-GA79-DONNEES.                                           00100021
             10 TS-GA79-NCODIC         PIC  X(7).                       00111021
             10 TS-GA79-NSEQ           PIC  X(1).                       00117021
             10 TS-GA79-CTAXE          PIC  X(5).                       00118021
             10 TS-GA79-DONNEES-O.                                      00120021
                15 TS-GA79-PAYS-O      PIC  X(2).                       00140021
                15 TS-GA79-CODFOU-O    PIC  X(5).                       00152021
      *--                                                    SSAAMMJJ   00160000
                15 TS-GA79-DEBEFF-O    PIC  X(8).                       00170021
                15 TS-GA79-FINEFF-O    PIC  X(8).                       00181021
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         15 TS-GA79-MONTAN-O    PIC  S9(5)V99 COMP.              00190021
      *--                                                                       
                15 TS-GA79-MONTAN-O    PIC  S9(5)V99 COMP-5.                    
      *}                                                                        
                15 TS-GA79-WIMPORTE-O  PIC  X(1).                       00190121
                15 TS-GA79-CODPRD-O    PIC  X(20).                      00191021
      * DONNEES DE L'ECRAN MISE A JOUR                                  00200000
             10 TS-GA79-DONNEES-N.                                      00210022
                15 TS-GA79-PAYS-N      PIC  X(2).                       00220021
                15 TS-GA79-CODFOU-N    PIC  X(5).                       00221021
      *--                                                    SSAAMMJJ   00222016
                15 TS-GA79-DEBEFF-N    PIC  X(8).                       00223021
                15 TS-GA79-FINEFF-N    PIC  X(8).                       00224021
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         15 TS-GA79-MONTAN-N    PIC  S9(5)V99 COMP.              00225021
      *--                                                                       
                15 TS-GA79-MONTAN-N    PIC  S9(5)V99 COMP-5.                    
      *}                                                                        
                15 TS-GA79-WIMPORTE-N  PIC  X(1).                       00225121
                15 TS-GA79-CODPRD-N    PIC  X(20).                      00226021
      * FLAG                                                            00282010
             10 TS-GA79-FLAG-ACTION    PIC  X(01) VALUE ' '.            00283021
                88 TS-GA79-NORMAL                 VALUE ' '.            00284021
                88 TS-GA79-CREATION               VALUE 'C'.            00285021
                88 TS-GA79-MAJ                    VALUE 'M'.            00286021
                88 TS-GA79-SUPPRESSION            VALUE 'S'.            00287021
                88 TS-GA79-SUPP-CRE               VALUE 'X'.            00288021
             10 TS-GA79-DONNEES-GA55   PIC  X(01).                      00289021
      *===============================================================* 00320000
                                                                                
