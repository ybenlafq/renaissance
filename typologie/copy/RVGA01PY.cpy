      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE QCTCA TYPES DE CARTES T                *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01PY.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01PY.                                                            
      *}                                                                        
           05  QCTCA-CTABLEG2    PIC X(15).                                     
           05  QCTCA-CTABLEG2-REDEF REDEFINES QCTCA-CTABLEG2.                   
               10  QCTCA-CTCARTE         PIC X(02).                             
           05  QCTCA-WTABLEG     PIC X(80).                                     
           05  QCTCA-WTABLEG-REDEF  REDEFINES QCTCA-WTABLEG.                    
               10  QCTCA-WACTIF          PIC X(01).                             
               10  QCTCA-LCARTE          PIC X(20).                             
               10  QCTCA-WACTIFS         PIC X(01).                             
               10  QCTCA-DELAI           PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  QCTCA-DELAI-N        REDEFINES QCTCA-DELAI                   
                                         PIC 9(02).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01PY-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01PY-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QCTCA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  QCTCA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QCTCA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  QCTCA-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
