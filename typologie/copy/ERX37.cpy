      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: Erx20                                                      00000020
      ***************************************************************** 00000030
       01   ERX37I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NCONCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 NCONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 NCONCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 NCONCI    PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LENSCONCL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 LENSCONCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 LENSCONCF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 LENSCONCI      PIC X(15).                                 00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NSOCL     COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 NSOCL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 NSOCF     PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 NSOCI     PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NMAGL     COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 NMAGL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 NMAGF     PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 NMAGI     PIC X(3).                                       00000290
           02 CRAYOND OCCURS   4 TIMES .                                00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 CRAYONL      COMP PIC S9(4).                            00000310
      *--                                                                       
             03 CRAYONL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 CRAYONF      PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 CRAYONI      PIC X(5).                                  00000340
           02 LRAYOND OCCURS   4 TIMES .                                00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 LRAYONL      COMP PIC S9(4).                            00000360
      *--                                                                       
             03 LRAYONL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 LRAYONF      PIC X.                                     00000370
             03 FILLER  PIC X(4).                                       00000380
             03 LRAYONI      PIC X(20).                                 00000390
           02 CRAYONRD OCCURS   15 TIMES .                              00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 CRAYONRL     COMP PIC S9(4).                            00000410
      *--                                                                       
             03 CRAYONRL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 CRAYONRF     PIC X.                                     00000420
             03 FILLER  PIC X(4).                                       00000430
             03 CRAYONRI     PIC X(5).                                  00000440
           02 LRAYONRD OCCURS   15 TIMES .                              00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 LRAYONRL     COMP PIC S9(4).                            00000460
      *--                                                                       
             03 LRAYONRL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 LRAYONRF     PIC X.                                     00000470
             03 FILLER  PIC X(4).                                       00000480
             03 LRAYONRI     PIC X(20).                                 00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLIBERRI  PIC X(78).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCODTRAI  PIC X(4).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCICSI    PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNETNAMI  PIC X(8).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MSCREENI  PIC X(4).                                       00000690
      ***************************************************************** 00000700
      * SDF: Erx20                                                      00000710
      ***************************************************************** 00000720
       01   ERX37O REDEFINES ERX37I.                                    00000730
           02 FILLER    PIC X(12).                                      00000740
           02 FILLER    PIC X(2).                                       00000750
           02 MDATJOUA  PIC X.                                          00000760
           02 MDATJOUC  PIC X.                                          00000770
           02 MDATJOUP  PIC X.                                          00000780
           02 MDATJOUH  PIC X.                                          00000790
           02 MDATJOUV  PIC X.                                          00000800
           02 MDATJOUO  PIC X(10).                                      00000810
           02 FILLER    PIC X(2).                                       00000820
           02 MTIMJOUA  PIC X.                                          00000830
           02 MTIMJOUC  PIC X.                                          00000840
           02 MTIMJOUP  PIC X.                                          00000850
           02 MTIMJOUH  PIC X.                                          00000860
           02 MTIMJOUV  PIC X.                                          00000870
           02 MTIMJOUO  PIC X(5).                                       00000880
           02 FILLER    PIC X(2).                                       00000890
           02 NCONCA    PIC X.                                          00000900
           02 NCONCC    PIC X.                                          00000910
           02 NCONCP    PIC X.                                          00000920
           02 NCONCH    PIC X.                                          00000930
           02 NCONCV    PIC X.                                          00000940
           02 NCONCO    PIC X(4).                                       00000950
           02 FILLER    PIC X(2).                                       00000960
           02 LENSCONCA      PIC X.                                     00000970
           02 LENSCONCC PIC X.                                          00000980
           02 LENSCONCP PIC X.                                          00000990
           02 LENSCONCH PIC X.                                          00001000
           02 LENSCONCV PIC X.                                          00001010
           02 LENSCONCO      PIC X(15).                                 00001020
           02 FILLER    PIC X(2).                                       00001030
           02 NSOCA     PIC X.                                          00001040
           02 NSOCC     PIC X.                                          00001050
           02 NSOCP     PIC X.                                          00001060
           02 NSOCH     PIC X.                                          00001070
           02 NSOCV     PIC X.                                          00001080
           02 NSOCO     PIC X(3).                                       00001090
           02 FILLER    PIC X(2).                                       00001100
           02 NMAGA     PIC X.                                          00001110
           02 NMAGC     PIC X.                                          00001120
           02 NMAGP     PIC X.                                          00001130
           02 NMAGH     PIC X.                                          00001140
           02 NMAGV     PIC X.                                          00001150
           02 NMAGO     PIC X(3).                                       00001160
           02 DFHMS1 OCCURS   4 TIMES .                                 00001170
             03 FILLER       PIC X(2).                                  00001180
             03 CRAYONA      PIC X.                                     00001190
             03 CRAYONC PIC X.                                          00001200
             03 CRAYONP PIC X.                                          00001210
             03 CRAYONH PIC X.                                          00001220
             03 CRAYONV PIC X.                                          00001230
             03 CRAYONO      PIC X(5).                                  00001240
           02 DFHMS2 OCCURS   4 TIMES .                                 00001250
             03 FILLER       PIC X(2).                                  00001260
             03 LRAYONA      PIC X.                                     00001270
             03 LRAYONC PIC X.                                          00001280
             03 LRAYONP PIC X.                                          00001290
             03 LRAYONH PIC X.                                          00001300
             03 LRAYONV PIC X.                                          00001310
             03 LRAYONO      PIC X(20).                                 00001320
           02 DFHMS3 OCCURS   15 TIMES .                                00001330
             03 FILLER       PIC X(2).                                  00001340
             03 CRAYONRA     PIC X.                                     00001350
             03 CRAYONRC     PIC X.                                     00001360
             03 CRAYONRP     PIC X.                                     00001370
             03 CRAYONRH     PIC X.                                     00001380
             03 CRAYONRV     PIC X.                                     00001390
             03 CRAYONRO     PIC X(5).                                  00001400
           02 DFHMS4 OCCURS   15 TIMES .                                00001410
             03 FILLER       PIC X(2).                                  00001420
             03 LRAYONRA     PIC X.                                     00001430
             03 LRAYONRC     PIC X.                                     00001440
             03 LRAYONRP     PIC X.                                     00001450
             03 LRAYONRH     PIC X.                                     00001460
             03 LRAYONRV     PIC X.                                     00001470
             03 LRAYONRO     PIC X(20).                                 00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MLIBERRA  PIC X.                                          00001500
           02 MLIBERRC  PIC X.                                          00001510
           02 MLIBERRP  PIC X.                                          00001520
           02 MLIBERRH  PIC X.                                          00001530
           02 MLIBERRV  PIC X.                                          00001540
           02 MLIBERRO  PIC X(78).                                      00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MCODTRAA  PIC X.                                          00001570
           02 MCODTRAC  PIC X.                                          00001580
           02 MCODTRAP  PIC X.                                          00001590
           02 MCODTRAH  PIC X.                                          00001600
           02 MCODTRAV  PIC X.                                          00001610
           02 MCODTRAO  PIC X(4).                                       00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MCICSA    PIC X.                                          00001640
           02 MCICSC    PIC X.                                          00001650
           02 MCICSP    PIC X.                                          00001660
           02 MCICSH    PIC X.                                          00001670
           02 MCICSV    PIC X.                                          00001680
           02 MCICSO    PIC X(5).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MNETNAMA  PIC X.                                          00001710
           02 MNETNAMC  PIC X.                                          00001720
           02 MNETNAMP  PIC X.                                          00001730
           02 MNETNAMH  PIC X.                                          00001740
           02 MNETNAMV  PIC X.                                          00001750
           02 MNETNAMO  PIC X(8).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MSCREENA  PIC X.                                          00001780
           02 MSCREENC  PIC X.                                          00001790
           02 MSCREENP  PIC X.                                          00001800
           02 MSCREENH  PIC X.                                          00001810
           02 MSCREENV  PIC X.                                          00001820
           02 MSCREENO  PIC X(4).                                       00001830
                                                                                
