      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGG06   EGG06                                              00000020
      ***************************************************************** 00000030
       01   EGG06I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHEFPL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCHEFPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCHEFPF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCHEFPI   PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCHEFPL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLCHEFPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCHEFPF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLCHEFPI  PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNCODICI  PIC X(7).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLREFI    PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCMARQI   PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCFAMI    PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLFAMI    PIC X(20).                                      00000490
           02 MWALIGNI OCCURS   11 TIMES .                              00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWZONPL      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MWZONPL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MWZONPF      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MWZONPI      PIC X(2).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCONCL      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNCONCL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNCONCF      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNCONCI      PIC X(4).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLENSGNL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MLENSGNL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLENSGNF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MLENSGNI     PIC X(15).                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLEMPLCL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MLEMPLCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLEMPLCF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MLEMPLCI     PIC X(15).                                 00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNALIGNL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MNALIGNL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNALIGNF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNALIGNI     PIC X(7).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATRELL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MDATRELL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDATRELF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MDATRELI     PIC X(8).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIRELL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MPRIRELL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPRIRELF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MPRIRELI     PIC X(6).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIDTYL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MPRIDTYL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPRIDTYF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MPRIDTYI     PIC X(6).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODDECL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MCODDECL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCODDECF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MCODDECI     PIC X(2).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MZONCMDI  PIC X(15).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MLIBERRI  PIC X(58).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCODTRAI  PIC X(4).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCICSI    PIC X(5).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MNETNAMI  PIC X(8).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MSCREENI  PIC X(4).                                       00001100
      ***************************************************************** 00001110
      * SDF: EGG06   EGG06                                              00001120
      ***************************************************************** 00001130
       01   EGG06O REDEFINES EGG06I.                                    00001140
           02 FILLER    PIC X(12).                                      00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MDATJOUA  PIC X.                                          00001170
           02 MDATJOUC  PIC X.                                          00001180
           02 MDATJOUP  PIC X.                                          00001190
           02 MDATJOUH  PIC X.                                          00001200
           02 MDATJOUV  PIC X.                                          00001210
           02 MDATJOUO  PIC X(10).                                      00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MTIMJOUA  PIC X.                                          00001240
           02 MTIMJOUC  PIC X.                                          00001250
           02 MTIMJOUP  PIC X.                                          00001260
           02 MTIMJOUH  PIC X.                                          00001270
           02 MTIMJOUV  PIC X.                                          00001280
           02 MTIMJOUO  PIC X(5).                                       00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MWPAGEA   PIC X.                                          00001310
           02 MWPAGEC   PIC X.                                          00001320
           02 MWPAGEP   PIC X.                                          00001330
           02 MWPAGEH   PIC X.                                          00001340
           02 MWPAGEV   PIC X.                                          00001350
           02 MWPAGEO   PIC X(3).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MWFONCA   PIC X.                                          00001380
           02 MWFONCC   PIC X.                                          00001390
           02 MWFONCP   PIC X.                                          00001400
           02 MWFONCH   PIC X.                                          00001410
           02 MWFONCV   PIC X.                                          00001420
           02 MWFONCO   PIC X(3).                                       00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MCHEFPA   PIC X.                                          00001450
           02 MCHEFPC   PIC X.                                          00001460
           02 MCHEFPP   PIC X.                                          00001470
           02 MCHEFPH   PIC X.                                          00001480
           02 MCHEFPV   PIC X.                                          00001490
           02 MCHEFPO   PIC X(5).                                       00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MLCHEFPA  PIC X.                                          00001520
           02 MLCHEFPC  PIC X.                                          00001530
           02 MLCHEFPP  PIC X.                                          00001540
           02 MLCHEFPH  PIC X.                                          00001550
           02 MLCHEFPV  PIC X.                                          00001560
           02 MLCHEFPO  PIC X(20).                                      00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MNCODICA  PIC X.                                          00001590
           02 MNCODICC  PIC X.                                          00001600
           02 MNCODICP  PIC X.                                          00001610
           02 MNCODICH  PIC X.                                          00001620
           02 MNCODICV  PIC X.                                          00001630
           02 MNCODICO  PIC X(7).                                       00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MLREFA    PIC X.                                          00001660
           02 MLREFC    PIC X.                                          00001670
           02 MLREFP    PIC X.                                          00001680
           02 MLREFH    PIC X.                                          00001690
           02 MLREFV    PIC X.                                          00001700
           02 MLREFO    PIC X(20).                                      00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MCMARQA   PIC X.                                          00001730
           02 MCMARQC   PIC X.                                          00001740
           02 MCMARQP   PIC X.                                          00001750
           02 MCMARQH   PIC X.                                          00001760
           02 MCMARQV   PIC X.                                          00001770
           02 MCMARQO   PIC X(5).                                       00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MCFAMA    PIC X.                                          00001800
           02 MCFAMC    PIC X.                                          00001810
           02 MCFAMP    PIC X.                                          00001820
           02 MCFAMH    PIC X.                                          00001830
           02 MCFAMV    PIC X.                                          00001840
           02 MCFAMO    PIC X(5).                                       00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MLFAMA    PIC X.                                          00001870
           02 MLFAMC    PIC X.                                          00001880
           02 MLFAMP    PIC X.                                          00001890
           02 MLFAMH    PIC X.                                          00001900
           02 MLFAMV    PIC X.                                          00001910
           02 MLFAMO    PIC X(20).                                      00001920
           02 MWALIGNO OCCURS   11 TIMES .                              00001930
             03 FILLER       PIC X(2).                                  00001940
             03 MWZONPA      PIC X.                                     00001950
             03 MWZONPC PIC X.                                          00001960
             03 MWZONPP PIC X.                                          00001970
             03 MWZONPH PIC X.                                          00001980
             03 MWZONPV PIC X.                                          00001990
             03 MWZONPO      PIC X(2).                                  00002000
             03 FILLER       PIC X(2).                                  00002010
             03 MNCONCA      PIC X.                                     00002020
             03 MNCONCC PIC X.                                          00002030
             03 MNCONCP PIC X.                                          00002040
             03 MNCONCH PIC X.                                          00002050
             03 MNCONCV PIC X.                                          00002060
             03 MNCONCO      PIC X(4).                                  00002070
             03 FILLER       PIC X(2).                                  00002080
             03 MLENSGNA     PIC X.                                     00002090
             03 MLENSGNC     PIC X.                                     00002100
             03 MLENSGNP     PIC X.                                     00002110
             03 MLENSGNH     PIC X.                                     00002120
             03 MLENSGNV     PIC X.                                     00002130
             03 MLENSGNO     PIC X(15).                                 00002140
             03 FILLER       PIC X(2).                                  00002150
             03 MLEMPLCA     PIC X.                                     00002160
             03 MLEMPLCC     PIC X.                                     00002170
             03 MLEMPLCP     PIC X.                                     00002180
             03 MLEMPLCH     PIC X.                                     00002190
             03 MLEMPLCV     PIC X.                                     00002200
             03 MLEMPLCO     PIC X(15).                                 00002210
             03 FILLER       PIC X(2).                                  00002220
             03 MNALIGNA     PIC X.                                     00002230
             03 MNALIGNC     PIC X.                                     00002240
             03 MNALIGNP     PIC X.                                     00002250
             03 MNALIGNH     PIC X.                                     00002260
             03 MNALIGNV     PIC X.                                     00002270
             03 MNALIGNO     PIC X(7).                                  00002280
             03 FILLER       PIC X(2).                                  00002290
             03 MDATRELA     PIC X.                                     00002300
             03 MDATRELC     PIC X.                                     00002310
             03 MDATRELP     PIC X.                                     00002320
             03 MDATRELH     PIC X.                                     00002330
             03 MDATRELV     PIC X.                                     00002340
             03 MDATRELO     PIC X(8).                                  00002350
             03 FILLER       PIC X(2).                                  00002360
             03 MPRIRELA     PIC X.                                     00002370
             03 MPRIRELC     PIC X.                                     00002380
             03 MPRIRELP     PIC X.                                     00002390
             03 MPRIRELH     PIC X.                                     00002400
             03 MPRIRELV     PIC X.                                     00002410
             03 MPRIRELO     PIC X(6).                                  00002420
             03 FILLER       PIC X(2).                                  00002430
             03 MPRIDTYA     PIC X.                                     00002440
             03 MPRIDTYC     PIC X.                                     00002450
             03 MPRIDTYP     PIC X.                                     00002460
             03 MPRIDTYH     PIC X.                                     00002470
             03 MPRIDTYV     PIC X.                                     00002480
             03 MPRIDTYO     PIC X(6).                                  00002490
             03 FILLER       PIC X(2).                                  00002500
             03 MCODDECA     PIC X.                                     00002510
             03 MCODDECC     PIC X.                                     00002520
             03 MCODDECP     PIC X.                                     00002530
             03 MCODDECH     PIC X.                                     00002540
             03 MCODDECV     PIC X.                                     00002550
             03 MCODDECO     PIC X(2).                                  00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MZONCMDA  PIC X.                                          00002580
           02 MZONCMDC  PIC X.                                          00002590
           02 MZONCMDP  PIC X.                                          00002600
           02 MZONCMDH  PIC X.                                          00002610
           02 MZONCMDV  PIC X.                                          00002620
           02 MZONCMDO  PIC X(15).                                      00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MLIBERRA  PIC X.                                          00002650
           02 MLIBERRC  PIC X.                                          00002660
           02 MLIBERRP  PIC X.                                          00002670
           02 MLIBERRH  PIC X.                                          00002680
           02 MLIBERRV  PIC X.                                          00002690
           02 MLIBERRO  PIC X(58).                                      00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MCODTRAA  PIC X.                                          00002720
           02 MCODTRAC  PIC X.                                          00002730
           02 MCODTRAP  PIC X.                                          00002740
           02 MCODTRAH  PIC X.                                          00002750
           02 MCODTRAV  PIC X.                                          00002760
           02 MCODTRAO  PIC X(4).                                       00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MCICSA    PIC X.                                          00002790
           02 MCICSC    PIC X.                                          00002800
           02 MCICSP    PIC X.                                          00002810
           02 MCICSH    PIC X.                                          00002820
           02 MCICSV    PIC X.                                          00002830
           02 MCICSO    PIC X(5).                                       00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MNETNAMA  PIC X.                                          00002860
           02 MNETNAMC  PIC X.                                          00002870
           02 MNETNAMP  PIC X.                                          00002880
           02 MNETNAMH  PIC X.                                          00002890
           02 MNETNAMV  PIC X.                                          00002900
           02 MNETNAMO  PIC X(8).                                       00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MSCREENA  PIC X.                                          00002930
           02 MSCREENC  PIC X.                                          00002940
           02 MSCREENP  PIC X.                                          00002950
           02 MSCREENH  PIC X.                                          00002960
           02 MSCREENV  PIC X.                                          00002970
           02 MSCREENO  PIC X(4).                                       00002980
                                                                                
