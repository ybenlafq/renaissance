      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CUTIL CODES UTILISATEUR                *        
      *----------------------------------------------------------------*        
       01  RVGA01B.                                                             
           05  CUTIL-CTABLEG2    PIC X(15).                                     
           05  CUTIL-CTABLEG2-REDEF REDEFINES CUTIL-CTABLEG2.                   
               10  CUTIL-CUTIL           PIC X(05).                             
           05  CUTIL-WTABLEG     PIC X(80).                                     
           05  CUTIL-WTABLEG-REDEF  REDEFINES CUTIL-WTABLEG.                    
               10  CUTIL-LCUTIL          PIC X(20).                             
               10  CUTIL-CPROG           PIC X(06).                             
               10  CUTIL-WACTIVE         PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01B-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CUTIL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CUTIL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CUTIL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CUTIL-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
