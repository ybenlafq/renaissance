      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE NCGFC AIGUILLAGE DES TRANSACTIONS      *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01FI.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01FI.                                                            
      *}                                                                        
           05  NCGFC-CTABLEG2    PIC X(15).                                     
           05  NCGFC-CTABLEG2-REDEF REDEFINES NCGFC-CTABLEG2.                   
               10  NCGFC-CPROG           PIC X(05).                             
           05  NCGFC-WTABLEG     PIC X(80).                                     
           05  NCGFC-WTABLEG-REDEF  REDEFINES NCGFC-WTABLEG.                    
               10  NCGFC-WFLAG           PIC X(01).                             
               10  NCGFC-QPVUNIT         PIC X(03).                             
               10  NCGFC-COMMENT         PIC X(15).                             
               10  NCGFC-WFLAG2          PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01FI-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01FI-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NCGFC-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  NCGFC-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NCGFC-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  NCGFC-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
