      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GATRA TRANCHES DE CODICS POUR GA50     *        
      *----------------------------------------------------------------*        
       01  RVGA01PC.                                                            
           05  GATRA-CTABLEG2    PIC X(15).                                     
           05  GATRA-CTABLEG2-REDEF REDEFINES GATRA-CTABLEG2.                   
               10  GATRA-SOCIETE         PIC X(05).                             
           05  GATRA-WTABLEG     PIC X(80).                                     
           05  GATRA-WTABLEG-REDEF  REDEFINES GATRA-WTABLEG.                    
               10  GATRA-TRANCHED        PIC X(07).                             
               10  GATRA-TRANCHED-N     REDEFINES GATRA-TRANCHED                
                                         PIC 9(07).                             
               10  GATRA-TRANCHEF        PIC X(07).                             
               10  GATRA-TRANCHEF-N     REDEFINES GATRA-TRANCHEF                
                                         PIC 9(07).                             
               10  GATRA-WACTIF          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01PC-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GATRA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GATRA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GATRA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GATRA-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
