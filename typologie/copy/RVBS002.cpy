      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE BS002 TYPES/NATURES INTERFACE VENTE    *        
      *----------------------------------------------------------------*        
       01  RVBS002.                                                             
           05  BS002-CTABLEG2    PIC X(15).                                     
           05  BS002-CTABLEG2-REDEF REDEFINES BS002-CTABLEG2.                   
               10  BS002-CODE            PIC X(01).                             
               10  BS002-TYPE            PIC X(05).                             
               10  BS002-CODES           PIC X(05).                             
           05  BS002-WTABLEG     PIC X(80).                                     
           05  BS002-WTABLEG-REDEF  REDEFINES BS002-WTABLEG.                    
               10  BS002-LIBELLE         PIC X(20).                             
               10  BS002-CTYPNAT         PIC X(10).                             
               10  BS002-WTVA            PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVBS002-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BS002-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  BS002-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BS002-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  BS002-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
