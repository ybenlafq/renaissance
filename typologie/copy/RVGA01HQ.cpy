      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TRACK TYPE DE CONTENEUR RACK           *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01HQ.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01HQ.                                                            
      *}                                                                        
           05  TRACK-CTABLEG2    PIC X(15).                                     
           05  TRACK-CTABLEG2-REDEF REDEFINES TRACK-CTABLEG2.                   
               10  TRACK-CTRACK          PIC X(01).                             
           05  TRACK-WTABLEG     PIC X(80).                                     
           05  TRACK-WTABLEG-REDEF  REDEFINES TRACK-WTABLEG.                    
               10  TRACK-LTRACK          PIC X(20).                             
               10  TRACK-QCAPCONT        PIC S9(03)V9(02)     COMP-3.           
               10  TRACK-QEQPAL          PIC S9(03)V9(02)     COMP-3.           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01HQ-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01HQ-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TRACK-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TRACK-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TRACK-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TRACK-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
