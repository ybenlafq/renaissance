      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TRX10 (MENU)             TR: RX10  *    00020000
      *                          TRX11                             *    00040000
      *                           TRX12                            *    00050000
      *                            TRX13                           *            
      *                             TRX14                          *    00060000
      *                              TRX16                         *    00070000
      *                               TRX17                        *    00070000
      *                                TRX18                       *    00080000
      *                                 TRX19                      *    00090000
      *           POUR L'ADMINISTATION DES DONNEES                 *    00100000
      **************************************************************    00110000
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00120000
      **************************************************************    00130000
      *                                                                 00140000
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00150000
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00160000
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00170000
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00180000
      *                                                                 00190000
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00200000
      * COMPRENANT :                                                    00210000
      * 1 - LES ZONES RESERVEES A AIDA                                  00220000
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00230000
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00240000
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00250000
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00260000
      *                                                                 00270000
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00280000
      * PAR AIDA                                                        00290000
      *                                                                 00300000
      *-------------------------------------------------------------    00310000
      *                                                                 00320000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-RX10-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00330000
      *--                                                                       
       01  COM-RX10-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00340000
       01  Z-COMMAREA.                                                  00350000
      *                                                                 00360000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00370000
          02 FILLER-COM-AIDA      PIC X(100).                           00380000
      *                                                                 00390000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00400000
          02 COMM-CICS-APPLID     PIC X(8).                             00410000
          02 COMM-CICS-NETNAM     PIC X(8).                             00420000
          02 COMM-CICS-TRANSA     PIC X(4).                             00430000
      *                                                                 00440000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00450000
          02 COMM-DATE-SIECLE     PIC XX.                               00460000
          02 COMM-DATE-ANNEE      PIC XX.                               00470000
          02 COMM-DATE-MOIS       PIC XX.                               00480000
          02 COMM-DATE-JOUR       PIC XX.                               00490000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00500000
          02 COMM-DATE-QNTA       PIC 999.                              00510000
          02 COMM-DATE-QNT0       PIC 99999.                            00520000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00530000
          02 COMM-DATE-BISX       PIC 9.                                00540000
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00550000
          02 COMM-DATE-JSM        PIC 9.                                00560000
      *   LIBELLES DU JOUR COURT - LONG                                 00570000
          02 COMM-DATE-JSM-LC     PIC XXX.                              00580000
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                         00590000
      *   LIBELLES DU MOIS COURT - LONG                                 00600000
          02 COMM-DATE-MOIS-LC    PIC XXX.                              00610000
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                         00620000
      *   DIFFERENTES FORMES DE DATE                                    00630000
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                             00640000
          02 COMM-DATE-AAMMJJ     PIC X(6).                             00650000
          02 COMM-DATE-JJMMSSAA   PIC X(8).                             00660000
          02 COMM-DATE-JJMMAA     PIC X(6).                             00670000
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                             00680000
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                            00690000
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00690010
          02 COMM-DATE-WEEK.                                            00690020
             05  COMM-DATE-SEMSS  PIC 99.                               00690030
             05  COMM-DATE-SEMAA  PIC 99.                               00690040
             05  COMM-DATE-SEMNU  PIC 99.                               00690050
          02 COMM-DATE-FILLER     PIC X(08).                            00690060
      *                                                                 00720000
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 153  00730000
      *                                                                 00740000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.              00750000
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                        00760000
      *                                                                 00810000
      * ZONES COMMUNES DONNEES SPECIFIQUES A RELEVE DE PRIX ------ 172  00820000
      *                                                                 00830000
          02 COMM-RX10-COMMUM.                                          00840000
              03 COMM-RX10-NSOC              PIC X(3).                  00860000
              03 COMM-RX10-NMAG              PIC X(3).                  00860000
              03 COMM-RX10-LMAG              PIC X(20).                 01090100
              03 COMM-RX10-SIEGE             PIC X(1).                  00860000
              03 COMM-RX10-NZONPRIX          PIC X(2).                  00860000
              03 COMM-RX10-NCONC             PIC X(4).                  00860000
              03 COMM-RX10-LENSCONC          PIC X(15).                 00860000
              03 COMM-RX10-CPROFIL           PIC X(5).                  00860000
              03 COMM-RX10-WFONC             PIC X(3).                  00860000
              03 COMM-RX10-TRANS-SUP         PIC X(5).                  00860000
              03 COMM-RX10-TRANS-GEN         PIC X(5).                  00860000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-RX10-PAGE              PIC S9(4) COMP.            01290210
      *--                                                                       
              03 COMM-RX10-PAGE              PIC S9(4) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-RX10-NBP               PIC S9(4) COMP.            01290210
      *--                                                                       
              03 COMM-RX10-NBP               PIC S9(4) COMP-5.                  
      *}                                                                        
              03 COMM-RX10-SUITE             PIC X(100).                00860000
      *                                                                 01000000
      * ZONES RESERVEES APPLICATIVES --------------- 28 + 3523          01230200
      *                                                                 01000000
          02 COMM-RX10-APPLI.                                           01240000
             03 COMM-RX10-ZONCMD         PIC X(15).                     01040000
             03 COMM-RX10-PRG            PIC X(5).                      01260200
             03 COMM-RX10-TOP            PIC X(1).                      01260300
             03 COMM-RX10-NFREQUENCE     PIC X(1).                      01100000
             03 COMM-RX10-NUMSEM         PIC X(6).                      01100100
             03 COMM-RX10-FILLER         PIC X(3523).                       0126
      ***************************************************************** 01270000
                                                                                
