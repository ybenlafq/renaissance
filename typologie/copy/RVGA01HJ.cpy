      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TNOM  TITRE DU NOM  MME,MR,MLLE        *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01HJ.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01HJ.                                                            
      *}                                                                        
           05  TNOM-CTABLEG2     PIC X(15).                                     
           05  TNOM-CTABLEG2-REDEF  REDEFINES TNOM-CTABLEG2.                    
               10  TNOM-TNOM             PIC X(05).                             
           05  TNOM-WTABLEG      PIC X(80).                                     
           05  TNOM-WTABLEG-REDEF   REDEFINES TNOM-WTABLEG.                     
               10  TNOM-LNOM             PIC X(20).                             
               10  TNOM-CAPPEL           PIC X(01).                             
               10  TNOM-WSOCIETE         PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01HJ-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01HJ-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TNOM-CTABLEG2-F   PIC S9(4)  COMP.                               
      *--                                                                       
           05  TNOM-CTABLEG2-F   PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TNOM-WTABLEG-F    PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TNOM-WTABLEG-F    PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
