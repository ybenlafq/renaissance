      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SLOPM NMD/SL : OPERATIONS PAR MAG/PR   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01Y8.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01Y8.                                                            
      *}                                                                        
           05  SLOPM-CTABLEG2    PIC X(15).                                     
           05  SLOPM-CTABLEG2-REDEF REDEFINES SLOPM-CTABLEG2.                   
               10  SLOPM-COPSL           PIC X(05).                             
               10  SLOPM-WMAGPR          PIC X(01).                             
               10  SLOPM-CMAGPR          PIC X(06).                             
           05  SLOPM-WTABLEG     PIC X(80).                                     
           05  SLOPM-WTABLEG-REDEF  REDEFINES SLOPM-WTABLEG.                    
               10  SLOPM-WAUT            PIC X(01).                             
               10  SLOPM-WARB            PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01Y8-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01Y8-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLOPM-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SLOPM-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLOPM-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SLOPM-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
