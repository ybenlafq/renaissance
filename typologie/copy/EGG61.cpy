      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGG61   EGG61                                              00000020
      ***************************************************************** 00000030
       01   EGG61I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVISEL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MCDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEVISEF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCDEVISEI      PIC X(3).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEVISEL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MLDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLDEVISEF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLDEVISEI      PIC X(25).                                 00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCODICI  PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTCDEL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MNENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNENTCDEF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNENTCDEI      PIC X(5).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTCDEL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MLENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLENTCDEF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLENTCDEI      PIC X(16).                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRECL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNRECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNRECF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNRECI    PIC X(7).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLREFFOURNI    PIC X(16).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRECL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDRECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDRECF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDRECI    PIC X(8).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCMARQI   PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEL    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCDEF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNCDEI    PIC X(7).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCFAMI    PIC X(16).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQRECL    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MQRECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MQRECF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MQRECI    PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQPRAL    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MQPRAL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MQPRAF    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MQPRAI    PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRAL     COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MPRAL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MPRAF     PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MPRAI     PIC X(9).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVDARTYL    COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MCDEVDARTYL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCDEVDARTYF    PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCDEVDARTYI    PIC X(3).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRADARTYL     COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MPRADARTYL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MPRADARTYF     PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MPRADARTYI     PIC X(9).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRAANCL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MPRAANCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPRAANCF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MPRAANCI  PIC X(9).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRMPANCL      COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MPRMPANCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPRMPANCF      PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MPRMPANCI      PIC X(13).                                 00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVFNRL      COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MCDEVFNRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEVFNRF      PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCDEVFNRI      PIC X(3).                                  00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRMPANCFNL    COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MPRMPANCFNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MPRMPANCFNF    PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MPRMPANCFNI    PIC X(13).                                 00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICSUIL    COMP PIC S9(4).                            00000940
      *--                                                                       
           02 MNCODICSUIL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNCODICSUIF    PIC X.                                     00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MNCODICSUII    PIC X(7).                                  00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MZONCMDI  PIC X(15).                                      00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MLIBERRI  PIC X(58).                                      00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MCODTRAI  PIC X(4).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MCICSI    PIC X(5).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MNETNAMI  PIC X(8).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MSCREENI  PIC X(4).                                       00001210
      ***************************************************************** 00001220
      * SDF: EGG61   EGG61                                              00001230
      ***************************************************************** 00001240
       01   EGG61O REDEFINES EGG61I.                                    00001250
           02 FILLER    PIC X(12).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MDATJOUA  PIC X.                                          00001280
           02 MDATJOUC  PIC X.                                          00001290
           02 MDATJOUP  PIC X.                                          00001300
           02 MDATJOUH  PIC X.                                          00001310
           02 MDATJOUV  PIC X.                                          00001320
           02 MDATJOUO  PIC X(10).                                      00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MTIMJOUA  PIC X.                                          00001350
           02 MTIMJOUC  PIC X.                                          00001360
           02 MTIMJOUP  PIC X.                                          00001370
           02 MTIMJOUH  PIC X.                                          00001380
           02 MTIMJOUV  PIC X.                                          00001390
           02 MTIMJOUO  PIC X(5).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MCDEVISEA      PIC X.                                     00001420
           02 MCDEVISEC PIC X.                                          00001430
           02 MCDEVISEP PIC X.                                          00001440
           02 MCDEVISEH PIC X.                                          00001450
           02 MCDEVISEV PIC X.                                          00001460
           02 MCDEVISEO      PIC X(3).                                  00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MLDEVISEA      PIC X.                                     00001490
           02 MLDEVISEC PIC X.                                          00001500
           02 MLDEVISEP PIC X.                                          00001510
           02 MLDEVISEH PIC X.                                          00001520
           02 MLDEVISEV PIC X.                                          00001530
           02 MLDEVISEO      PIC X(25).                                 00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MNCODICA  PIC X.                                          00001560
           02 MNCODICC  PIC X.                                          00001570
           02 MNCODICP  PIC X.                                          00001580
           02 MNCODICH  PIC X.                                          00001590
           02 MNCODICV  PIC X.                                          00001600
           02 MNCODICO  PIC X(7).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MNENTCDEA      PIC X.                                     00001630
           02 MNENTCDEC PIC X.                                          00001640
           02 MNENTCDEP PIC X.                                          00001650
           02 MNENTCDEH PIC X.                                          00001660
           02 MNENTCDEV PIC X.                                          00001670
           02 MNENTCDEO      PIC X(5).                                  00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MLENTCDEA      PIC X.                                     00001700
           02 MLENTCDEC PIC X.                                          00001710
           02 MLENTCDEP PIC X.                                          00001720
           02 MLENTCDEH PIC X.                                          00001730
           02 MLENTCDEV PIC X.                                          00001740
           02 MLENTCDEO      PIC X(16).                                 00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MNRECA    PIC X.                                          00001770
           02 MNRECC    PIC X.                                          00001780
           02 MNRECP    PIC X.                                          00001790
           02 MNRECH    PIC X.                                          00001800
           02 MNRECV    PIC X.                                          00001810
           02 MNRECO    PIC X(7).                                       00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MLREFFOURNA    PIC X.                                     00001840
           02 MLREFFOURNC    PIC X.                                     00001850
           02 MLREFFOURNP    PIC X.                                     00001860
           02 MLREFFOURNH    PIC X.                                     00001870
           02 MLREFFOURNV    PIC X.                                     00001880
           02 MLREFFOURNO    PIC X(16).                                 00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MDRECA    PIC X.                                          00001910
           02 MDRECC    PIC X.                                          00001920
           02 MDRECP    PIC X.                                          00001930
           02 MDRECH    PIC X.                                          00001940
           02 MDRECV    PIC X.                                          00001950
           02 MDRECO    PIC X(8).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MCMARQA   PIC X.                                          00001980
           02 MCMARQC   PIC X.                                          00001990
           02 MCMARQP   PIC X.                                          00002000
           02 MCMARQH   PIC X.                                          00002010
           02 MCMARQV   PIC X.                                          00002020
           02 MCMARQO   PIC X(5).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MNCDEA    PIC X.                                          00002050
           02 MNCDEC    PIC X.                                          00002060
           02 MNCDEP    PIC X.                                          00002070
           02 MNCDEH    PIC X.                                          00002080
           02 MNCDEV    PIC X.                                          00002090
           02 MNCDEO    PIC X(7).                                       00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MCFAMA    PIC X.                                          00002120
           02 MCFAMC    PIC X.                                          00002130
           02 MCFAMP    PIC X.                                          00002140
           02 MCFAMH    PIC X.                                          00002150
           02 MCFAMV    PIC X.                                          00002160
           02 MCFAMO    PIC X(16).                                      00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MQRECA    PIC X.                                          00002190
           02 MQRECC    PIC X.                                          00002200
           02 MQRECP    PIC X.                                          00002210
           02 MQRECH    PIC X.                                          00002220
           02 MQRECV    PIC X.                                          00002230
           02 MQRECO    PIC X(5).                                       00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MQPRAA    PIC X.                                          00002260
           02 MQPRAC    PIC X.                                          00002270
           02 MQPRAP    PIC X.                                          00002280
           02 MQPRAH    PIC X.                                          00002290
           02 MQPRAV    PIC X.                                          00002300
           02 MQPRAO    PIC X(5).                                       00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MPRAA     PIC X.                                          00002330
           02 MPRAC     PIC X.                                          00002340
           02 MPRAP     PIC X.                                          00002350
           02 MPRAH     PIC X.                                          00002360
           02 MPRAV     PIC X.                                          00002370
           02 MPRAO     PIC X(9).                                       00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MCDEVDARTYA    PIC X.                                     00002400
           02 MCDEVDARTYC    PIC X.                                     00002410
           02 MCDEVDARTYP    PIC X.                                     00002420
           02 MCDEVDARTYH    PIC X.                                     00002430
           02 MCDEVDARTYV    PIC X.                                     00002440
           02 MCDEVDARTYO    PIC X(3).                                  00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MPRADARTYA     PIC X.                                     00002470
           02 MPRADARTYC     PIC X.                                     00002480
           02 MPRADARTYP     PIC X.                                     00002490
           02 MPRADARTYH     PIC X.                                     00002500
           02 MPRADARTYV     PIC X.                                     00002510
           02 MPRADARTYO     PIC X(9).                                  00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MPRAANCA  PIC X.                                          00002540
           02 MPRAANCC  PIC X.                                          00002550
           02 MPRAANCP  PIC X.                                          00002560
           02 MPRAANCH  PIC X.                                          00002570
           02 MPRAANCV  PIC X.                                          00002580
           02 MPRAANCO  PIC X(9).                                       00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MPRMPANCA      PIC X.                                     00002610
           02 MPRMPANCC PIC X.                                          00002620
           02 MPRMPANCP PIC X.                                          00002630
           02 MPRMPANCH PIC X.                                          00002640
           02 MPRMPANCV PIC X.                                          00002650
           02 MPRMPANCO      PIC X(13).                                 00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MCDEVFNRA      PIC X.                                     00002680
           02 MCDEVFNRC PIC X.                                          00002690
           02 MCDEVFNRP PIC X.                                          00002700
           02 MCDEVFNRH PIC X.                                          00002710
           02 MCDEVFNRV PIC X.                                          00002720
           02 MCDEVFNRO      PIC X(3).                                  00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MPRMPANCFNA    PIC X.                                     00002750
           02 MPRMPANCFNC    PIC X.                                     00002760
           02 MPRMPANCFNP    PIC X.                                     00002770
           02 MPRMPANCFNH    PIC X.                                     00002780
           02 MPRMPANCFNV    PIC X.                                     00002790
           02 MPRMPANCFNO    PIC X(13).                                 00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MNCODICSUIA    PIC X.                                     00002820
           02 MNCODICSUIC    PIC X.                                     00002830
           02 MNCODICSUIP    PIC X.                                     00002840
           02 MNCODICSUIH    PIC X.                                     00002850
           02 MNCODICSUIV    PIC X.                                     00002860
           02 MNCODICSUIO    PIC X(7).                                  00002870
           02 FILLER    PIC X(2).                                       00002880
           02 MZONCMDA  PIC X.                                          00002890
           02 MZONCMDC  PIC X.                                          00002900
           02 MZONCMDP  PIC X.                                          00002910
           02 MZONCMDH  PIC X.                                          00002920
           02 MZONCMDV  PIC X.                                          00002930
           02 MZONCMDO  PIC X(15).                                      00002940
           02 FILLER    PIC X(2).                                       00002950
           02 MLIBERRA  PIC X.                                          00002960
           02 MLIBERRC  PIC X.                                          00002970
           02 MLIBERRP  PIC X.                                          00002980
           02 MLIBERRH  PIC X.                                          00002990
           02 MLIBERRV  PIC X.                                          00003000
           02 MLIBERRO  PIC X(58).                                      00003010
           02 FILLER    PIC X(2).                                       00003020
           02 MCODTRAA  PIC X.                                          00003030
           02 MCODTRAC  PIC X.                                          00003040
           02 MCODTRAP  PIC X.                                          00003050
           02 MCODTRAH  PIC X.                                          00003060
           02 MCODTRAV  PIC X.                                          00003070
           02 MCODTRAO  PIC X(4).                                       00003080
           02 FILLER    PIC X(2).                                       00003090
           02 MCICSA    PIC X.                                          00003100
           02 MCICSC    PIC X.                                          00003110
           02 MCICSP    PIC X.                                          00003120
           02 MCICSH    PIC X.                                          00003130
           02 MCICSV    PIC X.                                          00003140
           02 MCICSO    PIC X(5).                                       00003150
           02 FILLER    PIC X(2).                                       00003160
           02 MNETNAMA  PIC X.                                          00003170
           02 MNETNAMC  PIC X.                                          00003180
           02 MNETNAMP  PIC X.                                          00003190
           02 MNETNAMH  PIC X.                                          00003200
           02 MNETNAMV  PIC X.                                          00003210
           02 MNETNAMO  PIC X(8).                                       00003220
           02 FILLER    PIC X(2).                                       00003230
           02 MSCREENA  PIC X.                                          00003240
           02 MSCREENC  PIC X.                                          00003250
           02 MSCREENP  PIC X.                                          00003260
           02 MSCREENH  PIC X.                                          00003270
           02 MSCREENV  PIC X.                                          00003280
           02 MSCREENO  PIC X(4).                                       00003290
                                                                                
