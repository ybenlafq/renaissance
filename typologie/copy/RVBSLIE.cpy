      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE BSLIE TYPE DE LIEN B&S >TABLE RTBS01   *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVBSLIE.                                                             
           05  BSLIE-CTABLEG2    PIC X(15).                                     
           05  BSLIE-CTABLEG2-REDEF REDEFINES BSLIE-CTABLEG2.                   
               10  BSLIE-CLIEN           PIC X(02).                             
           05  BSLIE-WTABLEG     PIC X(80).                                     
           05  BSLIE-WTABLEG-REDEF  REDEFINES BSLIE-WTABLEG.                    
               10  BSLIE-LLIEN           PIC X(20).                             
               10  BSLIE-CTRANS          PIC X(04).                             
               10  BSLIE-WAUTO           PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVBSLIE-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BSLIE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  BSLIE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BSLIE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  BSLIE-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
