      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * ------------------------------------------------------------- *         
      * zones appel mbs91 module de calcul pour mise en Z             *         
      * ------------------------------------------------------------- *         
       01 commbs91-appli.                                                       
          05 mbs91-entree.                                                      
             10 mbs91-nsociete                 pic x(03).                       
             10 mbs91-nlieu                    pic x(03).                       
             10 mbs91-nvente                   pic x(07).                       
             10 mbs91-dvente                   pic x(08).                       
             10 mbs91-nordre                   pic x(05).                       
             10 mbs91-ctrl-cilot               pic x(01).                       
             10 mbs91-mise-en-z                pic x(01).                       
          05 mbs91-sortie.                                                      
             10 mbs91-codret                   pic x(02).                       
             10 mbs91-libret                   pic x(80).                       
                                                                                
