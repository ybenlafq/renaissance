      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MDLIV CODE MODE DELIVRANCE             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01E5.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01E5.                                                            
      *}                                                                        
           05  MDLIV-CTABLEG2    PIC X(15).                                     
           05  MDLIV-CTABLEG2-REDEF REDEFINES MDLIV-CTABLEG2.                   
               10  MDLIV-CMODDEL         PIC X(03).                             
           05  MDLIV-WTABLEG     PIC X(80).                                     
           05  MDLIV-WTABLEG-REDEF  REDEFINES MDLIV-WTABLEG.                    
               10  MDLIV-LMODDEL         PIC X(20).                             
               10  MDLIV-WPLASSO         PIC X(01).                             
               10  MDLIV-QDECMINI        PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  MDLIV-QDECMINI-N     REDEFINES MDLIV-QDECMINI                
                                         PIC 9(02).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  MDLIV-QDECMAXI        PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  MDLIV-QDECMAXI-N     REDEFINES MDLIV-QDECMAXI                
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  MDLIV-WDONNEES        PIC X(33).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01E5-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01E5-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MDLIV-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MDLIV-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MDLIV-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MDLIV-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
