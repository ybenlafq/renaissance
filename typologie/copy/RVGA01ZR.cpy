      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MQMGR QMANAGER ASSOCI� � UN LIEU       *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01Z.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01Z.                                                             
      *}                                                                        
           05  MQMGR-CTABLEG2    PIC X(15).                                     
           05  MQMGR-CTABLEG2-REDEF REDEFINES MQMGR-CTABLEG2.                   
               10  MQMGR-NSOC            PIC X(03).                             
               10  MQMGR-NLIEU           PIC X(03).                             
               10  MQMGR-CAPPLI          PIC X(08).                             
           05  MQMGR-WTABLEG     PIC X(80).                                     
           05  MQMGR-WTABLEG-REDEF  REDEFINES MQMGR-WTABLEG.                    
               10  MQMGR-QMANAGER        PIC X(10).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01Z-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01Z-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MQMGR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MQMGR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MQMGR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MQMGR-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
