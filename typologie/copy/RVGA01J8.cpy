      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE COLMP MODES DE PAIEMENT PAR COLONNE    *        
      *----------------------------------------------------------------*        
       01  RVGA01J8.                                                            
           05  COLMP-CTABLEG2    PIC X(15).                                     
           05  COLMP-CTABLEG2-REDEF REDEFINES COLMP-CTABLEG2.                   
               10  COLMP-CETAT           PIC X(09).                             
               10  COLMP-NUMCOL          PIC X(02).                             
               10  COLMP-NUMCOL-N       REDEFINES COLMP-NUMCOL                  
                                         PIC 9(02).                             
               10  COLMP-NSEQ            PIC X(02).                             
               10  COLMP-NSEQ-N         REDEFINES COLMP-NSEQ                    
                                         PIC 9(02).                             
           05  COLMP-WTABLEG     PIC X(80).                                     
           05  COLMP-WTABLEG-REDEF  REDEFINES COLMP-WTABLEG.                    
               10  COLMP-MODP            PIC X(01).                             
               10  COLMP-LIBMODP         PIC X(20).                             
               10  COLMP-WACTIF          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01J8-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COLMP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  COLMP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COLMP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  COLMP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
