      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGN010 AU 15/04/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,03,BI,A,                          *        
      *                           20,07,BI,A,                          *        
      *                           27,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGN010.                                                        
            05 NOMETAT-IGN010           PIC X(6) VALUE 'IGN010'.                
            05 RUPTURES-IGN010.                                                 
           10 IGN010-CHEFPROD           PIC X(05).                      007  005
           10 IGN010-CFAM               PIC X(05).                      012  005
           10 IGN010-CHAMPTRI           PIC X(03).                      017  003
           10 IGN010-NCODIC             PIC X(07).                      020  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGN010-SEQUENCE           PIC S9(04) COMP.                027  002
      *--                                                                       
           10 IGN010-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGN010.                                                   
           10 IGN010-CAPPRO             PIC X(03).                      029  003
           10 IGN010-CIMPER             PIC X(01).                      032  001
           10 IGN010-CMARQ              PIC X(05).                      033  005
           10 IGN010-LCHEFPROD          PIC X(20).                      038  020
           10 IGN010-LFAM               PIC X(20).                      058  020
           10 IGN010-LREFFOUR           PIC X(20).                      078  020
           10 IGN010-NSOCIETE           PIC X(03).                      098  003
           10 IGN010-QDEROFIL           PIC X(03).                      101  003
           10 IGN010-QDEROMOY           PIC X(03).                      104  003
           10 IGN010-QFILINF            PIC X(03).                      107  003
           10 IGN010-QFILMIL            PIC X(03).                      110  003
           10 IGN010-QFILSUP            PIC X(03).                      113  003
           10 IGN010-QMOYINF            PIC X(03).                      116  003
           10 IGN010-QMOYMIL            PIC X(03).                      119  003
           10 IGN010-QMOYSUP            PIC X(03).                      122  003
           10 IGN010-QPACFIL            PIC X(03).                      125  003
           10 IGN010-QPACMOY            PIC X(03).                      128  003
           10 IGN010-QPVEXFIL           PIC X(03).                      131  003
           10 IGN010-QPVEXMOY           PIC X(03).                      134  003
           10 IGN010-QPVRFIL            PIC X(03).                      137  003
           10 IGN010-QPVZFIL            PIC X(03).                      140  003
           10 IGN010-QPVZMOY            PIC X(03).                      143  003
           10 IGN010-QREFMOY            PIC X(03).                      146  003
           10 IGN010-QWOAFIL            PIC X(03).                      149  003
           10 IGN010-QWOAMOY            PIC X(03).                      152  003
           10 IGN010-WIMPERATIF         PIC X(01).                      155  001
           10 IGN010-WOA                PIC X(01).                      156  001
           10 IGN010-BORNEINF           PIC S9(03)V9(1) COMP-3.         157  003
           10 IGN010-BORNESUP           PIC S9(03)V9(1) COMP-3.         160  003
           10 IGN010-PMOYTTC            PIC S9(07)V9(2) COMP-3.         163  005
           10 IGN010-PREFTTC            PIC S9(07)V9(2) COMP-3.         168  005
            05 FILLER                      PIC X(340).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGN010-LONG           PIC S9(4)   COMP  VALUE +172.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGN010-LONG           PIC S9(4) COMP-5  VALUE +172.           
                                                                                
      *}                                                                        
