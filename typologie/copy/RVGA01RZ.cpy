      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE AVSRP ECHEANCE AVOIR RETROCES� SRP     *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01RZ.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01RZ.                                                            
      *}                                                                        
           05  AVSRP-CTABLEG2    PIC X(15).                                     
           05  AVSRP-CTABLEG2-REDEF REDEFINES AVSRP-CTABLEG2.                   
               10  AVSRP-MARQUE          PIC X(05).                             
           05  AVSRP-WTABLEG     PIC X(80).                                     
           05  AVSRP-WTABLEG-REDEF  REDEFINES AVSRP-WTABLEG.                    
               10  AVSRP-WACTIF          PIC X(01).                             
               10  AVSRP-JOUR            PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  AVSRP-JOUR-N         REDEFINES AVSRP-JOUR                    
                                         PIC 9(02).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  AVSRP-DELAI           PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  AVSRP-DELAI-N        REDEFINES AVSRP-DELAI                   
                                         PIC 9(02).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01RZ-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01RZ-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AVSRP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  AVSRP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AVSRP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  AVSRP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
