      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVLI0100                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLI0100                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVLI0100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVLI0100.                                                            
      *}                                                                        
           02  LI01-NSOC                                                        
               PIC X(0003).                                                     
           02  LI01-NLIEU                                                       
               PIC X(0003).                                                     
           02  LI01-WTYPEADR                                                    
               PIC X(0001).                                                     
           02  LI01-CTYPLIEU                                                    
               PIC X(0001).                                                     
           02  LI01-LLIEU                                                       
               PIC X(0050).                                                     
           02  LI01-LADR1                                                       
               PIC X(0050).                                                     
           02  LI01-LADR2                                                       
               PIC X(0050).                                                     
           02  LI01-LADR3                                                       
               PIC X(0050).                                                     
           02  LI01-LCOMMUNE                                                    
               PIC X(0050).                                                     
           02  LI01-CPOSTAL                                                     
               PIC X(0005).                                                     
           02  LI01-LBUREAU                                                     
               PIC X(0050).                                                     
           02  LI01-CDEPT                                                       
               PIC X(0002).                                                     
           02  LI01-TEL                                                         
               PIC X(0015).                                                     
           02  LI01-FAX                                                         
               PIC X(0015).                                                     
           02  LI01-QDELAIAPPRO                                                 
               PIC X(0003).                                                     
           02  LI01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVLI0100                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVLI0100-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVLI0100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LI01-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LI01-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LI01-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LI01-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LI01-WTYPEADR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LI01-WTYPEADR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LI01-CTYPLIEU-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LI01-CTYPLIEU-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LI01-LLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LI01-LLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LI01-LADR1-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LI01-LADR1-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LI01-LADR2-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LI01-LADR2-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LI01-LADR3-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LI01-LADR3-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LI01-LCOMMUNE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LI01-LCOMMUNE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LI01-CPOSTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LI01-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LI01-LBUREAU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LI01-LBUREAU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LI01-CDEPT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LI01-CDEPT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LI01-TEL-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LI01-TEL-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LI01-FAX-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LI01-FAX-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LI01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  LI01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
