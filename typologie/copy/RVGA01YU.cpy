      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE NMCPT GESTION NUM PIECES TOPE V.GRP    *        
      *----------------------------------------------------------------*        
       01  RVGA01YU.                                                            
           05  NMCPT-CTABLEG2    PIC X(15).                                     
           05  NMCPT-CTABLEG2-REDEF REDEFINES NMCPT-CTABLEG2.                   
               10  NMCPT-SOCIETE         PIC X(03).                             
           05  NMCPT-WTABLEG     PIC X(80).                                     
           05  NMCPT-WTABLEG-REDEF  REDEFINES NMCPT-WTABLEG.                    
               10  NMCPT-TOPNUM          PIC X(03).                             
               10  NMCPT-TOPNUM-N       REDEFINES NMCPT-TOPNUM                  
                                         PIC 9(03).                             
               10  NMCPT-VGRNUM          PIC X(03).                             
               10  NMCPT-VGRNUM-N       REDEFINES NMCPT-VGRNUM                  
                                         PIC 9(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01YU-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NMCPT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  NMCPT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NMCPT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  NMCPT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
