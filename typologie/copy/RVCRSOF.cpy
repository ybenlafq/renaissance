      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *                  VUE DE LA SOUS-TABLE CRSOF                    *        
      *          MONTANTS ET MENSUALITES PAR TRANCHE DE PRIX           *        
      *----------------------------------------------------------------*        
       01  RVGA01R.                                                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  CRSOF-CTABLEG2    PIC X(15).                                     
           05  CRSOF-CTABLEG2-REDEF REDEFINES CRSOF-CTABLEG2.                   
               10  CRSOF-CBAREME         PIC X(07).                             
           05  CRSOF-WTABLEG     PIC X(80).                                     
           05  CRSOF-WTABLEG-REDEF  REDEFINES CRSOF-WTABLEG.                    
               10  CRSOF-PSEUIL          PIC X(12).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  CRSOF-PSEUIL-N REDEFINES CRSOF-PSEUIL PIC 9(12).             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  CRSOF-WNMENS          PIC X(03).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  CRSOF-WNMENS-N REDEFINES CRSOF-WNMENS PIC 9(03).             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  CRSOF-PAGIOS          PIC X(06).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  CRSOF-PAGIOS-N REDEFINES CRSOF-PAGIOS PIC 9(06).             
               10  CRSOF-PTAEG           PIC S9(02)V9(03).                      
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01R-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CRSOF-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CRSOF-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CRSOF-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CRSOF-WTABLEG-F   PIC S9(4) COMP-5.                              
      *}                                                                        
