      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IHA030 AU 02/10/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,03,BI,A,                          *        
      *                           16,03,BI,A,                          *        
      *                           19,03,PD,A,                          *        
      *                           22,05,BI,A,                          *        
      *                           27,01,BI,A,                          *        
      *                           28,07,BI,A,                          *        
      *                           35,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IHA030.                                                        
            05 NOMETAT-IHA030           PIC X(6) VALUE 'IHA030'.                
            05 RUPTURES-IHA030.                                                 
           10 IHA030-NSOCLAB            PIC X(03).                      007  003
           10 IHA030-NLAB               PIC X(03).                      010  003
           10 IHA030-NSOCIETE           PIC X(03).                      013  003
           10 IHA030-NDEPOT             PIC X(03).                      016  003
           10 IHA030-WSEQFAM            PIC S9(05)      COMP-3.         019  003
           10 IHA030-CMARQ              PIC X(05).                      022  005
           10 IHA030-FLAG               PIC X(01).                      027  001
           10 IHA030-NCODIC             PIC X(07).                      028  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IHA030-SEQUENCE           PIC S9(04) COMP.                035  002
      *--                                                                       
           10 IHA030-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IHA030.                                                   
           10 IHA030-CFAM               PIC X(05).                      037  005
           10 IHA030-DATE1              PIC X(08).                      042  008
           10 IHA030-DATE2              PIC X(08).                      050  008
           10 IHA030-DATE3              PIC X(08).                      058  008
           10 IHA030-LDEPOT             PIC X(20).                      066  020
           10 IHA030-LREFFOURN          PIC X(20).                      086  020
           10 IHA030-LSTATCOMP          PIC X(03).                      106  003
           10 IHA030-TDATE1             PIC X(08).                      109  008
           10 IHA030-TDATE4             PIC X(08).                      117  008
           10 IHA030-QSTOCKC            PIC S9(05)      COMP-3.         125  003
           10 IHA030-QSTOCKV            PIC S9(05)      COMP-3.         128  003
           10 IHA030-REFARRPREV         PIC S9(05)      COMP-3.         131  003
           10 IHA030-REFAVENIR          PIC S9(05)      COMP-3.         134  003
           10 IHA030-REFDEJARR          PIC S9(05)      COMP-3.         137  003
           10 IHA030-REFTOTAL           PIC S9(06)      COMP-3.         140  004
            05 FILLER                      PIC X(369).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IHA030-LONG           PIC S9(4)   COMP  VALUE +143.           
      *                                                                         
      *--                                                                       
        01  DSECT-IHA030-LONG           PIC S9(4) COMP-5  VALUE +143.           
                                                                                
      *}                                                                        
