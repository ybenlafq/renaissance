      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: Erx20                                                      00000020
      ***************************************************************** 00000030
       01   ERX35I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NCONCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 NCONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 NCONCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 NCONCI    PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LENSCONCL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 LENSCONCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 LENSCONCF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 LENSCONCI      PIC X(15).                                 00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NSOCL     COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 NSOCL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 NSOCF     PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 NSOCI     PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NMAGL     COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 NMAGL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 NMAGF     PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 NMAGI     PIC X(3).                                       00000290
           02 NDETI OCCURS   12 TIMES .                                 00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 CFAML   COMP PIC S9(4).                                 00000310
      *--                                                                       
             03 CFAML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 CFAMF   PIC X.                                          00000320
             03 FILLER  PIC X(4).                                       00000330
             03 CFAMI   PIC X(5).                                       00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 LFAML   COMP PIC S9(4).                                 00000350
      *--                                                                       
             03 LFAML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 LFAMF   PIC X.                                          00000360
             03 FILLER  PIC X(4).                                       00000370
             03 LFAMI   PIC X(20).                                      00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000390
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000400
           02 FILLER    PIC X(4).                                       00000410
           02 MLIBERRI  PIC X(78).                                      00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000430
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MCODTRAI  PIC X(4).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MCICSI    PIC X(5).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MNETNAMI  PIC X(8).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MSCREENI  PIC X(4).                                       00000580
      ***************************************************************** 00000590
      * SDF: Erx20                                                      00000600
      ***************************************************************** 00000610
       01   ERX35O REDEFINES ERX35I.                                    00000620
           02 FILLER    PIC X(12).                                      00000630
           02 FILLER    PIC X(2).                                       00000640
           02 MDATJOUA  PIC X.                                          00000650
           02 MDATJOUC  PIC X.                                          00000660
           02 MDATJOUP  PIC X.                                          00000670
           02 MDATJOUH  PIC X.                                          00000680
           02 MDATJOUV  PIC X.                                          00000690
           02 MDATJOUO  PIC X(10).                                      00000700
           02 FILLER    PIC X(2).                                       00000710
           02 MTIMJOUA  PIC X.                                          00000720
           02 MTIMJOUC  PIC X.                                          00000730
           02 MTIMJOUP  PIC X.                                          00000740
           02 MTIMJOUH  PIC X.                                          00000750
           02 MTIMJOUV  PIC X.                                          00000760
           02 MTIMJOUO  PIC X(5).                                       00000770
           02 FILLER    PIC X(2).                                       00000780
           02 NCONCA    PIC X.                                          00000790
           02 NCONCC    PIC X.                                          00000800
           02 NCONCP    PIC X.                                          00000810
           02 NCONCH    PIC X.                                          00000820
           02 NCONCV    PIC X.                                          00000830
           02 NCONCO    PIC X(4).                                       00000840
           02 FILLER    PIC X(2).                                       00000850
           02 LENSCONCA      PIC X.                                     00000860
           02 LENSCONCC PIC X.                                          00000870
           02 LENSCONCP PIC X.                                          00000880
           02 LENSCONCH PIC X.                                          00000890
           02 LENSCONCV PIC X.                                          00000900
           02 LENSCONCO      PIC X(15).                                 00000910
           02 FILLER    PIC X(2).                                       00000920
           02 NSOCA     PIC X.                                          00000930
           02 NSOCC     PIC X.                                          00000940
           02 NSOCP     PIC X.                                          00000950
           02 NSOCH     PIC X.                                          00000960
           02 NSOCV     PIC X.                                          00000970
           02 NSOCO     PIC X(3).                                       00000980
           02 FILLER    PIC X(2).                                       00000990
           02 NMAGA     PIC X.                                          00001000
           02 NMAGC     PIC X.                                          00001010
           02 NMAGP     PIC X.                                          00001020
           02 NMAGH     PIC X.                                          00001030
           02 NMAGV     PIC X.                                          00001040
           02 NMAGO     PIC X(3).                                       00001050
           02 NDETO OCCURS   12 TIMES .                                 00001060
             03 FILLER       PIC X(2).                                  00001070
             03 CFAMA   PIC X.                                          00001080
             03 CFAMC   PIC X.                                          00001090
             03 CFAMP   PIC X.                                          00001100
             03 CFAMH   PIC X.                                          00001110
             03 CFAMV   PIC X.                                          00001120
             03 CFAMO   PIC X(5).                                       00001130
             03 FILLER       PIC X(2).                                  00001140
             03 LFAMA   PIC X.                                          00001150
             03 LFAMC   PIC X.                                          00001160
             03 LFAMP   PIC X.                                          00001170
             03 LFAMH   PIC X.                                          00001180
             03 LFAMV   PIC X.                                          00001190
             03 LFAMO   PIC X(20).                                      00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MLIBERRA  PIC X.                                          00001220
           02 MLIBERRC  PIC X.                                          00001230
           02 MLIBERRP  PIC X.                                          00001240
           02 MLIBERRH  PIC X.                                          00001250
           02 MLIBERRV  PIC X.                                          00001260
           02 MLIBERRO  PIC X(78).                                      00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MCODTRAA  PIC X.                                          00001290
           02 MCODTRAC  PIC X.                                          00001300
           02 MCODTRAP  PIC X.                                          00001310
           02 MCODTRAH  PIC X.                                          00001320
           02 MCODTRAV  PIC X.                                          00001330
           02 MCODTRAO  PIC X(4).                                       00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MCICSA    PIC X.                                          00001360
           02 MCICSC    PIC X.                                          00001370
           02 MCICSP    PIC X.                                          00001380
           02 MCICSH    PIC X.                                          00001390
           02 MCICSV    PIC X.                                          00001400
           02 MCICSO    PIC X(5).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MNETNAMA  PIC X.                                          00001430
           02 MNETNAMC  PIC X.                                          00001440
           02 MNETNAMP  PIC X.                                          00001450
           02 MNETNAMH  PIC X.                                          00001460
           02 MNETNAMV  PIC X.                                          00001470
           02 MNETNAMO  PIC X(8).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MSCREENA  PIC X.                                          00001500
           02 MSCREENC  PIC X.                                          00001510
           02 MSCREENP  PIC X.                                          00001520
           02 MSCREENH  PIC X.                                          00001530
           02 MSCREENV  PIC X.                                          00001540
           02 MSCREENO  PIC X(4).                                       00001550
                                                                                
