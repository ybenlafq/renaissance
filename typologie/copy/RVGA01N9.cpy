      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HEADR UTILISATEURS SPECIAUX GHE        *        
      *----------------------------------------------------------------*        
       01  RVGA01N9.                                                            
           05  HEADR-CTABLEG2    PIC X(15).                                     
           05  HEADR-CTABLEG2-REDEF REDEFINES HEADR-CTABLEG2.                   
               10  HEADR-CACID           PIC X(04).                             
               10  HEADR-NLIEUHED        PIC X(03).                             
           05  HEADR-WTABLEG     PIC X(80).                                     
           05  HEADR-WTABLEG-REDEF  REDEFINES HEADR-WTABLEG.                    
               10  HEADR-WACTIF          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01N9-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HEADR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HEADR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HEADR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HEADR-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
