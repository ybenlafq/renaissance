      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Menus NCG+ Menus for a Profile                                  00000020
      ***************************************************************** 00000030
       01   EMENDI.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMTITREL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MMTITREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMTITREF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MMTITREI  PIC X(6).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITREL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MTITREL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTITREF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MTITREI   PIC X(40).                                      00000210
           02 MLIGNEI OCCURS   15 TIMES .                               00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MXLL    COMP PIC S9(4).                                 00000230
      *--                                                                       
             03 MXLL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MXLF    PIC X.                                          00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MXLI    PIC X.                                          00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNIV1L  COMP PIC S9(4).                                 00000270
      *--                                                                       
             03 MNIV1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNIV1F  PIC X.                                          00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MNIV1I  PIC X(2).                                       00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNIV2L  COMP PIC S9(4).                                 00000310
      *--                                                                       
             03 MNIV2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNIV2F  PIC X.                                          00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MNIV2I  PIC X(2).                                       00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDISPL  COMP PIC S9(4).                                 00000350
      *--                                                                       
             03 MDISPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDISPF  PIC X.                                          00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MDISPI  PIC X(13).                                      00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MXML    COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 MXML COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MXMF    PIC X.                                          00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MXMI    PIC X.                                          00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLOPTL  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MLOPTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLOPTF  PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MLOPTI  PIC X(40).                                      00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTRANL  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MTRANL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTRANF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MTRANI  PIC X(4).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MLIBERRI  PIC X(79).                                      00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MCODTRAI  PIC X(4).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MZONCMDI  PIC X(15).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCICSI    PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MNETNAMI  PIC X(8).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MSCREENI  PIC X(4).                                       00000740
      ***************************************************************** 00000750
      * Menus NCG+ Menus for a Profile                                  00000760
      ***************************************************************** 00000770
       01   EMENDO REDEFINES EMENDI.                                    00000780
           02 FILLER    PIC X(12).                                      00000790
           02 FILLER    PIC X(2).                                       00000800
           02 MDATJOUA  PIC X.                                          00000810
           02 MDATJOUC  PIC X.                                          00000820
           02 MDATJOUP  PIC X.                                          00000830
           02 MDATJOUH  PIC X.                                          00000840
           02 MDATJOUV  PIC X.                                          00000850
           02 MDATJOUO  PIC X(10).                                      00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MTIMJOUA  PIC X.                                          00000880
           02 MTIMJOUC  PIC X.                                          00000890
           02 MTIMJOUP  PIC X.                                          00000900
           02 MTIMJOUH  PIC X.                                          00000910
           02 MTIMJOUV  PIC X.                                          00000920
           02 MTIMJOUO  PIC X(5).                                       00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MMTITREA  PIC X.                                          00000950
           02 MMTITREC  PIC X.                                          00000960
           02 MMTITREP  PIC X.                                          00000970
           02 MMTITREH  PIC X.                                          00000980
           02 MMTITREV  PIC X.                                          00000990
           02 MMTITREO  PIC X(6).                                       00001000
           02 FILLER    PIC X(2).                                       00001010
           02 MTITREA   PIC X.                                          00001020
           02 MTITREC   PIC X.                                          00001030
           02 MTITREP   PIC X.                                          00001040
           02 MTITREH   PIC X.                                          00001050
           02 MTITREV   PIC X.                                          00001060
           02 MTITREO   PIC X(40).                                      00001070
           02 MLIGNEO OCCURS   15 TIMES .                               00001080
             03 FILLER       PIC X(2).                                  00001090
             03 MXLA    PIC X.                                          00001100
             03 MXLC    PIC X.                                          00001110
             03 MXLP    PIC X.                                          00001120
             03 MXLH    PIC X.                                          00001130
             03 MXLV    PIC X.                                          00001140
             03 MXLO    PIC X.                                          00001150
             03 FILLER       PIC X(2).                                  00001160
             03 MNIV1A  PIC X.                                          00001170
             03 MNIV1C  PIC X.                                          00001180
             03 MNIV1P  PIC X.                                          00001190
             03 MNIV1H  PIC X.                                          00001200
             03 MNIV1V  PIC X.                                          00001210
             03 MNIV1O  PIC X(2).                                       00001220
             03 FILLER       PIC X(2).                                  00001230
             03 MNIV2A  PIC X.                                          00001240
             03 MNIV2C  PIC X.                                          00001250
             03 MNIV2P  PIC X.                                          00001260
             03 MNIV2H  PIC X.                                          00001270
             03 MNIV2V  PIC X.                                          00001280
             03 MNIV2O  PIC X(2).                                       00001290
             03 FILLER       PIC X(2).                                  00001300
             03 MDISPA  PIC X.                                          00001310
             03 MDISPC  PIC X.                                          00001320
             03 MDISPP  PIC X.                                          00001330
             03 MDISPH  PIC X.                                          00001340
             03 MDISPV  PIC X.                                          00001350
             03 MDISPO  PIC X(13).                                      00001360
             03 FILLER       PIC X(2).                                  00001370
             03 MXMA    PIC X.                                          00001380
             03 MXMC    PIC X.                                          00001390
             03 MXMP    PIC X.                                          00001400
             03 MXMH    PIC X.                                          00001410
             03 MXMV    PIC X.                                          00001420
             03 MXMO    PIC X.                                          00001430
             03 FILLER       PIC X(2).                                  00001440
             03 MLOPTA  PIC X.                                          00001450
             03 MLOPTC  PIC X.                                          00001460
             03 MLOPTP  PIC X.                                          00001470
             03 MLOPTH  PIC X.                                          00001480
             03 MLOPTV  PIC X.                                          00001490
             03 MLOPTO  PIC X(40).                                      00001500
             03 FILLER       PIC X(2).                                  00001510
             03 MTRANA  PIC X.                                          00001520
             03 MTRANC  PIC X.                                          00001530
             03 MTRANP  PIC X.                                          00001540
             03 MTRANH  PIC X.                                          00001550
             03 MTRANV  PIC X.                                          00001560
             03 MTRANO  PIC X(4).                                       00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MLIBERRA  PIC X.                                          00001590
           02 MLIBERRC  PIC X.                                          00001600
           02 MLIBERRP  PIC X.                                          00001610
           02 MLIBERRH  PIC X.                                          00001620
           02 MLIBERRV  PIC X.                                          00001630
           02 MLIBERRO  PIC X(79).                                      00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MCODTRAA  PIC X.                                          00001660
           02 MCODTRAC  PIC X.                                          00001670
           02 MCODTRAP  PIC X.                                          00001680
           02 MCODTRAH  PIC X.                                          00001690
           02 MCODTRAV  PIC X.                                          00001700
           02 MCODTRAO  PIC X(4).                                       00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MZONCMDA  PIC X.                                          00001730
           02 MZONCMDC  PIC X.                                          00001740
           02 MZONCMDP  PIC X.                                          00001750
           02 MZONCMDH  PIC X.                                          00001760
           02 MZONCMDV  PIC X.                                          00001770
           02 MZONCMDO  PIC X(15).                                      00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MCICSA    PIC X.                                          00001800
           02 MCICSC    PIC X.                                          00001810
           02 MCICSP    PIC X.                                          00001820
           02 MCICSH    PIC X.                                          00001830
           02 MCICSV    PIC X.                                          00001840
           02 MCICSO    PIC X(5).                                       00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MNETNAMA  PIC X.                                          00001870
           02 MNETNAMC  PIC X.                                          00001880
           02 MNETNAMP  PIC X.                                          00001890
           02 MNETNAMH  PIC X.                                          00001900
           02 MNETNAMV  PIC X.                                          00001910
           02 MNETNAMO  PIC X(8).                                       00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MSCREENA  PIC X.                                          00001940
           02 MSCREENC  PIC X.                                          00001950
           02 MSCREENP  PIC X.                                          00001960
           02 MSCREENH  PIC X.                                          00001970
           02 MSCREENV  PIC X.                                          00001980
           02 MSCREENO  PIC X(4).                                       00001990
                                                                                
