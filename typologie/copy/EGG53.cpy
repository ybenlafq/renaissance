      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGG00   EGG00                                              00000020
      ***************************************************************** 00000030
       01   EGG53I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MZONCMDI  PIC X(15).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNZONPRIL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNZONPRIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNZONPRIF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNZONPRII      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLZONPRIL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MLZONPRIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLZONPRIF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLZONPRII      PIC X(20).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNMAGI    PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMAGL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLMAGF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLMAGI    PIC X(20).                                      00000330
           02 MCFAMD OCCURS   10 TIMES .                                00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000350
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MCFAMI  PIC X(5).                                       00000380
           02 MLFAMD OCCURS   10 TIMES .                                00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLFAML  COMP PIC S9(4).                                 00000400
      *--                                                                       
             03 MLFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLFAMF  PIC X.                                          00000410
             03 FILLER  PIC X(4).                                       00000420
             03 MLFAMI  PIC X(20).                                      00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPERIL   COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MCPERIL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCPERIF   PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MCPERII   PIC X.                                          00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRIL    COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MCTRIL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCTRIF    PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MCTRII    PIC X.                                          00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MLIBERRI  PIC X(78).                                      00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MCODTRAI  PIC X(4).                                       00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MCICSI    PIC X(5).                                       00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MNETNAMI  PIC X(8).                                       00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MSCREENI  PIC X(4).                                       00000710
      ***************************************************************** 00000720
      * SDF: EGG00   EGG00                                              00000730
      ***************************************************************** 00000740
       01   EGG53O REDEFINES EGG53I.                                    00000750
           02 FILLER    PIC X(12).                                      00000760
           02 FILLER    PIC X(2).                                       00000770
           02 MDATJOUA  PIC X.                                          00000780
           02 MDATJOUC  PIC X.                                          00000790
           02 MDATJOUP  PIC X.                                          00000800
           02 MDATJOUH  PIC X.                                          00000810
           02 MDATJOUV  PIC X.                                          00000820
           02 MDATJOUO  PIC X(10).                                      00000830
           02 FILLER    PIC X(2).                                       00000840
           02 MTIMJOUA  PIC X.                                          00000850
           02 MTIMJOUC  PIC X.                                          00000860
           02 MTIMJOUP  PIC X.                                          00000870
           02 MTIMJOUH  PIC X.                                          00000880
           02 MTIMJOUV  PIC X.                                          00000890
           02 MTIMJOUO  PIC X(5).                                       00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MZONCMDA  PIC X.                                          00000920
           02 MZONCMDC  PIC X.                                          00000930
           02 MZONCMDP  PIC X.                                          00000940
           02 MZONCMDH  PIC X.                                          00000950
           02 MZONCMDV  PIC X.                                          00000960
           02 MZONCMDO  PIC X(15).                                      00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MNZONPRIA      PIC X.                                     00000990
           02 MNZONPRIC PIC X.                                          00001000
           02 MNZONPRIP PIC X.                                          00001010
           02 MNZONPRIH PIC X.                                          00001020
           02 MNZONPRIV PIC X.                                          00001030
           02 MNZONPRIO      PIC X(2).                                  00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MLZONPRIA      PIC X.                                     00001060
           02 MLZONPRIC PIC X.                                          00001070
           02 MLZONPRIP PIC X.                                          00001080
           02 MLZONPRIH PIC X.                                          00001090
           02 MLZONPRIV PIC X.                                          00001100
           02 MLZONPRIO      PIC X(20).                                 00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MNMAGA    PIC X.                                          00001130
           02 MNMAGC    PIC X.                                          00001140
           02 MNMAGP    PIC X.                                          00001150
           02 MNMAGH    PIC X.                                          00001160
           02 MNMAGV    PIC X.                                          00001170
           02 MNMAGO    PIC X(3).                                       00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MLMAGA    PIC X.                                          00001200
           02 MLMAGC    PIC X.                                          00001210
           02 MLMAGP    PIC X.                                          00001220
           02 MLMAGH    PIC X.                                          00001230
           02 MLMAGV    PIC X.                                          00001240
           02 MLMAGO    PIC X(20).                                      00001250
           02 DFHMS1 OCCURS   10 TIMES .                                00001260
             03 FILLER       PIC X(2).                                  00001270
             03 MCFAMA  PIC X.                                          00001280
             03 MCFAMC  PIC X.                                          00001290
             03 MCFAMP  PIC X.                                          00001300
             03 MCFAMH  PIC X.                                          00001310
             03 MCFAMV  PIC X.                                          00001320
             03 MCFAMO  PIC X(5).                                       00001330
           02 DFHMS2 OCCURS   10 TIMES .                                00001340
             03 FILLER       PIC X(2).                                  00001350
             03 MLFAMA  PIC X.                                          00001360
             03 MLFAMC  PIC X.                                          00001370
             03 MLFAMP  PIC X.                                          00001380
             03 MLFAMH  PIC X.                                          00001390
             03 MLFAMV  PIC X.                                          00001400
             03 MLFAMO  PIC X(20).                                      00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MCPERIA   PIC X.                                          00001430
           02 MCPERIC   PIC X.                                          00001440
           02 MCPERIP   PIC X.                                          00001450
           02 MCPERIH   PIC X.                                          00001460
           02 MCPERIV   PIC X.                                          00001470
           02 MCPERIO   PIC X.                                          00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MCTRIA    PIC X.                                          00001500
           02 MCTRIC    PIC X.                                          00001510
           02 MCTRIP    PIC X.                                          00001520
           02 MCTRIH    PIC X.                                          00001530
           02 MCTRIV    PIC X.                                          00001540
           02 MCTRIO    PIC X.                                          00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MLIBERRA  PIC X.                                          00001570
           02 MLIBERRC  PIC X.                                          00001580
           02 MLIBERRP  PIC X.                                          00001590
           02 MLIBERRH  PIC X.                                          00001600
           02 MLIBERRV  PIC X.                                          00001610
           02 MLIBERRO  PIC X(78).                                      00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MCODTRAA  PIC X.                                          00001640
           02 MCODTRAC  PIC X.                                          00001650
           02 MCODTRAP  PIC X.                                          00001660
           02 MCODTRAH  PIC X.                                          00001670
           02 MCODTRAV  PIC X.                                          00001680
           02 MCODTRAO  PIC X(4).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MCICSA    PIC X.                                          00001710
           02 MCICSC    PIC X.                                          00001720
           02 MCICSP    PIC X.                                          00001730
           02 MCICSH    PIC X.                                          00001740
           02 MCICSV    PIC X.                                          00001750
           02 MCICSO    PIC X(5).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MNETNAMA  PIC X.                                          00001780
           02 MNETNAMC  PIC X.                                          00001790
           02 MNETNAMP  PIC X.                                          00001800
           02 MNETNAMH  PIC X.                                          00001810
           02 MNETNAMV  PIC X.                                          00001820
           02 MNETNAMO  PIC X(8).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MSCREENA  PIC X.                                          00001850
           02 MSCREENC  PIC X.                                          00001860
           02 MSCREENP  PIC X.                                          00001870
           02 MSCREENH  PIC X.                                          00001880
           02 MSCREENV  PIC X.                                          00001890
           02 MSCREENO  PIC X(4).                                       00001900
                                                                                
