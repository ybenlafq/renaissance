      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA64   EGA64                                              00000020
      ***************************************************************** 00000030
       01   EGA64I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETOTL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGETOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGETOTF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGETOTI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFONCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFONCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCFONCI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MHISTOL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MHISTOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MHISTOF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MHISTOI   PIC X.                                          00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNCODICI  PIC X(7).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFOL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLREFOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLREFOF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLREFOI   PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPCOL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCOPCOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOPCOF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCOPCOI   PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODOPCOL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MCODOPCOL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCODOPCOF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCODOPCOI      PIC X(15).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCFAMI    PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLFAMI    PIC X(20).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATAFFL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDATAFFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATAFFF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDATAFFI  PIC X(10).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCMARQI   PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLMARQI   PIC X(20).                                      00000650
           02 MLIGNESI OCCURS   10 TIMES .                              00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTOPCOL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MTOPCOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MTOPCOF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MTOPCOI      PIC X(3).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTDMAJL      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MTDMAJL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MTDMAJF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MTDMAJI      PIC X(6).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTCOPCOL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MTCOPCOL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTCOPCOF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MTCOPCOI     PIC X(15).                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTLOPCOL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MTLOPCOL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTLOPCOF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MTLOPCOI     PIC X(20).                                 00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTREFL  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MTREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTREFF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MTREFI  PIC X.                                          00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTDTDEBL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MTDTDEBL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTDTDEBF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MTDTDEBI     PIC X(6).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTDTFINL     COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MTDTFINL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTDTFINF     PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MTDTFINI     PIC X(6).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MZONCMDI  PIC X(15).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MLIBERRI  PIC X(58).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCODTRAI  PIC X(4).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCICSI    PIC X(5).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MNETNAMI  PIC X(8).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MSCREENI  PIC X(4).                                       00001180
      ***************************************************************** 00001190
      * SDF: EGA64   EGA64                                              00001200
      ***************************************************************** 00001210
       01   EGA64O REDEFINES EGA64I.                                    00001220
           02 FILLER    PIC X(12).                                      00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MDATJOUA  PIC X.                                          00001250
           02 MDATJOUC  PIC X.                                          00001260
           02 MDATJOUP  PIC X.                                          00001270
           02 MDATJOUH  PIC X.                                          00001280
           02 MDATJOUV  PIC X.                                          00001290
           02 MDATJOUO  PIC X(10).                                      00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MTIMJOUA  PIC X.                                          00001320
           02 MTIMJOUC  PIC X.                                          00001330
           02 MTIMJOUP  PIC X.                                          00001340
           02 MTIMJOUH  PIC X.                                          00001350
           02 MTIMJOUV  PIC X.                                          00001360
           02 MTIMJOUO  PIC X(5).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MPAGEA    PIC X.                                          00001390
           02 MPAGEC    PIC X.                                          00001400
           02 MPAGEP    PIC X.                                          00001410
           02 MPAGEH    PIC X.                                          00001420
           02 MPAGEV    PIC X.                                          00001430
           02 MPAGEO    PIC X(3).                                       00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MPAGETOTA      PIC X.                                     00001460
           02 MPAGETOTC PIC X.                                          00001470
           02 MPAGETOTP PIC X.                                          00001480
           02 MPAGETOTH PIC X.                                          00001490
           02 MPAGETOTV PIC X.                                          00001500
           02 MPAGETOTO      PIC X(3).                                  00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MCFONCA   PIC X.                                          00001530
           02 MCFONCC   PIC X.                                          00001540
           02 MCFONCP   PIC X.                                          00001550
           02 MCFONCH   PIC X.                                          00001560
           02 MCFONCV   PIC X.                                          00001570
           02 MCFONCO   PIC X(3).                                       00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MHISTOA   PIC X.                                          00001600
           02 MHISTOC   PIC X.                                          00001610
           02 MHISTOP   PIC X.                                          00001620
           02 MHISTOH   PIC X.                                          00001630
           02 MHISTOV   PIC X.                                          00001640
           02 MHISTOO   PIC X.                                          00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MNCODICA  PIC X.                                          00001670
           02 MNCODICC  PIC X.                                          00001680
           02 MNCODICP  PIC X.                                          00001690
           02 MNCODICH  PIC X.                                          00001700
           02 MNCODICV  PIC X.                                          00001710
           02 MNCODICO  PIC X(7).                                       00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MLREFOA   PIC X.                                          00001740
           02 MLREFOC   PIC X.                                          00001750
           02 MLREFOP   PIC X.                                          00001760
           02 MLREFOH   PIC X.                                          00001770
           02 MLREFOV   PIC X.                                          00001780
           02 MLREFOO   PIC X(20).                                      00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MCOPCOA   PIC X.                                          00001810
           02 MCOPCOC   PIC X.                                          00001820
           02 MCOPCOP   PIC X.                                          00001830
           02 MCOPCOH   PIC X.                                          00001840
           02 MCOPCOV   PIC X.                                          00001850
           02 MCOPCOO   PIC X(3).                                       00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MCODOPCOA      PIC X.                                     00001880
           02 MCODOPCOC PIC X.                                          00001890
           02 MCODOPCOP PIC X.                                          00001900
           02 MCODOPCOH PIC X.                                          00001910
           02 MCODOPCOV PIC X.                                          00001920
           02 MCODOPCOO      PIC X(15).                                 00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MCFAMA    PIC X.                                          00001950
           02 MCFAMC    PIC X.                                          00001960
           02 MCFAMP    PIC X.                                          00001970
           02 MCFAMH    PIC X.                                          00001980
           02 MCFAMV    PIC X.                                          00001990
           02 MCFAMO    PIC X(5).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MLFAMA    PIC X.                                          00002020
           02 MLFAMC    PIC X.                                          00002030
           02 MLFAMP    PIC X.                                          00002040
           02 MLFAMH    PIC X.                                          00002050
           02 MLFAMV    PIC X.                                          00002060
           02 MLFAMO    PIC X(20).                                      00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MDATAFFA  PIC X.                                          00002090
           02 MDATAFFC  PIC X.                                          00002100
           02 MDATAFFP  PIC X.                                          00002110
           02 MDATAFFH  PIC X.                                          00002120
           02 MDATAFFV  PIC X.                                          00002130
           02 MDATAFFO  PIC X(10).                                      00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MCMARQA   PIC X.                                          00002160
           02 MCMARQC   PIC X.                                          00002170
           02 MCMARQP   PIC X.                                          00002180
           02 MCMARQH   PIC X.                                          00002190
           02 MCMARQV   PIC X.                                          00002200
           02 MCMARQO   PIC X(5).                                       00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MLMARQA   PIC X.                                          00002230
           02 MLMARQC   PIC X.                                          00002240
           02 MLMARQP   PIC X.                                          00002250
           02 MLMARQH   PIC X.                                          00002260
           02 MLMARQV   PIC X.                                          00002270
           02 MLMARQO   PIC X(20).                                      00002280
           02 MLIGNESO OCCURS   10 TIMES .                              00002290
             03 FILLER       PIC X(2).                                  00002300
             03 MTOPCOA      PIC X.                                     00002310
             03 MTOPCOC PIC X.                                          00002320
             03 MTOPCOP PIC X.                                          00002330
             03 MTOPCOH PIC X.                                          00002340
             03 MTOPCOV PIC X.                                          00002350
             03 MTOPCOO      PIC X(3).                                  00002360
             03 FILLER       PIC X(2).                                  00002370
             03 MTDMAJA      PIC X.                                     00002380
             03 MTDMAJC PIC X.                                          00002390
             03 MTDMAJP PIC X.                                          00002400
             03 MTDMAJH PIC X.                                          00002410
             03 MTDMAJV PIC X.                                          00002420
             03 MTDMAJO      PIC X(6).                                  00002430
             03 FILLER       PIC X(2).                                  00002440
             03 MTCOPCOA     PIC X.                                     00002450
             03 MTCOPCOC     PIC X.                                     00002460
             03 MTCOPCOP     PIC X.                                     00002470
             03 MTCOPCOH     PIC X.                                     00002480
             03 MTCOPCOV     PIC X.                                     00002490
             03 MTCOPCOO     PIC X(15).                                 00002500
             03 FILLER       PIC X(2).                                  00002510
             03 MTLOPCOA     PIC X.                                     00002520
             03 MTLOPCOC     PIC X.                                     00002530
             03 MTLOPCOP     PIC X.                                     00002540
             03 MTLOPCOH     PIC X.                                     00002550
             03 MTLOPCOV     PIC X.                                     00002560
             03 MTLOPCOO     PIC X(20).                                 00002570
             03 FILLER       PIC X(2).                                  00002580
             03 MTREFA  PIC X.                                          00002590
             03 MTREFC  PIC X.                                          00002600
             03 MTREFP  PIC X.                                          00002610
             03 MTREFH  PIC X.                                          00002620
             03 MTREFV  PIC X.                                          00002630
             03 MTREFO  PIC X.                                          00002640
             03 FILLER       PIC X(2).                                  00002650
             03 MTDTDEBA     PIC X.                                     00002660
             03 MTDTDEBC     PIC X.                                     00002670
             03 MTDTDEBP     PIC X.                                     00002680
             03 MTDTDEBH     PIC X.                                     00002690
             03 MTDTDEBV     PIC X.                                     00002700
             03 MTDTDEBO     PIC X(6).                                  00002710
             03 FILLER       PIC X(2).                                  00002720
             03 MTDTFINA     PIC X.                                     00002730
             03 MTDTFINC     PIC X.                                     00002740
             03 MTDTFINP     PIC X.                                     00002750
             03 MTDTFINH     PIC X.                                     00002760
             03 MTDTFINV     PIC X.                                     00002770
             03 MTDTFINO     PIC X(6).                                  00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MZONCMDA  PIC X.                                          00002800
           02 MZONCMDC  PIC X.                                          00002810
           02 MZONCMDP  PIC X.                                          00002820
           02 MZONCMDH  PIC X.                                          00002830
           02 MZONCMDV  PIC X.                                          00002840
           02 MZONCMDO  PIC X(15).                                      00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MLIBERRA  PIC X.                                          00002870
           02 MLIBERRC  PIC X.                                          00002880
           02 MLIBERRP  PIC X.                                          00002890
           02 MLIBERRH  PIC X.                                          00002900
           02 MLIBERRV  PIC X.                                          00002910
           02 MLIBERRO  PIC X(58).                                      00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MCODTRAA  PIC X.                                          00002940
           02 MCODTRAC  PIC X.                                          00002950
           02 MCODTRAP  PIC X.                                          00002960
           02 MCODTRAH  PIC X.                                          00002970
           02 MCODTRAV  PIC X.                                          00002980
           02 MCODTRAO  PIC X(4).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MCICSA    PIC X.                                          00003010
           02 MCICSC    PIC X.                                          00003020
           02 MCICSP    PIC X.                                          00003030
           02 MCICSH    PIC X.                                          00003040
           02 MCICSV    PIC X.                                          00003050
           02 MCICSO    PIC X(5).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MNETNAMA  PIC X.                                          00003080
           02 MNETNAMC  PIC X.                                          00003090
           02 MNETNAMP  PIC X.                                          00003100
           02 MNETNAMH  PIC X.                                          00003110
           02 MNETNAMV  PIC X.                                          00003120
           02 MNETNAMO  PIC X(8).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MSCREENA  PIC X.                                          00003150
           02 MSCREENC  PIC X.                                          00003160
           02 MSCREENP  PIC X.                                          00003170
           02 MSCREENH  PIC X.                                          00003180
           02 MSCREENV  PIC X.                                          00003190
           02 MSCREENO  PIC X(4).                                       00003200
                                                                                
