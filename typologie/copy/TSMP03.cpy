      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TMP03                                          *  00020001
      ****************************************************************  00050000
      *                                                                 00060000
      *------------------------------ LONGUEUR                          00070000
       01  TS-LONG              PIC S9(2) COMP-3 VALUE 45.              00080001
       01  TS-DONNEES.                                                  00090000
              10 TS-MCMOPAI     PIC X(05).                              00100001
              10 TS-MWTPRGT     PIC X(01).                              00120001
              10 TS-MLMDPAI     PIC X(15).                              00121001
              10 TS-MLMDPRB     PIC X(15).                              00122001
              10 TS-MWINFCP     PIC X(01).                                      
              10 TS-MWDCMPT     PIC X(01).                              00123001
              10 TS-MWCTFDC     PIC X(01).                                      
              10 TS-MWRCOFF     PIC X(01).                                      
              10 TS-MCMOPAC     PIC X(05).                                      
                                                                                
