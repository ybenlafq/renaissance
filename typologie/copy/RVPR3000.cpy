      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RTPR30                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPR3000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPR3000.                                                            
      *}                                                                        
           10 PR30-NCHRONO         PIC X(7).                                    
           10 PR30-NFILIALE        PIC X(3).                                    
           10 PR30-NSOC            PIC X(3).                                    
           10 PR30-NLIEU           PIC X(3).                                    
           10 PR30-WABOREN         PIC X(1).                                    
           10 PR30-DENVOI          PIC X(8).                                    
           10 PR30-NENTCDE         PIC X(5).                                    
           10 PR30-DSYST           PIC S9(13)V USAGE COMP-3.                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 8       *        
      ******************************************************************        
                                                                                
