      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESE03   ESE03                                              00000020
      ***************************************************************** 00000030
       01   EBS24I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPENT1L     COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MCTYPENT1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTYPENT1F     PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCTYPENT1I     PIC X(2).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPENT1L     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MLTYPENT1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLTYPENT1F     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLTYPENT1I     PIC X(20).                                 00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAM1L   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCFAM1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAM1F   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCFAM1I   PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAM1L   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLFAM1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLFAM1F   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLFAM1I   PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTA1L    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MSTA1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSTA1F    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MSTA1I    PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQ1L  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCMARQ1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMARQ1F  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCMARQ1I  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQ1L  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLMARQ1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLMARQ1F  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLMARQ1I  PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRIX1L   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MPRIX1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPRIX1F   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MPRIX1I   PIC X(8).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTITE1L     COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MNENTITE1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNENTITE1F     PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNENTITE1I     PIC X(7).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTITE1L     COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MLENTITE1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLENTITE1F     PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLENTITE1I     PIC X(20).                                 00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MPAGEI    PIC X(3).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MPAGEMAXI      PIC X(3).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITREL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MTITREL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTITREF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MTITREI   PIC X(75).                                      00000650
           02 MTABLISTI OCCURS   13 TIMES .                             00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELLEL    COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MLIBELLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBELLEF    PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MLIBELLEI    PIC X(56).                                 00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MSELI   PIC X.                                          00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIENFAML    COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MLIENFAML COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIENFAMF    PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MLIENFAMI    PIC X.                                     00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MZONCMDI  PIC X(15).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MLIBERRI  PIC X(58).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCODTRAI  PIC X(4).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCICSI    PIC X(5).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MNETNAMI  PIC X(8).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MSCREENI  PIC X(4).                                       00001020
      ***************************************************************** 00001030
      * SDF: ESE03   ESE03                                              00001040
      ***************************************************************** 00001050
       01   EBS24O REDEFINES EBS24I.                                    00001060
           02 FILLER    PIC X(12).                                      00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MDATJOUA  PIC X.                                          00001090
           02 MDATJOUC  PIC X.                                          00001100
           02 MDATJOUP  PIC X.                                          00001110
           02 MDATJOUH  PIC X.                                          00001120
           02 MDATJOUV  PIC X.                                          00001130
           02 MDATJOUO  PIC X(10).                                      00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MTIMJOUA  PIC X.                                          00001160
           02 MTIMJOUC  PIC X.                                          00001170
           02 MTIMJOUP  PIC X.                                          00001180
           02 MTIMJOUH  PIC X.                                          00001190
           02 MTIMJOUV  PIC X.                                          00001200
           02 MTIMJOUO  PIC X(5).                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MCTYPENT1A     PIC X.                                     00001230
           02 MCTYPENT1C     PIC X.                                     00001240
           02 MCTYPENT1P     PIC X.                                     00001250
           02 MCTYPENT1H     PIC X.                                     00001260
           02 MCTYPENT1V     PIC X.                                     00001270
           02 MCTYPENT1O     PIC X(2).                                  00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MLTYPENT1A     PIC X.                                     00001300
           02 MLTYPENT1C     PIC X.                                     00001310
           02 MLTYPENT1P     PIC X.                                     00001320
           02 MLTYPENT1H     PIC X.                                     00001330
           02 MLTYPENT1V     PIC X.                                     00001340
           02 MLTYPENT1O     PIC X(20).                                 00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MCFAM1A   PIC X.                                          00001370
           02 MCFAM1C   PIC X.                                          00001380
           02 MCFAM1P   PIC X.                                          00001390
           02 MCFAM1H   PIC X.                                          00001400
           02 MCFAM1V   PIC X.                                          00001410
           02 MCFAM1O   PIC X(5).                                       00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MLFAM1A   PIC X.                                          00001440
           02 MLFAM1C   PIC X.                                          00001450
           02 MLFAM1P   PIC X.                                          00001460
           02 MLFAM1H   PIC X.                                          00001470
           02 MLFAM1V   PIC X.                                          00001480
           02 MLFAM1O   PIC X(20).                                      00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MSTA1A    PIC X.                                          00001510
           02 MSTA1C    PIC X.                                          00001520
           02 MSTA1P    PIC X.                                          00001530
           02 MSTA1H    PIC X.                                          00001540
           02 MSTA1V    PIC X.                                          00001550
           02 MSTA1O    PIC X(3).                                       00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MCMARQ1A  PIC X.                                          00001580
           02 MCMARQ1C  PIC X.                                          00001590
           02 MCMARQ1P  PIC X.                                          00001600
           02 MCMARQ1H  PIC X.                                          00001610
           02 MCMARQ1V  PIC X.                                          00001620
           02 MCMARQ1O  PIC X(5).                                       00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MLMARQ1A  PIC X.                                          00001650
           02 MLMARQ1C  PIC X.                                          00001660
           02 MLMARQ1P  PIC X.                                          00001670
           02 MLMARQ1H  PIC X.                                          00001680
           02 MLMARQ1V  PIC X.                                          00001690
           02 MLMARQ1O  PIC X(20).                                      00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MPRIX1A   PIC X.                                          00001720
           02 MPRIX1C   PIC X.                                          00001730
           02 MPRIX1P   PIC X.                                          00001740
           02 MPRIX1H   PIC X.                                          00001750
           02 MPRIX1V   PIC X.                                          00001760
           02 MPRIX1O   PIC X(8).                                       00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MNENTITE1A     PIC X.                                     00001790
           02 MNENTITE1C     PIC X.                                     00001800
           02 MNENTITE1P     PIC X.                                     00001810
           02 MNENTITE1H     PIC X.                                     00001820
           02 MNENTITE1V     PIC X.                                     00001830
           02 MNENTITE1O     PIC X(7).                                  00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MLENTITE1A     PIC X.                                     00001860
           02 MLENTITE1C     PIC X.                                     00001870
           02 MLENTITE1P     PIC X.                                     00001880
           02 MLENTITE1H     PIC X.                                     00001890
           02 MLENTITE1V     PIC X.                                     00001900
           02 MLENTITE1O     PIC X(20).                                 00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MPAGEA    PIC X.                                          00001930
           02 MPAGEC    PIC X.                                          00001940
           02 MPAGEP    PIC X.                                          00001950
           02 MPAGEH    PIC X.                                          00001960
           02 MPAGEV    PIC X.                                          00001970
           02 MPAGEO    PIC X(3).                                       00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MPAGEMAXA      PIC X.                                     00002000
           02 MPAGEMAXC PIC X.                                          00002010
           02 MPAGEMAXP PIC X.                                          00002020
           02 MPAGEMAXH PIC X.                                          00002030
           02 MPAGEMAXV PIC X.                                          00002040
           02 MPAGEMAXO      PIC X(3).                                  00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MTITREA   PIC X.                                          00002070
           02 MTITREC   PIC X.                                          00002080
           02 MTITREP   PIC X.                                          00002090
           02 MTITREH   PIC X.                                          00002100
           02 MTITREV   PIC X.                                          00002110
           02 MTITREO   PIC X(75).                                      00002120
           02 MTABLISTO OCCURS   13 TIMES .                             00002130
             03 FILLER       PIC X(2).                                  00002140
             03 MLIBELLEA    PIC X.                                     00002150
             03 MLIBELLEC    PIC X.                                     00002160
             03 MLIBELLEP    PIC X.                                     00002170
             03 MLIBELLEH    PIC X.                                     00002180
             03 MLIBELLEV    PIC X.                                     00002190
             03 MLIBELLEO    PIC X(56).                                 00002200
             03 FILLER       PIC X(2).                                  00002210
             03 MSELA   PIC X.                                          00002220
             03 MSELC   PIC X.                                          00002230
             03 MSELP   PIC X.                                          00002240
             03 MSELH   PIC X.                                          00002250
             03 MSELV   PIC X.                                          00002260
             03 MSELO   PIC X.                                          00002270
             03 FILLER       PIC X(2).                                  00002280
             03 MLIENFAMA    PIC X.                                     00002290
             03 MLIENFAMC    PIC X.                                     00002300
             03 MLIENFAMP    PIC X.                                     00002310
             03 MLIENFAMH    PIC X.                                     00002320
             03 MLIENFAMV    PIC X.                                     00002330
             03 MLIENFAMO    PIC X.                                     00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MZONCMDA  PIC X.                                          00002360
           02 MZONCMDC  PIC X.                                          00002370
           02 MZONCMDP  PIC X.                                          00002380
           02 MZONCMDH  PIC X.                                          00002390
           02 MZONCMDV  PIC X.                                          00002400
           02 MZONCMDO  PIC X(15).                                      00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MLIBERRA  PIC X.                                          00002430
           02 MLIBERRC  PIC X.                                          00002440
           02 MLIBERRP  PIC X.                                          00002450
           02 MLIBERRH  PIC X.                                          00002460
           02 MLIBERRV  PIC X.                                          00002470
           02 MLIBERRO  PIC X(58).                                      00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MCODTRAA  PIC X.                                          00002500
           02 MCODTRAC  PIC X.                                          00002510
           02 MCODTRAP  PIC X.                                          00002520
           02 MCODTRAH  PIC X.                                          00002530
           02 MCODTRAV  PIC X.                                          00002540
           02 MCODTRAO  PIC X(4).                                       00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MCICSA    PIC X.                                          00002570
           02 MCICSC    PIC X.                                          00002580
           02 MCICSP    PIC X.                                          00002590
           02 MCICSH    PIC X.                                          00002600
           02 MCICSV    PIC X.                                          00002610
           02 MCICSO    PIC X(5).                                       00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MNETNAMA  PIC X.                                          00002640
           02 MNETNAMC  PIC X.                                          00002650
           02 MNETNAMP  PIC X.                                          00002660
           02 MNETNAMH  PIC X.                                          00002670
           02 MNETNAMV  PIC X.                                          00002680
           02 MNETNAMO  PIC X(8).                                       00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MSCREENA  PIC X.                                          00002710
           02 MSCREENC  PIC X.                                          00002720
           02 MSCREENP  PIC X.                                          00002730
           02 MSCREENH  PIC X.                                          00002740
           02 MSCREENV  PIC X.                                          00002750
           02 MSCREENO  PIC X(4).                                       00002760
                                                                                
