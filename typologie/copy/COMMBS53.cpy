      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
       01  Z-COMMAREA-MBS53.                                            00000020
           02 COMM-MBS53-ENTETE-RETOUR.                                 00000030
      *---- BOOLEEN TYPE D'ERREUR                                       00000040
              03 COMM-MBS53-TYPERR     PIC 9(01).                       00000050
                 88 MBS53-NO-ERROR       VALUE 0.                       00000060
                 88 MBS53-APPL-ERROR     VALUE 1.                       00000070
                 88 MBS53-DB2-ERROR      VALUE 2.                       00000080
              03 COMM-MBS53-FUNC-SQL   PIC X(08).                       00000090
              03 COMM-MBS53-TABLE-NAME PIC X(08).                       00000100
              03 COMM-MBS53-SQLCODE    PIC S9(04).                      00000110
              03 COMM-MBS53-POSITION   PIC  9(03).                      00000120
      *---- BOOLEEN CODE FONCTION                                       00000130
           02 COMM-MBS53-FONC      PIC 9(01).                           00000140
              88 MBS53-CTRL-DATA     VALUE 0.                           00000150
              88 MBS53-MAJ-DB2       VALUE 1.                           00000160
      *---- DONNEES EN ENTREE / SORTIE                                  00000170
           02 COMM-MBS53-ZINOUT.                                        00000180
              03 COMM-MBS53-DATEJ           PIC X(08).                  00000190
              03 COMM-MBS53-COFFRE          PIC X(07).                  00000200
              03 COMM-MBS53-CPROFASS        PIC X(05).                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-MBS53-NBR-OCCURS      PIC S9(04)   COMP.          00000220
      *--                                                                       
              03 COMM-MBS53-NBR-OCCURS      PIC S9(04) COMP-5.                  
      *}                                                                        
              03 COMM-MBS53-TABLEAU.                                    00000230
                 05 COMM-MBS53-LIGNE       OCCURS 100.                  00000240
                    10 COMM-MBS53-NSEQENT   PIC X(02).                  00000250
                    10 COMM-MBS53-CTYPENT2  PIC X(02).                  00000260
                    10 COMM-MBS53-NENTITE2  PIC X(07).                  00000270
                    10 COMM-MBS53-WCONTUNI   PIC X(01).                 00000280
                    10 COMM-MBS53-WCONTFAM   PIC X(01).                 00000290
                    10 COMM-MBS53-CONTROLE2  PIC X(07).                 00000300
                    10 COMM-MBS53-LIBELLE.                              00000310
                       15 COMM-MBS53-LIB12.                             00000320
                          20 COMM-MBS53-LIB1   PIC X(5).                00000330
                          20 COMM-MBS53-INTER1 PIC X.                   00000340
                          20 COMM-MBS53-LIB2   PIC X(5).                00000350
                          20 COMM-MBS53-INTER2 PIC X.                   00000360
                       15 COMM-MBS53-LIB3   PIC X(20).                  00000370
                    10 COMM-MBS53-NENTITE2-EN-REF  PIC X(07).           00000380
      *------------ CODE RETOUR POSITIONNEMENT DES ERREURS              00000390
                    10 COMM-MBS53-ENT-ERR   PIC X(04).                  00000400
           02 COMM-MBS53-FILLER    PIC X(1349).                         00000410
                                                                                
