      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SGSOC SOC/ETABL CPTA PAR UTILISATEUR   *        
      *----------------------------------------------------------------*        
       01  RVGA01TD.                                                            
           05  SGSOC-CTABLEG2    PIC X(15).                                     
           05  SGSOC-CTABLEG2-REDEF REDEFINES SGSOC-CTABLEG2.                   
               10  SGSOC-CACID           PIC X(08).                             
           05  SGSOC-WTABLEG     PIC X(80).                                     
           05  SGSOC-WTABLEG-REDEF  REDEFINES SGSOC-WTABLEG.                    
               10  SGSOC-WACTIF          PIC X(01).                             
               10  SGSOC-NSOC            PIC X(03).                             
               10  SGSOC-NETAB           PIC X(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01TD-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SGSOC-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SGSOC-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SGSOC-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SGSOC-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
