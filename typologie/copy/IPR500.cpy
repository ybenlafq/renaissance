      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPR500 AU 10/07/1997  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPR500.                                                        
            05 NOMETAT-IPR500           PIC X(6) VALUE 'IPR500'.                
            05 RUPTURES-IPR500.                                                 
           10 IPR500-TYPE               PIC X(03).                      007  003
           10 IPR500-CPRESTATION        PIC X(05).                      010  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPR500-SEQUENCE           PIC S9(04) COMP.                015  002
      *--                                                                       
           10 IPR500-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPR500.                                                   
           10 IPR500-CFAM               PIC X(05).                      017  005
           10 IPR500-CGRPETAT           PIC X(05).                      022  005
           10 IPR500-CPRESTELT          PIC X(05).                      027  005
           10 IPR500-LPRESTATION        PIC X(20).                      032  020
           10 IPR500-LPRESTELT          PIC X(20).                      052  020
            05 FILLER                      PIC X(441).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPR500-LONG           PIC S9(4)   COMP  VALUE +071.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPR500-LONG           PIC S9(4) COMP-5  VALUE +071.           
                                                                                
      *}                                                                        
