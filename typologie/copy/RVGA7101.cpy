      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGA7101                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA7101                         
      **********************************************************                
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVGA7101.                                                            
           02  GA71-CTABLE                                                      
               PIC X(0006).                                                     
           02  GA71-CSTABLE                                                     
               PIC X(0005).                                                     
           02  GA71-LTABLE                                                      
               PIC X(0030).                                                     
           02  GA71-CRESP                                                       
               PIC X(0003).                                                     
           02  GA71-DDEBUT                                                      
               PIC X(0008).                                                     
           02  GA71-DFIN                                                        
               PIC X(0008).                                                     
           02  GA71-DMAJ                                                        
               PIC X(0008).                                                     
           02  GA71-CHAMP1                                                      
               PIC X(0008).                                                     
           02  GA71-LCHAMP1                                                     
               PIC X(0030).                                                     
           02  GA71-QCHAMP1                                                     
               PIC S9(3) COMP-3.                                                
           02  GA71-WCHAMP1                                                     
               PIC X(0001).                                                     
           02  GA71-QDEC1                                                       
               PIC S9(3) COMP-3.                                                
           02  GA71-WCTLDIR1                                                    
               PIC X(0001).                                                     
           02  GA71-WCTLIND1                                                    
               PIC X(0001).                                                     
           02  GA71-CCTLIND11                                                   
               PIC X(0003).                                                     
           02  GA71-CCTLIND12                                                   
               PIC X(0003).                                                     
           02  GA71-CCTLIND13                                                   
               PIC X(0003).                                                     
           02  GA71-CCTLIND14                                                   
               PIC X(0003).                                                     
           02  GA71-CCTLIND15                                                   
               PIC X(0003).                                                     
           02  GA71-CTABASS1                                                    
               PIC X(0006).                                                     
           02  GA71-CSTABASS1                                                   
               PIC X(0005).                                                     
           02  GA71-WPOSCLE1                                                    
               PIC S9(1) COMP-3.                                                
           02  GA71-CHAMP2                                                      
               PIC X(0008).                                                     
           02  GA71-LCHAMP2                                                     
               PIC X(0030).                                                     
           02  GA71-QCHAMP2                                                     
               PIC S9(3) COMP-3.                                                
           02  GA71-WCHAMP2                                                     
               PIC X(0001).                                                     
           02  GA71-QDEC2                                                       
               PIC S9(3) COMP-3.                                                
           02  GA71-WCTLDIR2                                                    
               PIC X(0001).                                                     
           02  GA71-WCTLIND2                                                    
               PIC X(0001).                                                     
           02  GA71-CCTLIND21                                                   
               PIC X(0003).                                                     
           02  GA71-CCTLIND22                                                   
               PIC X(0003).                                                     
           02  GA71-CCTLIND23                                                   
               PIC X(0003).                                                     
           02  GA71-CCTLIND24                                                   
               PIC X(0003).                                                     
           02  GA71-CCTLIND25                                                   
               PIC X(0003).                                                     
           02  GA71-CTABASS2                                                    
               PIC X(0006).                                                     
           02  GA71-CSTABASS2                                                   
               PIC X(0005).                                                     
           02  GA71-WPOSCLE2                                                    
               PIC S9(1) COMP-3.                                                
           02  GA71-CHAMP3                                                      
               PIC X(0008).                                                     
           02  GA71-LCHAMP3                                                     
               PIC X(0030).                                                     
           02  GA71-QCHAMP3                                                     
               PIC S9(3) COMP-3.                                                
           02  GA71-WCHAMP3                                                     
               PIC X(0001).                                                     
           02  GA71-QDEC3                                                       
               PIC S9(3) COMP-3.                                                
           02  GA71-WCTLDIR3                                                    
               PIC X(0001).                                                     
           02  GA71-WCTLIND3                                                    
               PIC X(0001).                                                     
           02  GA71-CCTLIND31                                                   
               PIC X(0003).                                                     
           02  GA71-CCTLIND32                                                   
               PIC X(0003).                                                     
           02  GA71-CCTLIND33                                                   
               PIC X(0003).                                                     
           02  GA71-CCTLIND34                                                   
               PIC X(0003).                                                     
           02  GA71-CCTLIND35                                                   
               PIC X(0003).                                                     
           02  GA71-CTABASS3                                                    
               PIC X(0006).                                                     
           02  GA71-CSTABASS3                                                   
               PIC X(0005).                                                     
           02  GA71-WPOSCLE3                                                    
               PIC S9(1) COMP-3.                                                
           02  GA71-CHAMP4                                                      
               PIC X(0008).                                                     
           02  GA71-LCHAMP4                                                     
               PIC X(0030).                                                     
           02  GA71-QCHAMP4                                                     
               PIC S9(3) COMP-3.                                                
           02  GA71-WCHAMP4                                                     
               PIC X(0001).                                                     
           02  GA71-QDEC4                                                       
               PIC S9(3) COMP-3.                                                
           02  GA71-WCTLDIR4                                                    
               PIC X(0001).                                                     
           02  GA71-WCTLIND4                                                    
               PIC X(0001).                                                     
           02  GA71-CCTLIND41                                                   
               PIC X(0003).                                                     
           02  GA71-CCTLIND42                                                   
               PIC X(0003).                                                     
           02  GA71-CCTLIND43                                                   
               PIC X(0003).                                                     
           02  GA71-CCTLIND44                                                   
               PIC X(0003).                                                     
           02  GA71-CCTLIND45                                                   
               PIC X(0003).                                                     
           02  GA71-CTABASS4                                                    
               PIC X(0006).                                                     
           02  GA71-CSTABASS4                                                   
               PIC X(0005).                                                     
           02  GA71-WPOSCLE4                                                    
               PIC S9(1) COMP-3.                                                
           02  GA71-CHAMP5                                                      
               PIC X(0008).                                                     
           02  GA71-LCHAMP5                                                     
               PIC X(0030).                                                     
           02  GA71-QCHAMP5                                                     
               PIC S9(3) COMP-3.                                                
           02  GA71-WCHAMP5                                                     
               PIC X(0001).                                                     
           02  GA71-QDEC5                                                       
               PIC S9(3) COMP-3.                                                
           02  GA71-WCTLDIR5                                                    
               PIC X(0001).                                                     
           02  GA71-WCTLIND5                                                    
               PIC X(0001).                                                     
           02  GA71-CCTLIND51                                                   
               PIC X(0003).                                                     
           02  GA71-CCTLIND52                                                   
               PIC X(0003).                                                     
           02  GA71-CCTLIND53                                                   
               PIC X(0003).                                                     
           02  GA71-CCTLIND54                                                   
               PIC X(0003).                                                     
           02  GA71-CCTLIND55                                                   
               PIC X(0003).                                                     
           02  GA71-CTABASS5                                                    
               PIC X(0006).                                                     
           02  GA71-CSTABASS5                                                   
               PIC X(0005).                                                     
           02  GA71-WPOSCLE5                                                    
               PIC S9(1) COMP-3.                                                
           02  GA71-CHAMP6                                                      
               PIC X(0008).                                                     
           02  GA71-LCHAMP6                                                     
               PIC X(0030).                                                     
           02  GA71-QCHAMP6                                                     
               PIC S9(3) COMP-3.                                                
           02  GA71-WCHAMP6                                                     
               PIC X(0001).                                                     
           02  GA71-QDEC6                                                       
               PIC S9(3) COMP-3.                                                
           02  GA71-WCTLDIR6                                                    
               PIC X(0001).                                                     
           02  GA71-WCTLIND6                                                    
               PIC X(0001).                                                     
           02  GA71-CCTLIND61                                                   
               PIC X(0003).                                                     
           02  GA71-CCTLIND62                                                   
               PIC X(0003).                                                     
           02  GA71-CCTLIND63                                                   
               PIC X(0003).                                                     
           02  GA71-CCTLIND64                                                   
               PIC X(0003).                                                     
           02  GA71-CCTLIND65                                                   
               PIC X(0003).                                                     
           02  GA71-CTABASS6                                                    
               PIC X(0006).                                                     
           02  GA71-CSTABASS6                                                   
               PIC X(0005).                                                     
           02  GA71-WPOSCLE6                                                    
               PIC S9(1) COMP-3.                                                
           02  GA71-NOMVUE                                                      
               PIC X(0008).                                                     
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGA7101                                  
      **********************************************************                
       01  RVGA7101-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CTABLE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CTABLE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CSTABLE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CSTABLE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-LTABLE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-LTABLE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CRESP-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CRESP-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-DDEBUT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-DDEBUT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-DFIN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-DFIN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CHAMP1-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CHAMP1-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-LCHAMP1-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-LCHAMP1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-QCHAMP1-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-QCHAMP1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-WCHAMP1-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-WCHAMP1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-QDEC1-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-QDEC1-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-WCTLDIR1-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-WCTLDIR1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-WCTLIND1-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-WCTLIND1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND11-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND11-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND12-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND12-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND13-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND13-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND14-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND14-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND15-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND15-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CTABASS1-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CTABASS1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CSTABASS1-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CSTABASS1-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-WPOSCLE1-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-WPOSCLE1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CHAMP2-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CHAMP2-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-LCHAMP2-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-LCHAMP2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-QCHAMP2-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-QCHAMP2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-WCHAMP2-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-WCHAMP2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-QDEC2-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-QDEC2-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-WCTLDIR2-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-WCTLDIR2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-WCTLIND2-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-WCTLIND2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND21-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND21-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND22-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND22-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND23-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND23-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND24-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND24-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND25-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND25-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CTABASS2-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CTABASS2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CSTABASS2-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CSTABASS2-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-WPOSCLE2-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-WPOSCLE2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CHAMP3-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CHAMP3-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-LCHAMP3-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-LCHAMP3-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-QCHAMP3-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-QCHAMP3-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-WCHAMP3-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-WCHAMP3-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-QDEC3-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-QDEC3-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-WCTLDIR3-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-WCTLDIR3-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-WCTLIND3-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-WCTLIND3-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND31-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND31-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND32-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND32-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND33-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND33-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND34-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND34-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND35-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND35-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CTABASS3-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CTABASS3-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CSTABASS3-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CSTABASS3-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-WPOSCLE3-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-WPOSCLE3-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CHAMP4-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CHAMP4-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-LCHAMP4-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-LCHAMP4-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-QCHAMP4-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-QCHAMP4-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-WCHAMP4-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-WCHAMP4-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-QDEC4-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-QDEC4-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-WCTLDIR4-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-WCTLDIR4-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-WCTLIND4-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-WCTLIND4-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND41-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND41-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND42-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND42-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND43-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND43-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND44-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND44-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND45-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND45-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CTABASS4-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CTABASS4-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CSTABASS4-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CSTABASS4-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-WPOSCLE4-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-WPOSCLE4-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CHAMP5-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CHAMP5-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-LCHAMP5-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-LCHAMP5-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-QCHAMP5-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-QCHAMP5-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-WCHAMP5-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-WCHAMP5-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-QDEC5-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-QDEC5-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-WCTLDIR5-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-WCTLDIR5-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-WCTLIND5-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-WCTLIND5-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND51-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND51-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND52-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND52-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND53-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND53-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND54-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND54-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND55-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND55-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CTABASS5-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CTABASS5-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CSTABASS5-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CSTABASS5-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-WPOSCLE5-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-WPOSCLE5-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CHAMP6-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CHAMP6-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-LCHAMP6-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-LCHAMP6-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-QCHAMP6-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-QCHAMP6-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-WCHAMP6-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-WCHAMP6-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-QDEC6-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-QDEC6-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-WCTLDIR6-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-WCTLDIR6-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-WCTLIND6-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-WCTLIND6-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND61-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND61-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND62-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND62-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND63-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND63-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND64-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND64-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CCTLIND65-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CCTLIND65-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CTABASS6-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CTABASS6-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-CSTABASS6-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-CSTABASS6-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-WPOSCLE6-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA71-WPOSCLE6-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA71-NOMVUE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GA71-NOMVUE-F                                                    
               PIC S9(4) COMP-5.                                                
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
