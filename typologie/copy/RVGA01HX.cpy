      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TXTVA TAUX TVA                         *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01HX.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01HX.                                                            
      *}                                                                        
           05  TXTVA-CTABLEG2    PIC X(15).                                     
           05  TXTVA-CTABLEG2-REDEF REDEFINES TXTVA-CTABLEG2.                   
               10  TXTVA-TAUX            PIC X(05).                             
           05  TXTVA-WTABLEG     PIC X(80).                                     
           05  TXTVA-WTABLEG-REDEF  REDEFINES TXTVA-WTABLEG.                    
               10  TXTVA-LIBELLE         PIC X(20).                             
               10  TXTVA-TVA             PIC S9(03)V9(02)     COMP-3.           
               10  TXTVA-DATE            PIC X(08).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  TXTVA-DATE-N         REDEFINES TXTVA-DATE                    
                                         PIC 9(08).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  TXTVA-ANCTVA          PIC S9(03)V9(02)     COMP-3.           
               10  TXTVA-CW              PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01HX-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01HX-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TXTVA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TXTVA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TXTVA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TXTVA-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
