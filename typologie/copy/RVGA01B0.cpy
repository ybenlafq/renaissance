      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CSELA CODE SELART/ENTREPOT/MAGASIN     *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01B0.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01B0.                                                            
      *}                                                                        
           05  CSELA-CTABLEG2    PIC X(15).                                     
           05  CSELA-CTABLEG2-REDEF REDEFINES CSELA-CTABLEG2.                   
               10  CSELA-SOCENTRE        PIC X(03).                             
               10  CSELA-DEPOT           PIC X(03).                             
               10  CSELA-SOCLIEU         PIC X(03).                             
               10  CSELA-LIEU            PIC X(03).                             
               10  CSELA-NUMERO          PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  CSELA-NUMERO-N       REDEFINES CSELA-NUMERO                  
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  CSELA-WTABLEG     PIC X(80).                                     
           05  CSELA-WTABLEG-REDEF  REDEFINES CSELA-WTABLEG.                    
               10  CSELA-CSELART         PIC X(05).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01B0-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01B0-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CSELA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CSELA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CSELA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CSELA-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
