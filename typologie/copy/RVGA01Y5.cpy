      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SLOPA NMD SL : ARBITRAGE / CODE OPER   *        
      *----------------------------------------------------------------*        
       01  RVGA01Y5.                                                            
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  SLOPA-CTABLEG2    PIC X(15).                                     
           05  SLOPA-CTABLEG2-REDEF REDEFINES SLOPA-CTABLEG2.                   
               10  SLOPA-COPAR           PIC X(05).                             
               10  SLOPA-WOR             PIC X(01).                             
               10  SLOPA-EVENT           PIC X(01).                             
               10  SLOPA-NSEQ            PIC X(01).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  SLOPA-NSEQ-N         REDEFINES SLOPA-NSEQ                    
                                         PIC 9(01).                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  SLOPA-WTABLEG     PIC X(80).                                     
           05  SLOPA-WTABLEG-REDEF  REDEFINES SLOPA-WTABLEG.                    
               10  SLOPA-COPSL           PIC X(05).                             
               10  SLOPA-WHOST           PIC X(01).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01Y5-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLOPA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SLOPA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLOPA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SLOPA-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
