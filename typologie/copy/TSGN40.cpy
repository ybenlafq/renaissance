      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : VISUALISATION DU REFERENTIEL / FILIALE (GN40)          *         
      *****************************************************************         
      *                                                               *         
       01  TS-GN40.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GN40-LONG                 PIC S9(03) COMP-3                    
                                           VALUE +59.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-GN40-DONNEES.                                                  
      *----------------------------------  CODE ACTION                          
              03 TS-GN40-ACTION            PIC X(01).                           
      *----------------------------------  CODE SOCIETE                         
              03 TS-GN40-SOCIETE           PIC X(03).                           
      *----------------------------------  CODE ZONE PRIX                       
              03 TS-GN40-ZONPRIX           PIC X(02).                           
      *----------------------------------  CODE LIEU                            
              03 TS-GN40-NLIEU             PIC X(03).                           
      *----------------------------------  REF OU REF2                          
              03 TS-GN40-LREF              PIC X(04).                           
      *----------------------------------  DATE DEBUT EFFET DEROGATION          
              03 TS-GN40-DDEFFETR          PIC X(08).                           
      *----------------------------------  DATE FIN EFFET DEROGATION            
              03 TS-GN40-DFEFFETR          PIC X(08).                           
      *----------------------------------  PRIX LOCAL                           
              03 TS-GN40-PSTDTTC           PIC S9(5)V99.                        
      *----------------------------------  DATE DEBUT EFFET PRIX LOCAL          
              03 TS-GN40-DDEFFETS          PIC X(08).                           
      *----------------------------------  DATE FIN EFFET PRIX LOCAL            
              03 TS-GN40-DFEFFETS          PIC X(08).                           
      *----------------------------------  MARGE                                
              03 TS-GN40-MARGE             PIC S9(4)V99.                        
      *----------------------------------  CODE ORIGINE                         
              03 TS-GN40-CORIG             PIC X(01).                           
                                                                                
