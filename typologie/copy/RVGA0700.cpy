      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGA0700                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA0700                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA0700.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA0700.                                                            
      *}                                                                        
           02  GA07-CGRPFOURN                                                   
               PIC X(0005).                                                     
           02  GA07-NENTCDE                                                     
               PIC X(0005).                                                     
           02  GA07-LGRPFOURN                                                   
               PIC X(0020).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGA0700                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA0700-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA0700-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA07-CGRPFOURN-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA07-CGRPFOURN-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA07-NENTCDE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA07-NENTCDE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA07-LGRPFOURN-F                                                 
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GA07-LGRPFOURN-F                                                 
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
