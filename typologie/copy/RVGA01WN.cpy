      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FLEXC CMARQ/CFAM DEPOTS AFFIL SPECIF   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01WN.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01WN.                                                            
      *}                                                                        
           05  FLEXC-CTABLEG2    PIC X(15).                                     
           05  FLEXC-CTABLEG2-REDEF REDEFINES FLEXC-CTABLEG2.                   
               10  FLEXC-NSEQ            PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  FLEXC-NSEQ-N         REDEFINES FLEXC-NSEQ                    
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  FLEXC-WTABLEG     PIC X(80).                                     
           05  FLEXC-WTABLEG-REDEF  REDEFINES FLEXC-WTABLEG.                    
               10  FLEXC-CPROAFF         PIC X(05).                             
               10  FLEXC-CMARQ           PIC X(05).                             
               10  FLEXC-CFAM            PIC X(05).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01WN-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01WN-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FLEXC-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FLEXC-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FLEXC-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FLEXC-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
