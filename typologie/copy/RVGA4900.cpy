      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE CMTD00.RTGA49                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVGA4900.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVGA4900.                                                             
      *}                                                                        
           10 GA49-NCODIC          PIC X(7).                                    
           10 GA49-IDEPT           PIC X(3).                                    
           10 GA49-ISDEPT          PIC X(3).                                    
           10 GA49-ICLAS           PIC X(3).                                    
           10 GA49-ISCLAS          PIC X(3).                                    
           10 GA49-IATRB4          PIC X(2).                                    
           10 GA49-IATRB2          PIC X(2).                                    
           10 GA49-ISTYPE          PIC X(2).                                    
           10 GA49-IWARGC          PIC X(2).                                    
           10 GA49-NBCOMPO         PIC S9(2) COMP-3.                            
           10 GA49-IATRB3          PIC X(2).                                    
           10 GA49-IRPLCD          PIC X(1).                                    
           10 GA49-I3WKMX          PIC S9(2) COMP-3.                            
           10 GA49-I2TKTD          PIC X(60).                                   
           10 GA49-I2ADVD          PIC X(60).                                   
           10 GA49-I3MKDS          PIC X(15).                                   
           10 GA49-CCOLORM         PIC X(5).                                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVGA4900-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVGA4900-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA49-NCODIC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA49-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA49-IDEPT-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 GA49-IDEPT-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA49-ISDEPT-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA49-ISDEPT-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA49-ICLAS-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 GA49-ICLAS-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA49-ICLAS-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 GA49-ICLAS-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA49-IATRB4-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA49-IATRB4-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA49-IATRB2-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA49-IATRB2-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA49-ISTYPE-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA49-ISTYPE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA49-IWARGC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA49-IWARGC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA49-NBCOMPO-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GA49-NBCOMPO-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA49-IATRB3-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA49-IATRB3-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA49-IRPLCD-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA49-IRPLCD-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA49-I3WKMX-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA49-I3WKMX-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA49-I2TKTD-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA49-I2TKTD-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA49-I2ADVD-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA49-I2ADVD-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA49-I3MKDS-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA49-I3MKDS-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA49-CCOLORM-F       PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 GA49-CCOLORM-F       PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
