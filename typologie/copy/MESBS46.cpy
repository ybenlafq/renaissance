      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ DESTOCKAGE POUR BIENS & SERVICE (B&S)                 
      *         B-VERSION                                                       
      *                                                                         
      *****************************************************************         
      *                                                                         
      *                                                                         
       01  WS-MQ10.                                                             
         02 WS-QUEUE.                                                           
               10   MQ10-MSGID    PIC    X(24).                                 
               10   MQ10-CORRELID PIC    X(24).                                 
         02 WS-CODRET             PIC    XX.                                    
         02  MESSAGE46.                                                         
           05  MESBS46-ENTETE.                                                  
               10   MESBS46-TYPE     PIC   X(3).                                
               10   MESBS46-NSOCMSG  PIC   X(3).                                
               10   MESBS46-NLIEUMSG PIC   X(3).                                
               10   MESBS46-NSOCDST  PIC   X(3).                                
               10   MESBS46-NLIEUDST PIC   X(3).                                
               10   MESBS46-NORD     PIC   9(8).                                
               10   MESBS46-LPROG    PIC   X(10).                               
               10   MESBS46-DJOUR    PIC   X(8).                                
               10   MESBS46-WSID     PIC   X(10).                               
               10   MESBS46-USER     PIC   X(10).                               
               10   MESBS46-CHRONO   PIC   9(7).                                
               10   MESBS46-NBRMSG   PIC   9(7).                                
               10   MESBS46-FILLER   PIC   X(30).                               
           05  MESBS46-DATA.                                                    
AD01         08  MESBS46-IND         PIC   9(3).                                
AD01  *      08  MESBS46-LIGNES OCCURS 40.                                      
             08  MESBS46-LIGNES OCCURS 0 TO 450                                 
                         DEPENDING ON MESBS46-IND.                              
               10   MESBS46-NCODIC   PIC    X(7).                               
               10   MESBS46-QEMPRT   PIC    X(5).                               
               10   MESBS46-QLGVRP   PIC    X(5).                               
               10   MESBS46-NSOCV    PIC    X(3).                               
               10   MESBS46-NLIEUV   PIC    X(3).                               
               10   MESBS46-NBCDE    PIC    X(7).                               
               10   MESBS46-NLIGNE   PIC    X(2).                               
NMD            10   MESBS46-NSEQNQ   PIC    S9(5).                              
 "             10   MESBS46-NSEQRM   PIC    S9(5).                              
 "             10   MESBS46-NSEQRF   PIC    S9(5).                              
 "             10   MESBS46-NSOCC    PIC    X(3).                               
 "             10   MESBS46-NLIEUC   PIC    X(3).                               
 "             10   MESBS46-DTOPE    PIC    X(8).                               
 "             10   MESBS46-NMODIF   PIC    S9(5).                              
MH             10   MESBS46-TYPVTE   PIC    X(1).                               
B&S            10   MESBS46-NSEQENS  PIC    S9(5).                              
 "             10   MESBS46-NACTVTE  PIC    S9(5).                              
CB1            10   MESBS46-NEW-NSEQNQ PIC  9(5).                               
CB1            10   MESBS46-NEW-NSEQ PIC    X(2).                               
                                                                                
