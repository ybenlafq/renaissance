      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                               *         
       01  TS-GA76.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GA76-LONG                PIC S9(03) COMP-3                     
                                           VALUE +58.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-GA76-DONNEES.                                                  
              03 TS-GA76-TOPMAJ           PIC X(1).                             
              03 TS-GA76-NCODIC           PIC X(7).                             
              03 TS-GA76-NSEQUENCE        PIC 9(2).                             
              03 TS-GA76-NEANCOMPO        PIC X(13).                            
              03 TS-GA76-LNEANCOMPO       PIC X(15).                            
              03 TS-GA76-WAUTHENT         PIC X(1).                             
              03 TS-GA76-LIDENTIFIANT     PIC X(10).                            
              03 TS-GA76-WACTIF           PIC X(1).                             
              03 TS-GA76-DACTIF           PIC X(8).                             
                                                                                
