      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RTPR22                      *        
      ******************************************************************        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVPR2200.                                                            
           10 PR22-CTYPE           PIC X(1).                                    
           10 PR22-NENTCDE         PIC X(5).                                    
           10 PR22-NCHRONO         PIC X(5).                                    
           10 PR22-NLIGNE          PIC X(5).                                    
           10 PR22-MONTANT         PIC S9(7)V9(2) USAGE COMP-3.                 
           10 PR22-DEFFET          PIC X(8).                                    
           10 PR22-DFINEFFET       PIC X(8).                                    
           10 PR22-DMAJ            PIC X(8).                                    
           10 PR22-DSYST           PIC S9(13)V USAGE COMP-3.                    
       EXEC SQL END DECLARE SECTION END-EXEC.
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 9       *        
      ******************************************************************        
                                                                                
