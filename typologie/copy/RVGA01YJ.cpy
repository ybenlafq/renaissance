      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE QCNBL QUOTA D EXTRACTION LIVRAISONS    *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01YJ.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01YJ.                                                            
      *}                                                                        
           05  QCNBL-CTABLEG2    PIC X(15).                                     
           05  QCNBL-CTABLEG2-REDEF REDEFINES QCNBL-CTABLEG2.                   
               10  QCNBL-NSOCLIVR        PIC X(03).                             
               10  QCNBL-NDEPOT          PIC X(03).                             
           05  QCNBL-WTABLEG     PIC X(80).                                     
           05  QCNBL-WTABLEG-REDEF  REDEFINES QCNBL-WTABLEG.                    
               10  QCNBL-WACTIF          PIC X(01).                             
               10  QCNBL-WLIBELLE        PIC X(18).                             
               10  QCNBL-QUOTA1          PIC X(04).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  QCNBL-QUOTA1-N       REDEFINES QCNBL-QUOTA1                  
                                         PIC 9(04).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  QCNBL-QUOTA2          PIC X(04).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  QCNBL-QUOTA2-N       REDEFINES QCNBL-QUOTA2                  
                                         PIC 9(04).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01YJ-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01YJ-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QCNBL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  QCNBL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QCNBL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  QCNBL-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
