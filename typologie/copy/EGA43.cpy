      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA43   EGA43                                              00000020
      ***************************************************************** 00000030
       01   EGA43I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETOTL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGETOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGETOTF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGETOTI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSECTL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MSECTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSECTF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSECTI    PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MINSEEL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MINSEEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MINSEEF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MINSEEI   PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFFETL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDEFFETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEFFETF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDEFFETI  PIC X(10).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEF1L    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MDEF1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDEF1F    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDEF1I    PIC X(10).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEF2L    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDEF2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDEF2F    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDEF2I    PIC X(10).                                      00000410
           02 M121I OCCURS   11 TIMES .                                 00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZONEL  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MZONEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MZONEF  PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MZONEI  PIC X(5).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCINSEEL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MCINSEEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCINSEEF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MCINSEEI     PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MBUREAUL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MBUREAUL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MBUREAUF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MBUREAUI     PIC X(26).                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMMUNL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MCOMMUNL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCOMMUNF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCOMMUNI     PIC X(32).                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MZONCMDI  PIC X(15).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MLIBERRI  PIC X(58).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCODTRAI  PIC X(4).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCICSI    PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MNETNAMI  PIC X(8).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MSCREENI  PIC X(4).                                       00000820
      ***************************************************************** 00000830
      * SDF: EGA43   EGA43                                              00000840
      ***************************************************************** 00000850
       01   EGA43O REDEFINES EGA43I.                                    00000860
           02 FILLER    PIC X(12).                                      00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MDATJOUA  PIC X.                                          00000890
           02 MDATJOUC  PIC X.                                          00000900
           02 MDATJOUP  PIC X.                                          00000910
           02 MDATJOUH  PIC X.                                          00000920
           02 MDATJOUV  PIC X.                                          00000930
           02 MDATJOUO  PIC X(10).                                      00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MTIMJOUA  PIC X.                                          00000960
           02 MTIMJOUC  PIC X.                                          00000970
           02 MTIMJOUP  PIC X.                                          00000980
           02 MTIMJOUH  PIC X.                                          00000990
           02 MTIMJOUV  PIC X.                                          00001000
           02 MTIMJOUO  PIC X(5).                                       00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MPAGEA    PIC X.                                          00001030
           02 MPAGEC    PIC X.                                          00001040
           02 MPAGEP    PIC X.                                          00001050
           02 MPAGEH    PIC X.                                          00001060
           02 MPAGEV    PIC X.                                          00001070
           02 MPAGEO    PIC ZZ9.                                        00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MPAGETOTA      PIC X.                                     00001100
           02 MPAGETOTC PIC X.                                          00001110
           02 MPAGETOTP PIC X.                                          00001120
           02 MPAGETOTH PIC X.                                          00001130
           02 MPAGETOTV PIC X.                                          00001140
           02 MPAGETOTO      PIC ZZ9.                                   00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MSECTA    PIC X.                                          00001170
           02 MSECTC    PIC X.                                          00001180
           02 MSECTP    PIC X.                                          00001190
           02 MSECTH    PIC X.                                          00001200
           02 MSECTV    PIC X.                                          00001210
           02 MSECTO    PIC X(5).                                       00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MINSEEA   PIC X.                                          00001240
           02 MINSEEC   PIC X.                                          00001250
           02 MINSEEP   PIC X.                                          00001260
           02 MINSEEH   PIC X.                                          00001270
           02 MINSEEV   PIC X.                                          00001280
           02 MINSEEO   PIC X(5).                                       00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MDEFFETA  PIC X.                                          00001310
           02 MDEFFETC  PIC X.                                          00001320
           02 MDEFFETP  PIC X.                                          00001330
           02 MDEFFETH  PIC X.                                          00001340
           02 MDEFFETV  PIC X.                                          00001350
           02 MDEFFETO  PIC X(10).                                      00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MDEF1A    PIC X.                                          00001380
           02 MDEF1C    PIC X.                                          00001390
           02 MDEF1P    PIC X.                                          00001400
           02 MDEF1H    PIC X.                                          00001410
           02 MDEF1V    PIC X.                                          00001420
           02 MDEF1O    PIC X(10).                                      00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MDEF2A    PIC X.                                          00001450
           02 MDEF2C    PIC X.                                          00001460
           02 MDEF2P    PIC X.                                          00001470
           02 MDEF2H    PIC X.                                          00001480
           02 MDEF2V    PIC X.                                          00001490
           02 MDEF2O    PIC X(10).                                      00001500
           02 M121O OCCURS   11 TIMES .                                 00001510
             03 FILLER       PIC X(2).                                  00001520
             03 MZONEA  PIC X.                                          00001530
             03 MZONEC  PIC X.                                          00001540
             03 MZONEP  PIC X.                                          00001550
             03 MZONEH  PIC X.                                          00001560
             03 MZONEV  PIC X.                                          00001570
             03 MZONEO  PIC X(5).                                       00001580
             03 FILLER       PIC X(2).                                  00001590
             03 MCINSEEA     PIC X.                                     00001600
             03 MCINSEEC     PIC X.                                     00001610
             03 MCINSEEP     PIC X.                                     00001620
             03 MCINSEEH     PIC X.                                     00001630
             03 MCINSEEV     PIC X.                                     00001640
             03 MCINSEEO     PIC X(5).                                  00001650
             03 FILLER       PIC X(2).                                  00001660
             03 MBUREAUA     PIC X.                                     00001670
             03 MBUREAUC     PIC X.                                     00001680
             03 MBUREAUP     PIC X.                                     00001690
             03 MBUREAUH     PIC X.                                     00001700
             03 MBUREAUV     PIC X.                                     00001710
             03 MBUREAUO     PIC X(26).                                 00001720
             03 FILLER       PIC X(2).                                  00001730
             03 MCOMMUNA     PIC X.                                     00001740
             03 MCOMMUNC     PIC X.                                     00001750
             03 MCOMMUNP     PIC X.                                     00001760
             03 MCOMMUNH     PIC X.                                     00001770
             03 MCOMMUNV     PIC X.                                     00001780
             03 MCOMMUNO     PIC X(32).                                 00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MZONCMDA  PIC X.                                          00001810
           02 MZONCMDC  PIC X.                                          00001820
           02 MZONCMDP  PIC X.                                          00001830
           02 MZONCMDH  PIC X.                                          00001840
           02 MZONCMDV  PIC X.                                          00001850
           02 MZONCMDO  PIC X(15).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MLIBERRA  PIC X.                                          00001880
           02 MLIBERRC  PIC X.                                          00001890
           02 MLIBERRP  PIC X.                                          00001900
           02 MLIBERRH  PIC X.                                          00001910
           02 MLIBERRV  PIC X.                                          00001920
           02 MLIBERRO  PIC X(58).                                      00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MCODTRAA  PIC X.                                          00001950
           02 MCODTRAC  PIC X.                                          00001960
           02 MCODTRAP  PIC X.                                          00001970
           02 MCODTRAH  PIC X.                                          00001980
           02 MCODTRAV  PIC X.                                          00001990
           02 MCODTRAO  PIC X(4).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MCICSA    PIC X.                                          00002020
           02 MCICSC    PIC X.                                          00002030
           02 MCICSP    PIC X.                                          00002040
           02 MCICSH    PIC X.                                          00002050
           02 MCICSV    PIC X.                                          00002060
           02 MCICSO    PIC X(5).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MNETNAMA  PIC X.                                          00002090
           02 MNETNAMC  PIC X.                                          00002100
           02 MNETNAMP  PIC X.                                          00002110
           02 MNETNAMH  PIC X.                                          00002120
           02 MNETNAMV  PIC X.                                          00002130
           02 MNETNAMO  PIC X(8).                                       00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MSCREENA  PIC X.                                          00002160
           02 MSCREENC  PIC X.                                          00002170
           02 MSCREENP  PIC X.                                          00002180
           02 MSCREENH  PIC X.                                          00002190
           02 MSCREENV  PIC X.                                          00002200
           02 MSCREENO  PIC X(4).                                       00002210
                                                                                
