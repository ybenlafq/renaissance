      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
       01  Z-COMMAREA-MBS54.                                            00000020
           02 COMM-MBS54-ENTETE-RETOUR.                                 00000030
      *---- BOOLEEN TYPE D'ERREUR                                       00000040
              03 COMM-MBS54-TYPERR     PIC 9(01).                       00000050
                 88 MBS54-NO-ERROR       VALUE 0.                       00000060
                 88 MBS54-APPL-ERROR     VALUE 1.                       00000070
                 88 MBS54-DB2-ERROR      VALUE 2.                       00000080
              03 COMM-MBS54-FUNC-SQL   PIC X(08).                       00000090
              03 COMM-MBS54-TABLE-NAME PIC X(08).                       00000100
              03 COMM-MBS54-SQLCODE    PIC S9(04).                      00000110
              03 COMM-MBS54-POSITION   PIC  9(03).                      00000120
      *---- BOOLEEN CODE FONCTION                                       00000130
           02 COMM-MBS54-FONC      PIC 9(01).                           00000140
              88 MBS54-CTRL-DATA     VALUE 0.                           00000150
              88 MBS54-MAJ-DB2       VALUE 1.                           00000160
      *---- DONNEES EN ENTREE / SORTIE                                  00000170
           02 COMM-MBS54-ZINOUT.                                        00000180
              03 COMM-MBS54-CODESFONCTION   PIC X(12).                  00000190
              03 COMM-MBS54-WFONC           PIC X(03).                  00000200
              03 COMM-MBS54-NCOFFRE         PIC X(07).                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-MBS54-NBR-OCCURS      PIC S9(04)   COMP.          00000220
      *--                                                                       
              03 COMM-MBS54-NBR-OCCURS      PIC S9(04) COMP-5.                  
      *}                                                                        
              03 COMM-MBS54-TABLEAU.                                    00000230
                 05 COMM-MBS54-LIGNE       OCCURS 80.                   00000240
                    10 COMM-MBS54-MCDOSS    PIC X(05).                  00000250
                    10 COMM-MBS54-MLDOSS    PIC X(20).                  00000260
                    10 COMM-MBS54-DATDEB    PIC X(08).                  00000270
                    10 COMM-MBS54-DATFIN    PIC X(08).                  00000280
                    10 COMM-MBS54-MNSEQ-AV  PIC X(03).                  00000290
                    10 COMM-MBS54-MNSEQ     PIC X(03).                  00000300
                    10 COMM-MBS54-MSUPP     PIC X.                      00000310
      *------------ CODE RETOUR POSITIONNEMENT DES ERREURS              00000320
                    10 COMM-MBS54-CC-ERR    PIC X(04).                  00000330
           02 COMM-MBS54-FILLER    PIC X(1500).                         00000340
                                                                                
