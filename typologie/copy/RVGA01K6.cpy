      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE DSLIE LIEUX EXCLUS DEPRECIATION STCK   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01K6.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01K6.                                                            
      *}                                                                        
           05  DSLIE-CTABLEG2    PIC X(15).                                     
           05  DSLIE-CTABLEG2-REDEF REDEFINES DSLIE-CTABLEG2.                   
               10  DSLIE-NSOCVALO        PIC X(03).                             
               10  DSLIE-NLIEUVAL        PIC X(03).                             
           05  DSLIE-WTABLEG     PIC X(80).                                     
           05  DSLIE-WTABLEG-REDEF  REDEFINES DSLIE-WTABLEG.                    
               10  DSLIE-WACTIF          PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01K6-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01K6-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  DSLIE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  DSLIE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  DSLIE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  DSLIE-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
