      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   TS : LIENS ENTRE ARTICLES ISSUE DE TGA57                    *         
      *        POUR MISE A JOUR VUE RVGA5800                          *         
      *****************************************************************         
      *                                                               *         
       01  TS-GA58.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GA58-LONG                 PIC S9(3) COMP-3                     
                                           VALUE +115.                          
      *----------------------------------  DONNEES  TS LL=115.                  
           02 TS-GA58-DONNEES.                                                  
      *----------------------------------  CODIC                                
              03 TS-GA58-NCODIC            PIC X(07).                           
      *----------------------------------  CODIC LIE                            
              03 TS-GA58-NCODICLIE         PIC X(07).                           
      *----------------------------------  TYPE DE LIEN                         
              03 TS-GA58-TYPLIE            PIC X(05).                           
      *----------------------------------  DEGRE DE LIBERTE                     
              03 TS-GA58-WDEGRELIB         PIC X(05).                           
      *----------------------------------  DEGRE DE LIBERTE                     
              03 TS-GA58-QLIEN             PIC 9(02).                           
      *----------------------------------  CODE MISE A JOUR                     
      *                                         ' ' : PAS DE MAJ                
      *                                         'C' : CREATION                  
      *                                         'M' : MODIFICATION              
      *                                         'S' : SUPPRESSION               
               03 TS-GA58-CMAJ             PIC X(1).                            
      *----------------------------------  DONNEES SPECIFIQUES PGM 58           
              03 TS-GA58-MCLIENSI          PIC X(05).                           
              03 TS-GA58-MCLIBI            PIC X(05).                           
              03 TS-GA58-MNCODIQI          PIC X(07).                           
              03 TS-GA58-MNFAMSI           PIC X(05).                           
              03 TS-GA58-MNMARQSI          PIC X(05).                           
              03 TS-GA58-MLREFSI           PIC X(20).                           
              03 TS-GA58-MPPVASI           PIC X(10).                           
              03 TS-GA58-MPPVNSI           PIC X(10).                           
              03 TS-GA58-MDDATEPI          PIC X(06).                           
              03 TS-GA58-D1RECEPT          PIC X(08).                           
              03 TS-GA58-NCODICGRP         PIC X(07).                           
                                                                                
