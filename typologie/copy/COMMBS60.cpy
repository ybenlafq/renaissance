      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
      * COMMAREA UTILISEE POUR LES PROGRAMMES TBS6* ET TBS7*            00000010
      *                                                                 00000010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-BS60-LONG-COMMAREA    PIC S9(4) COMP VALUE +4096.       00000020
      *--                                                                       
       01  COMM-BS60-LONG-COMMAREA    PIC S9(4) COMP-5 VALUE +4096.             
      *}                                                                        
      *                                                                 00000030
       01  Z-COMMAREA.                                                  00000040
                                                                        00000050
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00000060
           02 FILLER-COM-AIDA         PIC X(100).                       00000070
                                                                        00000080
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00000090
           02 COMM-CICS-APPLID        PIC X(08).                        00000100
           02 COMM-CICS-NETNAM        PIC X(08).                        00000110
           02 COMM-CICS-TRANSA        PIC X(04).                        00000120
                                                                        00000130
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00000140
           02 COMM-DATE-SIECLE        PIC X(02).                        00000150
           02 COMM-DATE-ANNEE         PIC X(02).                        00000160
           02 COMM-DATE-MOIS          PIC X(02).                        00000170
           02 COMM-DATE-JOUR          PIC 99.                           00000180
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00000190
           02 COMM-DATE-QNTA          PIC 999.                          00000200
           02 COMM-DATE-QNT0          PIC 99999.                        00000210
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00000220
           02 COMM-DATE-BISX          PIC 9.                            00000230
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00000240
           02 COMM-DATE-JSM           PIC 9.                            00000250
      *   LIBELLES DU JOUR COURT - LONG                                 00000260
           02 COMM-DATE-JSM-LC        PIC X(03).                        00000270
           02 COMM-DATE-JSM-LL        PIC X(08).                        00000280
      *   LIBELLES DU MOIS COURT - LONG                                 00000290
           02 COMM-DATE-MOIS-LC       PIC X(03).                        00000300
           02 COMM-DATE-MOIS-LL       PIC X(08).                        00000310
      *   DIFFERENTES FORMES DE DATE                                    00000320
           02 COMM-DATE-SSAAMMJJ      PIC X(08).                        00000330
           02 COMM-DATE-AAMMJJ        PIC X(06).                        00000340
           02 COMM-DATE-JJMMSSAA      PIC X(08).                        00000350
           02 COMM-DATE-JJMMAA        PIC X(06).                        00000360
           02 COMM-DATE-JJ-MM-AA      PIC X(08).                        00000370
           02 COMM-DATE-JJ-MM-SSAA    PIC X(10).                        00000380
      *   DATE DU LENDEMAIN                                             00000390
           02 COMM-DATE-DEMAIN-SAMJ   PIC X(08).                        00000400
           02 COMM-DATE-DEMAIN-JMA    PIC X(08).                        00000410
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00000420
           02 COMM-DATE-WEEK.                                           00000430
              05 COMM-DATE-SEMSS      PIC 99.                           00000440
              05 COMM-DATE-SEMAA      PIC 99.                           00000450
              05 COMM-DATE-SEMNU      PIC 99.                           00000460
           02 COMM-DATE-FILLER        PIC X(08).                        00000470
      * ZONES RESERVEES TRAITEMENT DU SWAP                              00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS          PIC S9(4) COMP VALUE -1.          00000490
      *                                                                         
      *--                                                                       
           02 COMM-SWAP-CURS          PIC S9(4) COMP-5 VALUE -1.                
                                                                        00000500
      *}                                                                        
      * ZONES RESERVEES APPLICATIVES ------------- LG : 3874            00000510
           02 COMM-BS60-APPLI.                                          00000520
              03 COMM-CODLANG         PIC X(02).                                
              03 COMM-CODPIC          PIC X(02).                                
      ******* PAGINATION ***********************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-BS60-PAGE       PIC S9(4) COMP.                           
      *--                                                                       
              03 COMM-BS60-PAGE       PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-BS60-NBP        PIC S9(4) COMP.                           
      *--                                                                       
              03 COMM-BS60-NBP        PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-BS60-PAGE-B     PIC S9(4) COMP.                           
      *--                                                                       
              03 COMM-BS60-PAGE-B     PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-BS60-NBP-B      PIC S9(4) COMP.                           
      *--                                                                       
              03 COMM-BS60-NBP-B      PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-BS60-PAGE-T     PIC S9(4) COMP.                           
      *--                                                                       
              03 COMM-BS60-PAGE-T     PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-BS60-NBP-T      PIC S9(4) COMP.                           
      *--                                                                       
              03 COMM-BS60-NBP-T      PIC S9(4) COMP-5.                         
      *}                                                                        
      ******* NIVEAU D'ACCES *******************************************        
              03 COMM-BS60-NOM-PROG   PIC X(5).                                 
      ******* UTILISATEUR ET DROIT *************************************        
              03 COMM-BS60-CACID.                                               
                 04 COMM-BS60-CSERVICE   PIC X(4).                              
                 04 FILLER               PIC X(4).                              
              03 COMM-BS60-CACID-SOC  PIC X(3).                                 
              03 COMM-BS60-CACID-LIEU PIC X(3).                                 
      ******* FILLER ***************************************************        
              03 FILLER               PIC X(800).                               
           02 COMM-BS60-REDEFINES     PIC X(3000).                              
      *----------------------------------------------------------------*        
      * ZONES APPLICATIVES BS61                                        *        
      *----------------------------------------------------------------*        
           02 COMM-BS61 REDEFINES COMM-BS60-REDEFINES.                          
              03 COMM-BS61-NSOCS     PIC X(03).                                 
              03 COMM-BS61-NLIEUS    PIC X(03).                                 
              03 COMM-BS61-CFAMS     PIC X(05).                                 
              03 FILLER              PIC X(2989).                               
      *----------------------------------------------------------------*        
      * ZONES APPLICATIVES BS70                                        *        
      *----------------------------------------------------------------*        
           02 COMM-BS70 REDEFINES COMM-BS60-REDEFINES.                          
              03 COMM-BS70-ITEM         PIC 9(02).                              
              03 COMM-BS70-NLIGNE       PIC 9(02).                              
              03 COMM-BS70-PERMIN       PIC X(08).                              
              03 COMM-BS70-PERMAX       PIC X(08).                              
              03 COMM-BS70-TOUSMAG      PIC X(01).                              
              03 COMM-BS70-HEURE        PIC X(02).                              
              03 COMM-BS70-NSOCIETE     PIC X(03).                              
              03 FILLER                 PIC X(962).                             
              03 COMM-BS70-REDEFINES    PIC X(2000).                            
      *----------------------------------------------------------------*        
      * ZONES APPLICATIVES BS71                                        *        
      *----------------------------------------------------------------*        
              03 COMM-BS71 REDEFINES COMM-BS70-REDEFINES.                       
                 04 COMM-BS71-ITEM         PIC 9(02).                           
                 04 COMM-BS71-NLIGNE       PIC 9(02).                           
                 04 COMM-BS71-PERMIN       PIC X(08).                           
                 04 COMM-BS71-PERMAX       PIC X(08).                           
                 04 COMM-BS71-NSOC         PIC X(03).                           
                 04 COMM-BS71-NMAG         PIC X(03).                           
                 04 COMM-BS71-LMAG         PIC X(20).                           
                 04 COMM-BS71-HEURE        PIC X(02).                           
                 04 COMM-BS71-DUREE        PIC 9(08).                           
                 04 FILLER                 PIC X(900).                          
                                                                                
