      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE APRET APPORT RETRAIT                   *        
      *----------------------------------------------------------------*        
       01  RVGA01AG.                                                            
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  APRET-CTABLEG2    PIC X(15).                                     
           05  APRET-CTABLEG2-REDEF REDEFINES APRET-CTABLEG2.                   
               10  APRET-CPARAM          PIC X(02).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  APRET-CPARAM-N       REDEFINES APRET-CPARAM                  
                                         PIC 9(02).                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  APRET-TAFF            PIC X(01).                             
               10  APRET-CAFF            PIC X(01).                             
           05  APRET-WTABLEG     PIC X(80).                                     
           05  APRET-WTABLEG-REDEF  REDEFINES APRET-WTABLEG.                    
               10  APRET-W               PIC X(01).                             
               10  APRET-LIB             PIC X(20).                             
               10  APRET-T               PIC X(01).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01AG-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  APRET-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  APRET-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  APRET-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  APRET-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
