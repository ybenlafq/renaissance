      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HETRT TYPES DE TRAITEMENTS GHE         *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01L8.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01L8.                                                            
      *}                                                                        
           05  HETRT-CTABLEG2    PIC X(15).                                     
           05  HETRT-CTABLEG2-REDEF REDEFINES HETRT-CTABLEG2.                   
               10  HETRT-CTRAIT          PIC X(05).                             
           05  HETRT-WTABLEG     PIC X(80).                                     
           05  HETRT-WTABLEG-REDEF  REDEFINES HETRT-WTABLEG.                    
               10  HETRT-WACTIF          PIC X(01).                             
               10  HETRT-LTRAIT          PIC X(20).                             
               10  HETRT-CMOTIF          PIC X(10).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01L8-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01L8-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HETRT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HETRT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HETRT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HETRT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
