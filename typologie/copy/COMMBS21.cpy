      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      *                                                                 00000010
       01  Z-COMMAREA-MBS21.                                            00000020
           02 COMM-MBS21-ENTETE-RETOUR.                                 00000030
      *---- BOOLEEN TYPE D'ERREUR                                       00000040
              03 COMM-MBS21-TYPERR      PIC 9(01).                      00000050
                 88 MBS21-NO-ERROR        VALUE 0.                      00000060
                 88 MBS21-APPL-ERROR      VALUE 1.                      00000070
                 88 MBS21-DB2-ERROR       VALUE 2.                      00000080
              03 COMM-MBS21-FUNC-SQL    PIC X(08).                      00000090
              03 COMM-MBS21-TABLE-NAME  PIC X(08).                      00000100
              03 COMM-MBS21-SQLCODE     PIC S9(04).                     00000110
              03 COMM-MBS21-POSITION    PIC  9(03).                     00000120
           02 COMM-MBS21-CODESFONCTION    PIC X(12).                    00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-MBS21-INDMAX           PIC S9(04) COMP.              00000140
      *--                                                                       
           02 COMM-MBS21-INDMAX           PIC S9(04) COMP-5.                    
      *}                                                                        
           02 COMM-MBS21-WFONC            PIC X(03).                    00000150
           02 COMM-MBS21-NCOFFRE          PIC X(07).                    00000160
           02 COMM-MBS21-LCOFFRE          PIC X(20).                    00000170
           02 COMM-MBS21-CMARQ            PIC X(05).                    00000180
           02 COMM-MBS21-LMARQ            PIC X(20).                    00000190
           02 COMM-MBS21-CFAM             PIC X(05).                    00000200
           02 COMM-MBS21-LCFAM            PIC X(20).                    00000210
           02 COMM-MBS21-CASSOR           PIC X(05).                    00000220
           02 COMM-MBS21-CPROFASS         PIC X(05).                    00000230
           02 COMM-MBS21-ENTITE           PIC X(07).                    00000240
           02 COMM-MBS21-CTYPENT          PIC X(02).                    00000250
           02 COMM-MBS21-FILLER           PIC X(3960).                  00000260
                                                                                
