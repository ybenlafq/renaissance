      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVGA7700                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA7700                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
       01  RVGA7700.                                                    00000090
           02  GA77-NCODIC                                              00000100
               PIC X(0007).                                             00000110
           02  GA77-NZONPRIX                                            00000120
               PIC X(0002).                                             00000130
           02  GA77-DEFFET                                              00000140
               PIC X(0008).                                             00000150
           02  GA77-DFINEFFET                                           00000160
               PIC X(0008).                                             00000170
           02  GA77-WTYPCOMM                                            00000180
               PIC X(0001).                                             00000190
           02  GA77-PCOMMVOL                                            00000200
               PIC S9(5)V9(0002) COMP-3.                                00000210
           02  GA77-WEXTENSION                                          00000220
               PIC X(0001).                                             00000230
           02  GA77-DSYST                                               00000240
               PIC S9(13) COMP-3.                                       00000250
      *                                                                 00000260
      *---------------------------------------------------------        00000270
      *   LISTE DES FLAGS DE LA TABLE RVGA7700                          00000280
      *---------------------------------------------------------        00000290
      *                                                                 00000300
       01  RVGA7700-FLAGS.                                              00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA77-NCODIC-F                                            00000320
      *        PIC S9(4) COMP.                                          00000330
      *--                                                                       
           02  GA77-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA77-NZONPRIX-F                                          00000340
      *        PIC S9(4) COMP.                                          00000350
      *--                                                                       
           02  GA77-NZONPRIX-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA77-DEFFET-F                                            00000360
      *        PIC S9(4) COMP.                                          00000370
      *--                                                                       
           02  GA77-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA77-DFINEFFET-F                                         00000380
      *        PIC S9(4) COMP.                                          00000390
      *--                                                                       
           02  GA77-DFINEFFET-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA77-WTYPCOMM-F                                          00000400
      *        PIC S9(4) COMP.                                          00000410
      *--                                                                       
           02  GA77-WTYPCOMM-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA77-PCOMMVOL-F                                          00000420
      *        PIC S9(4) COMP.                                          00000430
      *--                                                                       
           02  GA77-PCOMMVOL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA77-WEXTENSION-F                                        00000440
      *        PIC S9(4) COMP.                                          00000450
      *--                                                                       
           02  GA77-WEXTENSION-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA77-DSYST-F                                             00000460
      *        PIC S9(4) COMP.                                          00000470
      *--                                                                       
           02  GA77-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00000480
                                                                                
