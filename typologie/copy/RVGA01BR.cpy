      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CONSL CONSOLIDATION DES LIEUX          *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVGA01BR.                                                            
           05  CONSL-CTABLEG2    PIC X(15).                                     
           05  CONSL-CTABLEG2-REDEF REDEFINES CONSL-CTABLEG2.                   
               10  CONSL-CONSO           PIC X(05).                             
               10  CONSL-NSOC            PIC X(03).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  CONSL-NSOC-N         REDEFINES CONSL-NSOC                    
                                         PIC 9(03).                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  CONSL-NLIEU           PIC X(03).                             
           05  CONSL-WTABLEG     PIC X(80).                                     
           05  CONSL-WTABLEG-REDEF  REDEFINES CONSL-WTABLEG.                    
               10  CONSL-CAGR            PIC X(15).                             
               10  CONSL-NSEQ            PIC S9(02)       COMP-3.               
               10  CONSL-WACTIF          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01BR-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CONSL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CONSL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CONSL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CONSL-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
