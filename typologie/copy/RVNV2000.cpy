      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RTNV20                        *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVNV2000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVNV2000.                                                            
      *}                                                                        
           10 NV20-NSOCIETE        PIC X(03).                                   
           10 NV20-NLIEU           PIC X(03).                                   
           10 NV20-NVENTE          PIC X(07).                                   
           10 NV20-NSEQNQ          PIC S9(05)V USAGE COMP-3.                    
           10 NV20-NCODIC          PIC X(07).                                   
           10 NV20-NSOCRES         PIC X(03).                                   
           10 NV20-NLIEURES        PIC X(03).                                   
           10 NV20-CTYPRES         PIC X(02).                                   
           10 NV20-NUMRES          PIC S9(09)V USAGE COMP-3.                    
           10 NV20-ORDERID         PIC X(11).                                   
           10 NV20-ORDERITID       PIC X(11).                                   
           10 NV20-DSYST           PIC S9(13)V USAGE COMP-3.                    
           10 NV20-ORDREF          PIC X(11).                                   
           10 NV20-ORDITREF        PIC X(11).                                   
           10 NV20-BLOCKED         PIC X(01).                                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVNV2000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVNV2000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV20-NSOCIETE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 NV20-NSOCIETE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV20-NLIEU-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 NV20-NLIEU-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV20-NVENTE-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 NV20-NVENTE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV20-NSEQNQ-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 NV20-NSEQNQ-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV20-NCODIC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 NV20-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV20-NSOCRES-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 NV20-NSOCRES-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV20-NLIEURES-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 NV20-NLIEURES-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV20-CTYPRES-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 NV20-CTYPRES-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV20-NUMRES-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 NV20-NUMRES-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV20-ORDERID-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 NV20-ORDERID-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV20-ORDERITID-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 NV20-ORDERITID-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV20-DSYST-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 NV20-DSYST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV20-ORDREF-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 NV20-ORDREF-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV20-ORDITREF-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 NV20-ORDITREF-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 NV20-BLOCKED-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 NV20-BLOCKED-F       PIC S9(4) COMP-5.                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 15      *        
      ******************************************************************        
                                                                                
