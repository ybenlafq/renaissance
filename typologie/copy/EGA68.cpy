      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFL00   EFL00                                              00000020
      ***************************************************************** 00000030
       01   EGA68I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNCODICI  PIC X(7).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MREFCOD1L      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MREFCOD1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MREFCOD1F      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MREFCOD1I      PIC X(20).                                 00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAM1L   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCFAM1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAM1F   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCFAM1I   PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAM1L   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLFAM1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLFAM1F   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLFAM1I   PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQ1L  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCMARQ1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMARQ1F  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCMARQ1I  PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQ1L  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLMARQ1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLMARQ1F  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLMARQ1I  PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPROFL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCPROFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCPROFF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCPROFI   PIC X(10).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFAMCLI1L      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MFAMCLI1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MFAMCLI1F      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MFAMCLI1I      PIC X(30).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODICORL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MCODICORL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCODICORF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCODICORI      PIC X(7).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MREFCOD2L      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MREFCOD2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MREFCOD2F      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MREFCOD2I      PIC X(20).                                 00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAM2L   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCFAM2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAM2F   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCFAM2I   PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAM2L   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLFAM2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLFAM2F   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLFAM2I   PIC X(20).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQ2L  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCMARQ2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMARQ2F  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCMARQ2I  PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQ2L  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MLMARQ2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLMARQ2F  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLMARQ2I  PIC X(20).                                      00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPROFORL      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MCPROFORL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPROFORF      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCPROFORI      PIC X(10).                                 00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFAMCLI2L      COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MFAMCLI2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MFAMCLI2F      PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MFAMCLI2I      PIC X(30).                                 00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLIBERRI  PIC X(78).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCODTRAI  PIC X(4).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCICSI    PIC X(5).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MNETNAMI  PIC X(8).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MSCREENI  PIC X(4).                                       00000970
      ***************************************************************** 00000980
      * SDF: EFL00   EFL00                                              00000990
      ***************************************************************** 00001000
       01   EGA68O REDEFINES EGA68I.                                    00001010
           02 FILLER    PIC X(12).                                      00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MDATJOUA  PIC X.                                          00001040
           02 MDATJOUC  PIC X.                                          00001050
           02 MDATJOUP  PIC X.                                          00001060
           02 MDATJOUH  PIC X.                                          00001070
           02 MDATJOUV  PIC X.                                          00001080
           02 MDATJOUO  PIC X(10).                                      00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MTIMJOUA  PIC X.                                          00001110
           02 MTIMJOUC  PIC X.                                          00001120
           02 MTIMJOUP  PIC X.                                          00001130
           02 MTIMJOUH  PIC X.                                          00001140
           02 MTIMJOUV  PIC X.                                          00001150
           02 MTIMJOUO  PIC X(5).                                       00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MNCODICA  PIC X.                                          00001180
           02 MNCODICC  PIC X.                                          00001190
           02 MNCODICP  PIC X.                                          00001200
           02 MNCODICH  PIC X.                                          00001210
           02 MNCODICV  PIC X.                                          00001220
           02 MNCODICO  PIC X(7).                                       00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MREFCOD1A      PIC X.                                     00001250
           02 MREFCOD1C PIC X.                                          00001260
           02 MREFCOD1P PIC X.                                          00001270
           02 MREFCOD1H PIC X.                                          00001280
           02 MREFCOD1V PIC X.                                          00001290
           02 MREFCOD1O      PIC X(20).                                 00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MCFAM1A   PIC X.                                          00001320
           02 MCFAM1C   PIC X.                                          00001330
           02 MCFAM1P   PIC X.                                          00001340
           02 MCFAM1H   PIC X.                                          00001350
           02 MCFAM1V   PIC X.                                          00001360
           02 MCFAM1O   PIC X(5).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MLFAM1A   PIC X.                                          00001390
           02 MLFAM1C   PIC X.                                          00001400
           02 MLFAM1P   PIC X.                                          00001410
           02 MLFAM1H   PIC X.                                          00001420
           02 MLFAM1V   PIC X.                                          00001430
           02 MLFAM1O   PIC X(20).                                      00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MCMARQ1A  PIC X.                                          00001460
           02 MCMARQ1C  PIC X.                                          00001470
           02 MCMARQ1P  PIC X.                                          00001480
           02 MCMARQ1H  PIC X.                                          00001490
           02 MCMARQ1V  PIC X.                                          00001500
           02 MCMARQ1O  PIC X(5).                                       00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MLMARQ1A  PIC X.                                          00001530
           02 MLMARQ1C  PIC X.                                          00001540
           02 MLMARQ1P  PIC X.                                          00001550
           02 MLMARQ1H  PIC X.                                          00001560
           02 MLMARQ1V  PIC X.                                          00001570
           02 MLMARQ1O  PIC X(20).                                      00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MCPROFA   PIC X.                                          00001600
           02 MCPROFC   PIC X.                                          00001610
           02 MCPROFP   PIC X.                                          00001620
           02 MCPROFH   PIC X.                                          00001630
           02 MCPROFV   PIC X.                                          00001640
           02 MCPROFO   PIC X(10).                                      00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MFAMCLI1A      PIC X.                                     00001670
           02 MFAMCLI1C PIC X.                                          00001680
           02 MFAMCLI1P PIC X.                                          00001690
           02 MFAMCLI1H PIC X.                                          00001700
           02 MFAMCLI1V PIC X.                                          00001710
           02 MFAMCLI1O      PIC X(30).                                 00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MCODICORA      PIC X.                                     00001740
           02 MCODICORC PIC X.                                          00001750
           02 MCODICORP PIC X.                                          00001760
           02 MCODICORH PIC X.                                          00001770
           02 MCODICORV PIC X.                                          00001780
           02 MCODICORO      PIC X(7).                                  00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MREFCOD2A      PIC X.                                     00001810
           02 MREFCOD2C PIC X.                                          00001820
           02 MREFCOD2P PIC X.                                          00001830
           02 MREFCOD2H PIC X.                                          00001840
           02 MREFCOD2V PIC X.                                          00001850
           02 MREFCOD2O      PIC X(20).                                 00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MCFAM2A   PIC X.                                          00001880
           02 MCFAM2C   PIC X.                                          00001890
           02 MCFAM2P   PIC X.                                          00001900
           02 MCFAM2H   PIC X.                                          00001910
           02 MCFAM2V   PIC X.                                          00001920
           02 MCFAM2O   PIC X(5).                                       00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MLFAM2A   PIC X.                                          00001950
           02 MLFAM2C   PIC X.                                          00001960
           02 MLFAM2P   PIC X.                                          00001970
           02 MLFAM2H   PIC X.                                          00001980
           02 MLFAM2V   PIC X.                                          00001990
           02 MLFAM2O   PIC X(20).                                      00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MCMARQ2A  PIC X.                                          00002020
           02 MCMARQ2C  PIC X.                                          00002030
           02 MCMARQ2P  PIC X.                                          00002040
           02 MCMARQ2H  PIC X.                                          00002050
           02 MCMARQ2V  PIC X.                                          00002060
           02 MCMARQ2O  PIC X(5).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MLMARQ2A  PIC X.                                          00002090
           02 MLMARQ2C  PIC X.                                          00002100
           02 MLMARQ2P  PIC X.                                          00002110
           02 MLMARQ2H  PIC X.                                          00002120
           02 MLMARQ2V  PIC X.                                          00002130
           02 MLMARQ2O  PIC X(20).                                      00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MCPROFORA      PIC X.                                     00002160
           02 MCPROFORC PIC X.                                          00002170
           02 MCPROFORP PIC X.                                          00002180
           02 MCPROFORH PIC X.                                          00002190
           02 MCPROFORV PIC X.                                          00002200
           02 MCPROFORO      PIC X(10).                                 00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MFAMCLI2A      PIC X.                                     00002230
           02 MFAMCLI2C PIC X.                                          00002240
           02 MFAMCLI2P PIC X.                                          00002250
           02 MFAMCLI2H PIC X.                                          00002260
           02 MFAMCLI2V PIC X.                                          00002270
           02 MFAMCLI2O      PIC X(30).                                 00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MLIBERRA  PIC X.                                          00002300
           02 MLIBERRC  PIC X.                                          00002310
           02 MLIBERRP  PIC X.                                          00002320
           02 MLIBERRH  PIC X.                                          00002330
           02 MLIBERRV  PIC X.                                          00002340
           02 MLIBERRO  PIC X(78).                                      00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MCODTRAA  PIC X.                                          00002370
           02 MCODTRAC  PIC X.                                          00002380
           02 MCODTRAP  PIC X.                                          00002390
           02 MCODTRAH  PIC X.                                          00002400
           02 MCODTRAV  PIC X.                                          00002410
           02 MCODTRAO  PIC X(4).                                       00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MCICSA    PIC X.                                          00002440
           02 MCICSC    PIC X.                                          00002450
           02 MCICSP    PIC X.                                          00002460
           02 MCICSH    PIC X.                                          00002470
           02 MCICSV    PIC X.                                          00002480
           02 MCICSO    PIC X(5).                                       00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MNETNAMA  PIC X.                                          00002510
           02 MNETNAMC  PIC X.                                          00002520
           02 MNETNAMP  PIC X.                                          00002530
           02 MNETNAMH  PIC X.                                          00002540
           02 MNETNAMV  PIC X.                                          00002550
           02 MNETNAMO  PIC X(8).                                       00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MSCREENA  PIC X.                                          00002580
           02 MSCREENC  PIC X.                                          00002590
           02 MSCREENP  PIC X.                                          00002600
           02 MSCREENH  PIC X.                                          00002610
           02 MSCREENV  PIC X.                                          00002620
           02 MSCREENO  PIC X(4).                                       00002630
                                                                                
