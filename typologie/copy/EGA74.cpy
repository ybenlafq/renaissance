      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA77   EGA77                                              00000020
      ***************************************************************** 00000030
       01   EGA74I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MCFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPCOL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCOPCOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOPCOF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCOPCOI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCODICI  PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCREATIONL    COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MDCREATIONL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDCREATIONF    PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDCREATIONI    PIC X(8).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICKL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MNCODICKL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODICKF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNCODICKI      PIC X(7).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMAJL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MDMAJL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDMAJF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDMAJI    PIC X(8).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCMARQI   PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLMARQI   PIC X(20).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFOL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLREFOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLREFOF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLREFOI   PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNEANL    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNEANL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNEANF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNEANI    PIC X(13).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPRODL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MNPRODL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPRODF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNPRODI   PIC X(15).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCOLORL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCCOLORL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCCOLORF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCCOLORI  PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOLORL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLCOLORL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCOLORF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLCOLORI  PIC X(20).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSKUKL    COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MSKUKL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSKUKF    PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MSKUKI    PIC X(7).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSKUL     COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MSKUL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSKUF     PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MSKUI     PIC X(7).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCFAMI    PIC X(5).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLFAMI    PIC X(20).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIGPRODL     COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MCLIGPRODL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCLIGPRODF     PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCLIGPRODI     PIC X(5).                                  00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIGPRODL     COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MLLIGPRODL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLLIGPRODF     PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MLLIGPRODI     PIC X(20).                                 00000890
           02 MTABLEI OCCURS   6 TIMES .                                00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCDESTL      COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MCDESTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCDESTF      PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MCDESTI      PIC X(5).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLDESTL      COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MLDESTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLDESTF      PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MLDESTI      PIC X(20).                                 00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMODBASEL     COMP PIC S9(4).                            00000990
      *--                                                                       
           02 MLMODBASEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLMODBASEF     PIC X.                                     00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MLMODBASEI     PIC X(20).                                 00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVERSIONL      COMP PIC S9(4).                            00001030
      *--                                                                       
           02 MVERSIONL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MVERSIONF      PIC X.                                     00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MVERSIONI      PIC X(20).                                 00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCUSINEL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCUSINEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCUSINEF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCUSINEI  PIC X(5).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLUSINEL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MLUSINEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLUSINEF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MLUSINEI  PIC X(20).                                      00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCORIGPRODL    COMP PIC S9(4).                            00001150
      *--                                                                       
           02 MCORIGPRODL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCORIGPRODF    PIC X.                                     00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCORIGPRODI    PIC X(5).                                  00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLORIGPRODL    COMP PIC S9(4).                            00001190
      *--                                                                       
           02 MLORIGPRODL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLORIGPRODF    PIC X.                                     00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MLORIGPRODI    PIC X(20).                                 00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDESTPRODL    COMP PIC S9(4).                            00001230
      *--                                                                       
           02 MCDESTPRODL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCDESTPRODF    PIC X.                                     00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MCDESTPRODI    PIC X(5).                                  00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDESTPRODL    COMP PIC S9(4).                            00001270
      *--                                                                       
           02 MLDESTPRODL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLDESTPRODF    PIC X.                                     00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MLDESTPRODI    PIC X(20).                                 00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MZONCMDI  PIC X(15).                                      00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MLIBERRI  PIC X(58).                                      00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MCODTRAI  PIC X(4).                                       00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MCICSI    PIC X(5).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MNETNAMI  PIC X(8).                                       00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MSCREENI  PIC X(4).                                       00001540
      ***************************************************************** 00001550
      * SDF: EGA77   EGA77                                              00001560
      ***************************************************************** 00001570
       01   EGA74O REDEFINES EGA74I.                                    00001580
           02 FILLER    PIC X(12).                                      00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MDATJOUA  PIC X.                                          00001610
           02 MDATJOUC  PIC X.                                          00001620
           02 MDATJOUP  PIC X.                                          00001630
           02 MDATJOUH  PIC X.                                          00001640
           02 MDATJOUV  PIC X.                                          00001650
           02 MDATJOUO  PIC X(10).                                      00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MTIMJOUA  PIC X.                                          00001680
           02 MTIMJOUC  PIC X.                                          00001690
           02 MTIMJOUP  PIC X.                                          00001700
           02 MTIMJOUH  PIC X.                                          00001710
           02 MTIMJOUV  PIC X.                                          00001720
           02 MTIMJOUO  PIC X(5).                                       00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MCFONCA   PIC X.                                          00001750
           02 MCFONCC   PIC X.                                          00001760
           02 MCFONCP   PIC X.                                          00001770
           02 MCFONCH   PIC X.                                          00001780
           02 MCFONCV   PIC X.                                          00001790
           02 MCFONCO   PIC X(3).                                       00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MCOPCOA   PIC X.                                          00001820
           02 MCOPCOC   PIC X.                                          00001830
           02 MCOPCOP   PIC X.                                          00001840
           02 MCOPCOH   PIC X.                                          00001850
           02 MCOPCOV   PIC X.                                          00001860
           02 MCOPCOO   PIC X(3).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MNCODICA  PIC X.                                          00001890
           02 MNCODICC  PIC X.                                          00001900
           02 MNCODICP  PIC X.                                          00001910
           02 MNCODICH  PIC X.                                          00001920
           02 MNCODICV  PIC X.                                          00001930
           02 MNCODICO  PIC X(7).                                       00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MDCREATIONA    PIC X.                                     00001960
           02 MDCREATIONC    PIC X.                                     00001970
           02 MDCREATIONP    PIC X.                                     00001980
           02 MDCREATIONH    PIC X.                                     00001990
           02 MDCREATIONV    PIC X.                                     00002000
           02 MDCREATIONO    PIC X(8).                                  00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MNCODICKA      PIC X.                                     00002030
           02 MNCODICKC PIC X.                                          00002040
           02 MNCODICKP PIC X.                                          00002050
           02 MNCODICKH PIC X.                                          00002060
           02 MNCODICKV PIC X.                                          00002070
           02 MNCODICKO      PIC X(7).                                  00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MDMAJA    PIC X.                                          00002100
           02 MDMAJC    PIC X.                                          00002110
           02 MDMAJP    PIC X.                                          00002120
           02 MDMAJH    PIC X.                                          00002130
           02 MDMAJV    PIC X.                                          00002140
           02 MDMAJO    PIC X(8).                                       00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCMARQA   PIC X.                                          00002170
           02 MCMARQC   PIC X.                                          00002180
           02 MCMARQP   PIC X.                                          00002190
           02 MCMARQH   PIC X.                                          00002200
           02 MCMARQV   PIC X.                                          00002210
           02 MCMARQO   PIC X(5).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MLMARQA   PIC X.                                          00002240
           02 MLMARQC   PIC X.                                          00002250
           02 MLMARQP   PIC X.                                          00002260
           02 MLMARQH   PIC X.                                          00002270
           02 MLMARQV   PIC X.                                          00002280
           02 MLMARQO   PIC X(20).                                      00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MLREFOA   PIC X.                                          00002310
           02 MLREFOC   PIC X.                                          00002320
           02 MLREFOP   PIC X.                                          00002330
           02 MLREFOH   PIC X.                                          00002340
           02 MLREFOV   PIC X.                                          00002350
           02 MLREFOO   PIC X(20).                                      00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MNEANA    PIC X.                                          00002380
           02 MNEANC    PIC X.                                          00002390
           02 MNEANP    PIC X.                                          00002400
           02 MNEANH    PIC X.                                          00002410
           02 MNEANV    PIC X.                                          00002420
           02 MNEANO    PIC X(13).                                      00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MNPRODA   PIC X.                                          00002450
           02 MNPRODC   PIC X.                                          00002460
           02 MNPRODP   PIC X.                                          00002470
           02 MNPRODH   PIC X.                                          00002480
           02 MNPRODV   PIC X.                                          00002490
           02 MNPRODO   PIC X(15).                                      00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MCCOLORA  PIC X.                                          00002520
           02 MCCOLORC  PIC X.                                          00002530
           02 MCCOLORP  PIC X.                                          00002540
           02 MCCOLORH  PIC X.                                          00002550
           02 MCCOLORV  PIC X.                                          00002560
           02 MCCOLORO  PIC X(5).                                       00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MLCOLORA  PIC X.                                          00002590
           02 MLCOLORC  PIC X.                                          00002600
           02 MLCOLORP  PIC X.                                          00002610
           02 MLCOLORH  PIC X.                                          00002620
           02 MLCOLORV  PIC X.                                          00002630
           02 MLCOLORO  PIC X(20).                                      00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MSKUKA    PIC X.                                          00002660
           02 MSKUKC    PIC X.                                          00002670
           02 MSKUKP    PIC X.                                          00002680
           02 MSKUKH    PIC X.                                          00002690
           02 MSKUKV    PIC X.                                          00002700
           02 MSKUKO    PIC X(7).                                       00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MSKUA     PIC X.                                          00002730
           02 MSKUC     PIC X.                                          00002740
           02 MSKUP     PIC X.                                          00002750
           02 MSKUH     PIC X.                                          00002760
           02 MSKUV     PIC X.                                          00002770
           02 MSKUO     PIC X(7).                                       00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MCFAMA    PIC X.                                          00002800
           02 MCFAMC    PIC X.                                          00002810
           02 MCFAMP    PIC X.                                          00002820
           02 MCFAMH    PIC X.                                          00002830
           02 MCFAMV    PIC X.                                          00002840
           02 MCFAMO    PIC X(5).                                       00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MLFAMA    PIC X.                                          00002870
           02 MLFAMC    PIC X.                                          00002880
           02 MLFAMP    PIC X.                                          00002890
           02 MLFAMH    PIC X.                                          00002900
           02 MLFAMV    PIC X.                                          00002910
           02 MLFAMO    PIC X(20).                                      00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MCLIGPRODA     PIC X.                                     00002940
           02 MCLIGPRODC     PIC X.                                     00002950
           02 MCLIGPRODP     PIC X.                                     00002960
           02 MCLIGPRODH     PIC X.                                     00002970
           02 MCLIGPRODV     PIC X.                                     00002980
           02 MCLIGPRODO     PIC X(5).                                  00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MLLIGPRODA     PIC X.                                     00003010
           02 MLLIGPRODC     PIC X.                                     00003020
           02 MLLIGPRODP     PIC X.                                     00003030
           02 MLLIGPRODH     PIC X.                                     00003040
           02 MLLIGPRODV     PIC X.                                     00003050
           02 MLLIGPRODO     PIC X(20).                                 00003060
           02 MTABLEO OCCURS   6 TIMES .                                00003070
             03 FILLER       PIC X(2).                                  00003080
             03 MCDESTA      PIC X.                                     00003090
             03 MCDESTC PIC X.                                          00003100
             03 MCDESTP PIC X.                                          00003110
             03 MCDESTH PIC X.                                          00003120
             03 MCDESTV PIC X.                                          00003130
             03 MCDESTO      PIC X(5).                                  00003140
             03 FILLER       PIC X(2).                                  00003150
             03 MLDESTA      PIC X.                                     00003160
             03 MLDESTC PIC X.                                          00003170
             03 MLDESTP PIC X.                                          00003180
             03 MLDESTH PIC X.                                          00003190
             03 MLDESTV PIC X.                                          00003200
             03 MLDESTO      PIC X(20).                                 00003210
           02 FILLER    PIC X(2).                                       00003220
           02 MLMODBASEA     PIC X.                                     00003230
           02 MLMODBASEC     PIC X.                                     00003240
           02 MLMODBASEP     PIC X.                                     00003250
           02 MLMODBASEH     PIC X.                                     00003260
           02 MLMODBASEV     PIC X.                                     00003270
           02 MLMODBASEO     PIC X(20).                                 00003280
           02 FILLER    PIC X(2).                                       00003290
           02 MVERSIONA      PIC X.                                     00003300
           02 MVERSIONC PIC X.                                          00003310
           02 MVERSIONP PIC X.                                          00003320
           02 MVERSIONH PIC X.                                          00003330
           02 MVERSIONV PIC X.                                          00003340
           02 MVERSIONO      PIC X(20).                                 00003350
           02 FILLER    PIC X(2).                                       00003360
           02 MCUSINEA  PIC X.                                          00003370
           02 MCUSINEC  PIC X.                                          00003380
           02 MCUSINEP  PIC X.                                          00003390
           02 MCUSINEH  PIC X.                                          00003400
           02 MCUSINEV  PIC X.                                          00003410
           02 MCUSINEO  PIC X(5).                                       00003420
           02 FILLER    PIC X(2).                                       00003430
           02 MLUSINEA  PIC X.                                          00003440
           02 MLUSINEC  PIC X.                                          00003450
           02 MLUSINEP  PIC X.                                          00003460
           02 MLUSINEH  PIC X.                                          00003470
           02 MLUSINEV  PIC X.                                          00003480
           02 MLUSINEO  PIC X(20).                                      00003490
           02 FILLER    PIC X(2).                                       00003500
           02 MCORIGPRODA    PIC X.                                     00003510
           02 MCORIGPRODC    PIC X.                                     00003520
           02 MCORIGPRODP    PIC X.                                     00003530
           02 MCORIGPRODH    PIC X.                                     00003540
           02 MCORIGPRODV    PIC X.                                     00003550
           02 MCORIGPRODO    PIC X(5).                                  00003560
           02 FILLER    PIC X(2).                                       00003570
           02 MLORIGPRODA    PIC X.                                     00003580
           02 MLORIGPRODC    PIC X.                                     00003590
           02 MLORIGPRODP    PIC X.                                     00003600
           02 MLORIGPRODH    PIC X.                                     00003610
           02 MLORIGPRODV    PIC X.                                     00003620
           02 MLORIGPRODO    PIC X(20).                                 00003630
           02 FILLER    PIC X(2).                                       00003640
           02 MCDESTPRODA    PIC X.                                     00003650
           02 MCDESTPRODC    PIC X.                                     00003660
           02 MCDESTPRODP    PIC X.                                     00003670
           02 MCDESTPRODH    PIC X.                                     00003680
           02 MCDESTPRODV    PIC X.                                     00003690
           02 MCDESTPRODO    PIC X(5).                                  00003700
           02 FILLER    PIC X(2).                                       00003710
           02 MLDESTPRODA    PIC X.                                     00003720
           02 MLDESTPRODC    PIC X.                                     00003730
           02 MLDESTPRODP    PIC X.                                     00003740
           02 MLDESTPRODH    PIC X.                                     00003750
           02 MLDESTPRODV    PIC X.                                     00003760
           02 MLDESTPRODO    PIC X(20).                                 00003770
           02 FILLER    PIC X(2).                                       00003780
           02 MZONCMDA  PIC X.                                          00003790
           02 MZONCMDC  PIC X.                                          00003800
           02 MZONCMDP  PIC X.                                          00003810
           02 MZONCMDH  PIC X.                                          00003820
           02 MZONCMDV  PIC X.                                          00003830
           02 MZONCMDO  PIC X(15).                                      00003840
           02 FILLER    PIC X(2).                                       00003850
           02 MLIBERRA  PIC X.                                          00003860
           02 MLIBERRC  PIC X.                                          00003870
           02 MLIBERRP  PIC X.                                          00003880
           02 MLIBERRH  PIC X.                                          00003890
           02 MLIBERRV  PIC X.                                          00003900
           02 MLIBERRO  PIC X(58).                                      00003910
           02 FILLER    PIC X(2).                                       00003920
           02 MCODTRAA  PIC X.                                          00003930
           02 MCODTRAC  PIC X.                                          00003940
           02 MCODTRAP  PIC X.                                          00003950
           02 MCODTRAH  PIC X.                                          00003960
           02 MCODTRAV  PIC X.                                          00003970
           02 MCODTRAO  PIC X(4).                                       00003980
           02 FILLER    PIC X(2).                                       00003990
           02 MCICSA    PIC X.                                          00004000
           02 MCICSC    PIC X.                                          00004010
           02 MCICSP    PIC X.                                          00004020
           02 MCICSH    PIC X.                                          00004030
           02 MCICSV    PIC X.                                          00004040
           02 MCICSO    PIC X(5).                                       00004050
           02 FILLER    PIC X(2).                                       00004060
           02 MNETNAMA  PIC X.                                          00004070
           02 MNETNAMC  PIC X.                                          00004080
           02 MNETNAMP  PIC X.                                          00004090
           02 MNETNAMH  PIC X.                                          00004100
           02 MNETNAMV  PIC X.                                          00004110
           02 MNETNAMO  PIC X(8).                                       00004120
           02 FILLER    PIC X(2).                                       00004130
           02 MSCREENA  PIC X.                                          00004140
           02 MSCREENC  PIC X.                                          00004150
           02 MSCREENP  PIC X.                                          00004160
           02 MSCREENH  PIC X.                                          00004170
           02 MSCREENV  PIC X.                                          00004180
           02 MSCREENO  PIC X(4).                                       00004190
                                                                                
