      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
AD05  *01  TS-LONG                 PIC S9(4) COMP-3 VALUE +89.                  
AD05   01  TS-LONG                 PIC S9(4) COMP-3 VALUE +356.                 
       01  TS-DONNEES.                                                          
           05  TS-GG51-PRIEX       PIC S9(7)V99 COMP-3.                         
           05  TS-GG51-COMEX       PIC S9(4)V9 COMP-3.                          
           05  TS-GG51-DPRIEX      PIC X(8).                                    
           05  TS-GG51-PMBUEX      PIC S9(5)V99 COMP-3.                         
           05  TS-GG51-QTMAREX     PIC S9(4)V9  COMP-3.                         
           05  TS-GG51-FMILEX      PIC S9(5)V99 COMP-3.                         
           05  TS-GG51-DFIEFEX     PIC X(8).                                    
           05  TS-GG51-NCONC       PIC X(4).                                    
           05  TS-GG51-LCONC       PIC X(15).                                   
AD05  *    05  TS-GG51-LIEU OCCURS 10.                                          
AD05       05  TS-GG51-LIEU OCCURS 99.                                          
               30  TS-GG51-NLIEU   PIC X(3).                                    
           05  TS-GG51-EXISTANT    PIC X(1).                                    
           05  TS-GG51-CREATION    PIC X(1).                                    
           05  TS-GG51-MAJ         PIC X(1).                                    
           05  TS-GG51-PRIEX-EXIST PIC X.                                       
           05  TS-GG51-CORIG       PIC X.                                       
                                                                                
