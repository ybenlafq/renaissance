      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMA10   EMA10                                              00000020
      ***************************************************************** 00000030
       01   EMA10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MFONCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPLL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCTYPLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCTYPLF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCTYPLI   PIC X.                                          00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPSL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCTYPSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCTYPSF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCTYPSI   PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBSL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLIBSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIBSF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLIBSI    PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODSOCL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCODSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODSOCF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCODSOCI  PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBSOCL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLIBSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBSOCF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLIBSOCI  PIC X(20).                                      00000410
           02 M95I OCCURS   13 TIMES .                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODMAGL     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MCODMAGL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCODMAGF     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCODMAGI     PIC X(3).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBMAGL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MLIBMAGL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLIBMAGF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MLIBMAGI     PIC X(20).                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATOUVL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MDATOUVL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDATOUVF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MDATOUVI     PIC X(6).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATFERL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MDATFERL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDATFERF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MDATFERI     PIC X(6).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZONPRIL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MZONPRIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MZONPRIF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MZONPRII     PIC X(2).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZPPRESTL    COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MZPPRESTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MZPPRESTF    PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MZPPRESTI    PIC X(2).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MGRPMAGL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MGRPMAGL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MGRPMAGF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MGRPMAGI     PIC X(2).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MGRPMUTL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MGRPMUTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MGRPMUTF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MGRPMUTI     PIC X(2).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MEXPMAGL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MEXPMAGL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MEXPMAGF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MEXPMAGI     PIC X(5).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWDACEML     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MWDACEML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWDACEMF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MWDACEMI     PIC X.                                     00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCACIDL      COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MCACIDL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCACIDF      PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MCACIDI      PIC X(4).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPML      COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MCTYPML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCTYPMF      PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MCTYPMI      PIC X(3).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MZONCMDI  PIC X(15).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLIBERRI  PIC X(58).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCODTRAI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCICSI    PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MNETNAMI  PIC X(8).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MSCREENI  PIC X(5).                                       00001140
      ***************************************************************** 00001150
      * SDF: EMA10   EMA10                                              00001160
      ***************************************************************** 00001170
       01   EMA10O REDEFINES EMA10I.                                    00001180
           02 FILLER    PIC X(12).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTIMJOUA  PIC X.                                          00001280
           02 MTIMJOUC  PIC X.                                          00001290
           02 MTIMJOUP  PIC X.                                          00001300
           02 MTIMJOUH  PIC X.                                          00001310
           02 MTIMJOUV  PIC X.                                          00001320
           02 MTIMJOUO  PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MPAGEA    PIC X.                                          00001350
           02 MPAGEC    PIC X.                                          00001360
           02 MPAGEP    PIC X.                                          00001370
           02 MPAGEH    PIC X.                                          00001380
           02 MPAGEV    PIC X.                                          00001390
           02 MPAGEO    PIC X(3).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MFONCA    PIC X.                                          00001420
           02 MFONCC    PIC X.                                          00001430
           02 MFONCP    PIC X.                                          00001440
           02 MFONCH    PIC X.                                          00001450
           02 MFONCV    PIC X.                                          00001460
           02 MFONCO    PIC X(3).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MCTYPLA   PIC X.                                          00001490
           02 MCTYPLC   PIC X.                                          00001500
           02 MCTYPLP   PIC X.                                          00001510
           02 MCTYPLH   PIC X.                                          00001520
           02 MCTYPLV   PIC X.                                          00001530
           02 MCTYPLO   PIC X.                                          00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MCTYPSA   PIC X.                                          00001560
           02 MCTYPSC   PIC X.                                          00001570
           02 MCTYPSP   PIC X.                                          00001580
           02 MCTYPSH   PIC X.                                          00001590
           02 MCTYPSV   PIC X.                                          00001600
           02 MCTYPSO   PIC X(3).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MLIBSA    PIC X.                                          00001630
           02 MLIBSC    PIC X.                                          00001640
           02 MLIBSP    PIC X.                                          00001650
           02 MLIBSH    PIC X.                                          00001660
           02 MLIBSV    PIC X.                                          00001670
           02 MLIBSO    PIC X(20).                                      00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MCODSOCA  PIC X.                                          00001700
           02 MCODSOCC  PIC X.                                          00001710
           02 MCODSOCP  PIC X.                                          00001720
           02 MCODSOCH  PIC X.                                          00001730
           02 MCODSOCV  PIC X.                                          00001740
           02 MCODSOCO  PIC X(3).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MLIBSOCA  PIC X.                                          00001770
           02 MLIBSOCC  PIC X.                                          00001780
           02 MLIBSOCP  PIC X.                                          00001790
           02 MLIBSOCH  PIC X.                                          00001800
           02 MLIBSOCV  PIC X.                                          00001810
           02 MLIBSOCO  PIC X(20).                                      00001820
           02 M95O OCCURS   13 TIMES .                                  00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MCODMAGA     PIC X.                                     00001850
             03 MCODMAGC     PIC X.                                     00001860
             03 MCODMAGP     PIC X.                                     00001870
             03 MCODMAGH     PIC X.                                     00001880
             03 MCODMAGV     PIC X.                                     00001890
             03 MCODMAGO     PIC X(3).                                  00001900
             03 FILLER       PIC X(2).                                  00001910
             03 MLIBMAGA     PIC X.                                     00001920
             03 MLIBMAGC     PIC X.                                     00001930
             03 MLIBMAGP     PIC X.                                     00001940
             03 MLIBMAGH     PIC X.                                     00001950
             03 MLIBMAGV     PIC X.                                     00001960
             03 MLIBMAGO     PIC X(20).                                 00001970
             03 FILLER       PIC X(2).                                  00001980
             03 MDATOUVA     PIC X.                                     00001990
             03 MDATOUVC     PIC X.                                     00002000
             03 MDATOUVP     PIC X.                                     00002010
             03 MDATOUVH     PIC X.                                     00002020
             03 MDATOUVV     PIC X.                                     00002030
             03 MDATOUVO     PIC X(6).                                  00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MDATFERA     PIC X.                                     00002060
             03 MDATFERC     PIC X.                                     00002070
             03 MDATFERP     PIC X.                                     00002080
             03 MDATFERH     PIC X.                                     00002090
             03 MDATFERV     PIC X.                                     00002100
             03 MDATFERO     PIC X(6).                                  00002110
             03 FILLER       PIC X(2).                                  00002120
             03 MZONPRIA     PIC X.                                     00002130
             03 MZONPRIC     PIC X.                                     00002140
             03 MZONPRIP     PIC X.                                     00002150
             03 MZONPRIH     PIC X.                                     00002160
             03 MZONPRIV     PIC X.                                     00002170
             03 MZONPRIO     PIC X(2).                                  00002180
             03 FILLER       PIC X(2).                                  00002190
             03 MZPPRESTA    PIC X.                                     00002200
             03 MZPPRESTC    PIC X.                                     00002210
             03 MZPPRESTP    PIC X.                                     00002220
             03 MZPPRESTH    PIC X.                                     00002230
             03 MZPPRESTV    PIC X.                                     00002240
             03 MZPPRESTO    PIC X(2).                                  00002250
             03 FILLER       PIC X(2).                                  00002260
             03 MGRPMAGA     PIC X.                                     00002270
             03 MGRPMAGC     PIC X.                                     00002280
             03 MGRPMAGP     PIC X.                                     00002290
             03 MGRPMAGH     PIC X.                                     00002300
             03 MGRPMAGV     PIC X.                                     00002310
             03 MGRPMAGO     PIC X(2).                                  00002320
             03 FILLER       PIC X(2).                                  00002330
             03 MGRPMUTA     PIC X.                                     00002340
             03 MGRPMUTC     PIC X.                                     00002350
             03 MGRPMUTP     PIC X.                                     00002360
             03 MGRPMUTH     PIC X.                                     00002370
             03 MGRPMUTV     PIC X.                                     00002380
             03 MGRPMUTO     PIC X(2).                                  00002390
             03 FILLER       PIC X(2).                                  00002400
             03 MEXPMAGA     PIC X.                                     00002410
             03 MEXPMAGC     PIC X.                                     00002420
             03 MEXPMAGP     PIC X.                                     00002430
             03 MEXPMAGH     PIC X.                                     00002440
             03 MEXPMAGV     PIC X.                                     00002450
             03 MEXPMAGO     PIC X(5).                                  00002460
             03 FILLER       PIC X(2).                                  00002470
             03 MWDACEMA     PIC X.                                     00002480
             03 MWDACEMC     PIC X.                                     00002490
             03 MWDACEMP     PIC X.                                     00002500
             03 MWDACEMH     PIC X.                                     00002510
             03 MWDACEMV     PIC X.                                     00002520
             03 MWDACEMO     PIC X.                                     00002530
             03 FILLER       PIC X(2).                                  00002540
             03 MCACIDA      PIC X.                                     00002550
             03 MCACIDC PIC X.                                          00002560
             03 MCACIDP PIC X.                                          00002570
             03 MCACIDH PIC X.                                          00002580
             03 MCACIDV PIC X.                                          00002590
             03 MCACIDO      PIC X(4).                                  00002600
             03 FILLER       PIC X(2).                                  00002610
             03 MCTYPMA      PIC X.                                     00002620
             03 MCTYPMC PIC X.                                          00002630
             03 MCTYPMP PIC X.                                          00002640
             03 MCTYPMH PIC X.                                          00002650
             03 MCTYPMV PIC X.                                          00002660
             03 MCTYPMO      PIC X(3).                                  00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MZONCMDA  PIC X.                                          00002690
           02 MZONCMDC  PIC X.                                          00002700
           02 MZONCMDP  PIC X.                                          00002710
           02 MZONCMDH  PIC X.                                          00002720
           02 MZONCMDV  PIC X.                                          00002730
           02 MZONCMDO  PIC X(15).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MLIBERRA  PIC X.                                          00002760
           02 MLIBERRC  PIC X.                                          00002770
           02 MLIBERRP  PIC X.                                          00002780
           02 MLIBERRH  PIC X.                                          00002790
           02 MLIBERRV  PIC X.                                          00002800
           02 MLIBERRO  PIC X(58).                                      00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MCODTRAA  PIC X.                                          00002830
           02 MCODTRAC  PIC X.                                          00002840
           02 MCODTRAP  PIC X.                                          00002850
           02 MCODTRAH  PIC X.                                          00002860
           02 MCODTRAV  PIC X.                                          00002870
           02 MCODTRAO  PIC X(4).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MCICSA    PIC X.                                          00002900
           02 MCICSC    PIC X.                                          00002910
           02 MCICSP    PIC X.                                          00002920
           02 MCICSH    PIC X.                                          00002930
           02 MCICSV    PIC X.                                          00002940
           02 MCICSO    PIC X(5).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MNETNAMA  PIC X.                                          00002970
           02 MNETNAMC  PIC X.                                          00002980
           02 MNETNAMP  PIC X.                                          00002990
           02 MNETNAMH  PIC X.                                          00003000
           02 MNETNAMV  PIC X.                                          00003010
           02 MNETNAMO  PIC X(8).                                       00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MSCREENA  PIC X.                                          00003040
           02 MSCREENC  PIC X.                                          00003050
           02 MSCREENP  PIC X.                                          00003060
           02 MSCREENH  PIC X.                                          00003070
           02 MSCREENV  PIC X.                                          00003080
           02 MSCREENO  PIC X(5).                                       00003090
                                                                                
