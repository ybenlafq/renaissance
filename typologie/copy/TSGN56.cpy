      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : GESTION DES CONCURRENTS NATIONAUX      (GN56)          *         
      *****************************************************************         
      *                                                               *         
       01  TS-GN56.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GN56-LONG                 PIC S9(03) COMP-3                    
                                           VALUE +45.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-GN56-DONNEES.                                                  
      *----------------------------------  CODE SOCIETE                         
              03 TS-GN56-NSOC              PIC X(3).                            
      *----------------------------------  CODE CONCURRENT LOCAL                
              03 TS-GN56-NCONCL            PIC X(4).                            
      *----------------------------------  LIBELLE CONCURRENT LOCAL             
              03 TS-GN56-LCONCL            PIC X(15).                           
      *----------------------------------  CODE MAGASIN                         
              03 TS-GN56-NLIEU             PIC X(3).                            
      *----------------------------------  LIBELLE MAGASIN                      
              03 TS-GN56-LLIEU             PIC X(20).                           
                                                                                
