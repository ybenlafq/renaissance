      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *-----------------------------------------------------------------00000010
      *  F I C H E   D E S C R I P T I V E   D E   C O M M A R E A      00000020
      *-----------------------------------------------------------------00000030
      *                                                                 00000040
      *  PROGRAMME  : MBS11                                             00000050
      *  TITRE      : MODULE DE CALCUL DE LA COMMISSION MINIMUM OFFRE   00000060
      *                                DE LA PRIME VENDEUR              00000070
      *  ENTREE     : CODIC (CO)                                        00000080
      *  SORTIE     : MONTANT COMMISSION MINIMAL                        00000090
      *               MONTANT PRIME VENDEUR                             00000100
      *                                                                 00000110
      *-----------------------------------------------------------------00000120
       01 COMM-MBS11.                                                   00000130
          03 COMM-MBS11-MESSAGE.                                        00000140
             05 COMM-MBS11-CODE-RETOUR       PIC X(1).                  00000150
                88 COMM-MBS11-OK                         VALUE ' '.     00000160
                88 COMM-MBS11-KO                         VALUE '1'.     00000170
             05 COMM-MBS11-LIBERR            PIC X(50).                 00000180
          03 COMM-MBS11-ENTREE.                                         00000190
             05 COMM-MBS11-NCODIC            PIC X(7).                  00000200
             05 COMM-MBS11-DEFFET            PIC X(8).                  00000210
          03 COMM-MBS11-SORTIE.                                         00000220
             05 COMM-MBS11-COM-MINI          PIC S9(7)V9(2) COMP-3.     00000230
             05 COMM-MBS11-PRM-VDR           PIC S9(7)V9(2) COMP-3.     00000240
C0366-    03 COMM-MBS11-FLAG.                                           00000250
             05 COMM-MBS11-F-TRAITEMENT      PIC X(1).                  00000260
                88 COMM-MBS11-CM                      VALUE 'C'.        00000270
                88 COMM-MBS11-PV                      VALUE 'P'.        00000280
-C0366          88 COMM-MBS11-CM-PV                   VALUE '2'.        00000290
C0366 *   03 COMM-MBS11-FILLER               PIC X(24).                 00000300
C0366     03 COMM-MBS11-FILLER               PIC X(23).                 00000310
                                                                                
