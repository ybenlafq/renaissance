      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      ******************************************************************        
      *               F I C H E   D E S C R I P T I V E                *        
      ******************************************************************        
      *                                                                *        
      *  PROJET      : SUIVI DU LIEN NEM / MONETIQUE                   *        
      *  COPY        : TSBS70 : TS UTILISEE DANS L'ECRAN BS70          *        
      *  CREATION    : 17/08/2005                                      *        
      ******************************************************************        
      ******************************************************************        
      *                                                                         
          01 TS-BS70-NOM.                                                       
             03 FILLER        PIC X(4) VALUE 'BS70'.                            
             03 TS-BS70-TERM  PIC X(4) VALUE SPACES.                            
      *                                                                         
          01 TS-BS70-DATA.                                                      
             03 TS-BS70-LIGNE OCCURS 11.                                        
                05 TS-BS70-SEL     PIC X(01)   VALUE SPACES.                    
                05 TS-BS70-NSOC    PIC X(03)   VALUE SPACES.                    
                05 TS-BS70-NMAG    PIC X(03)   VALUE SPACES.                    
                05 TS-BS70-LMAG    PIC X(20)   VALUE SPACES.                    
                05 TS-BS70-CONTPS  PIC X(06)   VALUE SPACES.                    
                05 TS-BS70-TJOURS  PIC X(03)   VALUE SPACES.                    
                05 TS-BS70-CONNB   PIC 9(03)   VALUE 0.                         
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   01 TS-BS70-LONG     PIC S9(4) COMP  VALUE +429.                       
      *--                                                                       
          01 TS-BS70-LONG     PIC S9(4) COMP-5  VALUE +429.                     
      *}                                                                        
      *-------------------------------------- LONGUEUR = 11 * 36                
      *                                                                         
                                                                                
