      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGA1900                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA1900                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA1900.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA1900.                                                            
      *}                                                                        
           02  GA19-CFAM                                                        
               PIC X(0005).                                                     
           02  GA19-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GA19-NLIEU                                                       
               PIC X(0003).                                                     
           02  GA19-CEXPO                                                       
               PIC X(0010).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGA1900                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA1900-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA1900-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA19-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA19-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA19-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA19-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA19-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA19-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA19-CEXPO-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GA19-CEXPO-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
