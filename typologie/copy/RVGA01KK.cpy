      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EFINT INTERVENANTS GEF                 *        
      *----------------------------------------------------------------*        
       01  RVGA01KK.                                                            
           05  EFINT-CTABLEG2    PIC X(15).                                     
           05  EFINT-CTABLEG2-REDEF REDEFINES EFINT-CTABLEG2.                   
               10  EFINT-NSOC            PIC X(03).                             
               10  EFINT-NLIEU           PIC X(03).                             
               10  EFINT-CTRAIT          PIC X(05).                             
           05  EFINT-WTABLEG     PIC X(80).                                     
           05  EFINT-WTABLEG-REDEF  REDEFINES EFINT-WTABLEG.                    
               10  EFINT-WACTIF          PIC X(01).                             
               10  EFINT-CTYPOPT         PIC X(05).                             
               10  EFINT-LIBELLE         PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01KK-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EFINT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EFINT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EFINT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EFINT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
