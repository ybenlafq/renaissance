      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGM7000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGM7000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGM7000.                                                            
           02  GM70-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GM70-NZONE                                                       
               PIC X(0002).                                                     
           02  GM70-CHEFPROD                                                    
               PIC X(0005).                                                     
           02  GM70-WSEQFAM                                                     
               PIC S9(5) COMP-3.                                                
           02  GM70-LVMARKET1                                                   
               PIC X(0020).                                                     
           02  GM70-LVMARKET2                                                   
               PIC X(0020).                                                     
           02  GM70-LVMARKET3                                                   
               PIC X(0020).                                                     
           02  GM70-NCODIC2                                                     
               PIC X(0007).                                                     
           02  GM70-NCODIC                                                      
               PIC X(0007).                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-SEQUENCE                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-SEQUENCE                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           02  GM70-CAPPRO                                                      
               PIC X(0005).                                                     
           02  GM70-CFAM                                                        
               PIC X(0005).                                                     
           02  GM70-CMARQ                                                       
               PIC X(0005).                                                     
           02  GM70-CODLVM1                                                     
               PIC X(0001).                                                     
           02  GM70-CODLVM2                                                     
               PIC X(0001).                                                     
           02  GM70-CODLVM3                                                     
               PIC X(0001).                                                     
           02  GM70-CVDESCRIPTA                                                 
               PIC X(0005).                                                     
           02  GM70-CVDESCRIPTB                                                 
               PIC X(0005).                                                     
           02  GM70-DIFPRELEVE                                                  
               PIC X(0001).                                                     
           02  GM70-DRELEVE                                                     
               PIC X(0004).                                                     
           02  GM70-LCHEFPROD                                                   
               PIC X(0020).                                                     
           02  GM70-LCOMMENT                                                    
               PIC X(0002).                                                     
           02  GM70-LENSCONC                                                    
               PIC X(0015).                                                     
           02  GM70-LFAM                                                        
               PIC X(0020).                                                     
           02  GM70-LMAGASIN                                                    
               PIC X(0020).                                                     
           02  GM70-LREFFOURN                                                   
               PIC X(0020).                                                     
           02  GM70-LSTATCOMP                                                   
               PIC X(0003).                                                     
           02  GM70-LZONPRIX                                                    
               PIC X(0020).                                                     
           02  GM70-NCONC                                                       
               PIC X(0004).                                                     
           02  GM70-NMAG                                                        
               PIC X(0003).                                                     
           02  GM70-NMAGASIN                                                    
               PIC X(0003).                                                     
           02  GM70-NZONPRIX                                                    
               PIC X(0002).                                                     
           02  GM70-OAM                                                         
               PIC X(0001).                                                     
           02  GM70-WANO                                                        
               PIC X(0001).                                                     
           02  GM70-WANOE                                                       
               PIC X(0001).                                                     
           02  GM70-WDERO                                                       
               PIC X(0001).                                                     
           02  GM70-WDEROE                                                      
               PIC X(0001).                                                     
           02  GM70-WD15                                                        
               PIC X(0001).                                                     
           02  GM70-WEDIT                                                       
               PIC X(0001).                                                     
           02  GM70-WGROUPE                                                     
               PIC X(0001).                                                     
           02  GM70-WOA                                                         
               PIC X(0001).                                                     
           02  GM70-PCA1S                                                       
               PIC S9(11)V9(0002) COMP-3.                                       
           02  GM70-PCA4S                                                       
               PIC S9(11)V9(0002) COMP-3.                                       
           02  GM70-PCHT                                                        
               PIC S9(7)V9(0006) COMP-3.                                        
           02  GM70-PCOMMART                                                    
               PIC S9(5)V9(0002) COMP-3.                                        
           02  GM70-PCOMMARTE                                                   
               PIC S9(5)V9(0002) COMP-3.                                        
           02  GM70-PCOMMFAM                                                    
               PIC S9(5)V9(0002) COMP-3.                                        
           02  GM70-PCOMMSTD                                                    
               PIC S9(5)V9(0002) COMP-3.                                        
           02  GM70-PEXPTTC                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GM70-PMBU1S                                                      
               PIC S9(11)V9(0002) COMP-3.                                       
           02  GM70-PMBU4S                                                      
               PIC S9(11)V9(0002) COMP-3.                                       
           02  GM70-PMTPRIMES                                                   
               PIC S9(11)V9(0002) COMP-3.                                       
           02  GM70-PRELEVE                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GM70-PSTDMAG                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GM70-PSTDTTC                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GM70-QSTOCKDEP                                                   
               PIC S9(5) COMP-3.                                                
           02  GM70-QSTOCKMAG                                                   
               PIC S9(5) COMP-3.                                                
           02  GM70-QSTOCKMAGP                                                  
               PIC S9(5) COMP-3.                                                
           02  GM70-QSTOCKMAGS                                                  
               PIC S9(5) COMP-3.                                                
           02  GM70-QTAUXTVA                                                    
               PIC S9(2)V9(0003) COMP-3.                                        
           02  GM70-QV1S                                                        
               PIC S9(7) COMP-3.                                                
           02  GM70-QV2S                                                        
               PIC S9(7) COMP-3.                                                
           02  GM70-QV3S                                                        
               PIC S9(7) COMP-3.                                                
           02  GM70-QV4S                                                        
               PIC S9(7) COMP-3.                                                
           02  GM70-SRP                                                         
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GM70-QTRI                                                        
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GM70-QTRI2                                                       
               PIC S9(7)         COMP-3.                                        
           02  GM70-DFINEFFET                                                   
               PIC X(0008).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGM7000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGM7000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-NZONE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-NZONE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-CHEFPROD-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-CHEFPROD-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-WSEQFAM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-WSEQFAM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-LVMARKET1-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-LVMARKET1-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-LVMARKET2-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-LVMARKET2-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-LVMARKET3-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-LVMARKET3-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-NCODIC2-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-NCODIC2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-SEQUENCE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-SEQUENCE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-CAPPRO-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-CAPPRO-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-CODLVM1-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-CODLVM1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-CODLVM2-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-CODLVM2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-CODLVM3-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-CODLVM3-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-CVDESCRIPTA-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-CVDESCRIPTA-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-CVDESCRIPTB-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-CVDESCRIPTB-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-DIFPRELEVE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-DIFPRELEVE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-DRELEVE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-DRELEVE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-LCHEFPROD-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-LCHEFPROD-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-LCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-LENSCONC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-LENSCONC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-LFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-LFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-LMAGASIN-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-LMAGASIN-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-LREFFOURN-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-LREFFOURN-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-LSTATCOMP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-LSTATCOMP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-LZONPRIX-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-LZONPRIX-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-NCONC-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-NCONC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-NMAG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-NMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-NMAGASIN-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-NMAGASIN-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-NZONPRIX-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-NZONPRIX-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-OAM-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-OAM-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-WANO-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-WANO-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-WANOE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-WANOE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-WDERO-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-WDERO-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-WDEROE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-WDEROE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-WD15-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-WD15-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-WEDIT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-WEDIT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-WGROUPE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-WGROUPE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-WOA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-WOA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-PCA1S-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-PCA1S-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-PCA4S-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-PCA4S-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-PCHT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-PCHT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-PCOMMART-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-PCOMMART-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-PCOMMARTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-PCOMMARTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-PCOMMFAM-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-PCOMMFAM-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-PCOMMSTD-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-PCOMMSTD-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-PEXPTTC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-PEXPTTC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-PMBU1S-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-PMBU1S-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-PMBU4S-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-PMBU4S-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-PMTPRIMES-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-PMTPRIMES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-PRELEVE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-PRELEVE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-PSTDMAG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-PSTDMAG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-PSTDTTC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-PSTDTTC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-QSTOCKDEP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-QSTOCKDEP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-QSTOCKMAG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-QSTOCKMAG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-QSTOCKMAGP-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-QSTOCKMAGP-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-QSTOCKMAGS-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-QSTOCKMAGS-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-QTAUXTVA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-QTAUXTVA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-QV1S-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-QV1S-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-QV2S-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-QV2S-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-QV3S-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-QV3S-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-QV4S-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-QV4S-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-SRP-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-SRP-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-QTRI-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-QTRI-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-QTRI2-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-QTRI2-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GM70-DFINEFFET-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GM70-DFINEFFET-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
