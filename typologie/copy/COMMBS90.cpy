      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      *                                                                 00000010
       01  Z-COMMAREA-MBS90.                                            00000020
           02 COMM-MBS90-ENTETE-RETOUR.                                 00000030
      *---- BOOLEEN TYPE D'ERREUR                                       00000040
              03 COMM-MBS90-TYPERR     PIC 9(01).                       00000050
                 88 MBS90-NO-ERROR       VALUE 0.                       00000060
                 88 MBS90-DB2-ERROR      VALUE 2.                       00000070
              03 COMM-MBS90-FUNC-SQL   PIC X(08).                       00000080
              03 COMM-MBS90-TABLE-NAME PIC X(08).                       00000090
              03 COMM-MBS90-SQLCODE    PIC S9(04).                      00000100
              03 COMM-MBS90-POSITION   PIC  9(03).                      00000110
              03 COMM-MBS90-MESSAGE    PIC  X(63).                      00000120
           02 COMM-MBS90-ZINOUT.                                        00000130
              03 COMM-MBS90-CODESFONCTION   PIC X(12).                  00000140
              03 COMM-MBS90-WFONC           PIC X(03).                  00000150
              03 COMM-MBS90-DATEJ           PIC X(08).                  00000160
              03 COMM-MBS90-DATEJ1          PIC X(08).                  00000170
              03 COMM-MBS90-CICS            PIC X(05).                  00000180
              03 COMM-MBS90-NCOFFRE         PIC X(07).                  00000190
              03 COMM-MBS90-CPROFASS        PIC X(07).                  00000200
              03 COMM-MBS90-CTYPENT         PIC X(02).                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-MBS90-CPT1            PIC S9(04)   COMP.          00000220
      *--                                                                       
              03 COMM-MBS90-CPT1            PIC S9(04) COMP-5.                  
      *}                                                                        
              03 COMM-MBS90-LCOFFRE         PIC X(20).                  00000230
              03 COMM-MBS90-CMARQ           PIC X(05).                  00000240
              03 COMM-MBS90-CTVA            PIC X(05).                  00000250
              03 COMM-MBS90-CASSORTC        PIC X(05).                  00000260
              03 COMM-MBS90-CASSORTF-AV     PIC X(05).                  00000270
              03 COMM-MBS90-CASSORTF-AP     PIC X(05).                  00000280
              03 COMM-MBS90-DATEC           PIC X(08).                  00000290
              03 COMM-MBS90-DATEF           PIC X(08).                  00000300
              03 COMM-MBS90-DMAJ            PIC X(08).                  00000310
              03 COMM-MBS90-DCREATION       PIC X(08).                  00000320
              03 COMM-MBS90-LREFDARTY       PIC X(20).                  00000330
              03 COMM-MBS90-LCOMMENT        PIC X(50).                  00000340
              03 COMM-MBS90-CHEF            PIC X(05).                  00000350
              03 COMM-MBS90-STATCOMP-AV     PIC X(03).                  00000360
              03 COMM-MBS90-STATCOMP-AP     PIC X(03).                  00000370
              03 COMM-MBS90-PRIX            PIC S9(7)V99 COMP-3.        00000380
              03 COMM-MBS90-PRIME           PIC S9(7)V99 COMP-3.        00000390
              03 COMM-MBS90-CFAM-AV         PIC X(05).                  00000400
              03 COMM-MBS90-CFAM-AP         PIC X(05).                  00000410
NV01          03 COMM-MBS90-QTEMUL-AV       PIC X(01).                  00000420
NV01          03 COMM-MBS90-QTEMUL-AP       PIC X(01).                  00000430
NV01          03 COMM-MBS90-REMISE-AV       PIC X(01).                  00000440
NV01          03 COMM-MBS90-REMISE-AP       PIC X(01).                  00000450
NV01          03 COMM-MBS90-PRNTTCK-AV      PIC X(01).                  00000460
NV01          03 COMM-MBS90-PRNTTCK-AP      PIC X(01).                  00000470
SR99          03 COMM-MBS90-CLTREQ-AV       PIC X(01).                  00000480
SR99          03 COMM-MBS90-CLTREQ-AP       PIC X(01).                  00000490
SR99          03 COMM-MBS90-CLIREQ-AV       PIC X(01).                  00000500
SR99          03 COMM-MBS90-CLIREQ-AP       PIC X(01).                  00000510
              03 COMM-MBS90-CODPROF         PIC X(10).                  00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-MBS90-NBR-OCCURS-2    PIC S9(04)   COMP.          00000530
      *--                                                                       
              03 COMM-MBS90-NBR-OCCURS-2    PIC S9(04) COMP-5.                  
      *}                                                                        
              03 COMM-MBS90-TABLEAU2.                                   00000540
                 05 COMM-MBS90-LIGNE2      OCCURS 30.                   00000550
                    10 COMM-MBS90-MCCDESC   PIC X(05).                  00000560
                    10 COMM-MBS90-MLCDESC   PIC X(20).                  00000570
                    10 COMM-MBS90-MCVDESC   PIC X(05).                  00000580
                    10 COMM-MBS90-MLVDESC   PIC X(20).                  00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-MBS90-NBR-OCCURS-3    PIC S9(04)   COMP.          00000600
      *--                                                                       
              03 COMM-MBS90-NBR-OCCURS-3    PIC S9(04) COMP-5.                  
      *}                                                                        
              03 COMM-MBS90-TABLEAU3.                                   00000610
                 05 COMM-MBS90-LIGNE3      OCCURS 30.                   00000620
                    07 COMM-MBS90-KEY3-1.                               00000630
                       10 COMM-MBS90-NSEQENT        PIC X(02).          00000640
                    07 COMM-MBS90-DATA3-1.                              00000650
                       10 COMM-MBS90-NENTITE2       PIC X(07).          00000660
                       10 COMM-MBS90-DEFFET         PIC X(08).          00000670
                       10 COMM-MBS90-DFINEFFET      PIC X(08).          00000680
                    07 COMM-MBS90-KEY3-2.                               00000690
                       10 COMM-MBS90-NSEQENTORIG    PIC X(02).          00000700
                    07 COMM-MBS90-DATA3-2.                              00000710
                       10 COMM-MBS90-BLOCSEQ-AV     PIC X(03).          00000720
                       10 COMM-MBS90-BLOCSEQ-AP     PIC X(03).          00000730
                       10 COMM-MBS90-NSEQENTREF-AV  PIC X(02).          00000740
                       10 COMM-MBS90-NSEQENTREF-AP  PIC X(02).          00000750
                       10 COMM-MBS90-QDELTACLI      PIC S999V99 COMP-3. 00000760
                       10 COMM-MBS90-CPROFLIEN      PIC X(05).          00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-MBS90-NBR-OCCURS-4    PIC S9(04)   COMP.          00000780
      *--                                                                       
              03 COMM-MBS90-NBR-OCCURS-4    PIC S9(04) COMP-5.                  
      *}                                                                        
              03 COMM-MBS90-TABLEAU4.                                   00000790
                 05 COMM-MBS90-LIGNE4      OCCURS 100.                  00000800
                    10 COMM-MBS90-NSEQENT-DOSS  PIC X(02).              00000810
                    10 COMM-MBS90-LIEN-AV       PIC X(01).              00000820
                    10 COMM-MBS90-LIEN-AP       PIC X(01).              00000830
                    10 COMM-MBS90-CDOSS-AV      PIC X(05).              00000840
                    10 COMM-MBS90-CDOSS-AP      PIC X(05).              00000850
                    10 COMM-MBS90-DATDEB-AV     PIC X(08).              00000860
                    10 COMM-MBS90-DATDEB-AP     PIC X(08).              00000870
                    10 COMM-MBS90-DATFIN-AV     PIC X(08).              00000880
                    10 COMM-MBS90-DATFIN-AP     PIC X(08).              00000890
                    10 COMM-MBS90-NSEQDOS-AV    PIC X(03).              00000900
                    10 COMM-MBS90-NSEQDOS-AP    PIC X(03).              00000910
                    10 COMM-MBS90-SUPP          PIC X(01).              00000920
                                                                                
