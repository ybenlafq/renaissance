      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPR010 AU 26/10/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,05,BI,A,                          *        
      *                           25,12,BI,A,                          *        
      *                           37,05,BI,A,                          *        
      *                           42,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPR010.                                                        
            05 NOMETAT-IPR010           PIC X(6) VALUE 'IPR010'.                
            05 RUPTURES-IPR010.                                                 
           10 IPR010-NSOCIETE           PIC X(03).                      007  003
           10 IPR010-CHEFPROD           PIC X(05).                      010  005
           10 IPR010-CTYPPREST          PIC X(05).                      015  005
           10 IPR010-NENTCDE            PIC X(05).                      020  005
           10 IPR010-PREMAJOP           PIC X(12).                      025  012
           10 IPR010-CPRESTATION        PIC X(05).                      037  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPR010-SEQUENCE           PIC S9(04) COMP.                042  002
      *--                                                                       
           10 IPR010-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPR010.                                                   
           10 IPR010-CPRIME             PIC X(03).                      044  003
           10 IPR010-LCHEFPROD          PIC X(20).                      047  020
           10 IPR010-LENTCDE            PIC X(20).                      067  020
           10 IPR010-LLIEU              PIC X(20).                      087  020
           10 IPR010-LPRESTATION        PIC X(20).                      107  020
           10 IPR010-LTYPEPREST         PIC X(20).                      127  020
           10 IPR010-NZONPRIX           PIC X(02).                      147  002
           10 IPR010-MTPREST            PIC S9(07)V9(2) COMP-3.         149  005
            05 FILLER                      PIC X(359).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPR010-LONG           PIC S9(4)   COMP  VALUE +153.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPR010-LONG           PIC S9(4) COMP-5  VALUE +153.           
                                                                                
      *}                                                                        
