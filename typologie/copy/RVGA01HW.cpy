      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TXFIN TAUX D ESCOMPTE FINANCIER        *        
      *----------------------------------------------------------------*        
       01  RVGA01HW.                                                            
           05  TXFIN-CTABLEG2    PIC X(15).                                     
           05  TXFIN-CTABLEG2-REDEF REDEFINES TXFIN-CTABLEG2.                   
               10  TXFIN-SOCIETE         PIC X(03).                             
               10  TXFIN-SOCIETE-N      REDEFINES TXFIN-SOCIETE                 
                                         PIC 9(03).                             
           05  TXFIN-WTABLEG     PIC X(80).                                     
           05  TXFIN-WTABLEG-REDEF  REDEFINES TXFIN-WTABLEG.                    
               10  TXFIN-POURCENT        PIC X(05).                             
               10  TXFIN-POURCENT-N     REDEFINES TXFIN-POURCENT                
                                         PIC 9(03)V9(02).                       
               10  TXFIN-NBJOUR          PIC X(03).                             
               10  TXFIN-NBJOUR-N       REDEFINES TXFIN-NBJOUR                  
                                         PIC 9(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01HW-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TXFIN-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TXFIN-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TXFIN-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TXFIN-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
