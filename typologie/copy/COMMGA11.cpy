      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGA11 (MENU -> TGA00)    TR: GA00  *    00020001
      *              MAJ DES CODES EXPO PAR MAGASIN / FAMILLES     *    00030001
      *                                                                 00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00050000
      *                                                                 00060000
      *   TRANSACTION GA11 : MAJ DES CODES EXPO PAR MAGASIN/FAMILLES    00070001
      *                                                                 00080000
          02 COMM-GA11-APPLI REDEFINES COMM-GA00-APPLI.                 00090001
      *------------------------------ CODE FONCTION                     00100000
             03 COMM-GA11-FONCT          PIC XXX.                       00110001
      *------------------------------ LIBELLE FONCTION                  00120001
             03 COMM-GA11-LIBFCT         PIC X(13).                     00130001
      *------------------------------ CODE SOCIETE                      00140001
             03 COMM-GA11-CODSOC         PIC XXX.                       00150001
      *------------------------------ LIBELLE SOCIETE                   00160001
             03 COMM-GA11-LIBSOC         PIC X(20).                     00170001
      *------------------------------ CODE MAGASIN                      00180001
             03 COMM-GA11-CODMAG         PIC XXX.                       00190001
      *------------------------------ LIBELLE MAGASIN                   00200001
             03 COMM-GA11-LIBMAG         PIC X(20).                     00210001
      *------------------------------ CODE EXPO STANDARD DU MAGASIN     00220001
             03 COMM-GA11-EXPSTD         PIC X(5).                      00230001
      *------------------------------ POSTES FAMILLE                    00240001
             03 COMM-GA11-POSFAM         OCCURS 12.                     00250001
      *------------------------------ CODE FAMILLE                      00260001
                04 COMM-GA11-CODFAM         PIC X(5).                   00270001
      *------------------------------ LIBELLE FAMILLE                   00280001
                04 COMM-GA11-LIBFAM         PIC X(20).                  00290001
      *------------------------------ CODE EXPO FAMILLE                 00300001
                04 COMM-GA11-EXPFAM.                                    00310001
      *            06 COMM-GA11-CEXPFAM    PIC X OCCURS 5.              00320001
                   06 COMM-GA11-CEXPFAM    PIC X OCCURS 10.                     
      *------------------------------ TABLE CODE FAMILLE PR PAGINATION  00330001
             03 COMM-GA11-TABFAM         OCCURS 100.                    00340001
      *------------------------------ 1ERS CODES FAMILLE DES PAGES      00350001
                04 COMM-GA11-CODTAB         PIC 9(5).                   00360002
      *------------------------------ INDICATEUR ETAT FICHIER           00370001
             03 COMM-GA11-INDPAG            PIC 9.                      00380001
      *------------------------------ NUMERO SUIVANT WSEQFAM A AFFICHER 00390001
             03 COMM-GA11-PAGSUI            PIC 9(5).                   00400001
      *------------------------------ ANCIEN NUMERO WSEQFAM AFFICHE     00410001
             03 COMM-GA11-PAGENC            PIC 9(5).                   00420001
      *------------------------------ NUMERO DE PAGE                    00430001
             03 COMM-GA11-NUMPAG            PIC 9(3).                   00440001
      *------------------------------ ZONE SWAP                         00450001
             03 COMM-GA11-ATTR           PIC X OCCURS 200.              00460001
      *------------------------------ ZONE LIBRE                        00470001
             03 COMM-GA11-LIBRE          PIC X(2583).                   00480001
      ***************************************************************** 00490001
      ***************************************************************** 00500000
                                                                                
