      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GESTION % TRANSPOSITION FAMILLE                                 00000020
      ***************************************************************** 00000030
       01   EGG25I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLE1L     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MLIBELLE1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIBELLE1F     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLIBELLE1I     PIC X(35).                                 00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCI    PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLE2AL    COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLIBELLE2AL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLIBELLE2AF    PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLIBELLE2AI    PIC X(22).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLE2BL    COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MLIBELLE2BL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLIBELLE2BF    PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLIBELLE2BI    PIC X.                                     00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNZP1L    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNZP1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNZP1F    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNZP1I    PIC X(2).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLZP1L    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLZP1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLZP1F    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLZP1I    PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLE3AL    COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MLIBELLE3AL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLIBELLE3AF    PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLIBELLE3AI    PIC X(22).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLE3BL    COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MLIBELLE3BL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLIBELLE3BF    PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLIBELLE3BI    PIC X.                                     00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAM1L   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCFAM1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAM1F   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCFAM1I   PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAM1L   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLFAM1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLFAM1F   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLFAM1I   PIC X(20).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MPAGEI    PIC X(3).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MPAGEMAXI      PIC X(3).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLE4L     COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MLIBELLE4L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIBELLE4F     PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLIBELLE4I     PIC X(5).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLE5L     COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MLIBELLE5L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIBELLE5F     PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLIBELLE5I     PIC X(5).                                  00000730
           02 MLIGNEI OCCURS   12 TIMES .                               00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTL   COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MACTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MACTF   PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MACTI   PIC X.                                          00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MCFAMI  PIC X(5).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPOURCENTL   COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MPOURCENTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MPOURCENTF   PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MPOURCENTI   PIC X(5).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MDEFFETI     PIC X(8).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDUPCOEFFL   COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MDUPCOEFFL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MDUPCOEFFF   PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MDUPCOEFFI   PIC X.                                     00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOMMENTL   COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MLCOMMENTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLCOMMENTF   PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MLCOMMENTI   PIC X(20).                                 00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTOPMAJL     COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MTOPMAJL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTOPMAJF     PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MTOPMAJI     PIC X.                                     00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDUPTOPEL    COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MDUPTOPEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDUPTOPEF    PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MDUPTOPEI    PIC X.                                     00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MLIBERRI  PIC X(79).                                      00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCODTRAI  PIC X(4).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCICSI    PIC X(5).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MNETNAMI  PIC X(8).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MSCREENI  PIC X(4).                                       00001260
      ***************************************************************** 00001270
      * GESTION % TRANSPOSITION FAMILLE                                 00001280
      ***************************************************************** 00001290
       01   EGG25O REDEFINES EGG25I.                                    00001300
           02 FILLER    PIC X(12).                                      00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MDATJOUA  PIC X.                                          00001330
           02 MDATJOUC  PIC X.                                          00001340
           02 MDATJOUP  PIC X.                                          00001350
           02 MDATJOUH  PIC X.                                          00001360
           02 MDATJOUV  PIC X.                                          00001370
           02 MDATJOUO  PIC X(10).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MTIMJOUA  PIC X.                                          00001400
           02 MTIMJOUC  PIC X.                                          00001410
           02 MTIMJOUP  PIC X.                                          00001420
           02 MTIMJOUH  PIC X.                                          00001430
           02 MTIMJOUV  PIC X.                                          00001440
           02 MTIMJOUO  PIC X(5).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MWFONCA   PIC X.                                          00001470
           02 MWFONCC   PIC X.                                          00001480
           02 MWFONCP   PIC X.                                          00001490
           02 MWFONCH   PIC X.                                          00001500
           02 MWFONCV   PIC X.                                          00001510
           02 MWFONCO   PIC X(3).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MLIBELLE1A     PIC X.                                     00001540
           02 MLIBELLE1C     PIC X.                                     00001550
           02 MLIBELLE1P     PIC X.                                     00001560
           02 MLIBELLE1H     PIC X.                                     00001570
           02 MLIBELLE1V     PIC X.                                     00001580
           02 MLIBELLE1O     PIC X(35).                                 00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MNSOCA    PIC X.                                          00001610
           02 MNSOCC    PIC X.                                          00001620
           02 MNSOCP    PIC X.                                          00001630
           02 MNSOCH    PIC X.                                          00001640
           02 MNSOCV    PIC X.                                          00001650
           02 MNSOCO    PIC X(3).                                       00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MLIBELLE2AA    PIC X.                                     00001680
           02 MLIBELLE2AC    PIC X.                                     00001690
           02 MLIBELLE2AP    PIC X.                                     00001700
           02 MLIBELLE2AH    PIC X.                                     00001710
           02 MLIBELLE2AV    PIC X.                                     00001720
           02 MLIBELLE2AO    PIC X(22).                                 00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MLIBELLE2BA    PIC X.                                     00001750
           02 MLIBELLE2BC    PIC X.                                     00001760
           02 MLIBELLE2BP    PIC X.                                     00001770
           02 MLIBELLE2BH    PIC X.                                     00001780
           02 MLIBELLE2BV    PIC X.                                     00001790
           02 MLIBELLE2BO    PIC X.                                     00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MNZP1A    PIC X.                                          00001820
           02 MNZP1C    PIC X.                                          00001830
           02 MNZP1P    PIC X.                                          00001840
           02 MNZP1H    PIC X.                                          00001850
           02 MNZP1V    PIC X.                                          00001860
           02 MNZP1O    PIC X(2).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MLZP1A    PIC X.                                          00001890
           02 MLZP1C    PIC X.                                          00001900
           02 MLZP1P    PIC X.                                          00001910
           02 MLZP1H    PIC X.                                          00001920
           02 MLZP1V    PIC X.                                          00001930
           02 MLZP1O    PIC X(20).                                      00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MLIBELLE3AA    PIC X.                                     00001960
           02 MLIBELLE3AC    PIC X.                                     00001970
           02 MLIBELLE3AP    PIC X.                                     00001980
           02 MLIBELLE3AH    PIC X.                                     00001990
           02 MLIBELLE3AV    PIC X.                                     00002000
           02 MLIBELLE3AO    PIC X(22).                                 00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MLIBELLE3BA    PIC X.                                     00002030
           02 MLIBELLE3BC    PIC X.                                     00002040
           02 MLIBELLE3BP    PIC X.                                     00002050
           02 MLIBELLE3BH    PIC X.                                     00002060
           02 MLIBELLE3BV    PIC X.                                     00002070
           02 MLIBELLE3BO    PIC X.                                     00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MCFAM1A   PIC X.                                          00002100
           02 MCFAM1C   PIC X.                                          00002110
           02 MCFAM1P   PIC X.                                          00002120
           02 MCFAM1H   PIC X.                                          00002130
           02 MCFAM1V   PIC X.                                          00002140
           02 MCFAM1O   PIC X(5).                                       00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MLFAM1A   PIC X.                                          00002170
           02 MLFAM1C   PIC X.                                          00002180
           02 MLFAM1P   PIC X.                                          00002190
           02 MLFAM1H   PIC X.                                          00002200
           02 MLFAM1V   PIC X.                                          00002210
           02 MLFAM1O   PIC X(20).                                      00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MPAGEA    PIC X.                                          00002240
           02 MPAGEC    PIC X.                                          00002250
           02 MPAGEP    PIC X.                                          00002260
           02 MPAGEH    PIC X.                                          00002270
           02 MPAGEV    PIC X.                                          00002280
           02 MPAGEO    PIC X(3).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MPAGEMAXA      PIC X.                                     00002310
           02 MPAGEMAXC PIC X.                                          00002320
           02 MPAGEMAXP PIC X.                                          00002330
           02 MPAGEMAXH PIC X.                                          00002340
           02 MPAGEMAXV PIC X.                                          00002350
           02 MPAGEMAXO      PIC X(3).                                  00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MLIBELLE4A     PIC X.                                     00002380
           02 MLIBELLE4C     PIC X.                                     00002390
           02 MLIBELLE4P     PIC X.                                     00002400
           02 MLIBELLE4H     PIC X.                                     00002410
           02 MLIBELLE4V     PIC X.                                     00002420
           02 MLIBELLE4O     PIC X(5).                                  00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MLIBELLE5A     PIC X.                                     00002450
           02 MLIBELLE5C     PIC X.                                     00002460
           02 MLIBELLE5P     PIC X.                                     00002470
           02 MLIBELLE5H     PIC X.                                     00002480
           02 MLIBELLE5V     PIC X.                                     00002490
           02 MLIBELLE5O     PIC X(5).                                  00002500
           02 MLIGNEO OCCURS   12 TIMES .                               00002510
             03 FILLER       PIC X(2).                                  00002520
             03 MACTA   PIC X.                                          00002530
             03 MACTC   PIC X.                                          00002540
             03 MACTP   PIC X.                                          00002550
             03 MACTH   PIC X.                                          00002560
             03 MACTV   PIC X.                                          00002570
             03 MACTO   PIC X.                                          00002580
             03 FILLER       PIC X(2).                                  00002590
             03 MCFAMA  PIC X.                                          00002600
             03 MCFAMC  PIC X.                                          00002610
             03 MCFAMP  PIC X.                                          00002620
             03 MCFAMH  PIC X.                                          00002630
             03 MCFAMV  PIC X.                                          00002640
             03 MCFAMO  PIC X(5).                                       00002650
             03 FILLER       PIC X(2).                                  00002660
             03 MPOURCENTA   PIC X.                                     00002670
             03 MPOURCENTC   PIC X.                                     00002680
             03 MPOURCENTP   PIC X.                                     00002690
             03 MPOURCENTH   PIC X.                                     00002700
             03 MPOURCENTV   PIC X.                                     00002710
             03 MPOURCENTO   PIC X(5).                                  00002720
             03 FILLER       PIC X(2).                                  00002730
             03 MDEFFETA     PIC X.                                     00002740
             03 MDEFFETC     PIC X.                                     00002750
             03 MDEFFETP     PIC X.                                     00002760
             03 MDEFFETH     PIC X.                                     00002770
             03 MDEFFETV     PIC X.                                     00002780
             03 MDEFFETO     PIC X(8).                                  00002790
             03 FILLER       PIC X(2).                                  00002800
             03 MDUPCOEFFA   PIC X.                                     00002810
             03 MDUPCOEFFC   PIC X.                                     00002820
             03 MDUPCOEFFP   PIC X.                                     00002830
             03 MDUPCOEFFH   PIC X.                                     00002840
             03 MDUPCOEFFV   PIC X.                                     00002850
             03 MDUPCOEFFO   PIC X.                                     00002860
             03 FILLER       PIC X(2).                                  00002870
             03 MLCOMMENTA   PIC X.                                     00002880
             03 MLCOMMENTC   PIC X.                                     00002890
             03 MLCOMMENTP   PIC X.                                     00002900
             03 MLCOMMENTH   PIC X.                                     00002910
             03 MLCOMMENTV   PIC X.                                     00002920
             03 MLCOMMENTO   PIC X(20).                                 00002930
             03 FILLER       PIC X(2).                                  00002940
             03 MTOPMAJA     PIC X.                                     00002950
             03 MTOPMAJC     PIC X.                                     00002960
             03 MTOPMAJP     PIC X.                                     00002970
             03 MTOPMAJH     PIC X.                                     00002980
             03 MTOPMAJV     PIC X.                                     00002990
             03 MTOPMAJO     PIC X.                                     00003000
             03 FILLER       PIC X(2).                                  00003010
             03 MDUPTOPEA    PIC X.                                     00003020
             03 MDUPTOPEC    PIC X.                                     00003030
             03 MDUPTOPEP    PIC X.                                     00003040
             03 MDUPTOPEH    PIC X.                                     00003050
             03 MDUPTOPEV    PIC X.                                     00003060
             03 MDUPTOPEO    PIC X.                                     00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MLIBERRA  PIC X.                                          00003090
           02 MLIBERRC  PIC X.                                          00003100
           02 MLIBERRP  PIC X.                                          00003110
           02 MLIBERRH  PIC X.                                          00003120
           02 MLIBERRV  PIC X.                                          00003130
           02 MLIBERRO  PIC X(79).                                      00003140
           02 FILLER    PIC X(2).                                       00003150
           02 MCODTRAA  PIC X.                                          00003160
           02 MCODTRAC  PIC X.                                          00003170
           02 MCODTRAP  PIC X.                                          00003180
           02 MCODTRAH  PIC X.                                          00003190
           02 MCODTRAV  PIC X.                                          00003200
           02 MCODTRAO  PIC X(4).                                       00003210
           02 FILLER    PIC X(2).                                       00003220
           02 MCICSA    PIC X.                                          00003230
           02 MCICSC    PIC X.                                          00003240
           02 MCICSP    PIC X.                                          00003250
           02 MCICSH    PIC X.                                          00003260
           02 MCICSV    PIC X.                                          00003270
           02 MCICSO    PIC X(5).                                       00003280
           02 FILLER    PIC X(2).                                       00003290
           02 MNETNAMA  PIC X.                                          00003300
           02 MNETNAMC  PIC X.                                          00003310
           02 MNETNAMP  PIC X.                                          00003320
           02 MNETNAMH  PIC X.                                          00003330
           02 MNETNAMV  PIC X.                                          00003340
           02 MNETNAMO  PIC X(8).                                       00003350
           02 FILLER    PIC X(2).                                       00003360
           02 MSCREENA  PIC X.                                          00003370
           02 MSCREENC  PIC X.                                          00003380
           02 MSCREENP  PIC X.                                          00003390
           02 MSCREENH  PIC X.                                          00003400
           02 MSCREENV  PIC X.                                          00003410
           02 MSCREENO  PIC X(4).                                       00003420
                                                                                
