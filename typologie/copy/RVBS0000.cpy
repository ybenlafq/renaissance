      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVBS0000                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVBS0000                 00000060
      *   CLE UNIQUE : CTYPENT A NENTITE                                00000070
      *---------------------------------------------------------        00000080
      *                                                                 00000090
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBS0000.                                                    00000100
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBS0000.                                                            
      *}                                                                        
           10 BS00-CTYPENT         PIC X(2).                            00000110
           10 BS00-NENTITE         PIC X(7).                            00000120
           10 BS00-WREMAUT         PIC X(1).                            00000130
           10 BS00-CPROFASS        PIC X(5).                            00000140
           10 BS00-CCALCUL         PIC X(5).                            00000150
           10 BS00-DSYST           PIC S9(13) COMP-3.                   00000160
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00000170
      *---------------------------------------------------------        00000180
      *   LISTE DES FLAGS DE LA TABLE RVBS0000                          00000190
      *---------------------------------------------------------        00000200
      *                                                                 00000210
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBS0000-FLAGS.                                              00000220
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBS0000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS00-CTYPENT-F       PIC S9(4) COMP.                      00000230
      *--                                                                       
           10 BS00-CTYPENT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS00-NENTITE-F       PIC S9(4) COMP.                      00000240
      *--                                                                       
           10 BS00-NENTITE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS00-WREMAUT-F       PIC S9(4) COMP.                      00000250
      *--                                                                       
           10 BS00-WREMAUT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS00-CPROFASS-F      PIC S9(4) COMP.                      00000260
      *--                                                                       
           10 BS00-CPROFASS-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS00-CCALCUL-F       PIC S9(4) COMP.                      00000270
      *--                                                                       
           10 BS00-CCALCUL-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS00-DSYST-F         PIC S9(4) COMP.                      00000280
      *                                                                         
      *--                                                                       
           10 BS00-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
