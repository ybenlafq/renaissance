      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGG6001                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGG6001                         
      **********************************************************                
       01  RVGG6001.                                                            
           02  GG60-NCODIC                                                      
               PIC X(0007).                                                     
           02  GG60-DREC                                                        
               PIC X(0008).                                                     
           02  GG60-NREC                                                        
               PIC X(0007).                                                     
           02  GG60-NCDE                                                        
               PIC X(0007).                                                     
           02  GG60-PRARECYCL                                                   
               PIC S9(7)V9(0006) COMP-3.                                        
           02  GG60-DDEMANDE                                                    
               PIC X(0008).                                                     
           02  GG60-WTRAITE                                                     
               PIC X(0001).                                                     
           02  GG60-PRIST                                                       
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GG60-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GG60-PRAHF                                                       
               PIC S9(7)V9(0006) COMP-3.                                        
           02  GG60-PRASF                                                       
               PIC S9(7)V9(0006) COMP-3.                                        
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGG6001                                  
      **********************************************************                
       01  RVGG6001-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG60-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG60-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG60-DREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG60-DREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG60-NREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG60-NREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG60-NCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG60-NCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG60-PRARECYCL-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG60-PRARECYCL-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG60-DDEMANDE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG60-DDEMANDE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG60-WTRAITE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG60-WTRAITE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG60-PRIST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG60-PRIST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG60-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG60-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG60-PRAHF-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG60-PRAHF-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG60-PRASF-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GG60-PRASF-F                                                     
               PIC S9(4) COMP-5.                                                
EMOD                                                                            
      *}                                                                        
