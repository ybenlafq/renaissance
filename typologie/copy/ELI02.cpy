      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ELI02   ELI02                                              00000020
      ***************************************************************** 00000030
       01   ELI02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNSOCIETEI     PIC X(3).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNLIEUI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLLIEUI   PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPLL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCTYPLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCTYPLF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCTYPLI   PIC X.                                          00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPSOCL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MTYPSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPSOCF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MTYPSOCI  PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MACIDL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MACIDL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MACIDF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MACIDI    PIC X(4).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZPL      COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MZPL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MZPF      PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MZPI      PIC X(2).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNZPPRESTL     COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MNZPPRESTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNZPPRESTF     PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNZPPRESTI     PIC X(2).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCMPAD1L      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MLCMPAD1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLCMPAD1F      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLCMPAD1I      PIC X(32).                                 00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGRPMAGL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MCGRPMAGL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCGRPMAGF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCGRPMAGI      PIC X(2).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCMPAD2L      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MLCMPAD2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLCMPAD2F      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLCMPAD2I      PIC X(32).                                 00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSTATEXPOL    COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MCSTATEXPOL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCSTATEXPOF    PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCSTATEXPOI    PIC X(5).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCMPAD3L      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MLCMPAD3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLCMPAD3F      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLCMPAD3I      PIC X(32).                                 00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWDACEML  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MWDACEML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWDACEMF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MWDACEMI  PIC X.                                          00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTALL      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MCPOSTALL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPOSTALF      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCPOSTALI      PIC X(5).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMMUNEL     COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MLCOMMUNEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLCOMMUNEF     PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLCOMMUNEI     PIC X(32).                                 00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPFACTL      COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MTYPFACTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPFACTF      PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MTYPFACTI      PIC X(5).                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCCAL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MNSOCCAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSOCCAF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MNSOCCAI  PIC X(3).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPCONTL      COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MTYPCONTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPCONTF      PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MTYPCONTI      PIC X(5).                                  00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBTYPCONL    COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MLIBTYPCONL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLIBTYPCONF    PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MLIBTYPCONI    PIC X(20).                                 00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MORDRECAL      COMP PIC S9(4).                            00000940
      *--                                                                       
           02 MORDRECAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MORDRECAF      PIC X.                                     00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MORDRECAI      PIC X(3).                                  00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTESCPTEL      COMP PIC S9(4).                            00000980
      *--                                                                       
           02 MTESCPTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTESCPTEF      PIC X.                                     00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MTESCPTEI      PIC X(5).                                  00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCONDREGLL     COMP PIC S9(4).                            00001020
      *--                                                                       
           02 MCONDREGLL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCONDREGLF     PIC X.                                     00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MCONDREGLI     PIC X(20).                                 00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MACID1L   COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MACID1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MACID1F   PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MACID1I   PIC X(4).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MACID2L   COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MACID2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MACID2F   PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MACID2I   PIC X(4).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MACID3L   COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MACID3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MACID3F   PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MACID3I   PIC X(4).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCFACL      COMP PIC S9(4).                            00001180
      *--                                                                       
           02 MNSOCFACL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCFACF      PIC X.                                     00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MNSOCFACI      PIC X(3).                                  00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUFACL     COMP PIC S9(4).                            00001220
      *--                                                                       
           02 MNLIEUFACL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNLIEUFACF     PIC X.                                     00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MNLIEUFACI     PIC X(3).                                  00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MACID4L   COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MACID4L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MACID4F   PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MACID4I   PIC X(4).                                       00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MACID5L   COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MACID5L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MACID5F   PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MACID5I   PIC X(4).                                       00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MACID6L   COMP PIC S9(4).                                 00001340
      *--                                                                       
           02 MACID6L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MACID6F   PIC X.                                          00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MACID6I   PIC X(4).                                       00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCCOMPTL    COMP PIC S9(4).                            00001380
      *--                                                                       
           02 MNSOCCOMPTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNSOCCOMPTF    PIC X.                                     00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MNSOCCOMPTI    PIC X(3).                                  00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUCOMPTL   COMP PIC S9(4).                            00001420
      *--                                                                       
           02 MNLIEUCOMPTL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNLIEUCOMPTF   PIC X.                                     00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MNLIEUCOMPTI   PIC X(3).                                  00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSECTANAL      COMP PIC S9(4).                            00001460
      *--                                                                       
           02 MSECTANAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSECTANAF      PIC X.                                     00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MSECTANAI      PIC X(6).                                  00001490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCUMCAL   COMP PIC S9(4).                                 00001500
      *--                                                                       
           02 MCUMCAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCUMCAF   PIC X.                                          00001510
           02 FILLER    PIC X(4).                                       00001520
           02 MCUMCAI   PIC X.                                          00001530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCVALOL     COMP PIC S9(4).                            00001540
      *--                                                                       
           02 MNSOCVALOL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCVALOF     PIC X.                                     00001550
           02 FILLER    PIC X(4).                                       00001560
           02 MNSOCVALOI     PIC X(3).                                  00001570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUVALOL    COMP PIC S9(4).                            00001580
      *--                                                                       
           02 MNLIEUVALOL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNLIEUVALOF    PIC X.                                     00001590
           02 FILLER    PIC X(4).                                       00001600
           02 MNLIEUVALOI    PIC X(3).                                  00001610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWRETROL  COMP PIC S9(4).                                 00001620
      *--                                                                       
           02 MWRETROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWRETROF  PIC X.                                          00001630
           02 FILLER    PIC X(4).                                       00001640
           02 MWRETROI  PIC X.                                          00001650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTYPVALOL     COMP PIC S9(4).                            00001660
      *--                                                                       
           02 MWTYPVALOL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MWTYPVALOF     PIC X.                                     00001670
           02 FILLER    PIC X(4).                                       00001680
           02 MWTYPVALOI     PIC X.                                     00001690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFRAISL  COMP PIC S9(4).                                 00001700
      *--                                                                       
           02 MCFRAISL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCFRAISF  PIC X.                                          00001710
           02 FILLER    PIC X(4).                                       00001720
           02 MCFRAISI  PIC X(6).                                       00001730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIERSL   COMP PIC S9(4).                                 00001740
      *--                                                                       
           02 MTIERSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTIERSF   PIC X.                                          00001750
           02 FILLER    PIC X(4).                                       00001760
           02 MTIERSI   PIC X(6).                                       00001770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001780
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001790
           02 FILLER    PIC X(4).                                       00001800
           02 MZONCMDI  PIC X(15).                                      00001810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001820
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001830
           02 FILLER    PIC X(4).                                       00001840
           02 MLIBERRI  PIC X(58).                                      00001850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001860
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001870
           02 FILLER    PIC X(4).                                       00001880
           02 MCODTRAI  PIC X(4).                                       00001890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001900
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001910
           02 FILLER    PIC X(4).                                       00001920
           02 MCICSI    PIC X(5).                                       00001930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001940
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001950
           02 FILLER    PIC X(4).                                       00001960
           02 MNETNAMI  PIC X(8).                                       00001970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001980
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001990
           02 FILLER    PIC X(4).                                       00002000
           02 MSCREENI  PIC X(5).                                       00002010
      ***************************************************************** 00002020
      * SDF: ELI02   ELI02                                              00002030
      ***************************************************************** 00002040
       01   ELI02O REDEFINES ELI02I.                                    00002050
           02 FILLER    PIC X(12).                                      00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MDATJOUA  PIC X.                                          00002080
           02 MDATJOUC  PIC X.                                          00002090
           02 MDATJOUP  PIC X.                                          00002100
           02 MDATJOUH  PIC X.                                          00002110
           02 MDATJOUV  PIC X.                                          00002120
           02 MDATJOUO  PIC X(10).                                      00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MTIMJOUA  PIC X.                                          00002150
           02 MTIMJOUC  PIC X.                                          00002160
           02 MTIMJOUP  PIC X.                                          00002170
           02 MTIMJOUH  PIC X.                                          00002180
           02 MTIMJOUV  PIC X.                                          00002190
           02 MTIMJOUO  PIC X(5).                                       00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MNSOCIETEA     PIC X.                                     00002220
           02 MNSOCIETEC     PIC X.                                     00002230
           02 MNSOCIETEP     PIC X.                                     00002240
           02 MNSOCIETEH     PIC X.                                     00002250
           02 MNSOCIETEV     PIC X.                                     00002260
           02 MNSOCIETEO     PIC X(3).                                  00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MNLIEUA   PIC X.                                          00002290
           02 MNLIEUC   PIC X.                                          00002300
           02 MNLIEUP   PIC X.                                          00002310
           02 MNLIEUH   PIC X.                                          00002320
           02 MNLIEUV   PIC X.                                          00002330
           02 MNLIEUO   PIC X(3).                                       00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MLLIEUA   PIC X.                                          00002360
           02 MLLIEUC   PIC X.                                          00002370
           02 MLLIEUP   PIC X.                                          00002380
           02 MLLIEUH   PIC X.                                          00002390
           02 MLLIEUV   PIC X.                                          00002400
           02 MLLIEUO   PIC X(20).                                      00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MCTYPLA   PIC X.                                          00002430
           02 MCTYPLC   PIC X.                                          00002440
           02 MCTYPLP   PIC X.                                          00002450
           02 MCTYPLH   PIC X.                                          00002460
           02 MCTYPLV   PIC X.                                          00002470
           02 MCTYPLO   PIC X.                                          00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MTYPSOCA  PIC X.                                          00002500
           02 MTYPSOCC  PIC X.                                          00002510
           02 MTYPSOCP  PIC X.                                          00002520
           02 MTYPSOCH  PIC X.                                          00002530
           02 MTYPSOCV  PIC X.                                          00002540
           02 MTYPSOCO  PIC X(3).                                       00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MACIDA    PIC X.                                          00002570
           02 MACIDC    PIC X.                                          00002580
           02 MACIDP    PIC X.                                          00002590
           02 MACIDH    PIC X.                                          00002600
           02 MACIDV    PIC X.                                          00002610
           02 MACIDO    PIC X(4).                                       00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MZPA      PIC X.                                          00002640
           02 MZPC      PIC X.                                          00002650
           02 MZPP      PIC X.                                          00002660
           02 MZPH      PIC X.                                          00002670
           02 MZPV      PIC X.                                          00002680
           02 MZPO      PIC X(2).                                       00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MNZPPRESTA     PIC X.                                     00002710
           02 MNZPPRESTC     PIC X.                                     00002720
           02 MNZPPRESTP     PIC X.                                     00002730
           02 MNZPPRESTH     PIC X.                                     00002740
           02 MNZPPRESTV     PIC X.                                     00002750
           02 MNZPPRESTO     PIC X(2).                                  00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MLCMPAD1A      PIC X.                                     00002780
           02 MLCMPAD1C PIC X.                                          00002790
           02 MLCMPAD1P PIC X.                                          00002800
           02 MLCMPAD1H PIC X.                                          00002810
           02 MLCMPAD1V PIC X.                                          00002820
           02 MLCMPAD1O      PIC X(32).                                 00002830
           02 FILLER    PIC X(2).                                       00002840
           02 MCGRPMAGA      PIC X.                                     00002850
           02 MCGRPMAGC PIC X.                                          00002860
           02 MCGRPMAGP PIC X.                                          00002870
           02 MCGRPMAGH PIC X.                                          00002880
           02 MCGRPMAGV PIC X.                                          00002890
           02 MCGRPMAGO      PIC X(2).                                  00002900
           02 FILLER    PIC X(2).                                       00002910
           02 MLCMPAD2A      PIC X.                                     00002920
           02 MLCMPAD2C PIC X.                                          00002930
           02 MLCMPAD2P PIC X.                                          00002940
           02 MLCMPAD2H PIC X.                                          00002950
           02 MLCMPAD2V PIC X.                                          00002960
           02 MLCMPAD2O      PIC X(32).                                 00002970
           02 FILLER    PIC X(2).                                       00002980
           02 MCSTATEXPOA    PIC X.                                     00002990
           02 MCSTATEXPOC    PIC X.                                     00003000
           02 MCSTATEXPOP    PIC X.                                     00003010
           02 MCSTATEXPOH    PIC X.                                     00003020
           02 MCSTATEXPOV    PIC X.                                     00003030
           02 MCSTATEXPOO    PIC X(5).                                  00003040
           02 FILLER    PIC X(2).                                       00003050
           02 MLCMPAD3A      PIC X.                                     00003060
           02 MLCMPAD3C PIC X.                                          00003070
           02 MLCMPAD3P PIC X.                                          00003080
           02 MLCMPAD3H PIC X.                                          00003090
           02 MLCMPAD3V PIC X.                                          00003100
           02 MLCMPAD3O      PIC X(32).                                 00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MWDACEMA  PIC X.                                          00003130
           02 MWDACEMC  PIC X.                                          00003140
           02 MWDACEMP  PIC X.                                          00003150
           02 MWDACEMH  PIC X.                                          00003160
           02 MWDACEMV  PIC X.                                          00003170
           02 MWDACEMO  PIC X.                                          00003180
           02 FILLER    PIC X(2).                                       00003190
           02 MCPOSTALA      PIC X.                                     00003200
           02 MCPOSTALC PIC X.                                          00003210
           02 MCPOSTALP PIC X.                                          00003220
           02 MCPOSTALH PIC X.                                          00003230
           02 MCPOSTALV PIC X.                                          00003240
           02 MCPOSTALO      PIC X(5).                                  00003250
           02 FILLER    PIC X(2).                                       00003260
           02 MLCOMMUNEA     PIC X.                                     00003270
           02 MLCOMMUNEC     PIC X.                                     00003280
           02 MLCOMMUNEP     PIC X.                                     00003290
           02 MLCOMMUNEH     PIC X.                                     00003300
           02 MLCOMMUNEV     PIC X.                                     00003310
           02 MLCOMMUNEO     PIC X(32).                                 00003320
           02 FILLER    PIC X(2).                                       00003330
           02 MTYPFACTA      PIC X.                                     00003340
           02 MTYPFACTC PIC X.                                          00003350
           02 MTYPFACTP PIC X.                                          00003360
           02 MTYPFACTH PIC X.                                          00003370
           02 MTYPFACTV PIC X.                                          00003380
           02 MTYPFACTO      PIC X(5).                                  00003390
           02 FILLER    PIC X(2).                                       00003400
           02 MNSOCCAA  PIC X.                                          00003410
           02 MNSOCCAC  PIC X.                                          00003420
           02 MNSOCCAP  PIC X.                                          00003430
           02 MNSOCCAH  PIC X.                                          00003440
           02 MNSOCCAV  PIC X.                                          00003450
           02 MNSOCCAO  PIC X(3).                                       00003460
           02 FILLER    PIC X(2).                                       00003470
           02 MTYPCONTA      PIC X.                                     00003480
           02 MTYPCONTC PIC X.                                          00003490
           02 MTYPCONTP PIC X.                                          00003500
           02 MTYPCONTH PIC X.                                          00003510
           02 MTYPCONTV PIC X.                                          00003520
           02 MTYPCONTO      PIC X(5).                                  00003530
           02 FILLER    PIC X(2).                                       00003540
           02 MLIBTYPCONA    PIC X.                                     00003550
           02 MLIBTYPCONC    PIC X.                                     00003560
           02 MLIBTYPCONP    PIC X.                                     00003570
           02 MLIBTYPCONH    PIC X.                                     00003580
           02 MLIBTYPCONV    PIC X.                                     00003590
           02 MLIBTYPCONO    PIC X(20).                                 00003600
           02 FILLER    PIC X(2).                                       00003610
           02 MORDRECAA      PIC X.                                     00003620
           02 MORDRECAC PIC X.                                          00003630
           02 MORDRECAP PIC X.                                          00003640
           02 MORDRECAH PIC X.                                          00003650
           02 MORDRECAV PIC X.                                          00003660
           02 MORDRECAO      PIC X(3).                                  00003670
           02 FILLER    PIC X(2).                                       00003680
           02 MTESCPTEA      PIC X.                                     00003690
           02 MTESCPTEC PIC X.                                          00003700
           02 MTESCPTEP PIC X.                                          00003710
           02 MTESCPTEH PIC X.                                          00003720
           02 MTESCPTEV PIC X.                                          00003730
           02 MTESCPTEO      PIC X(5).                                  00003740
           02 FILLER    PIC X(2).                                       00003750
           02 MCONDREGLA     PIC X.                                     00003760
           02 MCONDREGLC     PIC X.                                     00003770
           02 MCONDREGLP     PIC X.                                     00003780
           02 MCONDREGLH     PIC X.                                     00003790
           02 MCONDREGLV     PIC X.                                     00003800
           02 MCONDREGLO     PIC X(20).                                 00003810
           02 FILLER    PIC X(2).                                       00003820
           02 MACID1A   PIC X.                                          00003830
           02 MACID1C   PIC X.                                          00003840
           02 MACID1P   PIC X.                                          00003850
           02 MACID1H   PIC X.                                          00003860
           02 MACID1V   PIC X.                                          00003870
           02 MACID1O   PIC X(4).                                       00003880
           02 FILLER    PIC X(2).                                       00003890
           02 MACID2A   PIC X.                                          00003900
           02 MACID2C   PIC X.                                          00003910
           02 MACID2P   PIC X.                                          00003920
           02 MACID2H   PIC X.                                          00003930
           02 MACID2V   PIC X.                                          00003940
           02 MACID2O   PIC X(4).                                       00003950
           02 FILLER    PIC X(2).                                       00003960
           02 MACID3A   PIC X.                                          00003970
           02 MACID3C   PIC X.                                          00003980
           02 MACID3P   PIC X.                                          00003990
           02 MACID3H   PIC X.                                          00004000
           02 MACID3V   PIC X.                                          00004010
           02 MACID3O   PIC X(4).                                       00004020
           02 FILLER    PIC X(2).                                       00004030
           02 MNSOCFACA      PIC X.                                     00004040
           02 MNSOCFACC PIC X.                                          00004050
           02 MNSOCFACP PIC X.                                          00004060
           02 MNSOCFACH PIC X.                                          00004070
           02 MNSOCFACV PIC X.                                          00004080
           02 MNSOCFACO      PIC X(3).                                  00004090
           02 FILLER    PIC X(2).                                       00004100
           02 MNLIEUFACA     PIC X.                                     00004110
           02 MNLIEUFACC     PIC X.                                     00004120
           02 MNLIEUFACP     PIC X.                                     00004130
           02 MNLIEUFACH     PIC X.                                     00004140
           02 MNLIEUFACV     PIC X.                                     00004150
           02 MNLIEUFACO     PIC X(3).                                  00004160
           02 FILLER    PIC X(2).                                       00004170
           02 MACID4A   PIC X.                                          00004180
           02 MACID4C   PIC X.                                          00004190
           02 MACID4P   PIC X.                                          00004200
           02 MACID4H   PIC X.                                          00004210
           02 MACID4V   PIC X.                                          00004220
           02 MACID4O   PIC X(4).                                       00004230
           02 FILLER    PIC X(2).                                       00004240
           02 MACID5A   PIC X.                                          00004250
           02 MACID5C   PIC X.                                          00004260
           02 MACID5P   PIC X.                                          00004270
           02 MACID5H   PIC X.                                          00004280
           02 MACID5V   PIC X.                                          00004290
           02 MACID5O   PIC X(4).                                       00004300
           02 FILLER    PIC X(2).                                       00004310
           02 MACID6A   PIC X.                                          00004320
           02 MACID6C   PIC X.                                          00004330
           02 MACID6P   PIC X.                                          00004340
           02 MACID6H   PIC X.                                          00004350
           02 MACID6V   PIC X.                                          00004360
           02 MACID6O   PIC X(4).                                       00004370
           02 FILLER    PIC X(2).                                       00004380
           02 MNSOCCOMPTA    PIC X.                                     00004390
           02 MNSOCCOMPTC    PIC X.                                     00004400
           02 MNSOCCOMPTP    PIC X.                                     00004410
           02 MNSOCCOMPTH    PIC X.                                     00004420
           02 MNSOCCOMPTV    PIC X.                                     00004430
           02 MNSOCCOMPTO    PIC X(3).                                  00004440
           02 FILLER    PIC X(2).                                       00004450
           02 MNLIEUCOMPTA   PIC X.                                     00004460
           02 MNLIEUCOMPTC   PIC X.                                     00004470
           02 MNLIEUCOMPTP   PIC X.                                     00004480
           02 MNLIEUCOMPTH   PIC X.                                     00004490
           02 MNLIEUCOMPTV   PIC X.                                     00004500
           02 MNLIEUCOMPTO   PIC X(3).                                  00004510
           02 FILLER    PIC X(2).                                       00004520
           02 MSECTANAA      PIC X.                                     00004530
           02 MSECTANAC PIC X.                                          00004540
           02 MSECTANAP PIC X.                                          00004550
           02 MSECTANAH PIC X.                                          00004560
           02 MSECTANAV PIC X.                                          00004570
           02 MSECTANAO      PIC X(6).                                  00004580
           02 FILLER    PIC X(2).                                       00004590
           02 MCUMCAA   PIC X.                                          00004600
           02 MCUMCAC   PIC X.                                          00004610
           02 MCUMCAP   PIC X.                                          00004620
           02 MCUMCAH   PIC X.                                          00004630
           02 MCUMCAV   PIC X.                                          00004640
           02 MCUMCAO   PIC X.                                          00004650
           02 FILLER    PIC X(2).                                       00004660
           02 MNSOCVALOA     PIC X.                                     00004670
           02 MNSOCVALOC     PIC X.                                     00004680
           02 MNSOCVALOP     PIC X.                                     00004690
           02 MNSOCVALOH     PIC X.                                     00004700
           02 MNSOCVALOV     PIC X.                                     00004710
           02 MNSOCVALOO     PIC X(3).                                  00004720
           02 FILLER    PIC X(2).                                       00004730
           02 MNLIEUVALOA    PIC X.                                     00004740
           02 MNLIEUVALOC    PIC X.                                     00004750
           02 MNLIEUVALOP    PIC X.                                     00004760
           02 MNLIEUVALOH    PIC X.                                     00004770
           02 MNLIEUVALOV    PIC X.                                     00004780
           02 MNLIEUVALOO    PIC X(3).                                  00004790
           02 FILLER    PIC X(2).                                       00004800
           02 MWRETROA  PIC X.                                          00004810
           02 MWRETROC  PIC X.                                          00004820
           02 MWRETROP  PIC X.                                          00004830
           02 MWRETROH  PIC X.                                          00004840
           02 MWRETROV  PIC X.                                          00004850
           02 MWRETROO  PIC X.                                          00004860
           02 FILLER    PIC X(2).                                       00004870
           02 MWTYPVALOA     PIC X.                                     00004880
           02 MWTYPVALOC     PIC X.                                     00004890
           02 MWTYPVALOP     PIC X.                                     00004900
           02 MWTYPVALOH     PIC X.                                     00004910
           02 MWTYPVALOV     PIC X.                                     00004920
           02 MWTYPVALOO     PIC X.                                     00004930
           02 FILLER    PIC X(2).                                       00004940
           02 MCFRAISA  PIC X.                                          00004950
           02 MCFRAISC  PIC X.                                          00004960
           02 MCFRAISP  PIC X.                                          00004970
           02 MCFRAISH  PIC X.                                          00004980
           02 MCFRAISV  PIC X.                                          00004990
           02 MCFRAISO  PIC X(6).                                       00005000
           02 FILLER    PIC X(2).                                       00005010
           02 MTIERSA   PIC X.                                          00005020
           02 MTIERSC   PIC X.                                          00005030
           02 MTIERSP   PIC X.                                          00005040
           02 MTIERSH   PIC X.                                          00005050
           02 MTIERSV   PIC X.                                          00005060
           02 MTIERSO   PIC X(6).                                       00005070
           02 FILLER    PIC X(2).                                       00005080
           02 MZONCMDA  PIC X.                                          00005090
           02 MZONCMDC  PIC X.                                          00005100
           02 MZONCMDP  PIC X.                                          00005110
           02 MZONCMDH  PIC X.                                          00005120
           02 MZONCMDV  PIC X.                                          00005130
           02 MZONCMDO  PIC X(15).                                      00005140
           02 FILLER    PIC X(2).                                       00005150
           02 MLIBERRA  PIC X.                                          00005160
           02 MLIBERRC  PIC X.                                          00005170
           02 MLIBERRP  PIC X.                                          00005180
           02 MLIBERRH  PIC X.                                          00005190
           02 MLIBERRV  PIC X.                                          00005200
           02 MLIBERRO  PIC X(58).                                      00005210
           02 FILLER    PIC X(2).                                       00005220
           02 MCODTRAA  PIC X.                                          00005230
           02 MCODTRAC  PIC X.                                          00005240
           02 MCODTRAP  PIC X.                                          00005250
           02 MCODTRAH  PIC X.                                          00005260
           02 MCODTRAV  PIC X.                                          00005270
           02 MCODTRAO  PIC X(4).                                       00005280
           02 FILLER    PIC X(2).                                       00005290
           02 MCICSA    PIC X.                                          00005300
           02 MCICSC    PIC X.                                          00005310
           02 MCICSP    PIC X.                                          00005320
           02 MCICSH    PIC X.                                          00005330
           02 MCICSV    PIC X.                                          00005340
           02 MCICSO    PIC X(5).                                       00005350
           02 FILLER    PIC X(2).                                       00005360
           02 MNETNAMA  PIC X.                                          00005370
           02 MNETNAMC  PIC X.                                          00005380
           02 MNETNAMP  PIC X.                                          00005390
           02 MNETNAMH  PIC X.                                          00005400
           02 MNETNAMV  PIC X.                                          00005410
           02 MNETNAMO  PIC X(8).                                       00005420
           02 FILLER    PIC X(2).                                       00005430
           02 MSCREENA  PIC X.                                          00005440
           02 MSCREENC  PIC X.                                          00005450
           02 MSCREENP  PIC X.                                          00005460
           02 MSCREENH  PIC X.                                          00005470
           02 MSCREENV  PIC X.                                          00005480
           02 MSCREENO  PIC X(5).                                       00005490
                                                                                
