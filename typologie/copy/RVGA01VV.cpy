      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GVRX2 CONTROLE DE SAISIE DES RX2       *        
      *----------------------------------------------------------------*        
       01  RVGA01VV.                                                            
           05  GVRX2-CTABLEG2    PIC X(15).                                     
           05  GVRX2-CTABLEG2-REDEF REDEFINES GVRX2-CTABLEG2.                   
               10  GVRX2-CODERX2         PIC X(05).                             
           05  GVRX2-WTABLEG     PIC X(80).                                     
           05  GVRX2-WTABLEG-REDEF  REDEFINES GVRX2-WTABLEG.                    
               10  GVRX2-WACTIF          PIC X(01).                             
               10  GVRX2-LIBRX2          PIC X(20).                             
               10  GVRX2-CTR1RX2         PIC X(06).                             
               10  GVRX2-CTR1RX2-N      REDEFINES GVRX2-CTR1RX2                 
                                         PIC 9(06).                             
               10  GVRX2-CTR2RX2         PIC X(06).                             
               10  GVRX2-CTR2RX2-N      REDEFINES GVRX2-CTR2RX2                 
                                         PIC 9(06).                             
               10  GVRX2-FILLER          PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01VV-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GVRX2-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GVRX2-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GVRX2-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GVRX2-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
