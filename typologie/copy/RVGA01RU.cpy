      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TRACP TRANCHE ACOMPTE EN POURCENTAGE   *        
      *----------------------------------------------------------------*        
       01  RVGA01RU.                                                            
           05  TRACP-CTABLEG2    PIC X(15).                                     
           05  TRACP-CTABLEG2-REDEF REDEFINES TRACP-CTABLEG2.                   
               10  TRACP-NSEQ            PIC X(02).                             
           05  TRACP-WTABLEG     PIC X(80).                                     
           05  TRACP-WTABLEG-REDEF  REDEFINES TRACP-WTABLEG.                    
               10  TRACP-WACTIF          PIC X(01).                             
               10  TRACP-CRITERE         PIC X(02).                             
               10  TRACP-POURMIN         PIC X(03).                             
               10  TRACP-POURMAX         PIC X(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01RU-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TRACP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TRACP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TRACP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TRACP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
