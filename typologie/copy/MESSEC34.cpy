      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00010000
      *   COPY MESSAGE MQ RECEPTION DES PRODUITS DE VENTE DARTY.COM     00020000
      *   EN LD2                                                        00030000
      ***************************************************************** 00040000
      *                                                                 00050000
      *                                                                 00060000
       01  WS-MQ10.                                                     00070003
         02 WS-QUEUE.                                                   00080003
               10   MQ10-MSGID     PIC    X(24).                        00090003
               10   MQ10-CORRELID  PIC    X(24).                        00100003
         02 WS-CODRET              PIC    XX.                           00110003
         02  MESSAGE34.                                                 00120003
           03 MESSAGE34-ENTETE.                                         00130003
               05   MES34-TYPE     PIC    X(3).                         00140003
               05   MES34-NSOCMSG  PIC    X(3).                         00150003
               05   MES34-NLIEUMSG PIC    X(3).                         00160003
               05   MES34-NSOCDST  PIC    X(3).                         00170003
               05   MES34-NLIEUDST PIC    X(3).                         00171003
               05   MES34-NORD     PIC    9(8).                         00172003
               05   MES34-LPROG    PIC    X(10).                        00173003
               05   MES34-DJOUR    PIC    X(8).                         00174003
               05   MES34-WSID     PIC    X(10).                        00175003
               05   MES34-USER     PIC    X(10).                        00176003
               05   MES34-CHRONO   PIC    9(7).                         00177003
               05   MES34-NBRMSG   PIC    9(7).                         00178003
               05   MES34-FILLER   PIC    X(30).                        00179003
      *--DATA MESSAGE = 5403 CAR                                        00180000
           03 MESSAGE34-DATA.                                           00190003
               05  MES34-DATEJOUR      PIC    X(8).                     00200003
               05  MES34-NBVENTES      PIC    9(3).                     00201003
      *--LISTE DES VENTES POUR LESQUELLES UN CODIC EST DISPO            00210000
               05  MES34-TAB.                                           00211006
               10  MES34-VENTE-TAB  OCCURS 200.                         00220006
                   15 MES34-NSOCIETE   PIC    X(3).                     00230006
                   15 MES34-NLIEU      PIC    X(3).                     00240006
                   15 MES34-NVENTE     PIC    X(7).                     00250006
                   15 MES34-NSOCLIVR   PIC    X(3).                     00260006
                   15 MES34-NDEPOT     PIC    X(3).                     00270006
                   15 MES34-DCREATION  PIC    X(8).                     00280006
                                                                                
