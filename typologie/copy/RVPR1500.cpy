      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVPR1500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVPR1500                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPR1500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPR1500.                                                            
      *}                                                                        
           02  PR15-CPRESTATION                                                 
               PIC X(0005).                                                     
           02  PR15-NZONPRIX                                                    
               PIC X(0002).                                                     
           02  PR15-NCODIC                                                      
               PIC X(0007).                                                     
           02  PR15-MTPRMP                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  PR15-DEFFET                                                      
               PIC X(0008).                                                     
           02  PR15-DFEFFET                                                     
               PIC X(0008).                                                     
           02  PR15-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVPR1500                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPR1500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPR1500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR15-CPRESTATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR15-CPRESTATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR15-NZONPRIX-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR15-NZONPRIX-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR15-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR15-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR15-MTPRMP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR15-MTPRMP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR15-DEFFET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR15-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR15-DFEFFET-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR15-DFEFFET-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR15-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR15-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
