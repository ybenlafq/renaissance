      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESE00   ESE00                                              00000020
      ***************************************************************** 00000030
       01   ETH12I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFONCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFONCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCFONCI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPCOL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCOPCOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOPCOF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCOPCOI   PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLOPCOL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLOPCOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLOPCOF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLOPCOI   PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNCODICI  PIC X(7).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLREFFOURNI    PIC X(20).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCFAMI    PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLFAMI    PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MHISTOL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MHISTOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MHISTOF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MHISTOI   PIC X.                                          00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCMARQI   PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLMARQI   PIC X(20).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEL    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MDATEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDATEF    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MDATEI    PIC X(10).                                      00000650
           02 MSD OCCURS   10 TIMES .                                   00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSL     COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MSL COMP-5 PIC S9(4).                                           
      *}                                                                        
             03 MSF     PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MSI     PIC X.                                          00000700
           02 MLCOPCOD OCCURS   10 TIMES .                              00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOPCOL     COMP PIC S9(4).                            00000720
      *--                                                                       
             03 MLCOPCOL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLCOPCOF     PIC X.                                     00000730
             03 FILLER  PIC X(4).                                       00000740
             03 MLCOPCOI     PIC X(3).                                  00000750
           02 MDEFFETD OCCURS   10 TIMES .                              00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00000770
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MDEFFETI     PIC X(10).                                 00000800
           02 MPCFD OCCURS   10 TIMES .                                 00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPCFL   COMP PIC S9(4).                                 00000820
      *--                                                                       
             03 MPCFL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MPCFF   PIC X.                                          00000830
             03 FILLER  PIC X(4).                                       00000840
             03 MPCFI   PIC X(14).                                      00000850
           02 MFORCED OCCURS   10 TIMES .                               00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFORCEL      COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MFORCEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MFORCEF      PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MFORCEI      PIC X.                                     00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MZONCMDI  PIC X(15).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLIBERRI  PIC X(58).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCODTRAI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCICSI    PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MNETNAMI  PIC X(8).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MSCREENI  PIC X(4).                                       00001140
      ***************************************************************** 00001150
      * SDF: ESE00   ESE00                                              00001160
      ***************************************************************** 00001170
       01   ETH12O REDEFINES ETH12I.                                    00001180
           02 FILLER    PIC X(12).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTIMJOUA  PIC X.                                          00001280
           02 MTIMJOUC  PIC X.                                          00001290
           02 MTIMJOUP  PIC X.                                          00001300
           02 MTIMJOUH  PIC X.                                          00001310
           02 MTIMJOUV  PIC X.                                          00001320
           02 MTIMJOUO  PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MPAGEA    PIC X.                                          00001350
           02 MPAGEC    PIC X.                                          00001360
           02 MPAGEP    PIC X.                                          00001370
           02 MPAGEH    PIC X.                                          00001380
           02 MPAGEV    PIC X.                                          00001390
           02 MPAGEO    PIC X(3).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MPAGEMAXA      PIC X.                                     00001420
           02 MPAGEMAXC PIC X.                                          00001430
           02 MPAGEMAXP PIC X.                                          00001440
           02 MPAGEMAXH PIC X.                                          00001450
           02 MPAGEMAXV PIC X.                                          00001460
           02 MPAGEMAXO      PIC X(3).                                  00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MCFONCA   PIC X.                                          00001490
           02 MCFONCC   PIC X.                                          00001500
           02 MCFONCP   PIC X.                                          00001510
           02 MCFONCH   PIC X.                                          00001520
           02 MCFONCV   PIC X.                                          00001530
           02 MCFONCO   PIC X(3).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MCOPCOA   PIC X.                                          00001560
           02 MCOPCOC   PIC X.                                          00001570
           02 MCOPCOP   PIC X.                                          00001580
           02 MCOPCOH   PIC X.                                          00001590
           02 MCOPCOV   PIC X.                                          00001600
           02 MCOPCOO   PIC X(3).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MLOPCOA   PIC X.                                          00001630
           02 MLOPCOC   PIC X.                                          00001640
           02 MLOPCOP   PIC X.                                          00001650
           02 MLOPCOH   PIC X.                                          00001660
           02 MLOPCOV   PIC X.                                          00001670
           02 MLOPCOO   PIC X(20).                                      00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MNCODICA  PIC X.                                          00001700
           02 MNCODICC  PIC X.                                          00001710
           02 MNCODICP  PIC X.                                          00001720
           02 MNCODICH  PIC X.                                          00001730
           02 MNCODICV  PIC X.                                          00001740
           02 MNCODICO  PIC X(7).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MLREFFOURNA    PIC X.                                     00001770
           02 MLREFFOURNC    PIC X.                                     00001780
           02 MLREFFOURNP    PIC X.                                     00001790
           02 MLREFFOURNH    PIC X.                                     00001800
           02 MLREFFOURNV    PIC X.                                     00001810
           02 MLREFFOURNO    PIC X(20).                                 00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MCFAMA    PIC X.                                          00001840
           02 MCFAMC    PIC X.                                          00001850
           02 MCFAMP    PIC X.                                          00001860
           02 MCFAMH    PIC X.                                          00001870
           02 MCFAMV    PIC X.                                          00001880
           02 MCFAMO    PIC X(5).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MLFAMA    PIC X.                                          00001910
           02 MLFAMC    PIC X.                                          00001920
           02 MLFAMP    PIC X.                                          00001930
           02 MLFAMH    PIC X.                                          00001940
           02 MLFAMV    PIC X.                                          00001950
           02 MLFAMO    PIC X(20).                                      00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MHISTOA   PIC X.                                          00001980
           02 MHISTOC   PIC X.                                          00001990
           02 MHISTOP   PIC X.                                          00002000
           02 MHISTOH   PIC X.                                          00002010
           02 MHISTOV   PIC X.                                          00002020
           02 MHISTOO   PIC X.                                          00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MCMARQA   PIC X.                                          00002050
           02 MCMARQC   PIC X.                                          00002060
           02 MCMARQP   PIC X.                                          00002070
           02 MCMARQH   PIC X.                                          00002080
           02 MCMARQV   PIC X.                                          00002090
           02 MCMARQO   PIC X(5).                                       00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MLMARQA   PIC X.                                          00002120
           02 MLMARQC   PIC X.                                          00002130
           02 MLMARQP   PIC X.                                          00002140
           02 MLMARQH   PIC X.                                          00002150
           02 MLMARQV   PIC X.                                          00002160
           02 MLMARQO   PIC X(20).                                      00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MDATEA    PIC X.                                          00002190
           02 MDATEC    PIC X.                                          00002200
           02 MDATEP    PIC X.                                          00002210
           02 MDATEH    PIC X.                                          00002220
           02 MDATEV    PIC X.                                          00002230
           02 MDATEO    PIC X(10).                                      00002240
           02 DFHMS1 OCCURS   10 TIMES .                                00002250
             03 FILLER       PIC X(2).                                  00002260
             03 MSA     PIC X.                                          00002270
             03 MSC     PIC X.                                          00002280
             03 MSP     PIC X.                                          00002290
             03 MSH     PIC X.                                          00002300
             03 MSV     PIC X.                                          00002310
             03 MSO     PIC X.                                          00002320
           02 DFHMS2 OCCURS   10 TIMES .                                00002330
             03 FILLER       PIC X(2).                                  00002340
             03 MLCOPCOA     PIC X.                                     00002350
             03 MLCOPCOC     PIC X.                                     00002360
             03 MLCOPCOP     PIC X.                                     00002370
             03 MLCOPCOH     PIC X.                                     00002380
             03 MLCOPCOV     PIC X.                                     00002390
             03 MLCOPCOO     PIC X(3).                                  00002400
           02 DFHMS3 OCCURS   10 TIMES .                                00002410
             03 FILLER       PIC X(2).                                  00002420
             03 MDEFFETA     PIC X.                                     00002430
             03 MDEFFETC     PIC X.                                     00002440
             03 MDEFFETP     PIC X.                                     00002450
             03 MDEFFETH     PIC X.                                     00002460
             03 MDEFFETV     PIC X.                                     00002470
             03 MDEFFETO     PIC X(10).                                 00002480
           02 DFHMS4 OCCURS   10 TIMES .                                00002490
             03 FILLER       PIC X(2).                                  00002500
             03 MPCFA   PIC X.                                          00002510
             03 MPCFC   PIC X.                                          00002520
             03 MPCFP   PIC X.                                          00002530
             03 MPCFH   PIC X.                                          00002540
             03 MPCFV   PIC X.                                          00002550
             03 MPCFO   PIC X(14).                                      00002560
           02 DFHMS5 OCCURS   10 TIMES .                                00002570
             03 FILLER       PIC X(2).                                  00002580
             03 MFORCEA      PIC X.                                     00002590
             03 MFORCEC PIC X.                                          00002600
             03 MFORCEP PIC X.                                          00002610
             03 MFORCEH PIC X.                                          00002620
             03 MFORCEV PIC X.                                          00002630
             03 MFORCEO      PIC X.                                     00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MZONCMDA  PIC X.                                          00002660
           02 MZONCMDC  PIC X.                                          00002670
           02 MZONCMDP  PIC X.                                          00002680
           02 MZONCMDH  PIC X.                                          00002690
           02 MZONCMDV  PIC X.                                          00002700
           02 MZONCMDO  PIC X(15).                                      00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MLIBERRA  PIC X.                                          00002730
           02 MLIBERRC  PIC X.                                          00002740
           02 MLIBERRP  PIC X.                                          00002750
           02 MLIBERRH  PIC X.                                          00002760
           02 MLIBERRV  PIC X.                                          00002770
           02 MLIBERRO  PIC X(58).                                      00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MCODTRAA  PIC X.                                          00002800
           02 MCODTRAC  PIC X.                                          00002810
           02 MCODTRAP  PIC X.                                          00002820
           02 MCODTRAH  PIC X.                                          00002830
           02 MCODTRAV  PIC X.                                          00002840
           02 MCODTRAO  PIC X(4).                                       00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MCICSA    PIC X.                                          00002870
           02 MCICSC    PIC X.                                          00002880
           02 MCICSP    PIC X.                                          00002890
           02 MCICSH    PIC X.                                          00002900
           02 MCICSV    PIC X.                                          00002910
           02 MCICSO    PIC X(5).                                       00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MNETNAMA  PIC X.                                          00002940
           02 MNETNAMC  PIC X.                                          00002950
           02 MNETNAMP  PIC X.                                          00002960
           02 MNETNAMH  PIC X.                                          00002970
           02 MNETNAMV  PIC X.                                          00002980
           02 MNETNAMO  PIC X(8).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MSCREENA  PIC X.                                          00003010
           02 MSCREENC  PIC X.                                          00003020
           02 MSCREENP  PIC X.                                          00003030
           02 MSCREENH  PIC X.                                          00003040
           02 MSCREENV  PIC X.                                          00003050
           02 MSCREENO  PIC X(4).                                       00003060
                                                                                
