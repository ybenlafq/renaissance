      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVRX2100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRX2100                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRX2100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRX2100.                                                            
      *}                                                                        
           02  RX21-NRELEVE                                                     
               PIC X(0007).                                                     
           02  RX21-NCONC                                                       
               PIC X(0004).                                                     
           02  RX21-NMAG                                                        
               PIC X(0003).                                                     
           02  RX21-WTYPREL                                                     
               PIC X(0001).                                                     
           02  RX21-DSUPPORT                                                    
               PIC X(0008).                                                     
           02  RX21-DRELEVE                                                     
               PIC X(0008).                                                     
           02  RX21-DHRELEVE                                                    
               PIC X(0002).                                                     
           02  RX21-D1SAIREL                                                    
               PIC X(0008).                                                     
           02  RX21-DCLOREL                                                     
               PIC X(0008).                                                     
           02  RX21-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRX2100                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRX2100-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRX2100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX21-NRELEVE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX21-NRELEVE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX21-NCONC-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX21-NCONC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX21-NMAG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX21-NMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX21-WTYPREL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX21-WTYPREL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX21-DSUPPORT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX21-DSUPPORT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX21-DRELEVE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX21-DRELEVE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX21-DHRELEVE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX21-DHRELEVE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX21-D1SAIREL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX21-D1SAIREL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX21-DCLOREL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX21-DCLOREL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX21-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX21-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
