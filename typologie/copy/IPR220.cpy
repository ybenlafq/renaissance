      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPR220 AU 13/08/1998  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,07,BI,A,                          *        
      *                           20,02,BI,A,                          *        
      *                           22,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPR220.                                                        
            05 NOMETAT-IPR220           PIC X(6) VALUE 'IPR220'.                
            05 RUPTURES-IPR220.                                                 
           10 IPR220-NSOCIETE           PIC X(03).                      007  003
           10 IPR220-NLIEU              PIC X(03).                      010  003
           10 IPR220-NVENTE             PIC X(07).                      013  007
           10 IPR220-NLIGNE             PIC X(02).                      020  002
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPR220-SEQUENCE           PIC S9(04) COMP.                022  002
      *--                                                                       
           10 IPR220-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPR220.                                                   
           10 IPR220-CACID              PIC X(08).                      024  008
           10 IPR220-CFAM               PIC X(05).                      032  005
           10 IPR220-CLD2               PIC X(05).                      037  005
           10 IPR220-CMARQ              PIC X(05).                      042  005
           10 IPR220-CMODDEL            PIC X(04).                      047  004
           10 IPR220-CTYPPREST          PIC X(05).                      051  005
           10 IPR220-CVENDEUR           PIC X(06).                      056  006
           10 IPR220-LCOMMENT           PIC X(27).                      062  027
           10 IPR220-LREFFOURN          PIC X(20).                      089  020
           10 IPR220-NCODIC             PIC X(07).                      109  007
           10 IPR220-NBSRP-ANNUL        PIC S9(03)      COMP-3.         116  002
           10 IPR220-NBSRP-CREAT        PIC S9(03)      COMP-3.         118  002
           10 IPR220-NBSRP-DIFF         PIC S9(03)      COMP-3.         120  002
           10 IPR220-PVTOTAL            PIC S9(07)V9(2) COMP-3.         122  005
           10 IPR220-DCREATION          PIC X(08).                      127  008
           10 IPR220-DMODIF             PIC X(08).                      135  008
            05 FILLER                      PIC X(370).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPR220-LONG           PIC S9(4)   COMP  VALUE +142.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPR220-LONG           PIC S9(4) COMP-5  VALUE +142.           
                                                                                
      *}                                                                        
