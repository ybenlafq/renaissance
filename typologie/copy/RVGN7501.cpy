      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGN7501.                                                    00080004
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGN7501.                                                            
      *}                                                                        
           05  GN75-NCODIC     PIC X(7).                                00090001
           05  GN75-DEFFET     PIC X(8).                                00100001
           05  GN75-TYPCOMM    PIC X(1).                                00110001
           05  GN75-PCOMMREF   PIC S9(5)V9(2) COMP-3.                   00120004
           05  GN75-PCOMMCAL   PIC S9(5)V9(2) COMP-3.                   00121004
           05  GN75-WCALCUL    PIC X(1).                                00130002
           05  GN75-LCOMMENT   PIC X(10).                               00150002
           05  GN75-DSYST      PIC S9(13) COMP-3.                       00160003
           05  GN75-USERID     PIC X(8).                                00161004
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGN7501-FLAGS.                                              00200004
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGN7501-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN75-NCODIC-F   PIC S9(4) COMP.                          00210004
      *--                                                                       
           05  GN75-NCODIC-F   PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN75-DEFFET-F   PIC S9(4) COMP.                          00211004
      *--                                                                       
           05  GN75-DEFFET-F   PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN75-TYPCOMM-F  PIC S9(4) COMP.                          00220004
      *--                                                                       
           05  GN75-TYPCOMM-F  PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN75-PCOMMREF-F PIC S9(4) COMP.                          00221004
      *--                                                                       
           05  GN75-PCOMMREF-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN75-PCOMMCAL-F PIC S9(4) COMP.                          00230004
      *--                                                                       
           05  GN75-PCOMMCAL-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN75-WCALCUL-F  PIC S9(4) COMP.                          00231004
      *--                                                                       
           05  GN75-WCALCUL-F  PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN75-LCOMMENT-F PIC S9(4) COMP.                          00240004
      *--                                                                       
           05  GN75-LCOMMENT-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN75-DSYST-F    PIC S9(4) COMP.                          00241004
      *--                                                                       
           05  GN75-DSYST-F    PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GN75-USERID-F   PIC S9(4) COMP.                          00250004
      *                                                                         
      *--                                                                       
           05  GN75-USERID-F   PIC S9(4) COMP-5.                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
