      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGA4300                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA4300                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGA4300.                                                            
           02  GA43-CINSEE                                                      
               PIC X(0005).                                                     
           02  GA43-CSECTEUR                                                    
               PIC X(0005).                                                     
           02  GA43-CZONE                                                       
               PIC X(0005).                                                     
           02  GA43-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GA43-DEFFET                                                      
               PIC X(0008).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGA4300                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGA4300-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA43-CINSEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA43-CINSEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA43-CSECTEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA43-CSECTEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA43-CZONE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA43-CZONE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA43-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA43-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA43-DEFFET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA43-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
