      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE VTREM CODES REMISE                     *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01H8.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01H8.                                                            
      *}                                                                        
           05  VTREM-CTABLEG2    PIC X(15).                                     
           05  VTREM-CTABLEG2-REDEF REDEFINES VTREM-CTABLEG2.                   
               10  VTREM-CREMVTE         PIC X(05).                             
           05  VTREM-WTABLEG     PIC X(80).                                     
           05  VTREM-WTABLEG-REDEF  REDEFINES VTREM-WTABLEG.                    
               10  VTREM-LREMVTE         PIC X(20).                             
               10  VTREM-DEFFET          PIC X(08).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  VTREM-DEFFET-N       REDEFINES VTREM-DEFFET                  
                                         PIC 9(08).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  VTREM-WTYPCOND        PIC X(03).                             
               10  VTREM-QPOURCEN        PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  VTREM-QPOURCEN-N     REDEFINES VTREM-QPOURCEN                
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  VTREM-WREMVTE         PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01H8-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01H8-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  VTREM-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  VTREM-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  VTREM-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  VTREM-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
