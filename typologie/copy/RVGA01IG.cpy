      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EGIMP PARAMETRAGE IMPRESSION           *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01IG.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01IG.                                                            
      *}                                                                        
           05  EGIMP-CTABLEG2    PIC X(15).                                     
           05  EGIMP-CTABLEG2-REDEF REDEFINES EGIMP-CTABLEG2.                   
               10  EGIMP-CETAT           PIC X(06).                             
               10  EGIMP-DEB-FIN         PIC X(01).                             
           05  EGIMP-WTABLEG     PIC X(80).                                     
           05  EGIMP-WTABLEG-REDEF  REDEFINES EGIMP-WTABLEG.                    
               10  EGIMP-NBLIGNES        PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  EGIMP-NBLIGNES-N     REDEFINES EGIMP-NBLIGNES                
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  EGIMP-COMMANDE        PIC X(56).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01IG-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01IG-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGIMP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EGIMP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGIMP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EGIMP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
