      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * TS SPECIFIQUE TABLE RTGG08                                 *            
      *       TR : GG00  GESTION DES GAMMES MENU                   *            
      *       PG : TGG08 MAJ DES PRIX ET COMMISSIONS ARTICLE       *            
      ******************************************************************        
      * DSA057 23/05/05 SUPPORT EVOLUTION FP943                                 
      *                 STOCKAGE PRIME ADAPTEE                                  
      ******************************************************************        
      *01  TS-LONG            PIC S9(4) COMP-3 VALUE 6530.                      
       01  TS-LONG            PIC S9(4) COMP-3 VALUE 6538.                      
       01  TS-DONNEES.                                                          
           05 TS-LIGNE   OCCURS 10.                                             
      * ZONES PRIX ARTICLE ET COMMISSION ANCIENS------------------              
              10  TS-NZONPRIX          PIC X(2).                                
              10  TS-PRIXAC            PIC S9(7)V99   COMP-3.                   
              10  TS-PACTRES           PIC S9(7)V99   COMP-3.                   
              10  TS-PCOMMA            PIC S9(4)V99   COMP-3.                   
              10  TS-PCOMMVOLA         PIC S9(4)V99   COMP-3.                   
              10  TS-LCOMMAC           PIC X(4).                                
              10  TS-DATEAC            PIC X(8).                                
              10  TS-DATAC             PIC X(8).                                
              10  TS-PRIXRES           PIC S9(7)V99   COMP-3.                   
              10  TS-PRIXEXCEP         PIC X.                                   
      * ZONES PRIX ARTICLE ET COMMISSION NOUVEAUX-----------------              
              10  TS-PRIXN1            PIC S9(7)V99   COMP-3.                   
              10  TS-LCOMM1            PIC X(05).                               
              10  TS-PCOMMN1           PIC S9(4)V99   COMP-3.                   
              10  TS-PCOMMADP1         PIC S9(5)V99   COMP-3.                   
              10  TS-PCOMMVOLN         PIC S9(4)V99   COMP-3.                   
              10  TS-DPRIX1            PIC X(8).                                
              10  TS-DCOMM1            PIC X(8).                                
      * ZONES PRIX ARTICLE ET COMMISSION SAISIS ------------------              
              10  TS-PRIXN2            PIC S9(7)V99   COMP-3.                   
              10  TS-LCOMM2            PIC X(10).                               
              10  TS-PCOMMN2           PIC S9(4)V99   COMP-3.                   
              10  TS-PCOMMADP2         PIC S9(5)V99   COMP-3.                   
              10  TS-DPRIX2            PIC X(8).                                
              10  TS-DCOMM2            PIC X(8).                                
      * ZONES MODIFIEES ------------------------------------------              
              10  TS-PRIX-MOD          PIC X.                                   
              10  TS-PCOMM-MOD         PIC X.                                   
              10  TS-LCOMM-MOD         PIC X.                                   
              10  TS-DPRIX-MOD         PIC X.                                   
              10  TS-DCOMM-MOD         PIC X.                                   
      * ZONES CALCULEES ------------------------------------------              
              10  TS-PMBUN             PIC S9(5)V99   COMP-3.                   
              10  TS-QTMBN             PIC S9(4)V9(3) COMP-3.                   
              10  TS-EDMN              PIC S9(5)V99   COMP-3.                   
              10  TS-COEF-FAM          PIC 9(3).                                
              10  TS-COEF-TRANS        PIC S9V9(6)    COMP-3.                   
              10  TS-VARB-TRANS        PIC S9(2)V9(2)    COMP-3.                
      * ZONE CODE MAGASIN  --------------------------------------       00010000
              10  TS-ZONE-MAG.                                          00002700
                  15  TS-CODE-MAG   PIC 9(3) OCCURS 170.                00002700
      *                                                                 00010000
              10  TS-EXIST-ZP          PIC X(1).                                
                                                                                
