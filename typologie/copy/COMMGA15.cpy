      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * COMMAREA SPECIFIQUE PROG TGA15                             *            
      *       TR : GA00  ADMISTRATION DES DONNEES                  *            
      *       PG : TGA15 DEFINITION DES FAMILLES: PARMS GENERAUX   *            
      *  MODIF LE 18 08 87     LONG: 330 + FILLER 3394             *            
      **************************************************************            
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724          
      *************************************************************             
      * DE02004 01/12/14 AJOUT DE LA GARANTIE MGD                               
      ************************************************************              
          02 COMM-GA15-APPLI REDEFINES COMM-GA00-APPLI.                         
      *------------------------------ CODE FONCTION                             
             03 COMM-GA15-WFONC          PIC X(3).                              
      *---------                         CHOIX                                  
             03 COMM-GA15-ZONCMD         PIC X(15).                             
      *---------                                                                
             03 FILLER                   PIC X(4).                              
      *---------                                                                
             03 COMM-GA15-GA1400.                                               
      *                                                                         
                 05 COMM-GA15-CFAM           PIC X(5).                          
                 05 COMM-GA15-LFAM           PIC X(20).                         
                 05 COMM-GA15-CMODSTOCK      PIC X(5).                          
                 05 COMM-GA15-CTAUXTVA       PIC X(5).                          
                 05 COMM-GA15-CGARANTIE      PIC X(5).                          
                 05 COMM-GA15-QBORNESOL      PIC S99V999 COMP-3.                
                 05 COMM-GA15-QBORNVRAC      PIC S99V999 COMP-3.                
                 05 COMM-GA15-QNBPSCL        PIC S999    COMP-3.                
                 05 COMM-GA15-WMULTIFAM      PIC X.                             
                 05 COMM-GA15-WSEQFAM        PIC S9(5)   COMP-3.                
                 05 COMM-GA15-CTYPENT        PIC X(2).                          
                 05 COMM-GA15-LTYPENT        PIC X(20).                         
             03 COMM-COMPLEMT-EGA15.                                            
                 05 COMM-GA15-LGARANT        PIC X(20).                         
                 05 COMM-GA15-LTVA           PIC X(20).                         
                 05 COMM-GA15-LMODSTK        PIC X(20).                         
                 05 COMM-GA15-CGARANMGD      PIC X(5).                          
                 05 COMM-GA15-LGARANMGD      PIC X(20).                         
             03 COMM-DECISION       OCCURS 5.                                   
                 05 COMM-GA15-GAUCHE         PIC X.                             
                 05 COMM-GA15-DROITE         PIC X.                             
      *---------    TAB       CODES                                             
             03 COMM-GA15-TABCOD.                                               
               05 COMM-TABCOD-ELEM    OCCURS 5.                                 
      *                                                                         
      *MAP : PARTIE GAUCHE                                                      
                   07 COMM-GA15-CG             PIC X(5).                        
                   07 COMM-GA15-LG             PIC X(20).                       
      *                                                                         
      *MAP : PARTIE DROITE                                                      
                   07 COMM-GA15-CTYP           PIC X(5).                        
                   07 COMM-GA15-LTYP           PIC X(20).                       
      *-------                                                                  
             03 COMM-GA15-FAMPREV        PIC X(5).                              
             03 COMM-GA15-FAMSUIV        PIC X(5).                              
      *-------                                                                  
             03 COMM-GA15-IDEMSEQ        PIC X(1).                              
             03 COMM-GA15-NEWSEQ         PIC S9(5)   COMP-3.                    
      *                                                                         
      *------------------------------ ZONE LIBRE                                
      *      03 COMM-GA15-LIBRE1         PIC X(3294).                           
             03 COMM-GA15-LIBRE1         PIC X(3269).                           
      *                                                                         
      *****************************************************************         
                                                                                
