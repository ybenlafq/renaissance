      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * -------------------------------------------------------------- *        
      * -  COPIE GéNéRALISéE POUR APPEL MODULES PAR TNV60            - *        
      * -  LONGUEUR DE LA ZONE DE MESSAGE 32500 CARACTéRES           - *        
      * -                                                            - *        
      * -                                                            - *        
      * -------------------------------------------------------------- *        
       01 COMM-NV60-APPLI.                                                      
          02 COMM-NV60-ENTETE.                                                  
             05 COMM-NV60-CORRELID                  PIC X(24).                  
             05 COMM-NV60-CODRET                    PIC X(02).                  
             05 COMM-NV60-libret                    pic x(80).                  
          02 COMM-NV60-MESSAGE                      PIC X(66000).               
      * -> ----------------------------------------------------                 
      * -> partie credential, avoirs ba et chéques ménafinance.                 
      * -> ----------------------------------------------------                 
          02 comm-nv60-cred redefines comm-nv60-message.                        
             05 mess-cred-tmst                      pic x(20).                  
             05 mess-cred-legacycode                pic x(01).                  
             05 mess-cred-payinstr          occurs 250.                         
                10 mess-cred-refnum                 pic x(14).                  
                10 mess-cred-validitydate           pic x(08).                  
                10 mess-cred-amount                 pic X(07).                  
                10 mess-cred-statut                 pic x(01).                  
             05 filler                              pic x(58479).               
                                                                                
