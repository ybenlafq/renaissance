      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************          
      *  TR GA22  : SELECTION DES FAMILLES DE PRESTATION                        
      ****************************************************************          
       01  TS-GA22.                                                             
         05  TS-LONG           PIC S9(4) COMP-3 VALUE 280.                      
         05  TS-DONNEES.                                                        
           07  TS-OCCURS       OCCURS 10.                                       
               10  TS-WFLAG        PIC  X(1).                                   
               10  TS-CFAM         PIC  X(5).                                   
               10  TS-WSEQED       PIC  X(2).                                   
               10  TS-LFAM         PIC  X(20).                                  
                                                                                
