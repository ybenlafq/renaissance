      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGES MQ GENERATION GV00 LOCAL                                
      *                                                                         
      *****************************************************************         
      *  POUR MBS43 AVEC GET ET PUT EXTERNES --> VA AVEC MESBS43R               
      *                                                                         
           10  MBS-MESSAGE.                                                     
      *--------------------ZONE ENTETE = 105 CAR-------------------*            
              15  MBS-ENTETE.                                                   
                 20   MBS-TYPE      PIC    X(3).                                
                 20   MBS-NSOCMSG   PIC    X(3).                                
                 20   MBS-NLIEUMSG  PIC    X(3).                                
                 20   MBS-NSOCDST   PIC    X(3).                                
                 20   MBS-NLIEUDST  PIC    X(3).                                
                 20   MBS-NORD      PIC    9(8).                                
                 20   MBS-LPROG     PIC    X(10).                               
                 20   MBS-DJOUR     PIC    X(8).                                
                 20   MBS-WSID      PIC    X(10).                               
                 20   MBS-USER      PIC    X(10).                               
                 20   MBS-CHRONO    PIC    9(7).                                
                 20   MBS-NBRMSG    PIC    9(7).                                
                 20   MBS-NBROCC    PIC    9(5).                                
                 20   MBS-LGNOCC    PIC    9(5).                                
                 20   MBS-VERSION   PIC    X(2).                                
                 20   MBS-DSYST     PIC   S9(13).                               
                 20   MBS-FILLER    PIC    X(05).                               
             15  MBS-DATA.                                                      
      *--------------------ZONE COMPTEURS = 26 CAR-----------------*            
                20  MBS-NB-MBS-NOMPROG.                                         
                   25   MBS-WOUTQ      PIC    X(10).                            
                   25   MBS-WARCHIVAGE PIC    X(01).                            
                   25   MBS-WDBK       PIC    X(01).                            
                   25   MBS-NB-ENREG   PIC    9(3).                             
                   25   MBS-NB-TOPE    PIC    9(3).                             
                   25   MBS-NB-GV10    PIC    9(3).                             
                   25   MBS-NB-GV02    PIC    9(3).                             
V41R0              25   MBS-NB-GV03    PIC    9(3).                             
                   25   MBS-NB-GV11    PIC    9(3).                             
                   25   MBS-NB-GV05    PIC    9(3).                             
                   25   MBS-NB-GV06    PIC    9(3).                             
                   25   MBS-NB-GV08    PIC    9(3).                             
                   25   MBS-NB-GV14    PIC    9(3).                             
                   25   MBS-NTRANSMAX  PIC    9(8).                             
                20  MBS-TABLES.                                                 
      *  MBS-TABLES CONTIENT AU MAXIMUM TOUS LES ENREGS SUIVANTS :              
      *  LONGUEUR DES TABLES = LONGUEUR + 4 CAR POUR LE NOM                     
      *  TABLE RTGV10 : 1 OCC. = 426 CAR              =>   426                  
      *  TABLE RTGV02 : 1 OCC. = 453 CAR MAXI  4 OCC. =>  1812                  
      *  TABLE RTGV11 : 1 OCC. = 367 CAR MAXI 80 OCC. => 29360                  
      *  TABLE RTGV14 : 1 OCC. =  74 CAR MAXI 99 OCC. =>  7326                  
      *  TABLE RTGV05 : 1 OCC. =  68 CAR MAXI  5 OCC. =>   340                  
      *  TABLE RTGV06 : 1 OCC. =  31 CAR MAXI 10 OCC. =>   310                  
      *  TABLE RTGV08 : 1 OCC. = 103 CAR MAXI 20 OCC. =>  2060                  
V41R0 *  TABLE RTGV03 : 1 OCC. =  99 CAR MAXI 05 OCC. =>   495                  
      *  TABLES RTGV* :                               => 42129                  
      *  FILLER AU CAS OU                             =>  7729                  
      *  TOTAL TABLES + FILLER                        => 49858                  
                   25   MBS-TABLES-DATA       PIC X(49848).                     
      *-----FIN-MESSAGE---LONGUEUR TOTALE = 50000 CAR------------------*        
       01 MBS.                                                                  
      *   LISTE PARTIELLE TABLE RVGV1404 : 1 OCCURENCE = 74 CAR                 
         02 MBS-GV14.                                                           
          05   MBS-GV14-NOMTAB            PIC X(0004).                          
          05   MBS-GV14-DSAISIE           PIC X(0008).                          
          05   MBS-GV14-CMODPAIMT         PIC X(0005).                          
          05   MBS-GV14-PREGLTVTE         PIC S9(7)V9(2) COMP-3.                
          05   MBS-GV14-WREGLTVTE         PIC X(0001).                          
          05   MBS-GV14-CDEVISE           PIC X(0003).                          
          05   MBS-GV14-MDEVISE           PIC S9(7)V9(02) COMP-3.               
          05   MBS-GV14-MECART            PIC S9V9(02) COMP-3.                  
          05   MBS-GV14-CDEV              PIC X(0003).                          
          05   MBS-GV14-NSOCP             PIC X(0003).                          
          05   MBS-GV14-NLIEUP            PIC X(0003).                          
          05   MBS-GV14-NTRANS            PIC 9(0008).                          
          05   MBS-GV14-PMODPAIMT         PIC S9(7)V9(2) COMP-3.                
          05   MBS-GV14-CFCRED            PIC X(0005).                          
          05   MBS-GV14-NCREDI            PIC X(0014).                          
      *-----FIN-MESSAGE------------------------------------------------*        
                                                                                
