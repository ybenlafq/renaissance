      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CARSP CARACTERISTIQUES SPECIFIQUES     *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01AX.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01AX.                                                            
      *}                                                                        
           05  CARSP-CTABLEG2    PIC X(15).                                     
           05  CARSP-CTABLEG2-REDEF REDEFINES CARSP-CTABLEG2.                   
               10  CARSP-CODE-CAR        PIC X(05).                             
           05  CARSP-WTABLEG     PIC X(80).                                     
           05  CARSP-WTABLEG-REDEF  REDEFINES CARSP-WTABLEG.                    
               10  CARSP-LIBELLE         PIC X(20).                             
               10  CARSP-CVDCARAC        PIC X(05).                             
               10  CARSP-WCARACTS        PIC X(01).                             
               10  CARSP-FINNOV          PIC X(05).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01AX-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01AX-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CARSP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CARSP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CARSP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CARSP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
