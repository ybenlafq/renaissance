      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * VISUALISATION DU REF / FILIALE                                  00000020
      ***************************************************************** 00000030
       01   EGN30I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(5).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(5).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(5).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(5).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000230
           02 FILLER    PIC X(5).                                       00000240
           02 MNCODICI  PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOUL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLREFFOUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLREFFOUF      PIC X.                                     00000270
           02 FILLER    PIC X(5).                                       00000280
           02 MLREFFOUI      PIC X(20).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000310
           02 FILLER    PIC X(5).                                       00000320
           02 MCMARQI   PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(5).                                       00000360
           02 MCFAMI    PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWOAL     COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MWOAL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MWOAF     PIC X.                                          00000390
           02 FILLER    PIC X(5).                                       00000400
           02 MWOAI     PIC X.                                          00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSUBSL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MSUBSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSUBSF    PIC X.                                          00000430
           02 FILLER    PIC X(5).                                       00000440
           02 MSUBSI    PIC X(6).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICSL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MNCODICSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODICSF      PIC X.                                     00000470
           02 FILLER    PIC X(5).                                       00000480
           02 MNCODICSI      PIC X(7).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 DEFFETOAL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 DEFFETOAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 DEFFETOAF      PIC X.                                     00000510
           02 FILLER    PIC X(5).                                       00000520
           02 DEFFETOAI      PIC X(8).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 DFINEFOAL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 DFINEFOAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 DFINEFOAF      PIC X.                                     00000550
           02 FILLER    PIC X(5).                                       00000560
           02 DFINEFOAI      PIC X(8).                                  00000570
           02 MPREFTTCD OCCURS   2 TIMES .                              00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPREFTTCL    COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MPREFTTCL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MPREFTTCF    PIC X.                                     00000600
             03 FILLER  PIC X(5).                                       00000610
             03 MPREFTTCI    PIC X(8).                                  00000620
           02 MWACTIFD OCCURS   2 TIMES .                               00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWACTIFL     COMP PIC S9(4).                            00000640
      *--                                                                       
             03 MWACTIFL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWACTIFF     PIC X.                                     00000650
             03 FILLER  PIC X(5).                                       00000660
             03 MWACTIFI     PIC X.                                     00000670
           02 DEFFETD OCCURS   2 TIMES .                                00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 DEFFETL      COMP PIC S9(4).                            00000690
      *--                                                                       
             03 DEFFETL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 DEFFETF      PIC X.                                     00000700
             03 FILLER  PIC X(5).                                       00000710
             03 DEFFETI      PIC X(8).                                  00000720
           02 DFINEFFED OCCURS   2 TIMES .                              00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 DFINEFFEL    COMP PIC S9(4).                            00000740
      *--                                                                       
             03 DFINEFFEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 DFINEFFEF    PIC X.                                     00000750
             03 FILLER  PIC X(5).                                       00000760
             03 DFINEFFEI    PIC X(8).                                  00000770
           02 MOPARENTD OCCURS   2 TIMES .                              00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MOPARENTL    COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MOPARENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MOPARENTF    PIC X.                                     00000800
             03 FILLER  PIC X(5).                                       00000810
             03 MOPARENTI    PIC X.                                     00000820
           02 LCOMMENTD OCCURS   2 TIMES .                              00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 LCOMMENTL    COMP PIC S9(4).                            00000840
      *--                                                                       
             03 LCOMMENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 LCOMMENTF    PIC X.                                     00000850
             03 FILLER  PIC X(5).                                       00000860
             03 LCOMMENTI    PIC X(10).                                 00000870
           02 MFPARENTD OCCURS   2 TIMES .                              00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFPARENTL    COMP PIC S9(4).                            00000890
      *--                                                                       
             03 MFPARENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MFPARENTF    PIC X.                                     00000900
             03 FILLER  PIC X(5).                                       00000910
             03 MFPARENTI    PIC X.                                     00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRRL     COMP PIC S9(4).                                 00000930
      *--                                                                       
           02 MPRRL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MPRRF     PIC X.                                          00000940
           02 FILLER    PIC X(5).                                       00000950
           02 MPRRI     PIC X(6).                                       00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDPRRL   COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MDDPRRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDDPRRF   PIC X.                                          00000980
           02 FILLER    PIC X(5).                                       00000990
           02 MDDPRRI   PIC X(8).                                       00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFPRRL   COMP PIC S9(4).                                 00001010
      *--                                                                       
           02 MDFPRRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDFPRRF   PIC X.                                          00001020
           02 FILLER    PIC X(5).                                       00001030
           02 MDFPRRI   PIC X(8).                                       00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOPPRRL   COMP PIC S9(4).                                 00001050
      *--                                                                       
           02 MOPPRRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MOPPRRF   PIC X.                                          00001060
           02 FILLER    PIC X(5).                                       00001070
           02 MOPPRRI   PIC X.                                          00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPRRL    COMP PIC S9(4).                                 00001090
      *--                                                                       
           02 MCPRRL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCPRRF    PIC X.                                          00001100
           02 FILLER    PIC X(5).                                       00001110
           02 MCPRRI    PIC X(10).                                      00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFPPRRL   COMP PIC S9(4).                                 00001130
      *--                                                                       
           02 MFPPRRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MFPPRRF   PIC X.                                          00001140
           02 FILLER    PIC X(5).                                       00001150
           02 MFPPRRI   PIC X.                                          00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LIBF10F11L     COMP PIC S9(4).                            00001170
      *--                                                                       
           02 LIBF10F11L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 LIBF10F11F     PIC X.                                     00001180
           02 FILLER    PIC X(5).                                       00001190
           02 LIBF10F11I     PIC X(33).                                 00001200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPROL  COMP PIC S9(4).                                 00001210
      *--                                                                       
           02 MCAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAPPROF  PIC X.                                          00001220
           02 FILLER    PIC X(5).                                       00001230
           02 MCAPPROI  PIC X(3).                                       00001240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRMPL    COMP PIC S9(4).                                 00001250
      *--                                                                       
           02 MPRMPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPRMPF    PIC X.                                          00001260
           02 FILLER    PIC X(5).                                       00001270
           02 MPRMPI    PIC X(8).                                       00001280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SOCDEPOTL      COMP PIC S9(4).                            00001290
      *--                                                                       
           02 SOCDEPOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 SOCDEPOTF      PIC X.                                     00001300
           02 FILLER    PIC X(5).                                       00001310
           02 SOCDEPOTI      PIC X(3).                                  00001320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPOTL   COMP PIC S9(4).                                 00001330
      *--                                                                       
           02 MDEPOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEPOTF   PIC X.                                          00001340
           02 FILLER    PIC X(5).                                       00001350
           02 MDEPOTI   PIC X(3).                                       00001360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQSTOCKDL      COMP PIC S9(4).                            00001370
      *--                                                                       
           02 MQSTOCKDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQSTOCKDF      PIC X.                                     00001380
           02 FILLER    PIC X(5).                                       00001390
           02 MQSTOCKDI      PIC X(5).                                  00001400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPOL   COMP PIC S9(4).                                 00001410
      *--                                                                       
           02 MCEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXPOF   PIC X.                                          00001420
           02 FILLER    PIC X(5).                                       00001430
           02 MCEXPOI   PIC X(2).                                       00001440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSRPL     COMP PIC S9(4).                                 00001450
      *--                                                                       
           02 MSRPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSRPF     PIC X.                                          00001460
           02 FILLER    PIC X(5).                                       00001470
           02 MSRPI     PIC X(8).                                       00001480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 DATECDEL  COMP PIC S9(4).                                 00001490
      *--                                                                       
           02 DATECDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 DATECDEF  PIC X.                                          00001500
           02 FILLER    PIC X(5).                                       00001510
           02 DATECDEI  PIC X(10).                                      00001520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTECDEL  COMP PIC S9(4).                                 00001530
      *--                                                                       
           02 MQTECDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQTECDEF  PIC X.                                          00001540
           02 FILLER    PIC X(5).                                       00001550
           02 MQTECDEI  PIC X(5).                                       00001560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCIETEL      COMP PIC S9(4).                            00001570
      *--                                                                       
           02 MSOCIETEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOCIETEF      PIC X.                                     00001580
           02 FILLER    PIC X(5).                                       00001590
           02 MSOCIETEI      PIC X(3).                                  00001600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LSTATCOML      COMP PIC S9(4).                            00001610
      *--                                                                       
           02 LSTATCOML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 LSTATCOMF      PIC X.                                     00001620
           02 FILLER    PIC X(5).                                       00001630
           02 LSTATCOMI      PIC X(3).                                  00001640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRAL     COMP PIC S9(4).                                 00001650
      *--                                                                       
           02 MPRAL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MPRAF     PIC X.                                          00001660
           02 FILLER    PIC X(5).                                       00001670
           02 MPRAI     PIC X(8).                                       00001680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCROSSL   COMP PIC S9(4).                                 00001690
      *--                                                                       
           02 MCROSSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCROSSF   PIC X.                                          00001700
           02 FILLER    PIC X(5).                                       00001710
           02 MCROSSI   PIC X.                                          00001720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBVAR1L      COMP PIC S9(4).                            00001730
      *--                                                                       
           02 MLIBVAR1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBVAR1F      PIC X.                                     00001740
           02 FILLER    PIC X(5).                                       00001750
           02 MLIBVAR1I      PIC X(5).                                  00001760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBVAR2L      COMP PIC S9(4).                            00001770
      *--                                                                       
           02 MLIBVAR2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBVAR2F      PIC X.                                     00001780
           02 FILLER    PIC X(5).                                       00001790
           02 MLIBVAR2I      PIC X(5).                                  00001800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBVAR3L      COMP PIC S9(4).                            00001810
      *--                                                                       
           02 MLIBVAR3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBVAR3F      PIC X.                                     00001820
           02 FILLER    PIC X(5).                                       00001830
           02 MLIBVAR3I      PIC X(5).                                  00001840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBVAR4L      COMP PIC S9(4).                            00001850
      *--                                                                       
           02 MLIBVAR4L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBVAR4F      PIC X.                                     00001860
           02 FILLER    PIC X(5).                                       00001870
           02 MLIBVAR4I      PIC X(5).                                  00001880
           02 MZONPRIXD OCCURS   7 TIMES .                              00001890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZONPRIXL    COMP PIC S9(4).                            00001900
      *--                                                                       
             03 MZONPRIXL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MZONPRIXF    PIC X.                                     00001910
             03 FILLER  PIC X(5).                                       00001920
             03 MZONPRIXI    PIC X(2).                                  00001930
           02 MNLIEUD OCCURS   7 TIMES .                                00001940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00001950
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00001960
             03 FILLER  PIC X(5).                                       00001970
             03 MNLIEUI      PIC X(3).                                  00001980
           02 MWOAED OCCURS   7 TIMES .                                 00001990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWOAEL  COMP PIC S9(4).                                 00002000
      *--                                                                       
             03 MWOAEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MWOAEF  PIC X.                                          00002010
             03 FILLER  PIC X(5).                                       00002020
             03 MWOAEI  PIC X.                                          00002030
           02 MPSTDTTCD OCCURS   7 TIMES .                              00002040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPSTDTTCL    COMP PIC S9(4).                            00002050
      *--                                                                       
             03 MPSTDTTCL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MPSTDTTCF    PIC X.                                     00002060
             03 FILLER  PIC X(5).                                       00002070
             03 MPSTDTTCI    PIC X(8).                                  00002080
           02 DEFFETPVD OCCURS   7 TIMES .                              00002090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 DEFFETPVL    COMP PIC S9(4).                            00002100
      *--                                                                       
             03 DEFFETPVL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 DEFFETPVF    PIC X.                                     00002110
             03 FILLER  PIC X(5).                                       00002120
             03 DEFFETPVI    PIC X(8).                                  00002130
           02 MCORIGD OCCURS   7 TIMES .                                00002140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCORIGL      COMP PIC S9(4).                            00002150
      *--                                                                       
             03 MCORIGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCORIGF      PIC X.                                     00002160
             03 FILLER  PIC X(5).                                       00002170
             03 MCORIGI      PIC X.                                     00002180
           02 MTAUXMBD OCCURS   7 TIMES .                               00002190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTAUXMBL     COMP PIC S9(4).                            00002200
      *--                                                                       
             03 MTAUXMBL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTAUXMBF     PIC X.                                     00002210
             03 FILLER  PIC X(5).                                       00002220
             03 MTAUXMBI     PIC X(7).                                  00002230
           02 MPRIMED OCCURS   7 TIMES .                                00002240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIMEL      COMP PIC S9(4).                            00002250
      *--                                                                       
             03 MPRIMEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPRIMEF      PIC X.                                     00002260
             03 FILLER  PIC X(5).                                       00002270
             03 MPRIMEI      PIC X(6).                                  00002280
           02 MINDPRD OCCURS   7 TIMES .                                00002290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MINDPRL      COMP PIC S9(4).                            00002300
      *--                                                                       
             03 MINDPRL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MINDPRF      PIC X.                                     00002310
             03 FILLER  PIC X(5).                                       00002320
             03 MINDPRI      PIC X.                                     00002330
           02 DEFFETPD OCCURS   7 TIMES .                               00002340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 DEFFETPL     COMP PIC S9(4).                            00002350
      *--                                                                       
             03 DEFFETPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 DEFFETPF     PIC X.                                     00002360
             03 FILLER  PIC X(5).                                       00002370
             03 DEFFETPI     PIC X(8).                                  00002380
           02 MQSTOCKMD OCCURS   7 TIMES .                              00002390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSTOCKML    COMP PIC S9(4).                            00002400
      *--                                                                       
             03 MQSTOCKML COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQSTOCKMF    PIC X.                                     00002410
             03 FILLER  PIC X(5).                                       00002420
             03 MQSTOCKMI    PIC X(4).                                  00002430
           02 MCOEFTRAND OCCURS   7 TIMES .                             00002440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOEFTRANL   COMP PIC S9(4).                            00002450
      *--                                                                       
             03 MCOEFTRANL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCOEFTRANF   PIC X.                                     00002460
             03 FILLER  PIC X(5).                                       00002470
             03 MCOEFTRANI   PIC X(8).                                  00002480
           02 MVARBTRAND OCCURS   7 TIMES .                             00002490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVARBTRANL   COMP PIC S9(4).                            00002500
      *--                                                                       
             03 MVARBTRANL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MVARBTRANF   PIC X.                                     00002510
             03 FILLER  PIC X(5).                                       00002520
             03 MVARBTRANI   PIC X(5).                                  00002530
           02 MVTE4SD OCCURS   7 TIMES .                                00002540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVTE4SL      COMP PIC S9(4).                            00002550
      *--                                                                       
             03 MVTE4SL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MVTE4SF      PIC X.                                     00002560
             03 FILLER  PIC X(5).                                       00002570
             03 MVTE4SI      PIC X(4).                                  00002580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00002590
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00002600
           02 FILLER    PIC X(5).                                       00002610
           02 MLIBERRI  PIC X(78).                                      00002620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00002630
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00002640
           02 FILLER    PIC X(5).                                       00002650
           02 MCODTRAI  PIC X(4).                                       00002660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00002670
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00002680
           02 FILLER    PIC X(5).                                       00002690
           02 MCICSI    PIC X(5).                                       00002700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00002710
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00002720
           02 FILLER    PIC X(5).                                       00002730
           02 MNETNAMI  PIC X(8).                                       00002740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00002750
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00002760
           02 FILLER    PIC X(5).                                       00002770
           02 MSCREENI  PIC X(4).                                       00002780
      ***************************************************************** 00002790
      * VISUALISATION DU REF / FILIALE                                  00002800
      ***************************************************************** 00002810
       01   EGN30O REDEFINES EGN30I.                                    00002820
           02 FILLER    PIC X(12).                                      00002830
           02 FILLER    PIC X(2).                                       00002840
           02 MDATJOUA  PIC X.                                          00002850
           02 MDATJOUC  PIC X.                                          00002860
           02 MDATJOUP  PIC X.                                          00002870
           02 MDATJOUH  PIC X.                                          00002880
           02 MDATJOUV  PIC X.                                          00002890
           02 MDATJOUU  PIC X.                                          00002900
           02 MDATJOUO  PIC X(10).                                      00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MTIMJOUA  PIC X.                                          00002930
           02 MTIMJOUC  PIC X.                                          00002940
           02 MTIMJOUP  PIC X.                                          00002950
           02 MTIMJOUH  PIC X.                                          00002960
           02 MTIMJOUV  PIC X.                                          00002970
           02 MTIMJOUU  PIC X.                                          00002980
           02 MTIMJOUO  PIC X(5).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MPAGEA    PIC X.                                          00003010
           02 MPAGEC    PIC X.                                          00003020
           02 MPAGEP    PIC X.                                          00003030
           02 MPAGEH    PIC X.                                          00003040
           02 MPAGEV    PIC X.                                          00003050
           02 MPAGEU    PIC X.                                          00003060
           02 MPAGEO    PIC X(3).                                       00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MPAGEMAXA      PIC X.                                     00003090
           02 MPAGEMAXC PIC X.                                          00003100
           02 MPAGEMAXP PIC X.                                          00003110
           02 MPAGEMAXH PIC X.                                          00003120
           02 MPAGEMAXV PIC X.                                          00003130
           02 MPAGEMAXU PIC X.                                          00003140
           02 MPAGEMAXO      PIC X(3).                                  00003150
           02 FILLER    PIC X(2).                                       00003160
           02 MNCODICA  PIC X.                                          00003170
           02 MNCODICC  PIC X.                                          00003180
           02 MNCODICP  PIC X.                                          00003190
           02 MNCODICH  PIC X.                                          00003200
           02 MNCODICV  PIC X.                                          00003210
           02 MNCODICU  PIC X.                                          00003220
           02 MNCODICO  PIC X(7).                                       00003230
           02 FILLER    PIC X(2).                                       00003240
           02 MLREFFOUA      PIC X.                                     00003250
           02 MLREFFOUC PIC X.                                          00003260
           02 MLREFFOUP PIC X.                                          00003270
           02 MLREFFOUH PIC X.                                          00003280
           02 MLREFFOUV PIC X.                                          00003290
           02 MLREFFOUU PIC X.                                          00003300
           02 MLREFFOUO      PIC X(20).                                 00003310
           02 FILLER    PIC X(2).                                       00003320
           02 MCMARQA   PIC X.                                          00003330
           02 MCMARQC   PIC X.                                          00003340
           02 MCMARQP   PIC X.                                          00003350
           02 MCMARQH   PIC X.                                          00003360
           02 MCMARQV   PIC X.                                          00003370
           02 MCMARQU   PIC X.                                          00003380
           02 MCMARQO   PIC X(5).                                       00003390
           02 FILLER    PIC X(2).                                       00003400
           02 MCFAMA    PIC X.                                          00003410
           02 MCFAMC    PIC X.                                          00003420
           02 MCFAMP    PIC X.                                          00003430
           02 MCFAMH    PIC X.                                          00003440
           02 MCFAMV    PIC X.                                          00003450
           02 MCFAMU    PIC X.                                          00003460
           02 MCFAMO    PIC X(5).                                       00003470
           02 FILLER    PIC X(2).                                       00003480
           02 MWOAA     PIC X.                                          00003490
           02 MWOAC     PIC X.                                          00003500
           02 MWOAP     PIC X.                                          00003510
           02 MWOAH     PIC X.                                          00003520
           02 MWOAV     PIC X.                                          00003530
           02 MWOAU     PIC X.                                          00003540
           02 MWOAO     PIC X.                                          00003550
           02 FILLER    PIC X(2).                                       00003560
           02 MSUBSA    PIC X.                                          00003570
           02 MSUBSC    PIC X.                                          00003580
           02 MSUBSP    PIC X.                                          00003590
           02 MSUBSH    PIC X.                                          00003600
           02 MSUBSV    PIC X.                                          00003610
           02 MSUBSU    PIC X.                                          00003620
           02 MSUBSO    PIC X(6).                                       00003630
           02 FILLER    PIC X(2).                                       00003640
           02 MNCODICSA      PIC X.                                     00003650
           02 MNCODICSC PIC X.                                          00003660
           02 MNCODICSP PIC X.                                          00003670
           02 MNCODICSH PIC X.                                          00003680
           02 MNCODICSV PIC X.                                          00003690
           02 MNCODICSU PIC X.                                          00003700
           02 MNCODICSO      PIC X(7).                                  00003710
           02 FILLER    PIC X(2).                                       00003720
           02 DEFFETOAA      PIC X.                                     00003730
           02 DEFFETOAC PIC X.                                          00003740
           02 DEFFETOAP PIC X.                                          00003750
           02 DEFFETOAH PIC X.                                          00003760
           02 DEFFETOAV PIC X.                                          00003770
           02 DEFFETOAU PIC X.                                          00003780
           02 DEFFETOAO      PIC X(8).                                  00003790
           02 FILLER    PIC X(2).                                       00003800
           02 DFINEFOAA      PIC X.                                     00003810
           02 DFINEFOAC PIC X.                                          00003820
           02 DFINEFOAP PIC X.                                          00003830
           02 DFINEFOAH PIC X.                                          00003840
           02 DFINEFOAV PIC X.                                          00003850
           02 DFINEFOAU PIC X.                                          00003860
           02 DFINEFOAO      PIC X(8).                                  00003870
           02 DFHMS1 OCCURS   2 TIMES .                                 00003880
             03 FILLER       PIC X(2).                                  00003890
             03 MPREFTTCA    PIC X.                                     00003900
             03 MPREFTTCC    PIC X.                                     00003910
             03 MPREFTTCP    PIC X.                                     00003920
             03 MPREFTTCH    PIC X.                                     00003930
             03 MPREFTTCV    PIC X.                                     00003940
             03 MPREFTTCU    PIC X.                                     00003950
             03 MPREFTTCO    PIC X(8).                                  00003960
           02 DFHMS2 OCCURS   2 TIMES .                                 00003970
             03 FILLER       PIC X(2).                                  00003980
             03 MWACTIFA     PIC X.                                     00003990
             03 MWACTIFC     PIC X.                                     00004000
             03 MWACTIFP     PIC X.                                     00004010
             03 MWACTIFH     PIC X.                                     00004020
             03 MWACTIFV     PIC X.                                     00004030
             03 MWACTIFU     PIC X.                                     00004040
             03 MWACTIFO     PIC X.                                     00004050
           02 DFHMS3 OCCURS   2 TIMES .                                 00004060
             03 FILLER       PIC X(2).                                  00004070
             03 DEFFETA      PIC X.                                     00004080
             03 DEFFETC PIC X.                                          00004090
             03 DEFFETP PIC X.                                          00004100
             03 DEFFETH PIC X.                                          00004110
             03 DEFFETV PIC X.                                          00004120
             03 DEFFETU PIC X.                                          00004130
             03 DEFFETO      PIC X(8).                                  00004140
           02 DFHMS4 OCCURS   2 TIMES .                                 00004150
             03 FILLER       PIC X(2).                                  00004160
             03 DFINEFFEA    PIC X.                                     00004170
             03 DFINEFFEC    PIC X.                                     00004180
             03 DFINEFFEP    PIC X.                                     00004190
             03 DFINEFFEH    PIC X.                                     00004200
             03 DFINEFFEV    PIC X.                                     00004210
             03 DFINEFFEU    PIC X.                                     00004220
             03 DFINEFFEO    PIC X(8).                                  00004230
           02 DFHMS5 OCCURS   2 TIMES .                                 00004240
             03 FILLER       PIC X(2).                                  00004250
             03 MOPARENTA    PIC X.                                     00004260
             03 MOPARENTC    PIC X.                                     00004270
             03 MOPARENTP    PIC X.                                     00004280
             03 MOPARENTH    PIC X.                                     00004290
             03 MOPARENTV    PIC X.                                     00004300
             03 MOPARENTU    PIC X.                                     00004310
             03 MOPARENTO    PIC X.                                     00004320
           02 DFHMS6 OCCURS   2 TIMES .                                 00004330
             03 FILLER       PIC X(2).                                  00004340
             03 LCOMMENTA    PIC X.                                     00004350
             03 LCOMMENTC    PIC X.                                     00004360
             03 LCOMMENTP    PIC X.                                     00004370
             03 LCOMMENTH    PIC X.                                     00004380
             03 LCOMMENTV    PIC X.                                     00004390
             03 LCOMMENTU    PIC X.                                     00004400
             03 LCOMMENTO    PIC X(10).                                 00004410
           02 DFHMS7 OCCURS   2 TIMES .                                 00004420
             03 FILLER       PIC X(2).                                  00004430
             03 MFPARENTA    PIC X.                                     00004440
             03 MFPARENTC    PIC X.                                     00004450
             03 MFPARENTP    PIC X.                                     00004460
             03 MFPARENTH    PIC X.                                     00004470
             03 MFPARENTV    PIC X.                                     00004480
             03 MFPARENTU    PIC X.                                     00004490
             03 MFPARENTO    PIC X.                                     00004500
           02 FILLER    PIC X(2).                                       00004510
           02 MPRRA     PIC X.                                          00004520
           02 MPRRC     PIC X.                                          00004530
           02 MPRRP     PIC X.                                          00004540
           02 MPRRH     PIC X.                                          00004550
           02 MPRRV     PIC X.                                          00004560
           02 MPRRU     PIC X.                                          00004570
           02 MPRRO     PIC X(6).                                       00004580
           02 FILLER    PIC X(2).                                       00004590
           02 MDDPRRA   PIC X.                                          00004600
           02 MDDPRRC   PIC X.                                          00004610
           02 MDDPRRP   PIC X.                                          00004620
           02 MDDPRRH   PIC X.                                          00004630
           02 MDDPRRV   PIC X.                                          00004640
           02 MDDPRRU   PIC X.                                          00004650
           02 MDDPRRO   PIC X(8).                                       00004660
           02 FILLER    PIC X(2).                                       00004670
           02 MDFPRRA   PIC X.                                          00004680
           02 MDFPRRC   PIC X.                                          00004690
           02 MDFPRRP   PIC X.                                          00004700
           02 MDFPRRH   PIC X.                                          00004710
           02 MDFPRRV   PIC X.                                          00004720
           02 MDFPRRU   PIC X.                                          00004730
           02 MDFPRRO   PIC X(8).                                       00004740
           02 FILLER    PIC X(2).                                       00004750
           02 MOPPRRA   PIC X.                                          00004760
           02 MOPPRRC   PIC X.                                          00004770
           02 MOPPRRP   PIC X.                                          00004780
           02 MOPPRRH   PIC X.                                          00004790
           02 MOPPRRV   PIC X.                                          00004800
           02 MOPPRRU   PIC X.                                          00004810
           02 MOPPRRO   PIC X.                                          00004820
           02 FILLER    PIC X(2).                                       00004830
           02 MCPRRA    PIC X.                                          00004840
           02 MCPRRC    PIC X.                                          00004850
           02 MCPRRP    PIC X.                                          00004860
           02 MCPRRH    PIC X.                                          00004870
           02 MCPRRV    PIC X.                                          00004880
           02 MCPRRU    PIC X.                                          00004890
           02 MCPRRO    PIC X(10).                                      00004900
           02 FILLER    PIC X(2).                                       00004910
           02 MFPPRRA   PIC X.                                          00004920
           02 MFPPRRC   PIC X.                                          00004930
           02 MFPPRRP   PIC X.                                          00004940
           02 MFPPRRH   PIC X.                                          00004950
           02 MFPPRRV   PIC X.                                          00004960
           02 MFPPRRU   PIC X.                                          00004970
           02 MFPPRRO   PIC X.                                          00004980
           02 FILLER    PIC X(2).                                       00004990
           02 LIBF10F11A     PIC X.                                     00005000
           02 LIBF10F11C     PIC X.                                     00005010
           02 LIBF10F11P     PIC X.                                     00005020
           02 LIBF10F11H     PIC X.                                     00005030
           02 LIBF10F11V     PIC X.                                     00005040
           02 LIBF10F11U     PIC X.                                     00005050
           02 LIBF10F11O     PIC X(33).                                 00005060
           02 FILLER    PIC X(2).                                       00005070
           02 MCAPPROA  PIC X.                                          00005080
           02 MCAPPROC  PIC X.                                          00005090
           02 MCAPPROP  PIC X.                                          00005100
           02 MCAPPROH  PIC X.                                          00005110
           02 MCAPPROV  PIC X.                                          00005120
           02 MCAPPROU  PIC X.                                          00005130
           02 MCAPPROO  PIC X(3).                                       00005140
           02 FILLER    PIC X(2).                                       00005150
           02 MPRMPA    PIC X.                                          00005160
           02 MPRMPC    PIC X.                                          00005170
           02 MPRMPP    PIC X.                                          00005180
           02 MPRMPH    PIC X.                                          00005190
           02 MPRMPV    PIC X.                                          00005200
           02 MPRMPU    PIC X.                                          00005210
           02 MPRMPO    PIC X(8).                                       00005220
           02 FILLER    PIC X(2).                                       00005230
           02 SOCDEPOTA      PIC X.                                     00005240
           02 SOCDEPOTC PIC X.                                          00005250
           02 SOCDEPOTP PIC X.                                          00005260
           02 SOCDEPOTH PIC X.                                          00005270
           02 SOCDEPOTV PIC X.                                          00005280
           02 SOCDEPOTU PIC X.                                          00005290
           02 SOCDEPOTO      PIC X(3).                                  00005300
           02 FILLER    PIC X(2).                                       00005310
           02 MDEPOTA   PIC X.                                          00005320
           02 MDEPOTC   PIC X.                                          00005330
           02 MDEPOTP   PIC X.                                          00005340
           02 MDEPOTH   PIC X.                                          00005350
           02 MDEPOTV   PIC X.                                          00005360
           02 MDEPOTU   PIC X.                                          00005370
           02 MDEPOTO   PIC X(3).                                       00005380
           02 FILLER    PIC X(2).                                       00005390
           02 MQSTOCKDA      PIC X.                                     00005400
           02 MQSTOCKDC PIC X.                                          00005410
           02 MQSTOCKDP PIC X.                                          00005420
           02 MQSTOCKDH PIC X.                                          00005430
           02 MQSTOCKDV PIC X.                                          00005440
           02 MQSTOCKDU PIC X.                                          00005450
           02 MQSTOCKDO      PIC X(5).                                  00005460
           02 FILLER    PIC X(2).                                       00005470
           02 MCEXPOA   PIC X.                                          00005480
           02 MCEXPOC   PIC X.                                          00005490
           02 MCEXPOP   PIC X.                                          00005500
           02 MCEXPOH   PIC X.                                          00005510
           02 MCEXPOV   PIC X.                                          00005520
           02 MCEXPOU   PIC X.                                          00005530
           02 MCEXPOO   PIC X(2).                                       00005540
           02 FILLER    PIC X(2).                                       00005550
           02 MSRPA     PIC X.                                          00005560
           02 MSRPC     PIC X.                                          00005570
           02 MSRPP     PIC X.                                          00005580
           02 MSRPH     PIC X.                                          00005590
           02 MSRPV     PIC X.                                          00005600
           02 MSRPU     PIC X.                                          00005610
           02 MSRPO     PIC X(8).                                       00005620
           02 FILLER    PIC X(2).                                       00005630
           02 DATECDEA  PIC X.                                          00005640
           02 DATECDEC  PIC X.                                          00005650
           02 DATECDEP  PIC X.                                          00005660
           02 DATECDEH  PIC X.                                          00005670
           02 DATECDEV  PIC X.                                          00005680
           02 DATECDEU  PIC X.                                          00005690
           02 DATECDEO  PIC X(10).                                      00005700
           02 FILLER    PIC X(2).                                       00005710
           02 MQTECDEA  PIC X.                                          00005720
           02 MQTECDEC  PIC X.                                          00005730
           02 MQTECDEP  PIC X.                                          00005740
           02 MQTECDEH  PIC X.                                          00005750
           02 MQTECDEV  PIC X.                                          00005760
           02 MQTECDEU  PIC X.                                          00005770
           02 MQTECDEO  PIC X(5).                                       00005780
           02 FILLER    PIC X(2).                                       00005790
           02 MSOCIETEA      PIC X.                                     00005800
           02 MSOCIETEC PIC X.                                          00005810
           02 MSOCIETEP PIC X.                                          00005820
           02 MSOCIETEH PIC X.                                          00005830
           02 MSOCIETEV PIC X.                                          00005840
           02 MSOCIETEU PIC X.                                          00005850
           02 MSOCIETEO      PIC X(3).                                  00005860
           02 FILLER    PIC X(2).                                       00005870
           02 LSTATCOMA      PIC X.                                     00005880
           02 LSTATCOMC PIC X.                                          00005890
           02 LSTATCOMP PIC X.                                          00005900
           02 LSTATCOMH PIC X.                                          00005910
           02 LSTATCOMV PIC X.                                          00005920
           02 LSTATCOMU PIC X.                                          00005930
           02 LSTATCOMO      PIC X(3).                                  00005940
           02 FILLER    PIC X(2).                                       00005950
           02 MPRAA     PIC X.                                          00005960
           02 MPRAC     PIC X.                                          00005970
           02 MPRAP     PIC X.                                          00005980
           02 MPRAH     PIC X.                                          00005990
           02 MPRAV     PIC X.                                          00006000
           02 MPRAU     PIC X.                                          00006010
           02 MPRAO     PIC X(8).                                       00006020
           02 FILLER    PIC X(2).                                       00006030
           02 MCROSSA   PIC X.                                          00006040
           02 MCROSSC   PIC X.                                          00006050
           02 MCROSSP   PIC X.                                          00006060
           02 MCROSSH   PIC X.                                          00006070
           02 MCROSSV   PIC X.                                          00006080
           02 MCROSSU   PIC X.                                          00006090
           02 MCROSSO   PIC X.                                          00006100
           02 FILLER    PIC X(2).                                       00006110
           02 MLIBVAR1A      PIC X.                                     00006120
           02 MLIBVAR1C PIC X.                                          00006130
           02 MLIBVAR1P PIC X.                                          00006140
           02 MLIBVAR1H PIC X.                                          00006150
           02 MLIBVAR1V PIC X.                                          00006160
           02 MLIBVAR1U PIC X.                                          00006170
           02 MLIBVAR1O      PIC X(5).                                  00006180
           02 FILLER    PIC X(2).                                       00006190
           02 MLIBVAR2A      PIC X.                                     00006200
           02 MLIBVAR2C PIC X.                                          00006210
           02 MLIBVAR2P PIC X.                                          00006220
           02 MLIBVAR2H PIC X.                                          00006230
           02 MLIBVAR2V PIC X.                                          00006240
           02 MLIBVAR2U PIC X.                                          00006250
           02 MLIBVAR2O      PIC X(5).                                  00006260
           02 FILLER    PIC X(2).                                       00006270
           02 MLIBVAR3A      PIC X.                                     00006280
           02 MLIBVAR3C PIC X.                                          00006290
           02 MLIBVAR3P PIC X.                                          00006300
           02 MLIBVAR3H PIC X.                                          00006310
           02 MLIBVAR3V PIC X.                                          00006320
           02 MLIBVAR3U PIC X.                                          00006330
           02 MLIBVAR3O      PIC X(5).                                  00006340
           02 FILLER    PIC X(2).                                       00006350
           02 MLIBVAR4A      PIC X.                                     00006360
           02 MLIBVAR4C PIC X.                                          00006370
           02 MLIBVAR4P PIC X.                                          00006380
           02 MLIBVAR4H PIC X.                                          00006390
           02 MLIBVAR4V PIC X.                                          00006400
           02 MLIBVAR4U PIC X.                                          00006410
           02 MLIBVAR4O      PIC X(5).                                  00006420
           02 DFHMS8 OCCURS   7 TIMES .                                 00006430
             03 FILLER       PIC X(2).                                  00006440
             03 MZONPRIXA    PIC X.                                     00006450
             03 MZONPRIXC    PIC X.                                     00006460
             03 MZONPRIXP    PIC X.                                     00006470
             03 MZONPRIXH    PIC X.                                     00006480
             03 MZONPRIXV    PIC X.                                     00006490
             03 MZONPRIXU    PIC X.                                     00006500
             03 MZONPRIXO    PIC X(2).                                  00006510
           02 DFHMS9 OCCURS   7 TIMES .                                 00006520
             03 FILLER       PIC X(2).                                  00006530
             03 MNLIEUA      PIC X.                                     00006540
             03 MNLIEUC PIC X.                                          00006550
             03 MNLIEUP PIC X.                                          00006560
             03 MNLIEUH PIC X.                                          00006570
             03 MNLIEUV PIC X.                                          00006580
             03 MNLIEUU PIC X.                                          00006590
             03 MNLIEUO      PIC X(3).                                  00006600
           02 DFHMS10 OCCURS   7 TIMES .                                00006610
             03 FILLER       PIC X(2).                                  00006620
             03 MWOAEA  PIC X.                                          00006630
             03 MWOAEC  PIC X.                                          00006640
             03 MWOAEP  PIC X.                                          00006650
             03 MWOAEH  PIC X.                                          00006660
             03 MWOAEV  PIC X.                                          00006670
             03 MWOAEU  PIC X.                                          00006680
             03 MWOAEO  PIC X.                                          00006690
           02 DFHMS11 OCCURS   7 TIMES .                                00006700
             03 FILLER       PIC X(2).                                  00006710
             03 MPSTDTTCA    PIC X.                                     00006720
             03 MPSTDTTCC    PIC X.                                     00006730
             03 MPSTDTTCP    PIC X.                                     00006740
             03 MPSTDTTCH    PIC X.                                     00006750
             03 MPSTDTTCV    PIC X.                                     00006760
             03 MPSTDTTCU    PIC X.                                     00006770
             03 MPSTDTTCO    PIC X(8).                                  00006780
           02 DFHMS12 OCCURS   7 TIMES .                                00006790
             03 FILLER       PIC X(2).                                  00006800
             03 DEFFETPVA    PIC X.                                     00006810
             03 DEFFETPVC    PIC X.                                     00006820
             03 DEFFETPVP    PIC X.                                     00006830
             03 DEFFETPVH    PIC X.                                     00006840
             03 DEFFETPVV    PIC X.                                     00006850
             03 DEFFETPVU    PIC X.                                     00006860
             03 DEFFETPVO    PIC X(8).                                  00006870
           02 DFHMS13 OCCURS   7 TIMES .                                00006880
             03 FILLER       PIC X(2).                                  00006890
             03 MCORIGA      PIC X.                                     00006900
             03 MCORIGC PIC X.                                          00006910
             03 MCORIGP PIC X.                                          00006920
             03 MCORIGH PIC X.                                          00006930
             03 MCORIGV PIC X.                                          00006940
             03 MCORIGU PIC X.                                          00006950
             03 MCORIGO      PIC X.                                     00006960
           02 DFHMS14 OCCURS   7 TIMES .                                00006970
             03 FILLER       PIC X(2).                                  00006980
             03 MTAUXMBA     PIC X.                                     00006990
             03 MTAUXMBC     PIC X.                                     00007000
             03 MTAUXMBP     PIC X.                                     00007010
             03 MTAUXMBH     PIC X.                                     00007020
             03 MTAUXMBV     PIC X.                                     00007030
             03 MTAUXMBU     PIC X.                                     00007040
             03 MTAUXMBO     PIC X(7).                                  00007050
           02 DFHMS15 OCCURS   7 TIMES .                                00007060
             03 FILLER       PIC X(2).                                  00007070
             03 MPRIMEA      PIC X.                                     00007080
             03 MPRIMEC PIC X.                                          00007090
             03 MPRIMEP PIC X.                                          00007100
             03 MPRIMEH PIC X.                                          00007110
             03 MPRIMEV PIC X.                                          00007120
             03 MPRIMEU PIC X.                                          00007130
             03 MPRIMEO      PIC X(6).                                  00007140
           02 DFHMS16 OCCURS   7 TIMES .                                00007150
             03 FILLER       PIC X(2).                                  00007160
             03 MINDPRA      PIC X.                                     00007170
             03 MINDPRC PIC X.                                          00007180
             03 MINDPRP PIC X.                                          00007190
             03 MINDPRH PIC X.                                          00007200
             03 MINDPRV PIC X.                                          00007210
             03 MINDPRU PIC X.                                          00007220
             03 MINDPRO      PIC X.                                     00007230
           02 DFHMS17 OCCURS   7 TIMES .                                00007240
             03 FILLER       PIC X(2).                                  00007250
             03 DEFFETPA     PIC X.                                     00007260
             03 DEFFETPC     PIC X.                                     00007270
             03 DEFFETPP     PIC X.                                     00007280
             03 DEFFETPH     PIC X.                                     00007290
             03 DEFFETPV     PIC X.                                     00007300
             03 DEFFETPU     PIC X.                                     00007310
             03 DEFFETPO     PIC X(8).                                  00007320
           02 DFHMS18 OCCURS   7 TIMES .                                00007330
             03 FILLER       PIC X(2).                                  00007340
             03 MQSTOCKMA    PIC X.                                     00007350
             03 MQSTOCKMC    PIC X.                                     00007360
             03 MQSTOCKMP    PIC X.                                     00007370
             03 MQSTOCKMH    PIC X.                                     00007380
             03 MQSTOCKMV    PIC X.                                     00007390
             03 MQSTOCKMU    PIC X.                                     00007400
             03 MQSTOCKMO    PIC X(4).                                  00007410
           02 DFHMS19 OCCURS   7 TIMES .                                00007420
             03 FILLER       PIC X(2).                                  00007430
             03 MCOEFTRANA   PIC X.                                     00007440
             03 MCOEFTRANC   PIC X.                                     00007450
             03 MCOEFTRANP   PIC X.                                     00007460
             03 MCOEFTRANH   PIC X.                                     00007470
             03 MCOEFTRANV   PIC X.                                     00007480
             03 MCOEFTRANU   PIC X.                                     00007490
             03 MCOEFTRANO   PIC X(8).                                  00007500
           02 DFHMS20 OCCURS   7 TIMES .                                00007510
             03 FILLER       PIC X(2).                                  00007520
             03 MVARBTRANA   PIC X.                                     00007530
             03 MVARBTRANC   PIC X.                                     00007540
             03 MVARBTRANP   PIC X.                                     00007550
             03 MVARBTRANH   PIC X.                                     00007560
             03 MVARBTRANV   PIC X.                                     00007570
             03 MVARBTRANU   PIC X.                                     00007580
             03 MVARBTRANO   PIC X(5).                                  00007590
           02 DFHMS21 OCCURS   7 TIMES .                                00007600
             03 FILLER       PIC X(2).                                  00007610
             03 MVTE4SA      PIC X.                                     00007620
             03 MVTE4SC PIC X.                                          00007630
             03 MVTE4SP PIC X.                                          00007640
             03 MVTE4SH PIC X.                                          00007650
             03 MVTE4SV PIC X.                                          00007660
             03 MVTE4SU PIC X.                                          00007670
             03 MVTE4SO      PIC X(4).                                  00007680
           02 FILLER    PIC X(2).                                       00007690
           02 MLIBERRA  PIC X.                                          00007700
           02 MLIBERRC  PIC X.                                          00007710
           02 MLIBERRP  PIC X.                                          00007720
           02 MLIBERRH  PIC X.                                          00007730
           02 MLIBERRV  PIC X.                                          00007740
           02 MLIBERRU  PIC X.                                          00007750
           02 MLIBERRO  PIC X(78).                                      00007760
           02 FILLER    PIC X(2).                                       00007770
           02 MCODTRAA  PIC X.                                          00007780
           02 MCODTRAC  PIC X.                                          00007790
           02 MCODTRAP  PIC X.                                          00007800
           02 MCODTRAH  PIC X.                                          00007810
           02 MCODTRAV  PIC X.                                          00007820
           02 MCODTRAU  PIC X.                                          00007830
           02 MCODTRAO  PIC X(4).                                       00007840
           02 FILLER    PIC X(2).                                       00007850
           02 MCICSA    PIC X.                                          00007860
           02 MCICSC    PIC X.                                          00007870
           02 MCICSP    PIC X.                                          00007880
           02 MCICSH    PIC X.                                          00007890
           02 MCICSV    PIC X.                                          00007900
           02 MCICSU    PIC X.                                          00007910
           02 MCICSO    PIC X(5).                                       00007920
           02 FILLER    PIC X(2).                                       00007930
           02 MNETNAMA  PIC X.                                          00007940
           02 MNETNAMC  PIC X.                                          00007950
           02 MNETNAMP  PIC X.                                          00007960
           02 MNETNAMH  PIC X.                                          00007970
           02 MNETNAMV  PIC X.                                          00007980
           02 MNETNAMU  PIC X.                                          00007990
           02 MNETNAMO  PIC X(8).                                       00008000
           02 FILLER    PIC X(2).                                       00008010
           02 MSCREENA  PIC X.                                          00008020
           02 MSCREENC  PIC X.                                          00008030
           02 MSCREENP  PIC X.                                          00008040
           02 MSCREENH  PIC X.                                          00008050
           02 MSCREENV  PIC X.                                          00008060
           02 MSCREENU  PIC X.                                          00008070
           02 MSCREENO  PIC X(4).                                       00008080
                                                                                
