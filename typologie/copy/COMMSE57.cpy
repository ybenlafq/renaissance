      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
       01  Z-COMMAREA-MSE57.                                            00000020
           02 COMM-MSE57-ENTETE-RETOUR.                                 00000030
      *---- BOOLEEN TYPE D'ERREUR                                       00000040
              03 COMM-MSE57-TYPERR     PIC 9(01).                       00000050
                 88 MSE57-NO-ERROR       VALUE 0.                       00000060
                 88 MSE57-APPL-ERROR     VALUE 1.                       00000070
                 88 MSE57-DB2-ERROR      VALUE 2.                       00000080
              03 COMM-MSE57-FUNC-SQL   PIC X(08).                       00000090
              03 COMM-MSE57-TABLE-NAME PIC X(08).                       00000100
              03 COMM-MSE57-SQLCODE    PIC S9(04).                      00000110
              03 COMM-MSE57-POSITION   PIC  9(03).                      00000120
      *---- BOOLEEN CODE FONCTION                                       00000130
           02 COMM-MSE57-FONC      PIC 9(01).                           00000140
              88 MSE57-CTRL-DATA     VALUE 0.                           00000150
              88 MSE57-CONT56        VALUE 1.                           00000180
              88 MSE57-MAJ-DB2       VALUE 2.                           00000180
      *---- DONNEES EN ENTREE / SORTIE                                  00000190
           02 COMM-MSE57-ZINOUT.                                        00000200
              03 COMM-MSE57-CODESFONCTION   PIC X(12).                  00000210
              03 COMM-MSE57-WFONC           PIC X(03).                  00000220
              03 COMM-MSE57-NCSERV          PIC X(05).                  00000230
              03 COMM-MSE57-LCSERV          PIC X(20).                  00000240
              03 COMM-MSE57-COPER           PIC X(05).                  00000270
              03 COMM-MSE57-LOPER           PIC X(20).                  00000280
              03 COMM-MSE57-CFAM            PIC X(05).                  00000290
              03 COMM-MSE57-LCFAM           PIC X(20).                  00000300
              03 COMM-MSE57-LIBELLE         PIC X(50).                  00000320
              03 COMM-MSE57-AFFICH          PIC X(05).                  00000330
              03 COMM-MSE57-SEQFAM          PIC X(02).                  00000340
              03 COMM-MSE57-RDV             PIC X(03).                  00000350
              03 COMM-MSE57-LIEN            PIC X(07).                  00000360
              03 COMM-MSE57-LLIEN           PIC X(20).                  00000360
              03 COMM-MSE57-QTE             PIC X(03).                  00000370
              03 COMM-MSE57-INFO.                                       00000380
                05 COMM-MSE57-INFO1         PIC X(039).                 00000380
                05 COMM-MSE57-INFO2         PIC X(072).                 00000380
                05 COMM-MSE57-INFO3         PIC X(072).                 00000380
                05 COMM-MSE57-INFO4         PIC X(072).                 00000380
      *---- BOOLEENS POSITIONNEMENT ERREURS                             00000520
           02 COMM-MSE57-ERROR1    PIC 9(01).                           00000570
              88 LIBELLE-OK          VALUE 0.                           00000580
              88 LIBELLE-KO          VALUE 1.                           00000590
           02 COMM-MSE57-CDRET1    PIC X(04).                           00000600
           02 COMM-MSE57-ERROR2    PIC 9(01).                           00000610
              88 AFFICH-OK           VALUE 0.                           00000620
              88 AFFICH-KO           VALUE 1.                           00000630
           02 COMM-MSE57-CDRET2    PIC X(04).                           00000640
           02 COMM-MSE57-ERROR3    PIC 9(01).                           00000650
              88 SEQFAM-OK           VALUE 0.                           00000660
              88 SEQFAM-KO           VALUE 1.                           00000670
           02 COMM-MSE57-CDRET3    PIC X(04).                           00000680
           02 COMM-MSE57-ERROR4    PIC 9(01).                           00000690
              88 RDV-OK              VALUE 0.                           00000700
              88 RDV-KO              VALUE 1.                           00000710
           02 COMM-MSE57-CDRET4    PIC X(04).                           00000720
           02 COMM-MSE57-ERROR5    PIC 9(01).                           00000730
              88 LIEN-OK             VALUE 0.                           00000740
              88 LIEN-KO             VALUE 1.                           00000750
           02 COMM-MSE57-CDRET5    PIC X(04).                           00000760
           02 COMM-MSE57-ERROR6    PIC 9(01).                           00000770
              88 QTE-OK              VALUE 0.                           00000780
              88 QTE-KO              VALUE 1.                           00000790
           02 COMM-MSE57-CDRET6    PIC X(04).                           00000800
           02 COMM-MSE57-ERROR7    PIC 9(01).                           00000810
              88 INFO-OK             VALUE 0.                           00000820
              88 INFO-KO             VALUE 1.                           00000830
           02 COMM-MSE57-CDRET7    PIC X(04).                           00000840
                                                                        00000850
      *    02 COMM-MSE57-FILLER    PIC X(3655).                         00000870
                                                                                
