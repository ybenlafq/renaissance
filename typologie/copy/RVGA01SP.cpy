      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EMDCS SELECTION ARTICLES EMD           *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01SP.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01SP.                                                            
      *}                                                                        
           05  EMDCS-CTABLEG2    PIC X(15).                                     
           05  EMDCS-CTABLEG2-REDEF REDEFINES EMDCS-CTABLEG2.                   
               10  EMDCS-NSOCDEPO        PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  EMDCS-NSOCDEPO-N     REDEFINES EMDCS-NSOCDEPO                
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  EMDCS-NDEPOT          PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  EMDCS-NDEPOT-N       REDEFINES EMDCS-NDEPOT                  
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  EMDCS-NSEQ            PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  EMDCS-NSEQ-N         REDEFINES EMDCS-NSEQ                    
                                         PIC 9(02).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  EMDCS-WTABLEG     PIC X(80).                                     
           05  EMDCS-WTABLEG-REDEF  REDEFINES EMDCS-WTABLEG.                    
               10  EMDCS-CSELART         PIC X(05).                             
               10  EMDCS-HEURELIM        PIC X(04).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  EMDCS-HEURELIM-N     REDEFINES EMDCS-HEURELIM                
                                         PIC 9(04).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  EMDCS-LMMJVSD         PIC X(07).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01SP-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01SP-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EMDCS-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EMDCS-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EMDCS-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EMDCS-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
