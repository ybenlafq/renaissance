      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-SE00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.          00000020
      *--                                                                       
       01  COMM-SE00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                
      *}                                                                        
      *                                                                 00000030
       01  Z-COMMAREA.                                                  00000040
                                                                        00000050
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00000060
           02 FILLER-COM-AIDA      PIC X(100).                          00000070
                                                                        00000080
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00000090
           02 COMM-CICS-APPLID     PIC X(08).                           00000100
           02 COMM-CICS-NETNAM     PIC X(08).                           00000110
           02 COMM-CICS-TRANSA     PIC X(04).                           00000120
                                                                        00000130
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00000140
           02 COMM-DATE-SIECLE     PIC X(02).                           00000150
           02 COMM-DATE-ANNEE      PIC X(02).                           00000160
           02 COMM-DATE-MOIS       PIC X(02).                           00000170
           02 COMM-DATE-JOUR       PIC 99.                              00000180
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00000190
           02 COMM-DATE-QNTA       PIC 999.                             00000200
           02 COMM-DATE-QNT0       PIC 99999.                           00000210
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00000220
           02 COMM-DATE-BISX       PIC 9.                               00000230
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00000240
           02 COMM-DATE-JSM        PIC 9.                               00000250
      *   LIBELLES DU JOUR COURT - LONG                                 00000260
           02 COMM-DATE-JSM-LC     PIC X(03).                           00000270
           02 COMM-DATE-JSM-LL     PIC X(08).                           00000280
      *   LIBELLES DU MOIS COURT - LONG                                 00000290
           02 COMM-DATE-MOIS-LC    PIC X(03).                           00000300
           02 COMM-DATE-MOIS-LL    PIC X(08).                           00000310
      *   DIFFERENTES FORMES DE DATE                                    00000320
           02 COMM-DATE-SSAAMMJJ   PIC X(08).                           00000330
           02 COMM-DATE-AAMMJJ     PIC X(06).                           00000340
           02 COMM-DATE-JJMMSSAA   PIC X(08).                           00000350
           02 COMM-DATE-JJMMAA     PIC X(06).                           00000360
           02 COMM-DATE-JJ-MM-AA   PIC X(08).                           00000370
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00000380
      *   DATE DU LENDEMAIN                                             00000390
           02 COMM-DATE-DEMAIN-SAMJ PIC X(08).                          00000400
           02 COMM-DATE-DEMAIN-JMA PIC X(08).                           00000410
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00000420
           02 COMM-DATE-WEEK.                                           00000430
              05 COMM-DATE-SEMSS   PIC 99.                              00000440
              05 COMM-DATE-SEMAA   PIC 99.                              00000450
              05 COMM-DATE-SEMNU   PIC 99.                              00000460
           02 COMM-DATE-FILLER     PIC X(08).                           00000470
      * ZONES RESERVEES TRAITEMENT DU SWAP                              00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00000490
      *                                                                         
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
                                                                        00000500
      *}                                                                        
      * ZONES RESERVEES APPLICATIVES ------------- LG : 3874            00000510
           02 COMM-SE00-APPLI.                                          00000520
              03 COMM-ENTETE.                                           00000530
                 05 COMM-CODLANG            PIC X(02).                  00000540
                 05 COMM-CODPIC             PIC X(02).                  00000550
                 05 COMM-CODESFONCTION      PIC X(12).                  00000560
                 05 COMM-DROIT              PIC X(01).                  00000570
              03 COMM-00-ENTETE.                                        00000580
                 05 COMM-00-NCSERV          PIC X(05).                  00000590
                 05 COMM-00-COPER           PIC X(05).                  00000600
                 05 COMM-00-LOPER           PIC X(20).                  00000610
                 05 COMM-00-CFAM            PIC X(05).                  00000620
                 05 COMM-00-LCFAM           PIC X(20).                  00000630
                 05 COMM-00-CASSOR          PIC X(05).                  00000640
                 05 COMM-00-LASSOR          PIC X(20).                  00000650
                 05 COMM-00-TYPSERVM        PIC X(01).                  00000660
                 05 COMM-00-TYPSERVO        PIC X(01).                  00000670
                 05 COMM-00-TYPSERVC        PIC X(01).                  00000680
                 05 COMM-00-WFONC           PIC X(03).                  00000690
              03 COMM-01-ENTETE.                                        00000700
                 05 COMM-01-INDTS           PIC S9(05) COMP-3.          00000710
                 05 COMM-01-INDMAX          PIC S9(05) COMP-3.          00000720
                 05 COMM-01-NUMPAGE         PIC  9(03).                 00000730
                 05 COMM-01-NBRACT          PIC  9.                     00000740
                 05 COMM-01-NBRCRE          PIC  9.                     00000750
                 05 COMM-01-TYPSERV         PIC  X OCCURS 10.           00000760
              03 COMM-50-ENTETE.                                        00000770
                 05 COMM-50-SERV-ENC        PIC  9.                     00000780
                 05 COMM-50-SERV-TOT        PIC  9.                     00000790
                 05 COMM-50-TAB1.                                       00000800
                    10 COMM-50-POSTE-TAB1  OCCURS 2  PIC 9.             00000810
                 05 COMM-50-TAB2.                                       00000820
                    10 COMM-50-POSTE-TAB2  OCCURS 2.                    00000830
                       15 COMM-50-FONCTION        PIC X(03).            00000840
                       15 COMM-50-NCSERV          PIC X(05).            00000850
                       15 COMM-50-NCSERV-2        PIC X(05).            00000860
                       15 COMM-50-LCSERV          PIC X(20).            00000870
                       15 COMM-50-COPER           PIC X(05).            00000880
                       15 COMM-50-LOPER           PIC X(20).            00000890
                       15 COMM-50-CFAM            PIC X(05).            00000900
                       15 COMM-50-CFAM-AV         PIC X(05).            00000910
                       15 COMM-50-LCFAM           PIC X(20).            00000920
                       15 COMM-50-LCFAM-AV        PIC X(20).            00000930
                       15 COMM-50-CASSOR          PIC X(05).            00000940
                       15 COMM-50-LASSOR          PIC X(20).            00000950
                       15 COMM-50-CSTATUT         PIC X(01).            00000960
                       15 COMM-50-OBLIG2          PIC X(01).            00000970
                       15 COMM-50-OBLIG4          PIC X(01).            00000980
                       15 COMM-50-OBLIG5          PIC X(01).            00000990
PV0408                 15 COMM-50-OBLIG6          PIC X(01).            00001000
211208                 15 COMM-50-OBLIG7          PIC X(01).            00001010
                       15 COMM-50-CPT-MAJ1        PIC S9(03) COMP-3.    00001020
                       15 COMM-50-CPT-MAJ2        PIC S9(03) COMP-3.    00001030
                       15 COMM-50-CPT-MAJ3        PIC S9(03) COMP-3.    00001040
                       15 COMM-50-CPT-MAJ4        PIC S9(03) COMP-3.    00001050
                       15 COMM-50-CPT-MAJ5        PIC S9(03) COMP-3.    00001060
PV0408                 15 COMM-50-CPT-MAJ6        PIC S9(03) COMP-3.    00001070
211208                 15 COMM-50-CPT-MAJ7        PIC S9(03) COMP-3.    00001080
                       15 COMM-50-CPT-ERR1        PIC S9(03) COMP-3.    00001090
                       15 COMM-50-CPT-ERR2        PIC S9(03) COMP-3.    00001100
                       15 COMM-50-CPT-ERR3        PIC S9(03) COMP-3.    00001110
                       15 COMM-50-CPT-ERR4        PIC S9(03) COMP-3.    00001120
                       15 COMM-50-CPT-ERR5        PIC S9(03) COMP-3.    00001130
PV0408                 15 COMM-50-CPT-ERR6        PIC S9(03) COMP-3.    00001140
211208                 15 COMM-50-CPT-ERR7        PIC S9(03) COMP-3.    00001150
              03 COMM-51-PAGE.                                          00001160
                 05 COMM-51-NBRATT           PIC  999.                  00001170
FV1              05 COMM-51-NBRATT-VISIBLE   PIC  999.                  00001180
                 05 COMM-51-NUMPAGE          PIC  999  OCCURS 5.        00001190
                 05 COMM-51-LIGNE.                                      00001200
      * ZONES COMMUNES AUX DEUX SERVICES EN MEMOIRE :                   00001210
      *  LES LIBELLES DES CODES GESTION POSSIBLES                       00001220
FV1   *            10 COMM-51-LCODGES-AP  OCCURS 30 TIMES.              00001230
FV1   *            10 COMM-51-LCODGES-AP  OCCURS 20 TIMES.              00001240
      *                15 COMM-51-LGEST-AP PIC X(30).                   00001250
      *                15 COMM-51-CGEST-AP PIC X(05).                   00001260
FV1   *                15 COMM-51-TYPE     PIC X.                       00001270
FV1   *                15 COMM-51-TABLE    PIC X(6).                    00001280
                   10 COMM-51-POSTE        OCCURS 2.                    00001290
FVM                   15 COMM-51-CTYPINN-SAI PIC X.                     00001300
FV2   *               15 COMM-51-LCODGES-AP  OCCURS 30 TIMES.           00001310
 !    *                                                                 00001320
 !    *                  20 COMM-51-LGEST-AP PIC X(30).                 00001330
 !    *                  20 COMM-51-CGEST-AP PIC X(05).                 00001340
 !    *                  20 COMM-51-TYPE     PIC X.                     00001350
 !    *                  20 COMM-51-TABLE    PIC X(6).                  00001360
      * ZONES NON SAISISSABLES D'UN SERVICE EN CRE / MAJ :              00001370
                      15 COMM-51-NCSERV-AP      PIC X(05).              00001380
                      15 COMM-51-DCREATE-AP     PIC X(08).              00001390
                      15 COMM-51-DMAJ-AP        PIC X(08).              00001400
                      15 COMM-51-CASSORTC-AP    PIC X(05).              00001410
                      15 COMM-51-DATEC-AP       PIC X(08).              00001420
      * ZONES MODIFIABLES EN MAJ / CRE :                                00001430
      *   LES CHAMPS SUFFIXES PAR -AV CONTIENNENT LES VAL INITIALES     00001440
      *   LES CHAMPS SUFFIXES PAR -AP CONTIENNENT LES NEW VALEURS       00001450
                      15 COMM-51-DATA-AV.                               00001460
                         20 COMM-51-LCSERVL-AV  PIC X(20).              00001470
                         20 COMM-51-LCSERVC-AV  PIC X(05).              00001480
                         20 COMM-51-NORDRE-AV   PIC X(03).              00001490
                         20 COMM-51-COPER-AV    PIC X(05).              00001500
                         20 COMM-51-LCOPER-AV   PIC X(20).              00001510
                         20 COMM-51-CTVA-AV     PIC X(01).              00001520
                         20 COMM-51-NTVA-AV     PIC S999V99.            00001530
                         20 COMM-51-LCTVA-AV    PIC X(20).              00001540
                         20 COMM-51-CASSORTF-AV PIC X(05).              00001550
                         20 COMM-51-DATEF-AV    PIC X(08).              00001560
                         20 COMM-51-CTYPSERV-AV PIC X(01).              00001570
                         20 COMM-51-CTYP-AV     PIC X(05).              00001580
                         20 COMM-51-LTYP-AV     PIC X(20).              00001590
                         20 COMM-51-CHEF-AV     PIC X(05).              00001600
                         20 COMM-51-LCHEF-AV    PIC X(20).              00001610
FV-3.2                   20 FILLER              PIC X(13).              00001620
FV-3.2*                  20 COMM-51-EAN-AV      PIC X(13).              00001630
FVM                      20 COMM-51-CTYPINN-AV  PIC X(1).               00001640
FV1 *****************    20 COMM-51-VCODGES-AV  OCCURS 20 TIMES.        00001650
FV1 *****************       25 COMM-51-VGEST-AV PIC X.                  00001660
FV1   * FV2              20 COMM-51-FCODGES-AV  OCCURS 30 TIMES.        00001670
FV1   * FV2                 25 COMM-51-FGEST-AV PIC X.                  00001680
FV1   * FV2              20 COMM-51-VCODGES-AV  OCCURS 30 TIMES.        00001690
FV1   * FV2                 25 COMM-51-VGEST-AV PIC X(20).              00001700
                      15 COMM-51-DATA-AP.                               00001710
                         20 COMM-51-LCSERVL-AP  PIC X(20).              00001720
                         20 COMM-51-LCSERVC-AP  PIC X(05).              00001730
                         20 COMM-51-NORDRE-AP   PIC X(03).              00001740
                         20 COMM-51-COPER-AP    PIC X(05).              00001750
                         20 COMM-51-LCOPER-AP   PIC X(20).              00001760
                         20 COMM-51-CTVA-AP     PIC X(01).              00001770
                         20 COMM-51-NTVA-AP     PIC S9(3)V99.           00001780
                         20 COMM-51-LCTVA-AP    PIC X(20).              00001790
                         20 COMM-51-CASSORTF-AP PIC X(05).              00001800
                         20 COMM-51-DATEF-AP    PIC X(08).              00001810
                         20 COMM-51-CTYPSERV-AP PIC X(01).              00001820
                         20 COMM-51-CTYP-AP     PIC X(05).              00001830
                         20 COMM-51-LTYP-AP     PIC X(20).              00001840
                         20 COMM-51-CHEF-AP     PIC X(05).              00001850
                         20 COMM-51-LCHEF-AP    PIC X(20).              00001860
FV-3.2                   20 FILLER              PIC X(13).              00001870
FV-3.2*                  20 COMM-51-EAN-AP      PIC X(13).              00001880
FVM                      20 COMM-51-CTYPINN-AP  PIC X(1).               00001890
FV1 *****************    20 COMM-51-VCODGES-AP  OCCURS 20 TIMES.        00001900
FV1 *****************       25 COMM-51-VGEST-AP PIC X.                  00001910
FV1   *FV2               20 COMM-51-FCODGES-AP  OCCURS 30 TIMES.        00001920
FV1   *FV2                  25 COMM-51-FGEST-AP PIC X.                  00001930
FV1   *FV2               20 COMM-51-VCODGES-AP  OCCURS 30 TIMES.        00001940
FV1   *FV2                  25 COMM-51-VGEST-AP PIC X(20).              00001950
FV1   *               15    COMM-51-INOTR       OCCURS 30.              00001960
FV1   *                     25 COMM-51-VISIBLE  PIC X.                  00001970
FV1   *                     25 COMM-51-OBLIG    PIC X.                  00001980
FV1   *                     25 COMM-51-SAISIE   PIC X(5).               00001990
FV1   *                     25 COMM-51-VALDFAUT PIC X(20).              00002000
      *--- FLAG DE CONTROLE DE SAISIE + CODE ERREUR ASSOCIE             00002010
      *    VALEURS PERMISES : A CONTROLER    VALUE SPACES               00002020
      *                       SAISIE OK      VALUE '0'                  00002030
      *                       SAISIE KO      VALUE '1'                  00002040
                      15 COMM-51-FLAG-CTRL.                             00002050
                         20 COMM-51-FLAG-COPER     PIC X(01).           00002060
                         20 COMM-51-CDRET-COPER    PIC X(04).           00002070
                         20 COMM-51-FLAG-CFAM      PIC X(01).           00002080
                         20 COMM-51-CDRET-CFAM     PIC X(04).           00002090
                         20 COMM-51-FLAG-TVA       PIC X(01).           00002100
                         20 COMM-51-CDRET-TVA      PIC X(04).           00002110
                         20 COMM-51-FLAG-ASSORT    PIC X(01).           00002120
                         20 COMM-51-CDRET-ASSORT   PIC X(04).           00002130
                         20 COMM-51-FLAG-CTYP      PIC X(01).           00002140
                         20 COMM-51-CDRET-CTYP     PIC X(04).           00002150
                         20 COMM-51-FLAG-CHEF      PIC X(01).           00002160
                         20 COMM-51-CDRET-CHEF     PIC X(04).           00002170
FV-3.2                   20 FILLER                 PIC X(05).           00002180
FV-3.2*                  20 COMM-51-FLAG-EAN       PIC X(01).           00002190
FV-3.2*                  20 COMM-51-CDRET-EAN      PIC X(04).           00002200
FVM                      20 COMM-51-FLAG-CTYPINN   PIC X(01).           00002210
FVM                      20 COMM-51-CDRET-CTYPINN  PIC X(04).           00002220
                         20 COMM-51-FLAG-CTYPSERV  PIC X(01).           00002230
                         20 COMM-51-CDRET-CTYPSERV PIC X(04).           00002240
      *--- FLAG D'AUTORISATION                                          00002250
      *    VALEURS PERMISES : CHAMP SAISISSABLE      : VALUE 'O'        00002260
      *                     : CHAMP  NON SAISISSABLE : VALUE 'N'        00002270
FVM   *               15 COMM-51-CTYPINN-SAI PIC X.                     00002280
                      15 COMM-51-FLAG-AFF.                              00002290
                         20 COMM-51-MLCSERV-AFF PIC X.                  00002300
                         20 COMM-51-MNORDRE-AFF PIC X.                  00002310
                         20 COMM-51-COPER-AFF   PIC X.                  00002320
                         20 COMM-51-CFAM-AFF    PIC X.                  00002330
                         20 COMM-51-MCTYPSER-AFF PIC X.                 00002340
                         20 COMM-51-MCTXTVA-AFF PIC X.                  00002350
                         20 COMM-51-MCHEF-AFF   PIC X.                  00002360
FV-3.2                   20 FILLER              PIC X.                  00002370
FV-3.2*                  20 COMM-51-EAN-AFF     PIC X.                  00002380
FVM                      20 COMM-51-CTYPINN-AFF PIC X.                  00002390
                         20 COMM-51-MCASSOR-AFF PIC X.                  00002400
                         20 COMM-54-MCSIGNE-AFF PIC X.                  00002410
                         20 COMM-51-MCTYP-AFF   PIC X.                  00002420
FV1  ***************     20 COMM-51-CODGES-AFF  OCCURS 20 TIMES.        00002430
FV1   * FV2              20 COMM-51-CODGES-AFF  OCCURS 30 TIMES.        00002440
      * FV2                 25 COMM-51-CGEST-AFF PIC X.                 00002450
              03 COMM-52-ENTETE.                                        00002460
                 05 COMM-52-TAB-IND-TS  OCCURS 2.                       00002470
                    10 COMM-52-INDTS          PIC S9(03) COMP-3.        00002480
                    10 COMM-52-INDMAX         PIC S9(03) COMP-3.        00002490
                    10 COMM-52-NUMPAGE        PIC  9(03).               00002500
              03 COMM-53-ENTETE.                                        00002510
                 05 COMM-53-TAB-IND-TS  OCCURS 2.                       00002520
                    10 COMM-53-INDTS          PIC S9(03) COMP-3.        00002530
                    10 COMM-53-INDMAX         PIC S9(03) COMP-3.        00002540
                    10 COMM-53-INDMAX-AV      PIC S9(03) COMP-3.        00002550
                    10 COMM-53-NUMPAGE        PIC  9(03).               00002560
              03 COMM-54-ENTETE.                                        00002570
                 05 COMM-54-TAB-IND-TS  OCCURS 2.                       00002580
                    10 COMM-54-CSIGNE-AV      PIC X(01).                00002590
                    10 COMM-54-CSIGNE-AP      PIC X(01).                00002600
                    10 COMM-54-INDTS          PIC S9(03) COMP-3.        00002610
                    10 COMM-54-INDMAX         PIC S9(03) COMP-3.        00002620
                    10 COMM-54-NUMPAGE        PIC  9(03).               00002630
              03 COMM-55-ENTETE.                                        00002640
                 05 COMM-55-TAB-IND-TS  OCCURS 2.                       00002650
                    10 COMM-55-INDTS          PIC S9(03) COMP-3.        00002660
                    10 COMM-55-INDMAX         PIC S9(03) COMP-3.        00002670
                    10 COMM-55-INDMAX-AV      PIC S9(03) COMP-3.        00002680
                    10 COMM-55-NUMPAGE        PIC  9(03).               00002690
                    10 COMM-55-NENTCDE        PIC  X(05).               00002700
 "    *       03 COMM-56-ENTETE.                                        00002710
 "    *          05 COMM-56-POSTE       OCCURS 2.                       00002720
 "    * ZONES MODIFIABLES EN MAJ / CRE :                                00002730
 "    *   LES CHAMPS SUFFIXES PAR -AV CONTIENNENT LES VAL INITIALES     00002740
 "    *   LES CHAMPS SUFFIXES PAR -AP CONTIENNENT LES NEW VALEURS       00002750
 "    *             10 COMM-56-DATA-AV.                                 00002760
 "    *                15 COMM-56-CTYPINN-AV  PIC X(01).                00002770
 "    *                15 COMM-56-FQTEMUL-AV  PIC X(01).                00002780
 "    *                15 COMM-56-FREMISE-AV  PIC X(01).                00002790
 "    *                15 COMM-56-FTICKET-AV  PIC X(01).                00002800
 "    *                15 COMM-56-FGEO-AV     PIC X(01).                00002810
 "    *                15 COMM-56-FASSOC-AV   PIC X(01).                00002820
 "    *                15 COMM-56-MAXASSO-AV  PIC S9(05) COMP-3.        00002830
 "    *                15 COMM-56-CLTREQ-AV   PIC X(01).                00002840
 "    *                15 COMM-56-CLIREQ-AV   PIC X(01).                00002850
 "    *                15 COMM-56-SERCTRA-AV  PIC X(01).                00002860
 "    *                15 COMM-56-CCONTRA-AV  PIC X(05).                00002870
 "    *                15 COMM-56-LCCONTR-AV  PIC X(20).                00002880
 "    *             10 COMM-56-DATA-AP.                                 00002890
 "    *                15 COMM-56-CTYPINN-AP  PIC X(01).                00002900
 "    *                15 COMM-56-FQTEMUL-AP  PIC X(01).                00002910
 "    *                15 COMM-56-FREMISE-AP  PIC X(01).                00002920
 "    *                15 COMM-56-FTICKET-AP  PIC X(01).                00002930
 "    *                15 COMM-56-FGEO-AP     PIC X(01).                00002940
 "    *                15 COMM-56-FASSOC-AP   PIC X(01).                00002950
 "    *                15 COMM-56-MAXASSO-AP  PIC S9(05) COMP-3.        00002960
 "    *                15 COMM-56-CLTREQ-AP   PIC X(01).                00002970
 "    *                15 COMM-56-CLIREQ-AP   PIC X(01).                00002980
 "    *                15 COMM-56-SERCTRA-AP  PIC X(01).                00002990
 "    *                15 COMM-56-CCONTRA-AP  PIC X(05).                00003000
 "    *                15 COMM-56-LCCONTR-AP  PIC X(20).                00003010
 "    *--- FLAG DE CONTROLE DE SAISIE + CODE ERREUR ASSOCIE             00003020
 "    *    VALEURS PERMISES : A CONTROLER    VALUE SPACES               00003030
 "    *                       SAISIE OK      VALUE '0'                  00003040
 "    *                       SAISIE KO      VALUE '1'                  00003050
 "    *            10 COMM-56-FLAG-CTRL.                                00003060
 "    *               15 COMM-56-FLAG-CTYPINN   PIC X(01).              00003070
 "    *               15 COMM-56-CDRET-CTYPINN  PIC X(04).              00003080
 "    *               15 COMM-56-FLAG-FQTEMUL   PIC X(01).              00003090
 "    *               15 COMM-56-CDRET-FQTEMUL  PIC X(04).              00003100
 "    *               15 COMM-56-FLAG-FREMISE   PIC X(01).              00003110
 "    *               15 COMM-56-CDRET-FREMISE  PIC X(04).              00003120
 "    *               15 COMM-56-FLAG-FTICKET   PIC X(01).              00003130
 "    *               15 COMM-56-CDRET-FTICKET  PIC X(04).              00003140
 "    *               15 COMM-56-FLAG-FGEO      PIC X(01).              00003150
 "    *               15 COMM-56-CDRET-FGEO     PIC X(04).              00003160
 "    *               15 COMM-56-FLAG-FASSOC    PIC X(01).              00003170
 "    *               15 COMM-56-CDRET-FASSOC   PIC X(04).              00003180
 "    *               15 COMM-56-FLAG-MAXASSO   PIC X(01).              00003190
 "    *               15 COMM-56-CDRET-MAXASSO  PIC X(04).              00003200
 "    *               15 COMM-56-FLAG-CLTREQ    PIC X(01).              00003210
 "    *               15 COMM-56-CDRET-CLTREQ   PIC X(04).              00003220
 "    *               15 COMM-56-FLAG-SERCTRA   PIC X(01).              00003230
 "    *               15 COMM-56-CDRET-SERCTRA  PIC X(04).              00003240
 "    *               15 COMM-56-FLAG-CCONTRA   PIC X(01).              00003250
 "    *               15 COMM-56-CDRET-CCONTRA  PIC X(04).              00003260
 "    *--- FLAG D'AUTORISATION                                          00003270
 "    *    VALEURS PERMISES : CHAMP SAISISSABLE      : VALUE 'O'        00003280
 "    *                     : CHAMP  NON SAISISSABLE : VALUE 'N'        00003290
 "    *            10 COMM-56-FLAG-AFF.                                 00003300
 "    *               15 COMM-56-CTYPINN-AFF PIC X.                     00003310
 "    *               15 COMM-56-FQTEMUL-AFF PIC X.                     00003320
 "    *               15 COMM-56-FREMISE-AFF PIC X.                     00003330
 "    *               15 COMM-56-FTICKET-AFF PIC X.                     00003340
 "    *               15 COMM-56-FGEO-AFF    PIC X.                     00003350
 "    *               15 COMM-56-FASSOC-AFF  PIC X.                     00003360
 "    *               15 COMM-56-MAXASSO-AFF PIC X.                     00003370
 "    *               15 COMM-56-CLTREQ-AFF  PIC X.                     00003380
 "    *               15 COMM-56-SERCTRA-AFF PIC X.                     00003390
FV1   *               15 COMM-56-CCONTRA-AFF PIC X.                     00003400
211208        03 COMM-57-ENTETE.                                        00003410
211208           05 COMM-57-POSTE       OCCURS 2.                       00003420
211208* ZONES MODIFIABLES EN MAJ / CRE :                                00003430
211208*   LES CHAMPS SUFFIXES PAR -AV CONTIENNENT LES VAL INITIALES     00003440
211208*   LES CHAMPS SUFFIXES PAR -AP CONTIENNENT LES NEW VALEURS       00003450
211208              10 COMM-57-DATA-AV.                                 00003460
211208                 15 COMM-57-LIBELLE-AV  PIC X(50).                00003470
211208                 15 COMM-57-AFFICH-AV   PIC X(05).                00003480
211208                 15 COMM-57-SEQFAM-AV   PIC X(02).                00003490
211208                 15 COMM-57-RDV-AV      PIC X(03).                00003500
211208                 15 COMM-57-LIEN-AV     PIC X(07).                00003510
211208                 15 COMM-57-QTE-AV      PIC 999.                  00003520
211208                 15 COMM-57-INFO1-AV    PIC X(39).                00003530
211208                 15 COMM-57-INFO2-AV    PIC X(72).                00003540
211208                 15 COMM-57-INFO3-AV    PIC X(72).                00003550
211208                 15 COMM-57-INFO4-AV    PIC X(72).                00003560
211208              10 COMM-57-DATA-AP.                                 00003570
211208                 15 COMM-57-LIBELLE-AP  PIC X(50).                00003580
211208                 15 COMM-57-AFFICH-AP   PIC X(05).                00003590
211208                 15 COMM-57-SEQFAM-AP   PIC X(02).                00003600
211208                 15 COMM-57-RDV-AP      PIC X(03).                00003610
211208                 15 COMM-57-LIEN-AP     PIC X(07).                00003620
211208                 15 COMM-57-QTE-AP      PIC 999.                  00003630
211208                 15 COMM-57-INFO1-AP    PIC X(39).                00003640
211208                 15 COMM-57-INFO2-AP    PIC X(72).                00003650
211208                 15 COMM-57-INFO3-AP    PIC X(72).                00003660
211208                 15 COMM-57-INFO4-AP    PIC X(72).                00003670
211208*--- FLAG DE CONTROLE DE SAISIE + CODE ERREUR ASSOCIE             00003680
211208*    VALEURS PERMISES : A CONTROLER    VALUE SPACES               00003690
211208*                       SAISIE OK      VALUE '0'                  00003700
211208*                       SAISIE KO      VALUE '1'                  00003710
211208             10 COMM-57-FLAG-CTRL.                                00003720
211208                15 COMM-57-FLAG-LIBELLE   PIC X(01).              00003730
211208                15 COMM-57-CDRET-LIBELLE  PIC X(04).              00003740
211208                15 COMM-57-FLAG-AFFICH    PIC X(01).              00003750
211208                15 COMM-57-CDRET-AFFICH   PIC X(04).              00003760
211208                15 COMM-57-FLAG-SEQFAM    PIC X(01).              00003770
211208                15 COMM-57-CDRET-SEQFAM   PIC X(04).              00003780
211208                15 COMM-57-FLAG-RDV       PIC X(01).              00003790
211208                15 COMM-57-CDRET-RDV      PIC X(04).              00003800
211208                15 COMM-57-FLAG-LIEN      PIC X(01).              00003810
211208                15 COMM-57-CDRET-LIEN     PIC X(04).              00003820
211208                15 COMM-57-FLAG-QTE       PIC X(01).              00003830
211208                15 COMM-57-CDRET-QTE      PIC X(04).              00003840
211208                15 COMM-57-FLAG-INFO1     PIC X(01).              00003850
211208                15 COMM-57-CDRET-INFO1    PIC X(04).              00003860
211208                15 COMM-57-FLAG-INFO2     PIC X(01).              00003870
211208                15 COMM-57-CDRET-INFO2    PIC X(04).              00003880
211208                15 COMM-57-FLAG-INFO3     PIC X(01).              00003890
211208                15 COMM-57-CDRET-INFO3    PIC X(04).              00003900
211208                15 COMM-57-FLAG-INFO4     PIC X(01).              00003910
211208                15 COMM-57-CDRET-INFO4    PIC X(04).              00003920
211208*--- FLAG D'AUTORISATION                                          00003930
211208*    VALEURS PERMISES : CHAMP SAISISSABLE      : VALUE 'O'        00003940
211208*                     : CHAMP  NON SAISISSABLE : VALUE 'N'        00003950
211208             10 COMM-57-FLAG-AFF.                                 00003960
211208                15 COMM-57-LIBELLE-AFF PIC X.                     00003970
211208                15 COMM-57-AFFICH-AFF  PIC X.                     00003980
211208                15 COMM-57-SEQFAM-AFF  PIC X.                     00003990
211208                15 COMM-57-RDV-AFF     PIC X.                     00004000
211208                15 COMM-57-LIEN-AFF    PIC X.                     00004010
211208                15 COMM-57-QTE-AFF     PIC X.                     00004020
211208                15 COMM-57-INFO1-AFF   PIC X.                     00004030
211208                15 COMM-57-INFO2-AFF   PIC X.                     00004040
211208                15 COMM-57-INFO3-AFF   PIC X.                     00004050
211208                15 COMM-57-INFO4-AFF   PIC X.                     00004060
FV1              05 FILLER                   PIC X(1065).               00004070
      *    02 FILLER-COMM                     PIC X(1762).              00004080
AD1   *    02 FILLER-COMM                     PIC X(1730).              00004090
PV0408*    02 FILLER-COMM                     PIC X(1449).              00004100
211208*RG0102 FILLER-COMM                     PIC X(0019).              00004110
FVM   *    02 FILLER-COMM                     PIC X(0017).              00004120
FVM   *    02 FILLER-COMM                     PIC X(0001).              00004130
                                                                                
