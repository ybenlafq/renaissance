      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE QCGAT LIEUX  RTGA10 DE TRAITEMENT      *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01PU.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01PU.                                                            
      *}                                                                        
           05  QCGAT-CTABLEG2    PIC X(15).                                     
           05  QCGAT-CTABLEG2-REDEF REDEFINES QCGAT-CTABLEG2.                   
               10  QCGAT-NCISOC          PIC X(03).                             
               10  QCGAT-CTYPLIEU        PIC X(01).                             
               10  QCGAT-CTYPSOC         PIC X(03).                             
           05  QCGAT-WTABLEG     PIC X(80).                                     
           05  QCGAT-WTABLEG-REDEF  REDEFINES QCGAT-WTABLEG.                    
               10  QCGAT-WACTIF          PIC X(01).                             
               10  QCGAT-CENTITE         PIC X(05).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01PU-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01PU-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QCGAT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  QCGAT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QCGAT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  QCGAT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
