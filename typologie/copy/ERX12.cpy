      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERX12   RX12                                               00000020
      ***************************************************************** 00000030
       01   ERX12I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCONCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCONCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCONCI   PIC X(4).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNMAGI    PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMAGL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLMAGF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLMAGI    PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENSEIL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNENSEIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNENSEIF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNENSEII  PIC X(15).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNZONPRIL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MNZONPRIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNZONPRIF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNZONPRII      PIC X(2).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLZONPRIL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MLZONPRIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLZONPRIF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLZONPRII      PIC X(20).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNEMPLL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNEMPLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNEMPLF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNEMPLI   PIC X(15).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTYPMAGL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MNTYPMAGL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNTYPMAGF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNTYPMAGI      PIC X(2).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPMAGL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MLTYPMAGL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLTYPMAGF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLTYPMAGI      PIC X(20).                                 00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 ML1ADRESL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 ML1ADRESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 ML1ADRESF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 ML1ADRESI      PIC X(32).                                 00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 ML2ADRESL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 ML2ADRESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 ML2ADRESF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 ML2ADRESI      PIC X(32).                                 00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODPOSTL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MCODPOSTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCODPOSTF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCODPOSTI      PIC X(5).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVILLEL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MNVILLEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVILLEF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNVILLEI  PIC X(18).                                      00000730
           02 MNRAYOND OCCURS   15 TIMES .                              00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNRAYONL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MNRAYONL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNRAYONF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MNRAYONI     PIC X(5).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MLIBERRI  PIC X(78).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCODTRAI  PIC X(4).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCICSI    PIC X(5).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MNETNAMI  PIC X(8).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MSCREENI  PIC X(4).                                       00000980
      ***************************************************************** 00000990
      * SDF: ERX12   RX12                                               00001000
      ***************************************************************** 00001010
       01   ERX12O REDEFINES ERX12I.                                    00001020
           02 FILLER    PIC X(12).                                      00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MDATJOUA  PIC X.                                          00001050
           02 MDATJOUC  PIC X.                                          00001060
           02 MDATJOUP  PIC X.                                          00001070
           02 MDATJOUH  PIC X.                                          00001080
           02 MDATJOUV  PIC X.                                          00001090
           02 MDATJOUO  PIC X(10).                                      00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MTIMJOUA  PIC X.                                          00001120
           02 MTIMJOUC  PIC X.                                          00001130
           02 MTIMJOUP  PIC X.                                          00001140
           02 MTIMJOUH  PIC X.                                          00001150
           02 MTIMJOUV  PIC X.                                          00001160
           02 MTIMJOUO  PIC X(5).                                       00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MWFONCA   PIC X.                                          00001190
           02 MWFONCC   PIC X.                                          00001200
           02 MWFONCP   PIC X.                                          00001210
           02 MWFONCH   PIC X.                                          00001220
           02 MWFONCV   PIC X.                                          00001230
           02 MWFONCO   PIC X(3).                                       00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MNSOCA    PIC X.                                          00001260
           02 MNSOCC    PIC X.                                          00001270
           02 MNSOCP    PIC X.                                          00001280
           02 MNSOCH    PIC X.                                          00001290
           02 MNSOCV    PIC X.                                          00001300
           02 MNSOCO    PIC X(3).                                       00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MNCONCA   PIC X.                                          00001330
           02 MNCONCC   PIC X.                                          00001340
           02 MNCONCP   PIC X.                                          00001350
           02 MNCONCH   PIC X.                                          00001360
           02 MNCONCV   PIC X.                                          00001370
           02 MNCONCO   PIC X(4).                                       00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MNMAGA    PIC X.                                          00001400
           02 MNMAGC    PIC X.                                          00001410
           02 MNMAGP    PIC X.                                          00001420
           02 MNMAGH    PIC X.                                          00001430
           02 MNMAGV    PIC X.                                          00001440
           02 MNMAGO    PIC X(3).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MLMAGA    PIC X.                                          00001470
           02 MLMAGC    PIC X.                                          00001480
           02 MLMAGP    PIC X.                                          00001490
           02 MLMAGH    PIC X.                                          00001500
           02 MLMAGV    PIC X.                                          00001510
           02 MLMAGO    PIC X(20).                                      00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MNENSEIA  PIC X.                                          00001540
           02 MNENSEIC  PIC X.                                          00001550
           02 MNENSEIP  PIC X.                                          00001560
           02 MNENSEIH  PIC X.                                          00001570
           02 MNENSEIV  PIC X.                                          00001580
           02 MNENSEIO  PIC X(15).                                      00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MNZONPRIA      PIC X.                                     00001610
           02 MNZONPRIC PIC X.                                          00001620
           02 MNZONPRIP PIC X.                                          00001630
           02 MNZONPRIH PIC X.                                          00001640
           02 MNZONPRIV PIC X.                                          00001650
           02 MNZONPRIO      PIC X(2).                                  00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MLZONPRIA      PIC X.                                     00001680
           02 MLZONPRIC PIC X.                                          00001690
           02 MLZONPRIP PIC X.                                          00001700
           02 MLZONPRIH PIC X.                                          00001710
           02 MLZONPRIV PIC X.                                          00001720
           02 MLZONPRIO      PIC X(20).                                 00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MNEMPLA   PIC X.                                          00001750
           02 MNEMPLC   PIC X.                                          00001760
           02 MNEMPLP   PIC X.                                          00001770
           02 MNEMPLH   PIC X.                                          00001780
           02 MNEMPLV   PIC X.                                          00001790
           02 MNEMPLO   PIC X(15).                                      00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MNTYPMAGA      PIC X.                                     00001820
           02 MNTYPMAGC PIC X.                                          00001830
           02 MNTYPMAGP PIC X.                                          00001840
           02 MNTYPMAGH PIC X.                                          00001850
           02 MNTYPMAGV PIC X.                                          00001860
           02 MNTYPMAGO      PIC X(2).                                  00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MLTYPMAGA      PIC X.                                     00001890
           02 MLTYPMAGC PIC X.                                          00001900
           02 MLTYPMAGP PIC X.                                          00001910
           02 MLTYPMAGH PIC X.                                          00001920
           02 MLTYPMAGV PIC X.                                          00001930
           02 MLTYPMAGO      PIC X(20).                                 00001940
           02 FILLER    PIC X(2).                                       00001950
           02 ML1ADRESA      PIC X.                                     00001960
           02 ML1ADRESC PIC X.                                          00001970
           02 ML1ADRESP PIC X.                                          00001980
           02 ML1ADRESH PIC X.                                          00001990
           02 ML1ADRESV PIC X.                                          00002000
           02 ML1ADRESO      PIC X(32).                                 00002010
           02 FILLER    PIC X(2).                                       00002020
           02 ML2ADRESA      PIC X.                                     00002030
           02 ML2ADRESC PIC X.                                          00002040
           02 ML2ADRESP PIC X.                                          00002050
           02 ML2ADRESH PIC X.                                          00002060
           02 ML2ADRESV PIC X.                                          00002070
           02 ML2ADRESO      PIC X(32).                                 00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MCODPOSTA      PIC X.                                     00002100
           02 MCODPOSTC PIC X.                                          00002110
           02 MCODPOSTP PIC X.                                          00002120
           02 MCODPOSTH PIC X.                                          00002130
           02 MCODPOSTV PIC X.                                          00002140
           02 MCODPOSTO      PIC X(5).                                  00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MNVILLEA  PIC X.                                          00002170
           02 MNVILLEC  PIC X.                                          00002180
           02 MNVILLEP  PIC X.                                          00002190
           02 MNVILLEH  PIC X.                                          00002200
           02 MNVILLEV  PIC X.                                          00002210
           02 MNVILLEO  PIC X(18).                                      00002220
           02 DFHMS1 OCCURS   15 TIMES .                                00002230
             03 FILLER       PIC X(2).                                  00002240
             03 MNRAYONA     PIC X.                                     00002250
             03 MNRAYONC     PIC X.                                     00002260
             03 MNRAYONP     PIC X.                                     00002270
             03 MNRAYONH     PIC X.                                     00002280
             03 MNRAYONV     PIC X.                                     00002290
             03 MNRAYONO     PIC X(5).                                  00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLIBERRA  PIC X.                                          00002320
           02 MLIBERRC  PIC X.                                          00002330
           02 MLIBERRP  PIC X.                                          00002340
           02 MLIBERRH  PIC X.                                          00002350
           02 MLIBERRV  PIC X.                                          00002360
           02 MLIBERRO  PIC X(78).                                      00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MCODTRAA  PIC X.                                          00002390
           02 MCODTRAC  PIC X.                                          00002400
           02 MCODTRAP  PIC X.                                          00002410
           02 MCODTRAH  PIC X.                                          00002420
           02 MCODTRAV  PIC X.                                          00002430
           02 MCODTRAO  PIC X(4).                                       00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MCICSA    PIC X.                                          00002460
           02 MCICSC    PIC X.                                          00002470
           02 MCICSP    PIC X.                                          00002480
           02 MCICSH    PIC X.                                          00002490
           02 MCICSV    PIC X.                                          00002500
           02 MCICSO    PIC X(5).                                       00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MNETNAMA  PIC X.                                          00002530
           02 MNETNAMC  PIC X.                                          00002540
           02 MNETNAMP  PIC X.                                          00002550
           02 MNETNAMH  PIC X.                                          00002560
           02 MNETNAMV  PIC X.                                          00002570
           02 MNETNAMO  PIC X(8).                                       00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MSCREENA  PIC X.                                          00002600
           02 MSCREENC  PIC X.                                          00002610
           02 MSCREENP  PIC X.                                          00002620
           02 MSCREENH  PIC X.                                          00002630
           02 MSCREENV  PIC X.                                          00002640
           02 MSCREENO  PIC X(4).                                       00002650
                                                                                
