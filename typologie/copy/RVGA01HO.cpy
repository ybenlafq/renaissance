      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TPROG PROFIL CONST. TOURNEE GENERAL    *        
      *----------------------------------------------------------------*        
       01  RVGA01H.                                                             
           05  TPROG-CTABLEG2    PIC X(15).                                     
           05  TPROG-CTABLEG2-REDEF REDEFINES TPROG-CTABLEG2.                   
               10  TPROG-PROFIL          PIC X(05).                             
           05  TPROG-WTABLEG     PIC X(80).                                     
           05  TPROG-WTABLEG-REDEF  REDEFINES TPROG-WTABLEG.                    
               10  TPROG-LIBELLE         PIC X(20).                             
               10  TPROG-FLAG            PIC X(01).                             
               10  TPROG-WECART          PIC X(01).                             
               10  TPROG-WECART-N       REDEFINES TPROG-WECART                  
                                         PIC 9(01).                             
               10  TPROG-IMPRIM          PIC X(19).                             
               10  TPROG-SATE            PIC X(04).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01H-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TPROG-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TPROG-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TPROG-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TPROG-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
