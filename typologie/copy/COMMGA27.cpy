      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
           02 COMM-GA27-APPLI REDEFINES COMM-GA00-APPLI.                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 COMM-GA27-NOITEM               PIC  S9(4) COMP.                
      *--                                                                       
              05 COMM-GA27-NOITEM               PIC  S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 COMM-GA27-NBITEM               PIC  S9(4) COMP.                
      *--                                                                       
              05 COMM-GA27-NBITEM               PIC  S9(4) COMP-5.              
      *}                                                                        
              05 COMM-GA27-CFAM                 PIC  X(5).                      
              05 COMM-GA27-CRAYON               PIC  X(5).                      
                                                                                
