      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE COPAR CODE PARAMETRE                   *        
      *----------------------------------------------------------------*        
       01  RVGA01BU.                                                            
           05  COPAR-CTABLEG2    PIC X(15).                                     
           05  COPAR-CTABLEG2-REDEF REDEFINES COPAR-CTABLEG2.                   
               10  COPAR-VALEUR          PIC X(05).                             
           05  COPAR-WTABLEG     PIC X(80).                                     
           05  COPAR-WTABLEG-REDEF  REDEFINES COPAR-WTABLEG.                    
               10  COPAR-LIBELLE         PIC X(20).                             
               10  COPAR-TYPES           PIC X(01).                             
               10  COPAR-TYPE            PIC X(01).                             
               10  COPAR-WGROUPE         PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01BU-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COPAR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  COPAR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COPAR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  COPAR-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
