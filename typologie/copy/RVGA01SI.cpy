      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE IFORG ORGANISMES DE FINANCEMENT        *        
      *----------------------------------------------------------------*        
       01  RVGA01SI.                                                            
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  IFORG-CTABLEG2    PIC X(15).                                     
           05  IFORG-CTABLEG2-REDEF REDEFINES IFORG-CTABLEG2.                   
               10  IFORG-CORGAN          PIC X(03).                             
           05  IFORG-WTABLEG     PIC X(80).                                     
           05  IFORG-WTABLEG-REDEF  REDEFINES IFORG-WTABLEG.                    
               10  IFORG-WACTIF          PIC X(01).                             
               10  IFORG-LORGAN          PIC X(20).                             
               10  IFORG-LGIDENT         PIC X(02).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  IFORG-LGIDENT-N      REDEFINES IFORG-LGIDENT                 
                                         PIC 9(02).                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  IFORG-SUFFIXE         PIC X(01).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01SI-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IFORG-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  IFORG-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IFORG-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  IFORG-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
