      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGM070 AU 11/02/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,03,PD,A,                          *        
      *                           20,20,BI,A,                          *        
      *                           40,20,BI,A,                          *        
      *                           60,20,BI,A,                          *        
      *                           80,07,BI,A,                          *        
      *                           87,07,BI,A,                          *        
      *                           94,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGM070.                                                        
            05 NOMETAT-IGM070           PIC X(6) VALUE 'IGM070'.                
            05 RUPTURES-IGM070.                                                 
           10 IGM070-NSOCIETE           PIC X(03).                      007  003
           10 IGM070-NZONE              PIC X(02).                      010  002
           10 IGM070-CHEFPROD           PIC X(05).                      012  005
           10 IGM070-WSEQFAM            PIC S9(05)      COMP-3.         017  003
           10 IGM070-LVMARKET1          PIC X(20).                      020  020
           10 IGM070-LVMARKET2          PIC X(20).                      040  020
           10 IGM070-LVMARKET3          PIC X(20).                      060  020
           10 IGM070-NCODIC2            PIC X(07).                      080  007
           10 IGM070-NCODIC             PIC X(07).                      087  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGM070-SEQUENCE           PIC S9(04) COMP.                094  002
      *--                                                                       
           10 IGM070-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGM070.                                                   
           10 IGM070-CAPPRO             PIC X(05).                      096  005
           10 IGM070-CFAM               PIC X(05).                      101  005
           10 IGM070-CMARQ              PIC X(05).                      106  005
           10 IGM070-CODLVM1            PIC X(01).                      111  001
           10 IGM070-CODLVM2            PIC X(01).                      112  001
           10 IGM070-CODLVM3            PIC X(01).                      113  001
           10 IGM070-CVDESCRIPTA        PIC X(05).                      114  005
           10 IGM070-CVDESCRIPTB        PIC X(05).                      119  005
           10 IGM070-DIFPRELEVE         PIC X(01).                      124  001
           10 IGM070-DRELEVE            PIC X(04).                      125  004
           10 IGM070-LCHEFPROD          PIC X(20).                      129  020
           10 IGM070-LCOMMENT           PIC X(02).                      149  002
           10 IGM070-LENSCONC           PIC X(15).                      151  015
           10 IGM070-LFAM               PIC X(20).                      166  020
           10 IGM070-LIBINT             PIC X(03).                      186  003
           10 IGM070-LMAGASIN           PIC X(20).                      189  020
           10 IGM070-LREFFOURN          PIC X(20).                      209  020
           10 IGM070-LSTATCOMP          PIC X(03).                      229  003
           10 IGM070-LZONPRIX           PIC X(20).                      232  020
           10 IGM070-NCONC              PIC X(04).                      252  004
           10 IGM070-NMAG               PIC X(03).                      256  003
           10 IGM070-NMAGASIN           PIC X(03).                      259  003
           10 IGM070-NZONPRIX           PIC X(02).                      262  002
           10 IGM070-OAM                PIC X(01).                      264  001
           10 IGM070-WANO               PIC X(01).                      265  001
           10 IGM070-WANOE              PIC X(01).                      266  001
           10 IGM070-WDERO              PIC X(01).                      267  001
           10 IGM070-WDEROE             PIC X(01).                      268  001
           10 IGM070-WD15               PIC X(01).                      269  001
           10 IGM070-WEDIT              PIC X(01).                      270  001
           10 IGM070-WGROUPE            PIC X(01).                      271  001
           10 IGM070-WOA                PIC X(01).                      272  001
           10 IGM070-PCA1S              PIC S9(11)V9(2) COMP-3.         273  007
           10 IGM070-PCA4S              PIC S9(11)V9(2) COMP-3.         280  007
           10 IGM070-PCHT               PIC S9(07)V9(6) COMP-3.         287  007
           10 IGM070-PCOMMART           PIC S9(05)V9(2) COMP-3.         294  004
           10 IGM070-PCOMMARTE          PIC S9(05)V9(2) COMP-3.         298  004
           10 IGM070-PCOMMFAM           PIC S9(05)V9(2) COMP-3.         302  004
           10 IGM070-PCOMMSTD           PIC S9(05)V9(2) COMP-3.         306  004
           10 IGM070-PEXPTTC            PIC S9(07)V9(2) COMP-3.         310  005
           10 IGM070-PMBU1S             PIC S9(11)V9(2) COMP-3.         315  007
           10 IGM070-PMBU4S             PIC S9(11)V9(2) COMP-3.         322  007
           10 IGM070-PMTPRIMES          PIC S9(11)V9(2) COMP-3.         329  007
           10 IGM070-PRELEVE            PIC S9(06)V9(2) COMP-3.         336  005
           10 IGM070-PSTDMAG            PIC S9(06)V9(2) COMP-3.         341  005
           10 IGM070-PSTDTTC            PIC S9(07)V9(2) COMP-3.         346  005
           10 IGM070-QSTOCKDEP          PIC S9(05)      COMP-3.         351  003
           10 IGM070-QSTOCKMAG          PIC S9(05)      COMP-3.         354  003
           10 IGM070-QSTOCKMAGP         PIC S9(05)      COMP-3.         357  003
           10 IGM070-QSTOCKMAGS         PIC S9(05)      COMP-3.         360  003
           10 IGM070-QTAUXTVA           PIC S9(02)V9(3) COMP-3.         363  003
           10 IGM070-QV1S               PIC S9(07)      COMP-3.         366  004
           10 IGM070-QV2S               PIC S9(07)      COMP-3.         370  004
           10 IGM070-QV3S               PIC S9(07)      COMP-3.         374  004
           10 IGM070-QV4S               PIC S9(07)      COMP-3.         378  004
           10 IGM070-SRP                PIC S9(06)V9(2) COMP-3.         382  005
           10 IGM070-ZTRI               PIC S9(07)V9(2) COMP-3.         387  005
           10 IGM070-ZTRI2              PIC S9(07)      COMP-3.         392  004
           10 IGM070-DEPF               PIC X(08).                      396  008
           10 IGM070-DFINEFFET          PIC X(08).                      404  008
            05 FILLER                      PIC X(101).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGM070-LONG           PIC S9(4)   COMP  VALUE +411.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGM070-LONG           PIC S9(4) COMP-5  VALUE +411.           
                                                                                
      *}                                                                        
