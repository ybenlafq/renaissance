      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVBS0600                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVBS0600                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVBS0600.                                                    00000090
           10 BS06-CDATA           PIC X(5).                            00000100
           10 BS06-IDDATA          PIC S9(15)V USAGE COMP-3.            00000110
           10 BS06-CTYPENT1        PIC X(2).                            00000120
           10 BS06-NENTITE1        PIC X(7).                            00000130
           10 BS06-NSEQENT         PIC S9(5)V USAGE COMP-3.             00000140
           10 BS06-NLIST           PIC X(7).                            00000150
           10 BS06-DSYST           PIC S9(13)V USAGE COMP-3.            00000160
      *                                                                 00000170
      *---------------------------------------------------------        00000180
      *   LISTE DES FLAGS DE LA TABLE RVBS0600                          00000190
      *---------------------------------------------------------        00000200
      *                                                                 00000210
       01  RVBS0600-FLAGS.                                              00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS06-CDATA-F         PIC S9(4) COMP.                      00000230
      *--                                                                       
           10 BS06-CDATA-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS06-IDDATA-F        PIC S9(4) COMP.                      00000240
      *--                                                                       
           10 BS06-IDDATA-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS06-CTYPENT1-F      PIC S9(4) COMP.                      00000250
      *--                                                                       
           10 BS06-CTYPENT1-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS06-NENTITE1-F      PIC S9(4) COMP.                      00000260
      *--                                                                       
           10 BS06-NENTITE1-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS06-NSEQENT-F       PIC S9(4) COMP.                      00000270
      *--                                                                       
           10 BS06-NSEQENT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS06-NLIST-F         PIC S9(4) COMP.                      00000280
      *--                                                                       
           10 BS06-NLIST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS06-DSYST-F         PIC S9(4) COMP.                      00000290
      *                                                                         
      *--                                                                       
           10 BS06-DSYST-F         PIC S9(4) COMP-5.                            
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
