      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      **************************************************************    00010009
      * COMMAREA SPECIFIQUE PRG TGA03                    TR: GA03  *    00020009
      *                  GESTION DES ENTITES DE COMMANDE           *    00030009
      **************************************************************    00040009
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00050009
      *                                                                 00060009
          02 COMM-GA03-APPLI REDEFINES COMM-GA00-APPLI.                 00070009
      *------------------------------ CODE FONCTION                     00080009
             03 COMM-GA03-FONCT          PIC X(3).                      00090009
      *------------------------------ CODE ENTITE DE COMMANDE           00100009
             03 COMM-GA03-NENTCDE        PIC X(5).                      00110009
      *------------------------------ LIBELLE ENTITE DE COMMANDE        00120009
             03 COMM-GA03-LENTCDE        PIC X(20).                     00130009
      *------------------------------ ADRESSE 1                         00140009
             03 COMM-GA03-LENTCDEADR1    PIC X(32).                     00150009
      *------------------------------ ADRESSE 2                         00160009
             03 COMM-GA03-LENTCDEADR2    PIC X(32).                     00170009
      *------------------------------ CODE POSTAL                       00180009
             03 COMM-GA03-NENTCDEADR3    PIC 9(5).                      00190009
      *------------------------------ VILLE                             00200009
             03 COMM-GA03-LENTCDEADR4    PIC X(26).                     00210009
      *------------------------------ TELEPHONE                         00220009
             03 COMM-GA03-NENTCDETEL     PIC X(15).                     00230009
      *------------------------------ TELEX                             00240009
             03 COMM-GA03-CENTCDETELX    PIC X(15).                     00250009
      *------------------------------ CODE DEVISE                       00260009
             03 COMM-GA03-CDEVISE        PIC X(3).                      00270009
      *------------------------------ DATE EFFET                        00280009
             03 COMM-GA03-DEFFET         PIC X(8).                      00290009
      *------------------------------                                   00300009
             03 COMM-GA03-CDEVISA       PIC X(3).                       00310009
E0191 *--- ZONE POUR L'EDI                                              00320009
             03 COMM-GA03-RECORD               PIC X(1).                00330009
                88 COMM-GA03-RECORD-PRESENT    VALUE '1'.               00340009
                88 COMM-GA03-RECORD-ABSENT     VALUE '0'.               00350009
             03 COMM-GA03-EDI-MAJ              PIC X(1).                00360009
                88 COMM-GA03-EDI-MODIF         VALUE '1'.               00370009
                88 COMM-GA03-EDI-PAS-MODIF     VALUE '0'.               00380009
             03 COMM-GA03-WEDI                 PIC X(1).                00390009
             03 COMM-GA03-DEFFETEDI            PIC X(8).                00400009
             03 COMM-GA03-DEFFETEDI-JMSA       PIC X(10).               00410009
             03 COMM-GA03-WEAN                 PIC X(1).                00420009
             03 COMM-GA03-WMULTIDEP            PIC X(1).                00430009
             03 COMM-GA03-WEDITCDE             PIC X(1).                00440009
E0247        03 COMM-GA03-ORDERS               PIC X(1).                00450009
E0466 *E0247 03 COMM-GA03-ORDCHG               PIC X(1).                00460016
E0466        03 COMM-GA03-CALAGE               PIC X(1).                00461016
      *---- D3E                                                         00470009
E0268        03 COMM-GA03-CPAYS                PIC X(2).                00480009
E0268        03 COMM-GA03-WIMPORT              PIC X(1).                00490009
E0268        03 COMM-GA03-CECO                 PIC X(3).                00500009
      *---- CONVERGENCE                                                 00510009
E0309        03 COMM-GA03-CCOMPTE              PIC X(2).                00520009
E0311-       03 COMM-GA03-WSAP                 PIC X(1).                00520113
      *      03 COMM-GA03-ENVSAP-MAJ           PIC X(1).                00521014
      *         88 COMM-GA03-PAS-ENVSAP        VALUE '0'.               00522014
-E0311*         88 COMM-GA03-ENVSAP            VALUE '1'.               00523014
E0466        03 COMM-GA03-DEFFETCAL            PIC X(8).                00524018
E0672        03 COMM-GA03-CCOMPTE-SVG          PIC X(2).                00525019
E0733        03 COMM-GA03-CROSSDOCK            PIC X(1).                00526020
      *------------------------------ FILLER                            00530009
E0247 *      03 COMM-GA03-FILLER         PIC X(3533).                   00540009
E0247 *      03 COMM-GA03-FILLER         PIC X(3531).                   00550009
E0268 *      03 COMM-GA03-FILLER         PIC X(3528).                   00560009
E0268 *      03 COMM-GA03-FILLER         PIC X(3525).                   00570009
E0309 *      03 COMM-GA03-FILLER         PIC X(3523).                   00580015
E0311 *      03 COMM-GA03-FILLER         PIC X(3521).                   00580116
E0466 *E0311 03 COMM-GA03-FILLER         PIC X(3522).                   00581016
E0466 *E0672 03 COMM-GA03-FILLER         PIC X(3514).                   00582019
E0672 *E0733 03 COMM-GA03-FILLER         PIC X(3512).                   00583020
E0733        03 COMM-GA03-FILLER         PIC X(3511).                   00584020
      ***************************************************************** 00590009
                                                                                
