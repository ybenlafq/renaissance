      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RTPR21                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPR2100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPR2100.                                                            
      *}                                                                        
           10 PR21-CTYPE           PIC X(1).                                    
           10 PR21-NENTCDE         PIC X(5).                                    
           10 PR21-NCHRONO         PIC X(5).                                    
           10 PR21-LIBELLE         PIC X(26).                                   
           10 PR21-CTYPPREST       PIC X(5).                                    
           10 PR21-CPREST          PIC X(5).                                    
           10 PR21-NCODIC          PIC X(7).                                    
           10 PR21-WINCCOND        PIC X(1).                                    
           10 PR21-CRGLT           PIC X(3).                                    
           10 PR21-CFACT           PIC X(3).                                    
           10 PR21-WMTVENTE        PIC X(1).                                    
           10 PR21-DMAJ            PIC X(8).                                    
           10 PR21-DSYST           PIC S9(13)V USAGE COMP-3.                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 13      *        
      ******************************************************************        
                                                                                
