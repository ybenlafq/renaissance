      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CAADR CORRESP. ACID =>SOCLIEU /TAC00   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01NT.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01NT.                                                            
      *}                                                                        
           05  CAADR-CTABLEG2    PIC X(15).                                     
           05  CAADR-CTABLEG2-REDEF REDEFINES CAADR-CTABLEG2.                   
               10  CAADR-CACID           PIC X(07).                             
           05  CAADR-WTABLEG     PIC X(80).                                     
           05  CAADR-WTABLEG-REDEF  REDEFINES CAADR-WTABLEG.                    
               10  CAADR-WACTIF          PIC X(01).                             
               10  CAADR-NSOCIETE        PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  CAADR-NSOCIETE-N     REDEFINES CAADR-NSOCIETE                
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  CAADR-NLIEU           PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  CAADR-NLIEU-N        REDEFINES CAADR-NLIEU                   
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  CAADR-LIBELLE         PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01NT-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01NT-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CAADR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CAADR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CAADR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CAADR-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
