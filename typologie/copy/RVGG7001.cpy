      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGG7001                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGG7001                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGG7001.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGG7001.                                                            
      *}                                                                        
           02  GG70-NCODIC                                                      
               PIC X(0007).                                                     
           02  GG70-DREC                                                        
               PIC X(0008).                                                     
           02  GG70-NREC                                                        
               PIC X(0007).                                                     
           02  GG70-NCDE                                                        
               PIC X(0007).                                                     
           02  GG70-PRA                                                         
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GG70-QTEREC                                                      
               PIC S9(5) COMP-3.                                                
           02  GG70-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GG70-PRISTT                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GG70-NRECPRA                                                     
               PIC X(0007).                                                     
           02  GG70-NCDEPRA                                                     
               PIC X(0007).                                                     
           02  GG70-WFICTIF                                                     
               PIC X(0001).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGG7001                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGG7001-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGG7001-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG70-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG70-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG70-DREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG70-DREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG70-NREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG70-NREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG70-NCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG70-NCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG70-PRA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG70-PRA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG70-QTEREC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG70-QTEREC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG70-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG70-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG70-PRISTT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG70-PRISTT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG70-NRECPRA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG70-NRECPRA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG70-NCDEPRA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG70-NCDEPRA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG70-WFICTIF-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG70-WFICTIF-F                                                   
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
