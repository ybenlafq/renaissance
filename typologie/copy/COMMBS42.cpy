      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES VENTES                               *        
      *  TRANSACTION: GV00                                             *        
      *  TITRE      : COMMAREA DE L'APPLICATION BS42 APELLEE PAR GV00  *        
      *  LONGUEUR   : 200                                              *        
      *                                                                *        
      ******************************************************************        
      *                                                                 00230000
      *                                                                 00230000
       01  COMM-BS42-APPLI.                                             00260000
           02 COMM-BS42-NSOCIETE        PIC X(03).                      00320000
           02 COMM-BS42-NLIEU           PIC X(03).                      00330000
           02 COMM-BS42-NVENTE          PIC X(07).                      00340000
           02 COMM-BS42-TYPVTE          PIC X(01).                      00340000
           02 COMM-BS42-NFLAG           PIC X(01).                      00340000
           02 COMM-BS42-NCLUSTER        PIC X(13).                      00340000
           02 COMM-BS42-CODE-RETOUR     PIC X(01).                      00340000
           02 COMM-BS42-MESSAGE.                                                
                10 COMM-BS42-CODRET          PIC X.                             
                   88  COMM-BS42-CODRET-OK   VALUE ' '.                         
                   88  COMM-BS42-CODRET-ERREUR  VALUE '1'.                      
                   88  COMM-BS42-CODRET-TS-OK   VALUE '2'.                      
                10 COMM-BS42-LIBERR          PIC X(58).                         
           02 COMM-BS42-PGAPPEL         PIC X(05).                      00340000
           02 COMM-BS42-IDENT-TS        PIC X(08).                              
           02 FILLER                    PIC X(173).                     00340000
                                                                                
