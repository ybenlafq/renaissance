      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SUIVI LIEN NEM/MONETIQUE ECRAN 1                                00000020
      ***************************************************************** 00000030
       01   EBS70I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC XX.                                         00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MPAGEMAXI      PIC 99.                                    00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCACIDL   COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MCACIDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCACIDF   PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MCACIDI   PIC X(8).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPERMINL  COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MPERMINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPERMINF  PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MPERMINI  PIC X(10).                                      00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPERMAXL  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MPERMAXL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPERMAXF  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MPERMAXI  PIC X(10).                                      00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOUSMAGL      COMP PIC S9(4).                            00000360
      *--                                                                       
           02 MTOUSMAGL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTOUSMAGF      PIC X.                                     00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MTOUSMAGI      PIC X.                                     00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBTPSL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MLIBTPSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBTPSF  PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MLIBTPSI  PIC X(5).                                       00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBNBL   COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MLIBNBL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIBNBF   PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MLIBNBI   PIC X(5).                                       00000470
           02 MLISTMAGI OCCURS   11 TIMES .                             00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000490
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MSELI   PIC X.                                          00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00000530
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MNSOCI  PIC X(3).                                       00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMAGL  COMP PIC S9(4).                                 00000570
      *--                                                                       
             03 MNMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMAGF  PIC X.                                          00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MNMAGI  PIC X(3).                                       00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMAGL  COMP PIC S9(4).                                 00000610
      *--                                                                       
             03 MLMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLMAGF  PIC X.                                          00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MLMAGI  PIC X(20).                                      00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTJOURSL     COMP PIC S9(4).                            00000650
      *--                                                                       
             03 MTJOURSL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTJOURSF     PIC X.                                     00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MTJOURSI     PIC X(3).                                  00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCONTPSL     COMP PIC S9(4).                            00000690
      *--                                                                       
             03 MCONTPSL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCONTPSF     PIC X.                                     00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MCONTPSI     PIC X(6).                                  00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCONNBL      COMP PIC S9(4).                            00000730
      *--                                                                       
             03 MCONNBL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCONNBF      PIC X.                                     00000740
             03 FILLER  PIC X(4).                                       00000750
             03 MCONNBI      PIC X(3).                                  00000760
      * ZONE CMD AIDA                                                   00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLIBERRI  PIC X(79).                                      00000810
      * CODE TRANSACTION                                                00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCODTRAI  PIC X(4).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MZONCMDI  PIC X(14).                                      00000900
      * CICS DE TRAVAIL                                                 00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MCICSI    PIC X(5).                                       00000950
      * NETNAME                                                         00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MNETNAMI  PIC X(8).                                       00001000
      * CODE TERMINAL                                                   00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MSCREENI  PIC X(4).                                       00001050
      ***************************************************************** 00001060
      * SUIVI LIEN NEM/MONETIQUE ECRAN 1                                00001070
      ***************************************************************** 00001080
       01   EBS70O REDEFINES EBS70I.                                    00001090
           02 FILLER    PIC X(12).                                      00001100
      * DATE DU JOUR                                                    00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MDATJOUA  PIC X.                                          00001130
           02 MDATJOUC  PIC X.                                          00001140
           02 MDATJOUP  PIC X.                                          00001150
           02 MDATJOUH  PIC X.                                          00001160
           02 MDATJOUV  PIC X.                                          00001170
           02 MDATJOUO  PIC X(10).                                      00001180
      * HEURE                                                           00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MTIMJOUA  PIC X.                                          00001210
           02 MTIMJOUC  PIC X.                                          00001220
           02 MTIMJOUP  PIC X.                                          00001230
           02 MTIMJOUH  PIC X.                                          00001240
           02 MTIMJOUV  PIC X.                                          00001250
           02 MTIMJOUO  PIC X(5).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MPAGEA    PIC X.                                          00001280
           02 MPAGEC    PIC X.                                          00001290
           02 MPAGEP    PIC X.                                          00001300
           02 MPAGEH    PIC X.                                          00001310
           02 MPAGEV    PIC X.                                          00001320
           02 MPAGEO    PIC 99.                                         00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MPAGEMAXA      PIC X.                                     00001350
           02 MPAGEMAXC PIC X.                                          00001360
           02 MPAGEMAXP PIC X.                                          00001370
           02 MPAGEMAXH PIC X.                                          00001380
           02 MPAGEMAXV PIC X.                                          00001390
           02 MPAGEMAXO      PIC 99.                                    00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MCACIDA   PIC X.                                          00001420
           02 MCACIDC   PIC X.                                          00001430
           02 MCACIDP   PIC X.                                          00001440
           02 MCACIDH   PIC X.                                          00001450
           02 MCACIDV   PIC X.                                          00001460
           02 MCACIDO   PIC X(8).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MPERMINA  PIC X.                                          00001490
           02 MPERMINC  PIC X.                                          00001500
           02 MPERMINP  PIC X.                                          00001510
           02 MPERMINH  PIC X.                                          00001520
           02 MPERMINV  PIC X.                                          00001530
           02 MPERMINO  PIC X(10).                                      00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MPERMAXA  PIC X.                                          00001560
           02 MPERMAXC  PIC X.                                          00001570
           02 MPERMAXP  PIC X.                                          00001580
           02 MPERMAXH  PIC X.                                          00001590
           02 MPERMAXV  PIC X.                                          00001600
           02 MPERMAXO  PIC X(10).                                      00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MTOUSMAGA      PIC X.                                     00001630
           02 MTOUSMAGC PIC X.                                          00001640
           02 MTOUSMAGP PIC X.                                          00001650
           02 MTOUSMAGH PIC X.                                          00001660
           02 MTOUSMAGV PIC X.                                          00001670
           02 MTOUSMAGO      PIC X.                                     00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MLIBTPSA  PIC X.                                          00001700
           02 MLIBTPSC  PIC X.                                          00001710
           02 MLIBTPSP  PIC X.                                          00001720
           02 MLIBTPSH  PIC X.                                          00001730
           02 MLIBTPSV  PIC X.                                          00001740
           02 MLIBTPSO  PIC X(5).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MLIBNBA   PIC X.                                          00001770
           02 MLIBNBC   PIC X.                                          00001780
           02 MLIBNBP   PIC X.                                          00001790
           02 MLIBNBH   PIC X.                                          00001800
           02 MLIBNBV   PIC X.                                          00001810
           02 MLIBNBO   PIC X(5).                                       00001820
           02 MLISTMAGO OCCURS   11 TIMES .                             00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MSELA   PIC X.                                          00001850
             03 MSELC   PIC X.                                          00001860
             03 MSELP   PIC X.                                          00001870
             03 MSELH   PIC X.                                          00001880
             03 MSELV   PIC X.                                          00001890
             03 MSELO   PIC X.                                          00001900
             03 FILLER       PIC X(2).                                  00001910
             03 MNSOCA  PIC X.                                          00001920
             03 MNSOCC  PIC X.                                          00001930
             03 MNSOCP  PIC X.                                          00001940
             03 MNSOCH  PIC X.                                          00001950
             03 MNSOCV  PIC X.                                          00001960
             03 MNSOCO  PIC X(3).                                       00001970
             03 FILLER       PIC X(2).                                  00001980
             03 MNMAGA  PIC X.                                          00001990
             03 MNMAGC  PIC X.                                          00002000
             03 MNMAGP  PIC X.                                          00002010
             03 MNMAGH  PIC X.                                          00002020
             03 MNMAGV  PIC X.                                          00002030
             03 MNMAGO  PIC X(3).                                       00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MLMAGA  PIC X.                                          00002060
             03 MLMAGC  PIC X.                                          00002070
             03 MLMAGP  PIC X.                                          00002080
             03 MLMAGH  PIC X.                                          00002090
             03 MLMAGV  PIC X.                                          00002100
             03 MLMAGO  PIC X(20).                                      00002110
             03 FILLER       PIC X(2).                                  00002120
             03 MTJOURSA     PIC X.                                     00002130
             03 MTJOURSC     PIC X.                                     00002140
             03 MTJOURSP     PIC X.                                     00002150
             03 MTJOURSH     PIC X.                                     00002160
             03 MTJOURSV     PIC X.                                     00002170
             03 MTJOURSO     PIC X(3).                                  00002180
             03 FILLER       PIC X(2).                                  00002190
             03 MCONTPSA     PIC X.                                     00002200
             03 MCONTPSC     PIC X.                                     00002210
             03 MCONTPSP     PIC X.                                     00002220
             03 MCONTPSH     PIC X.                                     00002230
             03 MCONTPSV     PIC X.                                     00002240
             03 MCONTPSO     PIC X(6).                                  00002250
             03 FILLER       PIC X(2).                                  00002260
             03 MCONNBA      PIC X.                                     00002270
             03 MCONNBC PIC X.                                          00002280
             03 MCONNBP PIC X.                                          00002290
             03 MCONNBH PIC X.                                          00002300
             03 MCONNBV PIC X.                                          00002310
             03 MCONNBO      PIC X(3).                                  00002320
      * ZONE CMD AIDA                                                   00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MLIBERRA  PIC X.                                          00002350
           02 MLIBERRC  PIC X.                                          00002360
           02 MLIBERRP  PIC X.                                          00002370
           02 MLIBERRH  PIC X.                                          00002380
           02 MLIBERRV  PIC X.                                          00002390
           02 MLIBERRO  PIC X(79).                                      00002400
      * CODE TRANSACTION                                                00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MCODTRAA  PIC X.                                          00002430
           02 MCODTRAC  PIC X.                                          00002440
           02 MCODTRAP  PIC X.                                          00002450
           02 MCODTRAH  PIC X.                                          00002460
           02 MCODTRAV  PIC X.                                          00002470
           02 MCODTRAO  PIC X(4).                                       00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MZONCMDA  PIC X.                                          00002500
           02 MZONCMDC  PIC X.                                          00002510
           02 MZONCMDP  PIC X.                                          00002520
           02 MZONCMDH  PIC X.                                          00002530
           02 MZONCMDV  PIC X.                                          00002540
           02 MZONCMDO  PIC X(14).                                      00002550
      * CICS DE TRAVAIL                                                 00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MCICSA    PIC X.                                          00002580
           02 MCICSC    PIC X.                                          00002590
           02 MCICSP    PIC X.                                          00002600
           02 MCICSH    PIC X.                                          00002610
           02 MCICSV    PIC X.                                          00002620
           02 MCICSO    PIC X(5).                                       00002630
      * NETNAME                                                         00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MNETNAMA  PIC X.                                          00002660
           02 MNETNAMC  PIC X.                                          00002670
           02 MNETNAMP  PIC X.                                          00002680
           02 MNETNAMH  PIC X.                                          00002690
           02 MNETNAMV  PIC X.                                          00002700
           02 MNETNAMO  PIC X(8).                                       00002710
      * CODE TERMINAL                                                   00002720
           02 FILLER    PIC X(2).                                       00002730
           02 MSCREENA  PIC X.                                          00002740
           02 MSCREENC  PIC X.                                          00002750
           02 MSCREENP  PIC X.                                          00002760
           02 MSCREENH  PIC X.                                          00002770
           02 MSCREENV  PIC X.                                          00002780
           02 MSCREENO  PIC X(4).                                       00002790
                                                                                
