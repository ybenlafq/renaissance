      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EMDQU QUOTA EMD PAR MAG ET CSELART     *        
      *----------------------------------------------------------------*        
       01  RVGA01SQ.                                                            
           05  EMDQU-CTABLEG2    PIC X(15).                                     
           05  EMDQU-CTABLEG2-REDEF REDEFINES EMDQU-CTABLEG2.                   
               10  EMDQU-NSOCMAG         PIC X(03).                             
               10  EMDQU-NSOCMAG-N      REDEFINES EMDQU-NSOCMAG                 
                                         PIC 9(03).                             
               10  EMDQU-NMAG            PIC X(03).                             
               10  EMDQU-NMAG-N         REDEFINES EMDQU-NMAG                    
                                         PIC 9(03).                             
               10  EMDQU-CSELART         PIC X(05).                             
           05  EMDQU-WTABLEG     PIC X(80).                                     
           05  EMDQU-WTABLEG-REDEF  REDEFINES EMDQU-WTABLEG.                    
               10  EMDQU-QNBP            PIC X(05).                             
               10  EMDQU-QNBP-N         REDEFINES EMDQU-QNBP                    
                                         PIC 9(05).                             
               10  EMDQU-QNBM3           PIC X(04).                             
               10  EMDQU-QNBM3-N        REDEFINES EMDQU-QNBM3                   
                                         PIC 9(02)V9(02).                       
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01SQ-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EMDQU-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EMDQU-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EMDQU-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EMDQU-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
