      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA27   EGA27                                              00000020
      ***************************************************************** 00000030
       01   EGA27I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(2).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(2).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOPAGEL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNOPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNOPAGEF  PIC X.                                          00000150
           02 FILLER    PIC X(2).                                       00000160
           02 MNOPAGEI  PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000190
           02 FILLER    PIC X(2).                                       00000200
           02 MNBPAGESI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000230
           02 FILLER    PIC X(2).                                       00000240
           02 MCFAMI    PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000270
           02 FILLER    PIC X(2).                                       00000280
           02 MLFAMI    PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPREVL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MPREVL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPREVF    PIC X.                                          00000310
           02 FILLER    PIC X(2).                                       00000320
           02 MPREVI    PIC X(7).                                       00000330
           02 FILLER  OCCURS   14 TIMES .                               00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCRAYONL     COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MCRAYONL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCRAYONF     PIC X.                                     00000360
             03 FILLER  PIC X(2).                                       00000370
             03 MCRAYONI     PIC X(5).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLRAYONL     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MLRAYONL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLRAYONF     PIC X.                                     00000400
             03 FILLER  PIC X(2).                                       00000410
             03 MLRAYONI     PIC X(20).                                 00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCETATL      COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MCETATL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCETATF      PIC X.                                     00000440
             03 FILLER  PIC X(2).                                       00000450
             03 MCETATI      PIC X(10).                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLETATL      COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MLETATL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLETATF      PIC X.                                     00000480
             03 FILLER  PIC X(2).                                       00000490
             03 MLETATI      PIC X(39).                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSUITEL   COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MSUITEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSUITEF   PIC X.                                          00000520
           02 FILLER    PIC X(2).                                       00000530
           02 MSUITEI   PIC X(7).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000560
           02 FILLER    PIC X(2).                                       00000570
           02 MLIBERRI  PIC X(79).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000600
           02 FILLER    PIC X(2).                                       00000610
           02 MCODTRAI  PIC X(4).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000640
           02 FILLER    PIC X(2).                                       00000650
           02 MZONCMDI  PIC X(15).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000680
           02 FILLER    PIC X(2).                                       00000690
           02 MCICSI    PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000720
           02 FILLER    PIC X(2).                                       00000730
           02 MNETNAMI  PIC X(8).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000760
           02 FILLER    PIC X(2).                                       00000770
           02 MSCREENI  PIC X(4).                                       00000780
      ***************************************************************** 00000790
      * SDF: EGA27   EGA27                                              00000800
      ***************************************************************** 00000810
       01   EGA27O REDEFINES EGA27I.                                    00000820
           02 FILLER    PIC X(12).                                      00000830
           02 FILLER    PIC X(2).                                       00000840
           02 MDATJOUA  PIC X.                                          00000850
           02 MDATJOUC  PIC X.                                          00000860
           02 MDATJOUH  PIC X.                                          00000870
           02 MDATJOUO  PIC X(10).                                      00000880
           02 FILLER    PIC X(2).                                       00000890
           02 MTIMJOUA  PIC X.                                          00000900
           02 MTIMJOUC  PIC X.                                          00000910
           02 MTIMJOUH  PIC X.                                          00000920
           02 MTIMJOUO  PIC X(5).                                       00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MNOPAGEA  PIC X.                                          00000950
           02 MNOPAGEC  PIC X.                                          00000960
           02 MNOPAGEH  PIC X.                                          00000970
           02 MNOPAGEO  PIC ZZ.                                         00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MNBPAGESA      PIC X.                                     00001000
           02 MNBPAGESC PIC X.                                          00001010
           02 MNBPAGESH PIC X.                                          00001020
           02 MNBPAGESO      PIC ZZ.                                    00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MCFAMA    PIC X.                                          00001050
           02 MCFAMC    PIC X.                                          00001060
           02 MCFAMH    PIC X.                                          00001070
           02 MCFAMO    PIC X(5).                                       00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MLFAMA    PIC X.                                          00001100
           02 MLFAMC    PIC X.                                          00001110
           02 MLFAMH    PIC X.                                          00001120
           02 MLFAMO    PIC X(20).                                      00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MPREVA    PIC X.                                          00001150
           02 MPREVC    PIC X.                                          00001160
           02 MPREVH    PIC X.                                          00001170
           02 MPREVO    PIC X(7).                                       00001180
           02 FILLER  OCCURS   14 TIMES .                               00001190
             03 FILLER       PIC X(2).                                  00001200
             03 MCRAYONA     PIC X.                                     00001210
             03 MCRAYONC     PIC X.                                     00001220
             03 MCRAYONH     PIC X.                                     00001230
             03 MCRAYONO     PIC X(5).                                  00001240
             03 FILLER       PIC X(2).                                  00001250
             03 MLRAYONA     PIC X.                                     00001260
             03 MLRAYONC     PIC X.                                     00001270
             03 MLRAYONH     PIC X.                                     00001280
             03 MLRAYONO     PIC X(20).                                 00001290
             03 FILLER       PIC X(2).                                  00001300
             03 MCETATA      PIC X.                                     00001310
             03 MCETATC PIC X.                                          00001320
             03 MCETATH PIC X.                                          00001330
             03 MCETATO      PIC X(10).                                 00001340
             03 FILLER       PIC X(2).                                  00001350
             03 MLETATA      PIC X.                                     00001360
             03 MLETATC PIC X.                                          00001370
             03 MLETATH PIC X.                                          00001380
             03 MLETATO      PIC X(39).                                 00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MSUITEA   PIC X.                                          00001410
           02 MSUITEC   PIC X.                                          00001420
           02 MSUITEH   PIC X.                                          00001430
           02 MSUITEO   PIC X(7).                                       00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MLIBERRA  PIC X.                                          00001460
           02 MLIBERRC  PIC X.                                          00001470
           02 MLIBERRH  PIC X.                                          00001480
           02 MLIBERRO  PIC X(79).                                      00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MCODTRAA  PIC X.                                          00001510
           02 MCODTRAC  PIC X.                                          00001520
           02 MCODTRAH  PIC X.                                          00001530
           02 MCODTRAO  PIC X(4).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MZONCMDA  PIC X.                                          00001560
           02 MZONCMDC  PIC X.                                          00001570
           02 MZONCMDH  PIC X.                                          00001580
           02 MZONCMDO  PIC X(15).                                      00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MCICSA    PIC X.                                          00001610
           02 MCICSC    PIC X.                                          00001620
           02 MCICSH    PIC X.                                          00001630
           02 MCICSO    PIC X(5).                                       00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MNETNAMA  PIC X.                                          00001660
           02 MNETNAMC  PIC X.                                          00001670
           02 MNETNAMH  PIC X.                                          00001680
           02 MNETNAMO  PIC X(8).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MSCREENA  PIC X.                                          00001710
           02 MSCREENC  PIC X.                                          00001720
           02 MSCREENH  PIC X.                                          00001730
           02 MSCREENO  PIC X(4).                                       00001740
                                                                                
