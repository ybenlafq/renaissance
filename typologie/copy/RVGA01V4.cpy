      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 11/07/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MQPUT PARAM PUT MQSERIES HOST          *        
      *----------------------------------------------------------------*        
       01  RVGA01V4.                                                            
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  MQPUT-CTABLEG2    PIC X(15).                                     
           05  MQPUT-CTABLEG2-REDEF REDEFINES MQPUT-CTABLEG2.                   
               10  MQPUT-NOMPROG         PIC X(06).                             
               10  MQPUT-CFONC           PIC X(03).                             
           05  MQPUT-WTABLEG     PIC X(80).                                     
           05  MQPUT-WTABLEG-REDEF  REDEFINES MQPUT-WTABLEG.                    
               10  MQPUT-QALIAS          PIC X(18).                             
               10  MQPUT-MSGID           PIC X(24).                             
               10  MQPUT-DUREE           PIC X(05).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  MQPUT-DUREE-N        REDEFINES MQPUT-DUREE                   
                                         PIC S9(05).                            
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  MQPUT-PERS            PIC X(01).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01V4-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MQPUT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MQPUT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MQPUT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MQPUT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
