      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EFRDU TYPES DE RENDU DE LA GEF         *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01KP.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01KP.                                                            
      *}                                                                        
           05  EFRDU-CTABLEG2    PIC X(15).                                     
           05  EFRDU-CTABLEG2-REDEF REDEFINES EFRDU-CTABLEG2.                   
               10  EFRDU-CRENDU          PIC X(05).                             
           05  EFRDU-WTABLEG     PIC X(80).                                     
           05  EFRDU-WTABLEG-REDEF  REDEFINES EFRDU-WTABLEG.                    
               10  EFRDU-WACTIF          PIC X(01).                             
               10  EFRDU-LRENDU          PIC X(20).                             
               10  EFRDU-WRENDU          PIC X(01).                             
               10  EFRDU-CTYPRDU         PIC X(01).                             
               10  EFRDU-TYPREP          PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01KP-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01KP-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EFRDU-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EFRDU-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EFRDU-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EFRDU-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
