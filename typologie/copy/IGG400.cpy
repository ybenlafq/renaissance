      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGG400 AU 07/12/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,01,BI,A,                          *        
      *                           11,03,PD,A,                          *        
      *                           14,07,BI,A,                          *        
      *                           21,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGG400.                                                        
            05 NOMETAT-IGG400           PIC X(6) VALUE 'IGG400'.                
            05 RUPTURES-IGG400.                                                 
           10 IGG400-NSOCIETE           PIC X(03).                      007  003
           10 IGG400-WTLMELA            PIC X(01).                      010  001
           10 IGG400-WSEQFAM            PIC S9(05)      COMP-3.         011  003
           10 IGG400-NCODIC             PIC X(07).                      014  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGG400-SEQUENCE           PIC S9(04) COMP.                021  002
      *--                                                                       
           10 IGG400-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGG400.                                                   
           10 IGG400-CFAM               PIC X(05).                      023  005
           10 IGG400-CMARQ              PIC X(05).                      028  005
           10 IGG400-LFAM               PIC X(20).                      033  020
           10 IGG400-LREFFOURN          PIC X(20).                      053  020
           10 IGG400-STATCOMP           PIC X(03).                      073  003
           10 IGG400-ECART              PIC S9(05)V9(2) COMP-3.         076  004
           10 IGG400-PRMPDIF            PIC S9(07)V9(2) COMP-3.         080  005
           10 IGG400-PRMPSOC            PIC S9(07)V9(2) COMP-3.         085  005
            05 FILLER                      PIC X(423).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGG400-LONG           PIC S9(4)   COMP  VALUE +089.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGG400-LONG           PIC S9(4) COMP-5  VALUE +089.           
                                                                                
      *}                                                                        
