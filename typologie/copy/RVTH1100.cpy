      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVTH1100                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTH1100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTH1100.                                                            
      *}                                                                        
      *    *************************************************************        
      *                       NCODIC                                            
           10 TH11-NCODIC          PIC X(7).                                    
      *    *************************************************************        
      *                       DJOUR                                             
           10 TH11-DEFFET          PIC X(8).                                    
      *    *************************************************************        
      *                       PTC                                               
           10 TH11-PTC             PIC S9(7)V9(6) USAGE COMP-3.                 
      *    *************************************************************        
      *                       DCRESYST                                          
           10 TH11-DCRESYST        PIC X(26).                                   
      *    *************************************************************        
      *                       DMAJSYST                                          
           10 TH11-DMAJSYST        PIC X(26).                                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 5       *        
      ******************************************************************        
                                                                                
