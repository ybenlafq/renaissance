      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      **************************************************************            
      * COMMAREA SPECIFIQUE PROG TGA07                             *            
      *       TR : GA00  ADMISTRATION DES DONNEES                  *            
      *       PG : TGA07 SOUS DEFINITION DES FAMILLES              *            
      **************************************************************            
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724          
      *                                                                         
      *                                                                         
          02 COMM-GA07-APPLI REDEFINES COMM-GA00-APPLI.                         
      *------------------------------ CODE FONCTION                             
             03 COMM-GA07-WFONC          PIC X(3).                              
      *---------                         CHOIX                                  
             03 COMM-GA07-ZONCMD         PIC X(15).                             
      *---------                                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GA07-ITEM           PIC S9(4)   COMP.                      
      *--                                                                       
             03 COMM-GA07-ITEM           PIC S9(4) COMP-5.                      
      *}                                                                        
      *---------                                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GA07-MAXITEM        PIC S9(4)   COMP.                      
      *--                                                                       
             03 COMM-GA07-MAXITEM        PIC S9(4) COMP-5.                      
      *}                                                                        
      *---------                          LIGNE FAMILLE                         
             03 COMM-GA07-GA1400.                                               
      *                                                                         
                 05 COMM-GA07-CFAM           PIC X(5).                          
                 05 COMM-GA07-LFAM           PIC X(20).                         
                 05 COMM-GA07-CMODSTOCK      PIC X(5).                          
                 05 COMM-GA07-CTAUXTVA       PIC X(5).                          
                 05 COMM-GA07-CGARANTIE      PIC X(5).                          
                 05 COMM-GA07-QBORNESOL      PIC S99V999 COMP-3.                
                 05 COMM-GA07-QBORNEVRAC     PIC S99V999 COMP-3.                
                 05 COMM-GA07-QNBPSCL        PIC S999    COMP-3.                
                 05 COMM-GA07-WMULTIFAM      PIC X.                             
                 05 COMM-GA07-WSEQFAM        PIC S9(5)   COMP-3.                
                 05 COMM-GA07-CTYPENT        PIC X(2).                          
                 05 COMM-GA07-LTYPENT        PIC X(20).                         
             03 COMM-DECISION       OCCURS 14.                                  
                 05 COMM-GA07-ACTION         PIC X.                             
      *---------    TAB       CODES DESCRIPTS   (PAGE COURANTE)                 
             03 COMM-TABCOD-DESCR.                                              
               05 COMM-TABCOD-ELEM    OCCURS 14.                                
                   07 COMM-GA07-CDESCR         PIC X(5).                        
                   07 COMM-GA07-LDESCR         PIC X(20).                       
      *                                                                         
      *---------                                                                
             03 COMM-GA07-MAXCODES           PIC S9(3)   COMP-3.                
      *---------                                                                
             03 COMM-GA07-WTABLEG.                                              
                05 COMM-GA07-LPARAM         PIC X(20).                          
                05 COMM-GA07-TYPE           PIC X(1).                           
                05 COMM-GA07-TYP2           PIC X(1).                           
                05 COMM-GA07-WGROUPE        PIC X(1).                           
                05 COMM-GA07-REST           PIC X(57).                          
             03 COMM-GA07-CPARAM            PIC X(15).                          
      *-------------------------------ZONE TRANSACTION                          
AB           03 COMM-GA07-TRANSA         PIC X(4).                              
      *------------------------------ ZONE LIBRE                                
AB    *      03 COMM-GA07-LIBRE1         PIC X(3169).                           
             03 COMM-GA07-LIBRE1         PIC X(3143).                           
      *                                                                         
      *****************************************************************         
                                                                                
