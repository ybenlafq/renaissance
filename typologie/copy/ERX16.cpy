      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERX16   ERX16                                              00000020
      ***************************************************************** 00000030
       01   ERX16I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWFONCI   PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPROFL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNPROFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPROFF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNPROFI   PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPROFL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLPROFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLPROFF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLPROFI   PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNMAGI    PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMAGL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLMAGF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLMAGI    PIC X(20).                                      00000370
           02 MDETAILMUTI OCCURS   12 TIMES .                           00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNCODICI     PIC X(7).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNFAML  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MNFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNFAMF  PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNFAMI  PIC X(5).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMARQL      COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNMARQF      PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNMARQI      PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNREFL  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MNREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNREFF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNREFI  PIC X(20).                                      00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MLIBERRI  PIC X(78).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MCODTRAI  PIC X(4).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCICSI    PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MNETNAMI  PIC X(8).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MSCREENI  PIC X(4).                                       00000740
      ***************************************************************** 00000750
      * SDF: ERX16   ERX16                                              00000760
      ***************************************************************** 00000770
       01   ERX16O REDEFINES ERX16I.                                    00000780
           02 FILLER    PIC X(12).                                      00000790
           02 FILLER    PIC X(2).                                       00000800
           02 MDATJOUA  PIC X.                                          00000810
           02 MDATJOUC  PIC X.                                          00000820
           02 MDATJOUP  PIC X.                                          00000830
           02 MDATJOUH  PIC X.                                          00000840
           02 MDATJOUV  PIC X.                                          00000850
           02 MDATJOUO  PIC X(10).                                      00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MTIMJOUA  PIC X.                                          00000880
           02 MTIMJOUC  PIC X.                                          00000890
           02 MTIMJOUP  PIC X.                                          00000900
           02 MTIMJOUH  PIC X.                                          00000910
           02 MTIMJOUV  PIC X.                                          00000920
           02 MTIMJOUO  PIC X(5).                                       00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MWFONCA   PIC X.                                          00000950
           02 MWFONCC   PIC X.                                          00000960
           02 MWFONCP   PIC X.                                          00000970
           02 MWFONCH   PIC X.                                          00000980
           02 MWFONCV   PIC X.                                          00000990
           02 MWFONCO   PIC X(4).                                       00001000
           02 FILLER    PIC X(2).                                       00001010
           02 MNSOCA    PIC X.                                          00001020
           02 MNSOCC    PIC X.                                          00001030
           02 MNSOCP    PIC X.                                          00001040
           02 MNSOCH    PIC X.                                          00001050
           02 MNSOCV    PIC X.                                          00001060
           02 MNSOCO    PIC X(3).                                       00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MNPROFA   PIC X.                                          00001090
           02 MNPROFC   PIC X.                                          00001100
           02 MNPROFP   PIC X.                                          00001110
           02 MNPROFH   PIC X.                                          00001120
           02 MNPROFV   PIC X.                                          00001130
           02 MNPROFO   PIC X(5).                                       00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MLPROFA   PIC X.                                          00001160
           02 MLPROFC   PIC X.                                          00001170
           02 MLPROFP   PIC X.                                          00001180
           02 MLPROFH   PIC X.                                          00001190
           02 MLPROFV   PIC X.                                          00001200
           02 MLPROFO   PIC X(20).                                      00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MNMAGA    PIC X.                                          00001230
           02 MNMAGC    PIC X.                                          00001240
           02 MNMAGP    PIC X.                                          00001250
           02 MNMAGH    PIC X.                                          00001260
           02 MNMAGV    PIC X.                                          00001270
           02 MNMAGO    PIC X(3).                                       00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MLMAGA    PIC X.                                          00001300
           02 MLMAGC    PIC X.                                          00001310
           02 MLMAGP    PIC X.                                          00001320
           02 MLMAGH    PIC X.                                          00001330
           02 MLMAGV    PIC X.                                          00001340
           02 MLMAGO    PIC X(20).                                      00001350
           02 MDETAILMUTO OCCURS   12 TIMES .                           00001360
             03 FILLER       PIC X(2).                                  00001370
             03 MNCODICA     PIC X.                                     00001380
             03 MNCODICC     PIC X.                                     00001390
             03 MNCODICP     PIC X.                                     00001400
             03 MNCODICH     PIC X.                                     00001410
             03 MNCODICV     PIC X.                                     00001420
             03 MNCODICO     PIC X(7).                                  00001430
             03 FILLER       PIC X(2).                                  00001440
             03 MNFAMA  PIC X.                                          00001450
             03 MNFAMC  PIC X.                                          00001460
             03 MNFAMP  PIC X.                                          00001470
             03 MNFAMH  PIC X.                                          00001480
             03 MNFAMV  PIC X.                                          00001490
             03 MNFAMO  PIC X(5).                                       00001500
             03 FILLER       PIC X(2).                                  00001510
             03 MNMARQA      PIC X.                                     00001520
             03 MNMARQC PIC X.                                          00001530
             03 MNMARQP PIC X.                                          00001540
             03 MNMARQH PIC X.                                          00001550
             03 MNMARQV PIC X.                                          00001560
             03 MNMARQO      PIC X(5).                                  00001570
             03 FILLER       PIC X(2).                                  00001580
             03 MNREFA  PIC X.                                          00001590
             03 MNREFC  PIC X.                                          00001600
             03 MNREFP  PIC X.                                          00001610
             03 MNREFH  PIC X.                                          00001620
             03 MNREFV  PIC X.                                          00001630
             03 MNREFO  PIC X(20).                                      00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MLIBERRA  PIC X.                                          00001660
           02 MLIBERRC  PIC X.                                          00001670
           02 MLIBERRP  PIC X.                                          00001680
           02 MLIBERRH  PIC X.                                          00001690
           02 MLIBERRV  PIC X.                                          00001700
           02 MLIBERRO  PIC X(78).                                      00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MCODTRAA  PIC X.                                          00001730
           02 MCODTRAC  PIC X.                                          00001740
           02 MCODTRAP  PIC X.                                          00001750
           02 MCODTRAH  PIC X.                                          00001760
           02 MCODTRAV  PIC X.                                          00001770
           02 MCODTRAO  PIC X(4).                                       00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MCICSA    PIC X.                                          00001800
           02 MCICSC    PIC X.                                          00001810
           02 MCICSP    PIC X.                                          00001820
           02 MCICSH    PIC X.                                          00001830
           02 MCICSV    PIC X.                                          00001840
           02 MCICSO    PIC X(5).                                       00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MNETNAMA  PIC X.                                          00001870
           02 MNETNAMC  PIC X.                                          00001880
           02 MNETNAMP  PIC X.                                          00001890
           02 MNETNAMH  PIC X.                                          00001900
           02 MNETNAMV  PIC X.                                          00001910
           02 MNETNAMO  PIC X(8).                                       00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MSCREENA  PIC X.                                          00001940
           02 MSCREENC  PIC X.                                          00001950
           02 MSCREENP  PIC X.                                          00001960
           02 MSCREENH  PIC X.                                          00001970
           02 MSCREENV  PIC X.                                          00001980
           02 MSCREENO  PIC X(4).                                       00001990
                                                                                
