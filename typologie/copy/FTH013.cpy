      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE LA TABLE RTTH14              *        
      *----------------------------------------------------------------*        
       01  DSECT-FTH013.                                                        
         05 CHAMPS-FTH013.                                                      
           10 FTH013-NCODIC             PIC X(07).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FTH013-COPCO              PIC X(03).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FTH013-C1                 PIC -ZZZZ9,99.                          
           10 FILLER                    PIC X VALUE ';'.                        
           10 FTH013-C2                 PIC -ZZZZ9,99.                          
           10 FILLER                    PIC X VALUE ';'.                        
           10 FTH013-C3                 PIC -ZZZZ9,99.                          
           10 FILLER                    PIC X VALUE ';'.                        
           10 FTH013-C4                 PIC -ZZZZ9,99.                          
           10 FILLER                    PIC X VALUE ';'.                        
           10 FTH013-C5                 PIC -ZZZZ9,99.                          
           10 FILLER                    PIC X VALUE ';'.                        
           10 FTH013-C6                 PIC -ZZZZ9,99.                          
           10 FILLER                    PIC X VALUE ';'.                        
           10 FTH013-C7                 PIC -ZZZZ9,99.                          
           10 FILLER                    PIC X VALUE ';'.                        
           10 FTH013-C8                 PIC -ZZZZ9,99.                          
           10 FILLER                    PIC X VALUE ';'.                        
           10 FTH013-C9                 PIC -ZZZZ9,99.                          
           10 FILLER                    PIC X VALUE ';'.                        
           10 FTH013-C10                PIC -ZZZZ9,99.                          
           10 FILLER                    PIC X VALUE ';'.                        
           10 FTH013-C11                PIC -ZZZZ9,99.                          
           10 FILLER                    PIC X VALUE ';'.                        
           10 FTH013-C12                PIC -ZZZZ9,99.                          
           10 FILLER                    PIC X VALUE ';'.                        
           10 FTH013-DEFFET             PIC X(08).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FTH013-DENVOI             PIC X(08).                              
           10 FILLER                    PIC X VALUE ';'.                        
         05 FILLER                      PIC X(150) VALUE SPACES.                
       01 FTH013-LIBELLES.                                                      
         05 FILLER                      PIC X(05) VALUE 'CODIC'.                
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(05) VALUE 'COPCO'.                
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(02) VALUE 'C1'.                   
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(02) VALUE 'C2'.                   
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(02) VALUE 'C3'.                   
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(02) VALUE 'C4'.                   
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(02) VALUE 'C5'.                   
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(02) VALUE 'C6'.                   
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(02) VALUE 'C7'.                   
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(02) VALUE 'C8'.                   
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(02) VALUE 'C9'.                   
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(03) VALUE 'C10'.                  
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(03) VALUE 'C11'.                  
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(03) VALUE 'C12'.                  
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(06) VALUE 'DEFFET'.               
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(06) VALUE 'DENVOI'.               
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(235) VALUE SPACES.                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  DSECT-FTH013-LONG           PIC S9(4)   COMP  VALUE +300.            
      *                                                                         
      *--                                                                       
       01  DSECT-FTH013-LONG           PIC S9(4) COMP-5  VALUE +300.            
                                                                                
      *}                                                                        
