      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-RX55-LONG             PIC S9(4) COMP-3 VALUE +960.                
       01  TS-RX55-RECORD.                                                      
           05 TS-RX55-LIGNE                OCCURS 16.                           
              10 TS-RX55-ZP         PIC X(2).                                   
              10 TS-RX55-NMAG       PIC X(3).                                   
              10 TS-RX55-NCONC      PIC X(4).                                   
              10 TS-RX55-LENSCONC   PIC X(15).                                  
              10 TS-RX55-CFAM       PIC X(5).                                   
              10 TS-RX55-NCODIC     PIC X(7).                                   
              10 TS-RX55-NRELEVE    PIC X(7).                                   
              10 TS-RX55-TYPE       PIC X(4).                                   
              10 TS-RX55-DATE       PIC X(8).                                   
              10 TS-RX55-PRELEVE    PIC S9(7)V99     COMP-3.                    
                                                                                
