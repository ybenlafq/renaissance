      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GVCPL PLAGE INTERDITE JOUR DE MUT      *        
      *----------------------------------------------------------------*        
       01  RVGA01W4.                                                            
           05  GVCPL-CTABLEG2    PIC X(15).                                     
           05  GVCPL-CTABLEG2-REDEF REDEFINES GVCPL-CTABLEG2.                   
               10  GVCPL-NSOCENTR        PIC X(06).                             
               10  GVCPL-LDEPLIV         PIC X(06).                             
               10  GVCPL-CMODDEL         PIC X(03).                             
           05  GVCPL-WTABLEG     PIC X(80).                                     
           05  GVCPL-WTABLEG-REDEF  REDEFINES GVCPL-WTABLEG.                    
               10  GVCPL-WACTIF          PIC X(01).                             
               10  GVCPL-CPLAGES         PIC X(30).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01W4-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GVCPL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GVCPL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GVCPL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GVCPL-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
