      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-NV10-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.          00000020
      *--                                                                       
       01  COMM-NV10-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                
      *}                                                                        
      *                                                                 00000030
       01  Z-COMMAREA.                                                  00000040
                                                                        00000050
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00000060
           02 FILLER-COM-AIDA      PIC X(100).                          00000070
                                                                        00000080
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00000090
           02 COMM-CICS-APPLID     PIC X(08).                           00000100
           02 COMM-CICS-NETNAM     PIC X(08).                           00000110
           02 COMM-CICS-TRANSA     PIC X(04).                           00000120
                                                                        00000130
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00000140
           02 COMM-DATE-SIECLE     PIC X(02).                           00000150
           02 COMM-DATE-ANNEE      PIC X(02).                           00000160
           02 COMM-DATE-MOIS       PIC X(02).                           00000170
           02 COMM-DATE-JOUR       PIC 99.                              00000180
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00000190
           02 COMM-DATE-QNTA       PIC 999.                             00000200
           02 COMM-DATE-QNT0       PIC 99999.                           00000210
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00000220
           02 COMM-DATE-BISX       PIC 9.                               00000230
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00000240
           02 COMM-DATE-JSM        PIC 9.                               00000250
      *   LIBELLES DU JOUR COURT - LONG                                 00000260
           02 COMM-DATE-JSM-LC     PIC X(03).                           00000270
           02 COMM-DATE-JSM-LL     PIC X(08).                           00000280
      *   LIBELLES DU MOIS COURT - LONG                                 00000290
           02 COMM-DATE-MOIS-LC    PIC X(03).                           00000300
           02 COMM-DATE-MOIS-LL    PIC X(08).                           00000310
      *   DIFFERENTES FORMES DE DATE                                    00000320
           02 COMM-DATE-SSAAMMJJ   PIC X(08).                           00000330
           02 COMM-DATE-AAMMJJ     PIC X(06).                           00000340
           02 COMM-DATE-JJMMSSAA   PIC X(08).                           00000350
           02 COMM-DATE-JJMMAA     PIC X(06).                           00000360
           02 COMM-DATE-JJ-MM-AA   PIC X(08).                           00000370
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00000380
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00000390
           02 COMM-DATE-WEEK.                                           00000400
              05 COMM-DATE-SEMSS   PIC 99.                              00000410
              05 COMM-DATE-SEMAA   PIC 99.                              00000420
              05 COMM-DATE-SEMNU   PIC 99.                              00000430
           02 COMM-DATE-FILLER     PIC X(08).                           00000440
      *   ZONES RESERVEES TRAITEMENT DU SWAP                            00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00000460
      *                                                                         
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
                                                                        00000470
      *}                                                                        
      *--  ZONES RESERVEES APPLICATIVES COMMUNES ----------------- 3874 00000480
                                                                        00000490
           02 COMM-NV10-ENTETE.                                         00000500
              03 COMM-ENTETE.                                           00000510
                 05 COMM-CODLANG            PIC X(02).                  00000520
                 05 COMM-CODPIC             PIC X(02).                  00000530
                 05 COMM-CODESFONCTION  .                               00000540
                    10 COMM-CODE-FONCTION      OCCURS 3.                   00000
                       15  COMM-CFONC     PIC  X(03).                           
                 05 COMM-CFONCTION-OPTION   PIC X(03).                  00000530
                 05 COMM-11-CFONC-OPTION    PIC X(03).                          
                 05 COMM-MODE-INTEROGATION    PIC X VALUE ' '.                  
                     88 NON-MODE-INTEROGATION VALUE ' '.                        
                     88     MODE-INTEROGATION VALUE '1'.                        
                 05 COMM-COPTION            PIC X(02).                  00000530
                 05 COMM-LEVEL-MAX          PIC X(06).                  00000500
      *--  N'EST PAS UTILISEE POUR L'INSTANT                            00000480
                 05 COMM-NV10-LIBRE        PIC X(44).                   00000500
           02 COMM-NV10-FILLER              PIC X(3800).                00000620
      *--  ZONES RESERVEES APPLICATIVES OPTION 1 ----------------- 3854 00000020
                                                                        00000030
           02 COMM-NV11-APPLI REDEFINES COMM-NV10-FILLER.               00000040
      *    COMM-PC12-APPLI   EST LE MEME QUE PC11                       00000040
              03 COMM-11.                                               00000050
                 05 COMM-11-MODE-INTERO     PIC X(01).                          
                     88     11-MODE-INTERO    VALUE '1'.                        
      *-                              GESTION SUPPRESSION LIGNE                 
                 05  COMM-11-CONFIRM-SUP     PIC X(01).                         
                     88 11-CONFIRM-SUP           VALUE '1'.                     
                 05  COMM-11-DEMANDE-CONFIRM PIC X(01).                         
                     88 11-DEMANDE-CONFIRM       VALUE '1'.                     
      *-                              NUMERO PAGE COURANTE                      
                 05 COMM-11-NUMPAG      PIC 9(03).                              
      *-                              NUMERO PAGE MAXI                          
                 05 COMM-11-PAGE-MAX    PIC 9(03).                              
      *-                              NUMERO PAGE ECRAN                         
                 05 COMM-11-ECRAN       PIC 9(03).                              
      *-                              NUMERO PAGE ECRAN MAXI                    
                 05 COMM-11-ECRAN-MAX PIC 9(03).                                
      *-                              NB LIGNES                                 
                 05 COMM-11-I-CHOIX-MAX    PIC 9(02).                           
      *-                              LIGNE EN COURS                            
                 05 COMM-11-I-CHOIX        PIC 9(02).                           
      *                               CODE TRAITEMENT (MAJ CRE)                 
                 05 COMM-11-CODETR           PIC X(03).                         
      *                                                                         
                 05 COMM-11-INDMAX          PIC S9(5) COMP-3.           00000080
                 05 COMM-11-ENTETE1         PIC X(55).                          
                 05 COMM-11-ENTETE2         PIC X(55).                          
                 05 COMM-11-ENTETE-DTD      PIC X(20).                          
      *             L"ALIMENTATION DE L"ECRAN                           00000040
                 05 COMM-11-VAL-ECRAN.                                  00000090
                    10 COMM-11-LIGNE  OCCURS 10.                                
                       15 COMM-11-CDTDWCS      PIC X(20).                       
                       15 COMM-11-ECRAN1.                                       
                          20 COMM-11-CACTION      PIC X(1).                     
                          20 COMM-11-CNOMTS       PIC X(8).                     
                          20 COMM-11-CNOMTRA      PIC X(4).                     
                          20 COMM-11-CNOMEXTRACT  PIC X(6).                     
                          20 COMM-11-QNBATTENTE   PIC ZZZZZZ9.                  
                          20 COMM-11-QDELAIATT    PIC ZZZZ9.                    
                          20 COMM-11-CFONCTION    PIC X(3).                     
                          20 COMM-11-CDTDNCG      PIC X(20).                    
                       15 COMM-11-ECRAN2.                                       
                          20 COMM-11-QMSGALERT    PIC ZZZZZZ9.                  
                          20 COMM-11-QMSGREACT    PIC ZZZZZZ9.                  
                          20 COMM-11-QMSGERROR    PIC ZZZZZZ9.                  
                          20 COMM-11-QMSGOK       PIC ZZZZZZ9.                  
                          20 COMM-11-QMSGSSRET    PIC ZZZZZZ9.                  
                 05 COMM-11-FILLER PIC X(197).                          00000550
              03 COMM-12.                                               00000050
      *!            ATTENTION LA DESCRIPTION DE CETTE LIGNE                     
      *!            DOIT ETRE IDENTIQUE � CELLE DE LA LIGNE DU TABLEAUX         
                    10 COMM-12-LIGNE.                                           
                       15 COMM-12-CDTDWCS      PIC X(20).                       
      *                   LES FLUX D"ENVOI                                      
                       15 COMM-12-CACTION      PIC X(1).                        
                       15 COMM-12-CNOMTS       PIC X(8).                        
                       15 COMM-12-CNOMTRA      PIC X(4).                        
                       15 COMM-12-CNOMEXTRACT  PIC X(6).                        
                       15 COMM-12-QNBATTENTE   PIC ZZZZZZ9.                     
                       15 COMM-12-QDELAIATT    PIC ZZZZ9.                       
                       15 COMM-12-CFONCTION    PIC X(3).                        
                       15 COMM-12-CDTDNCG      PIC X(20).                       
      *                   LES FLUS DE LA RECEPTION                              
                       15 COMM-12-QMSGALERT    PIC ZZZZZZ9.                     
                       15 COMM-12-QMSGREACT    PIC ZZZZZZ9.                     
                       15 COMM-12-QMSGERROR    PIC ZZZZZZ9.                     
                       15 COMM-12-QMSGOK       PIC ZZZZZZ9.                     
                       15 COMM-12-QMSGSSRET    PIC ZZZZZZ9.                     
      *                FLAG MODIF NOM TS                                        
                    10 COMM-12-MODIF-TS.                                00000550
      *                FLAG MODIF DONNEES TS 1 FOIS                             
                       15 MODIF-TS      PIC X(01).                         00000
      *                FLAG MODIF DONNEES TS : UNICITE DE DONNEES PAR TS        
                       15 MODIF-TS-INFO PIC X(01).                         00000
                 05 COMM-12-FILLER PIC X(195).                          00000550
                 05 COMM-12-FILLER PIC X(1800).                         00000550
                                                                                
                                                                        00000030
