      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *===============================================================* 00010000
      *                                                               * 00020000
      *      TS ASSOCIE A LA TRANSACTION GA38                         * 00030000
      *            AJOUT DE L'ECOTAXE AU PRODUIT                      * 00040000
      *            AFFICHAGE HISTORIQUE                               * 00050000
      *===============================================================* 00060000
                                                                        00070000
       01 TS-GA38H.                                                     00080000
          05 TS-GA38H-LONG             PIC S9(5) COMP-3     VALUE +53.  00090002
          05 TS-GA38H-DONNEES.                                          00100000
             10 TS-GA38H-CPAYS         PIC  X(2).                       00120000
             10 TS-GA38H-DEFFETA       PIC  X(8).                       00130001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      10 TS-GA38H-MONTANTA      PIC  S9(5)V99 COMP.              00140001
      *--                                                                       
             10 TS-GA38H-MONTANTA      PIC  S9(5)V99 COMP-5.                    
      *}                                                                        
             10 TS-GA38H-DEFFETV       PIC  X(8).                       00141001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      10 TS-GA38H-MONTANTV      PIC  S9(5)V99 COMP.              00142001
      *--                                                                       
             10 TS-GA38H-MONTANTV      PIC  S9(5)V99 COMP-5.                    
      *}                                                                        
             10 TS-GA38H-CFOURN        PIC  X(5).                       00150000
             10 TS-GA38H-LFOURN        PIC  X(20).                      00160000
             10 TS-GA38H-WTFOUR        PIC  X(01).                      00220001
             10 TS-GA38H-WIMPORT       PIC  X(01).                      00230002
      *===============================================================* 00400000
                                                                                
