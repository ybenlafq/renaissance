      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
           03  COMMGG11 REDEFINES COMM-GG00-FILLER.                             
             05 COMM-GG11-TABLE-DEMANDE-ALIG.                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       10 COMM-GG11-NBR-NDEMALI    PIC S9(4)     COMP.                   
      *--                                                                       
              10 COMM-GG11-NBR-NDEMALI    PIC S9(4) COMP-5.                     
      *}                                                                        
              10 COMM-GG11-TAB-NDEMALI    PIC 9(7) OCCURS 100.                  
             05 COMM-GG11-MAP.                                                  
              10 COMM-GG11-CORIG          PIC X(01).                            
              10 COMM-GG11-LFAM           PIC X(20).                            
              10 COMM-GG11-PGM            PIC X(05).                            
              10 COMM-GG11-PRIX-MODIFIE   PIC X(03).                            
              10 COMM-GG11-LMARQ          PIC X(20).                            
              10 COMM-GG11-PEXPTTC        PIC S9(7)V99  COMP-3.                 
              10 COMM-GG11-PCF            PIC S9(7)V99  COMP-3.                 
              10 COMM-GG11-DATDEB         PIC 9(8).                             
              10 COMM-GG11-DATANC         PIC 9(8).                             
              10 COMM-GG11-DATSUIV        PIC 9(8).                             
              10 COMM-GG11-QDATDEB        PIC 9(5).                             
              10 COMM-GG11-DATFIN         PIC 9(8).                             
              10 COMM-GG11-WMULTIFAM      PIC X.                                
              10 COMM-GG11-MAGASIN.                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          15 COMM-GG11-NBR-MAG     PIC S9(4)     COMP.                   
      *--                                                                       
                 15 COMM-GG11-NBR-MAG     PIC S9(4) COMP-5.                     
      *}                                                                        
                 15 COMM-GG11-NMAGASIN    OCCURS 8.                             
                    20 COMM-GG11-NMAG     PIC 9(3).                             
                    20 COMM-GG11-LMAG     PIC X(20).                            
              10 COMM-GG11-PRMP           PIC S9(7)V99  COMP-3.                 
              10 COMM-GG11-PMBU           PIC S9(7)V99  COMP-3.                 
              10 COMM-GG11-QTMB           PIC S9(3)V9(4)   COMP-3.              
             05 COMM-GG11-PRIX.                                                 
              10 COMM-GG11-QTAUXTVA       PIC S9V9(4)  COMP-3.                  
              10 COMM-GG11-PSTDMINI       PIC S9(7)V99 COMP-3.                  
             05 COMM-GG11-SIMILAIRE       PIC X.                                
                                                                                
