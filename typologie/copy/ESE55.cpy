      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESE55   ESE55                                              00000020
      ***************************************************************** 00000030
       01   ESE55I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCSERVL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNCSERVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCSERVF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNCSERVI  PIC X(5).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCSERVL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLCSERVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCSERVF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLCSERVI  PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPERL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCOPERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOPERF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCOPERI   PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLOPERL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLOPERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLOPERF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLOPERI   PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCFAMI    PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLFAMI    PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MPAGEI    PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MPAGEMAXI      PIC X(3).                                  00000490
           02 MWTABLEI OCCURS   8 TIMES .                               00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNENTCDEL    COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MNENTCDEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNENTCDEF    PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNENTCDEI    PIC X(5).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLENTCDEL    COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MLENTCDEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLENTCDEF    PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MLENTCDEI    PIC X(20).                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWENTCDEL    COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MWENTCDEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWENTCDEF    PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MWENTCDEI    PIC X.                                     00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSUPPL  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MSUPPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSUPPF  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MSUPPI  PIC X.                                          00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTCDE1L     COMP PIC S9(4).                            00000670
      *--                                                                       
           02 MNENTCDE1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNENTCDE1F     PIC X.                                     00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MNENTCDE1I     PIC X(5).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWENTCDE1L     COMP PIC S9(4).                            00000710
      *--                                                                       
           02 MWENTCDE1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MWENTCDE1F     PIC X.                                     00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MWENTCDE1I     PIC X.                                     00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MZONCMDI  PIC X(15).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MLIBERRI  PIC X(58).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCODTRAI  PIC X(4).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCICSI    PIC X(5).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MNETNAMI  PIC X(8).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MSCREENI  PIC X(4).                                       00000980
      ***************************************************************** 00000990
      * SDF: ESE55   ESE55                                              00001000
      ***************************************************************** 00001010
       01   ESE55O REDEFINES ESE55I.                                    00001020
           02 FILLER    PIC X(12).                                      00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MDATJOUA  PIC X.                                          00001050
           02 MDATJOUC  PIC X.                                          00001060
           02 MDATJOUP  PIC X.                                          00001070
           02 MDATJOUH  PIC X.                                          00001080
           02 MDATJOUV  PIC X.                                          00001090
           02 MDATJOUO  PIC X(10).                                      00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MTIMJOUA  PIC X.                                          00001120
           02 MTIMJOUC  PIC X.                                          00001130
           02 MTIMJOUP  PIC X.                                          00001140
           02 MTIMJOUH  PIC X.                                          00001150
           02 MTIMJOUV  PIC X.                                          00001160
           02 MTIMJOUO  PIC X(5).                                       00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MWFONCA   PIC X.                                          00001190
           02 MWFONCC   PIC X.                                          00001200
           02 MWFONCP   PIC X.                                          00001210
           02 MWFONCH   PIC X.                                          00001220
           02 MWFONCV   PIC X.                                          00001230
           02 MWFONCO   PIC X(3).                                       00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MNCSERVA  PIC X.                                          00001260
           02 MNCSERVC  PIC X.                                          00001270
           02 MNCSERVP  PIC X.                                          00001280
           02 MNCSERVH  PIC X.                                          00001290
           02 MNCSERVV  PIC X.                                          00001300
           02 MNCSERVO  PIC X(5).                                       00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MLCSERVA  PIC X.                                          00001330
           02 MLCSERVC  PIC X.                                          00001340
           02 MLCSERVP  PIC X.                                          00001350
           02 MLCSERVH  PIC X.                                          00001360
           02 MLCSERVV  PIC X.                                          00001370
           02 MLCSERVO  PIC X(20).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MCOPERA   PIC X.                                          00001400
           02 MCOPERC   PIC X.                                          00001410
           02 MCOPERP   PIC X.                                          00001420
           02 MCOPERH   PIC X.                                          00001430
           02 MCOPERV   PIC X.                                          00001440
           02 MCOPERO   PIC X(5).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MLOPERA   PIC X.                                          00001470
           02 MLOPERC   PIC X.                                          00001480
           02 MLOPERP   PIC X.                                          00001490
           02 MLOPERH   PIC X.                                          00001500
           02 MLOPERV   PIC X.                                          00001510
           02 MLOPERO   PIC X(20).                                      00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MCFAMA    PIC X.                                          00001540
           02 MCFAMC    PIC X.                                          00001550
           02 MCFAMP    PIC X.                                          00001560
           02 MCFAMH    PIC X.                                          00001570
           02 MCFAMV    PIC X.                                          00001580
           02 MCFAMO    PIC X(5).                                       00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MLFAMA    PIC X.                                          00001610
           02 MLFAMC    PIC X.                                          00001620
           02 MLFAMP    PIC X.                                          00001630
           02 MLFAMH    PIC X.                                          00001640
           02 MLFAMV    PIC X.                                          00001650
           02 MLFAMO    PIC X(20).                                      00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MPAGEA    PIC X.                                          00001680
           02 MPAGEC    PIC X.                                          00001690
           02 MPAGEP    PIC X.                                          00001700
           02 MPAGEH    PIC X.                                          00001710
           02 MPAGEV    PIC X.                                          00001720
           02 MPAGEO    PIC X(3).                                       00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MPAGEMAXA      PIC X.                                     00001750
           02 MPAGEMAXC PIC X.                                          00001760
           02 MPAGEMAXP PIC X.                                          00001770
           02 MPAGEMAXH PIC X.                                          00001780
           02 MPAGEMAXV PIC X.                                          00001790
           02 MPAGEMAXO      PIC X(3).                                  00001800
           02 MWTABLEO OCCURS   8 TIMES .                               00001810
             03 FILLER       PIC X(2).                                  00001820
             03 MNENTCDEA    PIC X.                                     00001830
             03 MNENTCDEC    PIC X.                                     00001840
             03 MNENTCDEP    PIC X.                                     00001850
             03 MNENTCDEH    PIC X.                                     00001860
             03 MNENTCDEV    PIC X.                                     00001870
             03 MNENTCDEO    PIC X(5).                                  00001880
             03 FILLER       PIC X(2).                                  00001890
             03 MLENTCDEA    PIC X.                                     00001900
             03 MLENTCDEC    PIC X.                                     00001910
             03 MLENTCDEP    PIC X.                                     00001920
             03 MLENTCDEH    PIC X.                                     00001930
             03 MLENTCDEV    PIC X.                                     00001940
             03 MLENTCDEO    PIC X(20).                                 00001950
             03 FILLER       PIC X(2).                                  00001960
             03 MWENTCDEA    PIC X.                                     00001970
             03 MWENTCDEC    PIC X.                                     00001980
             03 MWENTCDEP    PIC X.                                     00001990
             03 MWENTCDEH    PIC X.                                     00002000
             03 MWENTCDEV    PIC X.                                     00002010
             03 MWENTCDEO    PIC X.                                     00002020
             03 FILLER       PIC X(2).                                  00002030
             03 MSUPPA  PIC X.                                          00002040
             03 MSUPPC  PIC X.                                          00002050
             03 MSUPPP  PIC X.                                          00002060
             03 MSUPPH  PIC X.                                          00002070
             03 MSUPPV  PIC X.                                          00002080
             03 MSUPPO  PIC X.                                          00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MNENTCDE1A     PIC X.                                     00002110
           02 MNENTCDE1C     PIC X.                                     00002120
           02 MNENTCDE1P     PIC X.                                     00002130
           02 MNENTCDE1H     PIC X.                                     00002140
           02 MNENTCDE1V     PIC X.                                     00002150
           02 MNENTCDE1O     PIC X(5).                                  00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MWENTCDE1A     PIC X.                                     00002180
           02 MWENTCDE1C     PIC X.                                     00002190
           02 MWENTCDE1P     PIC X.                                     00002200
           02 MWENTCDE1H     PIC X.                                     00002210
           02 MWENTCDE1V     PIC X.                                     00002220
           02 MWENTCDE1O     PIC X.                                     00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MZONCMDA  PIC X.                                          00002250
           02 MZONCMDC  PIC X.                                          00002260
           02 MZONCMDP  PIC X.                                          00002270
           02 MZONCMDH  PIC X.                                          00002280
           02 MZONCMDV  PIC X.                                          00002290
           02 MZONCMDO  PIC X(15).                                      00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLIBERRA  PIC X.                                          00002320
           02 MLIBERRC  PIC X.                                          00002330
           02 MLIBERRP  PIC X.                                          00002340
           02 MLIBERRH  PIC X.                                          00002350
           02 MLIBERRV  PIC X.                                          00002360
           02 MLIBERRO  PIC X(58).                                      00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MCODTRAA  PIC X.                                          00002390
           02 MCODTRAC  PIC X.                                          00002400
           02 MCODTRAP  PIC X.                                          00002410
           02 MCODTRAH  PIC X.                                          00002420
           02 MCODTRAV  PIC X.                                          00002430
           02 MCODTRAO  PIC X(4).                                       00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MCICSA    PIC X.                                          00002460
           02 MCICSC    PIC X.                                          00002470
           02 MCICSP    PIC X.                                          00002480
           02 MCICSH    PIC X.                                          00002490
           02 MCICSV    PIC X.                                          00002500
           02 MCICSO    PIC X(5).                                       00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MNETNAMA  PIC X.                                          00002530
           02 MNETNAMC  PIC X.                                          00002540
           02 MNETNAMP  PIC X.                                          00002550
           02 MNETNAMH  PIC X.                                          00002560
           02 MNETNAMV  PIC X.                                          00002570
           02 MNETNAMO  PIC X(8).                                       00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MSCREENA  PIC X.                                          00002600
           02 MSCREENC  PIC X.                                          00002610
           02 MSCREENP  PIC X.                                          00002620
           02 MSCREENH  PIC X.                                          00002630
           02 MSCREENV  PIC X.                                          00002640
           02 MSCREENO  PIC X(4).                                       00002650
                                                                                
