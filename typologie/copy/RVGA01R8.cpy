      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CRMAG ORGANISMES CR�DIT PAR MAGASIN    *        
      *----------------------------------------------------------------*        
       01  RVGA01R8.                                                            
           05  CRMAG-CTABLEG2    PIC X(15).                                     
           05  CRMAG-CTABLEG2-REDEF REDEFINES CRMAG-CTABLEG2.                   
               10  CRMAG-NSOCGRP         PIC X(03).                             
               10  CRMAG-NSOCIETE        PIC X(03).                             
               10  CRMAG-NLIEU           PIC X(03).                             
           05  CRMAG-WTABLEG     PIC X(80).                                     
           05  CRMAG-WTABLEG-REDEF  REDEFINES CRMAG-WTABLEG.                    
               10  CRMAG-WACTIF          PIC X(01).                             
               10  CRMAG-LLIEU           PIC X(20).                             
               10  CRMAG-CCRED           PIC X(05).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01R8-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CRMAG-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CRMAG-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CRMAG-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CRMAG-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
