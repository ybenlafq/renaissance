      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EFENV DEST STOCK DES ENVOIS GEF        *        
      *----------------------------------------------------------------*        
       01  RVGA01KY.                                                            
           05  EFENV-CTABLEG2    PIC X(15).                                     
           05  EFENV-CTABLEG2-REDEF REDEFINES EFENV-CTABLEG2.                   
               10  EFENV-CTRAIT          PIC X(05).                             
               10  EFENV-CRENDU          PIC X(05).                             
           05  EFENV-WTABLEG     PIC X(80).                                     
           05  EFENV-WTABLEG-REDEF  REDEFINES EFENV-WTABLEG.                    
               10  EFENV-WACTIF          PIC X(01).                             
               10  EFENV-ENVORIG         PIC X(14).                             
               10  EFENV-ENVDEST         PIC X(14).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01KY-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EFENV-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EFENV-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EFENV-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EFENV-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
