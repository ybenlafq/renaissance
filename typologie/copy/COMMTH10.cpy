      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      *         MAQUETTE COMMAREA STANDARD AIDA COBOL2             *            
      **************************************************************            
      *                                                                         
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET                    
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR          
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU                  
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                            
      *                                                                         
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                              
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES               
      * PAR AIDA                                                                
      *                                                                         
      *-----------------------------------------------------------------        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-TH10-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-TH10-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA -----------------------------------------        
           02 FILLER-COM-AIDA      PIC X(100).                                  
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS --------------------------        
           02 COMM-CICS-APPLID     PIC X(8).                                    
           02 COMM-CICS-NETNAM     PIC X(8).                                    
           02 COMM-CICS-TRANSA     PIC X(4).                                    
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------------        
           02 COMM-DATE-SIECLE     PIC XX.                                      
           02 COMM-DATE-ANNEE      PIC XX.                                      
           02 COMM-DATE-MOIS       PIC XX.                                      
           02 COMM-DATE-JOUR       PIC 99.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA       PIC 999.                                     
           02 COMM-DATE-QNT0       PIC 99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX       PIC 9.                                       
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM        PIC 9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC     PIC XXX.                                     
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC    PIC XXX.                                     
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                                    
           02 COMM-DATE-AAMMJJ     PIC X(6).                                    
           02 COMM-DATE-JJMMSSAA   PIC X(8).                                    
           02 COMM-DATE-JJMMAA     PIC X(6).                                    
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                                    
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS   PIC 99.                                      
              05 COMM-DATE-SEMAA   PIC 99.                                      
              05 COMM-DATE-SEMNU   PIC 99.                                      
      *                                                                         
           02 COMM-DATE-FILLER     PIC X(08).                                   
      *                                                                         
      * ZONES RESERVEES TRAITEMENT DU SWAP -----------------------------        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                     
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 180 PIC X(1).                               
      *                                                                         
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*        
      *>>>>>>>>>>>>>>>>> ZONES RESERVEES APPLICATIVES <<<<<<<<<<<<<<<<<*        
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*        
      *                                                                         
      *----------------------------------------------------------------*        
      * MENU GENERAL DE L'APPLICATION (PROGRAMME TTH10)                *        
      *----------------------------------------------------------------*        
      *                                                                         
           02 COMM-TH10-MENU.                                                   
      *..............................................DEFFET                     
              03 COMM-TH10-DEFFET         PIC X(8).                             
      *..............................................CODIC                      
              03 COMM-TH10-NCODIC         PIC X(7).                             
      *..............................................OPCO NSOC                  
              03 COMM-TH10-COPCO          PIC X(3).                             
      *..............................................OPCO CSOC                  
              03 COMM-TH10-CSOC           PIC X(3).                             
      *..............................................OPCO LSOC                  
              03 COMM-TH10-LSOC           PIC X(20).                            
      *..............................................HISTO                      
              03 COMM-TH10-HISTO          PIC X(1).                             
      *..............................................FONCTION                   
              03 COMM-TH10-CFONC          PIC X(3).                             
              03 COMM-TH10-LREFFOURN      PIC X(20).                            
              03 COMM-TH10-CFAM           PIC X(05).                            
              03 COMM-TH10-CMARQ          PIC X(05).                            
              03 COMM-TH10-DATEJ1         PIC X(10).                            
              03 COMM-TH10-DATEJ1-8       PIC X(08).                            
              03 COMM-CODLANG             PIC X(02).                            
              03 COMM-CODPIC              PIC X(02).                            
              03 COMM-TH11-DATA.                                                
               05 COMM-TH11-INDTS         PIC S9(5) COMP-3.                     
               05 COMM-TH11-INDMAX        PIC S9(5) COMP-3.                     
               05 COMM-TH11-ENTETE.                                             
                  10 COMM-TH11-NCODIC        PIC X(07).                         
                  10 COMM-TH11-DONNEES.                                         
                     15 COMM-TH11-LFAM       PIC X(20).                         
                     15 COMM-TH11-LMARQ      PIC X(20).                         
              03 COMM-TH12-DATA.                                                
               05 COMM-TH12-INDTS         PIC S9(5) COMP-3.                     
               05 COMM-TH12-INDMAX        PIC S9(5) COMP-3.                     
               05 COMM-TH12-ENTETE.                                             
                  10 COMM-TH12-NCODIC        PIC X(07).                         
                  10 COMM-TH12-COPCO         PIC X(07).                         
                  10 COMM-TH12-DONNEES.                                         
                     15 COMM-TH12-LFAM       PIC X(20).                         
                     15 COMM-TH12-LMARQ      PIC X(20).                         
                     15 COMM-TH12-LOPCO      PIC X(20).                         
              03 COMM-TH20-DATA.                                                
               05 COMM-TH20-INDTS        PIC S9(5) COMP-3.                      
               05 COMM-TH20-INDMAX       PIC S9(5) COMP-3.                      
               05 COMM-TH20-ENTETE.                                             
                  10 COMM-TH20-NCODIC       PIC X(07).                          
                  10 COMM-TH20-COPCO        PIC X(07).                          
                  10 COMM-TH20-DONNEES.                                         
                     15 COMM-TH20-LFAM      PIC X(20).                          
                     15 COMM-TH20-LMARQ     PIC X(20).                          
                     15 COMM-TH20-LOPCO     PIC X(20).                          
                     15 COMM-TH20-HISTO     PIC X(01).                          
                     15 COMM-TH20-DEFFET    PIC X(10).                          
      *                                                                         
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*        
      *>>>>>>>>>>>>>>>>>>>>>> OCTETS DISPONIBLES <<<<<<<<<<<<<<<<<<<<<<*        
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*        
      *                                                                         
           02 COMM-TH10-APPLI             PIC X(3000).                          
           02 COMM-TH16-APPLI REDEFINES COMM-TH10-APPLI.                        
              03 COMM-TH16-FLAG-USER              PIC X.                        
                 88  COMM-TH16-USER-AUTOR      VALUE 'O'.                       
                 88  COMM-TH16-USER-AUTOR-N    VALUE 'N'.                       
           02 COMM-TH17-APPLI REDEFINES COMM-TH10-APPLI.                        
              03 COMM-TH17-COPCO        PIC X(03).                              
              03 COMM-TH17-LOPCO        PIC X(20).                              
              03 COMM-TH17-COPCODUPLI   PIC X(03).                              
              03 COMM-TH17-LOPCODUPLI   PIC X(20).                              
              03 COMM-TH17-MODIF        PIC X(01).                              
              03 COMM-TH17-CONFIRM-CHG  PIC X(01).                              
              03 COMM-TH17-CONFIRM-PF3  PIC X(01).                              
              03 COMM-TH17-TABLEAU-TH08.                                        
                 05 COMM-TH17-TTH08-LISTE-PARAM    OCCURS 12.                   
                    07 COMM-TH17-TTH08-CPARAM       PIC X(10).                  
                    07 COMM-TH17-TTH08-LPARAM       PIC X(20).                  
                    07 COMM-TH17-TTH08-WFLAG        PIC X(01).                  
                    07 COMM-TH17-TTH08-LVPARAM      PIC X(50).                  
                    07 COMM-TH17-TTH08-LPARAM-S     PIC X(20).                  
                    07 COMM-TH17-TTH08-WFLAG-S      PIC X(01).                  
                    07 COMM-TH17-TTH08-LVPARAM-S    PIC X(50).                  
           02 COMM-TH18-APPLI REDEFINES COMM-TH10-APPLI.                        
              03 COMM-TH18-COPCO        PIC X(03).                              
              03 COMM-TH18-LOPCO        PIC X(20).                              
              03 COMM-TH18-COPCODUPLI   PIC X(03).                              
              03 COMM-TH18-LOPCODUPLI   PIC X(20).                              
              03 COMM-TH18-MODIF        PIC X(01).                              
              03 COMM-TH18-WDATEEF      PIC X(01).                              
              03 COMM-TH18-CONFIRM-CHG  PIC X(01).                              
              03 COMM-TH18-CONFIRM-PF3  PIC X(01).                              
              03 COMM-TH18-TABLEAU-TH08.                                        
                 05 COMM-TH18-TTH08-LISTE-PARAM    OCCURS 12.                   
                    07 COMM-TH18-TTH09-COMPOS       PIC X(03).                  
                    07 COMM-TH18-TTH09-DEFFET       PIC X(08).                  
                    07 COMM-TH18-TTH09-WACTIF       PIC X(01).                  
                    07 COMM-TH18-TTH09-WINCLUS      PIC X(01).                  
                    07 COMM-TH18-TTH09-IDREGLE      PIC X(10).                  
                    07 COMM-TH18-TTH09-LIBRG        PIC X(20).                  
                    07 COMM-TH18-TTH09-WSVAL        PIC X(01).                  
                    07 COMM-TH18-TTH09-WSPOURC      PIC X(01).                  
                    07 COMM-TH18-TTH09-MTTREF       PIC X(05).                  
                    07 COMM-TH18-TTH09-WBVAL        PIC X(01).                  
                    07 COMM-TH18-TTH09-WBPOURC      PIC X(01).                  
                    07 COMM-TH18-TTH09-WDELTA       PIC X(01).                  
                    07 COMM-TH18-TTH09-COMPOS-S     PIC X(03).                  
                    07 COMM-TH18-TTH09-DEFFET-S     PIC X(08).                  
                    07 COMM-TH18-TTH09-WACTIF-S     PIC X(01).                  
                    07 COMM-TH18-TTH09-WINCLUS-S    PIC X(01).                  
                    07 COMM-TH18-TTH09-IDREGLE-S    PIC X(10).                  
                    07 COMM-TH18-TTH09-LIBRG-S      PIC X(20).                  
                    07 COMM-TH18-TTH09-WSVAL-S      PIC X(01).                  
                    07 COMM-TH18-TTH09-WSPOURC-S    PIC X(01).                  
                    07 COMM-TH18-TTH09-MTTREF-S     PIC X(05).                  
                    07 COMM-TH18-TTH09-WBVAL-S      PIC X(01).                  
                    07 COMM-TH18-TTH09-WBPOURC-S    PIC X(01).                  
                    07 COMM-TH18-TTH09-WDELTA-S     PIC X(01).                  
           02 COMM-TH21-APPLI REDEFINES COMM-TH10-APPLI.                        
              03 COMM-TH21-TRIFAM        PIC X(01).                             
              03 COMM-TH21-IDRG          PIC X(10).                             
              03 COMM-TH21-ITEM          PIC 9(04).                             
              03 COMM-TH21-PAGE          PIC 9(04).                             
              03 COMM-TH21-PAGEMAX       PIC 9(04).                             
              03 COMM-TH21-NLIGNE        PIC 9(02).                             
              03 COMM-TH21-WDATEEF       PIC X(01).                             
              03 COMM-TH21-WSEL          PIC X(01).                             
              03 COMM-TH21-SCFAM         PIC X(05).                             
              03 COMM-TH21-SCMARQ        PIC X(05).                             
              03 COMM-TH21-SNCODIC       PIC X(07).                             
              03 COMM-TH21-TITRE1        PIC X(10).                             
              03 COMM-TH21-TITRE2        PIC X(13).                             
              03 COMM-TH21-CONFIRM-CHG   PIC X(01).                             
              03 COMM-TH21-CONFIRM-PF3   PIC X(01).                             
      *                                                                         
                                                                                
