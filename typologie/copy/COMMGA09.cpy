      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
           02  COMM-GA09-APPLI  REDEFINES  COMM-GA00-APPLI.                     
               05 COMM-GA09-PROG-APPELANT.                                      
                  10  COMM-GA09-T                PIC   X(1).                    
                  10  COMM-GA09-LEVEL-MAX        PIC   X(4).                    
               05 COMM-GA09-FONCTION             PIC   X(3).                    
               05 COMM-GA09-CRAYON               PIC   X(5).                    
               05 COMM-GA09-NSEQ  OCCURS  40     PIC   S9(5)  COMP-3.           
               05 COMM-GA09-LRAYON               PIC   X(20).                   
               05 COMM-GA09-PAGE                 PIC   9(3).                    
               05 COMM-GA09-TYPE                 PIC   X(3).                    
               05 COMM-GA09-TS.                                                 
                  10  COMM-GA09-OCC    OCCURS   10.                             
                       15  COMM-GA09-CODE        PIC  X.                        
                       15  COMM-GA09-WRAYONFAM   PIC  X.                        
                       15  COMM-GA09-CRAYONFAM   PIC  X(5).                     
                       15  COMM-GA09-LRAYONOC    PIC  X(20).                    
                       15  COMM-GA09-NSEQOC      PIC  S9(5)  COMP-3.            
               05 COMM-GA09-FILLER               PIC  X(3265).                  
                                                                                
