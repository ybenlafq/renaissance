      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: GESTION DES PAC                                            00000020
      ***************************************************************** 00000030
       01   EGN70I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(5).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(5).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000150
           02 FILLER    PIC X(5).                                       00000160
           02 MNCODICI  PIC X(7).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOUL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MLREFFOUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLREFFOUF      PIC X.                                     00000190
           02 FILLER    PIC X(5).                                       00000200
           02 MLREFFOUI      PIC X(20).                                 00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000230
           02 FILLER    PIC X(5).                                       00000240
           02 MCMARQI   PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000270
           02 FILLER    PIC X(5).                                       00000280
           02 MCFAMI    PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWOAL     COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MWOAL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MWOAF     PIC X.                                          00000310
           02 FILLER    PIC X(5).                                       00000320
           02 MWOAI     PIC X.                                          00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSUBSL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MSUBSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSUBSF    PIC X.                                          00000350
           02 FILLER    PIC X(5).                                       00000360
           02 MSUBSI    PIC X(6).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICSL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MNCODICSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODICSF      PIC X.                                     00000390
           02 FILLER    PIC X(5).                                       00000400
           02 MNCODICSI      PIC X(7).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 DEFFETOAL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 DEFFETOAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 DEFFETOAF      PIC X.                                     00000430
           02 FILLER    PIC X(5).                                       00000440
           02 DEFFETOAI      PIC X(8).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 DFINEFOAL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 DFINEFOAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 DFINEFOAF      PIC X.                                     00000470
           02 FILLER    PIC X(5).                                       00000480
           02 DFINEFOAI      PIC X(8).                                  00000490
           02 MPREFTTCD OCCURS   2 TIMES .                              00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPREFTTCL    COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MPREFTTCL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MPREFTTCF    PIC X.                                     00000520
             03 FILLER  PIC X(5).                                       00000530
             03 MPREFTTCI    PIC X(8).                                  00000540
           02 MWACTIFD OCCURS   2 TIMES .                               00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWACTIFL     COMP PIC S9(4).                            00000560
      *--                                                                       
             03 MWACTIFL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWACTIFF     PIC X.                                     00000570
             03 FILLER  PIC X(5).                                       00000580
             03 MWACTIFI     PIC X.                                     00000590
           02 DEFFETD OCCURS   2 TIMES .                                00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 DEFFETL      COMP PIC S9(4).                            00000610
      *--                                                                       
             03 DEFFETL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 DEFFETF      PIC X.                                     00000620
             03 FILLER  PIC X(5).                                       00000630
             03 DEFFETI      PIC X(8).                                  00000640
           02 DFINEFFED OCCURS   2 TIMES .                              00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 DFINEFFEL    COMP PIC S9(4).                            00000660
      *--                                                                       
             03 DFINEFFEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 DFINEFFEF    PIC X.                                     00000670
             03 FILLER  PIC X(5).                                       00000680
             03 DFINEFFEI    PIC X(8).                                  00000690
           02 MOPARENTD OCCURS   2 TIMES .                              00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MOPARENTL    COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MOPARENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MOPARENTF    PIC X.                                     00000720
             03 FILLER  PIC X(5).                                       00000730
             03 MOPARENTI    PIC X.                                     00000740
           02 LCOMMENTD OCCURS   2 TIMES .                              00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 LCOMMENTL    COMP PIC S9(4).                            00000760
      *--                                                                       
             03 LCOMMENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 LCOMMENTF    PIC X.                                     00000770
             03 FILLER  PIC X(5).                                       00000780
             03 LCOMMENTI    PIC X(10).                                 00000790
           02 MFPARENTD OCCURS   2 TIMES .                              00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFPARENTL    COMP PIC S9(4).                            00000810
      *--                                                                       
             03 MFPARENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MFPARENTF    PIC X.                                     00000820
             03 FILLER  PIC X(5).                                       00000830
             03 MFPARENTI    PIC X.                                     00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIPEXPTTCL     COMP PIC S9(4).                            00000850
      *--                                                                       
           02 MIPEXPTTCL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MIPEXPTTCF     PIC X.                                     00000860
           02 FILLER    PIC X(5).                                       00000870
           02 MIPEXPTTCI     PIC X(8).                                  00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIDDEFFETL     COMP PIC S9(4).                            00000890
      *--                                                                       
           02 MIDDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MIDDEFFETF     PIC X.                                     00000900
           02 FILLER    PIC X(5).                                       00000910
           02 MIDDEFFETI     PIC X(8).                                  00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIDFEFFETL     COMP PIC S9(4).                            00000930
      *--                                                                       
           02 MIDFEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MIDFEFFETF     PIC X.                                     00000940
           02 FILLER    PIC X(5).                                       00000950
           02 MIDFEFFETI     PIC X(8).                                  00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MICONCNL  COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MICONCNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MICONCNF  PIC X.                                          00000980
           02 FILLER    PIC X(5).                                       00000990
           02 MICONCNI  PIC X(4).                                       00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00001010
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00001020
           02 FILLER    PIC X(5).                                       00001030
           02 MNSOCI    PIC X(3).                                       00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPROL  COMP PIC S9(4).                                 00001050
      *--                                                                       
           02 MCAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAPPROF  PIC X.                                          00001060
           02 FILLER    PIC X(5).                                       00001070
           02 MCAPPROI  PIC X(3).                                       00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATCOMPL     COMP PIC S9(4).                            00001090
      *--                                                                       
           02 MSTATCOMPL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSTATCOMPF     PIC X.                                     00001100
           02 FILLER    PIC X(5).                                       00001110
           02 MSTATCOMPI     PIC X(3).                                  00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPOL   COMP PIC S9(4).                                 00001130
      *--                                                                       
           02 MCEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXPOF   PIC X.                                          00001140
           02 FILLER    PIC X(5).                                       00001150
           02 MCEXPOI   PIC X(2).                                       00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIB1L    COMP PIC S9(4).                                 00001170
      *--                                                                       
           02 MLIB1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIB1F    PIC X.                                          00001180
           02 FILLER    PIC X(5).                                       00001190
           02 MLIB1I    PIC X(15).                                      00001200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIB2L    COMP PIC S9(4).                                 00001210
      *--                                                                       
           02 MLIB2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIB2F    PIC X.                                          00001220
           02 FILLER    PIC X(5).                                       00001230
           02 MLIB2I    PIC X(15).                                      00001240
           02 MACTD OCCURS   10 TIMES .                                 00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTL   COMP PIC S9(4).                                 00001260
      *--                                                                       
             03 MACTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MACTF   PIC X.                                          00001270
             03 FILLER  PIC X(5).                                       00001280
             03 MACTI   PIC X.                                          00001290
           02 MPEXPTTCD OCCURS   10 TIMES .                             00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPEXPTTCL    COMP PIC S9(4).                            00001310
      *--                                                                       
             03 MPEXPTTCL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MPEXPTTCF    PIC X.                                     00001320
             03 FILLER  PIC X(5).                                       00001330
             03 MPEXPTTCI    PIC X(8).                                  00001340
           02 MDDEFFETD OCCURS   10 TIMES .                             00001350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDDEFFETL    COMP PIC S9(4).                            00001360
      *--                                                                       
             03 MDDEFFETL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDDEFFETF    PIC X.                                     00001370
             03 FILLER  PIC X(5).                                       00001380
             03 MDDEFFETI    PIC X(8).                                  00001390
           02 MDFEFFETD OCCURS   10 TIMES .                             00001400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDFEFFETL    COMP PIC S9(4).                            00001410
      *--                                                                       
             03 MDFEFFETL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDFEFFETF    PIC X.                                     00001420
             03 FILLER  PIC X(5).                                       00001430
             03 MDFEFFETI    PIC X(8).                                  00001440
           02 MNCONCND OCCURS   10 TIMES .                              00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCONCNL     COMP PIC S9(4).                            00001460
      *--                                                                       
             03 MNCONCNL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCONCNF     PIC X.                                     00001470
             03 FILLER  PIC X(5).                                       00001480
             03 MNCONCNI     PIC X(4).                                  00001490
           02 MLIGNED OCCURS   10 TIMES .                               00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNEL      COMP PIC S9(4).                            00001510
      *--                                                                       
             03 MLIGNEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLIGNEF      PIC X.                                     00001520
             03 FILLER  PIC X(5).                                       00001530
             03 MLIGNEI      PIC X(39).                                 00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00001560
           02 FILLER    PIC X(5).                                       00001570
           02 MPAGEI    PIC X(3).                                       00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00001590
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00001600
           02 FILLER    PIC X(5).                                       00001610
           02 MPAGEMAXI      PIC X(3).                                  00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGE2L   COMP PIC S9(4).                                 00001630
      *--                                                                       
           02 MPAGE2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGE2F   PIC X.                                          00001640
           02 FILLER    PIC X(5).                                       00001650
           02 MPAGE2I   PIC X(3).                                       00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAX2L     COMP PIC S9(4).                            00001670
      *--                                                                       
           02 MPAGEMAX2L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MPAGEMAX2F     PIC X.                                     00001680
           02 FILLER    PIC X(5).                                       00001690
           02 MPAGEMAX2I     PIC X(3).                                  00001700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001710
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001720
           02 FILLER    PIC X(5).                                       00001730
           02 MLIBERRI  PIC X(78).                                      00001740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001750
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001760
           02 FILLER    PIC X(5).                                       00001770
           02 MCODTRAI  PIC X(4).                                       00001780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001790
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001800
           02 FILLER    PIC X(5).                                       00001810
           02 MCICSI    PIC X(5).                                       00001820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001830
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001840
           02 FILLER    PIC X(5).                                       00001850
           02 MNETNAMI  PIC X(8).                                       00001860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001870
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001880
           02 FILLER    PIC X(5).                                       00001890
           02 MSCREENI  PIC X(4).                                       00001900
      ***************************************************************** 00001910
      * SDF: GESTION DES PAC                                            00001920
      ***************************************************************** 00001930
       01   EGN70O REDEFINES EGN70I.                                    00001940
           02 FILLER    PIC X(12).                                      00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MDATJOUA  PIC X.                                          00001970
           02 MDATJOUC  PIC X.                                          00001980
           02 MDATJOUP  PIC X.                                          00001990
           02 MDATJOUH  PIC X.                                          00002000
           02 MDATJOUV  PIC X.                                          00002010
           02 MDATJOUU  PIC X.                                          00002020
           02 MDATJOUO  PIC X(10).                                      00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MTIMJOUA  PIC X.                                          00002050
           02 MTIMJOUC  PIC X.                                          00002060
           02 MTIMJOUP  PIC X.                                          00002070
           02 MTIMJOUH  PIC X.                                          00002080
           02 MTIMJOUV  PIC X.                                          00002090
           02 MTIMJOUU  PIC X.                                          00002100
           02 MTIMJOUO  PIC X(5).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MNCODICA  PIC X.                                          00002130
           02 MNCODICC  PIC X.                                          00002140
           02 MNCODICP  PIC X.                                          00002150
           02 MNCODICH  PIC X.                                          00002160
           02 MNCODICV  PIC X.                                          00002170
           02 MNCODICU  PIC X.                                          00002180
           02 MNCODICO  PIC X(7).                                       00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLREFFOUA      PIC X.                                     00002210
           02 MLREFFOUC PIC X.                                          00002220
           02 MLREFFOUP PIC X.                                          00002230
           02 MLREFFOUH PIC X.                                          00002240
           02 MLREFFOUV PIC X.                                          00002250
           02 MLREFFOUU PIC X.                                          00002260
           02 MLREFFOUO      PIC X(20).                                 00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MCMARQA   PIC X.                                          00002290
           02 MCMARQC   PIC X.                                          00002300
           02 MCMARQP   PIC X.                                          00002310
           02 MCMARQH   PIC X.                                          00002320
           02 MCMARQV   PIC X.                                          00002330
           02 MCMARQU   PIC X.                                          00002340
           02 MCMARQO   PIC X(5).                                       00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MCFAMA    PIC X.                                          00002370
           02 MCFAMC    PIC X.                                          00002380
           02 MCFAMP    PIC X.                                          00002390
           02 MCFAMH    PIC X.                                          00002400
           02 MCFAMV    PIC X.                                          00002410
           02 MCFAMU    PIC X.                                          00002420
           02 MCFAMO    PIC X(5).                                       00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MWOAA     PIC X.                                          00002450
           02 MWOAC     PIC X.                                          00002460
           02 MWOAP     PIC X.                                          00002470
           02 MWOAH     PIC X.                                          00002480
           02 MWOAV     PIC X.                                          00002490
           02 MWOAU     PIC X.                                          00002500
           02 MWOAO     PIC X.                                          00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MSUBSA    PIC X.                                          00002530
           02 MSUBSC    PIC X.                                          00002540
           02 MSUBSP    PIC X.                                          00002550
           02 MSUBSH    PIC X.                                          00002560
           02 MSUBSV    PIC X.                                          00002570
           02 MSUBSU    PIC X.                                          00002580
           02 MSUBSO    PIC X(6).                                       00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MNCODICSA      PIC X.                                     00002610
           02 MNCODICSC PIC X.                                          00002620
           02 MNCODICSP PIC X.                                          00002630
           02 MNCODICSH PIC X.                                          00002640
           02 MNCODICSV PIC X.                                          00002650
           02 MNCODICSU PIC X.                                          00002660
           02 MNCODICSO      PIC X(7).                                  00002670
           02 FILLER    PIC X(2).                                       00002680
           02 DEFFETOAA      PIC X.                                     00002690
           02 DEFFETOAC PIC X.                                          00002700
           02 DEFFETOAP PIC X.                                          00002710
           02 DEFFETOAH PIC X.                                          00002720
           02 DEFFETOAV PIC X.                                          00002730
           02 DEFFETOAU PIC X.                                          00002740
           02 DEFFETOAO      PIC X(8).                                  00002750
           02 FILLER    PIC X(2).                                       00002760
           02 DFINEFOAA      PIC X.                                     00002770
           02 DFINEFOAC PIC X.                                          00002780
           02 DFINEFOAP PIC X.                                          00002790
           02 DFINEFOAH PIC X.                                          00002800
           02 DFINEFOAV PIC X.                                          00002810
           02 DFINEFOAU PIC X.                                          00002820
           02 DFINEFOAO      PIC X(8).                                  00002830
           02 DFHMS1 OCCURS   2 TIMES .                                 00002840
             03 FILLER       PIC X(2).                                  00002850
             03 MPREFTTCA    PIC X.                                     00002860
             03 MPREFTTCC    PIC X.                                     00002870
             03 MPREFTTCP    PIC X.                                     00002880
             03 MPREFTTCH    PIC X.                                     00002890
             03 MPREFTTCV    PIC X.                                     00002900
             03 MPREFTTCU    PIC X.                                     00002910
             03 MPREFTTCO    PIC X(8).                                  00002920
           02 DFHMS2 OCCURS   2 TIMES .                                 00002930
             03 FILLER       PIC X(2).                                  00002940
             03 MWACTIFA     PIC X.                                     00002950
             03 MWACTIFC     PIC X.                                     00002960
             03 MWACTIFP     PIC X.                                     00002970
             03 MWACTIFH     PIC X.                                     00002980
             03 MWACTIFV     PIC X.                                     00002990
             03 MWACTIFU     PIC X.                                     00003000
             03 MWACTIFO     PIC X.                                     00003010
           02 DFHMS3 OCCURS   2 TIMES .                                 00003020
             03 FILLER       PIC X(2).                                  00003030
             03 DEFFETA      PIC X.                                     00003040
             03 DEFFETC PIC X.                                          00003050
             03 DEFFETP PIC X.                                          00003060
             03 DEFFETH PIC X.                                          00003070
             03 DEFFETV PIC X.                                          00003080
             03 DEFFETU PIC X.                                          00003090
             03 DEFFETO      PIC X(8).                                  00003100
           02 DFHMS4 OCCURS   2 TIMES .                                 00003110
             03 FILLER       PIC X(2).                                  00003120
             03 DFINEFFEA    PIC X.                                     00003130
             03 DFINEFFEC    PIC X.                                     00003140
             03 DFINEFFEP    PIC X.                                     00003150
             03 DFINEFFEH    PIC X.                                     00003160
             03 DFINEFFEV    PIC X.                                     00003170
             03 DFINEFFEU    PIC X.                                     00003180
             03 DFINEFFEO    PIC X(8).                                  00003190
           02 DFHMS5 OCCURS   2 TIMES .                                 00003200
             03 FILLER       PIC X(2).                                  00003210
             03 MOPARENTA    PIC X.                                     00003220
             03 MOPARENTC    PIC X.                                     00003230
             03 MOPARENTP    PIC X.                                     00003240
             03 MOPARENTH    PIC X.                                     00003250
             03 MOPARENTV    PIC X.                                     00003260
             03 MOPARENTU    PIC X.                                     00003270
             03 MOPARENTO    PIC X.                                     00003280
           02 DFHMS6 OCCURS   2 TIMES .                                 00003290
             03 FILLER       PIC X(2).                                  00003300
             03 LCOMMENTA    PIC X.                                     00003310
             03 LCOMMENTC    PIC X.                                     00003320
             03 LCOMMENTP    PIC X.                                     00003330
             03 LCOMMENTH    PIC X.                                     00003340
             03 LCOMMENTV    PIC X.                                     00003350
             03 LCOMMENTU    PIC X.                                     00003360
             03 LCOMMENTO    PIC X(10).                                 00003370
           02 DFHMS7 OCCURS   2 TIMES .                                 00003380
             03 FILLER       PIC X(2).                                  00003390
             03 MFPARENTA    PIC X.                                     00003400
             03 MFPARENTC    PIC X.                                     00003410
             03 MFPARENTP    PIC X.                                     00003420
             03 MFPARENTH    PIC X.                                     00003430
             03 MFPARENTV    PIC X.                                     00003440
             03 MFPARENTU    PIC X.                                     00003450
             03 MFPARENTO    PIC X.                                     00003460
           02 FILLER    PIC X(2).                                       00003470
           02 MIPEXPTTCA     PIC X.                                     00003480
           02 MIPEXPTTCC     PIC X.                                     00003490
           02 MIPEXPTTCP     PIC X.                                     00003500
           02 MIPEXPTTCH     PIC X.                                     00003510
           02 MIPEXPTTCV     PIC X.                                     00003520
           02 MIPEXPTTCU     PIC X.                                     00003530
           02 MIPEXPTTCO     PIC X(8).                                  00003540
           02 FILLER    PIC X(2).                                       00003550
           02 MIDDEFFETA     PIC X.                                     00003560
           02 MIDDEFFETC     PIC X.                                     00003570
           02 MIDDEFFETP     PIC X.                                     00003580
           02 MIDDEFFETH     PIC X.                                     00003590
           02 MIDDEFFETV     PIC X.                                     00003600
           02 MIDDEFFETU     PIC X.                                     00003610
           02 MIDDEFFETO     PIC X(8).                                  00003620
           02 FILLER    PIC X(2).                                       00003630
           02 MIDFEFFETA     PIC X.                                     00003640
           02 MIDFEFFETC     PIC X.                                     00003650
           02 MIDFEFFETP     PIC X.                                     00003660
           02 MIDFEFFETH     PIC X.                                     00003670
           02 MIDFEFFETV     PIC X.                                     00003680
           02 MIDFEFFETU     PIC X.                                     00003690
           02 MIDFEFFETO     PIC X(8).                                  00003700
           02 FILLER    PIC X(2).                                       00003710
           02 MICONCNA  PIC X.                                          00003720
           02 MICONCNC  PIC X.                                          00003730
           02 MICONCNP  PIC X.                                          00003740
           02 MICONCNH  PIC X.                                          00003750
           02 MICONCNV  PIC X.                                          00003760
           02 MICONCNU  PIC X.                                          00003770
           02 MICONCNO  PIC X(4).                                       00003780
           02 FILLER    PIC X(2).                                       00003790
           02 MNSOCA    PIC X.                                          00003800
           02 MNSOCC    PIC X.                                          00003810
           02 MNSOCP    PIC X.                                          00003820
           02 MNSOCH    PIC X.                                          00003830
           02 MNSOCV    PIC X.                                          00003840
           02 MNSOCU    PIC X.                                          00003850
           02 MNSOCO    PIC X(3).                                       00003860
           02 FILLER    PIC X(2).                                       00003870
           02 MCAPPROA  PIC X.                                          00003880
           02 MCAPPROC  PIC X.                                          00003890
           02 MCAPPROP  PIC X.                                          00003900
           02 MCAPPROH  PIC X.                                          00003910
           02 MCAPPROV  PIC X.                                          00003920
           02 MCAPPROU  PIC X.                                          00003930
           02 MCAPPROO  PIC X(3).                                       00003940
           02 FILLER    PIC X(2).                                       00003950
           02 MSTATCOMPA     PIC X.                                     00003960
           02 MSTATCOMPC     PIC X.                                     00003970
           02 MSTATCOMPP     PIC X.                                     00003980
           02 MSTATCOMPH     PIC X.                                     00003990
           02 MSTATCOMPV     PIC X.                                     00004000
           02 MSTATCOMPU     PIC X.                                     00004010
           02 MSTATCOMPO     PIC X(3).                                  00004020
           02 FILLER    PIC X(2).                                       00004030
           02 MCEXPOA   PIC X.                                          00004040
           02 MCEXPOC   PIC X.                                          00004050
           02 MCEXPOP   PIC X.                                          00004060
           02 MCEXPOH   PIC X.                                          00004070
           02 MCEXPOV   PIC X.                                          00004080
           02 MCEXPOU   PIC X.                                          00004090
           02 MCEXPOO   PIC X(2).                                       00004100
           02 FILLER    PIC X(2).                                       00004110
           02 MLIB1A    PIC X.                                          00004120
           02 MLIB1C    PIC X.                                          00004130
           02 MLIB1P    PIC X.                                          00004140
           02 MLIB1H    PIC X.                                          00004150
           02 MLIB1V    PIC X.                                          00004160
           02 MLIB1U    PIC X.                                          00004170
           02 MLIB1O    PIC X(15).                                      00004180
           02 FILLER    PIC X(2).                                       00004190
           02 MLIB2A    PIC X.                                          00004200
           02 MLIB2C    PIC X.                                          00004210
           02 MLIB2P    PIC X.                                          00004220
           02 MLIB2H    PIC X.                                          00004230
           02 MLIB2V    PIC X.                                          00004240
           02 MLIB2U    PIC X.                                          00004250
           02 MLIB2O    PIC X(15).                                      00004260
           02 DFHMS8 OCCURS   10 TIMES .                                00004270
             03 FILLER       PIC X(2).                                  00004280
             03 MACTA   PIC X.                                          00004290
             03 MACTC   PIC X.                                          00004300
             03 MACTP   PIC X.                                          00004310
             03 MACTH   PIC X.                                          00004320
             03 MACTV   PIC X.                                          00004330
             03 MACTU   PIC X.                                          00004340
             03 MACTO   PIC X.                                          00004350
           02 DFHMS9 OCCURS   10 TIMES .                                00004360
             03 FILLER       PIC X(2).                                  00004370
             03 MPEXPTTCA    PIC X.                                     00004380
             03 MPEXPTTCC    PIC X.                                     00004390
             03 MPEXPTTCP    PIC X.                                     00004400
             03 MPEXPTTCH    PIC X.                                     00004410
             03 MPEXPTTCV    PIC X.                                     00004420
             03 MPEXPTTCU    PIC X.                                     00004430
             03 MPEXPTTCO    PIC X(8).                                  00004440
           02 DFHMS10 OCCURS   10 TIMES .                               00004450
             03 FILLER       PIC X(2).                                  00004460
             03 MDDEFFETA    PIC X.                                     00004470
             03 MDDEFFETC    PIC X.                                     00004480
             03 MDDEFFETP    PIC X.                                     00004490
             03 MDDEFFETH    PIC X.                                     00004500
             03 MDDEFFETV    PIC X.                                     00004510
             03 MDDEFFETU    PIC X.                                     00004520
             03 MDDEFFETO    PIC X(8).                                  00004530
           02 DFHMS11 OCCURS   10 TIMES .                               00004540
             03 FILLER       PIC X(2).                                  00004550
             03 MDFEFFETA    PIC X.                                     00004560
             03 MDFEFFETC    PIC X.                                     00004570
             03 MDFEFFETP    PIC X.                                     00004580
             03 MDFEFFETH    PIC X.                                     00004590
             03 MDFEFFETV    PIC X.                                     00004600
             03 MDFEFFETU    PIC X.                                     00004610
             03 MDFEFFETO    PIC X(8).                                  00004620
           02 DFHMS12 OCCURS   10 TIMES .                               00004630
             03 FILLER       PIC X(2).                                  00004640
             03 MNCONCNA     PIC X.                                     00004650
             03 MNCONCNC     PIC X.                                     00004660
             03 MNCONCNP     PIC X.                                     00004670
             03 MNCONCNH     PIC X.                                     00004680
             03 MNCONCNV     PIC X.                                     00004690
             03 MNCONCNU     PIC X.                                     00004700
             03 MNCONCNO     PIC X(4).                                  00004710
           02 DFHMS13 OCCURS   10 TIMES .                               00004720
             03 FILLER       PIC X(2).                                  00004730
             03 MLIGNEA      PIC X.                                     00004740
             03 MLIGNEC PIC X.                                          00004750
             03 MLIGNEP PIC X.                                          00004760
             03 MLIGNEH PIC X.                                          00004770
             03 MLIGNEV PIC X.                                          00004780
             03 MLIGNEU PIC X.                                          00004790
             03 MLIGNEO      PIC X(39).                                 00004800
           02 FILLER    PIC X(2).                                       00004810
           02 MPAGEA    PIC X.                                          00004820
           02 MPAGEC    PIC X.                                          00004830
           02 MPAGEP    PIC X.                                          00004840
           02 MPAGEH    PIC X.                                          00004850
           02 MPAGEV    PIC X.                                          00004860
           02 MPAGEU    PIC X.                                          00004870
           02 MPAGEO    PIC X(3).                                       00004880
           02 FILLER    PIC X(2).                                       00004890
           02 MPAGEMAXA      PIC X.                                     00004900
           02 MPAGEMAXC PIC X.                                          00004910
           02 MPAGEMAXP PIC X.                                          00004920
           02 MPAGEMAXH PIC X.                                          00004930
           02 MPAGEMAXV PIC X.                                          00004940
           02 MPAGEMAXU PIC X.                                          00004950
           02 MPAGEMAXO      PIC X(3).                                  00004960
           02 FILLER    PIC X(2).                                       00004970
           02 MPAGE2A   PIC X.                                          00004980
           02 MPAGE2C   PIC X.                                          00004990
           02 MPAGE2P   PIC X.                                          00005000
           02 MPAGE2H   PIC X.                                          00005010
           02 MPAGE2V   PIC X.                                          00005020
           02 MPAGE2U   PIC X.                                          00005030
           02 MPAGE2O   PIC X(3).                                       00005040
           02 FILLER    PIC X(2).                                       00005050
           02 MPAGEMAX2A     PIC X.                                     00005060
           02 MPAGEMAX2C     PIC X.                                     00005070
           02 MPAGEMAX2P     PIC X.                                     00005080
           02 MPAGEMAX2H     PIC X.                                     00005090
           02 MPAGEMAX2V     PIC X.                                     00005100
           02 MPAGEMAX2U     PIC X.                                     00005110
           02 MPAGEMAX2O     PIC X(3).                                  00005120
           02 FILLER    PIC X(2).                                       00005130
           02 MLIBERRA  PIC X.                                          00005140
           02 MLIBERRC  PIC X.                                          00005150
           02 MLIBERRP  PIC X.                                          00005160
           02 MLIBERRH  PIC X.                                          00005170
           02 MLIBERRV  PIC X.                                          00005180
           02 MLIBERRU  PIC X.                                          00005190
           02 MLIBERRO  PIC X(78).                                      00005200
           02 FILLER    PIC X(2).                                       00005210
           02 MCODTRAA  PIC X.                                          00005220
           02 MCODTRAC  PIC X.                                          00005230
           02 MCODTRAP  PIC X.                                          00005240
           02 MCODTRAH  PIC X.                                          00005250
           02 MCODTRAV  PIC X.                                          00005260
           02 MCODTRAU  PIC X.                                          00005270
           02 MCODTRAO  PIC X(4).                                       00005280
           02 FILLER    PIC X(2).                                       00005290
           02 MCICSA    PIC X.                                          00005300
           02 MCICSC    PIC X.                                          00005310
           02 MCICSP    PIC X.                                          00005320
           02 MCICSH    PIC X.                                          00005330
           02 MCICSV    PIC X.                                          00005340
           02 MCICSU    PIC X.                                          00005350
           02 MCICSO    PIC X(5).                                       00005360
           02 FILLER    PIC X(2).                                       00005370
           02 MNETNAMA  PIC X.                                          00005380
           02 MNETNAMC  PIC X.                                          00005390
           02 MNETNAMP  PIC X.                                          00005400
           02 MNETNAMH  PIC X.                                          00005410
           02 MNETNAMV  PIC X.                                          00005420
           02 MNETNAMU  PIC X.                                          00005430
           02 MNETNAMO  PIC X(8).                                       00005440
           02 FILLER    PIC X(2).                                       00005450
           02 MSCREENA  PIC X.                                          00005460
           02 MSCREENC  PIC X.                                          00005470
           02 MSCREENP  PIC X.                                          00005480
           02 MSCREENH  PIC X.                                          00005490
           02 MSCREENV  PIC X.                                          00005500
           02 MSCREENU  PIC X.                                          00005510
           02 MSCREENO  PIC X(4).                                       00005520
                                                                                
