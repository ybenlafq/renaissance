      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CRVAL VALEURS OU PLAGES DE VALEURS     *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCRVAL.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCRVAL.                                                             
      *}                                                                        
           05  CRVAL-CTABLEG2    PIC X(15).                                     
           05  CRVAL-CTABLEG2-REDEF REDEFINES CRVAL-CTABLEG2.                   
               10  CRVAL-CCTRL           PIC X(05).                             
               10  CRVAL-NSEQ            PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  CRVAL-NSEQ-N         REDEFINES CRVAL-NSEQ                    
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  CRVAL-WTABLEG     PIC X(80).                                     
           05  CRVAL-WTABLEG-REDEF  REDEFINES CRVAL-WTABLEG.                    
               10  CRVAL-TYVAL           PIC X(02).                             
               10  CRVAL-VAL1            PIC X(25).                             
               10  CRVAL-VAL2            PIC X(25).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCRVAL-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCRVAL-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CRVAL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CRVAL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CRVAL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CRVAL-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
