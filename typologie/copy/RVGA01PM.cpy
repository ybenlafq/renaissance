      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HEFLU AUTORISATIONS POUR THE70         *        
      *----------------------------------------------------------------*        
       01  RVGA01PM.                                                            
           05  HEFLU-CTABLEG2    PIC X(15).                                     
           05  HEFLU-CTABLEG2-REDEF REDEFINES HEFLU-CTABLEG2.                   
               10  HEFLU-CACID           PIC X(08).                             
           05  HEFLU-WTABLEG     PIC X(80).                                     
           05  HEFLU-WTABLEG-REDEF  REDEFINES HEFLU-WTABLEG.                    
               10  HEFLU-WACTIF          PIC X(01).                             
               10  HEFLU-LACID           PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01PM-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HEFLU-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HEFLU-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HEFLU-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HEFLU-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
