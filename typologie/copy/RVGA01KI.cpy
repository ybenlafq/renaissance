      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HELIT CENTRES DE TRAITEMENT GHE        *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVGA01KI.                                                            
           05  HELIT-CTABLEG2    PIC X(15).                                     
           05  HELIT-CTABLEG2-REDEF REDEFINES HELIT-CTABLEG2.                   
               10  HELIT-CLIEUHET        PIC X(05).                             
           05  HELIT-WTABLEG     PIC X(80).                                     
           05  HELIT-WTABLEG-REDEF  REDEFINES HELIT-WTABLEG.                    
               10  HELIT-WACTIF          PIC X(01).                             
               10  HELIT-LLIEUHET        PIC X(20).                             
               10  HELIT-WSAISIES        PIC X(05).                             
               10  HELIT-CLIEUNAT        PIC X(08).                             
               10  HELIT-PARMNASC        PIC X(09).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01KI-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HELIT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HELIT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HELIT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HELIT-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
