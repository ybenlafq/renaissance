      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPR600 AU 26/01/2000  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,07,BI,A,                          *        
      *                           20,07,BI,A,                          *        
      *                           27,07,BI,A,                          *        
      *                           34,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPR600.                                                        
            05 NOMETAT-IPR600           PIC X(6) VALUE 'IPR600'.                
            05 RUPTURES-IPR600.                                                 
           10 IPR600-NSOCIETE           PIC X(03).                      007  003
           10 IPR600-NLIEU              PIC X(03).                      010  003
           10 IPR600-CPRESTATION        PIC X(07).                      013  007
           10 IPR600-NVENTE             PIC X(07).                      020  007
           10 IPR600-NCODIC             PIC X(07).                      027  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPR600-SEQUENCE           PIC S9(04) COMP.                034  002
      *--                                                                       
           10 IPR600-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPR600.                                                   
           10 IPR600-CPOSTAL            PIC X(05).                      036  005
           10 IPR600-CTITRENOM          PIC X(05).                      041  005
           10 IPR600-CTVOIE             PIC X(04).                      046  004
           10 IPR600-CVOIE              PIC X(05).                      050  005
           10 IPR600-LCOMMUNE           PIC X(30).                      055  030
           10 IPR600-LLIEU              PIC X(20).                      085  020
           10 IPR600-LNOM               PIC X(25).                      105  025
           10 IPR600-LNOMVOIE           PIC X(20).                      130  020
           10 IPR600-LPRENOM            PIC X(15).                      150  015
           10 IPR600-LPRESTATION        PIC X(20).                      165  020
           10 IPR600-LREFFOURN          PIC X(20).                      185  020
           10 IPR600-NTELDOM            PIC X(12).                      205  012
           10 IPR600-QVENDUE            PIC X(03).                      217  003
           10 IPR600-PXREMTTC           PIC S9(05)V9(2) COMP-3.         220  004
           10 IPR600-PXTTCBRUT          PIC S9(05)V9(2) COMP-3.         224  004
           10 IPR600-DCOMPTA            PIC X(08).                      228  008
           10 IPR600-DCREATION          PIC X(08).                      236  008
            05 FILLER                      PIC X(269).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPR600-LONG           PIC S9(4)   COMP  VALUE +243.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPR600-LONG           PIC S9(4) COMP-5  VALUE +243.           
                                                                                
      *}                                                                        
