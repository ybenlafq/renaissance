      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PCFFI REGLE PCF FILIALES               *        
      *----------------------------------------------------------------*        
       01  RVGA01OL.                                                            
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  PCFFI-CTABLEG2    PIC X(15).                                     
           05  PCFFI-CTABLEG2-REDEF REDEFINES PCFFI-CTABLEG2.                   
               10  PCFFI-NSOC            PIC X(03).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  PCFFI-NSOC-N         REDEFINES PCFFI-NSOC                    
                                         PIC 9(03).                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  PCFFI-WTABLEG     PIC X(80).                                     
           05  PCFFI-WTABLEG-REDEF  REDEFINES PCFFI-WTABLEG.                    
               10  PCFFI-WFILIALE        PIC X(01).                             
               10  PCFFI-COMMENT         PIC X(20).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01OL-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PCFFI-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PCFFI-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PCFFI-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PCFFI-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
