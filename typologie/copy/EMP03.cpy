      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: Erx21                                                      00000020
      ***************************************************************** 00000030
       01   EMP03I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(5).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCI    PIC X(3).                                       00000250
           02 MNDETMODI OCCURS   13 TIMES .                             00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMOPAIL     COMP PIC S9(4).                            00000270
      *--                                                                       
             03 MCMOPAIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCMOPAIF     PIC X.                                     00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MCMOPAII     PIC X(5).                                  00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWTPRGTL     COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MWTPRGTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWTPRGTF     PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MWTPRGTI     PIC X.                                     00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMDPAIL     COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MLMDPAIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLMDPAIF     PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MLMDPAII     PIC X(15).                                 00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMDPRBL     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MLMDPRBL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLMDPRBF     PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MLMDPRBI     PIC X(15).                                 00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWINFCPL     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MWINFCPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWINFCPF     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MWINFCPI     PIC X.                                     00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWDCMPTL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MWDCMPTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWDCMPTF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MWDCMPTI     PIC X.                                     00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWCTFDCL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MWCTFDCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWCTFDCF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MWCTFDCI     PIC X.                                     00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWRCOFFL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MWRCOFFL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWRCOFFF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MWRCOFFI     PIC X.                                     00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMOPACL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MCMOPACL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCMOPACF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCMOPACI     PIC X(5).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MLIBERRI  PIC X(78).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCODTRAI  PIC X(4).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCICSI    PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MNETNAMI  PIC X(8).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MSCREENI  PIC X(4).                                       00000820
      ***************************************************************** 00000830
      * SDF: Erx21                                                      00000840
      ***************************************************************** 00000850
       01   EMP03O REDEFINES EMP03I.                                    00000860
           02 FILLER    PIC X(12).                                      00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MDATJOUA  PIC X.                                          00000890
           02 MDATJOUC  PIC X.                                          00000900
           02 MDATJOUP  PIC X.                                          00000910
           02 MDATJOUH  PIC X.                                          00000920
           02 MDATJOUV  PIC X.                                          00000930
           02 MDATJOUO  PIC X(10).                                      00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MTIMJOUA  PIC X.                                          00000960
           02 MTIMJOUC  PIC X.                                          00000970
           02 MTIMJOUP  PIC X.                                          00000980
           02 MTIMJOUH  PIC X.                                          00000990
           02 MTIMJOUV  PIC X.                                          00001000
           02 MTIMJOUO  PIC X(5).                                       00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MPAGEA    PIC X.                                          00001030
           02 MPAGEC    PIC X.                                          00001040
           02 MPAGEP    PIC X.                                          00001050
           02 MPAGEH    PIC X.                                          00001060
           02 MPAGEV    PIC X.                                          00001070
           02 MPAGEO    PIC X(5).                                       00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MWFONCA   PIC X.                                          00001100
           02 MWFONCC   PIC X.                                          00001110
           02 MWFONCP   PIC X.                                          00001120
           02 MWFONCH   PIC X.                                          00001130
           02 MWFONCV   PIC X.                                          00001140
           02 MWFONCO   PIC X(3).                                       00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MNSOCA    PIC X.                                          00001170
           02 MNSOCC    PIC X.                                          00001180
           02 MNSOCP    PIC X.                                          00001190
           02 MNSOCH    PIC X.                                          00001200
           02 MNSOCV    PIC X.                                          00001210
           02 MNSOCO    PIC X(3).                                       00001220
           02 MNDETMODO OCCURS   13 TIMES .                             00001230
             03 FILLER       PIC X(2).                                  00001240
             03 MCMOPAIA     PIC X.                                     00001250
             03 MCMOPAIC     PIC X.                                     00001260
             03 MCMOPAIP     PIC X.                                     00001270
             03 MCMOPAIH     PIC X.                                     00001280
             03 MCMOPAIV     PIC X.                                     00001290
             03 MCMOPAIO     PIC X(5).                                  00001300
             03 FILLER       PIC X(2).                                  00001310
             03 MWTPRGTA     PIC X.                                     00001320
             03 MWTPRGTC     PIC X.                                     00001330
             03 MWTPRGTP     PIC X.                                     00001340
             03 MWTPRGTH     PIC X.                                     00001350
             03 MWTPRGTV     PIC X.                                     00001360
             03 MWTPRGTO     PIC X.                                     00001370
             03 FILLER       PIC X(2).                                  00001380
             03 MLMDPAIA     PIC X.                                     00001390
             03 MLMDPAIC     PIC X.                                     00001400
             03 MLMDPAIP     PIC X.                                     00001410
             03 MLMDPAIH     PIC X.                                     00001420
             03 MLMDPAIV     PIC X.                                     00001430
             03 MLMDPAIO     PIC X(15).                                 00001440
             03 FILLER       PIC X(2).                                  00001450
             03 MLMDPRBA     PIC X.                                     00001460
             03 MLMDPRBC     PIC X.                                     00001470
             03 MLMDPRBP     PIC X.                                     00001480
             03 MLMDPRBH     PIC X.                                     00001490
             03 MLMDPRBV     PIC X.                                     00001500
             03 MLMDPRBO     PIC X(15).                                 00001510
             03 FILLER       PIC X(2).                                  00001520
             03 MWINFCPA     PIC X.                                     00001530
             03 MWINFCPC     PIC X.                                     00001540
             03 MWINFCPP     PIC X.                                     00001550
             03 MWINFCPH     PIC X.                                     00001560
             03 MWINFCPV     PIC X.                                     00001570
             03 MWINFCPO     PIC X.                                     00001580
             03 FILLER       PIC X(2).                                  00001590
             03 MWDCMPTA     PIC X.                                     00001600
             03 MWDCMPTC     PIC X.                                     00001610
             03 MWDCMPTP     PIC X.                                     00001620
             03 MWDCMPTH     PIC X.                                     00001630
             03 MWDCMPTV     PIC X.                                     00001640
             03 MWDCMPTO     PIC X.                                     00001650
             03 FILLER       PIC X(2).                                  00001660
             03 MWCTFDCA     PIC X.                                     00001670
             03 MWCTFDCC     PIC X.                                     00001680
             03 MWCTFDCP     PIC X.                                     00001690
             03 MWCTFDCH     PIC X.                                     00001700
             03 MWCTFDCV     PIC X.                                     00001710
             03 MWCTFDCO     PIC X.                                     00001720
             03 FILLER       PIC X(2).                                  00001730
             03 MWRCOFFA     PIC X.                                     00001740
             03 MWRCOFFC     PIC X.                                     00001750
             03 MWRCOFFP     PIC X.                                     00001760
             03 MWRCOFFH     PIC X.                                     00001770
             03 MWRCOFFV     PIC X.                                     00001780
             03 MWRCOFFO     PIC X.                                     00001790
             03 FILLER       PIC X(2).                                  00001800
             03 MCMOPACA     PIC X.                                     00001810
             03 MCMOPACC     PIC X.                                     00001820
             03 MCMOPACP     PIC X.                                     00001830
             03 MCMOPACH     PIC X.                                     00001840
             03 MCMOPACV     PIC X.                                     00001850
             03 MCMOPACO     PIC X(5).                                  00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MLIBERRA  PIC X.                                          00001880
           02 MLIBERRC  PIC X.                                          00001890
           02 MLIBERRP  PIC X.                                          00001900
           02 MLIBERRH  PIC X.                                          00001910
           02 MLIBERRV  PIC X.                                          00001920
           02 MLIBERRO  PIC X(78).                                      00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MCODTRAA  PIC X.                                          00001950
           02 MCODTRAC  PIC X.                                          00001960
           02 MCODTRAP  PIC X.                                          00001970
           02 MCODTRAH  PIC X.                                          00001980
           02 MCODTRAV  PIC X.                                          00001990
           02 MCODTRAO  PIC X(4).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MCICSA    PIC X.                                          00002020
           02 MCICSC    PIC X.                                          00002030
           02 MCICSP    PIC X.                                          00002040
           02 MCICSH    PIC X.                                          00002050
           02 MCICSV    PIC X.                                          00002060
           02 MCICSO    PIC X(5).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MNETNAMA  PIC X.                                          00002090
           02 MNETNAMC  PIC X.                                          00002100
           02 MNETNAMP  PIC X.                                          00002110
           02 MNETNAMH  PIC X.                                          00002120
           02 MNETNAMV  PIC X.                                          00002130
           02 MNETNAMO  PIC X(8).                                       00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MSCREENA  PIC X.                                          00002160
           02 MSCREENC  PIC X.                                          00002170
           02 MSCREENP  PIC X.                                          00002180
           02 MSCREENH  PIC X.                                          00002190
           02 MSCREENV  PIC X.                                          00002200
           02 MSCREENO  PIC X(4).                                       00002210
                                                                                
