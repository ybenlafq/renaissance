      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GARAN GARANTIES GRATUITES DARTY        *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01DB.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01DB.                                                            
      *}                                                                        
           05  GARAN-CTABLEG2    PIC X(15).                                     
           05  GARAN-CTABLEG2-REDEF REDEFINES GARAN-CTABLEG2.                   
               10  GARAN-CGARAN          PIC X(05).                             
           05  GARAN-WTABLEG     PIC X(80).                                     
           05  GARAN-WTABLEG-REDEF  REDEFINES GARAN-WTABLEG.                    
               10  GARAN-LGARAN          PIC X(20).                             
               10  GARAN-NBMOIS          PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  GARAN-NBMOIS-N       REDEFINES GARAN-NBMOIS                  
                                         PIC 9(02).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01DB-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01DB-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GARAN-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GARAN-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GARAN-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GARAN-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
