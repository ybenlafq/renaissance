      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE NUECS ECS VERSION GCV-GL-AP            *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01FK.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01FK.                                                            
      *}                                                                        
           05  NUECS-CTABLEG2    PIC X(15).                                     
           05  NUECS-CTABLEG2-REDEF REDEFINES NUECS-CTABLEG2.                   
               10  NUECS-NUMECS          PIC X(05).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  NUECS-NUMECS-N       REDEFINES NUECS-NUMECS                  
                                         PIC 9(05).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  NUECS-WTABLEG     PIC X(80).                                     
           05  NUECS-WTABLEG-REDEF  REDEFINES NUECS-WTABLEG.                    
               10  NUECS-JC              PIC X(10).                             
               10  NUECS-JP              PIC X(10).                             
               10  NUECS-JF              PIC X(10).                             
               10  NUECS-SENS            PIC X(01).                             
               10  NUECS-LECS            PIC X(25).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01FK-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01FK-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NUECS-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  NUECS-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NUECS-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  NUECS-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
