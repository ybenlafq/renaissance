      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  COMM-GN05-APPLI.                                                     
           05  COMM-GN05-CODTRAIT              PIC  9.                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COMM-GN05-CODRET       COMP     PIC  S9(4).                      
      *--                                                                       
           05  COMM-GN05-CODRET COMP-5     PIC  S9(4).                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COMM-GN05-TOTAL        COMP     PIC  S9(4).                      
      *--                                                                       
           05  COMM-GN05-TOTAL COMP-5     PIC  S9(4).                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COMM-GN05-NCONCN-MAX   COMP     PIC  S9(4).                      
      *--                                                                       
           05  COMM-GN05-NCONCN-MAX COMP-5     PIC  S9(4).                      
      *}                                                                        
           05  COMM-GN05-T-NCONCN.                                              
             10  COMM-GN05-NCONCN  OCCURS  1000  PIC  X(4).                     
           05  FILLER                          PIC  X(4089).                    
                                                                                
