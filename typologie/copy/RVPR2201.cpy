      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************00000010
      * COBOL DECLARATION FOR TABLE RVPR2201                           *00000020
      ******************************************************************00000030
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVPR2201.                                                    00000040
           10 PR22-CTYPE           PIC X(1).                            00000050
           10 PR22-NENTCDE         PIC X(5).                            00000060
           10 PR22-NCHRONO         PIC X(5).                            00000070
           10 PR22-NLIGNE          PIC X(5).                            00000080
           10 PR22-MONTANT         PIC S9(7)V9(2) USAGE COMP-3.         00000090
           10 PR22-DEFFET          PIC X(8).                            00000100
           10 PR22-DFINEFFET       PIC X(8).                            00000110
           10 PR22-DMAJ            PIC X(8).                            00000120
           10 PR22-DSYST           PIC S9(13)V USAGE COMP-3.            00000130
           10 PR22-TAUX            PIC S9(3)V9(2) USAGE COMP-3.         00000140
           10 PR22-DCOMPTA         PIC X(8).                            00000150
      ******************************************************************00000160
      * INDICATOR VARIABLE STRUCTURE                                   *00000170
      ******************************************************************00000180
       01  RVPR2201-FLAGS.                                              00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR22-CTYPE-F         PIC S9(4) COMP.                      00000200
      *--                                                                       
           10 PR22-CTYPE-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR22-NENTCDE-F       PIC S9(4) COMP.                      00000210
      *--                                                                       
           10 PR22-NENTCDE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR22-NCHRONO-F       PIC S9(4) COMP.                      00000220
      *--                                                                       
           10 PR22-NCHRONO-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR22-NLIGNE-F        PIC S9(4) COMP.                      00000230
      *--                                                                       
           10 PR22-NLIGNE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR22-MONTANT-F       PIC S9(4) COMP.                      00000240
      *--                                                                       
           10 PR22-MONTANT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR22-DEFFET-F        PIC S9(4) COMP.                      00000250
      *--                                                                       
           10 PR22-DEFFET-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR22-DFINEFFET-F     PIC S9(4) COMP.                      00000260
      *--                                                                       
           10 PR22-DFINEFFET-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR22-DMAJ-F          PIC S9(4) COMP.                      00000270
      *--                                                                       
           10 PR22-DMAJ-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR22-DSYST-F         PIC S9(4) COMP.                      00000280
      *--                                                                       
           10 PR22-DSYST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR22-TAUX-F          PIC S9(4) COMP.                      00000290
      *--                                                                       
           10 PR22-TAUX-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PR22-DCOMPTA-F       PIC S9(4) COMP.                      00000300
      *                                                                         
      *--                                                                       
           10 PR22-DCOMPTA-F       PIC S9(4) COMP-5.                            
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
