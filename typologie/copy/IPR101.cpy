      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPR101 AU 01/03/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,03,BI,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,05,BI,A,                          *        
      *                           25,01,BI,A,                          *        
      *                           26,05,BI,A,                          *        
      *                           31,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPR101.                                                        
            05 NOMETAT-IPR101           PIC X(6) VALUE 'IPR101'.                
            05 RUPTURES-IPR101.                                                 
           10 IPR101-CHEFPROD           PIC X(05).                      007  005
           10 IPR101-NLIEU              PIC X(03).                      012  003
           10 IPR101-CTYPPREST          PIC X(05).                      015  005
           10 IPR101-NENTCDE            PIC X(05).                      020  005
           10 IPR101-WMAJOPT            PIC X(01).                      025  001
           10 IPR101-CPRESTATION        PIC X(05).                      026  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPR101-SEQUENCE           PIC S9(04) COMP.                031  002
      *--                                                                       
           10 IPR101-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPR101.                                                   
           10 IPR101-CDEVISE            PIC X(06).                      033  006
           10 IPR101-LCHEFPROD          PIC X(20).                      039  020
           10 IPR101-LMAJOPT            PIC X(16).                      059  016
           10 IPR101-LPRESTATION        PIC X(20).                      075  020
           10 IPR101-NSOCIETE           PIC X(03).                      095  003
           10 IPR101-CA                 PIC S9(09)      COMP-3.         098  005
           10 IPR101-CA-MOIS            PIC S9(09)      COMP-3.         103  005
           10 IPR101-CA-7               PIC S9(09)      COMP-3.         108  005
           10 IPR101-NBPREST            PIC S9(06)      COMP-3.         113  004
           10 IPR101-NBPREST-MOIS       PIC S9(06)      COMP-3.         117  004
           10 IPR101-NBPREST-7          PIC S9(06)      COMP-3.         121  004
           10 IPR101-VOLNET             PIC S9(06)      COMP-3.         125  004
           10 IPR101-VOLNET-7           PIC S9(06)      COMP-3.         129  004
           10 IPR101-DVENTE             PIC X(08).                      133  008
            05 FILLER                      PIC X(372).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPR101-LONG           PIC S9(4)   COMP  VALUE +140.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPR101-LONG           PIC S9(4) COMP-5  VALUE +140.           
                                                                                
      *}                                                                        
