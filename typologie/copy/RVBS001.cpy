      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE BS001 COMMISSIONS TIERS                *        
      *----------------------------------------------------------------*        
       01  RVBS001.                                                             
           05  BS001-CTABLEG2    PIC X(15).                                     
           05  BS001-CTABLEG2-REDEF REDEFINES BS001-CTABLEG2.                   
               10  BS001-NENTITE         PIC X(05).                             
               10  BS001-TYPE            PIC X(05).                             
               10  BS001-CTYPCND         PIC X(05).                             
           05  BS001-WTABLEG     PIC X(80).                                     
           05  BS001-WTABLEG-REDEF  REDEFINES BS001-WTABLEG.                    
               10  BS001-LIBELLE         PIC X(20).                             
               10  BS001-TIERS           PIC X(06).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVBS001-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BS001-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  BS001-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BS001-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  BS001-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
