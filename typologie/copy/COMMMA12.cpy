      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************00000010
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *00000020
      ******************************************************************00000030
      *                                                                *00000040
      *  PROJET     :                                                   00000050
      *  PROGRAMME  : MFA12                                            *00000060
      *  TITRE      : COMMAREA DU MODULE TP                            *00000070
      *               D'IMPRESSION DE LA LISTE                         *00000080
      *               DES PRODUITS EN REPORTS DE LIVRAIS UNE PLATE FORME00000090
      *  LONGUEUR   : 200 C                                            *00000100
      *                                                                *00000110
      ******************************************************************00000120
       01  COMM-MA12-APPLI.                                             00000130
           05 COMM-MA12-ZONES-ENTREE.                                   00000140
             10 COMM-MA12-NSOCIETE           PIC X(3).                  00000150
             10 COMM-MA12-NLIEU              PIC X(3).                  00000160
             10 COMM-MA12-LLIEU              PIC X(20).                 00000170
             10 COMM-MA12-DELAI              PIC 9(2).                  00000180
             10 COMM-MA12-SSAAMMJJ           PIC X(8).                  00000190
GC           10 COMM-MA12-DLIVRAISON1        PIC X(8).                  00000200
GC           10 COMM-MA12-DLIVRAISON2        PIC X(8).                  00000210
GC           10 COMM-MA12-TRI                PIC X.                     00000220
           05 COMM-MA12-ZONES-SORTIE.                                   00000230
             10 COMM-MA12-MESSAGE            PIC X(80).                 00000240
      *    05 COMM-MA12-FILLER          PIC X(84).                      00000250
           05 COMM-MA12-FILLER          PIC X(68).                      00000260
                                                                                
