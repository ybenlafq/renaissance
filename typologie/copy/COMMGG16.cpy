      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
              03  COMMGG16 REDEFINES COMM-GG00-FILLER.                          
                  04  COMM-GG16-IAP        PIC 9.                               
                  04  COMM-GG16-PER        PIC 9.                               
                  04  COMM-GG16-LFAM       PIC X(20).                           
                  04  COMM-GG16-CRAYON     PIC X(05).                           
                  04  COMM-GG16-LRAYON     PIC X(20).                           
                  04  COMM-GG16-LAPPRO     PIC X(20).                           
                  04  COMM-GG16-FILLER     PIC X(3433).                         
                                                                                
