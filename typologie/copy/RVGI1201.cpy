      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      **********************************************************                
      *   COPY DE LA TABLE RVGI1201                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGI1201                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGI1201.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGI1201.                                                            
      *}                                                                        
           02  GI12-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GI12-CTABINTMB                                                   
               PIC X(0005).                                                     
           02  GI12-MB                                                          
               PIC S9(5) COMP-3.                                                
           02  GI12-TMB                                                         
               PIC S9(3) COMP-3.                                                
           02  GI12-FM                                                          
               PIC S9(3) COMP-3.                                                
           02  GI12-TYPPA                                                       
               PIC X.                                                           
           02  GI12-MONTANT                                                     
               PIC S9(3)V99 COMP-3.                                             
           02  GI12-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGI1201                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGI1201-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGI1201-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI12-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI12-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI12-CTABINTMB-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI12-CTABINTMB-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI12-MB-F                                                        
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI12-MB-F                                                        
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI12-TMB-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI12-TMB-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI12-FM-F                                                        
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI12-FM-F                                                        
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI12-TYPPA-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI12-TYPPA-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI12-MONTANT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI12-MONTANT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI12-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI12-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
