      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TGA01                     TR: GA00      00002200
      *           TR: GA00  ADMINISTRATION DES DONNEES                  00002300
      *        PG: TGA01 CREATION MAJ DES TABLES GENERALISEES           00002400
      **************************************************************    00002500
      * ZONES RESERVEES APPLICATIVES ----------------------- 3724       00010000
      * PROGRAMME TGA01 : CREATION MAJ DES TABLES GENERALISEES          00410200
      **************************************************************    00412000
          02 COMM-GA01-APPLI REDEFINES COMM-GA00-APPLI.                 00420000
      *------------------------------ CODE FONCTION                     00510000
             03 COMM-GA01-FONCT          PIC X(3).                      00520000
      *------------------------------ SELECTION CLE                     00521011
             03 COMM-GA01-SELECT         PIC X(15).                     00522011
      *------------------------------ LIBELLE FONCTION                  00523011
             03 COMM-GA01-LFONCT         PIC X(14).                     00524011
      *----- TABLE 71                                                   00570003
             03  COMM-GA01-T71.                                         00572004
              04  COMM-GA01-CTABLE                                      00572104
                  PIC X(0006).                                          00573004
              04  COMM-GA01-CSTABLE                                     00574004
                  PIC X(0005).                                          00575004
              04  COMM-GA01-LTABLE                                      00576004
                  PIC X(0030).                                          00577004
              04  COMM-GA01-CRESP                                       00578004
                  PIC X(0003).                                          00579004
              04  COMM-GA01-DDEBUT                                      00579104
                  PIC X(0008).                                          00579204
              04  COMM-GA01-DFIN                                        00579304
                  PIC X(0008).                                          00579404
              04  COMM-GA01-DMAJ                                        00579504
                  PIC X(0008).                                          00579604
              04  COMM-GA01-6-CHAMP OCCURS 6 TIMES.                     00579704
               05  COMM-GA01-CHAMP PIC  X(008).                         00579804
               05  COMM-GA01-LCHAMP PIC X(030).                         00579904
               05  COMM-GA01-QCHAMP PIC S9(003) COMP-3.                 00580004
               05  COMM-GA01-WCHAMP PIC X(001).                         00581004
               05  COMM-GA01-QDEC  PIC S9(003) COMP-3.                  00582004
               05  COMM-GA01-WCTLDIR PIC X(0001).                       00583004
               05  COMM-GA01-WCTLIND PIC X(0001).                       00584004
               05  COMM-GA01-CCTLIND1 PIC X(0003).                      00585004
               05  COMM-GA01-CCTLIND2 PIC X(0003).                      00586004
               05  COMM-GA01-CCTLIND3 PIC X(0003).                      00587004
               05  COMM-GA01-CCTLIND4 PIC X(0003).                      00588004
               05  COMM-GA01-CCTLIND5 PIC X(0003).                      00589004
               05  COMM-GA01-CTABASS PIC X(0006).                       00589104
               05  COMM-GA01-CSTABASS PIC X(0005).                      00589204
               05  COMM-GA01-WPOSCLE PIC S9(001) COMP-3.                00589304
      *-----                                                            00590000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GA01-PAGE-COURANTE  PIC S9(004) COMP.              00600006
      *--                                                                       
             03 COMM-GA01-PAGE-COURANTE  PIC S9(004) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GA01-NBRE-PAGE      PIC S9(004) COMP.              00610006
      *--                                                                       
             03 COMM-GA01-NBRE-PAGE      PIC S9(004) COMP-5.                    
      *}                                                                        
      *----- 168 C ETENDU                                               00620007
             03 COMM-GA01-MEMO-POS-ATTR.                                00630007
               05  FILLER OCCURS 14 TIMES.                              00631007
                07 FILLER OCCURS  6 TIMES.                              00632007
                 09 COMM-GA01-POS-ATTR PIC 9(0002).                     00633007
      *-----                                                            00640007
             03  FILLER OCCURS 100 TIMES.                               00641008
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 COMM-GA01-NBRE-LIGNE     PIC S9(004) COMP.             00650009
      *--                                                                       
              05 COMM-GA01-NBRE-LIGNE     PIC S9(004) COMP-5.                   
      *}                                                                        
      *-----                                                            00660008
             03 COMM-GA01-LIBRE1         PIC X(2820).                   00710611
      *                                                                 00711000
      ***************************************************************** 00740000
                                                                                
