      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE AV000 TYPE D UTILISATION DES AVOIRS    *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVGA01T9.                                                            
           05  AV000-CTABLEG2    PIC X(15).                                     
           05  AV000-CTABLEG2-REDEF REDEFINES AV000-CTABLEG2.                   
               10  AV000-CTYPUTIL        PIC X(05).                             
           05  AV000-WTABLEG     PIC X(80).                                     
           05  AV000-WTABLEG-REDEF  REDEFINES AV000-WTABLEG.                    
               10  AV000-LTYPUTIL        PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01T9-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AV000-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  AV000-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AV000-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  AV000-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
