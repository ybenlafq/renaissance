      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************            
      * TS GA50 DEPOT LIES A LA FAMILLE                                         
E0048 ******************************************************************        
      * DSA057 17/12/03 SUPPORT MAINTENANCE EVOLUTION 48                        
      *                 GESTION STATUT MULTI DEPOT SUR TGA53                    
      *                 REFONTE TOTALE                                          
      ******************************************************************        
E0713 * DE02004 03/12/14 PROJET MGD                                             
      *                  AJOUT DU NB COLIS                                      
      *****************************************************************         
       01 TS-DEP-CFAM.                                                          
           05 TS-DEP-TYPE PIC 9.                                                
               88 TS-DEP-NOTYPE VALUE 0.                                        
               88 TS-DEP-STATUT VALUE 1.                                        
               88 TS-DEP-MODIF  VALUE 2.                                        
               88 TS-DEP-PRINC  VALUE 3.                                        
E0673          88 TS-DEP-SECOND VALUE 4.                                        
           05 TS-DEP-WABC PIC X.                                                
           05 TS-DEP-WMS PIC X.                                                 
           05 TS-DEP-FL50.                                                      
               10 TS-DEP-DEPOT.                                                 
                   15 TS-DEP-NDEPSOC  PIC X(3).                                 
                   15 TS-DEP-NDEPLIEU PIC X(3).                                 
               10 TS-DEP-NCODIC       PIC X(7).                                 
               10 TS-DEP-D1RECEPT     PIC X(8).                                 
               10 TS-DEP-CAPPRO       PIC X(5).                                 
               10 TS-DEP-WSENSAPPRO   PIC X(1).                                 
               10 TS-DEP-QDELAIAPPRO  PIC X(3).                                 
               10 TS-DEP-QCOLICDE     PIC S9(3) COMP-3.                         
               10 TS-DEP-LEMBALLAGE   PIC X(50).                                
               10 TS-DEP-QCOLIRECEPT  PIC S9(5) COMP-3.                         
               10 TS-DEP-QCOLIDSTOCK  PIC S9(5) COMP-3.                         
               10 TS-DEP-CMODSTOCK1   PIC X(5).                                 
               10 TS-DEP-WMODSTOCK1   PIC X(1).                                 
               10 TS-DEP-CMODSTOCK2   PIC X(5).                                 
               10 TS-DEP-WMODSTOCK2   PIC X(1).                                 
               10 TS-DEP-CMODSTOCK3   PIC X(5).                                 
               10 TS-DEP-WMODSTOCK3   PIC X(1).                                 
               10 TS-DEP-CFETEMPL     PIC X(1).                                 
               10 TS-DEP-QNBRANMAIL   PIC S9(3) COMP-3.                         
               10 TS-DEP-QNBNIVGERB   PIC S9(3) COMP-3.                         
               10 TS-DEP-CZONEACCES   PIC X(1).                                 
               10 TS-DEP-CCONTENEUR   PIC X(1).                                 
               10 TS-DEP-CSPECIFSTK   PIC X(1).                                 
               10 TS-DEP-CCOTEHOLD    PIC X(1).                                 
               10 TS-DEP-QNBPVSOL     PIC S9(3) COMP-3.                         
               10 TS-DEP-QNBPRACK     PIC S9(5) COMP-3.                         
               10 TS-DEP-CTPSUP       PIC X(1).                                 
               10 TS-DEP-WRESFOURN    PIC X(1).                                 
               10 TS-DEP-CQUOTA       PIC X(5).                                 
               10 TS-DEP-CUSINE       PIC X(5).                                 
               10 TS-DEP-DMAJ         PIC X(8).                                 
               10 TS-DEP-CEXPO        PIC X(5).                                 
               10 TS-DEP-LSTATCOMP    PIC X(3).                                 
               10 TS-DEP-WSTOCKAVANCE PIC X(1).                                 
               10 TS-DEP-DSYST        PIC S9(13) COMP-3.                        
               10 TS-DEP-CLASSE       PIC X(1).                                 
               10 TS-DEP-QCOUCHPAL    PIC S9(2) COMP-3.                         
               10 TS-DEP-QCARTCOUCH   PIC S9(2) COMP-3.                         
               10 TS-DEP-QBOXCART     PIC S9(4) COMP-3.                         
               10 TS-DEP-QPRODBOX     PIC S9(4) COMP-3.                         
               10 TS-DEP-CUNITRECEPT  PIC X(3).                                 
E0601          10 TS-DEP-QPRODCAM     PIC S9(5) COMP-3.                         
E0713          10 TS-DEP-QECO         PIC S9(5) COMP-3.                         
E0713          10 TS-DEP-QNBCOL       PIC S9(5) COMP-3.                         
E0048-     05 TS-DEP-LIB.                                                       
               10 TS-DEP-LAPPRO       PIC X(20).                                
               10 TS-DEP-LEXPO        PIC X(20).                                
           05 TS-DEP-ORIG.                                                      
               10 TS-DEP-CAPPRO-O     PIC X(5).                                 
               10 TS-DEP-LAPPRO-O     PIC X(20).                                
               10 TS-DEP-WSENSAPPRO-O PIC X(1).                                 
               10 TS-DEP-CEXPO-O      PIC X(5).                                 
               10 TS-DEP-LEXPO-O      PIC X(20).                                
               10 TS-DEP-LSTATCOMP-O  PIC X(3).                                 
-E0048     05 TS-DEP-INDEX            PIC S9(3) COMP-3.                         
                                                                                
