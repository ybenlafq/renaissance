      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *                             DABORD LA COPY DE L"ENTETE MESSMQ  *      13
      *   MESTHE30 COPY                                                *      13
      *  201  LONGUEUR                                                 *      13
      * COPY POUR LES PROGRAMMES MHE30                                 *      13
      ******************************************************************        
      * LONGUEUR DE 70  OCTETS                                                  
              05 MESTHE30-RETOUR.                                               
      * LONGUEUR DE 70  OCTETS                                                  
                 10 MESTHE30-CODE-RETOUR.                                       
                    15  MESTHE30-CODRET  PIC X(1).                              
                         88  MESTHE30-CODRET-OK        VALUE ' '.               
                         88  MESTHE30-CODRET-ERREUR    VALUE '1'.               
                    15  MESTHE30-LIBERR  PIC X(60).                             
                    15  MESTHE30-AS400CR PIC X(08).                             
                    15  MESTHE30-DEPASS  PIC X(01).                             
              05 MESTHE30-DATA.                                                 
      * ZONE DE DONNEES DE TRANSFERT                                            
      * LONGUEUR DE 131 OCTETS                                                  
                 10 MESTHE30-DATA-TRANSFERT.                                    
                    15 MESTHE30-PDOSNASC   PIC X(03).                           
                    15 MESTHE30-DOSNASC    PIC X(09).                           
                    15 MESTHE30-NLIEUHED   PIC X(03).                           
                    15 MESTHE30-NGHE       PIC X(07).                           
                    15 MESTHE30-NCODIC     PIC X(07).                           
                    15 MESTHE30-NSERIE     PIC X(20).                           
                    15 MESTHE30-NSOC       PIC X(03).                           
                    15 MESTHE30-LIEU       PIC X(03).                           
                    15 MESTHE30-PREFIXE    PIC X(03).                           
                 10 FILLER                 PIC X(73).                           
                                                                                
