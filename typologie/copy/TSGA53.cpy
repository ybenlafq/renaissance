      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      * TS CODES DESCRIPTIFS ET VALEURS                                         
      *****************************************************************         
      * DSA057 01/03/04 SUPPORT EVOLUTION                                       
      *                 GESTION AUTOMATIQUE ATTRIBUTS NUMERIQUES                
      ******************************************************************        
      * DSA057 13/12/04 SUPPORT EVOLUTION E56                                   
      *                 SAISIE OPCO VALEUR MODIFIABLE                           
      *****************************************************************         
       01 TS-GA53.                                                              
      *    LONGUEUR TS                                                          
      *    05 TS-GA53-LONG              PIC S9(5) COMP-3 VALUE +58.             
      *    05 TS-GA53-LONG PIC S9(5) COMP-3 VALUE +100.                         
           05 TS-GA53-LONG PIC S9(5) COMP-3 VALUE +120.                         
           05 TS-GA53-DONNEES.                                                  
               10 TS-GA53-NCODIC        PIC X(7).                               
               10 TS-GA53-CDESCRIPTIF   PIC X(5).                               
               10 TS-GA53-CVDESCRIPT    PIC X(5).                               
               10 TS-GA53-CMAJ          PIC X(1).                               
               10 TS-GA53-LDESCRIPTIF   PIC X(20).                              
               10 TS-GA53-LVDESCRIPT    PIC X(20).                              
      *        DONNEES ORIGINALES                                               
               10 TS-GA53-CVDESCRIPT-O  PIC X(05).                              
               10 TS-GA53-LVDESCRIPT-O  PIC X(20).                              
      *        TYPE DU CODE DESCRIPTIF                                          
               10 TS-GA53-CTYPE         PIC X.                                  
               10 TS-GA53-NBDEC         PIC X.                                  
               10 TS-GA53-NMIN          PIC X(5).                               
               10 TS-GA53-NMAX          PIC X(5).                               
               10 TS-GA53-CMESURE       PIC X(5).                               
      *        FLAG CDESCRIPTIF ISSU DE LA RTGA18                               
               10 TS-GA53-WDESC         PIC X(1).                               
      *        TYPAGE DE LA VALEUR SELON SS-TABLE OBSOL                         
      *        A)CTIF SI RIEN DANS OBSOL                                        
      *        M)ODIFIABLE O)BSOLETE R)EMPLACABLE SELON OBSOL                   
               10 TS-GA53-VAL-WACTIF    PIC X(1).                               
               10 TS-GA53-VAL-WACTIF-O  PIC X(1).                               
               10 FILLER                PIC X(17).                              
                                                                                
