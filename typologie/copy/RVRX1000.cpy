      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRX1000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRX1000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRX1000.                                                            
           02  RX10-NCONC                                                       
               PIC X(0004).                                                     
           02  RX10-NMAG                                                        
               PIC X(0003).                                                     
           02  RX10-CRAYON                                                      
               PIC X(0005).                                                     
           02  RX10-NFREQUENCE                                                  
               PIC X(0001).                                                     
           02  RX10-NSEQUENCE                                                   
               PIC X(0001).                                                     
           02  RX10-D1SUPPORT                                                   
               PIC X(0008).                                                     
           02  RX10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRX1000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRX1000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX10-NCONC-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX10-NCONC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX10-NMAG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX10-NMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX10-CRAYON-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX10-CRAYON-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX10-NFREQUENCE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX10-NFREQUENCE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX10-NSEQUENCE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX10-NSEQUENCE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX10-D1SUPPORT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX10-D1SUPPORT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
