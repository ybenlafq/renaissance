      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TGA06                              *    00002200
      *       TR : GA00  ADMISTRATION DES DONNEES                  *    00002300
      *       PG : TGA06 MISE A JOUR DE LA TABLE DES ZONES DE PRIX *    00002400
      **************************************************************    00002500
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00009000
      *                                                                 00410100
      * PROGRAMME  TGA06 : MISE A JOUR DE LA TABLE DES ZONES DE PRIX  * 00411000
      *                                                                 00412000
          02 COMM-GA06-APPLI REDEFINES COMM-GA00-APPLI.                 00420000
      *------------------------------ CODE FONCTION                     00510000
             03 COMM-GA06-FONCT          PIC X(3).                      00520002
      *------------------------------ LIBELLE FONCTION                  00521001
             03 COMM-GA06-LFONCT         PIC X(20).                     00522001
      *------------------------------ CODE TABLE                        00530000
             03 COMM-GA06-CTABLE         PIC X(6).                      00540000
      *------------------------------ CODE SOUS-TABLE                   00550000
             03 COMM-GA06-CSTABLE        PIC X(5).                      00560000
      *------------------------------ CODE SOCIETE                      00560100
             03 COMM-GA06-NSOCIETE       PIC X(3).                      00560200
      *------------------------------ LIBELLE SOCIETE                   00560300
             03 COMM-GA06-LSOCIETE       PIC X(20).                     00560400
      *------------------------------ NOMBRE DE POSTES                  00560500
             03 COMM-GA06-NBPOSTE        PIC 99.                        00560600
      *                                                                 00560700
      *------------------------------GA06 : ZONE TABLE DES ZONES DE PRIX00561000
      *                                                                 00562000
      *------------------------------ POSTES TABLE                      00690000
             03 COMM-GA06-POSTE          OCCURS 6.                      00700000
      *------------------------------ CODE ZONES DE PRIX                00703000
                04 COMM-GA06-NUMZP       PIC X(2).                      00704000
      *------------------------------ LIBELLE ZONES DE PRIX             00705000
                04 COMM-GA06-LIBZP       PIC X(20).                     00706000
      *------------------------------ FLAG ZONES DE PRIX PRINCIPALE     00707000
                04 COMM-GA06-INDZP       PIC X(1).                      00708000
      *------------------------------ ZONE LIBRE                        00710400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GA06-POS-MAX        PIC S9(4) COMP.                00710502
      *--                                                                       
             03 COMM-GA06-POS-MAX        PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GA06-ITEM           PIC S9(4) COMP.                00710503
      *--                                                                       
             03 COMM-GA06-ITEM           PIC S9(4) COMP-5.                      
      *}                                                                        
             03 COMM-GA06-LIBRE1         PIC X(3523).                   00710504
      *                                                                 00711000
      ***************************************************************** 00740000
                                                                                
