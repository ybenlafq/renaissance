      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FGSOC LIEN SOC. COMPTA-SOC. FISCALE    *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01MC.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01MC.                                                            
      *}                                                                        
           05  FGSOC-CTABLEG2    PIC X(15).                                     
           05  FGSOC-CTABLEG2-REDEF REDEFINES FGSOC-CTABLEG2.                   
               10  FGSOC-NSOCC           PIC X(03).                             
           05  FGSOC-WTABLEG     PIC X(80).                                     
           05  FGSOC-WTABLEG-REDEF  REDEFINES FGSOC-WTABLEG.                    
               10  FGSOC-WACTIF          PIC X(01).                             
               10  FGSOC-NSOCFISC        PIC X(03).                             
               10  FGSOC-WFETAB          PIC X(01).                             
               10  FGSOC-WALIM           PIC X(01).                             
               10  FGSOC-LIBELLE         PIC X(25).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01MC-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01MC-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGSOC-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FGSOC-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FGSOC-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FGSOC-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
