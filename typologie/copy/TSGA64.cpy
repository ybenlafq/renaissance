      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00002000
      * TS SPECIFIQUE TABLE TSGA64 : MODES DE DELIVRANCE           *    00002200
      *                              DE L ARTICLE                  *    00002300
      *       TR : GA50  IDENTIFICATION ARTICLE                    *    00002400
      *       PG : TGA64 SAISIE, MISE A JOUR DES DONNEES DE VENTE  *    00002500
      **************************************************************    00002600
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ------------------------------ 40  00009000
      *                                                                 00410100
      * PROGRAMME  TGA64 : SAISIE , MISE A JOUR DES DONNEES DE VENTE  * 00411000
      *                                                                 00412000
       01  TS-GA64.                                                     00420000
      *------------------------------ LONGUEUR                          00430000
           05 TS-GA64-LONG            PIC S9(3) COMP-3 VALUE 40.        00440000
      *------------------------------ DONNEES TS                        00440100
           05 TS-GA64-DONNEES.                                          00440200
      *------------------------------ DONNEES RTGA64                    00440300
              10 TS-GA64-TABLE.                                         00440400
      *------------------------------ CODIC                             00521000
                 15 TS-GA64-NCODIC    PIC X(7).                         00522000
      *------------------------------ CODE MODE DE DELIVRANCE           00523000
                 15 TS-GA64-CMODDEL   PIC X(3).                         00524000
      *------------------------------ CODE MISE A JOUR                  00561100
                 15 TS-GA64-CMAJ      PIC X(1).                         00561200
      *------------------------------ LIBELLE CARACTERISTIQUE SPE.      00561300
              10 TS-GA64-LMODDEL      PIC X(20).                        00561400
      *------------------------------ RESERVE                           00561500
              10 TS-GA64-FILLER       PIC X(9).                         00561600
      *                                                                 00562000
      ***************************************************************** 00740000
                                                                                
