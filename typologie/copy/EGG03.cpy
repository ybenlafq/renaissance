      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGG03   EGG03                                              00000020
      ***************************************************************** 00000030
       01   EGG03I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNALIGNL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNALIGNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNALIGNF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNALIGNI  PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCONCL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNCONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCONCF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCONCI   PIC X(4).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENSGNL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLENSGNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLENSGNF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLENSGNI  PIC X(15).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLEMPLCL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLEMPLCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLEMPLCF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLEMPLCI  PIC X(15).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMZPL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNUMZPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNUMZPF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNUMZPI   PIC X(2).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLZONPL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLZONPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLZONPF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLZONPI   PIC X(20).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATSAIL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MDATSAIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATSAIF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDATSAII  PIC X(8).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATRELL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MDATRELL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATRELF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDATRELI  PIC X(8).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTPCARL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLTPCARL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTPCARF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLTPCARI  PIC X(15).                                      00000570
           02 MALIGNEI OCCURS   12 TIMES .                              00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNCODICI     PIC X(7).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCFAMI  PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCMARQI      PIC X(5).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MLREFI  PIC X(20).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPVRL   COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MPVRL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MPVRF   PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MPVRI   PIC X(6).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPVDARTL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MPVDARTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPVDARTF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MPVDARTI     PIC X(6).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MZONCMDI  PIC X(15).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MLIBERRI  PIC X(58).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCODTRAI  PIC X(4).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCICSI    PIC X(5).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MNETNAMI  PIC X(8).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MSCREENI  PIC X(4).                                       00001060
      ***************************************************************** 00001070
      * SDF: EGG03   EGG03                                              00001080
      ***************************************************************** 00001090
       01   EGG03O REDEFINES EGG03I.                                    00001100
           02 FILLER    PIC X(12).                                      00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MDATJOUA  PIC X.                                          00001130
           02 MDATJOUC  PIC X.                                          00001140
           02 MDATJOUP  PIC X.                                          00001150
           02 MDATJOUH  PIC X.                                          00001160
           02 MDATJOUV  PIC X.                                          00001170
           02 MDATJOUO  PIC X(10).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MTIMJOUA  PIC X.                                          00001200
           02 MTIMJOUC  PIC X.                                          00001210
           02 MTIMJOUP  PIC X.                                          00001220
           02 MTIMJOUH  PIC X.                                          00001230
           02 MTIMJOUV  PIC X.                                          00001240
           02 MTIMJOUO  PIC X(5).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MWPAGEA   PIC X.                                          00001270
           02 MWPAGEC   PIC X.                                          00001280
           02 MWPAGEP   PIC X.                                          00001290
           02 MWPAGEH   PIC X.                                          00001300
           02 MWPAGEV   PIC X.                                          00001310
           02 MWPAGEO   PIC X(3).                                       00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MWFONCA   PIC X.                                          00001340
           02 MWFONCC   PIC X.                                          00001350
           02 MWFONCP   PIC X.                                          00001360
           02 MWFONCH   PIC X.                                          00001370
           02 MWFONCV   PIC X.                                          00001380
           02 MWFONCO   PIC X(3).                                       00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MNALIGNA  PIC X.                                          00001410
           02 MNALIGNC  PIC X.                                          00001420
           02 MNALIGNP  PIC X.                                          00001430
           02 MNALIGNH  PIC X.                                          00001440
           02 MNALIGNV  PIC X.                                          00001450
           02 MNALIGNO  PIC X(7).                                       00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MNCONCA   PIC X.                                          00001480
           02 MNCONCC   PIC X.                                          00001490
           02 MNCONCP   PIC X.                                          00001500
           02 MNCONCH   PIC X.                                          00001510
           02 MNCONCV   PIC X.                                          00001520
           02 MNCONCO   PIC X(4).                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MLENSGNA  PIC X.                                          00001550
           02 MLENSGNC  PIC X.                                          00001560
           02 MLENSGNP  PIC X.                                          00001570
           02 MLENSGNH  PIC X.                                          00001580
           02 MLENSGNV  PIC X.                                          00001590
           02 MLENSGNO  PIC X(15).                                      00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MLEMPLCA  PIC X.                                          00001620
           02 MLEMPLCC  PIC X.                                          00001630
           02 MLEMPLCP  PIC X.                                          00001640
           02 MLEMPLCH  PIC X.                                          00001650
           02 MLEMPLCV  PIC X.                                          00001660
           02 MLEMPLCO  PIC X(15).                                      00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MNUMZPA   PIC X.                                          00001690
           02 MNUMZPC   PIC X.                                          00001700
           02 MNUMZPP   PIC X.                                          00001710
           02 MNUMZPH   PIC X.                                          00001720
           02 MNUMZPV   PIC X.                                          00001730
           02 MNUMZPO   PIC X(2).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MLZONPA   PIC X.                                          00001760
           02 MLZONPC   PIC X.                                          00001770
           02 MLZONPP   PIC X.                                          00001780
           02 MLZONPH   PIC X.                                          00001790
           02 MLZONPV   PIC X.                                          00001800
           02 MLZONPO   PIC X(20).                                      00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MDATSAIA  PIC X.                                          00001830
           02 MDATSAIC  PIC X.                                          00001840
           02 MDATSAIP  PIC X.                                          00001850
           02 MDATSAIH  PIC X.                                          00001860
           02 MDATSAIV  PIC X.                                          00001870
           02 MDATSAIO  PIC X(8).                                       00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MDATRELA  PIC X.                                          00001900
           02 MDATRELC  PIC X.                                          00001910
           02 MDATRELP  PIC X.                                          00001920
           02 MDATRELH  PIC X.                                          00001930
           02 MDATRELV  PIC X.                                          00001940
           02 MDATRELO  PIC X(8).                                       00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MLTPCARA  PIC X.                                          00001970
           02 MLTPCARC  PIC X.                                          00001980
           02 MLTPCARP  PIC X.                                          00001990
           02 MLTPCARH  PIC X.                                          00002000
           02 MLTPCARV  PIC X.                                          00002010
           02 MLTPCARO  PIC X(15).                                      00002020
           02 MALIGNEO OCCURS   12 TIMES .                              00002030
             03 FILLER       PIC X(2).                                  00002040
             03 MNCODICA     PIC X.                                     00002050
             03 MNCODICC     PIC X.                                     00002060
             03 MNCODICP     PIC X.                                     00002070
             03 MNCODICH     PIC X.                                     00002080
             03 MNCODICV     PIC X.                                     00002090
             03 MNCODICO     PIC X(7).                                  00002100
             03 FILLER       PIC X(2).                                  00002110
             03 MCFAMA  PIC X.                                          00002120
             03 MCFAMC  PIC X.                                          00002130
             03 MCFAMP  PIC X.                                          00002140
             03 MCFAMH  PIC X.                                          00002150
             03 MCFAMV  PIC X.                                          00002160
             03 MCFAMO  PIC X(5).                                       00002170
             03 FILLER       PIC X(2).                                  00002180
             03 MCMARQA      PIC X.                                     00002190
             03 MCMARQC PIC X.                                          00002200
             03 MCMARQP PIC X.                                          00002210
             03 MCMARQH PIC X.                                          00002220
             03 MCMARQV PIC X.                                          00002230
             03 MCMARQO      PIC X(5).                                  00002240
             03 FILLER       PIC X(2).                                  00002250
             03 MLREFA  PIC X.                                          00002260
             03 MLREFC  PIC X.                                          00002270
             03 MLREFP  PIC X.                                          00002280
             03 MLREFH  PIC X.                                          00002290
             03 MLREFV  PIC X.                                          00002300
             03 MLREFO  PIC X(20).                                      00002310
             03 FILLER       PIC X(2).                                  00002320
             03 MPVRA   PIC X.                                          00002330
             03 MPVRC   PIC X.                                          00002340
             03 MPVRP   PIC X.                                          00002350
             03 MPVRH   PIC X.                                          00002360
             03 MPVRV   PIC X.                                          00002370
             03 MPVRO   PIC X(6).                                       00002380
             03 FILLER       PIC X(2).                                  00002390
             03 MPVDARTA     PIC X.                                     00002400
             03 MPVDARTC     PIC X.                                     00002410
             03 MPVDARTP     PIC X.                                     00002420
             03 MPVDARTH     PIC X.                                     00002430
             03 MPVDARTV     PIC X.                                     00002440
             03 MPVDARTO     PIC X(6).                                  00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MZONCMDA  PIC X.                                          00002470
           02 MZONCMDC  PIC X.                                          00002480
           02 MZONCMDP  PIC X.                                          00002490
           02 MZONCMDH  PIC X.                                          00002500
           02 MZONCMDV  PIC X.                                          00002510
           02 MZONCMDO  PIC X(15).                                      00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MLIBERRA  PIC X.                                          00002540
           02 MLIBERRC  PIC X.                                          00002550
           02 MLIBERRP  PIC X.                                          00002560
           02 MLIBERRH  PIC X.                                          00002570
           02 MLIBERRV  PIC X.                                          00002580
           02 MLIBERRO  PIC X(58).                                      00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MCODTRAA  PIC X.                                          00002610
           02 MCODTRAC  PIC X.                                          00002620
           02 MCODTRAP  PIC X.                                          00002630
           02 MCODTRAH  PIC X.                                          00002640
           02 MCODTRAV  PIC X.                                          00002650
           02 MCODTRAO  PIC X(4).                                       00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MCICSA    PIC X.                                          00002680
           02 MCICSC    PIC X.                                          00002690
           02 MCICSP    PIC X.                                          00002700
           02 MCICSH    PIC X.                                          00002710
           02 MCICSV    PIC X.                                          00002720
           02 MCICSO    PIC X(5).                                       00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MNETNAMA  PIC X.                                          00002750
           02 MNETNAMC  PIC X.                                          00002760
           02 MNETNAMP  PIC X.                                          00002770
           02 MNETNAMH  PIC X.                                          00002780
           02 MNETNAMV  PIC X.                                          00002790
           02 MNETNAMO  PIC X(8).                                       00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MSCREENA  PIC X.                                          00002820
           02 MSCREENC  PIC X.                                          00002830
           02 MSCREENP  PIC X.                                          00002840
           02 MSCREENH  PIC X.                                          00002850
           02 MSCREENV  PIC X.                                          00002860
           02 MSCREENO  PIC X(4).                                       00002870
                                                                                
