      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE LIVRP PARAMETRES DU PERIMETRE DE LIV   *        
      *----------------------------------------------------------------*        
       01  RVGA01YF.                                                            
           05  LIVRP-CTABLEG2    PIC X(15).                                     
           05  LIVRP-CTABLEG2-REDEF REDEFINES LIVRP-CTABLEG2.                   
               10  LIVRP-CPERIM          PIC X(05).                             
               10  LIVRP-CTQUOTA         PIC X(01).                             
           05  LIVRP-WTABLEG     PIC X(80).                                     
           05  LIVRP-WTABLEG-REDEF  REDEFINES LIVRP-WTABLEG.                    
               10  LIVRP-CEQUIP          PIC X(05).                             
               10  LIVRP-CMODDEL         PIC X(05).                             
               10  LIVRP-WZONE           PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01YF-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LIVRP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  LIVRP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LIVRP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  LIVRP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
