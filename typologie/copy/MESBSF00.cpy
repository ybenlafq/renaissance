      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * - MESBSF00 COPY GENERALISES  29974 OCTETS                      *      13
      *   = 34  DE DEFINITION DE CODE RETOUR                           *        
      *   + 29940 DE MESSAGE REEL                                      *        
      * COPY POUR LE PROGRAMME BSF00 <-> MSF00                         *      13
      ******************************************************************        
              05 MESBSF00-RETOUR.                                               
                 10 MESBSF00-CODE-RETOUR.                                       
                    15  MESBSF00-CODRET  PIC X(1).                              
                         88  MESBSF00-CODRET-OK        VALUE ' '.               
                         88  MESBSF00-CODRET-ERREUR    VALUE '1'.               
                    15  MESBSF00-LIBERR  PIC X(33).                             
              05 MESBSF00-DATA           PIC X(29940).                          
              05 MESBSF00C-DATA REDEFINES MESBSF00-DATA.                        
      * LONGUEUR DE 1335 OCTETS                                                 
                 10 MESBSF00C-DATA-X.                                           
                    15 MESBSF00C-NSOCENTR      PIC  X(3).                       
                    15 MESBSF00C-NDEPOT        PIC  X(3).                       
                    15 MESBSF00C-NSOCIETE      PIC  X(3).                       
                    15 MESBSF00C-NLIEU         PIC  X(3).                       
                    15 MESBSF00C-NMUTATION     PIC  X(7).                       
                    15 MESBSF00C-DDEBSAIS      PIC  X(8).                       
                    15 MESBSF00C-DFINSAIS      PIC  X(8).                       
                    15 MESBSF00C-DDESTOCK      PIC  X(8).                       
                    15 MESBSF00C-DMUTATION     PIC  X(8).                       
                    15 MESBSF00C-CSELART       PIC  X(5).                       
                    15 MESBSF00C-DHEURMUT      PIC  X(2).                       
                    15 MESBSF00C-DMINUMUT      PIC  X(2).                       
                    15 MESBSF00C-QNBCAMIONS    PIC S9(3)      COMP-3.           
                    15 MESBSF00C-QNBM3QUOTA    PIC S9(5)V9(2) COMP-3.           
                    15 MESBSF00C-QNBPQUOTA     PIC S9(5)      COMP-3.           
                    15 MESBSF00C-QVOLUME       PIC S9(11)     COMP-3.           
                    15 MESBSF00C-QNBPIECES     PIC S9(5)      COMP-3.           
                    15 MESBSF00C-QNBLIGNES     PIC S9(5)      COMP-3.           
                    15 MESBSF00C-QNBPLANCE     PIC S9(5)      COMP-3.           
                    15 MESBSF00C-QNBLLANCE     PIC S9(5)      COMP-3.           
                    15 MESBSF00C-WPROPERMIS    PIC X(1).                        
                    15 MESBSF00C-WVAL          PIC X(1).                        
                    15 MESBSF00C-LHEURLIMIT    PIC X(10).                       
                    15 MESBSF00C-DVALID        PIC X(8).                        
                    15 MESBSF00C-QNBM3PROPOS   PIC S9(5)V9(2) COMP-3.           
                    15 MESBSF00C-QNBPPROPOS    PIC S9(5)      COMP-3.           
                    15 MESBSF00C-DCHARGT       PIC X(08).                       
                    15 MESBSF00C-HCHARGT       PIC X(05).                       
                    15 MESBSF00C-NLIGTRANS     PIC X(06).                       
                    15 MESBSF00C-NSEQUENCE     PIC X(02).                       
                    15 MESBSF00C-IDENTIFIANT.                                   
                       20 MESBSF00C-CORRELID   PIC X(24) OCCURS 50.             
                    15 MESBSF00C-TRANSFERT     PIC X.                           
                    15 MESBSF00C-FILLER        PIC X(99).                       
                 10 MESBSF00C-DATA-INUTILE     PIC X(28505).                    
      ******************************************************************        
      * MESSAGE DE DONNEES BSF00D1 CONCERNANT LE DETAIL DE MUT RTGB15  *        
      * MESSAGE DE DONNEES BSF00D2 CONCERNANT LES VENTES SUR MUT RTGD75*        
      * MESS DE DONNEES BSF00D3 CONCERNANT LES VENTES A COMMUTER RTWS15*        
      * MESSAGE DE DONNEES BSF00D4 CONCERNANT LES VENTES LDM RTWS35    *        
      ******************************************************************        
              05 MESBSF00D-DATA REDEFINES MESBSF00-DATA.                        
      * LONGUEUR DE 300 OCTETS *  99                                            
                 10 MESBSF00D-DATA-X           OCCURS  99.                      
                    15 MESBSF00D1-NMUTATION    PIC X(7).                        
                    15 MESBSF00D1-NCODIC       PIC X(7).                        
                    15 MESBSF00D1-WSEQFAM      PIC S9(5)  COMP-3.               
                    15 MESBSF00D1-CMARQ        PIC X(5).                        
                    15 MESBSF00D1-QDEMANDEE    PIC S9(5)  COMP-3.               
                    15 MESBSF00D1-QLIEE        PIC S9(5)  COMP-3.               
                    15 MESBSF00D1-QCDEPREL     PIC S9(5)  COMP-3.               
                    15 MESBSF00D1-QMUTEE       PIC S9(5)  COMP-3.               
                    15 MESBSF00D1-QPOIDS       PIC S9(7)  COMP-3.               
                    15 MESBSF00D1-QVOLUME      PIC S9(7)  COMP-3.               
                    15 MESBSF00D1-CMODSTOCK    PIC X(5).                        
                    15 MESBSF00D1-NSOCDEPOT    PIC X(3).                        
                    15 MESBSF00D1-NDEPOT       PIC X(3).                        
                    15 MESBSF00D1-DRAFALE      PIC X(8).                        
                    15 MESBSF00D1-NRAFALE      PIC X(3).                        
                    15 MESBSF00D1-NSATELLITE   PIC X(2).                        
                    15 MESBSF00D1-NCASE        PIC X(3).                        
                    15 MESBSF00D1-WGROUPE      PIC X(1).                        
                    15 MESBSF00D1-QDEMINIT     PIC S9(5)  COMP-3.               
                    15 MESBSF00D1-QCC          PIC S9(5)  COMP-3.               
                    15 MESBSF00D1-WORIGINE     PIC X(1).                        
                    15 MESBSF00D1-WGD75        PIC X(1).                        
                    15 MESBSF00D1-WWS15        PIC X(1).                        
                    15 MESBSF00D1-WWS35        PIC X(1).                        
                    15 MESBSF00D2-NSOCDEPOT    PIC X(3).                        
                    15 MESBSF00D2-NDEPOT       PIC X(3).                        
                    15 MESBSF00D2-DRAFALE      PIC X(8).                        
                    15 MESBSF00D2-NRAFALE      PIC X(3).                        
                    15 MESBSF00D2-NMUTATION    PIC X(7).                        
                    15 MESBSF00D2-NCODIC       PIC X(7).                        
                    15 MESBSF00D2-NLIEU        PIC X(3).                        
                    15 MESBSF00D2-NVENTE       PIC X(7).                        
                    15 MESBSF00D2-QVENDUE      PIC S9(5)  COMP-3.               
                    15 MESBSF00D2-QPCECOM      PIC S9(5)  COMP-3.               
                    15 MESBSF00D2-NSOCIETE     PIC X(3).                        
                    15 MESBSF00D2-NSOCDEPLIV   PIC X(3).                        
                    15 MESBSF00D2-NLIEUDEPLIV  PIC X(3).                        
                    15 MESBSF00D3-NSOCIETE     PIC X(3).                        
                    15 MESBSF00D3-NLIEU        PIC X(3).                        
                    15 MESBSF00D3-NVENTE       PIC X(7).                        
                    15 MESBSF00D3-NCODIC       PIC X(7).                        
                    15 MESBSF00D3-NSEQ         PIC X(02).                       
                    15 MESBSF00D3-LCOMMENT     PIC X(35).                       
                    15 MESBSF00D3-CARACTSPE1   PIC X(05).                       
                    15 MESBSF00D3-CVCARACTSPE1 PIC X(05).                       
                    15 MESBSF00D3-CARACTSPE2   PIC X(05).                       
                    15 MESBSF00D3-CVCARACTSPE2 PIC X(05).                       
                    15 MESBSF00D3-CARACTSPE3   PIC X(05).                       
                    15 MESBSF00D3-CVCARACTSPE3 PIC X(05).                       
                    15 MESBSF00D3-CARACTSPE4   PIC X(05).                       
                    15 MESBSF00D3-CVCARACTSPE4 PIC X(05).                       
                    15 MESBSF00D3-CARACTSPE5   PIC X(05).                       
                    15 MESBSF00D3-CVCARACTSPE5 PIC X(05).                       
                    15 MESBSF00D4-NSOCIETE     PIC X(3).                        
                    15 MESBSF00D4-NBONENLV     PIC X(7).                        
                    15 MESBSF00D4-NLIEUORIG    PIC X(3).                        
                    15 MESBSF00D4-NLIEUDEST    PIC X(3).                        
                    15 MESBSF00D4-NSSLIEUDEST  PIC X(3).                        
                    15 MESBSF00D4-LNOM         PIC X(25).                       
                    15 MESBSF00D-FILLER        PIC X(13).                       
                 10 MESBSF00D-DATA-INUTILE     PIC X(240).                      
                                                                                
