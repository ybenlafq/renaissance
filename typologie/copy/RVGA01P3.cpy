      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HSREG LIEUX POUR REGUL POST-INV        *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01P3.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01P3.                                                            
      *}                                                                        
           05  HSREG-CTABLEG2    PIC X(15).                                     
           05  HSREG-CTABLEG2-REDEF REDEFINES HSREG-CTABLEG2.                   
               10  HSREG-NSOC            PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  HSREG-NSOC-N         REDEFINES HSREG-NSOC                    
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  HSREG-WTABLEG     PIC X(80).                                     
           05  HSREG-WTABLEG-REDEF  REDEFINES HSREG-WTABLEG.                    
               10  HSREG-WACTIF          PIC X(01).                             
               10  HSREG-LORIG           PIC X(09).                             
               10  HSREG-LDEST           PIC X(09).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01P3-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01P3-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HSREG-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HSREG-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HSREG-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HSREG-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
