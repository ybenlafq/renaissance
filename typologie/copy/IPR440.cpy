      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPR440 AU 29/01/1998  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,21,BI,A,                          *        
      *                           34,05,BI,A,                          *        
      *                           39,05,BI,A,                          *        
      *                           44,01,BI,A,                          *        
      *                           45,05,BI,A,                          *        
      *                           50,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPR440.                                                        
            05 NOMETAT-IPR440           PIC X(6) VALUE 'IPR440'.                
            05 RUPTURES-IPR440.                                                 
           10 IPR440-NSOCIETE           PIC X(03).                      007  003
           10 IPR440-NLIEU              PIC X(03).                      010  003
           10 IPR440-VENDEUR            PIC X(21).                      013  021
           10 IPR440-CTYPPREST          PIC X(05).                      034  005
           10 IPR440-COPER              PIC X(05).                      039  005
           10 IPR440-WPRODMAJ           PIC X(01).                      044  001
           10 IPR440-CPREST             PIC X(05).                      045  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPR440-SEQUENCE           PIC S9(04) COMP.                050  002
      *--                                                                       
           10 IPR440-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPR440.                                                   
           10 IPR440-CVENDEUR           PIC X(06).                      052  006
           10 IPR440-LLIEU              PIC X(20).                      058  020
           10 IPR440-LPREST             PIC X(20).                      078  020
           10 IPR440-LVENDEUR           PIC X(15).                      098  015
           10 IPR440-TX-CONCRETISATION  PIC X(01).                      113  001
           10 IPR440-WMAJEUR            PIC X(01).                      114  001
           10 IPR440-WOPTIONS           PIC X(01).                      115  001
           10 IPR440-PPRIME             PIC S9(05)V9(2) COMP-3.         116  004
           10 IPR440-PVTOTAL            PIC S9(07)V9(2) COMP-3.         120  005
           10 IPR440-QVENDUES           PIC S9(06)      COMP-3.         125  004
           10 IPR440-DDEBUT             PIC X(08).                      129  008
           10 IPR440-DFIN               PIC X(08).                      137  008
            05 FILLER                      PIC X(368).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPR440-LONG           PIC S9(4)   COMP  VALUE +144.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPR440-LONG           PIC S9(4) COMP-5  VALUE +144.           
                                                                                
      *}                                                                        
