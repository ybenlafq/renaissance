      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************00000010
      * DCLGEN TABLE(PNMD.RVGA0199)                                    *        
      *        LIBRARY(DSA042.DEVL.SOURCE2(RVGA0199))                  *        
      *        ACTION(REPLACE)                                         *        
      *        LANGUAGE(COBOL)                                         *        
      *        NAMES(GA01-)                                            *        
      *        APOST                                                   *        
      *        COLSUFFIX(YES)                                          *        
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *        
      ******************************************************************        
           EXEC SQL DECLARE PNMD.RVGA0199 TABLE                                 
           ( NSOCIETE                       VARCHAR(3) NOT NULL,                
             CTABLEG1                       CHAR(5) NOT NULL,                   
             CTABLEG2                       CHAR(15) NOT NULL,                  
             WTABLEG                        CHAR(80) NOT NULL                   
           ) END-EXEC.                                                          
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVGA0199                      *00000020
      ******************************************************************00000030
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  DCLRVGA0199.                                                 00000040
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  DCLRVGA0199.                                                         
      *}                                                                        
           10 GA01-NSOCIETE    PIC X(03).                               00000050
      *                       CTABLEG1                                  00000060
           10 GA01-CTABLEG1        PIC X(5).                            00000070
      *                       CTABLEG2                                  00000080
           10 GA01-CTABLEG2        PIC X(15).                           00000090
      *                       WTABLEG                                   00000100
           10 GA01-WTABLEG         PIC X(80).                           00000110
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************00000120
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 4       *00000130
      ******************************************************************00000140
                                                                                
