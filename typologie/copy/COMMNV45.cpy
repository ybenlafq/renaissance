      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * ------------------------------------------------------------- *         
      * ZONE DE COMMUNICATION POUR MNV45                              *         
      *                                                               *         
      * ****ATTENTION: IL Y A UNE COPIE DE LA COMMNV45 SUR ECOM       *         
      * A METTRE � JOUR AUSSI EN CAS DE MODIF                         *         
      * ------------------------------------------------------------- *         
         01  Z-COMMAREA-MNV45.                                                  
            02  TAB-NV45-MAX                     PIC 9(02) VALUE 80.            
            02  IND-NV45                         PIC 9(02) VALUE 0.             
            02 NV45-DONNEES.                                                    
            05  NV45-NSOCIETE                    PIC X(03).                     
            05  NV45-NLIEU                       PIC X(03).                     
            05  NV45-NVENTE                      PIC X(07).                     
            05  NV45-CAGENT                    PIC X(02).                       
            05 TAB-VN45                          OCCURS 80.                     
               10  NV45-CODACT                    PIC X(01).                    
               10  NV45-NCODICGRP                 PIC X(07).                    
               10  NV45-NCODIC                    PIC X(07).                    
               10  NV45-CENREG                    PIC X(05).                    
               10  NV45-NSEQENS                   PIC S9(5) COMP-3.             
               10  NV45-DATENC                    PIC X(08).                    
               10  NV45-WTOPLIVRE                 PIC X(01).                    
               10  NV45-NSEQNQ                    PIC S9(5) COMP-3.             
               10  NV45-NLIEN                     PIC S9(5) COMP-3.             
               10  NV45-CTYPENT                   PIC X(02).                    
               10  NV45-COMMENTAIRE               PIC X(35).                    
            05  NV45-CARTEKDO.                                                  
                06 NV45-CARTE-KDO      OCCURS 20.                               
                  10 NV45-NOM         PIC X(020).                               
                  10 NV45-VALEUR      PIC X(020).                               
            05 NV45-CFIN                         PIC X(5).                      
            02 NV45-SORTIE.                                                     
            10 NV45-CODRET                       PIC X(02).                     
            10 NV45-LIBRET                       PIC X(80).                     
            10 NV45-ACTION-EXECUTEE              PIC 9(01).                     
               88 INSERT-GV05-FAIT                          VALUE 1.            
               88 INSERT-GV05-NON-FAIT                      VALUE 2.            
            10 NV45-VALCTRL                      PIC X(50).                     
            05 NV45-FILLER                       PIC X(2139).                   
                                                                                
