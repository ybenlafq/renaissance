      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
       01  Z-COMMAREA-MSE53.                                            00000020
           02 COMM-MSE53-ENTETE-RETOUR.                                 00000030
      *---- BOOLEEN TYPE D'ERREUR                                       00000040
              03 COMM-MSE53-TYPERR     PIC 9(01).                       00000050
                 88 MSE53-NO-ERROR       VALUE 0.                       00000060
                 88 MSE53-APPL-ERROR     VALUE 1.                       00000070
                 88 MSE53-DB2-ERROR      VALUE 2.                       00000080
              03 COMM-MSE53-FUNC-SQL   PIC X(08).                       00000090
              03 COMM-MSE53-TABLE-NAME PIC X(08).                       00000100
              03 COMM-MSE53-SQLCODE    PIC S9(04).                      00000110
              03 COMM-MSE53-POSITION   PIC  9(03).                      00000120
      *---- BOOLEEN CODE FONCTION                                       00000130
           02 COMM-MSE53-FONC      PIC 9(01).                           00000140
              88 MSE53-CTRL-DATA     VALUE 0.                           00000150
              88 MSE53-MAJ-DB2       VALUE 1.                           00000160
      *---- DONNEES EN ENTREE / SORTIE                                  00000170
           02 COMM-MSE53-ZINOUT.                                        00000180
              03 COMM-MSE53-CODESFONCTION   PIC X(12).                  00000190
              03 COMM-MSE53-WFONC           PIC X(03).                  00000200
              03 COMM-MSE53-NCSERV          PIC X(05).                  00000210
              03 COMM-MSE53-CTYPSERV-AV     PIC X(01).                  00000220
              03 COMM-MSE53-CTYPSERV-AP     PIC X(01).                  00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-MSE53-NBR-OCCURS      PIC S9(04)   COMP.          00000240
      *--                                                                       
              03 COMM-MSE53-NBR-OCCURS      PIC S9(04) COMP-5.                  
      *}                                                                        
              03 COMM-MSE53-TABLEAU.                                    00000250
                 05 COMM-MSE53-LIGNE       OCCURS 50.                   00000260
                    10 COMM-MSE53-MCDOSS    PIC X(05).                  00000270
                    10 COMM-MSE53-MLDOSS    PIC X(20).                  00000280
                    10 COMM-MSE53-DATDEB    PIC X(08).                  00000290
                    10 COMM-MSE53-DATFIN    PIC X(08).                  00000300
                    10 COMM-MSE53-MNSEQ-AV  PIC X(03).                  00000310
                    10 COMM-MSE53-MNSEQ     PIC X(03).                  00000320
                    10 COMM-MSE53-MSUPP     PIC X.                      00000330
      *------------ CODE RETOUR POSITIONNEMENT DES ERREURS              00000340
                    10 COMM-MSE53-CC-ERR    PIC X(04).                  00000350
                                                                        00000360
           02 COMM-MSE53-FILLER             PIC X(1447).                00000370
                                                                                
