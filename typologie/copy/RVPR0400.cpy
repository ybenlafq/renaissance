      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      **********************************************************                
      *   COPY DE LA TABLE RVPR0400                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVPR0400                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPR0400.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPR0400.                                                            
      *}                                                                        
           02  PR04-CPRESTATION                                                 
               PIC X(0005).                                                     
           02  PR04-CPRESTELT                                                   
               PIC X(0005).                                                     
           02  PR04-NORDRE                                                      
               PIC S9(3) COMP-3.                                                
           02  PR04-DCREATION                                                   
               PIC X(0008).                                                     
           02  PR04-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVPR0400                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPR0400-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPR0400-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR04-CPRESTATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR04-CPRESTATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR04-CPRESTELT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR04-CPRESTELT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR04-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR04-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR04-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR04-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR04-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR04-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
