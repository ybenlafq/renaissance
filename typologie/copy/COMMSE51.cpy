      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
       01  Z-COMMAREA-MSE51.                                            00000020
           02 COMM-MSE51-ENTETE-RETOUR.                                 00000030
      *---- BOOLEEN TYPE D'ERREUR                                       00000040
              03 COMM-MSE51-TYPERR     PIC 9(01).                       00000050
                 88 MSE51-NO-ERROR       VALUE 0.                       00000060
                 88 MSE51-APPL-ERROR     VALUE 1.                       00000070
                 88 MSE51-DB2-ERROR      VALUE 2.                       00000080
              03 COMM-MSE51-FUNC-SQL   PIC X(08).                       00000090
              03 COMM-MSE51-TABLE-NAME PIC X(08).                       00000100
              03 COMM-MSE51-SQLCODE    PIC S9(04).                      00000110
              03 COMM-MSE51-POSITION   PIC  9(03).                      00000120
      *---- BOOLEEN CODE FONCTION                                       00000130
           02 COMM-MSE51-FONC      PIC 9(01).                           00000140
              88 MSE51-CTRL-DATA     VALUE 0.                           00000150
              88 MSE51-CONT50        VALUE 1.                           00000160
              88 MSE51-CONT51        VALUE 2.                           00000170
              88 MSE51-MAJ-DB2       VALUE 3.                           00000180
      *---- DONNEES EN ENTREE / SORTIE                                  00000190
           02 COMM-MSE51-ZINOUT.                                        00000200
              03 COMM-MSE51-CODESFONCTION   PIC X(12).                  00000210
              03 COMM-MSE51-WFONC           PIC X(03).                  00000220
              03 COMM-MSE51-NCSERV          PIC X(05).                  00000230
              03 COMM-MSE51-LCSERV          PIC X(20).                  00000240
              03 COMM-MSE51-LCSERVC         PIC X(05).                  00000250
              03 COMM-MSE51-NORDRE          PIC X(03).                  00000260
              03 COMM-MSE51-COPER           PIC X(05).                  00000270
              03 COMM-MSE51-LOPER           PIC X(20).                  00000280
              03 COMM-MSE51-CFAM            PIC X(05).                  00000290
              03 COMM-MSE51-LCFAM           PIC X(20).                  00000300
              03 COMM-MSE51-CASSORTC        PIC X(05).                  00000310
              03 COMM-MSE51-LCASSORTC       PIC X(20).                  00000320
              03 COMM-MSE51-CASSORTF        PIC X(05).                  00000330
              03 COMM-MSE51-LCASSORTF       PIC X(20).                  00000340
              03 COMM-MSE51-DATEF           PIC X(20).                  00000350
              03 COMM-MSE51-CTVA            PIC X(01).                  00000360
              03 COMM-MSE51-NTVA            PIC S999V99.                00000370
              03 COMM-MSE51-CTVALIB         PIC X(20).                  00000380
              03 COMM-MSE51-CHEF            PIC X(05).                  00000390
              03 COMM-MSE51-LCHEF           PIC X(20).                  00000400
FV3  ****     03 COMM-MSE51-EAN             PIC X(13).                  00000410
FV-3.2        03 FILLER                     PIC X(26).                  00000420
FV-3.2*       03 COMM-MSE51-EAN-AV          PIC X(13).                  00000430
FV-3.2*       03 COMM-MSE51-EAN-AP          PIC X(13).                  00000440
FVM           03 COMM-MSE51-CTYPINN         PIC X(1).                   00000450
BF1   *       VALEUR JUSTE AVANT MODIF (AVANT ENTREE OU PFX DANS TSE51) 00000460
BF1           03 COMM-MSE51-CTYPINN-JAV     PIC X(1).                   00000470
BF1   *       VALEUR AVANT MODIF GLOBALE (EN ENTREE SE50)               00000480
BF1           03 COMM-MSE51-CTYPINN-AV      PIC X(1).                   00000490
FVM           03 COMM-MSE51-CTYPSERV-SV     PIC X(1).                   00000500
              03 COMM-MSE51-CTYPSERV-AV     PIC X(01).                  00000510
              03 COMM-MSE51-CTYPSERV-AP     PIC X(01).                  00000520
              03 COMM-MSE51-STATVTE         PIC X(01).                  00000530
              03 COMM-MSE51-CSIGNE          PIC X(01).                  00000540
GA0103*       03 COMM-MSE51-WNATPREST       PIC X(01).                  00000550
GA0103        03 COMM-MSE51-CTYP            PIC X(05).                  00000560
GA0103        03 COMM-MSE51-LTYP            PIC X(20).                  00000570
              03 COMM-MSE51-DMAJ            PIC X(08).                  00000580
FV1           03 COMM-MSE51-CGEST           PIC X(5)  OCCURS 30.        00000590
FV1           03 COMM-MSE51-FGEST           PIC X     OCCURS 30.        00000600
FV1           03 COMM-MSE51-VGEST-AV        PIC X(20) OCCURS 30.        00000610
FV1           03 COMM-MSE51-VGEST-AP        PIC X(20) OCCURS 30.        00000620
FV1           03 COMM-MSE51-TYPE            PIC X OCCURS 30.            00000630
FV1           03 COMM-MSE51-TABLE           PIC X(6) OCCURS 30.         00000640
FV1           03 COMM-MSE51-VISIBLE         PIC X OCCURS 30.            00000650
FV1           03 COMM-MSE51-OBLIG           PIC X OCCURS 30.            00000660
FV1           03 COMM-MSE51-SAISIE          PIC X(5) OCCURS 30.         00000670
FV1           03 COMM-MSE51-VALDFAUT        PIC X(20) OCCURS 30.        00000680
FV1   *       03 COMM-MSE51-CGEST           PIC X(5) OCCURS 20.         00000690
FV1   *       03 COMM-MSE51-VGEST           PIC X OCCURS 20.            00000700
      *---- BOOLEENS POSITIONNEMENT ERREURS                             00000710
           02 COMM-MSE51-ERROR1    PIC 9(01).                           00000720
              88 COPER-OK            VALUE 0.                           00000730
              88 COPER-KO            VALUE 1.                           00000740
           02 COMM-MSE51-CDRET1    PIC X(04).                           00000750
           02 COMM-MSE51-ERROR2    PIC 9(01).                           00000760
              88 CFAM-OK             VALUE 0.                           00000770
              88 CFAM-KO             VALUE 1.                           00000780
           02 COMM-MSE51-CDRET2    PIC X(04).                           00000790
           02 COMM-MSE51-ERROR3    PIC 9(01).                           00000800
              88 TVA-OK              VALUE 0.                           00000810
              88 TVA-KO              VALUE 1.                           00000820
           02 COMM-MSE51-CDRET3    PIC X(04).                           00000830
           02 COMM-MSE51-ERROR4    PIC 9(01).                           00000840
              88 CASSORT-OK          VALUE 0.                           00000850
              88 CASSORT-KO          VALUE 1.                           00000860
           02 COMM-MSE51-CDRET4    PIC X(04).                           00000870
           02 COMM-MSE51-ERROR5    PIC 9(01).                           00000880
              88 CTYP-OK             VALUE 0.                           00000890
              88 CTYP-KO             VALUE 1.                           00000900
           02 COMM-MSE51-CDRET5    PIC X(04).                           00000910
           02 COMM-MSE51-ERROR6    PIC 9(01).                           00000920
              88 CHEF-OK             VALUE 0.                           00000930
              88 CHEF-KO             VALUE 1.                           00000940
           02 COMM-MSE51-CDRET6    PIC X(04).                           00000950
           02 COMM-MSE51-ERROR7    PIC 9(01).                           00000960
              88 CTYPSERV-OK         VALUE 0.                           00000970
              88 CTYPSERV-KO         VALUE 1.                           00000980
           02 COMM-MSE51-CDRET7    PIC X(04).                           00000990
AD1        02 COMM-MSE51-ERROR8    PIC 9(01).                           00001000
              88 EAN-OK              VALUE 0.                           00001010
              88 EAN-KO              VALUE 1.                           00001020
           02 COMM-MSE51-CDRET8    PIC X(04).                           00001030
FVM        02 COMM-MSE51-ERROR9    PIC 9(01).                           00001040
              88 CTYPINN-OK          VALUE 0.                           00001050
              88 CTYPINN-KO          VALUE 1.                           00001060
           02 COMM-MSE51-CDRET9    PIC X(04).                           00001070
FVM5       02 COMM-MSE51-ERROR10   PIC 9(01).                           00001080
FVM5          88 CCONTRA-OK          VALUE 0.                           00001090
FVM5          88 CCONTRA-KO          VALUE 1.                           00001100
FVM5       02 COMM-MSE51-CDRET10   PIC X(04).                           00001110
                                                                        00001120
                                                                        00001130
AD1   *    02 COMM-MSE51-FILLER    PIC X(3660).                         00001140
AD1   *    02 COMM-MSE51-FILLER    PIC X(3655).                         00001150
FVM   *    02 COMM-MSE51-FILLER    PIC X(3647).                         00001160
FV1   *    02 COMM-MSE51-FILLER    PIC X(1351).                         00001170
FV3   *    02 COMM-MSE51-FILLER    PIC X(1338).                         00001180
BF1        02 COMM-MSE51-FILLER    PIC X(1336).                         00001190
                                                                                
