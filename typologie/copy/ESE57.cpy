      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: donnees redbox prestation                                  00000020
      ***************************************************************** 00000030
       01   ESE57I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * FONCTION                                                        00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MWFONCI   PIC X(3).                                       00000200
      * CODE SERVICE                                                    00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCSERVL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCSERVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCSERVF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCSERVI  PIC X(5).                                       00000250
      * LIBELLE LONG CODE SERVICE                                       00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCSERVL  COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MLCSERVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCSERVF  PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MLCSERVI  PIC X(20).                                      00000300
      * LIBELLE SERVICE                                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSERVICEL      COMP PIC S9(4).                            00000320
      *--                                                                       
           02 MSERVICEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSERVICEF      PIC X.                                     00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MSERVICEI      PIC X(20).                                 00000350
      * CODE OPERATEUR                                                  00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPERL   COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MCOPERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOPERF   PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MCOPERI   PIC X(5).                                       00000400
      * LIBELLE CODE OPERATEUR                                          00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLOPERL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLOPERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLOPERF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLOPERI   PIC X(20).                                      00000450
      * LIBELLE CODE FACTURATI                                          00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODEFACL      COMP PIC S9(4).                            00000470
      *--                                                                       
           02 MCODEFACL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCODEFACF      PIC X.                                     00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MCODEFACI      PIC X(20).                                 00000500
      * CODE FAMILLE                                                    00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MCFAMI    PIC X(5).                                       00000550
      * LIBELLE CODE FAMILLE                                            00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MLFAMI    PIC X(20).                                      00000600
      * LIBELLE ENTREE DIVERSE                                          00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENTRDIVL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MENTRDIVL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MENTRDIVF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MENTRDIVI      PIC X(20).                                 00000650
      * QUANTITE MULTIPLE AUTO                                          00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLEL      COMP PIC S9(4).                            00000670
      *--                                                                       
           02 MLIBELLEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBELLEF      PIC X.                                     00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLIBELLEI      PIC X(50).                                 00000700
      * REMISE AUTORISEE                                                00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MAFFICHL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MAFFICHL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MAFFICHF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MAFFICHI  PIC X(5).                                       00000750
      * A IMPRIMER TICKET                                               00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSEQFAML  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MSEQFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSEQFAMF  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MSEQFAMI  PIC X(2).                                       00000800
      * RESTRICTION GEOGRAPHIQ                                          00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRDVL     COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MRDVL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MRDVF     PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MRDVI     PIC X(3).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIENL    COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MLIENL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIENF    PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MLIENI    PIC X(7).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIENL   COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MLLIENL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIENF   PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MLLIENI   PIC X(20).                                      00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTEL     COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MQTEL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MQTEF     PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MQTEI     PIC X(3).                                       00000970
      * CLIENT REQUIS                                                   00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MINFO1L   COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MINFO1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MINFO1F   PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MINFO1I   PIC X(39).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MINFO2L   COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MINFO2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MINFO2F   PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MINFO2I   PIC X(72).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MINFO3L   COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MINFO3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MINFO3F   PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MINFO3I   PIC X(72).                                      00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MINFO4L   COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MINFO4L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MINFO4F   PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MINFO4I   PIC X(72).                                      00001140
      * ZONE CMD AIDA                                                   00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001160
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001170
           02 FILLER    PIC X(4).                                       00001180
           02 MZONCMDI  PIC X(15).                                      00001190
      * MESSAGE ERREUR                                                  00001200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001210
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001220
           02 FILLER    PIC X(4).                                       00001230
           02 MLIBERRI  PIC X(58).                                      00001240
      * CODE TRANSACTION                                                00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MCODTRAI  PIC X(4).                                       00001290
      * CICS DE TRAVAIL                                                 00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MCICSI    PIC X(5).                                       00001340
      * NETNAME                                                         00001350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001360
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001370
           02 FILLER    PIC X(4).                                       00001380
           02 MNETNAMI  PIC X(8).                                       00001390
      * CODE TERMINAL                                                   00001400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001410
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001420
           02 FILLER    PIC X(4).                                       00001430
           02 MSCREENI  PIC X(5).                                       00001440
      ***************************************************************** 00001450
      * SDF: donnees redbox prestation                                  00001460
      ***************************************************************** 00001470
       01   ESE57O REDEFINES ESE57I.                                    00001480
           02 FILLER    PIC X(12).                                      00001490
      * DATE DU JOUR                                                    00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MDATJOUA  PIC X.                                          00001520
           02 MDATJOUC  PIC X.                                          00001530
           02 MDATJOUP  PIC X.                                          00001540
           02 MDATJOUH  PIC X.                                          00001550
           02 MDATJOUV  PIC X.                                          00001560
           02 MDATJOUO  PIC X(10).                                      00001570
      * HEURE                                                           00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MTIMJOUA  PIC X.                                          00001600
           02 MTIMJOUC  PIC X.                                          00001610
           02 MTIMJOUP  PIC X.                                          00001620
           02 MTIMJOUH  PIC X.                                          00001630
           02 MTIMJOUV  PIC X.                                          00001640
           02 MTIMJOUO  PIC X(5).                                       00001650
      * FONCTION                                                        00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MWFONCA   PIC X.                                          00001680
           02 MWFONCC   PIC X.                                          00001690
           02 MWFONCP   PIC X.                                          00001700
           02 MWFONCH   PIC X.                                          00001710
           02 MWFONCV   PIC X.                                          00001720
           02 MWFONCO   PIC X(3).                                       00001730
      * CODE SERVICE                                                    00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MNCSERVA  PIC X.                                          00001760
           02 MNCSERVC  PIC X.                                          00001770
           02 MNCSERVP  PIC X.                                          00001780
           02 MNCSERVH  PIC X.                                          00001790
           02 MNCSERVV  PIC X.                                          00001800
           02 MNCSERVO  PIC X(5).                                       00001810
      * LIBELLE LONG CODE SERVICE                                       00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MLCSERVA  PIC X.                                          00001840
           02 MLCSERVC  PIC X.                                          00001850
           02 MLCSERVP  PIC X.                                          00001860
           02 MLCSERVH  PIC X.                                          00001870
           02 MLCSERVV  PIC X.                                          00001880
           02 MLCSERVO  PIC X(20).                                      00001890
      * LIBELLE SERVICE                                                 00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MSERVICEA      PIC X.                                     00001920
           02 MSERVICEC PIC X.                                          00001930
           02 MSERVICEP PIC X.                                          00001940
           02 MSERVICEH PIC X.                                          00001950
           02 MSERVICEV PIC X.                                          00001960
           02 MSERVICEO      PIC X(20).                                 00001970
      * CODE OPERATEUR                                                  00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MCOPERA   PIC X.                                          00002000
           02 MCOPERC   PIC X.                                          00002010
           02 MCOPERP   PIC X.                                          00002020
           02 MCOPERH   PIC X.                                          00002030
           02 MCOPERV   PIC X.                                          00002040
           02 MCOPERO   PIC X(5).                                       00002050
      * LIBELLE CODE OPERATEUR                                          00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MLOPERA   PIC X.                                          00002080
           02 MLOPERC   PIC X.                                          00002090
           02 MLOPERP   PIC X.                                          00002100
           02 MLOPERH   PIC X.                                          00002110
           02 MLOPERV   PIC X.                                          00002120
           02 MLOPERO   PIC X(20).                                      00002130
      * LIBELLE CODE FACTURATI                                          00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MCODEFACA      PIC X.                                     00002160
           02 MCODEFACC PIC X.                                          00002170
           02 MCODEFACP PIC X.                                          00002180
           02 MCODEFACH PIC X.                                          00002190
           02 MCODEFACV PIC X.                                          00002200
           02 MCODEFACO      PIC X(20).                                 00002210
      * CODE FAMILLE                                                    00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MCFAMA    PIC X.                                          00002240
           02 MCFAMC    PIC X.                                          00002250
           02 MCFAMP    PIC X.                                          00002260
           02 MCFAMH    PIC X.                                          00002270
           02 MCFAMV    PIC X.                                          00002280
           02 MCFAMO    PIC X(5).                                       00002290
      * LIBELLE CODE FAMILLE                                            00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLFAMA    PIC X.                                          00002320
           02 MLFAMC    PIC X.                                          00002330
           02 MLFAMP    PIC X.                                          00002340
           02 MLFAMH    PIC X.                                          00002350
           02 MLFAMV    PIC X.                                          00002360
           02 MLFAMO    PIC X(20).                                      00002370
      * LIBELLE ENTREE DIVERSE                                          00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MENTRDIVA      PIC X.                                     00002400
           02 MENTRDIVC PIC X.                                          00002410
           02 MENTRDIVP PIC X.                                          00002420
           02 MENTRDIVH PIC X.                                          00002430
           02 MENTRDIVV PIC X.                                          00002440
           02 MENTRDIVO      PIC X(20).                                 00002450
      * QUANTITE MULTIPLE AUTO                                          00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MLIBELLEA      PIC X.                                     00002480
           02 MLIBELLEC PIC X.                                          00002490
           02 MLIBELLEP PIC X.                                          00002500
           02 MLIBELLEH PIC X.                                          00002510
           02 MLIBELLEV PIC X.                                          00002520
           02 MLIBELLEO      PIC X(50).                                 00002530
      * REMISE AUTORISEE                                                00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MAFFICHA  PIC X.                                          00002560
           02 MAFFICHC  PIC X.                                          00002570
           02 MAFFICHP  PIC X.                                          00002580
           02 MAFFICHH  PIC X.                                          00002590
           02 MAFFICHV  PIC X.                                          00002600
           02 MAFFICHO  PIC X(5).                                       00002610
      * A IMPRIMER TICKET                                               00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MSEQFAMA  PIC X.                                          00002640
           02 MSEQFAMC  PIC X.                                          00002650
           02 MSEQFAMP  PIC X.                                          00002660
           02 MSEQFAMH  PIC X.                                          00002670
           02 MSEQFAMV  PIC X.                                          00002680
           02 MSEQFAMO  PIC X(2).                                       00002690
      * RESTRICTION GEOGRAPHIQ                                          00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MRDVA     PIC X.                                          00002720
           02 MRDVC     PIC X.                                          00002730
           02 MRDVP     PIC X.                                          00002740
           02 MRDVH     PIC X.                                          00002750
           02 MRDVV     PIC X.                                          00002760
           02 MRDVO     PIC X(3).                                       00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MLIENA    PIC X.                                          00002790
           02 MLIENC    PIC X.                                          00002800
           02 MLIENP    PIC X.                                          00002810
           02 MLIENH    PIC X.                                          00002820
           02 MLIENV    PIC X.                                          00002830
           02 MLIENO    PIC X(7).                                       00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MLLIENA   PIC X.                                          00002860
           02 MLLIENC   PIC X.                                          00002870
           02 MLLIENP   PIC X.                                          00002880
           02 MLLIENH   PIC X.                                          00002890
           02 MLLIENV   PIC X.                                          00002900
           02 MLLIENO   PIC X(20).                                      00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MQTEA     PIC X.                                          00002930
           02 MQTEC     PIC X.                                          00002940
           02 MQTEP     PIC X.                                          00002950
           02 MQTEH     PIC X.                                          00002960
           02 MQTEV     PIC X.                                          00002970
           02 MQTEO     PIC X(3).                                       00002980
      * CLIENT REQUIS                                                   00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MINFO1A   PIC X.                                          00003010
           02 MINFO1C   PIC X.                                          00003020
           02 MINFO1P   PIC X.                                          00003030
           02 MINFO1H   PIC X.                                          00003040
           02 MINFO1V   PIC X.                                          00003050
           02 MINFO1O   PIC X(39).                                      00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MINFO2A   PIC X.                                          00003080
           02 MINFO2C   PIC X.                                          00003090
           02 MINFO2P   PIC X.                                          00003100
           02 MINFO2H   PIC X.                                          00003110
           02 MINFO2V   PIC X.                                          00003120
           02 MINFO2O   PIC X(72).                                      00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MINFO3A   PIC X.                                          00003150
           02 MINFO3C   PIC X.                                          00003160
           02 MINFO3P   PIC X.                                          00003170
           02 MINFO3H   PIC X.                                          00003180
           02 MINFO3V   PIC X.                                          00003190
           02 MINFO3O   PIC X(72).                                      00003200
           02 FILLER    PIC X(2).                                       00003210
           02 MINFO4A   PIC X.                                          00003220
           02 MINFO4C   PIC X.                                          00003230
           02 MINFO4P   PIC X.                                          00003240
           02 MINFO4H   PIC X.                                          00003250
           02 MINFO4V   PIC X.                                          00003260
           02 MINFO4O   PIC X(72).                                      00003270
      * ZONE CMD AIDA                                                   00003280
           02 FILLER    PIC X(2).                                       00003290
           02 MZONCMDA  PIC X.                                          00003300
           02 MZONCMDC  PIC X.                                          00003310
           02 MZONCMDP  PIC X.                                          00003320
           02 MZONCMDH  PIC X.                                          00003330
           02 MZONCMDV  PIC X.                                          00003340
           02 MZONCMDO  PIC X(15).                                      00003350
      * MESSAGE ERREUR                                                  00003360
           02 FILLER    PIC X(2).                                       00003370
           02 MLIBERRA  PIC X.                                          00003380
           02 MLIBERRC  PIC X.                                          00003390
           02 MLIBERRP  PIC X.                                          00003400
           02 MLIBERRH  PIC X.                                          00003410
           02 MLIBERRV  PIC X.                                          00003420
           02 MLIBERRO  PIC X(58).                                      00003430
      * CODE TRANSACTION                                                00003440
           02 FILLER    PIC X(2).                                       00003450
           02 MCODTRAA  PIC X.                                          00003460
           02 MCODTRAC  PIC X.                                          00003470
           02 MCODTRAP  PIC X.                                          00003480
           02 MCODTRAH  PIC X.                                          00003490
           02 MCODTRAV  PIC X.                                          00003500
           02 MCODTRAO  PIC X(4).                                       00003510
      * CICS DE TRAVAIL                                                 00003520
           02 FILLER    PIC X(2).                                       00003530
           02 MCICSA    PIC X.                                          00003540
           02 MCICSC    PIC X.                                          00003550
           02 MCICSP    PIC X.                                          00003560
           02 MCICSH    PIC X.                                          00003570
           02 MCICSV    PIC X.                                          00003580
           02 MCICSO    PIC X(5).                                       00003590
      * NETNAME                                                         00003600
           02 FILLER    PIC X(2).                                       00003610
           02 MNETNAMA  PIC X.                                          00003620
           02 MNETNAMC  PIC X.                                          00003630
           02 MNETNAMP  PIC X.                                          00003640
           02 MNETNAMH  PIC X.                                          00003650
           02 MNETNAMV  PIC X.                                          00003660
           02 MNETNAMO  PIC X(8).                                       00003670
      * CODE TERMINAL                                                   00003680
           02 FILLER    PIC X(2).                                       00003690
           02 MSCREENA  PIC X.                                          00003700
           02 MSCREENC  PIC X.                                          00003710
           02 MSCREENP  PIC X.                                          00003720
           02 MSCREENH  PIC X.                                          00003730
           02 MSCREENV  PIC X.                                          00003740
           02 MSCREENO  PIC X(5).                                       00003750
                                                                                
