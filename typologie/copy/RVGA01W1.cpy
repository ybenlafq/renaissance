      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE AV005 LIBELLE DE SECTION/SERVICE       *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
       01  RVGA01W1.                                                            
           05  AV005-CTABLEG2    PIC X(15).                                     
           05  AV005-CTABLEG2-REDEF REDEFINES AV005-CTABLEG2.                   
               10  AV005-NSOCIETE        PIC X(03).                             
               10  AV005-NLIEU           PIC X(03).                             
               10  AV005-NSECTION        PIC X(06).                             
           05  AV005-WTABLEG     PIC X(80).                                     
           05  AV005-WTABLEG-REDEF  REDEFINES AV005-WTABLEG.                    
               10  AV005-LIBELLE         PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01W1-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AV005-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  AV005-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AV005-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  AV005-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.
                                                                                
      *}                                                                        
