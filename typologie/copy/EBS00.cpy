      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESE00   ESE00                                              00000020
      ***************************************************************** 00000030
       01   EBS00I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MWFONCI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCOFFRE1L     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MNCOFFRE1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNCOFFRE1F     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCOFFRE1I     PIC X(7).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAM1L   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAM1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAM1F   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAM1I   PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAM1L   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLFAM1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLFAM1F   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLFAM1I   PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCASSORTC1L    COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MCASSORTC1L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCASSORTC1F    PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCASSORTC1I    PIC X(5).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLASSORTC1L    COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MLASSORTC1L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLASSORTC1F    PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLASSORTC1I    PIC X(20).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQ1L  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCMARQ1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMARQ1F  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCMARQ1I  PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQ1L  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLMARQ1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLMARQ1F  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLMARQ1I  PIC X(20).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENTITE1L      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MENTITE1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MENTITE1F      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MENTITE1I      PIC X(7).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPENT1L     COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MCTYPENT1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTYPENT1F     PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCTYPENT1I     PIC X(2).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPROFASS1L    COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MCPROFASS1L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCPROFASS1F    PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCPROFASS1I    PIC X(5).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPROFASS1L    COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MLPROFASS1L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLPROFASS1F    PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLPROFASS1I    PIC X(20).                                 00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAMENT1L     COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MLFAMENT1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLFAMENT1F     PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLFAMENT1I     PIC X(5).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQENT1L    COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MLMARQENT1L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLMARQENT1F    PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLMARQENT1I    PIC X(5).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBENT1L      COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MLIBENT1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBENT1F      PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLIBENT1I      PIC X(20).                                 00000810
           02 MLIGNEI OCCURS   10 TIMES .                               00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MCFAMI  PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MCMARQI      PIC X(5).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSEGMENTL    COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MSEGMENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSEGMENTF    PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MSEGMENTI    PIC X(20).                                 00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCOFFREL    COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MNCOFFREL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNCOFFREF    PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MNCOFFREI    PIC X(7).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOFFREL    COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MLCOFFREL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLCOFFREF    PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MLCOFFREI    PIC X(20).                                 00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCASSORTCL   COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MCASSORTCL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCASSORTCF   PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MCASSORTCI   PIC X(5).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTL   COMP PIC S9(4).                                 00001070
      *--                                                                       
             03 MACTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MACTF   PIC X.                                          00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MACTI   PIC X(3).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MZONCMDI  PIC X(15).                                      00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MLIBERRI  PIC X(58).                                      00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MCODTRAI  PIC X(4).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MCICSI    PIC X(5).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MNETNAMI  PIC X(8).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MSCREENI  PIC X(4).                                       00001340
      ***************************************************************** 00001350
      * SDF: ESE00   ESE00                                              00001360
      ***************************************************************** 00001370
       01   EBS00O REDEFINES EBS00I.                                    00001380
           02 FILLER    PIC X(12).                                      00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MDATJOUA  PIC X.                                          00001410
           02 MDATJOUC  PIC X.                                          00001420
           02 MDATJOUP  PIC X.                                          00001430
           02 MDATJOUH  PIC X.                                          00001440
           02 MDATJOUV  PIC X.                                          00001450
           02 MDATJOUO  PIC X(10).                                      00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MTIMJOUA  PIC X.                                          00001480
           02 MTIMJOUC  PIC X.                                          00001490
           02 MTIMJOUP  PIC X.                                          00001500
           02 MTIMJOUH  PIC X.                                          00001510
           02 MTIMJOUV  PIC X.                                          00001520
           02 MTIMJOUO  PIC X(5).                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MPAGEA    PIC X.                                          00001550
           02 MPAGEC    PIC X.                                          00001560
           02 MPAGEP    PIC X.                                          00001570
           02 MPAGEH    PIC X.                                          00001580
           02 MPAGEV    PIC X.                                          00001590
           02 MPAGEO    PIC X(3).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MPAGEMAXA      PIC X.                                     00001620
           02 MPAGEMAXC PIC X.                                          00001630
           02 MPAGEMAXP PIC X.                                          00001640
           02 MPAGEMAXH PIC X.                                          00001650
           02 MPAGEMAXV PIC X.                                          00001660
           02 MPAGEMAXO      PIC X(3).                                  00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MWFONCA   PIC X.                                          00001690
           02 MWFONCC   PIC X.                                          00001700
           02 MWFONCP   PIC X.                                          00001710
           02 MWFONCH   PIC X.                                          00001720
           02 MWFONCV   PIC X.                                          00001730
           02 MWFONCO   PIC X(3).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MNCOFFRE1A     PIC X.                                     00001760
           02 MNCOFFRE1C     PIC X.                                     00001770
           02 MNCOFFRE1P     PIC X.                                     00001780
           02 MNCOFFRE1H     PIC X.                                     00001790
           02 MNCOFFRE1V     PIC X.                                     00001800
           02 MNCOFFRE1O     PIC X(7).                                  00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MCFAM1A   PIC X.                                          00001830
           02 MCFAM1C   PIC X.                                          00001840
           02 MCFAM1P   PIC X.                                          00001850
           02 MCFAM1H   PIC X.                                          00001860
           02 MCFAM1V   PIC X.                                          00001870
           02 MCFAM1O   PIC X(5).                                       00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MLFAM1A   PIC X.                                          00001900
           02 MLFAM1C   PIC X.                                          00001910
           02 MLFAM1P   PIC X.                                          00001920
           02 MLFAM1H   PIC X.                                          00001930
           02 MLFAM1V   PIC X.                                          00001940
           02 MLFAM1O   PIC X(20).                                      00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MCASSORTC1A    PIC X.                                     00001970
           02 MCASSORTC1C    PIC X.                                     00001980
           02 MCASSORTC1P    PIC X.                                     00001990
           02 MCASSORTC1H    PIC X.                                     00002000
           02 MCASSORTC1V    PIC X.                                     00002010
           02 MCASSORTC1O    PIC X(5).                                  00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MLASSORTC1A    PIC X.                                     00002040
           02 MLASSORTC1C    PIC X.                                     00002050
           02 MLASSORTC1P    PIC X.                                     00002060
           02 MLASSORTC1H    PIC X.                                     00002070
           02 MLASSORTC1V    PIC X.                                     00002080
           02 MLASSORTC1O    PIC X(20).                                 00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MCMARQ1A  PIC X.                                          00002110
           02 MCMARQ1C  PIC X.                                          00002120
           02 MCMARQ1P  PIC X.                                          00002130
           02 MCMARQ1H  PIC X.                                          00002140
           02 MCMARQ1V  PIC X.                                          00002150
           02 MCMARQ1O  PIC X(5).                                       00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MLMARQ1A  PIC X.                                          00002180
           02 MLMARQ1C  PIC X.                                          00002190
           02 MLMARQ1P  PIC X.                                          00002200
           02 MLMARQ1H  PIC X.                                          00002210
           02 MLMARQ1V  PIC X.                                          00002220
           02 MLMARQ1O  PIC X(20).                                      00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MENTITE1A      PIC X.                                     00002250
           02 MENTITE1C PIC X.                                          00002260
           02 MENTITE1P PIC X.                                          00002270
           02 MENTITE1H PIC X.                                          00002280
           02 MENTITE1V PIC X.                                          00002290
           02 MENTITE1O      PIC X(7).                                  00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MCTYPENT1A     PIC X.                                     00002320
           02 MCTYPENT1C     PIC X.                                     00002330
           02 MCTYPENT1P     PIC X.                                     00002340
           02 MCTYPENT1H     PIC X.                                     00002350
           02 MCTYPENT1V     PIC X.                                     00002360
           02 MCTYPENT1O     PIC X(2).                                  00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MCPROFASS1A    PIC X.                                     00002390
           02 MCPROFASS1C    PIC X.                                     00002400
           02 MCPROFASS1P    PIC X.                                     00002410
           02 MCPROFASS1H    PIC X.                                     00002420
           02 MCPROFASS1V    PIC X.                                     00002430
           02 MCPROFASS1O    PIC X(5).                                  00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MLPROFASS1A    PIC X.                                     00002460
           02 MLPROFASS1C    PIC X.                                     00002470
           02 MLPROFASS1P    PIC X.                                     00002480
           02 MLPROFASS1H    PIC X.                                     00002490
           02 MLPROFASS1V    PIC X.                                     00002500
           02 MLPROFASS1O    PIC X(20).                                 00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MLFAMENT1A     PIC X.                                     00002530
           02 MLFAMENT1C     PIC X.                                     00002540
           02 MLFAMENT1P     PIC X.                                     00002550
           02 MLFAMENT1H     PIC X.                                     00002560
           02 MLFAMENT1V     PIC X.                                     00002570
           02 MLFAMENT1O     PIC X(5).                                  00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MLMARQENT1A    PIC X.                                     00002600
           02 MLMARQENT1C    PIC X.                                     00002610
           02 MLMARQENT1P    PIC X.                                     00002620
           02 MLMARQENT1H    PIC X.                                     00002630
           02 MLMARQENT1V    PIC X.                                     00002640
           02 MLMARQENT1O    PIC X(5).                                  00002650
           02 FILLER    PIC X(2).                                       00002660
           02 MLIBENT1A      PIC X.                                     00002670
           02 MLIBENT1C PIC X.                                          00002680
           02 MLIBENT1P PIC X.                                          00002690
           02 MLIBENT1H PIC X.                                          00002700
           02 MLIBENT1V PIC X.                                          00002710
           02 MLIBENT1O      PIC X(20).                                 00002720
           02 MLIGNEO OCCURS   10 TIMES .                               00002730
             03 FILLER       PIC X(2).                                  00002740
             03 MCFAMA  PIC X.                                          00002750
             03 MCFAMC  PIC X.                                          00002760
             03 MCFAMP  PIC X.                                          00002770
             03 MCFAMH  PIC X.                                          00002780
             03 MCFAMV  PIC X.                                          00002790
             03 MCFAMO  PIC X(5).                                       00002800
             03 FILLER       PIC X(2).                                  00002810
             03 MCMARQA      PIC X.                                     00002820
             03 MCMARQC PIC X.                                          00002830
             03 MCMARQP PIC X.                                          00002840
             03 MCMARQH PIC X.                                          00002850
             03 MCMARQV PIC X.                                          00002860
             03 MCMARQO      PIC X(5).                                  00002870
             03 FILLER       PIC X(2).                                  00002880
             03 MSEGMENTA    PIC X.                                     00002890
             03 MSEGMENTC    PIC X.                                     00002900
             03 MSEGMENTP    PIC X.                                     00002910
             03 MSEGMENTH    PIC X.                                     00002920
             03 MSEGMENTV    PIC X.                                     00002930
             03 MSEGMENTO    PIC X(20).                                 00002940
             03 FILLER       PIC X(2).                                  00002950
             03 MNCOFFREA    PIC X.                                     00002960
             03 MNCOFFREC    PIC X.                                     00002970
             03 MNCOFFREP    PIC X.                                     00002980
             03 MNCOFFREH    PIC X.                                     00002990
             03 MNCOFFREV    PIC X.                                     00003000
             03 MNCOFFREO    PIC X(7).                                  00003010
             03 FILLER       PIC X(2).                                  00003020
             03 MLCOFFREA    PIC X.                                     00003030
             03 MLCOFFREC    PIC X.                                     00003040
             03 MLCOFFREP    PIC X.                                     00003050
             03 MLCOFFREH    PIC X.                                     00003060
             03 MLCOFFREV    PIC X.                                     00003070
             03 MLCOFFREO    PIC X(20).                                 00003080
             03 FILLER       PIC X(2).                                  00003090
             03 MCASSORTCA   PIC X.                                     00003100
             03 MCASSORTCC   PIC X.                                     00003110
             03 MCASSORTCP   PIC X.                                     00003120
             03 MCASSORTCH   PIC X.                                     00003130
             03 MCASSORTCV   PIC X.                                     00003140
             03 MCASSORTCO   PIC X(5).                                  00003150
             03 FILLER       PIC X(2).                                  00003160
             03 MACTA   PIC X.                                          00003170
             03 MACTC   PIC X.                                          00003180
             03 MACTP   PIC X.                                          00003190
             03 MACTH   PIC X.                                          00003200
             03 MACTV   PIC X.                                          00003210
             03 MACTO   PIC X(3).                                       00003220
           02 FILLER    PIC X(2).                                       00003230
           02 MZONCMDA  PIC X.                                          00003240
           02 MZONCMDC  PIC X.                                          00003250
           02 MZONCMDP  PIC X.                                          00003260
           02 MZONCMDH  PIC X.                                          00003270
           02 MZONCMDV  PIC X.                                          00003280
           02 MZONCMDO  PIC X(15).                                      00003290
           02 FILLER    PIC X(2).                                       00003300
           02 MLIBERRA  PIC X.                                          00003310
           02 MLIBERRC  PIC X.                                          00003320
           02 MLIBERRP  PIC X.                                          00003330
           02 MLIBERRH  PIC X.                                          00003340
           02 MLIBERRV  PIC X.                                          00003350
           02 MLIBERRO  PIC X(58).                                      00003360
           02 FILLER    PIC X(2).                                       00003370
           02 MCODTRAA  PIC X.                                          00003380
           02 MCODTRAC  PIC X.                                          00003390
           02 MCODTRAP  PIC X.                                          00003400
           02 MCODTRAH  PIC X.                                          00003410
           02 MCODTRAV  PIC X.                                          00003420
           02 MCODTRAO  PIC X(4).                                       00003430
           02 FILLER    PIC X(2).                                       00003440
           02 MCICSA    PIC X.                                          00003450
           02 MCICSC    PIC X.                                          00003460
           02 MCICSP    PIC X.                                          00003470
           02 MCICSH    PIC X.                                          00003480
           02 MCICSV    PIC X.                                          00003490
           02 MCICSO    PIC X(5).                                       00003500
           02 FILLER    PIC X(2).                                       00003510
           02 MNETNAMA  PIC X.                                          00003520
           02 MNETNAMC  PIC X.                                          00003530
           02 MNETNAMP  PIC X.                                          00003540
           02 MNETNAMH  PIC X.                                          00003550
           02 MNETNAMV  PIC X.                                          00003560
           02 MNETNAMO  PIC X(8).                                       00003570
           02 FILLER    PIC X(2).                                       00003580
           02 MSCREENA  PIC X.                                          00003590
           02 MSCREENC  PIC X.                                          00003600
           02 MSCREENP  PIC X.                                          00003610
           02 MSCREENH  PIC X.                                          00003620
           02 MSCREENV  PIC X.                                          00003630
           02 MSCREENO  PIC X(4).                                       00003640
                                                                                
