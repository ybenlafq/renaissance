      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MDMAG PARAMETRES NMD PAR MAGASIN       *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01X5.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01X5.                                                            
      *}                                                                        
           05  MDMAG-CTABLEG2    PIC X(15).                                     
           05  MDMAG-CTABLEG2-REDEF REDEFINES MDMAG-CTABLEG2.                   
               10  MDMAG-NSOC            PIC X(03).                             
               10  MDMAG-NLIEU           PIC X(03).                             
           05  MDMAG-WTABLEG     PIC X(80).                                     
           05  MDMAG-WTABLEG-REDEF  REDEFINES MDMAG-WTABLEG.                    
               10  MDMAG-WMG             PIC X(01).                             
               10  MDMAG-CMAGPR          PIC X(06).                             
               10  MDMAG-WSL             PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01X5-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01X5-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MDMAG-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MDMAG-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MDMAG-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MDMAG-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
