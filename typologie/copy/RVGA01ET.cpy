      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE LIVRG ZONE DE LIVRAISON/GENERALITES    *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01ET.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01ET.                                                            
      *}                                                                        
           05  LIVRG-CTABLEG2    PIC X(15).                                     
           05  LIVRG-CTABLEG2-REDEF REDEFINES LIVRG-CTABLEG2.                   
               10  LIVRG-CZONLIV         PIC X(05).                             
           05  LIVRG-WTABLEG     PIC X(80).                                     
           05  LIVRG-WTABLEG-REDEF  REDEFINES LIVRG-WTABLEG.                    
               10  LIVRG-LZONLIV         PIC X(20).                             
               10  LIVRG-LIEUGEST        PIC X(06).                             
               10  LIVRG-LDEPLIV         PIC X(06).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01ET-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01ET-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LIVRG-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  LIVRG-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LIVRG-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  LIVRG-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
