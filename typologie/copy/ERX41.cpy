      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERX41   ERX41                                              00000020
      ***************************************************************** 00000030
       01   ERX41I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRELEVL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNRELEVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNRELEVF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNRELEVI  PIC X(7).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCONCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCONCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCONCI   PIC X(4).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCONCL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLCONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLCONCF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLCONCI   PIC X(15).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNMAGI    PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCVENDL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCVENDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCVENDF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCVENDI   PIC X(6).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVENDL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLVENDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLVENDF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLVENDI   PIC X(15).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDERCODL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDERCODL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDERCODF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDERCODI  PIC X(7).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLEL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MLIBELLEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBELLEF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLIBELLEI      PIC X(10).                                 00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MREPORTL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MREPORTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MREPORTF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MREPORTI  PIC X(17).                                      00000530
           02 MCFAMD OCCURS   13 TIMES .                                00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCFAMI  PIC X(5).                                       00000580
           02 MCMARQD OCCURS   13 TIMES .                               00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000600
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000610
             03 FILLER  PIC X(4).                                       00000620
             03 MCMARQI      PIC X(5).                                  00000630
           02 MREFFOUD OCCURS   13 TIMES .                              00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREFFOUL     COMP PIC S9(4).                            00000650
      *--                                                                       
             03 MREFFOUL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MREFFOUF     PIC X.                                     00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MREFFOUI     PIC X(21).                                 00000680
           02 MNCODICD OCCURS   13 TIMES .                              00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000700
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000710
             03 FILLER  PIC X(4).                                       00000720
             03 MNCODICI     PIC X(7).                                  00000730
           02 MPRELEVD OCCURS   13 TIMES .                              00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRELEVL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MPRELEVL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPRELEVF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MPRELEVI     PIC X(9).                                  00000780
           02 MQCOLD OCCURS   13 TIMES .                                00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQCOLL  COMP PIC S9(4).                                 00000800
      *--                                                                       
             03 MQCOLL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQCOLF  PIC X.                                          00000810
             03 FILLER  PIC X(4).                                       00000820
             03 MQCOLI  PIC X(3).                                       00000830
           02 MLCOMMD OCCURS   13 TIMES .                               00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOMML      COMP PIC S9(4).                            00000850
      *--                                                                       
             03 MLCOMML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLCOMMF      PIC X.                                     00000860
             03 FILLER  PIC X(4).                                       00000870
             03 MLCOMMI      PIC X(17).                                 00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000890
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000900
           02 FILLER    PIC X(4).                                       00000910
           02 MLIBERRI  PIC X(77).                                      00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000930
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000940
           02 FILLER    PIC X(4).                                       00000950
           02 MCODTRAI  PIC X(4).                                       00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MCICSI    PIC X(5).                                       00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001010
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001020
           02 FILLER    PIC X(4).                                       00001030
           02 MNETNAMI  PIC X(8).                                       00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001050
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001060
           02 FILLER    PIC X(4).                                       00001070
           02 MSCREENI  PIC X(4).                                       00001080
      ***************************************************************** 00001090
      * SDF: ERX41   ERX41                                              00001100
      ***************************************************************** 00001110
       01   ERX41O REDEFINES ERX41I.                                    00001120
           02 FILLER    PIC X(12).                                      00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MDATJOUA  PIC X.                                          00001150
           02 MDATJOUC  PIC X.                                          00001160
           02 MDATJOUP  PIC X.                                          00001170
           02 MDATJOUH  PIC X.                                          00001180
           02 MDATJOUV  PIC X.                                          00001190
           02 MDATJOUO  PIC X(10).                                      00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MTIMJOUA  PIC X.                                          00001220
           02 MTIMJOUC  PIC X.                                          00001230
           02 MTIMJOUP  PIC X.                                          00001240
           02 MTIMJOUH  PIC X.                                          00001250
           02 MTIMJOUV  PIC X.                                          00001260
           02 MTIMJOUO  PIC X(5).                                       00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MNRELEVA  PIC X.                                          00001290
           02 MNRELEVC  PIC X.                                          00001300
           02 MNRELEVP  PIC X.                                          00001310
           02 MNRELEVH  PIC X.                                          00001320
           02 MNRELEVV  PIC X.                                          00001330
           02 MNRELEVO  PIC X(7).                                       00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MNSOCA    PIC X.                                          00001360
           02 MNSOCC    PIC X.                                          00001370
           02 MNSOCP    PIC X.                                          00001380
           02 MNSOCH    PIC X.                                          00001390
           02 MNSOCV    PIC X.                                          00001400
           02 MNSOCO    PIC X(3).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MNCONCA   PIC X.                                          00001430
           02 MNCONCC   PIC X.                                          00001440
           02 MNCONCP   PIC X.                                          00001450
           02 MNCONCH   PIC X.                                          00001460
           02 MNCONCV   PIC X.                                          00001470
           02 MNCONCO   PIC X(4).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MLCONCA   PIC X.                                          00001500
           02 MLCONCC   PIC X.                                          00001510
           02 MLCONCP   PIC X.                                          00001520
           02 MLCONCH   PIC X.                                          00001530
           02 MLCONCV   PIC X.                                          00001540
           02 MLCONCO   PIC X(15).                                      00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MNMAGA    PIC X.                                          00001570
           02 MNMAGC    PIC X.                                          00001580
           02 MNMAGP    PIC X.                                          00001590
           02 MNMAGH    PIC X.                                          00001600
           02 MNMAGV    PIC X.                                          00001610
           02 MNMAGO    PIC X(3).                                       00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MCVENDA   PIC X.                                          00001640
           02 MCVENDC   PIC X.                                          00001650
           02 MCVENDP   PIC X.                                          00001660
           02 MCVENDH   PIC X.                                          00001670
           02 MCVENDV   PIC X.                                          00001680
           02 MCVENDO   PIC X(6).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MLVENDA   PIC X.                                          00001710
           02 MLVENDC   PIC X.                                          00001720
           02 MLVENDP   PIC X.                                          00001730
           02 MLVENDH   PIC X.                                          00001740
           02 MLVENDV   PIC X.                                          00001750
           02 MLVENDO   PIC X(15).                                      00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MDERCODA  PIC X.                                          00001780
           02 MDERCODC  PIC X.                                          00001790
           02 MDERCODP  PIC X.                                          00001800
           02 MDERCODH  PIC X.                                          00001810
           02 MDERCODV  PIC X.                                          00001820
           02 MDERCODO  PIC X(7).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MLIBELLEA      PIC X.                                     00001850
           02 MLIBELLEC PIC X.                                          00001860
           02 MLIBELLEP PIC X.                                          00001870
           02 MLIBELLEH PIC X.                                          00001880
           02 MLIBELLEV PIC X.                                          00001890
           02 MLIBELLEO      PIC X(10).                                 00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MREPORTA  PIC X.                                          00001920
           02 MREPORTC  PIC X.                                          00001930
           02 MREPORTP  PIC X.                                          00001940
           02 MREPORTH  PIC X.                                          00001950
           02 MREPORTV  PIC X.                                          00001960
           02 MREPORTO  PIC X(17).                                      00001970
           02 DFHMS1 OCCURS   13 TIMES .                                00001980
             03 FILLER       PIC X(2).                                  00001990
             03 MCFAMA  PIC X.                                          00002000
             03 MCFAMC  PIC X.                                          00002010
             03 MCFAMP  PIC X.                                          00002020
             03 MCFAMH  PIC X.                                          00002030
             03 MCFAMV  PIC X.                                          00002040
             03 MCFAMO  PIC X(5).                                       00002050
           02 DFHMS2 OCCURS   13 TIMES .                                00002060
             03 FILLER       PIC X(2).                                  00002070
             03 MCMARQA      PIC X.                                     00002080
             03 MCMARQC PIC X.                                          00002090
             03 MCMARQP PIC X.                                          00002100
             03 MCMARQH PIC X.                                          00002110
             03 MCMARQV PIC X.                                          00002120
             03 MCMARQO      PIC X(5).                                  00002130
           02 DFHMS3 OCCURS   13 TIMES .                                00002140
             03 FILLER       PIC X(2).                                  00002150
             03 MREFFOUA     PIC X.                                     00002160
             03 MREFFOUC     PIC X.                                     00002170
             03 MREFFOUP     PIC X.                                     00002180
             03 MREFFOUH     PIC X.                                     00002190
             03 MREFFOUV     PIC X.                                     00002200
             03 MREFFOUO     PIC X(21).                                 00002210
           02 DFHMS4 OCCURS   13 TIMES .                                00002220
             03 FILLER       PIC X(2).                                  00002230
             03 MNCODICA     PIC X.                                     00002240
             03 MNCODICC     PIC X.                                     00002250
             03 MNCODICP     PIC X.                                     00002260
             03 MNCODICH     PIC X.                                     00002270
             03 MNCODICV     PIC X.                                     00002280
             03 MNCODICO     PIC X(7).                                  00002290
           02 DFHMS5 OCCURS   13 TIMES .                                00002300
             03 FILLER       PIC X(2).                                  00002310
             03 MPRELEVA     PIC X.                                     00002320
             03 MPRELEVC     PIC X.                                     00002330
             03 MPRELEVP     PIC X.                                     00002340
             03 MPRELEVH     PIC X.                                     00002350
             03 MPRELEVV     PIC X.                                     00002360
             03 MPRELEVO     PIC X(9).                                  00002370
           02 DFHMS6 OCCURS   13 TIMES .                                00002380
             03 FILLER       PIC X(2).                                  00002390
             03 MQCOLA  PIC X.                                          00002400
             03 MQCOLC  PIC X.                                          00002410
             03 MQCOLP  PIC X.                                          00002420
             03 MQCOLH  PIC X.                                          00002430
             03 MQCOLV  PIC X.                                          00002440
             03 MQCOLO  PIC ZZZ.                                        00002450
           02 DFHMS7 OCCURS   13 TIMES .                                00002460
             03 FILLER       PIC X(2).                                  00002470
             03 MLCOMMA      PIC X.                                     00002480
             03 MLCOMMC PIC X.                                          00002490
             03 MLCOMMP PIC X.                                          00002500
             03 MLCOMMH PIC X.                                          00002510
             03 MLCOMMV PIC X.                                          00002520
             03 MLCOMMO      PIC X(17).                                 00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MLIBERRA  PIC X.                                          00002550
           02 MLIBERRC  PIC X.                                          00002560
           02 MLIBERRP  PIC X.                                          00002570
           02 MLIBERRH  PIC X.                                          00002580
           02 MLIBERRV  PIC X.                                          00002590
           02 MLIBERRO  PIC X(77).                                      00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MCODTRAA  PIC X.                                          00002620
           02 MCODTRAC  PIC X.                                          00002630
           02 MCODTRAP  PIC X.                                          00002640
           02 MCODTRAH  PIC X.                                          00002650
           02 MCODTRAV  PIC X.                                          00002660
           02 MCODTRAO  PIC X(4).                                       00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MCICSA    PIC X.                                          00002690
           02 MCICSC    PIC X.                                          00002700
           02 MCICSP    PIC X.                                          00002710
           02 MCICSH    PIC X.                                          00002720
           02 MCICSV    PIC X.                                          00002730
           02 MCICSO    PIC X(5).                                       00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MNETNAMA  PIC X.                                          00002760
           02 MNETNAMC  PIC X.                                          00002770
           02 MNETNAMP  PIC X.                                          00002780
           02 MNETNAMH  PIC X.                                          00002790
           02 MNETNAMV  PIC X.                                          00002800
           02 MNETNAMO  PIC X(8).                                       00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MSCREENA  PIC X.                                          00002830
           02 MSCREENC  PIC X.                                          00002840
           02 MSCREENP  PIC X.                                          00002850
           02 MSCREENH  PIC X.                                          00002860
           02 MSCREENV  PIC X.                                          00002870
           02 MSCREENO  PIC X(4).                                       00002880
                                                                                
