      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE IFTCT TYPES DE COMPTABILITE            *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01SJ.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01SJ.                                                            
      *}                                                                        
           05  IFTCT-CTABLEG2    PIC X(15).                                     
           05  IFTCT-CTABLEG2-REDEF REDEFINES IFTCT-CTABLEG2.                   
               10  IFTCT-CTYPCTA         PIC X(03).                             
           05  IFTCT-WTABLEG     PIC X(80).                                     
           05  IFTCT-WTABLEG-REDEF  REDEFINES IFTCT-WTABLEG.                    
               10  IFTCT-WACTIF          PIC X(01).                             
               10  IFTCT-LTYPCTA         PIC X(20).                             
               10  IFTCT-APPC            PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01SJ-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01SJ-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IFTCT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  IFTCT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IFTCT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  IFTCT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
