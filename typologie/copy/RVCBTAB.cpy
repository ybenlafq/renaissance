      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CBTAB CBN TABLE INTERESSEMENT DEFAUT   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCBTAB.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCBTAB.                                                             
      *}                                                                        
           05  CBTAB-CTABLEG2    PIC X(15).                                     
           05  CBTAB-CTABLEG2-REDEF REDEFINES CBTAB-CTABLEG2.                   
               10  CBTAB-FONCTION        PIC X(05).                             
               10  CBTAB-CPRINC          PIC X(05).                             
               10  CBTAB-CSECOND         PIC X(05).                             
           05  CBTAB-WTABLEG     PIC X(80).                                     
           05  CBTAB-WTABLEG-REDEF  REDEFINES CBTAB-WTABLEG.                    
               10  CBTAB-WFLAG           PIC X(01).                             
               10  CBTAB-CTYPE           PIC X(05).                             
               10  CBTAB-CTABLE          PIC X(10).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCBTAB-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCBTAB-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CBTAB-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CBTAB-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CBTAB-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CBTAB-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
