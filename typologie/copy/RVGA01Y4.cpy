      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SLETA NMD/SL : ETAT DES PRODUITS       *        
      *----------------------------------------------------------------*        
       01  RVGA01Y4.                                                            
           05  SLETA-CTABLEG2    PIC X(15).                                     
           05  SLETA-CTABLEG2-REDEF REDEFINES SLETA-CTABLEG2.                   
               10  SLETA-CETAT           PIC X(02).                             
           05  SLETA-WTABLEG     PIC X(80).                                     
           05  SLETA-WTABLEG-REDEF  REDEFINES SLETA-WTABLEG.                    
               10  SLETA-LETAT           PIC X(20).                             
               10  SLETA-WVND            PIC X(01).                             
               10  SLETA-WTQ             PIC X(01).                             
               10  SLETA-WHS             PIC X(01).                             
               10  SLETA-WUT             PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01Y4-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLETA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SLETA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLETA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SLETA-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
