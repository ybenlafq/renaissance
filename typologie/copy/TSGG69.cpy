      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TSGG69                                         *  00020000
      *       TR : GG69  RECYCLAGE PRMP SUR STOCK                    *  00030000
      *       PG : TGG69 CALCUL DU NOUVEAU PRMP SUR STOCK            *  00040000
      ****************************************************************  00050000
      *                                                                 00060000
      *------------------------------ LONGUEUR                          00070000
       01  TS-GG69-RECORD.                                              00080006
           05 TS-GG69-LIGNE OCCURS 4.                                           
              10 TS-GG69-NSOCF       PIC X(3).                                  
              10 TS-GG69-SOC-FIL     PIC X(3).                                  
              10 TS-GG69-INDIC       PIC S9(1)       COMP-3.                    
              10 TS-GG69-DPRMP       PIC S9(7)V9(6)  COMP-3.                    
              10 TS-GG69-STOCKF      PIC S9(9)       COMP-3.                    
              10 TS-GG69-VSTOCK      PIC S9(11)V9(2) COMP-3.                    
              10 TS-GG69-STOCKCOUR   PIC S9(9)       COMP-3.                    
              10 TS-GG69-STOCKDEBJ   PIC S9(9)       COMP-3.                    
              10 TS-GG69-STOCKDEBD   PIC S9(9)       COMP-3.                    
              10 TS-GG69-PRIST       PIC S9(7)V9(6)  COMP-3.                    
              10 TS-GG69-PRISTT      PIC S9(7)V9(2)  COMP-3.                    
              10 TS-GG69-PSUPP       PIC S9(7)V9(6)  COMP-3.                    
              10 TS-GG69-NPRMP       PIC S9(7)V9(6)  COMP-3.                    
              10 TS-GG69-PRMP        PIC S9(7)V9(6)  COMP-3.                    
              10 TS-GG69-PCF         PIC S9(7)V9(6)  COMP-3.                    
              10 TS-GG69-DATE        PIC X(8).                                  
              10 TS-GG69-VSTOCKD     PIC S9(11)V9(2) COMP-3.                    
                                                                                
