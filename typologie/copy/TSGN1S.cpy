      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * GESTION CBN     PRODUITS SIMILAIRES                                     
E0107 ******************************************************************        
      * DSA057 07/09/04 SUPPORT MAINTENANCE CBN                                 
      *                 AJOUT GESTION CODIC SIMILAIRE                           
      ******************************************************************        
       01 TS-GN1S-ENR.                                                          
           05 TS-GN1S-DONNEE.                                                   
               10 TS-GN1S-NCODIC     PIC X(07).                                 
               10 TS-GN1S-LREFFOURN  PIC X(20).                                 
                                                                                
