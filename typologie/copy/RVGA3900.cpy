      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * DCLGEN TABLE(DSA000.RTGA39)                                    *        
      *        LIBRARY(DSA043.DEVL.SOURCE(RVGA3900))                   *        
      *        ACTION(REPLACE)                                         *        
      *        LANGUAGE(COBOL)                                         *        
      *        NAMES(GA39-)                                            *        
      *        STRUCTURE(RVGA3900)                                     *        
      *        APOST                                                   *        
      *        LABEL(YES)                                              *        
      *        DBCSDELIM(NO)                                           *        
      *        COLSUFFIX(YES)                                          *        
      *        INDVAR(YES)                                             *        
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *        
      ******************************************************************        
           EXEC SQL DECLARE DSA000.RTGA39 TABLE                                 
           ( NENTSAP                        CHAR(5) NOT NULL,                   
             NENTGRF                        CHAR(5) NOT NULL,                   
             LENTSAP                        CHAR(20) NOT NULL,                  
             NENTANC                        CHAR(5) NOT NULL,                   
             LENTANC                        CHAR(20) NOT NULL,                  
             DSYST                          DECIMAL(13, 0) NOT NULL             
           ) END-EXEC.                                                          
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RTGA39                      *        
      ******************************************************************        
       01  RVGA3900.                                                            
      *    *************************************************************        
      *                       NENTSAP                                           
           10 GA39-NENTSAP         PIC X(5).                                    
      *    *************************************************************        
      *                       NENTGRF                                           
           10 GA39-NENTGRF         PIC X(5).                                    
      *    *************************************************************        
      *                       LENTSAP                                           
           10 GA39-LENTSAP         PIC X(20).                                   
      *    *************************************************************        
      *                       NENTANC                                           
           10 GA39-NENTANC         PIC X(5).                                    
      *    *************************************************************        
      *                       LENTANC                                           
           10 GA39-LENTANC         PIC X(20).                                   
      *    *************************************************************        
      *                       DSYST                                             
           10 GA39-DSYST           PIC S9(13)V USAGE COMP-3.                    
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  IRTGA39.                                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 INDSTRUC           PIC S9(4) USAGE COMP OCCURS 6 TIMES.           
      *--                                                                       
           10 INDSTRUC           PIC S9(4) COMP-5 OCCURS 6 TIMES.               
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 6       *        
      ******************************************************************        
                                                                                
