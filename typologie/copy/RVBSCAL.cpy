      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE BSCAL REGLE DE CALCUL DU PRIX OFFRE    *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBSCAL.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBSCAL.                                                             
      *}                                                                        
           05  BSCAL-CTABLEG2    PIC X(15).                                     
           05  BSCAL-CTABLEG2-REDEF REDEFINES BSCAL-CTABLEG2.                   
               10  BSCAL-CCALCUL         PIC X(05).                             
           05  BSCAL-WTABLEG     PIC X(80).                                     
           05  BSCAL-WTABLEG-REDEF  REDEFINES BSCAL-WTABLEG.                    
               10  BSCAL-WACTIF          PIC X(01).                             
               10  BSCAL-LIBELLE         PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBSCAL-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBSCAL-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BSCAL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  BSCAL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BSCAL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  BSCAL-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
