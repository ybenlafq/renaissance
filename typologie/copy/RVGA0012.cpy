      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DEVNCG00.RTGA00                    *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA0012.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA0012.                                                            
      *}                                                                        
           10 GA00-NCODIC          PIC X(7).                                    
           10 GA00-LREFFOURN       PIC X(20).                                   
           10 GA00-CFAM            PIC X(5).                                    
           10 GA00-CMARQ           PIC X(5).                                    
           10 GA00-CASSORT         PIC X(5).                                    
           10 GA00-DEFSTATUT       PIC X(8).                                    
           10 GA00-DCREATION       PIC X(8).                                    
           10 GA00-D1RECEPT        PIC X(8).                                    
           10 GA00-DMAJ            PIC X(8).                                    
           10 GA00-LREFDARTY       PIC X(20).                                   
           10 GA00-LCOMMENT        PIC X(50).                                   
           10 GA00-LSTATCOMP       PIC X(3).                                    
           10 GA00-WDACEM          PIC X(1).                                    
           10 GA00-WTLMELA         PIC X(1).                                    
           10 GA00-PRAR            PIC S9(7)V9(2) COMP-3.                       
           10 GA00-CTAUXTVA        PIC X(5).                                    
           10 GA00-CEXPO           PIC X(5).                                    
           10 GA00-WSENSVTE        PIC X(1).                                    
           10 GA00-CGESTVTE        PIC X(3).                                    
           10 GA00-CGARANTIE       PIC X(5).                                    
           10 GA00-CAPPRO          PIC X(5).                                    
           10 GA00-WSENSAPPRO      PIC X(1).                                    
           10 GA00-QDELAIAPPRO     PIC X(3).                                    
           10 GA00-QCOLICDE        PIC S9(3) COMP-3.                            
           10 GA00-CHEFPROD        PIC X(5).                                    
           10 GA00-CORIGPROD       PIC X(5).                                    
           10 GA00-LEMBALLAGE      PIC X(50).                                   
           10 GA00-CTYPCONDT       PIC X(5).                                    
           10 GA00-QCONDT          PIC S9(5) COMP-3.                            
           10 GA00-QCOLIRECEPT     PIC S9(5) COMP-3.                            
           10 GA00-QCOLIDSTOCK     PIC S9(5) COMP-3.                            
           10 GA00-QCOLIVTE        PIC S9(5) COMP-3.                            
           10 GA00-QPOIDS          PIC S9(7) COMP-3.                            
           10 GA00-QLARGEUR        PIC S9(3) COMP-3.                            
           10 GA00-QPROFONDEUR     PIC S9(3) COMP-3.                            
           10 GA00-QHAUTEUR        PIC S9(3) COMP-3.                            
           10 GA00-CGARCONST       PIC X(5).                                    
           10 GA00-CMODSTOCK1      PIC X(5).                                    
           10 GA00-WMODSTOCK1      PIC X(1).                                    
           10 GA00-CMODSTOCK2      PIC X(5).                                    
           10 GA00-WMODSTOCK2      PIC X(1).                                    
           10 GA00-CMODSTOCK3      PIC X(5).                                    
           10 GA00-WMODSTOCK3      PIC X(1).                                    
           10 GA00-CFETEMPL        PIC X(1).                                    
           10 GA00-QNBRANMAIL      PIC S9(3) COMP-3.                            
           10 GA00-QNBNIVGERB      PIC S9(3) COMP-3.                            
           10 GA00-CZONEACCES      PIC X(1).                                    
           10 GA00-CCONTENEUR      PIC X(1).                                    
           10 GA00-CSPECIFSTK      PIC X(1).                                    
           10 GA00-CCOTEHOLD       PIC X(1).                                    
           10 GA00-CFETIQINFO      PIC X(1).                                    
           10 GA00-CFETIQPRIX      PIC X(1).                                    
           10 GA00-QNBPVSOL        PIC S9(3) COMP-3.                            
           10 GA00-QNBPRACK        PIC S9(3) COMP-3.                            
           10 GA00-WLCONF          PIC X(1).                                    
           10 GA00-CTPSUP          PIC X(1).                                    
           10 GA00-WRESFOURN       PIC X(1).                                    
           10 GA00-CQUOTA          PIC X(5).                                    
           10 GA00-QCONTENU        PIC S9(5) COMP-3.                            
           10 GA00-QGRATUITE       PIC S9(5) COMP-3.                            
           10 GA00-NEAN            PIC X(13).                                   
           10 GA00-WSTOCKAVANCE    PIC X(1).                                    
           10 GA00-COPCO           PIC X(3).                                    
           10 GA00-LREFO           PIC X(20).                                   
           10 GA00-NPROD           PIC X(15).                                   
           10 GA00-CCOLOR          PIC X(5).                                    
           10 GA00-CLIGPROD        PIC X(5).                                    
           10 GA00-LMODBASE        PIC X(20).                                   
           10 GA00-VERSION         PIC X(20).                                   
           10 GA00-WMULTISOC       PIC X(1).                                    
           10 GA00-QPOIDSDE        PIC S9(7) COMP-3.                            
           10 GA00-QLARGEURDE      PIC S9(3) COMP-3.                            
           10 GA00-QPROFONDEURDE   PIC S9(3) COMP-3.                            
           10 GA00-QHAUTEURDE      PIC S9(3) COMP-3.                            
           10 GA00-PBF             PIC S9(7)V9(2)  COMP-3.                      
           10 GA00-WCROSSDOCK      PIC X(01).                                   
           10 GA00-CUSINE          PIC X(01).                                   
           10 GA00-CUSINEVRAI      PIC X(05).                                   
           10 GA00-QLARGDE         PIC S9(4)V9 COMP-3.                          
           10 GA00-QPROFDE         PIC S9(4)V9 COMP-3.                          
           10 GA00-QHAUTDE         PIC S9(4)V9 COMP-3.                          
           10 GA00-CNOMDOU         PIC X(08).                                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA0012-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA0012-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-NCODIC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-LREFFOURN-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-LREFFOURN-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CFAM-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CFAM-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CMARQ-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CMARQ-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CASSORT-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CASSORT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-DEFSTATUT-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-DEFSTATUT-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-DCREATION-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-DCREATION-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-D1RECEPT-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-D1RECEPT-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-DMAJ-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-DMAJ-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-LREFDARTY-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-LREFDARTY-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-LCOMMENT-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-LCOMMENT-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-LSTATCOMP-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-LSTATCOMP-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-WDACEM-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-WDACEM-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-WTLMELA-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-WTLMELA-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-PRAR-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-PRAR-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CTAUXTVA-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CTAUXTVA-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CEXPO-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CEXPO-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-WSENSVTE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-WSENSVTE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CGESTVTE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CGESTVTE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CGARANTIE-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CGARANTIE-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CAPPRO-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CAPPRO-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-WSENSAPPRO-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-WSENSAPPRO-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-QDELAIAPPRO-F   PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-QDELAIAPPRO-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-QCOLICDE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-QCOLICDE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CHEFPROD-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CHEFPROD-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CORIGPROD-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CORIGPROD-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-LEMBALLAGE-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-LEMBALLAGE-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CTYPCONDT-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CTYPCONDT-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-QCONDT-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-QCONDT-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-QCOLIRECEPT-F   PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-QCOLIRECEPT-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-QCOLIDSTOCK-F   PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-QCOLIDSTOCK-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-QCOLIVTE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-QCOLIVTE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-QPOIDS-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-QPOIDS-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-QLARGEUR-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-QLARGEUR-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-QPROFONDEUR-F   PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-QPROFONDEUR-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-QHAUTEUR-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-QHAUTEUR-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CGARCONST-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CGARCONST-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CMODSTOCK1-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CMODSTOCK1-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-WMODSTOCK1-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-WMODSTOCK1-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CMODSTOCK2-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CMODSTOCK2-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-WMODSTOCK2-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-WMODSTOCK2-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CMODSTOCK3-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CMODSTOCK3-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-WMODSTOCK3-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-WMODSTOCK3-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CFETEMPL-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CFETEMPL-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-QNBRANMAIL-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-QNBRANMAIL-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-QNBNIVGERB-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-QNBNIVGERB-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CZONEACCES-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CZONEACCES-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CCONTENEUR-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CCONTENEUR-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CSPECIFSTK-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CSPECIFSTK-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CCOTEHOLD-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CCOTEHOLD-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CFETIQINFO-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CFETIQINFO-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CFETIQPRIX-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CFETIQPRIX-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-QNBPVSOL-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-QNBPVSOL-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-QNBPRACK-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-QNBPRACK-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-WLCONF-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-WLCONF-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CTPSUP-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CTPSUP-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-WRESFOURN-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-WRESFOURN-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CQUOTA-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CQUOTA-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-QCONTENU-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-QCONTENU-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-QGRATUITE-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-QGRATUITE-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-NEAN-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-NEAN-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-WSTOCKAVANCE-F  PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-WSTOCKAVANCE-F  PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-COPCO-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-COPCO-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-LREFO-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-LREFO-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-NPROD-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-NPROD-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CCOLOR-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CCOLOR-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CLIGPROD-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CLIGPROD-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-LMODBASE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-LMODBASE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-VERSION-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-VERSION-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-WMULTISOC-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-WMULTISOC-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-QPOIDSDE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-QPOIDSDE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-QLARGEURDE-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-QLARGEURDE-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-QPROFONDEURDE-F PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-QPROFONDEURDE-F PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-QHAUTEURDE-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-QHAUTEURDE-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-PBF-F           PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-PBF-F           PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-WCROSSDOCK-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-WCROSSDOCK-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CUSINE-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CUSINE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CUSINEVRAI-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-CUSINEVRAI-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-QLARGDE-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-QLARGDE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-QPROFDE-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-QPROFDE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-QHAUTDE-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GA00-QHAUTDE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GA00-CNOMDOU-F       PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 GA00-CNOMDOU-F       PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
