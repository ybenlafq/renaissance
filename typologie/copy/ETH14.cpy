      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESE00   ESE00                                              00000020
      ***************************************************************** 00000030
       01   ETH14I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFONCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFONCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCFONCI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPCOL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCOPCOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOPCOF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCOPCOI   PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLOPCOL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLOPCOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLOPCOF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLOPCOI   PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFFETL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MDEFFETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEFFETF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDEFFETI  PIC X(10).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNCODICI  PIC X(7).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLREFFOURNI    PIC X(20).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCFAMI    PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLFAMI    PIC X(20).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MHISTOL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MHISTOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MHISTOF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MHISTOI   PIC X.                                          00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCMARQI   PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLMARQI   PIC X(20).                                      00000650
           02 MPCFD OCCURS   10 TIMES .                                 00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPCFL   COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MPCFL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MPCFF   PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MPCFI   PIC X(14).                                      00000700
           02 MNTTAXED OCCURS   10 TIMES .                              00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNTTAXEL     COMP PIC S9(4).                            00000720
      *--                                                                       
             03 MNTTAXEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNTTAXEF     PIC X.                                     00000730
             03 FILLER  PIC X(4).                                       00000740
             03 MNTTAXEI     PIC X(8).                                  00000750
           02 MDEFFETLD OCCURS   10 TIMES .                             00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETLL    COMP PIC S9(4).                            00000770
      *--                                                                       
             03 MDEFFETLL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDEFFETLF    PIC X.                                     00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MDEFFETLI    PIC X(10).                                 00000800
           02 MC1D OCCURS   10 TIMES .                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MC1L    COMP PIC S9(4).                                 00000820
      *--                                                                       
             03 MC1L COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MC1F    PIC X.                                          00000830
             03 FILLER  PIC X(4).                                       00000840
             03 MC1I    PIC X(7).                                       00000850
           02 MC2D OCCURS   10 TIMES .                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MC2L    COMP PIC S9(4).                                 00000870
      *--                                                                       
             03 MC2L COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MC2F    PIC X.                                          00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MC2I    PIC X(7).                                       00000900
           02 MC3D OCCURS   10 TIMES .                                  00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MC3L    COMP PIC S9(4).                                 00000920
      *--                                                                       
             03 MC3L COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MC3F    PIC X.                                          00000930
             03 FILLER  PIC X(4).                                       00000940
             03 MC3I    PIC X(7).                                       00000950
           02 MC4D OCCURS   10 TIMES .                                  00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MC4L    COMP PIC S9(4).                                 00000970
      *--                                                                       
             03 MC4L COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MC4F    PIC X.                                          00000980
             03 FILLER  PIC X(4).                                       00000990
             03 MC4I    PIC X(6).                                       00001000
           02 MTAXED OCCURS   10 TIMES .                                00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTAXEL  COMP PIC S9(4).                                 00001020
      *--                                                                       
             03 MTAXEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTAXEF  PIC X.                                          00001030
             03 FILLER  PIC X(4).                                       00001040
             03 MTAXEI  PIC X.                                          00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MZONCMDI  PIC X(15).                                      00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MLIBERRI  PIC X(58).                                      00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MCODTRAI  PIC X(4).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MCICSI    PIC X(5).                                       00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MNETNAMI  PIC X(8).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MSCREENI  PIC X(4).                                       00001290
      ***************************************************************** 00001300
      * SDF: ESE00   ESE00                                              00001310
      ***************************************************************** 00001320
       01   ETH14O REDEFINES ETH14I.                                    00001330
           02 FILLER    PIC X(12).                                      00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MDATJOUA  PIC X.                                          00001360
           02 MDATJOUC  PIC X.                                          00001370
           02 MDATJOUP  PIC X.                                          00001380
           02 MDATJOUH  PIC X.                                          00001390
           02 MDATJOUV  PIC X.                                          00001400
           02 MDATJOUO  PIC X(10).                                      00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MTIMJOUA  PIC X.                                          00001430
           02 MTIMJOUC  PIC X.                                          00001440
           02 MTIMJOUP  PIC X.                                          00001450
           02 MTIMJOUH  PIC X.                                          00001460
           02 MTIMJOUV  PIC X.                                          00001470
           02 MTIMJOUO  PIC X(5).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MPAGEA    PIC X.                                          00001500
           02 MPAGEC    PIC X.                                          00001510
           02 MPAGEP    PIC X.                                          00001520
           02 MPAGEH    PIC X.                                          00001530
           02 MPAGEV    PIC X.                                          00001540
           02 MPAGEO    PIC X(3).                                       00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MPAGEMAXA      PIC X.                                     00001570
           02 MPAGEMAXC PIC X.                                          00001580
           02 MPAGEMAXP PIC X.                                          00001590
           02 MPAGEMAXH PIC X.                                          00001600
           02 MPAGEMAXV PIC X.                                          00001610
           02 MPAGEMAXO      PIC X(3).                                  00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MCFONCA   PIC X.                                          00001640
           02 MCFONCC   PIC X.                                          00001650
           02 MCFONCP   PIC X.                                          00001660
           02 MCFONCH   PIC X.                                          00001670
           02 MCFONCV   PIC X.                                          00001680
           02 MCFONCO   PIC X(3).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MCOPCOA   PIC X.                                          00001710
           02 MCOPCOC   PIC X.                                          00001720
           02 MCOPCOP   PIC X.                                          00001730
           02 MCOPCOH   PIC X.                                          00001740
           02 MCOPCOV   PIC X.                                          00001750
           02 MCOPCOO   PIC X(3).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MLOPCOA   PIC X.                                          00001780
           02 MLOPCOC   PIC X.                                          00001790
           02 MLOPCOP   PIC X.                                          00001800
           02 MLOPCOH   PIC X.                                          00001810
           02 MLOPCOV   PIC X.                                          00001820
           02 MLOPCOO   PIC X(20).                                      00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MDEFFETA  PIC X.                                          00001850
           02 MDEFFETC  PIC X.                                          00001860
           02 MDEFFETP  PIC X.                                          00001870
           02 MDEFFETH  PIC X.                                          00001880
           02 MDEFFETV  PIC X.                                          00001890
           02 MDEFFETO  PIC X(10).                                      00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MNCODICA  PIC X.                                          00001920
           02 MNCODICC  PIC X.                                          00001930
           02 MNCODICP  PIC X.                                          00001940
           02 MNCODICH  PIC X.                                          00001950
           02 MNCODICV  PIC X.                                          00001960
           02 MNCODICO  PIC X(7).                                       00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MLREFFOURNA    PIC X.                                     00001990
           02 MLREFFOURNC    PIC X.                                     00002000
           02 MLREFFOURNP    PIC X.                                     00002010
           02 MLREFFOURNH    PIC X.                                     00002020
           02 MLREFFOURNV    PIC X.                                     00002030
           02 MLREFFOURNO    PIC X(20).                                 00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCFAMA    PIC X.                                          00002060
           02 MCFAMC    PIC X.                                          00002070
           02 MCFAMP    PIC X.                                          00002080
           02 MCFAMH    PIC X.                                          00002090
           02 MCFAMV    PIC X.                                          00002100
           02 MCFAMO    PIC X(5).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MLFAMA    PIC X.                                          00002130
           02 MLFAMC    PIC X.                                          00002140
           02 MLFAMP    PIC X.                                          00002150
           02 MLFAMH    PIC X.                                          00002160
           02 MLFAMV    PIC X.                                          00002170
           02 MLFAMO    PIC X(20).                                      00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MHISTOA   PIC X.                                          00002200
           02 MHISTOC   PIC X.                                          00002210
           02 MHISTOP   PIC X.                                          00002220
           02 MHISTOH   PIC X.                                          00002230
           02 MHISTOV   PIC X.                                          00002240
           02 MHISTOO   PIC X.                                          00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MCMARQA   PIC X.                                          00002270
           02 MCMARQC   PIC X.                                          00002280
           02 MCMARQP   PIC X.                                          00002290
           02 MCMARQH   PIC X.                                          00002300
           02 MCMARQV   PIC X.                                          00002310
           02 MCMARQO   PIC X(5).                                       00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MLMARQA   PIC X.                                          00002340
           02 MLMARQC   PIC X.                                          00002350
           02 MLMARQP   PIC X.                                          00002360
           02 MLMARQH   PIC X.                                          00002370
           02 MLMARQV   PIC X.                                          00002380
           02 MLMARQO   PIC X(20).                                      00002390
           02 DFHMS1 OCCURS   10 TIMES .                                00002400
             03 FILLER       PIC X(2).                                  00002410
             03 MPCFA   PIC X.                                          00002420
             03 MPCFC   PIC X.                                          00002430
             03 MPCFP   PIC X.                                          00002440
             03 MPCFH   PIC X.                                          00002450
             03 MPCFV   PIC X.                                          00002460
             03 MPCFO   PIC X(14).                                      00002470
           02 DFHMS2 OCCURS   10 TIMES .                                00002480
             03 FILLER       PIC X(2).                                  00002490
             03 MNTTAXEA     PIC X.                                     00002500
             03 MNTTAXEC     PIC X.                                     00002510
             03 MNTTAXEP     PIC X.                                     00002520
             03 MNTTAXEH     PIC X.                                     00002530
             03 MNTTAXEV     PIC X.                                     00002540
             03 MNTTAXEO     PIC X(8).                                  00002550
           02 DFHMS3 OCCURS   10 TIMES .                                00002560
             03 FILLER       PIC X(2).                                  00002570
             03 MDEFFETLA    PIC X.                                     00002580
             03 MDEFFETLC    PIC X.                                     00002590
             03 MDEFFETLP    PIC X.                                     00002600
             03 MDEFFETLH    PIC X.                                     00002610
             03 MDEFFETLV    PIC X.                                     00002620
             03 MDEFFETLO    PIC X(10).                                 00002630
           02 DFHMS4 OCCURS   10 TIMES .                                00002640
             03 FILLER       PIC X(2).                                  00002650
             03 MC1A    PIC X.                                          00002660
             03 MC1C    PIC X.                                          00002670
             03 MC1P    PIC X.                                          00002680
             03 MC1H    PIC X.                                          00002690
             03 MC1V    PIC X.                                          00002700
             03 MC1O    PIC X(7).                                       00002710
           02 DFHMS5 OCCURS   10 TIMES .                                00002720
             03 FILLER       PIC X(2).                                  00002730
             03 MC2A    PIC X.                                          00002740
             03 MC2C    PIC X.                                          00002750
             03 MC2P    PIC X.                                          00002760
             03 MC2H    PIC X.                                          00002770
             03 MC2V    PIC X.                                          00002780
             03 MC2O    PIC X(7).                                       00002790
           02 DFHMS6 OCCURS   10 TIMES .                                00002800
             03 FILLER       PIC X(2).                                  00002810
             03 MC3A    PIC X.                                          00002820
             03 MC3C    PIC X.                                          00002830
             03 MC3P    PIC X.                                          00002840
             03 MC3H    PIC X.                                          00002850
             03 MC3V    PIC X.                                          00002860
             03 MC3O    PIC X(7).                                       00002870
           02 DFHMS7 OCCURS   10 TIMES .                                00002880
             03 FILLER       PIC X(2).                                  00002890
             03 MC4A    PIC X.                                          00002900
             03 MC4C    PIC X.                                          00002910
             03 MC4P    PIC X.                                          00002920
             03 MC4H    PIC X.                                          00002930
             03 MC4V    PIC X.                                          00002940
             03 MC4O    PIC X(6).                                       00002950
           02 DFHMS8 OCCURS   10 TIMES .                                00002960
             03 FILLER       PIC X(2).                                  00002970
             03 MTAXEA  PIC X.                                          00002980
             03 MTAXEC  PIC X.                                          00002990
             03 MTAXEP  PIC X.                                          00003000
             03 MTAXEH  PIC X.                                          00003010
             03 MTAXEV  PIC X.                                          00003020
             03 MTAXEO  PIC X.                                          00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MZONCMDA  PIC X.                                          00003050
           02 MZONCMDC  PIC X.                                          00003060
           02 MZONCMDP  PIC X.                                          00003070
           02 MZONCMDH  PIC X.                                          00003080
           02 MZONCMDV  PIC X.                                          00003090
           02 MZONCMDO  PIC X(15).                                      00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MLIBERRA  PIC X.                                          00003120
           02 MLIBERRC  PIC X.                                          00003130
           02 MLIBERRP  PIC X.                                          00003140
           02 MLIBERRH  PIC X.                                          00003150
           02 MLIBERRV  PIC X.                                          00003160
           02 MLIBERRO  PIC X(58).                                      00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MCODTRAA  PIC X.                                          00003190
           02 MCODTRAC  PIC X.                                          00003200
           02 MCODTRAP  PIC X.                                          00003210
           02 MCODTRAH  PIC X.                                          00003220
           02 MCODTRAV  PIC X.                                          00003230
           02 MCODTRAO  PIC X(4).                                       00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MCICSA    PIC X.                                          00003260
           02 MCICSC    PIC X.                                          00003270
           02 MCICSP    PIC X.                                          00003280
           02 MCICSH    PIC X.                                          00003290
           02 MCICSV    PIC X.                                          00003300
           02 MCICSO    PIC X(5).                                       00003310
           02 FILLER    PIC X(2).                                       00003320
           02 MNETNAMA  PIC X.                                          00003330
           02 MNETNAMC  PIC X.                                          00003340
           02 MNETNAMP  PIC X.                                          00003350
           02 MNETNAMH  PIC X.                                          00003360
           02 MNETNAMV  PIC X.                                          00003370
           02 MNETNAMO  PIC X(8).                                       00003380
           02 FILLER    PIC X(2).                                       00003390
           02 MSCREENA  PIC X.                                          00003400
           02 MSCREENC  PIC X.                                          00003410
           02 MSCREENP  PIC X.                                          00003420
           02 MSCREENH  PIC X.                                          00003430
           02 MSCREENV  PIC X.                                          00003440
           02 MSCREENO  PIC X(4).                                       00003450
                                                                                
