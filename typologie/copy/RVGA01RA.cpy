      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GBTLI TYPES DE LIGNE DE TRANSPORT      *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01RA.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01RA.                                                            
      *}                                                                        
           05  GBTLI-CTABLEG2    PIC X(15).                                     
           05  GBTLI-CTABLEG2-REDEF REDEFINES GBTLI-CTABLEG2.                   
               10  GBTLI-CTYPLIG         PIC X(05).                             
           05  GBTLI-WTABLEG     PIC X(80).                                     
           05  GBTLI-WTABLEG-REDEF  REDEFINES GBTLI-WTABLEG.                    
               10  GBTLI-LTYPLIG         PIC X(20).                             
               10  GBTLI-TLIEUSOC        PIC X(24).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01RA-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01RA-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GBTLI-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GBTLI-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GBTLI-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GBTLI-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
