      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      **********************************************************                
      *   COPY DE LA TABLE RVGI2500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGI2500                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGI2500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGI2500.                                                            
      *}                                                                        
           02  GI25-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GI25-CTYPEAFF                                                    
               PIC X(0001).                                                     
           02  GI25-CFAM                                                        
               PIC X(0005).                                                     
           02  GI25-CMARKETIN1                                                  
               PIC X(0005).                                                     
           02  GI25-CVMARKETIN1                                                 
               PIC X(0005).                                                     
           02  GI25-CMARKETIN2                                                  
               PIC X(0005).                                                     
           02  GI25-CVMARKETIN2                                                 
               PIC X(0005).                                                     
           02  GI25-DEFFET                                                      
               PIC X(0008).                                                     
           02  GI25-CTABINTMB                                                   
               PIC X(0005).                                                     
           02  GI25-CTABINTCA                                                   
               PIC X(0005).                                                     
           02  GI25-PCOMMVOL                                                    
               PIC S9(5)V9(0002) COMP-3.                                        
           02  GI25-PCOMMMINI                                                   
               PIC S9(4)V9(0001) COMP-3.                                        
           02  GI25-PCOMMMAXI                                                   
               PIC S9(4)V9(0001) COMP-3.                                        
           02  GI25-WAUTO                                                       
               PIC X(0001).                                                     
           02  GI25-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGI2500                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGI2500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGI2500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI25-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI25-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI25-CTYPEAFF-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI25-CTYPEAFF-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI25-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI25-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI25-CMARKETIN1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI25-CMARKETIN1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI25-CVMARKETIN1-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI25-CVMARKETIN1-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI25-CMARKETIN2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI25-CMARKETIN2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI25-CVMARKETIN2-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI25-CVMARKETIN2-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI25-DEFFET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI25-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI25-CTABINTMB-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI25-CTABINTMB-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI25-CTABINTCA-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI25-CTABINTCA-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI25-PCOMMVOL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI25-PCOMMVOL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI25-PCOMMMINI-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI25-PCOMMMINI-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI25-PCOMMMAXI-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI25-PCOMMMAXI-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI25-WAUTO-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI25-WAUTO-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GI25-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GI25-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
