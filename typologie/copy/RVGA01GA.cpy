      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PRDED PROFIL DELIVRANCE DETAILS        *        
      *----------------------------------------------------------------*        
       01  RVGA01GA.                                                            
           05  PRDED-CTABLEG2    PIC X(15).                                     
           05  PRDED-CTABLEG2-REDEF REDEFINES PRDED-CTABLEG2.                   
               10  PRDED-CPRODEL         PIC X(05).                             
               10  PRDED-CZONLIV         PIC X(05).                             
               10  PRDED-CPLAGE          PIC X(02).                             
           05  PRDED-WTABLEG     PIC X(80).                                     
           05  PRDED-WTABLEG-REDEF  REDEFINES PRDED-WTABLEG.                    
               10  PRDED-WZONACT         PIC X(01).                             
               10  PRDED-QHLIM           PIC S9(05)       COMP-3.               
               10  PRDED-QPS             PIC S9(03)V9(02)     COMP-3.           
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01GA-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRDED-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PRDED-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRDED-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PRDED-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
