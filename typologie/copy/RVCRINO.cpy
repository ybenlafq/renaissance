      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CRINO FLAG CONTROLE POUR INNOVENTE     *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCRINO .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCRINO .                                                            
      *}                                                                        
           05  CRINO-CTABLEG2    PIC X(15).                                     
           05  CRINO-CTABLEG2-REDEF REDEFINES CRINO-CTABLEG2.                   
               10  CRINO-CRCTR           PIC X(05).                             
           05  CRINO-WTABLEG     PIC X(80).                                     
           05  CRINO-WTABLEG-REDEF  REDEFINES CRINO-WTABLEG.                    
               10  CRINO-WBYPASS         PIC X(01).                             
               10  CRINO-WCONTEXT        PIC X(01).                             
               10  CRINO-WPRINT          PIC X(01).                             
               10  CRINO-VALID           PIC X(01).                             
               10  CRINO-FLAGS           PIC X(50).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCRINO-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCRINO-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CRINO-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CRINO-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CRINO-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CRINO-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
