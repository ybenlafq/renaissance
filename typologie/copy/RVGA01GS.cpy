      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SAVIN INTERVENTIONS SAV SECTEUR/ZONE   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01GS.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01GS.                                                            
      *}                                                                        
           05  SAVIN-CTABLEG2    PIC X(15).                                     
           05  SAVIN-CTABLEG2-REDEF REDEFINES SAVIN-CTABLEG2.                   
               10  SAVIN-SECTEUR         PIC X(05).                             
               10  SAVIN-CZONE           PIC X(05).                             
           05  SAVIN-WTABLEG     PIC X(80).                                     
           05  SAVIN-WTABLEG-REDEF  REDEFINES SAVIN-WTABLEG.                    
               10  SAVIN-LZONE           PIC X(20).                             
               10  SAVIN-NSOCIETE        PIC X(03).                             
               10  SAVIN-POUBELLE        PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01GS-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01GS-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SAVIN-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SAVIN-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SAVIN-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SAVIN-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
