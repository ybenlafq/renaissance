      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PROEN CLASSE DE CONSOMMATION ENERGIE   *        
      *----------------------------------------------------------------*        
       01  RVGA01OK.                                                            
           05  PROEN-CTABLEG2    PIC X(15).                                     
           05  PROEN-CTABLEG2-REDEF REDEFINES PROEN-CTABLEG2.                   
               10  PROEN-CCLASSE         PIC X(01).                             
           05  PROEN-WTABLEG     PIC X(80).                                     
           05  PROEN-WTABLEG-REDEF  REDEFINES PROEN-WTABLEG.                    
               10  PROEN-WACTIF          PIC X(01).                             
               10  PROEN-CCOUL           PIC X(05).                             
               10  PROEN-LCOMMENT        PIC X(50).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01OK-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PROEN-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PROEN-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PROEN-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PROEN-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
