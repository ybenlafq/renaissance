      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      *        IDENTIFICATION SERVICE - CODES CONTROLES               * 00000020
      ***************************************************************** 00000030
      *                                                               * 00000040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS-59-LONG          PIC S9(4) COMP VALUE +4140.              00000050
      *--                                                                       
       01  TS-59-LONG          PIC S9(4) COMP-5 VALUE +4140.                    
      *}                                                                        
       01      TS-59-DATA.                                              00000060
            10 TS-POSTE-TSSE59    OCCURS 2.                             00000080
               15    TS-59-LCODGES-AP    OCCURS 30 TIMES.               00001220
                     25 TS-59-LGEST-AP PIC X(30).                       00001230
                     25 TS-59-CGEST-AP PIC X(05).                       00001240
                     25 TS-59-TYPE       PIC X.                         00000600
                     25 TS-59-TABLE      PIC X(6).                      00000600
               15    TS-59-INOTR         OCCURS 30.                     00001530
                     25 TS-59-VISIBLE    PIC X.                         00000600
                     25 TS-59-OBLIG      PIC X.                         00000600
                     25 TS-59-SAISIE     PIC X(5).                      00000600
                     25 TS-59-VALDFAUT PIC X(20).                       00000600
                                                                                
