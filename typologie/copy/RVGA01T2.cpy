      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TFACT EDITION DOUBLE DEVISE            *        
      *----------------------------------------------------------------*        
       01  RVGA01T2.                                                            
           05  TFACT-CTABLEG2    PIC X(15).                                     
           05  TFACT-CTABLEG2-REDEF REDEFINES TFACT-CTABLEG2.                   
               10  TFACT-CLE             PIC X(12).                             
           05  TFACT-WTABLEG     PIC X(80).                                     
           05  TFACT-WTABLEG-REDEF  REDEFINES TFACT-WTABLEG.                    
               10  TFACT-WACTIF          PIC X(01).                             
               10  TFACT-DEFFET          PIC X(08).                             
               10  TFACT-DFEFFET         PIC X(08).                             
               10  TFACT-AUTORISE        PIC X(01).                             
               10  TFACT-WZONES          PIC X(30).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01T2-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TFACT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TFACT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TFACT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TFACT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
