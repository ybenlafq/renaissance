      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EXT99 LISTE DES ETATS A DUPLIQUER      *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVGA01C4.                                                            
           05  EXT99-CTABLEG2    PIC X(15).                                     
           05  EXT99-CTABLEG2-REDEF REDEFINES EXT99-CTABLEG2.                   
               10  EXT99-ETAT            PIC X(10).                             
           05  EXT99-WTABLEG     PIC X(80).                                     
           05  EXT99-WTABLEG-REDEF  REDEFINES EXT99-WTABLEG.                    
               10  EXT99-FLAG            PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01C4-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EXT99-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EXT99-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EXT99-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EXT99-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
