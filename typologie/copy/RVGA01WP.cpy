      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EDGEN EDITIONS GENERALISEES            *        
      *----------------------------------------------------------------*        
       01  RVGA01WP.                                                            
           05  EDGEN-CTABLEG2    PIC X(15).                                     
           05  EDGEN-CTABLEG2-REDEF REDEFINES EDGEN-CTABLEG2.                   
               10  EDGEN-CETAT           PIC X(10).                             
               10  EDGEN-NSEQ            PIC X(02).                             
               10  EDGEN-NSEQ-N         REDEFINES EDGEN-NSEQ                    
                                         PIC 9(02).                             
           05  EDGEN-WTABLEG     PIC X(80).                                     
           05  EDGEN-WTABLEG-REDEF  REDEFINES EDGEN-WTABLEG.                    
               10  EDGEN-NBLG            PIC X(02).                             
               10  EDGEN-NBLG-N         REDEFINES EDGEN-NBLG                    
                                         PIC 9(02).                             
               10  EDGEN-LIBL            PIC X(40).                             
               10  EDGEN-NBLGB           PIC X(02).                             
               10  EDGEN-NBLGB-N        REDEFINES EDGEN-NBLGB                   
                                         PIC 9(02).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01WP-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EDGEN-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EDGEN-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EDGEN-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EDGEN-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
