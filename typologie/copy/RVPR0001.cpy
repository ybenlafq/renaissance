      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVPR0001                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVPR0001                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPR0001.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPR0001.                                                            
      *}                                                                        
           02  PR00-CPRESTATION                                                 
               PIC X(0005).                                                     
           02  PR00-LPRESTATION                                                 
               PIC X(0020).                                                     
           02  PR00-CTYPPREST                                                   
               PIC X(0005).                                                     
           02  PR00-CHEFPROD                                                    
               PIC X(0005).                                                     
           02  PR00-NENTCDE                                                     
               PIC X(0005).                                                     
           02  PR00-CTAUXTVA                                                    
               PIC X(0005).                                                     
           02  PR00-CGRPGEO                                                     
               PIC X(0005).                                                     
           02  PR00-CCOMPTA                                                     
               PIC X(0005).                                                     
           02  PR00-CAGREPRE                                                    
               PIC X(0005).                                                     
           02  PR00-CCONCRET                                                    
               PIC X(0005).                                                     
           02  PR00-NORDRE                                                      
               PIC S9(3) COMP-3.                                                
           02  PR00-CSIGNE                                                      
               PIC X(0001).                                                     
           02  PR00-WPRODMAJ                                                    
               PIC X(0001).                                                     
           02  PR00-WGROUPE                                                     
               PIC X(0001).                                                     
           02  PR00-DMAJ                                                        
               PIC X(0008).                                                     
           02  PR00-DCREATION                                                   
               PIC X(0008).                                                     
           02  PR00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  PR00-CNATPREST                                                   
               PIC X(0002).                                                     
           02  PR00-CFAM                                                        
               PIC X(0005).                                                     
           02  PR00-CMARQ                                                       
               PIC X(0005).                                                     
           02  PR00-CASSORT                                                     
               PIC X(0005).                                                     
           02  PR00-LPRESTC                                                     
               PIC X(0005).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVPR0001                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPR0001-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPR0001-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR00-CPRESTATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR00-CPRESTATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR00-LPRESTATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR00-LPRESTATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR00-CTYPPREST-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR00-CTYPPREST-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR00-CHEFPROD-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR00-CHEFPROD-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR00-NENTCDE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR00-NENTCDE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR00-CTAUXTVA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR00-CTAUXTVA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR00-CGRPGEO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR00-CGRPGEO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR00-CCOMPTA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR00-CCOMPTA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR00-CAGREPRE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR00-CAGREPRE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR00-CCONCRET-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR00-CCONCRET-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR00-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR00-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR00-CSIGNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR00-CSIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR00-WPRODMAJ-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR00-WPRODMAJ-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR00-WGROUPE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR00-WGROUPE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR00-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR00-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR00-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR00-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR00-CNATPREST-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR00-CNATPREST-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR00-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR00-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR00-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR00-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR00-CASSORT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR00-CASSORT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PR00-LPRESTC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PR00-LPRESTC-F                                                   
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
