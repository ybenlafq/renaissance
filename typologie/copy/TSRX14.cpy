      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-RX14-LONG             PIC S9(4) COMP-3 VALUE +1027.               
       01  TS-RX14-RECORD.                                                      
           05 TS-RX14-LIGNE                OCCURS 13.                           
              10 TS-RX14-NCONC      PIC X(4).                                   
              10 TS-RX14-NCONCZP    PIC X(2).                                   
              10 TS-RX14-LENSCONC   PIC X(15).                                  
              10 TS-RX14-LEMPCONC   PIC X(15).                                  
              10 TS-RX14-NSEQUENCE  PIC X(1).                                   
              10 TS-RX14-CRAYON     PIC X(5).                                   
              10 TS-RX14-NMAG       PIC X(3).                                   
              10 TS-RX14-NMAGZP     PIC X(2).                                   
              10 TS-RX14-LMAG       PIC X(10).                                  
              10 TS-RX14-NFREQUENCE PIC X(1).                                   
              10 TS-RX14-DATE       PIC X(10).                                  
              10 TS-RX14-JOUR       PIC X(3).                                   
              10 TS-RX14-ACTIF      PIC X(1).                                   
              10 TS-RX14-DSYST      PIC S9(13) COMP-3.                          
                                                                                
