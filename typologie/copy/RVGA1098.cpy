      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGA1098 VUE SUR LA GA10 GROUPE                      
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA1098                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGA1098.                                                            
           02  GA10-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GA10-NLIEU                                                       
               PIC X(0003).                                                     
           02  GA10-CTYPLIEU                                                    
               PIC X(0001).                                                     
           02  GA10-LLIEU                                                       
               PIC X(0020).                                                     
           02  GA10-DOUV                                                        
               PIC X(0008).                                                     
           02  GA10-DFERM                                                       
               PIC X(0008).                                                     
           02  GA10-NZONPRIX                                                    
               PIC X(0002).                                                     
           02  GA10-CGRPMAG                                                     
               PIC X(0002).                                                     
           02  GA10-CSTATEXPO                                                   
               PIC X(0005).                                                     
           02  GA10-CACID                                                       
               PIC X(0004).                                                     
           02  GA10-WDACEM                                                      
               PIC X(0001).                                                     
           02  GA10-CTYPFACT                                                    
               PIC X(0005).                                                     
           02  GA10-CTYPCONTR                                                   
               PIC X(0005).                                                     
           02  GA10-QTAUXESCPT                                                  
               PIC S9(3)V9(0002) COMP-3.                                        
           02  GA10-LCONDRGLT                                                   
               PIC X(0020).                                                     
           02  GA10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GA10-LADRLIEU1                                                   
               PIC X(0032).                                                     
           02  GA10-LADRLIEU2                                                   
               PIC X(0032).                                                     
           02  GA10-LADRLIEU3                                                   
               PIC X(0032).                                                     
           02  GA10-CPOSTAL                                                     
               PIC X(0005).                                                     
           02  GA10-LCOMMUNE                                                    
               PIC X(0032).                                                     
           02  GA10-CTYPSOC                                                     
               PIC X(0003).                                                     
           02  GA10-NSOCCOMPT                                                   
               PIC X(0003).                                                     
           02  GA10-NLIEUCOMPT                                                  
               PIC X(0003).                                                     
           02  GA10-NSOCFACT                                                    
               PIC X(0003).                                                     
           02  GA10-NLIEUFACT                                                   
               PIC X(0003).                                                     
           02  GA10-CGRPMUT                                                     
               PIC X(0002).                                                     
           02  GA10-NSOCVALO                                                    
               PIC X(0003).                                                     
           02  GA10-NLIEUVALO                                                   
               PIC X(0003).                                                     
           02  GA10-CTYPLIEUVALO                                                
               PIC X(0001).                                                     
           02  GA10-NSOCGRP                                                     
               PIC X(0003).                                                     
           02  GA10-NORDRE                                                      
               PIC X(0003).                                                     
           02  GA10-CACID1                                                      
               PIC X(0004).                                                     
           02  GA10-CACID2                                                      
               PIC X(0004).                                                     
           02  GA10-CACID3                                                      
               PIC X(0004).                                                     
           02  GA10-CACID4                                                      
               PIC X(0004).                                                     
           02  GA10-CACID5                                                      
               PIC X(0004).                                                     
           02  GA10-CACID6                                                      
               PIC X(0004).                                                     
           02  GA10-CFRAIS                                                      
               PIC X(0006).                                                     
           02  GA10-SECTANA                                                     
               PIC X(0006).                                                     
           02  GA10-CCUMCA                                                      
               PIC X(0001).                                                     
           02  GA10-NZPPREST                                                    
               PIC X(0002).                                                     
           02  GA10-WRETRO                                                      
               PIC X(0001).                                                     
           02  GA10-TIERSGCT                                                    
               PIC X(0006).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGA1098                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGA1098-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-CTYPLIEU-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-CTYPLIEU-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-LLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-LLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-DOUV-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-DOUV-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-DFERM-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-DFERM-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-NZONPRIX-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-NZONPRIX-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-CGRPMAG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-CGRPMAG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-CSTATEXPO-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-CSTATEXPO-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-CACID-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-CACID-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-WDACEM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-WDACEM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-CTYPFACT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-CTYPFACT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-CTYPCONTR-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-CTYPCONTR-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-QTAUXESCPT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-QTAUXESCPT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-LCONDRGLT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-LCONDRGLT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-LADRLIEU1-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-LADRLIEU1-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-LADRLIEU2-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-LADRLIEU2-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-LADRLIEU3-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-LADRLIEU3-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-CPOSTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-LCOMMUNE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-LCOMMUNE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-CTYPSOC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-CTYPSOC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-NSOCCOMPT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-NSOCCOMPT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-NLIEUCOMPT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-NLIEUCOMPT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-NSOCFACT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-NSOCFACT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-NLIEUFACT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-NLIEUFACT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-CGRPMUT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-CGRPMUT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-NSOCVALO-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-NSOCVALO-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-NLIEUVALO-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-NLIEUVALO-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-CTYPLIEUVALO-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-CTYPLIEUVALO-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-NSOCGRP-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-NSOCGRP-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-CACID1-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-CACID1-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-CACID2-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-CACID2-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-CACID3-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-CACID3-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-CACID4-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-CACID4-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-CACID5-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-CACID5-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-CACID6-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-CACID6-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-CFRAIS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-CFRAIS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-SECTANA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-SECTANA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-CCUMCA-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-CCUMCA-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-NZPPREST-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-NZPPREST-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-WRETRO-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-WRETRO-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA10-TIERSGCT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA10-TIERSGCT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
