      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * TS SPECIFIQUE TABLE RTGG08                                 *            
      *       TR : GG00  GESTION DES GAMMES MENU                   *            
      *       PG : TGG08 MAJ DES PRIX ET COMMISSIONS ARTICLE       *            
      ******************************************************************        
      * DSA057 23/05/05 SUPPORT EVOLUTION FP943                                 
      *                 STOCKAGE PRIME ADAPTEE                                  
      ******************************************************************        
      *01  TSB-LONG            PIC S9(4) COMP-3 VALUE 350.                      
       01  TSB-LONG            PIC S9(4) COMP-3 VALUE 354.                      
       01  TSB-DONNEES.                                                         
           05 TSB-LIGNE   OCCURS 10.                                            
      * ZONES PRIX ARTICLE ET COMMISSION SAISIS ------------------              
              10  TSB-PRIXN            PIC S9(7)V99   COMP-3.                   
              10  TSB-LCOMM            PIC X(10).                               
              10  TSB-PCOMMN           PIC S9(4)V99   COMP-3.                   
              10  TSB-PCOMMADP         PIC S9(5)V99   COMP-3.                   
              10  TSB-DPRIX            PIC X(8).                                
              10  TSB-DCOMM            PIC X(8).                                
                                                                                
