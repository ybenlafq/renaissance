      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PPMAG MAGASINS EXCLUS DE L APPLI.      *        
      *----------------------------------------------------------------*        
       01  RVGA01QC.                                                            
           05  PPMAG-CTABLEG2    PIC X(15).                                     
           05  PPMAG-CTABLEG2-REDEF REDEFINES PPMAG-CTABLEG2.                   
               10  PPMAG-NSOCIETE        PIC X(03).                             
               10  PPMAG-NLIEU           PIC X(03).                             
           05  PPMAG-WTABLEG     PIC X(80).                                     
           05  PPMAG-WTABLEG-REDEF  REDEFINES PPMAG-WTABLEG.                    
               10  PPMAG-WACTIF          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01QC-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PPMAG-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PPMAG-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PPMAG-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PPMAG-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
