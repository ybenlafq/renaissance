      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      ******************************************************************        
      *               F I C H E   D E S C R I P T I V E                *        
      ******************************************************************        
      *                                                                *        
      *  PROJET      : SUIVI DU LIEN NEM / MONETIQUE                   *        
      *  COPY        : TSBS71 : TS UTILISEE DANS L'ECRAN BS71          *        
      *  CREATION    : 17/08/2005                                      *        
      ******************************************************************        
      ******************************************************************        
      *                                                                         
          01 TS-BS71-NOM.                                                       
             03 FILLER        PIC X(4) VALUE 'BS71'.                            
             03 TS-BS71-TERM  PIC X(4) VALUE SPACES.                            
      *                                                                         
          01 TS-BS71-DATA.                                                      
             03 TS-BS71-LIGNE OCCURS 10.                                        
                05 TS-BS71-DATED   PIC X(10)   VALUE SPACES.                    
                05 TS-BS71-DATEF   PIC X(10)   VALUE SPACES.                    
                05 TS-BS71-HEURED  PIC X(05)   VALUE SPACES.                    
                05 TS-BS71-HEUREF  PIC X(05)   VALUE SPACES.                    
                05 TS-BS71-CONTPS  PIC X(06)   VALUE SPACES.                    
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   01 TS-BS71-LONG     PIC S9(4) COMP  VALUE +360.                       
      *--                                                                       
          01 TS-BS71-LONG     PIC S9(4) COMP-5  VALUE +360.                     
      *}                                                                        
      *-------------------------------------- LONGUEUR = 10 * 36                
      *                                                                         
                                                                                
