      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CRCTR REFERENCEMENT DES CONTROLES      *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCRCTR.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCRCTR.                                                             
      *}                                                                        
           05  CRCTR-CTABLEG2    PIC X(15).                                     
           05  CRCTR-CTABLEG2-REDEF REDEFINES CRCTR-CTABLEG2.                   
               10  CRCTR-CCTRL           PIC X(05).                             
           05  CRCTR-WTABLEG     PIC X(80).                                     
           05  CRCTR-WTABLEG-REDEF  REDEFINES CRCTR-WTABLEG.                    
               10  CRCTR-WACTIF          PIC X(01).                             
               10  CRCTR-LCTRL           PIC X(50).                             
               10  CRCTR-CFORMAT         PIC X(02).                             
               10  CRCTR-QLONG           PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  CRCTR-QLONG-N        REDEFINES CRCTR-QLONG                   
                                         PIC 9(02).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  CRCTR-QLONGDEC        PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  CRCTR-QLONGDEC-N     REDEFINES CRCTR-QLONGDEC                
                                         PIC 9(02).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCRCTR-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCRCTR-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CRCTR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CRCTR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CRCTR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CRCTR-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
