      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ELI00   ELI00                                              00000020
      ***************************************************************** 00000030
       01   ELI00I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNSOCIETL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCIETF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNSOCIETI      PIC X(3).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNLIEUI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPSOCL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MTYPSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPSOCF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MTYPSOCI  PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLLIEUI   PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPLIEUL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MTYPLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPLIEUF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MTYPLIEUI      PIC X.                                     00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCGRPL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MSOCGRPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCGRPF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSOCGRPI  PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTRIL     COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MTRIL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTRIF     PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MTRII     PIC X.                                          00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MZONCMDI  PIC X(15).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLIBERRI  PIC X(58).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCODTRAI  PIC X(4).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCICSI    PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNETNAMI  PIC X(8).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MSCREENI  PIC X(4).                                       00000650
      ***************************************************************** 00000660
      * SDF: ELI00   ELI00                                              00000670
      ***************************************************************** 00000680
       01   ELI00O REDEFINES ELI00I.                                    00000690
           02 FILLER    PIC X(12).                                      00000700
           02 FILLER    PIC X(2).                                       00000710
           02 MDATJOUA  PIC X.                                          00000720
           02 MDATJOUC  PIC X.                                          00000730
           02 MDATJOUP  PIC X.                                          00000740
           02 MDATJOUH  PIC X.                                          00000750
           02 MDATJOUV  PIC X.                                          00000760
           02 MDATJOUO  PIC X(10).                                      00000770
           02 FILLER    PIC X(2).                                       00000780
           02 MTIMJOUA  PIC X.                                          00000790
           02 MTIMJOUC  PIC X.                                          00000800
           02 MTIMJOUP  PIC X.                                          00000810
           02 MTIMJOUH  PIC X.                                          00000820
           02 MTIMJOUV  PIC X.                                          00000830
           02 MTIMJOUO  PIC X(5).                                       00000840
           02 FILLER    PIC X(2).                                       00000850
           02 MNSOCIETA      PIC X.                                     00000860
           02 MNSOCIETC PIC X.                                          00000870
           02 MNSOCIETP PIC X.                                          00000880
           02 MNSOCIETH PIC X.                                          00000890
           02 MNSOCIETV PIC X.                                          00000900
           02 MNSOCIETO      PIC X(3).                                  00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MNLIEUA   PIC X.                                          00000930
           02 MNLIEUC   PIC X.                                          00000940
           02 MNLIEUP   PIC X.                                          00000950
           02 MNLIEUH   PIC X.                                          00000960
           02 MNLIEUV   PIC X.                                          00000970
           02 MNLIEUO   PIC X(3).                                       00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MTYPSOCA  PIC X.                                          00001000
           02 MTYPSOCC  PIC X.                                          00001010
           02 MTYPSOCP  PIC X.                                          00001020
           02 MTYPSOCH  PIC X.                                          00001030
           02 MTYPSOCV  PIC X.                                          00001040
           02 MTYPSOCO  PIC X(3).                                       00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MLLIEUA   PIC X.                                          00001070
           02 MLLIEUC   PIC X.                                          00001080
           02 MLLIEUP   PIC X.                                          00001090
           02 MLLIEUH   PIC X.                                          00001100
           02 MLLIEUV   PIC X.                                          00001110
           02 MLLIEUO   PIC X(20).                                      00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MTYPLIEUA      PIC X.                                     00001140
           02 MTYPLIEUC PIC X.                                          00001150
           02 MTYPLIEUP PIC X.                                          00001160
           02 MTYPLIEUH PIC X.                                          00001170
           02 MTYPLIEUV PIC X.                                          00001180
           02 MTYPLIEUO      PIC X.                                     00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MSOCGRPA  PIC X.                                          00001210
           02 MSOCGRPC  PIC X.                                          00001220
           02 MSOCGRPP  PIC X.                                          00001230
           02 MSOCGRPH  PIC X.                                          00001240
           02 MSOCGRPV  PIC X.                                          00001250
           02 MSOCGRPO  PIC X(3).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTRIA     PIC X.                                          00001280
           02 MTRIC     PIC X.                                          00001290
           02 MTRIP     PIC X.                                          00001300
           02 MTRIH     PIC X.                                          00001310
           02 MTRIV     PIC X.                                          00001320
           02 MTRIO     PIC X.                                          00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MZONCMDA  PIC X.                                          00001350
           02 MZONCMDC  PIC X.                                          00001360
           02 MZONCMDP  PIC X.                                          00001370
           02 MZONCMDH  PIC X.                                          00001380
           02 MZONCMDV  PIC X.                                          00001390
           02 MZONCMDO  PIC X(15).                                      00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MLIBERRA  PIC X.                                          00001420
           02 MLIBERRC  PIC X.                                          00001430
           02 MLIBERRP  PIC X.                                          00001440
           02 MLIBERRH  PIC X.                                          00001450
           02 MLIBERRV  PIC X.                                          00001460
           02 MLIBERRO  PIC X(58).                                      00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MCODTRAA  PIC X.                                          00001490
           02 MCODTRAC  PIC X.                                          00001500
           02 MCODTRAP  PIC X.                                          00001510
           02 MCODTRAH  PIC X.                                          00001520
           02 MCODTRAV  PIC X.                                          00001530
           02 MCODTRAO  PIC X(4).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MCICSA    PIC X.                                          00001560
           02 MCICSC    PIC X.                                          00001570
           02 MCICSP    PIC X.                                          00001580
           02 MCICSH    PIC X.                                          00001590
           02 MCICSV    PIC X.                                          00001600
           02 MCICSO    PIC X(5).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MNETNAMA  PIC X.                                          00001630
           02 MNETNAMC  PIC X.                                          00001640
           02 MNETNAMP  PIC X.                                          00001650
           02 MNETNAMH  PIC X.                                          00001660
           02 MNETNAMV  PIC X.                                          00001670
           02 MNETNAMO  PIC X(8).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MSCREENA  PIC X.                                          00001700
           02 MSCREENC  PIC X.                                          00001710
           02 MSCREENP  PIC X.                                          00001720
           02 MSCREENH  PIC X.                                          00001730
           02 MSCREENV  PIC X.                                          00001740
           02 MSCREENO  PIC X(4).                                       00001750
                                                                                
