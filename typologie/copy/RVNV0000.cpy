      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      **********************************************************                
      *   COPY DE LA TABLE RVNV0000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVNV0000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVNV0000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVNV0000.                                                            
      *}                                                                        
           02  NV00-NSEQ                                                        
               PIC S9(18) COMP-3.                                               
           02  NV00-DTD                                                         
               PIC X(0020).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
           02  NV00-CLE                                                         
               PIC X(0255).                                                     
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           02  NV00-EVENEMENT                                                   
               PIC X(0001).                                                     
           02  NV00-STATUT                                                      
               PIC X(0010).                                                     
           02  NV00-IDENTIFIANT                                                 
               PIC X(0012).                                                     
           02  NV00-TIMESTAMP                                                   
               PIC X(0026).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVNV0000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVNV0000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVNV0000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  NV00-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  NV00-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  NV00-DTD-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  NV00-DTD-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  NV00-CLE-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  NV00-CLE-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  NV00-EVENEMENT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  NV00-EVENEMENT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  NV00-STATUT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  NV00-STATUT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  NV00-IDENTIFIANT-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  NV00-IDENTIFIANT-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  NV00-TIMESTAMP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  NV00-TIMESTAMP-F                                                 
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *                                                                         
                                                                                
