      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      *        IDENTIFICATION SERVICE - CODES DESCRIPTIFS             * 00000020
      ***************************************************************** 00000030
      *                                                               * 00000040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS-54-LONG          PIC S9(4) COMP VALUE +288.               00000050
      *--                                                                       
       01  TS-54-LONG          PIC S9(4) COMP-5 VALUE +288.                     
      *}                                                                        
       01  TS-54-DATA.                                                  00000060
           05 TS-54-LIGNE.                                              00000070
              10 TS-POSTE-TSSE54    OCCURS 2.                           00000080
                 15 TS-54-PRMPAC                 PIC S9(7)V99 COMP-3.   00000090
                 15 TS-54-PRMPAC-F               PIC X.                 00000100
                 15 TS-54-DPRMPAC                PIC X(8).              00000110
                 15 TS-54-NZONPRIX               PIC X(2).              00000120
                 15 TS-54-PRIXAC                 PIC S9(7)V99 COMP-3.   00000130
                 15 TS-54-PRIXAC-F               PIC X.                 00000140
                 15 TS-54-DATEPAC                PIC X(8).              00000150
                 15 TS-54-INTERAC                PIC S9(5)V99 COMP-3.   00000160
                 15 TS-54-INTERAC-F              PIC X.                 00000170
                 15 TS-54-DATEIAC                PIC X(8).              00000180
                 15 TS-54-DPRMPN-SAVE            PIC X(8).              00000190
                 15 TS-54-DATEN-SAVE             PIC X(8).              00000200
                 15 TS-54-DATA-AV.                                      00000210
                    20 TS-54-PRMPN-AV            PIC S9(7)V99 COMP-3.   00000220
                    20 TS-54-PRMPN-F-AV          PIC X.                 00000230
                    20 TS-54-DPRMPN-AV           PIC X(8).              00000240
                    20 TS-54-PRIXN-AV            PIC S9(7)V99 COMP-3.   00000250
                    20 TS-54-PRIXN-F-AV          PIC X.                 00000260
                    20 TS-54-INTERN-AV           PIC S9(5)V99 COMP-3.   00000270
                    20 TS-54-INTERN-F-AV         PIC X.                 00000280
                    20 TS-54-DATEN-AV            PIC X(8).              00000290
                    20 TS-54-MBU-AV              PIC S9(7)V99 COMP-3.   00000300
                    20 TS-54-MBU-AV-F            PIC X.                 00000310
                    20 TS-54-TMBU-AV             PIC S9(4)V9 COMP-3.    00000320
                 15 TS-54-DATA-AP.                                      00000330
                    20 TS-54-PRMPN-AP            PIC S9(7)V99 COMP-3.   00000340
                    20 TS-54-PRMPN-F-AP          PIC X.                 00000350
                    20 TS-54-DPRMPN-AP           PIC X(8).              00000360
                    20 TS-54-PRIXN-AP            PIC S9(7)V99 COMP-3.   00000370
                    20 TS-54-PRIXN-F-AP          PIC X.                 00000380
                    20 TS-54-INTERN-AP           PIC S9(5)V99 COMP-3.   00000390
                    20 TS-54-INTERN-F-AP         PIC X.                 00000400
                    20 TS-54-DATEN-AP            PIC X(8).              00000410
                    20 TS-54-MBU-AP              PIC S9(7)V99 COMP-3.   00000420
GA0303              20 TS-54-MBU-AP-F            PIC X.                 00000430
                    20 TS-54-TMBU-AP             PIC S9(4)V9 COMP-3.    00000440
                 15 TS-54-FLAG-MAJ.                                     00000450
                    20 TS-54-WMAJTS              PIC X(01).             00000460
                                                                                
