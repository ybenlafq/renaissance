      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGM080 AU 11/02/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,03,PD,A,                          *        
      *                           20,03,PD,A,                          *        
      *                           23,20,BI,A,                          *        
      *                           43,20,BI,A,                          *        
      *                           63,20,BI,A,                          *        
      *                           83,65,BI,A,                          *        
      *                           48,05,PD,A,                          *        
      *                           53,07,BI,A,                          *        
      *                           60,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGM080.                                                        
            05 NOMETAT-IGM080           PIC X(6) VALUE 'IGM080'.                
            05 RUPTURES-IGM080.                                                 
           10 IGM080-NSOCIETE           PIC X(03).                      007  003
           10 IGM080-NZONE              PIC X(02).                      010  002
           10 IGM080-CHEFPROD           PIC X(05).                      012  005
           10 IGM080-WSAUT              PIC S9(05)      COMP-3.         017  003
           10 IGM080-WSEQFAM            PIC S9(05)      COMP-3.         020  003
           10 IGM080-LVMARKET1          PIC X(20).                      023  020
           10 IGM080-LVMARKET2          PIC X(20).                      043  020
           10 IGM080-LVMARKET3          PIC X(20).                      063  020
           10 IGM080-CFAMMARK           PIC X(65).                      083  065
           10 IGM080-ZTRI               PIC S9(07)V9(2) COMP-3.         148  005
           10 IGM080-NCODIC             PIC X(07).                      153  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGM080-SEQUENCE           PIC S9(04) COMP.                160  002
      *--                                                                       
           10 IGM080-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGM080.                                                   
           10 IGM080-CFAM               PIC X(05).                      162  005
           10 IGM080-CMARQ              PIC X(05).                      167  005
           10 IGM080-CVDESCRIPTA        PIC X(05).                      172  005
           10 IGM080-CVDESCRIPTB        PIC X(05).                      177  005
           10 IGM080-DIFPRELEVE         PIC X(01).                      182  001
           10 IGM080-DRELEVE            PIC X(04).                      183  004
           10 IGM080-LCHEFPROD          PIC X(20).                      187  020
           10 IGM080-LCOMMENT           PIC X(02).                      207  002
           10 IGM080-LENSCONC           PIC X(15).                      209  015
           10 IGM080-LFAM               PIC X(20).                      224  020
           10 IGM080-LFAMILLE           PIC X(09).                      244  009
           10 IGM080-LMAGASIN           PIC X(20).                      253  020
           10 IGM080-LREFFOURN          PIC X(20).                      273  020
           10 IGM080-LSEGMENT1          PIC X(10).                      293  010
           10 IGM080-LSEGMENT2          PIC X(10).                      303  010
           10 IGM080-LSEGMENT3          PIC X(10).                      313  010
           10 IGM080-LSTATCOMP          PIC X(03).                      323  003
           10 IGM080-LZONPRIX           PIC X(20).                      326  020
           10 IGM080-NCODIC2            PIC X(07).                      346  007
           10 IGM080-NCONC              PIC X(04).                      353  004
           10 IGM080-NMAG               PIC X(03).                      357  003
           10 IGM080-NMAGASIN           PIC X(03).                      360  003
           10 IGM080-NZONPRIX           PIC X(02).                      363  002
           10 IGM080-OAM                PIC X(01).                      365  001
           10 IGM080-WANO               PIC X(01).                      366  001
           10 IGM080-WANOE              PIC X(01).                      367  001
           10 IGM080-WDERO              PIC X(01).                      368  001
           10 IGM080-WDEROE             PIC X(01).                      369  001
           10 IGM080-WD15               PIC X(01).                      370  001
           10 IGM080-WD2                PIC X(01).                      371  001
           10 IGM080-WEDIT              PIC X(01).                      372  001
           10 IGM080-WGROUPE            PIC X(01).                      373  001
           10 IGM080-WOA                PIC X(01).                      374  001
           10 IGM080-PCHT               PIC S9(07)V9(6) COMP-3.         375  007
           10 IGM080-PCOMMART           PIC S9(05)V9(2) COMP-3.         382  004
           10 IGM080-PCOMMARTE          PIC S9(05)V9(2) COMP-3.         386  004
           10 IGM080-PCOMMFAM           PIC S9(05)V9(2) COMP-3.         390  004
           10 IGM080-PCOMMSTD           PIC S9(05)V9(2) COMP-3.         394  004
           10 IGM080-PEXPTTC            PIC S9(07)V9(2) COMP-3.         398  005
           10 IGM080-PRELEVE            PIC S9(07)V9(2) COMP-3.         403  005
           10 IGM080-PSTDMAG            PIC S9(07)V9(2) COMP-3.         408  005
           10 IGM080-PSTDTTC            PIC S9(07)V9(2) COMP-3.         413  005
           10 IGM080-QSTOCKDEP          PIC S9(05)      COMP-3.         418  003
           10 IGM080-QSTOCKMAG          PIC S9(05)      COMP-3.         421  003
           10 IGM080-QTAUXTVA           PIC S9(02)V9(3) COMP-3.         424  003
           10 IGM080-QV1S               PIC S9(07)      COMP-3.         427  004
           10 IGM080-QV2S               PIC S9(07)      COMP-3.         431  004
           10 IGM080-QV3S               PIC S9(07)      COMP-3.         435  004
           10 IGM080-QV4S               PIC S9(07)      COMP-3.         439  004
           10 IGM080-SRP                PIC S9(07)V9(2) COMP-3.         443  005
           10 IGM080-ZTRI2              PIC S9(07)      COMP-3.         448  004
           10 IGM080-DEPF               PIC X(08).                      452  008
           10 IGM080-DFINEFFET          PIC X(08).                      460  008
            05 FILLER                      PIC X(045).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGM080-LONG           PIC S9(4)   COMP  VALUE +467.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGM080-LONG           PIC S9(4) COMP-5  VALUE +467.           
                                                                                
      *}                                                                        
