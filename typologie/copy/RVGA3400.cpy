      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGA3400                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA3400                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA3400.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA3400.                                                            
      *}                                                                        
           02  GA34-CNOMDOU                                                     
               PIC X(0008).                                                     
           02  GA34-LNOMDOU                                                     
               PIC X(0040).                                                     
           02  GA34-LLNOMDOU.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        49  GA34-LLNOMDOU-L                                              
      *            PIC S9(4) COMP.                                              
      *--                                                                       
               49  GA34-LLNOMDOU-L                                              
                   PIC S9(4) COMP-5.                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
               49  GA34-LLNOMDOU-V                                              
                   PIC X(0500).                                                 
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGA3400                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA3400-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA3400-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA34-CNOMDOU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA34-CNOMDOU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA34-LNOMDOU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA34-LNOMDOU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA34-LLNOMDOU-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA34-LLNOMDOU-F                                                  
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
