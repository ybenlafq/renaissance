      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GRTRQ TYPE DE RECEPTION A QUAI         *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01QB.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01QB.                                                            
      *}                                                                        
           05  GRTRQ-CTABLEG2    PIC X(15).                                     
           05  GRTRQ-CTABLEG2-REDEF REDEFINES GRTRQ-CTABLEG2.                   
               10  GRTRQ-CTRQ            PIC X(05).                             
           05  GRTRQ-WTABLEG     PIC X(80).                                     
           05  GRTRQ-WTABLEG-REDEF  REDEFINES GRTRQ-WTABLEG.                    
               10  GRTRQ-LTRQ            PIC X(15).                             
               10  GRTRQ-WTRQ            PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01QB-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01QB-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GRTRQ-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GRTRQ-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GRTRQ-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GRTRQ-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
