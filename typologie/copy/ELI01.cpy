      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ELI01   ELI01                                              00000020
      ***************************************************************** 00000030
       01   ELI01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNSOCIETL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCIETF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCIETI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNLIEUI   PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPSOCL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MTYPSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPSOCF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MTYPSOCI  PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLLIEUI   PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPLIEUL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MTYPLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPLIEUF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MTYPLIEUI      PIC X.                                     00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCGRPL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MSOCGRPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCGRPF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MSOCGRPI  PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTRIL     COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MTRIL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTRIF     PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MTRII     PIC X.                                          00000490
           02 MGA10I OCCURS   10 TIMES .                                00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCL   COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MSOCL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSOCF   PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MSOCI   PIC X(3).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEUL  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MLIEUL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLIEUF  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MLIEUI  PIC X(3).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBL   COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MLIBL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MLIBF   PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MLIBI   PIC X(20).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPL   COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MTYPL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MTYPF   PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MTYPI   PIC X(3).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLL     COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MLL COMP-5 PIC S9(4).                                           
      *}                                                                        
             03 MLF     PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MLI     PIC X.                                          00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MGRPL   COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MGRPL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MGRPF   PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MGRPI   PIC X(3).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNUML   COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MNUML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNUMF   PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MNUMI   PIC X(3).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMPTAL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MCOMPTAL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCOMPTAF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MCOMPTAI     PIC X(6).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCVALL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MSOCVALL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSOCVALF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MSOCVALI     PIC X(7).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCFACL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MSOCFACL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSOCFACF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MSOCFACI     PIC X(6).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPFACL     COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MTYPFACL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTYPFACF     PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MTYPFACI     PIC X(5).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFRAISL      COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MFRAISL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MFRAISF      PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MFRAISI      PIC X(5).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWRETROL     COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MWRETROL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWRETROF     PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MWRETROI     PIC X.                                     00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MZONCMDI  PIC X(15).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MLIBERRI  PIC X(58).                                      00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCODTRAI  PIC X(4).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCICSI    PIC X(5).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MNETNAMI  PIC X(8).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MSCREENI  PIC X(4).                                       00001260
      ***************************************************************** 00001270
      * SDF: ELI01   ELI01                                              00001280
      ***************************************************************** 00001290
       01   ELI01O REDEFINES ELI01I.                                    00001300
           02 FILLER    PIC X(12).                                      00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MDATJOUA  PIC X.                                          00001330
           02 MDATJOUC  PIC X.                                          00001340
           02 MDATJOUP  PIC X.                                          00001350
           02 MDATJOUH  PIC X.                                          00001360
           02 MDATJOUV  PIC X.                                          00001370
           02 MDATJOUO  PIC X(10).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MTIMJOUA  PIC X.                                          00001400
           02 MTIMJOUC  PIC X.                                          00001410
           02 MTIMJOUP  PIC X.                                          00001420
           02 MTIMJOUH  PIC X.                                          00001430
           02 MTIMJOUV  PIC X.                                          00001440
           02 MTIMJOUO  PIC X(5).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MPAGEA    PIC X.                                          00001470
           02 MPAGEC    PIC X.                                          00001480
           02 MPAGEP    PIC X.                                          00001490
           02 MPAGEH    PIC X.                                          00001500
           02 MPAGEV    PIC X.                                          00001510
           02 MPAGEO    PIC X(3).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MPAGEMAXA      PIC X.                                     00001540
           02 MPAGEMAXC PIC X.                                          00001550
           02 MPAGEMAXP PIC X.                                          00001560
           02 MPAGEMAXH PIC X.                                          00001570
           02 MPAGEMAXV PIC X.                                          00001580
           02 MPAGEMAXO      PIC X(3).                                  00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MNSOCIETA      PIC X.                                     00001610
           02 MNSOCIETC PIC X.                                          00001620
           02 MNSOCIETP PIC X.                                          00001630
           02 MNSOCIETH PIC X.                                          00001640
           02 MNSOCIETV PIC X.                                          00001650
           02 MNSOCIETO      PIC X(3).                                  00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MNLIEUA   PIC X.                                          00001680
           02 MNLIEUC   PIC X.                                          00001690
           02 MNLIEUP   PIC X.                                          00001700
           02 MNLIEUH   PIC X.                                          00001710
           02 MNLIEUV   PIC X.                                          00001720
           02 MNLIEUO   PIC X(3).                                       00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MTYPSOCA  PIC X.                                          00001750
           02 MTYPSOCC  PIC X.                                          00001760
           02 MTYPSOCP  PIC X.                                          00001770
           02 MTYPSOCH  PIC X.                                          00001780
           02 MTYPSOCV  PIC X.                                          00001790
           02 MTYPSOCO  PIC X(3).                                       00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MLLIEUA   PIC X.                                          00001820
           02 MLLIEUC   PIC X.                                          00001830
           02 MLLIEUP   PIC X.                                          00001840
           02 MLLIEUH   PIC X.                                          00001850
           02 MLLIEUV   PIC X.                                          00001860
           02 MLLIEUO   PIC X(20).                                      00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MTYPLIEUA      PIC X.                                     00001890
           02 MTYPLIEUC PIC X.                                          00001900
           02 MTYPLIEUP PIC X.                                          00001910
           02 MTYPLIEUH PIC X.                                          00001920
           02 MTYPLIEUV PIC X.                                          00001930
           02 MTYPLIEUO      PIC X.                                     00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MSOCGRPA  PIC X.                                          00001960
           02 MSOCGRPC  PIC X.                                          00001970
           02 MSOCGRPP  PIC X.                                          00001980
           02 MSOCGRPH  PIC X.                                          00001990
           02 MSOCGRPV  PIC X.                                          00002000
           02 MSOCGRPO  PIC X(3).                                       00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MTRIA     PIC X.                                          00002030
           02 MTRIC     PIC X.                                          00002040
           02 MTRIP     PIC X.                                          00002050
           02 MTRIH     PIC X.                                          00002060
           02 MTRIV     PIC X.                                          00002070
           02 MTRIO     PIC X.                                          00002080
           02 MGA10O OCCURS   10 TIMES .                                00002090
             03 FILLER       PIC X(2).                                  00002100
             03 MSOCA   PIC X.                                          00002110
             03 MSOCC   PIC X.                                          00002120
             03 MSOCP   PIC X.                                          00002130
             03 MSOCH   PIC X.                                          00002140
             03 MSOCV   PIC X.                                          00002150
             03 MSOCO   PIC X(3).                                       00002160
             03 FILLER       PIC X(2).                                  00002170
             03 MLIEUA  PIC X.                                          00002180
             03 MLIEUC  PIC X.                                          00002190
             03 MLIEUP  PIC X.                                          00002200
             03 MLIEUH  PIC X.                                          00002210
             03 MLIEUV  PIC X.                                          00002220
             03 MLIEUO  PIC X(3).                                       00002230
             03 FILLER       PIC X(2).                                  00002240
             03 MLIBA   PIC X.                                          00002250
             03 MLIBC   PIC X.                                          00002260
             03 MLIBP   PIC X.                                          00002270
             03 MLIBH   PIC X.                                          00002280
             03 MLIBV   PIC X.                                          00002290
             03 MLIBO   PIC X(20).                                      00002300
             03 FILLER       PIC X(2).                                  00002310
             03 MTYPA   PIC X.                                          00002320
             03 MTYPC   PIC X.                                          00002330
             03 MTYPP   PIC X.                                          00002340
             03 MTYPH   PIC X.                                          00002350
             03 MTYPV   PIC X.                                          00002360
             03 MTYPO   PIC X(3).                                       00002370
             03 FILLER       PIC X(2).                                  00002380
             03 MLA     PIC X.                                          00002390
             03 MLC     PIC X.                                          00002400
             03 MLP     PIC X.                                          00002410
             03 MLH     PIC X.                                          00002420
             03 MLV     PIC X.                                          00002430
             03 MLO     PIC X.                                          00002440
             03 FILLER       PIC X(2).                                  00002450
             03 MGRPA   PIC X.                                          00002460
             03 MGRPC   PIC X.                                          00002470
             03 MGRPP   PIC X.                                          00002480
             03 MGRPH   PIC X.                                          00002490
             03 MGRPV   PIC X.                                          00002500
             03 MGRPO   PIC X(3).                                       00002510
             03 FILLER       PIC X(2).                                  00002520
             03 MNUMA   PIC X.                                          00002530
             03 MNUMC   PIC X.                                          00002540
             03 MNUMP   PIC X.                                          00002550
             03 MNUMH   PIC X.                                          00002560
             03 MNUMV   PIC X.                                          00002570
             03 MNUMO   PIC X(3).                                       00002580
             03 FILLER       PIC X(2).                                  00002590
             03 MCOMPTAA     PIC X.                                     00002600
             03 MCOMPTAC     PIC X.                                     00002610
             03 MCOMPTAP     PIC X.                                     00002620
             03 MCOMPTAH     PIC X.                                     00002630
             03 MCOMPTAV     PIC X.                                     00002640
             03 MCOMPTAO     PIC X(6).                                  00002650
             03 FILLER       PIC X(2).                                  00002660
             03 MSOCVALA     PIC X.                                     00002670
             03 MSOCVALC     PIC X.                                     00002680
             03 MSOCVALP     PIC X.                                     00002690
             03 MSOCVALH     PIC X.                                     00002700
             03 MSOCVALV     PIC X.                                     00002710
             03 MSOCVALO     PIC X(7).                                  00002720
             03 FILLER       PIC X(2).                                  00002730
             03 MSOCFACA     PIC X.                                     00002740
             03 MSOCFACC     PIC X.                                     00002750
             03 MSOCFACP     PIC X.                                     00002760
             03 MSOCFACH     PIC X.                                     00002770
             03 MSOCFACV     PIC X.                                     00002780
             03 MSOCFACO     PIC X(6).                                  00002790
             03 FILLER       PIC X(2).                                  00002800
             03 MTYPFACA     PIC X.                                     00002810
             03 MTYPFACC     PIC X.                                     00002820
             03 MTYPFACP     PIC X.                                     00002830
             03 MTYPFACH     PIC X.                                     00002840
             03 MTYPFACV     PIC X.                                     00002850
             03 MTYPFACO     PIC X(5).                                  00002860
             03 FILLER       PIC X(2).                                  00002870
             03 MFRAISA      PIC X.                                     00002880
             03 MFRAISC PIC X.                                          00002890
             03 MFRAISP PIC X.                                          00002900
             03 MFRAISH PIC X.                                          00002910
             03 MFRAISV PIC X.                                          00002920
             03 MFRAISO      PIC X(5).                                  00002930
             03 FILLER       PIC X(2).                                  00002940
             03 MWRETROA     PIC X.                                     00002950
             03 MWRETROC     PIC X.                                     00002960
             03 MWRETROP     PIC X.                                     00002970
             03 MWRETROH     PIC X.                                     00002980
             03 MWRETROV     PIC X.                                     00002990
             03 MWRETROO     PIC X.                                     00003000
           02 FILLER    PIC X(2).                                       00003010
           02 MZONCMDA  PIC X.                                          00003020
           02 MZONCMDC  PIC X.                                          00003030
           02 MZONCMDP  PIC X.                                          00003040
           02 MZONCMDH  PIC X.                                          00003050
           02 MZONCMDV  PIC X.                                          00003060
           02 MZONCMDO  PIC X(15).                                      00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MLIBERRA  PIC X.                                          00003090
           02 MLIBERRC  PIC X.                                          00003100
           02 MLIBERRP  PIC X.                                          00003110
           02 MLIBERRH  PIC X.                                          00003120
           02 MLIBERRV  PIC X.                                          00003130
           02 MLIBERRO  PIC X(58).                                      00003140
           02 FILLER    PIC X(2).                                       00003150
           02 MCODTRAA  PIC X.                                          00003160
           02 MCODTRAC  PIC X.                                          00003170
           02 MCODTRAP  PIC X.                                          00003180
           02 MCODTRAH  PIC X.                                          00003190
           02 MCODTRAV  PIC X.                                          00003200
           02 MCODTRAO  PIC X(4).                                       00003210
           02 FILLER    PIC X(2).                                       00003220
           02 MCICSA    PIC X.                                          00003230
           02 MCICSC    PIC X.                                          00003240
           02 MCICSP    PIC X.                                          00003250
           02 MCICSH    PIC X.                                          00003260
           02 MCICSV    PIC X.                                          00003270
           02 MCICSO    PIC X(5).                                       00003280
           02 FILLER    PIC X(2).                                       00003290
           02 MNETNAMA  PIC X.                                          00003300
           02 MNETNAMC  PIC X.                                          00003310
           02 MNETNAMP  PIC X.                                          00003320
           02 MNETNAMH  PIC X.                                          00003330
           02 MNETNAMV  PIC X.                                          00003340
           02 MNETNAMO  PIC X(8).                                       00003350
           02 FILLER    PIC X(2).                                       00003360
           02 MSCREENA  PIC X.                                          00003370
           02 MSCREENC  PIC X.                                          00003380
           02 MSCREENP  PIC X.                                          00003390
           02 MSCREENH  PIC X.                                          00003400
           02 MSCREENV  PIC X.                                          00003410
           02 MSCREENO  PIC X(4).                                       00003420
                                                                                
