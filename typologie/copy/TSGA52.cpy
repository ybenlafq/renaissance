      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   TS : GARANTIES COMPLEMENTAIRES                              *         
      *****************************************************************         
      *                                                               *         
       01  TS-GA52.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GA52-LONG                 PIC S9(5) COMP-3                     
                                           VALUE +16.                           
           02 TS-GA52-DONNEES.                                                  
      *------------------------------TABLE GARANTIE COMPLEMENTAIRE              
              03 TS-GA52-TOPMAJ               PIC X(1).                         
              03 TS-GA52-NCODIC               PIC X(7).                         
              03 TS-GA52-CGARANTCPLT          PIC X(5).                         
              03 TS-GA52-CVGARANCPLT          PIC X(3).                         
      *                                                                         
                                                                                
