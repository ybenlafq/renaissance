      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PROPP COULEURS PROFIL PAR PROFIL       *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01NY.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01NY.                                                            
      *}                                                                        
           05  PROPP-CTABLEG2    PIC X(15).                                     
           05  PROPP-CTABLEG2-REDEF REDEFINES PROPP-CTABLEG2.                   
               10  PROPP-CODPRO          PIC X(10).                             
           05  PROPP-WTABLEG     PIC X(80).                                     
           05  PROPP-WTABLEG-REDEF  REDEFINES PROPP-WTABLEG.                    
               10  PROPP-CCOUL1          PIC X(05).                             
               10  PROPP-CCOUL2          PIC X(05).                             
               10  PROPP-CCOUL3          PIC X(05).                             
               10  PROPP-CCOUL4          PIC X(05).                             
               10  PROPP-CCOUL5          PIC X(05).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01NY-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01NY-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PROPP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PROPP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PROPP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PROPP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
