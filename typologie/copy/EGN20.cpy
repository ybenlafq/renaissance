      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGN20   EGN20                                              00000020
      ***************************************************************** 00000030
       01   EGN20I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(5).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(5).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(5).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(5).                                       00000200
           02 MPAGEMAXI      PIC X(4).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWACTIFL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MWACTIFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWACTIFF  PIC X.                                          00000230
           02 FILLER    PIC X(5).                                       00000240
           02 MWACTIFI  PIC X.                                          00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWACTIF2L      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MWACTIF2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWACTIF2F      PIC X.                                     00000270
           02 FILLER    PIC X(5).                                       00000280
           02 MWACTIF2I      PIC X.                                     00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWOAL     COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MWOAL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MWOAF     PIC X.                                          00000310
           02 FILLER    PIC X(5).                                       00000320
           02 MWOAI     PIC X.                                          00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATAPPL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MSTATAPPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSTATAPPF      PIC X.                                     00000350
           02 FILLER    PIC X(5).                                       00000360
           02 MSTATAPPI      PIC X.                                     00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODICRL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCODICRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODICRF  PIC X.                                          00000390
           02 FILLER    PIC X(5).                                       00000400
           02 MCODICRI  PIC X(7).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMRL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCFAMRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAMRF   PIC X.                                          00000430
           02 FILLER    PIC X(5).                                       00000440
           02 MCFAMRI   PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQRL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCMARQRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMARQRF  PIC X.                                          00000470
           02 FILLER    PIC X(5).                                       00000480
           02 MCMARQRI  PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURL     COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MLREFFOURL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLREFFOURF     PIC X.                                     00000510
           02 FILLER    PIC X(5).                                       00000520
           02 MLREFFOURI     PIC X(20).                                 00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBF10F11L    COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MLIBF10F11L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLIBF10F11F    PIC X.                                     00000550
           02 FILLER    PIC X(5).                                       00000560
           02 MLIBF10F11I    PIC X(33).                                 00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLE1L     COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MLIBELLE1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIBELLE1F     PIC X.                                     00000590
           02 FILLER    PIC X(5).                                       00000600
           02 MLIBELLE1I     PIC X(32).                                 00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLE2L     COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MLIBELLE2L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIBELLE2F     PIC X.                                     00000630
           02 FILLER    PIC X(5).                                       00000640
           02 MLIBELLE2I     PIC X(32).                                 00000650
           02 MCODICD OCCURS   15 TIMES .                               00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODICL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MCODICL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCODICF      PIC X.                                     00000680
             03 FILLER  PIC X(5).                                       00000690
             03 MCODICI      PIC X(7).                                  00000700
           02 MCFAMD OCCURS   15 TIMES .                                00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000720
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000730
             03 FILLER  PIC X(5).                                       00000740
             03 MCFAMI  PIC X(5).                                       00000750
           02 MCMARQD OCCURS   15 TIMES .                               00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000770
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000780
             03 FILLER  PIC X(5).                                       00000790
             03 MCMARQI      PIC X(5).                                  00000800
           02 MLREFFOUD OCCURS   15 TIMES .                             00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFFOUL    COMP PIC S9(4).                            00000820
      *--                                                                       
             03 MLREFFOUL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLREFFOUF    PIC X.                                     00000830
             03 FILLER  PIC X(5).                                       00000840
             03 MLREFFOUI    PIC X(20).                                 00000850
           02 MSTATUTD OCCURS   15 TIMES .                              00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTATUTL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MSTATUTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTATUTF     PIC X.                                     00000880
             03 FILLER  PIC X(5).                                       00000890
             03 MSTATUTI     PIC X(3).                                  00000900
           02 MDONNEESD OCCURS   15 TIMES .                             00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDONNEESL    COMP PIC S9(4).                            00000920
      *--                                                                       
             03 MDONNEESL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDONNEESF    PIC X.                                     00000930
             03 FILLER  PIC X(5).                                       00000940
             03 MDONNEESI    PIC X(32).                                 00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000970
           02 FILLER    PIC X(5).                                       00000980
           02 MLIBERRI  PIC X(78).                                      00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001010
           02 FILLER    PIC X(5).                                       00001020
           02 MCODTRAI  PIC X(4).                                       00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001040
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001050
           02 FILLER    PIC X(5).                                       00001060
           02 MCICSI    PIC X(5).                                       00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001080
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001090
           02 FILLER    PIC X(5).                                       00001100
           02 MNETNAMI  PIC X(8).                                       00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001120
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001130
           02 FILLER    PIC X(5).                                       00001140
           02 MSCREENI  PIC X(4).                                       00001150
      ***************************************************************** 00001160
      * SDF: EGN20   EGN20                                              00001170
      ***************************************************************** 00001180
       01   EGN20O REDEFINES EGN20I.                                    00001190
           02 FILLER    PIC X(12).                                      00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MDATJOUA  PIC X.                                          00001220
           02 MDATJOUC  PIC X.                                          00001230
           02 MDATJOUP  PIC X.                                          00001240
           02 MDATJOUH  PIC X.                                          00001250
           02 MDATJOUV  PIC X.                                          00001260
           02 MDATJOUU  PIC X.                                          00001270
           02 MDATJOUO  PIC X(10).                                      00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MTIMJOUA  PIC X.                                          00001300
           02 MTIMJOUC  PIC X.                                          00001310
           02 MTIMJOUP  PIC X.                                          00001320
           02 MTIMJOUH  PIC X.                                          00001330
           02 MTIMJOUV  PIC X.                                          00001340
           02 MTIMJOUU  PIC X.                                          00001350
           02 MTIMJOUO  PIC X(5).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MPAGEA    PIC X.                                          00001380
           02 MPAGEC    PIC X.                                          00001390
           02 MPAGEP    PIC X.                                          00001400
           02 MPAGEH    PIC X.                                          00001410
           02 MPAGEV    PIC X.                                          00001420
           02 MPAGEU    PIC X.                                          00001430
           02 MPAGEO    PIC X(3).                                       00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MPAGEMAXA      PIC X.                                     00001460
           02 MPAGEMAXC PIC X.                                          00001470
           02 MPAGEMAXP PIC X.                                          00001480
           02 MPAGEMAXH PIC X.                                          00001490
           02 MPAGEMAXV PIC X.                                          00001500
           02 MPAGEMAXU PIC X.                                          00001510
           02 MPAGEMAXO      PIC X(4).                                  00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MWACTIFA  PIC X.                                          00001540
           02 MWACTIFC  PIC X.                                          00001550
           02 MWACTIFP  PIC X.                                          00001560
           02 MWACTIFH  PIC X.                                          00001570
           02 MWACTIFV  PIC X.                                          00001580
           02 MWACTIFU  PIC X.                                          00001590
           02 MWACTIFO  PIC X.                                          00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MWACTIF2A      PIC X.                                     00001620
           02 MWACTIF2C PIC X.                                          00001630
           02 MWACTIF2P PIC X.                                          00001640
           02 MWACTIF2H PIC X.                                          00001650
           02 MWACTIF2V PIC X.                                          00001660
           02 MWACTIF2U PIC X.                                          00001670
           02 MWACTIF2O      PIC X.                                     00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MWOAA     PIC X.                                          00001700
           02 MWOAC     PIC X.                                          00001710
           02 MWOAP     PIC X.                                          00001720
           02 MWOAH     PIC X.                                          00001730
           02 MWOAV     PIC X.                                          00001740
           02 MWOAU     PIC X.                                          00001750
           02 MWOAO     PIC X.                                          00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MSTATAPPA      PIC X.                                     00001780
           02 MSTATAPPC PIC X.                                          00001790
           02 MSTATAPPP PIC X.                                          00001800
           02 MSTATAPPH PIC X.                                          00001810
           02 MSTATAPPV PIC X.                                          00001820
           02 MSTATAPPU PIC X.                                          00001830
           02 MSTATAPPO      PIC X.                                     00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MCODICRA  PIC X.                                          00001860
           02 MCODICRC  PIC X.                                          00001870
           02 MCODICRP  PIC X.                                          00001880
           02 MCODICRH  PIC X.                                          00001890
           02 MCODICRV  PIC X.                                          00001900
           02 MCODICRU  PIC X.                                          00001910
           02 MCODICRO  PIC X(7).                                       00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MCFAMRA   PIC X.                                          00001940
           02 MCFAMRC   PIC X.                                          00001950
           02 MCFAMRP   PIC X.                                          00001960
           02 MCFAMRH   PIC X.                                          00001970
           02 MCFAMRV   PIC X.                                          00001980
           02 MCFAMRU   PIC X.                                          00001990
           02 MCFAMRO   PIC X(5).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MCMARQRA  PIC X.                                          00002020
           02 MCMARQRC  PIC X.                                          00002030
           02 MCMARQRP  PIC X.                                          00002040
           02 MCMARQRH  PIC X.                                          00002050
           02 MCMARQRV  PIC X.                                          00002060
           02 MCMARQRU  PIC X.                                          00002070
           02 MCMARQRO  PIC X(5).                                       00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MLREFFOURA     PIC X.                                     00002100
           02 MLREFFOURC     PIC X.                                     00002110
           02 MLREFFOURP     PIC X.                                     00002120
           02 MLREFFOURH     PIC X.                                     00002130
           02 MLREFFOURV     PIC X.                                     00002140
           02 MLREFFOURU     PIC X.                                     00002150
           02 MLREFFOURO     PIC X(20).                                 00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MLIBF10F11A    PIC X.                                     00002180
           02 MLIBF10F11C    PIC X.                                     00002190
           02 MLIBF10F11P    PIC X.                                     00002200
           02 MLIBF10F11H    PIC X.                                     00002210
           02 MLIBF10F11V    PIC X.                                     00002220
           02 MLIBF10F11U    PIC X.                                     00002230
           02 MLIBF10F11O    PIC X(33).                                 00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MLIBELLE1A     PIC X.                                     00002260
           02 MLIBELLE1C     PIC X.                                     00002270
           02 MLIBELLE1P     PIC X.                                     00002280
           02 MLIBELLE1H     PIC X.                                     00002290
           02 MLIBELLE1V     PIC X.                                     00002300
           02 MLIBELLE1U     PIC X.                                     00002310
           02 MLIBELLE1O     PIC X(32).                                 00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MLIBELLE2A     PIC X.                                     00002340
           02 MLIBELLE2C     PIC X.                                     00002350
           02 MLIBELLE2P     PIC X.                                     00002360
           02 MLIBELLE2H     PIC X.                                     00002370
           02 MLIBELLE2V     PIC X.                                     00002380
           02 MLIBELLE2U     PIC X.                                     00002390
           02 MLIBELLE2O     PIC X(32).                                 00002400
           02 DFHMS1 OCCURS   15 TIMES .                                00002410
             03 FILLER       PIC X(2).                                  00002420
             03 MCODICA      PIC X.                                     00002430
             03 MCODICC PIC X.                                          00002440
             03 MCODICP PIC X.                                          00002450
             03 MCODICH PIC X.                                          00002460
             03 MCODICV PIC X.                                          00002470
             03 MCODICU PIC X.                                          00002480
             03 MCODICO      PIC X(7).                                  00002490
           02 DFHMS2 OCCURS   15 TIMES .                                00002500
             03 FILLER       PIC X(2).                                  00002510
             03 MCFAMA  PIC X.                                          00002520
             03 MCFAMC  PIC X.                                          00002530
             03 MCFAMP  PIC X.                                          00002540
             03 MCFAMH  PIC X.                                          00002550
             03 MCFAMV  PIC X.                                          00002560
             03 MCFAMU  PIC X.                                          00002570
             03 MCFAMO  PIC X(5).                                       00002580
           02 DFHMS3 OCCURS   15 TIMES .                                00002590
             03 FILLER       PIC X(2).                                  00002600
             03 MCMARQA      PIC X.                                     00002610
             03 MCMARQC PIC X.                                          00002620
             03 MCMARQP PIC X.                                          00002630
             03 MCMARQH PIC X.                                          00002640
             03 MCMARQV PIC X.                                          00002650
             03 MCMARQU PIC X.                                          00002660
             03 MCMARQO      PIC X(5).                                  00002670
           02 DFHMS4 OCCURS   15 TIMES .                                00002680
             03 FILLER       PIC X(2).                                  00002690
             03 MLREFFOUA    PIC X.                                     00002700
             03 MLREFFOUC    PIC X.                                     00002710
             03 MLREFFOUP    PIC X.                                     00002720
             03 MLREFFOUH    PIC X.                                     00002730
             03 MLREFFOUV    PIC X.                                     00002740
             03 MLREFFOUU    PIC X.                                     00002750
             03 MLREFFOUO    PIC X(20).                                 00002760
           02 DFHMS5 OCCURS   15 TIMES .                                00002770
             03 FILLER       PIC X(2).                                  00002780
             03 MSTATUTA     PIC X.                                     00002790
             03 MSTATUTC     PIC X.                                     00002800
             03 MSTATUTP     PIC X.                                     00002810
             03 MSTATUTH     PIC X.                                     00002820
             03 MSTATUTV     PIC X.                                     00002830
             03 MSTATUTU     PIC X.                                     00002840
             03 MSTATUTO     PIC X(3).                                  00002850
           02 DFHMS6 OCCURS   15 TIMES .                                00002860
             03 FILLER       PIC X(2).                                  00002870
             03 MDONNEESA    PIC X.                                     00002880
             03 MDONNEESC    PIC X.                                     00002890
             03 MDONNEESP    PIC X.                                     00002900
             03 MDONNEESH    PIC X.                                     00002910
             03 MDONNEESV    PIC X.                                     00002920
             03 MDONNEESU    PIC X.                                     00002930
             03 MDONNEESO    PIC X(32).                                 00002940
           02 FILLER    PIC X(2).                                       00002950
           02 MLIBERRA  PIC X.                                          00002960
           02 MLIBERRC  PIC X.                                          00002970
           02 MLIBERRP  PIC X.                                          00002980
           02 MLIBERRH  PIC X.                                          00002990
           02 MLIBERRV  PIC X.                                          00003000
           02 MLIBERRU  PIC X.                                          00003010
           02 MLIBERRO  PIC X(78).                                      00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MCODTRAA  PIC X.                                          00003040
           02 MCODTRAC  PIC X.                                          00003050
           02 MCODTRAP  PIC X.                                          00003060
           02 MCODTRAH  PIC X.                                          00003070
           02 MCODTRAV  PIC X.                                          00003080
           02 MCODTRAU  PIC X.                                          00003090
           02 MCODTRAO  PIC X(4).                                       00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MCICSA    PIC X.                                          00003120
           02 MCICSC    PIC X.                                          00003130
           02 MCICSP    PIC X.                                          00003140
           02 MCICSH    PIC X.                                          00003150
           02 MCICSV    PIC X.                                          00003160
           02 MCICSU    PIC X.                                          00003170
           02 MCICSO    PIC X(5).                                       00003180
           02 FILLER    PIC X(2).                                       00003190
           02 MNETNAMA  PIC X.                                          00003200
           02 MNETNAMC  PIC X.                                          00003210
           02 MNETNAMP  PIC X.                                          00003220
           02 MNETNAMH  PIC X.                                          00003230
           02 MNETNAMV  PIC X.                                          00003240
           02 MNETNAMU  PIC X.                                          00003250
           02 MNETNAMO  PIC X(8).                                       00003260
           02 FILLER    PIC X(2).                                       00003270
           02 MSCREENA  PIC X.                                          00003280
           02 MSCREENC  PIC X.                                          00003290
           02 MSCREENP  PIC X.                                          00003300
           02 MSCREENH  PIC X.                                          00003310
           02 MSCREENV  PIC X.                                          00003320
           02 MSCREENU  PIC X.                                          00003330
           02 MSCREENO  PIC X(4).                                       00003340
                                                                                
