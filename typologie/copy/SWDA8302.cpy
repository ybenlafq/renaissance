      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      * *************************  DESCRIPTION  ************************        
      *****************************  WDA8302  **************************        
      *----------------------------------------------------------------*        
      *                            LONGUEUR 30         *                        
      *   ATTENTION PRESENT AUSSI SUR DSA.TEST.SOURCE  *                        
      ******************************************************************        
       01 SWDA8302-DSECT.                                               00010000
           03  WDA8302-CLEF.                                            00020000
           04  WDA8302-CLEF1.                                           00020000
      *                                          SOCIETE                        
           05  WDA8302-NSOCIETE         PIC  X(03).                     00020000
      *                                          NLIEU                          
           05  WDA8302-NLIEU            PIC  X(03).                     00030000
      *                                DATE DE VENTE AU SENS COMMERCIALE        
           05  WDA8302-DVENTECIALE      PIC  X(08).                     00040000
      *                                          NOMBRE DE PIECES               
           04  WDA8302-QPIECES          PIC  S9(05)     COMP-3.         00050000
      *                                          CA TTC                         
           03  WDA8302-CATTC            PIC  S9(09)V99  COMP-3.         00060000
      *                                          NOMBRE DE TRANSACTIONS         
           03  WDA8302-NBTRAN           PIC  S9(07)     COMP-3.         00050000
           03  WDA8302-NBTRANX REDEFINES                                00050000
               WDA8302-NBTRAN           PIC  X(4).                      00050000
      *                                          FILLER                         
           03  FILLERX                 PIC  X(03).                      00070000
                                                                                
