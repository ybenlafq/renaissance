      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CREVT PARAMETRE DE CREATION VENTE      *        
      *----------------------------------------------------------------*        
       01  RVCREVT.                                                             
           05  CREVT-CTABLEG2    PIC X(15).                                     
           05  CREVT-CTABLEG2-REDEF REDEFINES CREVT-CTABLEG2.                   
               10  CREVT-ORIGINE         PIC X(03).                             
           05  CREVT-WTABLEG     PIC X(80).                                     
           05  CREVT-WTABLEG-REDEF  REDEFINES CREVT-WTABLEG.                    
               10  CREVT-PRESTATI        PIC X(05).                             
               10  CREVT-TYPVTE          PIC X(01).                             
               10  CREVT-TYPE            PIC X(03).                             
               10  CREVT-SOCLIEU         PIC X(06).                             
               10  CREVT-SOCLIEU-N      REDEFINES CREVT-SOCLIEU                 
                                         PIC 9(06).                             
               10  CREVT-COMMENT         PIC X(40).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVCREVT-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CREVT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CREVT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CREVT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CREVT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
