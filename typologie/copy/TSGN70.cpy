      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : GESTION DES ALIGNEMENTS NATIONAUX      (GN70)          *         
      *****************************************************************         
      *                                                               *         
       01  TS-GN70.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GN70-LONG                 PIC S9(03) COMP-3                    
                                           VALUE +29.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-GN70-DONNEES.                                                  
      *----------------------------------  CODE ACTION                          
              03 TS-GN70-ACT               PIC X(1).                            
      *----------------------------------  PRIX D'ALIGNEMENT                    
              03 TS-GN70-PEXPTTC           PIC X(8).                            
      *----------------------------------  DEBUT D'EFFET                        
              03 TS-GN70-DDEFFET           PIC X(08).                           
      *----------------------------------  FIN D'EFFET                          
              03 TS-GN70-DFEFFET           PIC X(08).                           
      *----------------------------------  CODE CONCURRENT NATIONAL             
              03 TS-GN70-NCONCN            PIC X(04).                           
                                                                                
