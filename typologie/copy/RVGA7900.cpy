      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00030000
      *   COPY DE LA TABLE RVGA7900 X                                           
      **********************************************************        00050000
      *   LISTE DES HOST VARIABLES DE LA VUE RVGA7900                   00060001
      **********************************************************        00070000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA7900.                                                    00080001
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA7900.                                                            
      *}                                                                        
           05  GA79-NSOCIETE   PIC X(3).                                00090001
           05  GA79-NZONPRIX   PIC X(2).                                00100001
           05  GA79-NCODIC     PIC X(7).                                00120001
           05  GA79-DEFFET     PIC X(8).                                00140001
           05  GA79-DFINEFFET  PIC X(8).                                00150001
           05  GA79-WFINEFFET  PIC X(1).                                00150102
           05  GA79-PSTDTTC    PIC S9(5)V99 COMP-3.                     00151001
           05  GA79-LCOMMENT   PIC X(10).                               00152001
           05  GA79-DSYST PIC S9(13) COMP-3.                            00160001
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************        00170000
      *   LISTE DES FLAGS DE LA TABLE RVGA7900                          00180001
      **********************************************************        00190000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA7900-FLAGS.                                              00200001
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA7900-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GA79-NSOCIETE-F                                          00210001
      *        PIC S9(4) COMP.                                          00211000
      *--                                                                       
           05  GA79-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GA79-NZONPRIX-F                                          00220001
      *        PIC S9(4) COMP.                                          00221000
      *--                                                                       
           05  GA79-NZONPRIX-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GA79-NCODIC-F                                            00240001
      *        PIC S9(4) COMP.                                          00241000
      *--                                                                       
           05  GA79-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GA79-DEFFET-F                                            00260001
      *        PIC S9(4) COMP.                                          00261000
      *--                                                                       
           05  GA79-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GA79-DFINEFFET-F                                         00270001
      *        PIC S9(4) COMP.                                          00271000
      *--                                                                       
           05  GA79-DFINEFFET-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GA79-WFINEFFET-F                                         00271102
      *        PIC S9(4) COMP.                                          00271202
      *--                                                                       
           05  GA79-WFINEFFET-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GA79-PSTDTTC-F                                           00272001
      *        PIC S9(4) COMP.                                          00273001
      *--                                                                       
           05  GA79-PSTDTTC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GA79-LCOMMENT-F                                          00274001
      *        PIC S9(4) COMP.                                          00275001
      *--                                                                       
           05  GA79-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GA79-DSYST-F                                             00280001
      *        PIC S9(4) COMP.                                          00290000
      *                                                                         
      *--                                                                       
           05  GA79-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
