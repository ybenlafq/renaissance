      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : MISE A JOUR DU REFERENTIEL NATIONAL    (GN13)          *         
      *        PAGINATION DES PRIMES                                  *         
      *****************************************************************         
      *                                                               *         
       01  TS-GN13.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GN13-LONG                 PIC S9(03) COMP-3                    
                                           VALUE +38.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-GN13-DONNEES.                                                  
      *----------------------------------                                       
              03 TS-GN13-ACT4                PIC X(1).                          
      *----------------------------------                                       
              03 TS-GN13-TYPCOMM             PIC X(1).                          
      *----------------------------------                                       
              03 TS-GN13-PRR                 PIC S9(3)V9.                       
      *----------------------------------                                       
              03 TS-GN13-PRRC                PIC X(05).                         
      *----------------------------------                                       
              03 TS-GN13-DPRR                PIC X(8).                          
      *----------------------------------                                       
              03 TS-GN13-DPRR-AV             PIC X(8).                          
      *----------------------------------                                       
              03 TS-GN13-CPRR                PIC X(10).                         
      *----------------------------------                                       
              03 TS-GN13-MODIF               PIC X(01).                         
                                                                                
