      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      *             RELEVE DE PRIX                                   *  00030000
      *       CONSULTATION AU CODIC                                   * 00040000
      ****************************************************************  00050000
       01  TS-LONG              PIC S9(2) COMP-3 VALUE 54.              00080000
       01  TS-DONNEES.                                                  00090000
              10 TS-DTRTREL     PIC X(08).                              00120100
              10 TS-PRELEVE     PIC X(09).                              00131000
              10 TS-DRELEVE     PIC X(08).                              00132000
              10 TS-LCOMMENT    PIC X(20).                              00160000
              10 TS-CVENDEUR    PIC X(6).                               00170000
              10 TS-NMAG        PIC X(3).                               00180000
                                                                                
