      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGES MQ GENERATION GV00 LOCAL : REPONSE DU LOCAL             
      *         B-VERSION                                                       
      *                                                                         
      *****************************************************************         
      * VERSION POUR MBS43 AVEC PUT ET GET EXTERNES                             
      *                                                                         
           10  WS-MESSAGE-RECU REDEFINES COMM-MQ12-MESSAGE.                     
      *---                                                                      
           17  MESR-ENTETE.                                                     
               20   MESR-TYPE     PIC    X(3).                                  
               20   MESR-NSOCMSG  PIC    X(3).                                  
               20   MESR-NLIEUMSG PIC    X(3).                                  
               20   MESR-NSOCDST  PIC    X(3).                                  
               20   MESR-NLIEUDST PIC    X(3).                                  
               20   MESR-NORD     PIC    9(8).                                  
               20   MESR-LPROG    PIC    X(10).                                 
               20   MESR-DJOUR    PIC    X(8).                                  
               20   MESR-WSID     PIC    X(10).                                 
               20   MESR-USER     PIC    X(10).                                 
               20   MESR-CHRONO   PIC    9(7).                                  
               20   MESR-NBRMSG   PIC    9(7).                                  
               20   MESR-FILLER   PIC    X(30).                                 
           17  MESR-OK.                                                         
               20   MESR-DUPVTE   PIC    X(1).                                  
               20   MESR-NB-CODICS-KO PIC    9(2).                              
               20   MESR-KO-CODIC OCCURS 40.                                    
                 25   MESR-NCODIC PIC    X(7).                                  
                 25   MESR-QSTOCK PIC    9(5).                                  
                 25   MESR-NLIGNE PIC    X(2).                                  
           10  LONGUEUR-MESR      PIC    S9(6) VALUE +674.                      
                                                                                
