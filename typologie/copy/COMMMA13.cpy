      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************00000010
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *00000020
      ******************************************************************00000030
      *                                                                *00000040
      *  PROJET     :                                                   00000050
      *  PROGRAMME  : MFA13                                            *00000060
      *  TITRE      : COMMAREA DU MODULE TP                            *00000070
      *               D'IMPRESSION DE LA LISTE DE MISE EN CASE         *00000080
      *               DES MUTATIONS CLIENTS POUR UNE PLATE FORME       *00000090
      *  LONGUEUR   : 200 C                                            *00000100
      *                                                                *00000110
      ******************************************************************00000120
       01  COMM-MA13-APPLI.                                             00000130
           05 COMM-MA13-ZONES-ENTREE.                                   00000140
             10 COMM-MA13-NSOCIETE           PIC X(3).                  00000150
             10 COMM-MA13-NLIEU              PIC X(3).                  00000160
             10 COMM-MA13-LLIEU              PIC X(20).                 00000170
             10 COMM-MA13-SSAAMMJJ           PIC X(8).                  00000180
           05 COMM-MA13-ZONES-SORTIE.                                   00000190
             10 COMM-MA13-MESSAGE            PIC X(80).                 00000200
           05 COMM-MA13-FILLER          PIC X(86).                      00000210
                                                                                
