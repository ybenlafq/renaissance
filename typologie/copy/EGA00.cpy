      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA00   EGA00                                              00000020
      ***************************************************************** 00000030
       01   EGA00I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MZONCMDI  PIC X(15).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTABGNL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCTABGNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTABGNF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCTABGNI  PIC X(6).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSELECTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MSELECTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSELECTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MSELECTI  PIC X(15).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTMKGL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCTMKGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCTMKGF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCTMKGI   PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTDESCL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCTDESCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTDESCF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCTDESCI  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTITL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNENTITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNENTITF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNENTITI  PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNGRPFL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNGRPFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNGRPFF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNGRPFI   PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNSOCI    PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRAYONL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCRAYONL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCRAYONF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCRAYONI  PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPLL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCTYPLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCTYPLF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCTYPLI   PIC X.                                          00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNLIEUI   PIC X(3).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOC1L   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MNSOC1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSOC1F   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNSOC1I   PIC X(3).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAG2L   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MNMAG2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNMAG2F   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNMAG2I   PIC X(3).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOC2L   COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MNSOC2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSOC2F   PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNSOC2I   PIC X(3).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MNDEPOTI  PIC X(3).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOC3L   COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MNSOC3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSOC3F   PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MNSOC3I   PIC X(3).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLIBERRI  PIC X(78).                                      00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCODTRAI  PIC X(4).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCICSI    PIC X(5).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MNETNAMI  PIC X(8).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MSCREENI  PIC X(4).                                       00001010
      ***************************************************************** 00001020
      * SDF: EGA00   EGA00                                              00001030
      ***************************************************************** 00001040
       01   EGA00O REDEFINES EGA00I.                                    00001050
           02 FILLER    PIC X(12).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MDATJOUA  PIC X.                                          00001080
           02 MDATJOUC  PIC X.                                          00001090
           02 MDATJOUP  PIC X.                                          00001100
           02 MDATJOUH  PIC X.                                          00001110
           02 MDATJOUV  PIC X.                                          00001120
           02 MDATJOUO  PIC X(10).                                      00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MTIMJOUA  PIC X.                                          00001150
           02 MTIMJOUC  PIC X.                                          00001160
           02 MTIMJOUP  PIC X.                                          00001170
           02 MTIMJOUH  PIC X.                                          00001180
           02 MTIMJOUV  PIC X.                                          00001190
           02 MTIMJOUO  PIC X(5).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MZONCMDA  PIC X.                                          00001220
           02 MZONCMDC  PIC X.                                          00001230
           02 MZONCMDP  PIC X.                                          00001240
           02 MZONCMDH  PIC X.                                          00001250
           02 MZONCMDV  PIC X.                                          00001260
           02 MZONCMDO  PIC X(15).                                      00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MWFONCA   PIC X.                                          00001290
           02 MWFONCC   PIC X.                                          00001300
           02 MWFONCP   PIC X.                                          00001310
           02 MWFONCH   PIC X.                                          00001320
           02 MWFONCV   PIC X.                                          00001330
           02 MWFONCO   PIC X(3).                                       00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MCTABGNA  PIC X.                                          00001360
           02 MCTABGNC  PIC X.                                          00001370
           02 MCTABGNP  PIC X.                                          00001380
           02 MCTABGNH  PIC X.                                          00001390
           02 MCTABGNV  PIC X.                                          00001400
           02 MCTABGNO  PIC X(6).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MSELECTA  PIC X.                                          00001430
           02 MSELECTC  PIC X.                                          00001440
           02 MSELECTP  PIC X.                                          00001450
           02 MSELECTH  PIC X.                                          00001460
           02 MSELECTV  PIC X.                                          00001470
           02 MSELECTO  PIC X(15).                                      00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MCTMKGA   PIC X.                                          00001500
           02 MCTMKGC   PIC X.                                          00001510
           02 MCTMKGP   PIC X.                                          00001520
           02 MCTMKGH   PIC X.                                          00001530
           02 MCTMKGV   PIC X.                                          00001540
           02 MCTMKGO   PIC X(5).                                       00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MCTDESCA  PIC X.                                          00001570
           02 MCTDESCC  PIC X.                                          00001580
           02 MCTDESCP  PIC X.                                          00001590
           02 MCTDESCH  PIC X.                                          00001600
           02 MCTDESCV  PIC X.                                          00001610
           02 MCTDESCO  PIC X(5).                                       00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MNENTITA  PIC X.                                          00001640
           02 MNENTITC  PIC X.                                          00001650
           02 MNENTITP  PIC X.                                          00001660
           02 MNENTITH  PIC X.                                          00001670
           02 MNENTITV  PIC X.                                          00001680
           02 MNENTITO  PIC X(5).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MNGRPFA   PIC X.                                          00001710
           02 MNGRPFC   PIC X.                                          00001720
           02 MNGRPFP   PIC X.                                          00001730
           02 MNGRPFH   PIC X.                                          00001740
           02 MNGRPFV   PIC X.                                          00001750
           02 MNGRPFO   PIC X(5).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MNSOCA    PIC X.                                          00001780
           02 MNSOCC    PIC X.                                          00001790
           02 MNSOCP    PIC X.                                          00001800
           02 MNSOCH    PIC X.                                          00001810
           02 MNSOCV    PIC X.                                          00001820
           02 MNSOCO    PIC X(3).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MCRAYONA  PIC X.                                          00001850
           02 MCRAYONC  PIC X.                                          00001860
           02 MCRAYONP  PIC X.                                          00001870
           02 MCRAYONH  PIC X.                                          00001880
           02 MCRAYONV  PIC X.                                          00001890
           02 MCRAYONO  PIC X(5).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MCTYPLA   PIC X.                                          00001920
           02 MCTYPLC   PIC X.                                          00001930
           02 MCTYPLP   PIC X.                                          00001940
           02 MCTYPLH   PIC X.                                          00001950
           02 MCTYPLV   PIC X.                                          00001960
           02 MCTYPLO   PIC X.                                          00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MNLIEUA   PIC X.                                          00001990
           02 MNLIEUC   PIC X.                                          00002000
           02 MNLIEUP   PIC X.                                          00002010
           02 MNLIEUH   PIC X.                                          00002020
           02 MNLIEUV   PIC X.                                          00002030
           02 MNLIEUO   PIC X(3).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MNSOC1A   PIC X.                                          00002060
           02 MNSOC1C   PIC X.                                          00002070
           02 MNSOC1P   PIC X.                                          00002080
           02 MNSOC1H   PIC X.                                          00002090
           02 MNSOC1V   PIC X.                                          00002100
           02 MNSOC1O   PIC X(3).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MNMAG2A   PIC X.                                          00002130
           02 MNMAG2C   PIC X.                                          00002140
           02 MNMAG2P   PIC X.                                          00002150
           02 MNMAG2H   PIC X.                                          00002160
           02 MNMAG2V   PIC X.                                          00002170
           02 MNMAG2O   PIC X(3).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MNSOC2A   PIC X.                                          00002200
           02 MNSOC2C   PIC X.                                          00002210
           02 MNSOC2P   PIC X.                                          00002220
           02 MNSOC2H   PIC X.                                          00002230
           02 MNSOC2V   PIC X.                                          00002240
           02 MNSOC2O   PIC X(3).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MNDEPOTA  PIC X.                                          00002270
           02 MNDEPOTC  PIC X.                                          00002280
           02 MNDEPOTP  PIC X.                                          00002290
           02 MNDEPOTH  PIC X.                                          00002300
           02 MNDEPOTV  PIC X.                                          00002310
           02 MNDEPOTO  PIC X(3).                                       00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MNSOC3A   PIC X.                                          00002340
           02 MNSOC3C   PIC X.                                          00002350
           02 MNSOC3P   PIC X.                                          00002360
           02 MNSOC3H   PIC X.                                          00002370
           02 MNSOC3V   PIC X.                                          00002380
           02 MNSOC3O   PIC X(3).                                       00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MLIBERRA  PIC X.                                          00002410
           02 MLIBERRC  PIC X.                                          00002420
           02 MLIBERRP  PIC X.                                          00002430
           02 MLIBERRH  PIC X.                                          00002440
           02 MLIBERRV  PIC X.                                          00002450
           02 MLIBERRO  PIC X(78).                                      00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MCODTRAA  PIC X.                                          00002480
           02 MCODTRAC  PIC X.                                          00002490
           02 MCODTRAP  PIC X.                                          00002500
           02 MCODTRAH  PIC X.                                          00002510
           02 MCODTRAV  PIC X.                                          00002520
           02 MCODTRAO  PIC X(4).                                       00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MCICSA    PIC X.                                          00002550
           02 MCICSC    PIC X.                                          00002560
           02 MCICSP    PIC X.                                          00002570
           02 MCICSH    PIC X.                                          00002580
           02 MCICSV    PIC X.                                          00002590
           02 MCICSO    PIC X(5).                                       00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MNETNAMA  PIC X.                                          00002620
           02 MNETNAMC  PIC X.                                          00002630
           02 MNETNAMP  PIC X.                                          00002640
           02 MNETNAMH  PIC X.                                          00002650
           02 MNETNAMV  PIC X.                                          00002660
           02 MNETNAMO  PIC X(8).                                       00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MSCREENA  PIC X.                                          00002690
           02 MSCREENC  PIC X.                                          00002700
           02 MSCREENP  PIC X.                                          00002710
           02 MSCREENH  PIC X.                                          00002720
           02 MSCREENV  PIC X.                                          00002730
           02 MSCREENO  PIC X(4).                                       00002740
                                                                                
