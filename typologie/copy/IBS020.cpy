      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IBS020 AU 30/10/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,20,BI,A,                          *        
      *                           27,05,BI,A,                          *        
      *                           32,02,BI,A,                          *        
      *                           34,02,BI,A,                          *        
      *                           36,20,BI,A,                          *        
      *                           56,07,BI,A,                          *        
      *                           63,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IBS020.                                                        
            05 NOMETAT-IBS020           PIC X(6) VALUE 'IBS020'.                
            05 RUPTURES-IBS020.                                                 
           10 IBS020-TYPTRI             PIC X(20).                      007  020
           10 IBS020-CHEFP              PIC X(05).                      027  005
           10 IBS020-WSEQED             PIC X(02).                      032  002
           10 IBS020-NAGREGATED         PIC X(02).                      034  002
           10 IBS020-WTRI               PIC X(20).                      036  020
           10 IBS020-NCODIC             PIC X(07).                      056  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IBS020-SEQUENCE           PIC S9(04) COMP.                063  002
      *--                                                                       
           10 IBS020-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IBS020.                                                   
           10 IBS020-CFAM               PIC X(05).                      065  005
           10 IBS020-CMARQ              PIC X(05).                      070  005
           10 IBS020-ECARTPRIX          PIC X(02).                      075  002
           10 IBS020-LAGREGATED         PIC X(20).                      077  020
           10 IBS020-LCHEFP             PIC X(20).                      097  020
           10 IBS020-LFAM               PIC X(20).                      117  020
           10 IBS020-LREFFOURN          PIC X(20).                      137  020
           10 IBS020-LSTATCOMP          PIC X(03).                      157  003
           10 IBS020-WOA                PIC X(01).                      160  001
           10 IBS020-WSENSAPPRO         PIC X(01).                      161  001
           10 IBS020-MONTANT            PIC S9(05)V9(1) COMP-3.         162  004
           10 IBS020-MONTANT-MAX        PIC S9(05)V9(1) COMP-3.         166  004
           10 IBS020-PCOMMART           PIC S9(03)V9(2) COMP-3.         170  003
           10 IBS020-PREF1              PIC S9(06)V9(2) COMP-3.         173  005
           10 IBS020-PREF2              PIC S9(06)V9(2) COMP-3.         178  005
           10 IBS020-PRMP               PIC S9(05)V9(1) COMP-3.         183  004
           10 IBS020-QSTOCKDEP          PIC S9(05)      COMP-3.         187  003
           10 IBS020-QSTOCKMAG          PIC S9(05)      COMP-3.         190  003
           10 IBS020-QVS0               PIC S9(04)      COMP-3.         193  003
           10 IBS020-QV4S               PIC S9(05)      COMP-3.         196  003
           10 IBS020-REFI               PIC S9(05)V9(1) COMP-3.         199  004
           10 IBS020-TXTVA              PIC S9(03)V9(2) COMP-3.         203  003
            05 FILLER                      PIC X(307).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IBS020-LONG           PIC S9(4)   COMP  VALUE +205.           
      *                                                                         
      *--                                                                       
        01  DSECT-IBS020-LONG           PIC S9(4) COMP-5  VALUE +205.           
                                                                                
      *}                                                                        
