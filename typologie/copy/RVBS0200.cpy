      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVBS0200                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBS0200.                                                    00000100
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBS0200.                                                            
      *}                                                                        
           10 BS02-NLIST           PIC X(7).                            00000110
           10 BS02-CTYPENT         PIC X(2).                            00000120
           10 BS02-LLIST           PIC X(20).                           00000130
           10 BS02-DCREATION       PIC X(8).                            00000140
           10 BS02-NLISTORIG       PIC X(7).                            00000150
           10 BS02-CHEFPROD        PIC X(5).                            00000160
           10 BS02-CTYPCRIT        PIC X(5).                            00000170
           10 BS02-CCRIT1          PIC X(5).                            00000180
           10 BS02-CCRIT2          PIC X(5).                            00000190
           10 BS02-DSYST           PIC S9(13)V USAGE COMP-3.            00000200
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00000210
      *---------------------------------------------------------        00000220
      *   LISTE DES FLAGS DE LA TABLE RVBS0200                          00000230
      *---------------------------------------------------------        00000240
      *                                                                 00000250
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBS0200-FLAGS.                                              00000260
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBS0200-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS02-NLIST-F         PIC S9(4) COMP.                      00000270
      *--                                                                       
           10 BS02-NLIST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS02-CTYPENT-F       PIC S9(4) COMP.                      00000280
      *--                                                                       
           10 BS02-CTYPENT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS02-LLIST-F         PIC S9(4) COMP.                      00000290
      *--                                                                       
           10 BS02-LLIST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS02-DCREATION-F     PIC S9(4) COMP.                      00000300
      *--                                                                       
           10 BS02-DCREATION-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS02-NLISTORIG-F     PIC S9(4) COMP.                      00000310
      *--                                                                       
           10 BS02-NLISTORIG-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS02-CHEFPROD-F      PIC S9(4) COMP.                      00000320
      *--                                                                       
           10 BS02-CHEFPROD-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS02-CTYPCRIT-F      PIC S9(4) COMP.                      00000330
      *--                                                                       
           10 BS02-CTYPCRIT-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS02-CCRIT1-F        PIC S9(4) COMP.                      00000340
      *--                                                                       
           10 BS02-CCRIT1-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS02-CCRIT2-F        PIC S9(4) COMP.                      00000350
      *--                                                                       
           10 BS02-CCRIT2-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BS02-DSYST-F         PIC S9(4) COMP.                      00000360
      *                                                                         
      *--                                                                       
           10 BS02-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
