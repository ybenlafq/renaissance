      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * PARAMETRAGE DES REGLES                                          00000020
      ***************************************************************** 00000030
       01   ETH21I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(4).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIDRGL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MIDRGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MIDRGF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MIDRGI    PIC X(10).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWDATEEFL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MWDATEEFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWDATEEFF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MWDATEEFI      PIC X.                                     00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSELL     COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MSELL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSELF     PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MSELI     PIC X.                                          00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCFAML   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MSCFAML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSCFAMF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSCFAMI   PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCMARQL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MSCMARQL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCMARQF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MSCMARQI  PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNCODICL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MSNCODICL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSNCODICF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MSNCODICI      PIC X(7).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITRE1L  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MTITRE1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTITRE1F  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MTITRE1I  PIC X(10).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITRE2L  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MTITRE2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTITRE2F  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MTITRE2I  PIC X(13).                                      00000530
           02 MLIGNEI OCCURS   12 TIMES .                               00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCFAMI  PIC X(5).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCMARQI      PIC X(5).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MNCODICI     PIC X(7).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSMTTL  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MSMTTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSMTTF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MSMTTI  PIC X(8).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSPOURCL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MSPOURCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSPOURCF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MSPOURCI     PIC X(7).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MBMTTL  COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MBMTTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MBMTTF  PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MBMTTI  PIC X(8).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MBPOURCL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MBPOURCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MBPOURCF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MBPOURCI     PIC X(7).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MDEFFETI     PIC X(8).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MLIBERRI  PIC X(78).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCODTRAI  PIC X(4).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCICSI    PIC X(5).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MNETNAMI  PIC X(8).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MSCREENI  PIC X(4).                                       00001060
      ***************************************************************** 00001070
      * PARAMETRAGE DES REGLES                                          00001080
      ***************************************************************** 00001090
       01   ETH21O REDEFINES ETH21I.                                    00001100
           02 FILLER    PIC X(12).                                      00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MDATJOUA  PIC X.                                          00001130
           02 MDATJOUC  PIC X.                                          00001140
           02 MDATJOUP  PIC X.                                          00001150
           02 MDATJOUH  PIC X.                                          00001160
           02 MDATJOUV  PIC X.                                          00001170
           02 MDATJOUO  PIC X(10).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MTIMJOUA  PIC X.                                          00001200
           02 MTIMJOUC  PIC X.                                          00001210
           02 MTIMJOUP  PIC X.                                          00001220
           02 MTIMJOUH  PIC X.                                          00001230
           02 MTIMJOUV  PIC X.                                          00001240
           02 MTIMJOUO  PIC X(5).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MPAGEA    PIC X.                                          00001270
           02 MPAGEC    PIC X.                                          00001280
           02 MPAGEP    PIC X.                                          00001290
           02 MPAGEH    PIC X.                                          00001300
           02 MPAGEV    PIC X.                                          00001310
           02 MPAGEO    PIC X(4).                                       00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MPAGEMAXA      PIC X.                                     00001340
           02 MPAGEMAXC PIC X.                                          00001350
           02 MPAGEMAXP PIC X.                                          00001360
           02 MPAGEMAXH PIC X.                                          00001370
           02 MPAGEMAXV PIC X.                                          00001380
           02 MPAGEMAXO      PIC X(4).                                  00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MIDRGA    PIC X.                                          00001410
           02 MIDRGC    PIC X.                                          00001420
           02 MIDRGP    PIC X.                                          00001430
           02 MIDRGH    PIC X.                                          00001440
           02 MIDRGV    PIC X.                                          00001450
           02 MIDRGO    PIC X(10).                                      00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MWDATEEFA      PIC X.                                     00001480
           02 MWDATEEFC PIC X.                                          00001490
           02 MWDATEEFP PIC X.                                          00001500
           02 MWDATEEFH PIC X.                                          00001510
           02 MWDATEEFV PIC X.                                          00001520
           02 MWDATEEFO      PIC X.                                     00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MSELA     PIC X.                                          00001550
           02 MSELC     PIC X.                                          00001560
           02 MSELP     PIC X.                                          00001570
           02 MSELH     PIC X.                                          00001580
           02 MSELV     PIC X.                                          00001590
           02 MSELO     PIC X.                                          00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MSCFAMA   PIC X.                                          00001620
           02 MSCFAMC   PIC X.                                          00001630
           02 MSCFAMP   PIC X.                                          00001640
           02 MSCFAMH   PIC X.                                          00001650
           02 MSCFAMV   PIC X.                                          00001660
           02 MSCFAMO   PIC X(5).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MSCMARQA  PIC X.                                          00001690
           02 MSCMARQC  PIC X.                                          00001700
           02 MSCMARQP  PIC X.                                          00001710
           02 MSCMARQH  PIC X.                                          00001720
           02 MSCMARQV  PIC X.                                          00001730
           02 MSCMARQO  PIC X(5).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MSNCODICA      PIC X.                                     00001760
           02 MSNCODICC PIC X.                                          00001770
           02 MSNCODICP PIC X.                                          00001780
           02 MSNCODICH PIC X.                                          00001790
           02 MSNCODICV PIC X.                                          00001800
           02 MSNCODICO      PIC X(7).                                  00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MTITRE1A  PIC X.                                          00001830
           02 MTITRE1C  PIC X.                                          00001840
           02 MTITRE1P  PIC X.                                          00001850
           02 MTITRE1H  PIC X.                                          00001860
           02 MTITRE1V  PIC X.                                          00001870
           02 MTITRE1O  PIC X(10).                                      00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MTITRE2A  PIC X.                                          00001900
           02 MTITRE2C  PIC X.                                          00001910
           02 MTITRE2P  PIC X.                                          00001920
           02 MTITRE2H  PIC X.                                          00001930
           02 MTITRE2V  PIC X.                                          00001940
           02 MTITRE2O  PIC X(13).                                      00001950
           02 MLIGNEO OCCURS   12 TIMES .                               00001960
             03 FILLER       PIC X(2).                                  00001970
             03 MCFAMA  PIC X.                                          00001980
             03 MCFAMC  PIC X.                                          00001990
             03 MCFAMP  PIC X.                                          00002000
             03 MCFAMH  PIC X.                                          00002010
             03 MCFAMV  PIC X.                                          00002020
             03 MCFAMO  PIC X(5).                                       00002030
             03 FILLER       PIC X(2).                                  00002040
             03 MCMARQA      PIC X.                                     00002050
             03 MCMARQC PIC X.                                          00002060
             03 MCMARQP PIC X.                                          00002070
             03 MCMARQH PIC X.                                          00002080
             03 MCMARQV PIC X.                                          00002090
             03 MCMARQO      PIC X(5).                                  00002100
             03 FILLER       PIC X(2).                                  00002110
             03 MNCODICA     PIC X.                                     00002120
             03 MNCODICC     PIC X.                                     00002130
             03 MNCODICP     PIC X.                                     00002140
             03 MNCODICH     PIC X.                                     00002150
             03 MNCODICV     PIC X.                                     00002160
             03 MNCODICO     PIC X(7).                                  00002170
             03 FILLER       PIC X(2).                                  00002180
             03 MSMTTA  PIC X.                                          00002190
             03 MSMTTC  PIC X.                                          00002200
             03 MSMTTP  PIC X.                                          00002210
             03 MSMTTH  PIC X.                                          00002220
             03 MSMTTV  PIC X.                                          00002230
             03 MSMTTO  PIC X(8).                                       00002240
             03 FILLER       PIC X(2).                                  00002250
             03 MSPOURCA     PIC X.                                     00002260
             03 MSPOURCC     PIC X.                                     00002270
             03 MSPOURCP     PIC X.                                     00002280
             03 MSPOURCH     PIC X.                                     00002290
             03 MSPOURCV     PIC X.                                     00002300
             03 MSPOURCO     PIC X(7).                                  00002310
             03 FILLER       PIC X(2).                                  00002320
             03 MBMTTA  PIC X.                                          00002330
             03 MBMTTC  PIC X.                                          00002340
             03 MBMTTP  PIC X.                                          00002350
             03 MBMTTH  PIC X.                                          00002360
             03 MBMTTV  PIC X.                                          00002370
             03 MBMTTO  PIC X(8).                                       00002380
             03 FILLER       PIC X(2).                                  00002390
             03 MBPOURCA     PIC X.                                     00002400
             03 MBPOURCC     PIC X.                                     00002410
             03 MBPOURCP     PIC X.                                     00002420
             03 MBPOURCH     PIC X.                                     00002430
             03 MBPOURCV     PIC X.                                     00002440
             03 MBPOURCO     PIC X(7).                                  00002450
             03 FILLER       PIC X(2).                                  00002460
             03 MDEFFETA     PIC X.                                     00002470
             03 MDEFFETC     PIC X.                                     00002480
             03 MDEFFETP     PIC X.                                     00002490
             03 MDEFFETH     PIC X.                                     00002500
             03 MDEFFETV     PIC X.                                     00002510
             03 MDEFFETO     PIC X(8).                                  00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MLIBERRA  PIC X.                                          00002540
           02 MLIBERRC  PIC X.                                          00002550
           02 MLIBERRP  PIC X.                                          00002560
           02 MLIBERRH  PIC X.                                          00002570
           02 MLIBERRV  PIC X.                                          00002580
           02 MLIBERRO  PIC X(78).                                      00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MCODTRAA  PIC X.                                          00002610
           02 MCODTRAC  PIC X.                                          00002620
           02 MCODTRAP  PIC X.                                          00002630
           02 MCODTRAH  PIC X.                                          00002640
           02 MCODTRAV  PIC X.                                          00002650
           02 MCODTRAO  PIC X(4).                                       00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MCICSA    PIC X.                                          00002680
           02 MCICSC    PIC X.                                          00002690
           02 MCICSP    PIC X.                                          00002700
           02 MCICSH    PIC X.                                          00002710
           02 MCICSV    PIC X.                                          00002720
           02 MCICSO    PIC X(5).                                       00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MNETNAMA  PIC X.                                          00002750
           02 MNETNAMC  PIC X.                                          00002760
           02 MNETNAMP  PIC X.                                          00002770
           02 MNETNAMH  PIC X.                                          00002780
           02 MNETNAMV  PIC X.                                          00002790
           02 MNETNAMO  PIC X(8).                                       00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MSCREENA  PIC X.                                          00002820
           02 MSCREENC  PIC X.                                          00002830
           02 MSCREENP  PIC X.                                          00002840
           02 MSCREENH  PIC X.                                          00002850
           02 MSCREENV  PIC X.                                          00002860
           02 MSCREENO  PIC X(4).                                       00002870
                                                                                
