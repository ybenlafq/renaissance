      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HCTPI TYPES DE PIECES CENTRE HS        *        
      *----------------------------------------------------------------*        
       01  RVGA01PO.                                                            
           05  HCTPI-CTABLEG2    PIC X(15).                                     
           05  HCTPI-CTABLEG2-REDEF REDEFINES HCTPI-CTABLEG2.                   
               10  HCTPI-LIEUHC          PIC X(03).                             
               10  HCTPI-CTYPE           PIC X(03).                             
           05  HCTPI-WTABLEG     PIC X(80).                                     
           05  HCTPI-WTABLEG-REDEF  REDEFINES HCTPI-WTABLEG.                    
               10  HCTPI-WACTIF          PIC X(01).                             
               10  HCTPI-LTYPE           PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01PO-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HCTPI-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HCTPI-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HCTPI-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HCTPI-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
