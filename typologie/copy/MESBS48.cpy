      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * ------------------------------------------------------------- *         
      *  POUR ENVOI MQ DUPLI SPECIFIQUE C&C VERS 400.                           
      *  LONGUEUR TOTALE 50000 CARACTERES                                       
      * ------------------------------------------------------------- *         
           10  MBS-MESSAGE.                                                     
      *--------------------ZONE ENTETE = 105 CAR-------------------*            
              15  MBS-ENTETE.                                                   
                 20   MBS-TYPE      PIC    X(3).                                
                 20   MBS-NSOCMSG   PIC    X(3).                                
                 20   MBS-NLIEUMSG  PIC    X(3).                                
                 20   MBS-NSOCDST   PIC    X(3).                                
                 20   MBS-NLIEUDST  PIC    X(3).                                
                 20   MBS-NORD      PIC    9(8).                                
                 20   MBS-LPROG     PIC    X(10).                               
                 20   MBS-DJOUR     PIC    X(8).                                
                 20   MBS-WSID      PIC    X(10).                               
                 20   MBS-USER      PIC    X(10).                               
                 20   MBS-CHRONO    PIC    9(7).                                
                 20   MBS-NBRMSG    PIC    9(7).                                
                 20   MBS-NBROCC    PIC    9(5).                                
                 20   MBS-LGNOCC    PIC    9(5).                                
                 20   MBS-VERSION   PIC    X(2).                                
                 20   MBS-DSYST     PIC   S9(13).                               
                 20   MBS-FILLER    PIC    X(05).                               
             15  MBS-DATA.                                                      
      *--------------------ZONE COMPTEURS = 57 CAR-----------------*            
                20  MBS-NB-MBS-NOMPROG.                                         
      * -- NOM D'IMPRIMANTE                                                     
                   25   MBS-WOUTQ      PIC    X(10).                            
      * -- SORTIE D'ARCHIVAGE O/N                                               
                   25   MBS-WARCHIVAGE PIC    X(01).                            
                   25   MBS-WDBK       PIC    X(01).                            
      * -- NBRE D'ENREGISTREMENTS CONSTITUANT LA VENTE                          
                   25   MBS-NB-ENREG   PIC    9(3).                             
      * -- NBRE DE LIGNES ARTICLES QUI NE SONT PAS DES REPRISES                 
                   25   MBS-NB-TOPE    PIC    9(3).                             
      * -- NBRE DE LIGNES DE CHAQUE TABLE.                                      
                   25   MBS-NB-GV10    PIC    9(3).                             
                   25   MBS-NB-GV02    PIC    9(3).                             
                   25   MBS-NB-GV11    PIC    9(3).                             
      * -- PLUS GRAND N� DE TRANSACTION DE PAIEMENT                             
                   25   MBS-NTRANSMAX  PIC    9(8).                             
      * -- MOT DE PASSE QUI SERA DEMANDE AU CLIENT LORS DU RETRAIT              
      * -- COMPTOIR                                                             
                   25   MBS-PASSW      PIC    X(10).                            
      * -- DATE ET HEURE DE MISE � DISPO DE LA COMMANDE                         
                   25   MBS-DDISPO     PIC    X(08).                            
                   25   MBS-HDISPO     PIC    X(04).                            
                20  MBS-TABLES.                                                 
      * -- NOUVELLES ZONES POUR RETAIT CONSIGNE (45 CAR)                        
      * -- CODES EVENEMENTS POSSIBLES                                           
      *    EN PROVENANCE DE LOGIBAG (VIENT DU MEC16) :                          
      *    'DEPOT CONSIGNE'  , 'RETRAIT CLIENT' OU 'SORTIE CONSIGNE'            
      * CE CODE NE SERA PAS RENSEIGN� LORS DE LA 1�RE DUPLI                     
      * DE CR�ATION DE VENTE (VIENT DU MBS72)                                   
      * -- CODE EVENEMENT CONSIGNE                                              
                   25   MBS-EVENT      PIC    X(15).                            
      * -- CODE CASIER CONSIGNE                                                 
                   25   MBS-CASIER     PIC    X(10).                            
      * -- MOT DE PASSE QUI SERA DEMANDE AU CLIENT LORS DU RETRAIT              
      * -- CONSIGNE                                                             
                   25   MBS-PASSWC     PIC    X(10).                            
      * -- CODE EMPLOYE CONSIGNE                                                
                   25   MBS-EMPLOYE    PIC    X(10).                            
      *  MBS-TABLES CONTIENT AU MAXIMUM TOUS LES ENREGS SUIVANTS :              
      *  LONGUEUR DES TABLES = LONGUEUR + 4 CAR POUR LE NOM                     
      *  TABLE RTGV10 : 1 OCC. = 426 CAR              =>   426                  
      *  TABLE RTGV11 : 1 OCC. = 367 CAR MAXI 80 OCC. => 29360                  
      *  TABLE RTGV02 : 1 OCC. = 453 CAR MAXI  4 OCC. =>  1812                  
      * -- NOUVELLES ZONES POUR RETAIT CONSIGNE ( 45 CAR ) DEDUITES             
      *            25   MBS-TABLES-DATA       PIC X(49826).                     
                   25   MBS-TABLES-DATA       PIC X(49781).                     
                                                                                
