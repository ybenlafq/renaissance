      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGA7200                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA7200                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA7200.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA7200.                                                            
      *}                                                                        
           02  GA72-CNUMCONT                                                    
               PIC X(0003).                                                     
           02  GA72-WTYPCONT                                                    
               PIC X(0001).                                                     
           02  GA72-WBOMAX                                                      
               PIC X(0030).                                                     
           02  GA72-WBOMIN                                                      
               PIC X(0030).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGA7200                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA7200-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA7200-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA72-CNUMCONT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA72-CNUMCONT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA72-WTYPCONT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA72-WTYPCONT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA72-WBOMAX-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA72-WBOMAX-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA72-WBOMIN-F                                                    
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GA72-WBOMIN-F                                                    
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
