      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES VENTES                               *        
      *  TRANSACTION: GV00                                             *        
      *  TITRE      : COMMAREA DE L'APPLICATION IDV  APELLEE PAR TGV00 *        
      *                        INTERROGATION STATUT DE VENTE LOCAL     *        
      *  LONGUEUR   : 100                                              *        
      *                                                                *        
      *                                                                *        
      ******************************************************************        
      * M23862 PSE ASSURANTIELLE R�CUP�RATION NUM�RO DE PSE                     
      *         A PARTIR DU 400.                                                
      ******************************************************************        
      *                                                                 00530000
      *                                                                 00230000
       01  COMM-MBS49-APPLI.                                            00260000
         02 COMM-MBS49-ENTREE.                                          00260000
      * IDENTIFIANT VENTE                                                       
           05 COMM-MBS49-NSOCP                PIC X(03).                00320000
           05 COMM-MBS49-NLIEUP               PIC X(03).                00330000
           05 COMM-MBS49-NSOCIETE             PIC X(03).                00320000
           05 COMM-MBS49-NLIEU                PIC X(03).                00330000
           05 COMM-MBS49-NORDRE               PIC X(05).                00340000
           05 COMM-MBS49-DVENTE               PIC X(08).                00340000
           05 COMM-MBS49-NVENTE               PIC X(07).                00340000
           05 COMM-MBS49-TYPVTE               PIC X(01).                00340000
         02 COMM-MBS49-SORTIE.                                          00260000
           05 COMM-MBS49-MESSAGE.                                               
                10 COMM-MBS49-CODRET          PIC X.                            
                   88  COMM-MBS49-OK          VALUE ' '.                        
                   88  COMM-MBS49-ERR-BLQ     VALUE '1'.                        
                   88  COMM-MBS49-ERR-NON-BLQ VALUE '2'.                        
                10 COMM-MBS49-LIBERR          PIC X(58).                        
      *    05 FILLER                    PIC X(08).                      00340000
           05 COMM-MBS49-NPSE           PIC X(08).                              
                                                                                
