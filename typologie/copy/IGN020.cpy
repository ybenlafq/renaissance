      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGN020 AU 10/04/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,23,BI,A,                          *        
      *                           40,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGN020.                                                        
            05 NOMETAT-IGN020           PIC X(6) VALUE 'IGN020'.                
            05 RUPTURES-IGN020.                                                 
           10 IGN020-CHEFPROD           PIC X(05).                      007  005
           10 IGN020-CFAM               PIC X(05).                      012  005
           10 IGN020-CHAMPTRI           PIC X(23).                      017  023
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGN020-SEQUENCE           PIC S9(04) COMP.                040  002
      *--                                                                       
           10 IGN020-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGN020.                                                   
           10 IGN020-CACHE              PIC X(01).                      042  001
           10 IGN020-CACHE2             PIC X(01).                      043  001
           10 IGN020-CAPPRO             PIC X(03).                      044  003
           10 IGN020-CIMPER             PIC X(01).                      047  001
           10 IGN020-CMARQ              PIC X(05).                      048  005
           10 IGN020-DDEBPVR            PIC X(08).                      053  008
           10 IGN020-DDEBPVR2           PIC X(08).                      061  008
           10 IGN020-DFINPVR            PIC X(08).                      069  008
           10 IGN020-DFINPVR2           PIC X(08).                      077  008
           10 IGN020-LCHEFPROD          PIC X(20).                      085  020
           10 IGN020-LFAM               PIC X(20).                      105  020
           10 IGN020-LIBTRAIT           PIC X(08).                      125  008
           10 IGN020-LREFFOUR           PIC X(20).                      133  020
           10 IGN020-NCODIC             PIC X(07).                      153  007
           10 IGN020-NLIEU              PIC X(03).                      160  003
           10 IGN020-NLIEU2             PIC X(03).                      163  003
           10 IGN020-NSOCIETE           PIC X(03).                      166  003
           10 IGN020-NSOCIETE2          PIC X(03).                      169  003
           10 IGN020-NZONPRIX           PIC X(02).                      172  002
           10 IGN020-NZONPRIX2          PIC X(02).                      174  002
           10 IGN020-PREFTTC            PIC S9(07)V9(2) COMP-3.         176  005
           10 IGN020-PREFTTCS           PIC S9(07)V9(2) COMP-3.         181  005
           10 IGN020-PREFTTC2           PIC S9(07)V9(2) COMP-3.         186  005
           10 IGN020-PREFTTC2S          PIC S9(07)V9(2) COMP-3.         191  005
            05 FILLER                      PIC X(317).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGN020-LONG           PIC S9(4)   COMP  VALUE +195.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGN020-LONG           PIC S9(4) COMP-5  VALUE +195.           
                                                                                
      *}                                                                        
