      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SLCTL NMD SL: COLONNE ETAT ECART STK   *        
      *----------------------------------------------------------------*        
       01  RVGA01ZM.                                                            
           05  SLCTL-CTABLEG2    PIC X(15).                                     
           05  SLCTL-CTABLEG2-REDEF REDEFINES SLCTL-CTABLEG2.                   
               10  SLCTL-NCOL            PIC X(02).                             
               10  SLCTL-NSEQ            PIC X(02).                             
           05  SLCTL-WTABLEG     PIC X(80).                                     
           05  SLCTL-WTABLEG-REDEF  REDEFINES SLCTL-WTABLEG.                    
               10  SLCTL-WACTIF          PIC X(01).                             
               10  SLCTL-NSSLIEU         PIC X(03).                             
               10  SLCTL-LCOLONNE        PIC X(10).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01ZM-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLCTL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SLCTL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLCTL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SLCTL-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
