      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES VENTES                               *        
      *  TRANSACTION: MBS43                                            *        
      *  TITRE      : COMMAREA DE L'APPLICATION DGL  APELLEE PAR TGV01 *        
      *  LONGUEUR   : 700                                              *        
      *                                                                *        
      * ALINO : 2010/05/04 POUR �VITER LE BLOCKORDER LORS DE L'ENCAISS *        
      *         EMENT.                                                 *        
      ******************************************************************        
      *                                                                 00530000
      *                                                                 00230000
       01  COMM-MBS43-APPLI.                                            00260000
         02 COMM-MBS43-ENTREE.                                          00260000
      * IDENTIFIANT VENTE                                                       
           05 COMM-MBS43-NSOCIETE       PIC X(03).                      00320000
           05 COMM-MBS43-NLIEU          PIC X(03).                      00330000
           05 COMM-MBS43-NVENTE         PIC X(07).                      00340000
           05 COMM-MBS43-TYPVTE         PIC X(01).                      00340000
      * LIEU DE MODIFICATION                                                    
           05 COMM-MBS43-NSOCMODIF      PIC X(03).                      00320000
           05 COMM-MBS43-NLIEUMODIF     PIC X(03).                      00330000
      * LIEU DE PAIEMENT                                                        
           05 COMM-MBS43-NSOCP          PIC X(03).                      00320000
           05 COMM-MBS43-NLIEUP         PIC X(03).                      00330000
           05 COMM-MBS43-CIMPRIM        PIC X(10).                      00330000
         02 COMM-MBS43-SORTIE.                                          00260000
           05 COMM-MBS43-CODE-RETOUR    PIC X(01).                      00340000
           05 COMM-MBS43-MESSAGE.                                               
                10 COMM-MBS43-CODRET         PIC X.                             
                   88  COMM-MBS43-OK         VALUE ' '.                         
                   88  COMM-MBS43-ERR-BLQ    VALUE '1'.                         
                   88  COMM-MBS43-ERR-STO    VALUE '2'.                         
                   88  COMM-MBS43-ERR-DELAI  VALUE '3'.                         
BF                 88  COMM-MBS43-ERR-TOPE   VALUE '4'.                         
                10 COMM-MBS43-LIBERR         PIC X(58).                         
           05 COMM-MBS43-PBSTOCK OCCURS 40.                                     
              10 COMM-MBS43-NCODIC         PIC X(07).                   00340000
              10 COMM-MBS43-QSTOCK         PIC 9(05).                   00340000
              10 COMM-MBS43-NLIGNE         PIC X(02).                   00340000
           05 COMM-MBS43-MODIF-CTYPLIEU PIC X(01).                              
              88 COMM-MBS43-ENTREPOT        VALUE '2'.                          
              88 COMM-MBS43-MAGASIN         VALUE '3'.                          
ALINO      05 COMM-MBS43-BLOCKORDER     PIC X(03).                      00340000
V43R0 * COMM-MBS43-REC-TYPENVOI = 'OUI' SI LA TABLE TYPENV EST DEJA             
V43R0 * RENSEIGN�                                                               
V43R0      05 COMM-MBS43-REC-TYPENVOI   PIC X(03).                              
  !        05 COMM-MBS43-TAB-TYPENVOI   PIC X(10).                              
  !        05 FILLER                    PIC X(095).                     00340000
V43R0 *    05 FILLER                    PIC X(108).                     00340000
                                                                                
