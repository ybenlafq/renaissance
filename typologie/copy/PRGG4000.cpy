      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE GG4000                       
      ******************************************************************        
      *                                                                         
       CLEF-GG4000             SECTION.                                         
      *                                                                         
           MOVE 'RVGG4000          '       TO   TABLE-NAME.                     
           MOVE 'GG4000'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-GG4000. EXIT.                                                   
                EJECT                                                           
                                                                                
