      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * INT DES PRIX ARTICLES ET PRIMES                                 00000020
      ***************************************************************** 00000030
       01   EGG09I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNCODICI  PIC X(7).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLREFI    PIC X(20).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCASSORTL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MCASSORTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCASSORTF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCASSORTI      PIC X(5).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRMPL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MPRMPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPRMPF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MPRMPI    PIC X(9).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLFAMI    PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPROL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAPPROF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCAPPROI  PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDPRAL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDPRAL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDPRAF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDPRAI    PIC X(9).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCMARQI   PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLMARQI   PIC X(20).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPOL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXPOF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCEXPOI   PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPCFL     COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MPCFL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MPCFF     PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MPCFI     PIC X(9).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEFFICL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCEFFICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCEFFICF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCEFFICI  PIC X.                                          00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSVTEL   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCSVTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCSVTEF   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCSVTEI   PIC X.                                          00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRARL    COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MPRARL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPRARF    PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MPRARI    PIC X(9).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBLG1L  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MLIBLG1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBLG1F  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLIBLG1I  PIC X(6).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIGNE1L  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLIGNE1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIGNE1F  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLIGNE1I  PIC X(72).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBLG2L  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MLIBLG2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBLG2F  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLIBLG2I  PIC X(6).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIGNE2L  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MLIGNE2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIGNE2F  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MLIGNE2I  PIC X(72).                                      00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MPAGEI    PIC X(2).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETOTL      COMP PIC S9(4).                            00000940
      *--                                                                       
           02 MPAGETOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGETOTF      PIC X.                                     00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MPAGETOTI      PIC X(2).                                  00000970
           02 MLIGNESI OCCURS   10 TIMES .                              00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNZONPL      COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MNZONPL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNZONPF      PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MNZONPI      PIC X(2).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIXACL     COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MPRIXACL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPRIXACF     PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MPRIXACI     PIC X(9).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPMBUACL     COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MPMBUACL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPMBUACF     PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MPMBUACI     PIC X(6).                                  00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPCOMMAL     COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MPCOMMAL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPCOMMAF     PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MPCOMMAI     PIC X(5).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFMILACL     COMP PIC S9(4).                            00001150
      *--                                                                       
             03 MFMILACL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MFMILACF     PIC X.                                     00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MFMILACI     PIC X(4).                                  00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEACL     COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MDATEACL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDATEACF     PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MDATEACI     PIC X(8).                                  00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIXNL      COMP PIC S9(4).                            00001230
      *--                                                                       
             03 MPRIXNL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPRIXNF      PIC X.                                     00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MPRIXNI      PIC X(9).                                  00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOMML      COMP PIC S9(4).                            00001270
      *--                                                                       
             03 MLCOMML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLCOMMF      PIC X.                                     00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MLCOMMI      PIC X(5).                                  00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPMBUNL      COMP PIC S9(4).                            00001310
      *--                                                                       
             03 MPMBUNL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPMBUNF      PIC X.                                     00001320
             03 FILLER  PIC X(4).                                       00001330
             03 MPMBUNI      PIC X(6).                                  00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTMBNL      COMP PIC S9(4).                            00001350
      *--                                                                       
             03 MQTMBNL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQTMBNF      PIC X.                                     00001360
             03 FILLER  PIC X(4).                                       00001370
             03 MQTMBNI      PIC X(5).                                  00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPCOMMNL     COMP PIC S9(4).                            00001390
      *--                                                                       
             03 MPCOMMNL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPCOMMNF     PIC X.                                     00001400
             03 FILLER  PIC X(4).                                       00001410
             03 MPCOMMNI     PIC X(5).                                  00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFMILNL      COMP PIC S9(4).                            00001430
      *--                                                                       
             03 MFMILNL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MFMILNF      PIC X.                                     00001440
             03 FILLER  PIC X(4).                                       00001450
             03 MFMILNI      PIC X(4).                                  00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MZONCMDI  PIC X(15).                                      00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MLIBERRI  PIC X(58).                                      00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MCODTRAI  PIC X(4).                                       00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001590
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001600
           02 FILLER    PIC X(4).                                       00001610
           02 MCICSI    PIC X(5).                                       00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001630
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001640
           02 FILLER    PIC X(4).                                       00001650
           02 MNETNAMI  PIC X(8).                                       00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001670
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001680
           02 FILLER    PIC X(4).                                       00001690
           02 MSCREENI  PIC X(4).                                       00001700
      ***************************************************************** 00001710
      * INT DES PRIX ARTICLES ET PRIMES                                 00001720
      ***************************************************************** 00001730
       01   EGG09O REDEFINES EGG09I.                                    00001740
           02 FILLER    PIC X(12).                                      00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MDATJOUA  PIC X.                                          00001770
           02 MDATJOUC  PIC X.                                          00001780
           02 MDATJOUP  PIC X.                                          00001790
           02 MDATJOUH  PIC X.                                          00001800
           02 MDATJOUV  PIC X.                                          00001810
           02 MDATJOUO  PIC X(10).                                      00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MTIMJOUA  PIC X.                                          00001840
           02 MTIMJOUC  PIC X.                                          00001850
           02 MTIMJOUP  PIC X.                                          00001860
           02 MTIMJOUH  PIC X.                                          00001870
           02 MTIMJOUV  PIC X.                                          00001880
           02 MTIMJOUO  PIC X(5).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MNCODICA  PIC X.                                          00001910
           02 MNCODICC  PIC X.                                          00001920
           02 MNCODICP  PIC X.                                          00001930
           02 MNCODICH  PIC X.                                          00001940
           02 MNCODICV  PIC X.                                          00001950
           02 MNCODICO  PIC X(7).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MLREFA    PIC X.                                          00001980
           02 MLREFC    PIC X.                                          00001990
           02 MLREFP    PIC X.                                          00002000
           02 MLREFH    PIC X.                                          00002010
           02 MLREFV    PIC X.                                          00002020
           02 MLREFO    PIC X(20).                                      00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MCASSORTA      PIC X.                                     00002050
           02 MCASSORTC PIC X.                                          00002060
           02 MCASSORTP PIC X.                                          00002070
           02 MCASSORTH PIC X.                                          00002080
           02 MCASSORTV PIC X.                                          00002090
           02 MCASSORTO      PIC X(5).                                  00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MPRMPA    PIC X.                                          00002120
           02 MPRMPC    PIC X.                                          00002130
           02 MPRMPP    PIC X.                                          00002140
           02 MPRMPH    PIC X.                                          00002150
           02 MPRMPV    PIC X.                                          00002160
           02 MPRMPO    PIC Z(5)9,99.                                   00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MCFAMA    PIC X.                                          00002190
           02 MCFAMC    PIC X.                                          00002200
           02 MCFAMP    PIC X.                                          00002210
           02 MCFAMH    PIC X.                                          00002220
           02 MCFAMV    PIC X.                                          00002230
           02 MCFAMO    PIC X(5).                                       00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MLFAMA    PIC X.                                          00002260
           02 MLFAMC    PIC X.                                          00002270
           02 MLFAMP    PIC X.                                          00002280
           02 MLFAMH    PIC X.                                          00002290
           02 MLFAMV    PIC X.                                          00002300
           02 MLFAMO    PIC X(20).                                      00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MCAPPROA  PIC X.                                          00002330
           02 MCAPPROC  PIC X.                                          00002340
           02 MCAPPROP  PIC X.                                          00002350
           02 MCAPPROH  PIC X.                                          00002360
           02 MCAPPROV  PIC X.                                          00002370
           02 MCAPPROO  PIC X(5).                                       00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MDPRAA    PIC X.                                          00002400
           02 MDPRAC    PIC X.                                          00002410
           02 MDPRAP    PIC X.                                          00002420
           02 MDPRAH    PIC X.                                          00002430
           02 MDPRAV    PIC X.                                          00002440
           02 MDPRAO    PIC Z(5)9,99.                                   00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MCMARQA   PIC X.                                          00002470
           02 MCMARQC   PIC X.                                          00002480
           02 MCMARQP   PIC X.                                          00002490
           02 MCMARQH   PIC X.                                          00002500
           02 MCMARQV   PIC X.                                          00002510
           02 MCMARQO   PIC X(5).                                       00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MLMARQA   PIC X.                                          00002540
           02 MLMARQC   PIC X.                                          00002550
           02 MLMARQP   PIC X.                                          00002560
           02 MLMARQH   PIC X.                                          00002570
           02 MLMARQV   PIC X.                                          00002580
           02 MLMARQO   PIC X(20).                                      00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MCEXPOA   PIC X.                                          00002610
           02 MCEXPOC   PIC X.                                          00002620
           02 MCEXPOP   PIC X.                                          00002630
           02 MCEXPOH   PIC X.                                          00002640
           02 MCEXPOV   PIC X.                                          00002650
           02 MCEXPOO   PIC X(5).                                       00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MPCFA     PIC X.                                          00002680
           02 MPCFC     PIC X.                                          00002690
           02 MPCFP     PIC X.                                          00002700
           02 MPCFH     PIC X.                                          00002710
           02 MPCFV     PIC X.                                          00002720
           02 MPCFO     PIC Z(5)9,99.                                   00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MCEFFICA  PIC X.                                          00002750
           02 MCEFFICC  PIC X.                                          00002760
           02 MCEFFICP  PIC X.                                          00002770
           02 MCEFFICH  PIC X.                                          00002780
           02 MCEFFICV  PIC X.                                          00002790
           02 MCEFFICO  PIC X.                                          00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MCSVTEA   PIC X.                                          00002820
           02 MCSVTEC   PIC X.                                          00002830
           02 MCSVTEP   PIC X.                                          00002840
           02 MCSVTEH   PIC X.                                          00002850
           02 MCSVTEV   PIC X.                                          00002860
           02 MCSVTEO   PIC X.                                          00002870
           02 FILLER    PIC X(2).                                       00002880
           02 MPRARA    PIC X.                                          00002890
           02 MPRARC    PIC X.                                          00002900
           02 MPRARP    PIC X.                                          00002910
           02 MPRARH    PIC X.                                          00002920
           02 MPRARV    PIC X.                                          00002930
           02 MPRARO    PIC Z(5)9,99.                                   00002940
           02 FILLER    PIC X(2).                                       00002950
           02 MLIBLG1A  PIC X.                                          00002960
           02 MLIBLG1C  PIC X.                                          00002970
           02 MLIBLG1P  PIC X.                                          00002980
           02 MLIBLG1H  PIC X.                                          00002990
           02 MLIBLG1V  PIC X.                                          00003000
           02 MLIBLG1O  PIC X(6).                                       00003010
           02 FILLER    PIC X(2).                                       00003020
           02 MLIGNE1A  PIC X.                                          00003030
           02 MLIGNE1C  PIC X.                                          00003040
           02 MLIGNE1P  PIC X.                                          00003050
           02 MLIGNE1H  PIC X.                                          00003060
           02 MLIGNE1V  PIC X.                                          00003070
           02 MLIGNE1O  PIC X(72).                                      00003080
           02 FILLER    PIC X(2).                                       00003090
           02 MLIBLG2A  PIC X.                                          00003100
           02 MLIBLG2C  PIC X.                                          00003110
           02 MLIBLG2P  PIC X.                                          00003120
           02 MLIBLG2H  PIC X.                                          00003130
           02 MLIBLG2V  PIC X.                                          00003140
           02 MLIBLG2O  PIC X(6).                                       00003150
           02 FILLER    PIC X(2).                                       00003160
           02 MLIGNE2A  PIC X.                                          00003170
           02 MLIGNE2C  PIC X.                                          00003180
           02 MLIGNE2P  PIC X.                                          00003190
           02 MLIGNE2H  PIC X.                                          00003200
           02 MLIGNE2V  PIC X.                                          00003210
           02 MLIGNE2O  PIC X(72).                                      00003220
           02 FILLER    PIC X(2).                                       00003230
           02 MPAGEA    PIC X.                                          00003240
           02 MPAGEC    PIC X.                                          00003250
           02 MPAGEP    PIC X.                                          00003260
           02 MPAGEH    PIC X.                                          00003270
           02 MPAGEV    PIC X.                                          00003280
           02 MPAGEO    PIC ZZ.                                         00003290
           02 FILLER    PIC X(2).                                       00003300
           02 MPAGETOTA      PIC X.                                     00003310
           02 MPAGETOTC PIC X.                                          00003320
           02 MPAGETOTP PIC X.                                          00003330
           02 MPAGETOTH PIC X.                                          00003340
           02 MPAGETOTV PIC X.                                          00003350
           02 MPAGETOTO      PIC ZZ.                                    00003360
           02 MLIGNESO OCCURS   10 TIMES .                              00003370
             03 FILLER       PIC X(2).                                  00003380
             03 MNZONPA      PIC X.                                     00003390
             03 MNZONPC PIC X.                                          00003400
             03 MNZONPP PIC X.                                          00003410
             03 MNZONPH PIC X.                                          00003420
             03 MNZONPV PIC X.                                          00003430
             03 MNZONPO      PIC X(2).                                  00003440
             03 FILLER       PIC X(2).                                  00003450
             03 MPRIXACA     PIC X.                                     00003460
             03 MPRIXACC     PIC X.                                     00003470
             03 MPRIXACP     PIC X.                                     00003480
             03 MPRIXACH     PIC X.                                     00003490
             03 MPRIXACV     PIC X.                                     00003500
             03 MPRIXACO     PIC Z(5)9,99.                              00003510
             03 FILLER       PIC X(2).                                  00003520
             03 MPMBUACA     PIC X.                                     00003530
             03 MPMBUACC     PIC X.                                     00003540
             03 MPMBUACP     PIC X.                                     00003550
             03 MPMBUACH     PIC X.                                     00003560
             03 MPMBUACV     PIC X.                                     00003570
             03 MPMBUACO     PIC X(6).                                  00003580
             03 FILLER       PIC X(2).                                  00003590
             03 MPCOMMAA     PIC X.                                     00003600
             03 MPCOMMAC     PIC X.                                     00003610
             03 MPCOMMAP     PIC X.                                     00003620
             03 MPCOMMAH     PIC X.                                     00003630
             03 MPCOMMAV     PIC X.                                     00003640
             03 MPCOMMAO     PIC X(5).                                  00003650
             03 FILLER       PIC X(2).                                  00003660
             03 MFMILACA     PIC X.                                     00003670
             03 MFMILACC     PIC X.                                     00003680
             03 MFMILACP     PIC X.                                     00003690
             03 MFMILACH     PIC X.                                     00003700
             03 MFMILACV     PIC X.                                     00003710
             03 MFMILACO     PIC X(4).                                  00003720
             03 FILLER       PIC X(2).                                  00003730
             03 MDATEACA     PIC X.                                     00003740
             03 MDATEACC     PIC X.                                     00003750
             03 MDATEACP     PIC X.                                     00003760
             03 MDATEACH     PIC X.                                     00003770
             03 MDATEACV     PIC X.                                     00003780
             03 MDATEACO     PIC X(8).                                  00003790
             03 FILLER       PIC X(2).                                  00003800
             03 MPRIXNA      PIC X.                                     00003810
             03 MPRIXNC PIC X.                                          00003820
             03 MPRIXNP PIC X.                                          00003830
             03 MPRIXNH PIC X.                                          00003840
             03 MPRIXNV PIC X.                                          00003850
             03 MPRIXNO      PIC Z(5)9,99.                              00003860
             03 FILLER       PIC X(2).                                  00003870
             03 MLCOMMA      PIC X.                                     00003880
             03 MLCOMMC PIC X.                                          00003890
             03 MLCOMMP PIC X.                                          00003900
             03 MLCOMMH PIC X.                                          00003910
             03 MLCOMMV PIC X.                                          00003920
             03 MLCOMMO      PIC X(5).                                  00003930
             03 FILLER       PIC X(2).                                  00003940
             03 MPMBUNA      PIC X.                                     00003950
             03 MPMBUNC PIC X.                                          00003960
             03 MPMBUNP PIC X.                                          00003970
             03 MPMBUNH PIC X.                                          00003980
             03 MPMBUNV PIC X.                                          00003990
             03 MPMBUNO      PIC X(6).                                  00004000
             03 FILLER       PIC X(2).                                  00004010
             03 MQTMBNA      PIC X.                                     00004020
             03 MQTMBNC PIC X.                                          00004030
             03 MQTMBNP PIC X.                                          00004040
             03 MQTMBNH PIC X.                                          00004050
             03 MQTMBNV PIC X.                                          00004060
             03 MQTMBNO      PIC ZZ9,9.                                 00004070
             03 FILLER       PIC X(2).                                  00004080
             03 MPCOMMNA     PIC X.                                     00004090
             03 MPCOMMNC     PIC X.                                     00004100
             03 MPCOMMNP     PIC X.                                     00004110
             03 MPCOMMNH     PIC X.                                     00004120
             03 MPCOMMNV     PIC X.                                     00004130
             03 MPCOMMNO     PIC X(5).                                  00004140
             03 FILLER       PIC X(2).                                  00004150
             03 MFMILNA      PIC X.                                     00004160
             03 MFMILNC PIC X.                                          00004170
             03 MFMILNP PIC X.                                          00004180
             03 MFMILNH PIC X.                                          00004190
             03 MFMILNV PIC X.                                          00004200
             03 MFMILNO      PIC X(4).                                  00004210
           02 FILLER    PIC X(2).                                       00004220
           02 MZONCMDA  PIC X.                                          00004230
           02 MZONCMDC  PIC X.                                          00004240
           02 MZONCMDP  PIC X.                                          00004250
           02 MZONCMDH  PIC X.                                          00004260
           02 MZONCMDV  PIC X.                                          00004270
           02 MZONCMDO  PIC X(15).                                      00004280
           02 FILLER    PIC X(2).                                       00004290
           02 MLIBERRA  PIC X.                                          00004300
           02 MLIBERRC  PIC X.                                          00004310
           02 MLIBERRP  PIC X.                                          00004320
           02 MLIBERRH  PIC X.                                          00004330
           02 MLIBERRV  PIC X.                                          00004340
           02 MLIBERRO  PIC X(58).                                      00004350
           02 FILLER    PIC X(2).                                       00004360
           02 MCODTRAA  PIC X.                                          00004370
           02 MCODTRAC  PIC X.                                          00004380
           02 MCODTRAP  PIC X.                                          00004390
           02 MCODTRAH  PIC X.                                          00004400
           02 MCODTRAV  PIC X.                                          00004410
           02 MCODTRAO  PIC X(4).                                       00004420
           02 FILLER    PIC X(2).                                       00004430
           02 MCICSA    PIC X.                                          00004440
           02 MCICSC    PIC X.                                          00004450
           02 MCICSP    PIC X.                                          00004460
           02 MCICSH    PIC X.                                          00004470
           02 MCICSV    PIC X.                                          00004480
           02 MCICSO    PIC X(5).                                       00004490
           02 FILLER    PIC X(2).                                       00004500
           02 MNETNAMA  PIC X.                                          00004510
           02 MNETNAMC  PIC X.                                          00004520
           02 MNETNAMP  PIC X.                                          00004530
           02 MNETNAMH  PIC X.                                          00004540
           02 MNETNAMV  PIC X.                                          00004550
           02 MNETNAMO  PIC X(8).                                       00004560
           02 FILLER    PIC X(2).                                       00004570
           02 MSCREENA  PIC X.                                          00004580
           02 MSCREENC  PIC X.                                          00004590
           02 MSCREENP  PIC X.                                          00004600
           02 MSCREENH  PIC X.                                          00004610
           02 MSCREENV  PIC X.                                          00004620
           02 MSCREENO  PIC X(4).                                       00004630
                                                                                
