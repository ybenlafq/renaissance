      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************                00000010
      *   DESCRIPTION DU FICHIER FBS200 LG = 100                        00000020
      **************************************************                00000030
                                                                        00000040
DEB    01  W-ENR-FBS200.                                                00000050
1          02 FBS200-NCODIC         PIC X(07).                          00000060
           02 FBS200-INFOS.                                             00000070
8             03 FBS200-LREFFOURN      PIC X(20).                       00000080
28            03 FBS200-CFAM           PIC X(05).                       00000090
33            03 FBS200-CMARQ          PIC X(05).                       00000100
38            03 FBS200-LSTATCOMP      PIC X(03).                       00000110
41            03 FBS200-CHEFPROD       PIC X(05).                       00000120
46            03 FBS200-CTAUXTVA       PIC X(05).                       00000130
51            03 FBS200-DEFSTATUT      PIC X(08).                       00000140
59            03 FBS200-WSEQED         PIC X(02).                       00000150
61            03 FBS200-NAGREGATED     PIC X(02).                       00000160
62            03 FBS200-LAGREGATED     PIC X(20).                       00000170
83            03 FBS200-CPROFASS       PIC X(05).                       00000180
              03 FILLER                PIC X(13) VALUE SPACES.          00000190
       01  ST-FBS200                PIC 99              VALUE ZERO.     00000200
                                                                                
                                                                        00000210
