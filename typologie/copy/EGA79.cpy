      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA79   EGA79                                              00000020
      ***************************************************************** 00000030
       01   EGA79I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MLREFI    PIC X(20).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFAML    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNFAMF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNFAMI    PIC X(5).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLFAMI    PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMARQL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNMARQF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNMARQI   PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLMARQI   PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MZONCMDI  PIC X(15).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLIBERRI  PIC X(58).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCODTRAI  PIC X(4).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCICSI    PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNETNAMI  PIC X(8).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MSCREENI  PIC X(4).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFLAGHISL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MFLAGHISL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MFLAGHISF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MFLAGHISI      PIC X.                                     00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MPAGEI    PIC X(3).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETOTL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MPAGETOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGETOTF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MPAGETOTI      PIC X(3).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MWFONCI   PIC X(3).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBTAXL   COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MNBTAXL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNBTAXF   PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MNBTAXI   PIC X(3).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBTAXTTL      COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MNBTAXTTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBTAXTTF      PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MNBTAXTTI      PIC X(3).                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MNCODICI  PIC X(7).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTAXL    COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MCTAXL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCTAXF    PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCTAXI    PIC X(5).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTAXL    COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MLTAXL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLTAXF    PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MLTAXI    PIC X(20).                                      00000930
           02 MLIGNESI OCCURS   12 TIMES .                              00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPAYSL  COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MPAYSL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MPAYSF  PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MPAYSI  PIC X(2).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODFOUL     COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MCODFOUL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCODFOUF     PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MCODFOUI     PIC X(5).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEBEFFL     COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MDEBEFFL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEBEFFF     PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MDEBEFFI     PIC X(10).                                 00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFINEFFL     COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MFINEFFL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MFINEFFF     PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MFINEFFI     PIC X(10).                                 00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMONTANL     COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MMONTANL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMONTANF     PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MMONTANI     PIC X(8).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MIMPORTL     COMP PIC S9(4).                            00001150
      *--                                                                       
             03 MIMPORTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MIMPORTF     PIC X.                                     00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MIMPORTI     PIC X.                                     00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODPRDL     COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MCODPRDL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCODPRDF     PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MCODPRDI     PIC X(20).                                 00001220
      ***************************************************************** 00001230
      * SDF: EGA79   EGA79                                              00001240
      ***************************************************************** 00001250
       01   EGA79O REDEFINES EGA79I.                                    00001260
           02 FILLER    PIC X(12).                                      00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MDATJOUA  PIC X.                                          00001290
           02 MDATJOUC  PIC X.                                          00001300
           02 MDATJOUP  PIC X.                                          00001310
           02 MDATJOUH  PIC X.                                          00001320
           02 MDATJOUV  PIC X.                                          00001330
           02 MDATJOUO  PIC X(10).                                      00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MTIMJOUA  PIC X.                                          00001360
           02 MTIMJOUC  PIC X.                                          00001370
           02 MTIMJOUP  PIC X.                                          00001380
           02 MTIMJOUH  PIC X.                                          00001390
           02 MTIMJOUV  PIC X.                                          00001400
           02 MTIMJOUO  PIC X(5).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MLREFA    PIC X.                                          00001430
           02 MLREFC    PIC X.                                          00001440
           02 MLREFP    PIC X.                                          00001450
           02 MLREFH    PIC X.                                          00001460
           02 MLREFV    PIC X.                                          00001470
           02 MLREFO    PIC X(20).                                      00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MNFAMA    PIC X.                                          00001500
           02 MNFAMC    PIC X.                                          00001510
           02 MNFAMP    PIC X.                                          00001520
           02 MNFAMH    PIC X.                                          00001530
           02 MNFAMV    PIC X.                                          00001540
           02 MNFAMO    PIC X(5).                                       00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MLFAMA    PIC X.                                          00001570
           02 MLFAMC    PIC X.                                          00001580
           02 MLFAMP    PIC X.                                          00001590
           02 MLFAMH    PIC X.                                          00001600
           02 MLFAMV    PIC X.                                          00001610
           02 MLFAMO    PIC X(20).                                      00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MNMARQA   PIC X.                                          00001640
           02 MNMARQC   PIC X.                                          00001650
           02 MNMARQP   PIC X.                                          00001660
           02 MNMARQH   PIC X.                                          00001670
           02 MNMARQV   PIC X.                                          00001680
           02 MNMARQO   PIC X(5).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MLMARQA   PIC X.                                          00001710
           02 MLMARQC   PIC X.                                          00001720
           02 MLMARQP   PIC X.                                          00001730
           02 MLMARQH   PIC X.                                          00001740
           02 MLMARQV   PIC X.                                          00001750
           02 MLMARQO   PIC X(20).                                      00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MZONCMDA  PIC X.                                          00001780
           02 MZONCMDC  PIC X.                                          00001790
           02 MZONCMDP  PIC X.                                          00001800
           02 MZONCMDH  PIC X.                                          00001810
           02 MZONCMDV  PIC X.                                          00001820
           02 MZONCMDO  PIC X(15).                                      00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MLIBERRA  PIC X.                                          00001850
           02 MLIBERRC  PIC X.                                          00001860
           02 MLIBERRP  PIC X.                                          00001870
           02 MLIBERRH  PIC X.                                          00001880
           02 MLIBERRV  PIC X.                                          00001890
           02 MLIBERRO  PIC X(58).                                      00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MCODTRAA  PIC X.                                          00001920
           02 MCODTRAC  PIC X.                                          00001930
           02 MCODTRAP  PIC X.                                          00001940
           02 MCODTRAH  PIC X.                                          00001950
           02 MCODTRAV  PIC X.                                          00001960
           02 MCODTRAO  PIC X(4).                                       00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MCICSA    PIC X.                                          00001990
           02 MCICSC    PIC X.                                          00002000
           02 MCICSP    PIC X.                                          00002010
           02 MCICSH    PIC X.                                          00002020
           02 MCICSV    PIC X.                                          00002030
           02 MCICSO    PIC X(5).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MNETNAMA  PIC X.                                          00002060
           02 MNETNAMC  PIC X.                                          00002070
           02 MNETNAMP  PIC X.                                          00002080
           02 MNETNAMH  PIC X.                                          00002090
           02 MNETNAMV  PIC X.                                          00002100
           02 MNETNAMO  PIC X(8).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MSCREENA  PIC X.                                          00002130
           02 MSCREENC  PIC X.                                          00002140
           02 MSCREENP  PIC X.                                          00002150
           02 MSCREENH  PIC X.                                          00002160
           02 MSCREENV  PIC X.                                          00002170
           02 MSCREENO  PIC X(4).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MFLAGHISA      PIC X.                                     00002200
           02 MFLAGHISC PIC X.                                          00002210
           02 MFLAGHISP PIC X.                                          00002220
           02 MFLAGHISH PIC X.                                          00002230
           02 MFLAGHISV PIC X.                                          00002240
           02 MFLAGHISO      PIC X.                                     00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MPAGEA    PIC X.                                          00002270
           02 MPAGEC    PIC X.                                          00002280
           02 MPAGEP    PIC X.                                          00002290
           02 MPAGEH    PIC X.                                          00002300
           02 MPAGEV    PIC X.                                          00002310
           02 MPAGEO    PIC X(3).                                       00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MPAGETOTA      PIC X.                                     00002340
           02 MPAGETOTC PIC X.                                          00002350
           02 MPAGETOTP PIC X.                                          00002360
           02 MPAGETOTH PIC X.                                          00002370
           02 MPAGETOTV PIC X.                                          00002380
           02 MPAGETOTO      PIC X(3).                                  00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MWFONCA   PIC X.                                          00002410
           02 MWFONCC   PIC X.                                          00002420
           02 MWFONCP   PIC X.                                          00002430
           02 MWFONCH   PIC X.                                          00002440
           02 MWFONCV   PIC X.                                          00002450
           02 MWFONCO   PIC X(3).                                       00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MNBTAXA   PIC X.                                          00002480
           02 MNBTAXC   PIC X.                                          00002490
           02 MNBTAXP   PIC X.                                          00002500
           02 MNBTAXH   PIC X.                                          00002510
           02 MNBTAXV   PIC X.                                          00002520
           02 MNBTAXO   PIC X(3).                                       00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MNBTAXTTA      PIC X.                                     00002550
           02 MNBTAXTTC PIC X.                                          00002560
           02 MNBTAXTTP PIC X.                                          00002570
           02 MNBTAXTTH PIC X.                                          00002580
           02 MNBTAXTTV PIC X.                                          00002590
           02 MNBTAXTTO      PIC X(3).                                  00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MNCODICA  PIC X.                                          00002620
           02 MNCODICC  PIC X.                                          00002630
           02 MNCODICP  PIC X.                                          00002640
           02 MNCODICH  PIC X.                                          00002650
           02 MNCODICV  PIC X.                                          00002660
           02 MNCODICO  PIC X(7).                                       00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MCTAXA    PIC X.                                          00002690
           02 MCTAXC    PIC X.                                          00002700
           02 MCTAXP    PIC X.                                          00002710
           02 MCTAXH    PIC X.                                          00002720
           02 MCTAXV    PIC X.                                          00002730
           02 MCTAXO    PIC X(5).                                       00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MLTAXA    PIC X.                                          00002760
           02 MLTAXC    PIC X.                                          00002770
           02 MLTAXP    PIC X.                                          00002780
           02 MLTAXH    PIC X.                                          00002790
           02 MLTAXV    PIC X.                                          00002800
           02 MLTAXO    PIC X(20).                                      00002810
           02 MLIGNESO OCCURS   12 TIMES .                              00002820
             03 FILLER       PIC X(2).                                  00002830
             03 MPAYSA  PIC X.                                          00002840
             03 MPAYSC  PIC X.                                          00002850
             03 MPAYSP  PIC X.                                          00002860
             03 MPAYSH  PIC X.                                          00002870
             03 MPAYSV  PIC X.                                          00002880
             03 MPAYSO  PIC X(2).                                       00002890
             03 FILLER       PIC X(2).                                  00002900
             03 MCODFOUA     PIC X.                                     00002910
             03 MCODFOUC     PIC X.                                     00002920
             03 MCODFOUP     PIC X.                                     00002930
             03 MCODFOUH     PIC X.                                     00002940
             03 MCODFOUV     PIC X.                                     00002950
             03 MCODFOUO     PIC X(5).                                  00002960
             03 FILLER       PIC X(2).                                  00002970
             03 MDEBEFFA     PIC X.                                     00002980
             03 MDEBEFFC     PIC X.                                     00002990
             03 MDEBEFFP     PIC X.                                     00003000
             03 MDEBEFFH     PIC X.                                     00003010
             03 MDEBEFFV     PIC X.                                     00003020
             03 MDEBEFFO     PIC X(10).                                 00003030
             03 FILLER       PIC X(2).                                  00003040
             03 MFINEFFA     PIC X.                                     00003050
             03 MFINEFFC     PIC X.                                     00003060
             03 MFINEFFP     PIC X.                                     00003070
             03 MFINEFFH     PIC X.                                     00003080
             03 MFINEFFV     PIC X.                                     00003090
             03 MFINEFFO     PIC X(10).                                 00003100
             03 FILLER       PIC X(2).                                  00003110
             03 MMONTANA     PIC X.                                     00003120
             03 MMONTANC     PIC X.                                     00003130
             03 MMONTANP     PIC X.                                     00003140
             03 MMONTANH     PIC X.                                     00003150
             03 MMONTANV     PIC X.                                     00003160
             03 MMONTANO     PIC X(8).                                  00003170
             03 FILLER       PIC X(2).                                  00003180
             03 MIMPORTA     PIC X.                                     00003190
             03 MIMPORTC     PIC X.                                     00003200
             03 MIMPORTP     PIC X.                                     00003210
             03 MIMPORTH     PIC X.                                     00003220
             03 MIMPORTV     PIC X.                                     00003230
             03 MIMPORTO     PIC X.                                     00003240
             03 FILLER       PIC X(2).                                  00003250
             03 MCODPRDA     PIC X.                                     00003260
             03 MCODPRDC     PIC X.                                     00003270
             03 MCODPRDP     PIC X.                                     00003280
             03 MCODPRDH     PIC X.                                     00003290
             03 MCODPRDV     PIC X.                                     00003300
             03 MCODPRDO     PIC X(20).                                 00003310
                                                                                
