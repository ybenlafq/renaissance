      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GESTION PARAMETRES OPCO                                         00000020
      ***************************************************************** 00000030
       01   ETH17I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPCOL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MCOPCOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOPCOF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCOPCOI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLOPCOL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MLOPCOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLOPCOF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLOPCOI   PIC X(23).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPCODUPL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MCOPCODUPL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCOPCODUPF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCOPCODUPI     PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLOPCODUPL     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLOPCODUPL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLOPCODUPF     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLOPCODUPI     PIC X(23).                                 00000290
           02 MLIGNEI OCCURS   12 TIMES .                               00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPARAML     COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MCPARAML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCPARAMF     PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MCPARAMI     PIC X(10).                                 00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLPARAML     COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MLPARAML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLPARAMF     PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MLPARAMI     PIC X(15).                                 00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWFLAGL      COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MWFLAGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MWFLAGF      PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MWFLAGI      PIC X.                                     00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLVPARAML    COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MLVPARAML COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLVPARAMF    PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MLVPARAMI    PIC X(48).                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MLIBERRI  PIC X(78).                                      00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MCODTRAI  PIC X(4).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MCICSI    PIC X(5).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MNETNAMI  PIC X(8).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MSCREENI  PIC X(4).                                       00000660
      ***************************************************************** 00000670
      * GESTION PARAMETRES OPCO                                         00000680
      ***************************************************************** 00000690
       01   ETH17O REDEFINES ETH17I.                                    00000700
           02 FILLER    PIC X(12).                                      00000710
           02 FILLER    PIC X(2).                                       00000720
           02 MDATJOUA  PIC X.                                          00000730
           02 MDATJOUC  PIC X.                                          00000740
           02 MDATJOUP  PIC X.                                          00000750
           02 MDATJOUH  PIC X.                                          00000760
           02 MDATJOUV  PIC X.                                          00000770
           02 MDATJOUO  PIC X(10).                                      00000780
           02 FILLER    PIC X(2).                                       00000790
           02 MTIMJOUA  PIC X.                                          00000800
           02 MTIMJOUC  PIC X.                                          00000810
           02 MTIMJOUP  PIC X.                                          00000820
           02 MTIMJOUH  PIC X.                                          00000830
           02 MTIMJOUV  PIC X.                                          00000840
           02 MTIMJOUO  PIC X(5).                                       00000850
           02 FILLER    PIC X(2).                                       00000860
           02 MCOPCOA   PIC X.                                          00000870
           02 MCOPCOC   PIC X.                                          00000880
           02 MCOPCOP   PIC X.                                          00000890
           02 MCOPCOH   PIC X.                                          00000900
           02 MCOPCOV   PIC X.                                          00000910
           02 MCOPCOO   PIC X(3).                                       00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MLOPCOA   PIC X.                                          00000940
           02 MLOPCOC   PIC X.                                          00000950
           02 MLOPCOP   PIC X.                                          00000960
           02 MLOPCOH   PIC X.                                          00000970
           02 MLOPCOV   PIC X.                                          00000980
           02 MLOPCOO   PIC X(23).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MCOPCODUPA     PIC X.                                     00001010
           02 MCOPCODUPC     PIC X.                                     00001020
           02 MCOPCODUPP     PIC X.                                     00001030
           02 MCOPCODUPH     PIC X.                                     00001040
           02 MCOPCODUPV     PIC X.                                     00001050
           02 MCOPCODUPO     PIC X(3).                                  00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MLOPCODUPA     PIC X.                                     00001080
           02 MLOPCODUPC     PIC X.                                     00001090
           02 MLOPCODUPP     PIC X.                                     00001100
           02 MLOPCODUPH     PIC X.                                     00001110
           02 MLOPCODUPV     PIC X.                                     00001120
           02 MLOPCODUPO     PIC X(23).                                 00001130
           02 MLIGNEO OCCURS   12 TIMES .                               00001140
             03 FILLER       PIC X(2).                                  00001150
             03 MCPARAMA     PIC X.                                     00001160
             03 MCPARAMC     PIC X.                                     00001170
             03 MCPARAMP     PIC X.                                     00001180
             03 MCPARAMH     PIC X.                                     00001190
             03 MCPARAMV     PIC X.                                     00001200
             03 MCPARAMO     PIC X(10).                                 00001210
             03 FILLER       PIC X(2).                                  00001220
             03 MLPARAMA     PIC X.                                     00001230
             03 MLPARAMC     PIC X.                                     00001240
             03 MLPARAMP     PIC X.                                     00001250
             03 MLPARAMH     PIC X.                                     00001260
             03 MLPARAMV     PIC X.                                     00001270
             03 MLPARAMO     PIC X(15).                                 00001280
             03 FILLER       PIC X(2).                                  00001290
             03 MWFLAGA      PIC X.                                     00001300
             03 MWFLAGC PIC X.                                          00001310
             03 MWFLAGP PIC X.                                          00001320
             03 MWFLAGH PIC X.                                          00001330
             03 MWFLAGV PIC X.                                          00001340
             03 MWFLAGO      PIC X.                                     00001350
             03 FILLER       PIC X(2).                                  00001360
             03 MLVPARAMA    PIC X.                                     00001370
             03 MLVPARAMC    PIC X.                                     00001380
             03 MLVPARAMP    PIC X.                                     00001390
             03 MLVPARAMH    PIC X.                                     00001400
             03 MLVPARAMV    PIC X.                                     00001410
             03 MLVPARAMO    PIC X(48).                                 00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MLIBERRA  PIC X.                                          00001440
           02 MLIBERRC  PIC X.                                          00001450
           02 MLIBERRP  PIC X.                                          00001460
           02 MLIBERRH  PIC X.                                          00001470
           02 MLIBERRV  PIC X.                                          00001480
           02 MLIBERRO  PIC X(78).                                      00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MCODTRAA  PIC X.                                          00001510
           02 MCODTRAC  PIC X.                                          00001520
           02 MCODTRAP  PIC X.                                          00001530
           02 MCODTRAH  PIC X.                                          00001540
           02 MCODTRAV  PIC X.                                          00001550
           02 MCODTRAO  PIC X(4).                                       00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MCICSA    PIC X.                                          00001580
           02 MCICSC    PIC X.                                          00001590
           02 MCICSP    PIC X.                                          00001600
           02 MCICSH    PIC X.                                          00001610
           02 MCICSV    PIC X.                                          00001620
           02 MCICSO    PIC X(5).                                       00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MNETNAMA  PIC X.                                          00001650
           02 MNETNAMC  PIC X.                                          00001660
           02 MNETNAMP  PIC X.                                          00001670
           02 MNETNAMH  PIC X.                                          00001680
           02 MNETNAMV  PIC X.                                          00001690
           02 MNETNAMO  PIC X(8).                                       00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MSCREENA  PIC X.                                          00001720
           02 MSCREENC  PIC X.                                          00001730
           02 MSCREENP  PIC X.                                          00001740
           02 MSCREENH  PIC X.                                          00001750
           02 MSCREENV  PIC X.                                          00001760
           02 MSCREENO  PIC X(4).                                       00001770
                                                                                
