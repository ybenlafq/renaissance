      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE RABQE TABLE DES LIBELLES BANQUES       *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01J3.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01J3.                                                            
      *}                                                                        
           05  RABQE-CTABLEG2    PIC X(15).                                     
           05  RABQE-CTABLEG2-REDEF REDEFINES RABQE-CTABLEG2.                   
               10  RABQE-NSOCIETE        PIC X(03).                             
               10  RABQE-CBANQ           PIC X(05).                             
           05  RABQE-WTABLEG     PIC X(80).                                     
           05  RABQE-WTABLEG-REDEF  REDEFINES RABQE-WTABLEG.                    
               10  RABQE-LBANQUE         PIC X(20).                             
               10  RABQE-FLAG            PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01J3-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01J3-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RABQE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  RABQE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RABQE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  RABQE-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
