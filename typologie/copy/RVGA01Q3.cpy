      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CTIN3 CODE TRANSACTION EL03 DAL        *        
      *----------------------------------------------------------------*        
       01  RVGA01Q3.                                                            
           05  CTIN3-CTABLEG2    PIC X(15).                                     
           05  CTIN3-CTABLEG2-REDEF REDEFINES CTIN3-CTABLEG2.                   
               10  CTIN3-CTRANS          PIC X(03).                             
           05  CTIN3-WTABLEG     PIC X(80).                                     
           05  CTIN3-WTABLEG-REDEF  REDEFINES CTIN3-WTABLEG.                    
               10  CTIN3-CTRAN           PIC X(01).                             
               10  CTIN3-TYPAP           PIC X(01).                             
               10  CTIN3-LIBTRAN         PIC X(30).                             
               10  CTIN3-LIBCTRAN        PIC X(17).                             
               10  CTIN3-WCOMPTA         PIC X(10).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01Q3-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CTIN3-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CTIN3-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CTIN3-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CTIN3-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
