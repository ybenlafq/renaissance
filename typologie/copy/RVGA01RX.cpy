      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PRZPX ZONES DE PRIX DES PRESTATIONS    *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01RX.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01RX.                                                            
      *}                                                                        
           05  PRZPX-CTABLEG2    PIC X(15).                                     
           05  PRZPX-CTABLEG2-REDEF REDEFINES PRZPX-CTABLEG2.                   
               10  PRZPX-ZPRIX           PIC X(02).                             
           05  PRZPX-WTABLEG     PIC X(80).                                     
           05  PRZPX-WTABLEG-REDEF  REDEFINES PRZPX-WTABLEG.                    
               10  PRZPX-WACTIF          PIC X(01).                             
               10  PRZPX-LZPRIX          PIC X(20).                             
               10  PRZPX-WPRINCIP        PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01RX-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01RX-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRZPX-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PRZPX-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRZPX-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PRZPX-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
