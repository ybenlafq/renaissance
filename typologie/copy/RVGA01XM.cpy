      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE ITFAM FF                               *        
      *----------------------------------------------------------------*        
       01  RVGA01XM.                                                            
           05  ITFAM-CTABLEG2    PIC X(15).                                     
           05  ITFAM-CTABLEG2-REDEF REDEFINES ITFAM-CTABLEG2.                   
               10  ITFAM-CFAM            PIC X(05).                             
           05  ITFAM-WTABLEG     PIC X(80).                                     
           05  ITFAM-WTABLEG-REDEF  REDEFINES ITFAM-WTABLEG.                    
               10  ITFAM-ACTIF           PIC X(01).                             
               10  ITFAM-NBETIQ          PIC X(01).                             
               10  ITFAM-NBETIQ-N       REDEFINES ITFAM-NBETIQ                  
                                         PIC 9(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01XM-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ITFAM-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  ITFAM-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ITFAM-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  ITFAM-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
