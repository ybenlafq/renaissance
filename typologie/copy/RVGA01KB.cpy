      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE VTSTK MAGASIN EDITES SUR BSM140        *        
      *----------------------------------------------------------------*        
       01  RVGA01KB.                                                            
           05  VTSTK-CTABLEG2    PIC X(15).                                     
           05  VTSTK-CTABLEG2-REDEF REDEFINES VTSTK-CTABLEG2.                   
               10  VTSTK-NSOC            PIC X(03).                             
               10  VTSTK-NSOC-N         REDEFINES VTSTK-NSOC                    
                                         PIC 9(03).                             
               10  VTSTK-NLIEU           PIC X(03).                             
               10  VTSTK-NLIEU-N        REDEFINES VTSTK-NLIEU                   
                                         PIC 9(03).                             
           05  VTSTK-WTABLEG     PIC X(80).                                     
           05  VTSTK-WTABLEG-REDEF  REDEFINES VTSTK-WTABLEG.                    
               10  VTSTK-WACTIF          PIC X(01).                             
               10  VTSTK-CGRP            PIC X(02).                             
               10  VTSTK-CGRP-N         REDEFINES VTSTK-CGRP                    
                                         PIC 9(02).                             
               10  VTSTK-NSEQ            PIC X(02).                             
               10  VTSTK-NSEQ-N         REDEFINES VTSTK-NSEQ                    
                                         PIC 9(02).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01KB-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  VTSTK-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  VTSTK-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  VTSTK-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  VTSTK-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
