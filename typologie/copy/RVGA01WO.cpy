      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FLTRT CTYPTRAIT DANS MATRICE AFFIL     *        
      *----------------------------------------------------------------*        
       01  RVGA01WO.                                                            
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  FLTRT-CTABLEG2    PIC X(15).                                     
           05  FLTRT-CTABLEG2-REDEF REDEFINES FLTRT-CTABLEG2.                   
               10  FLTRT-CTYPTRT         PIC X(05).                             
           05  FLTRT-WTABLEG     PIC X(80).                                     
           05  FLTRT-WTABLEG-REDEF  REDEFINES FLTRT-WTABLEG.                    
               10  FLTRT-LTYPTRT         PIC X(20).                             
               10  FLTRT-NPRIORIT        PIC X(05).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  FLTRT-NPRIORIT-N     REDEFINES FLTRT-NPRIORIT                
                                         PIC 9(05).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01WO-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FLTRT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FLTRT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FLTRT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FLTRT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
