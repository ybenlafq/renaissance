      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : GESTION DES CONCURRENTS NATIONAUX      (GN50)          *         
      *****************************************************************         
      *                                                               *         
       01  TS-GN50.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GN50-LONG                 PIC S9(03) COMP-3                    
                                           VALUE +26.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-GN50-DONNEES.                                                  
      *----------------------------------  CODE ACTION                          
              03 TS-GN50-ACT               PIC X.                               
      *----------------------------------  CODE CONCURRENT                      
              03 TS-GN50-NCONC             PIC X(4).                            
      *----------------------------------  LIBELLE CONCURRENT                   
              03 TS-GN50-LCONC             PIC X(20).                           
      *----------------------------------  TYPE CONCURRENT                      
              03 TS-GN50-LIENCO            PIC X(1).                            
                                                                                
