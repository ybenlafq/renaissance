      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGA2900                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA2900                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA2900.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA2900.                                                            
      *}                                                                        
           02  GA29-CETAT                                                       
               PIC X(0010).                                                     
           02  GA29-CFAM                                                        
               PIC X(0005).                                                     
           02  GA29-WSEQED                                                      
               PIC X(0002).                                                     
           02  GA29-NAGREGATED                                                  
               PIC X(0002).                                                     
           02  GA29-LAGREGATED                                                  
               PIC X(0020).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGA2900                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA2900-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA2900-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA29-CETAT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA29-CETAT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA29-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA29-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA29-WSEQED-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA29-WSEQED-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA29-NAGREGATED-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA29-NAGREGATED-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA29-LAGREGATED-F                                                
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GA29-LAGREGATED-F                                                
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
