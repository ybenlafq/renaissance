      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>>>>> DESCRIPTION DE LA TSGA44 <<<<<<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
       01  TS-GA44.                                                             
           05  TS-GA44-LONGUEUR             PIC S9(4) VALUE +220.               
           05  TS-GA44-DATAS.                                                   
               10  TS-GA44-LIGNE            OCCURS 10.                          
                   15  TS-GA44-CINSEE       PIC X(05).                          
                   15  TS-GA44-CPARITE      PIC X(01).                          
                   15  TS-GA44-NDEBTR       PIC X(04).                          
                   15  TS-GA44-NFINTR       PIC X(04).                          
                   15  TS-GA44-CILOT        PIC X(08).                          
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<<<*         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
                                                                                
