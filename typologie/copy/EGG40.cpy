      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGG40   EGG40                                              00000020
      ***************************************************************** 00000030
       01   EGG40I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCI    PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNZPL     COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNZPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNZPF     PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNZPI     PIC X(2).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNCODICI  PIC X(7).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLREFFOURNI    PIC X(20).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCFAMI    PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCMARQI   PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPROL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAPPROF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCAPPROI  PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOANATL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MOANATL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MOANATF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MOANATI   PIC X.                                          00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDOANATL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDOANATL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDOANATF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDOANATI  PIC X(8).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPOL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXPOF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCEXPOI   PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRAL     COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MPRAL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MPRAF     PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MPRAI     PIC X(9).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSENSAPPL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MSENSAPPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSENSAPPF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MSENSAPPI      PIC X.                                     00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOAZPL    COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MOAZPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MOAZPF    PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MOAZPI    PIC X.                                          00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDOAZPL   COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MDOAZPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDOAZPF   PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MDOAZPI   PIC X(8).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATCL   COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MSTATCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSTATCF   PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MSTATCI   PIC X(9).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSRPL     COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MSRPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSRPF     PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MSRPI     PIC X(9).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIB1L    COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MLIB1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIB1F    PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MLIB1I    PIC X(11).                                      00000890
           02 MTABLEI OCCURS   10 TIMES .                               00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOL1L  COMP PIC S9(4).                                 00000910
      *--                                                                       
             03 MCOL1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCOL1F  PIC X.                                          00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MCOL1I  PIC X(3).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIXL  COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MPRIXL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MPRIXF  PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MPRIXI  PIC X(8).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODEPRIXL   COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MCODEPRIXL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCODEPRIXF   PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MCODEPRIXI   PIC X(3).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MDEFFETI     PIC X(8).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMMENT1L   COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MCOMMENT1L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCOMMENT1F   PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MCOMMENT1I   PIC X(17).                                 00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPCOMML      COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MPCOMML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPCOMMF      PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MPCOMMI      PIC X(5).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMMENT2L   COMP PIC S9(4).                            00001150
      *--                                                                       
             03 MCOMMENT2L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCOMMENT2F   PIC X.                                     00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MCOMMENT2I   PIC X(16).                                 00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDPCOMML     COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MDPCOMML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDPCOMMF     PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MDPCOMMI     PIC X(8).                                  00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCONCL   COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MNCONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCONCF   PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MNCONCI   PIC X(4).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCONCL   COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MLCONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLCONCF   PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MLCONCI   PIC X(15).                                      00001300
           02 MNLIEUD OCCURS   20 TIMES .                               00001310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00001320
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00001330
             03 FILLER  PIC X(4).                                       00001340
             03 MNLIEUI      PIC X(3).                                  00001350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRIEXL   COMP PIC S9(4).                                 00001360
      *--                                                                       
           02 MPRIEXL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPRIEXF   PIC X.                                          00001370
           02 FILLER    PIC X(4).                                       00001380
           02 MPRIEXI   PIC X(8).                                       00001390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDPRIEXL      COMP PIC S9(4).                            00001400
      *--                                                                       
           02 MDDPRIEXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDDPRIEXF      PIC X.                                     00001410
           02 FILLER    PIC X(4).                                       00001420
           02 MDDPRIEXI      PIC X(8).                                  00001430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMEXL   COMP PIC S9(4).                                 00001440
      *--                                                                       
           02 MCOMEXL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOMEXF   PIC X.                                          00001450
           02 FILLER    PIC X(4).                                       00001460
           02 MCOMEXI   PIC X(5).                                       00001470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDCOMEXL      COMP PIC S9(4).                            00001480
      *--                                                                       
           02 MDDCOMEXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDDCOMEXF      PIC X.                                     00001490
           02 FILLER    PIC X(4).                                       00001500
           02 MDDCOMEXI      PIC X(8).                                  00001510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTMBLIBL  COMP PIC S9(4).                                 00001520
      *--                                                                       
           02 MTMBLIBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTMBLIBF  PIC X.                                          00001530
           02 FILLER    PIC X(4).                                       00001540
           02 MTMBLIBI  PIC X(7).                                       00001550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPMBUEXL  COMP PIC S9(4).                                 00001560
      *--                                                                       
           02 MPMBUEXL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPMBUEXF  PIC X.                                          00001570
           02 FILLER    PIC X(4).                                       00001580
           02 MPMBUEXI  PIC X(8).                                       00001590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFPRIEXL      COMP PIC S9(4).                            00001600
      *--                                                                       
           02 MDFPRIEXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDFPRIEXF      PIC X.                                     00001610
           02 FILLER    PIC X(4).                                       00001620
           02 MDFPRIEXI      PIC X(8).                                  00001630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPCOMLIBL      COMP PIC S9(4).                            00001640
      *--                                                                       
           02 MPCOMLIBL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPCOMLIBF      PIC X.                                     00001650
           02 FILLER    PIC X(4).                                       00001660
           02 MPCOMLIBI      PIC X(16).                                 00001670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMADAPTL     COMP PIC S9(4).                            00001680
      *--                                                                       
           02 MCOMADAPTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCOMADAPTF     PIC X.                                     00001690
           02 FILLER    PIC X(4).                                       00001700
           02 MCOMADAPTI     PIC X(5).                                  00001710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFCOMEXL      COMP PIC S9(4).                            00001720
      *--                                                                       
           02 MDFCOMEXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDFCOMEXF      PIC X.                                     00001730
           02 FILLER    PIC X(4).                                       00001740
           02 MDFCOMEXI      PIC X(8).                                  00001750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MANNPRIEXL     COMP PIC S9(4).                            00001760
      *--                                                                       
           02 MANNPRIEXL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MANNPRIEXF     PIC X.                                     00001770
           02 FILLER    PIC X(4).                                       00001780
           02 MANNPRIEXI     PIC X.                                     00001790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MANNCOMEXL     COMP PIC S9(4).                            00001800
      *--                                                                       
           02 MANNCOMEXL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MANNCOMEXF     PIC X.                                     00001810
           02 FILLER    PIC X(4).                                       00001820
           02 MANNCOMEXI     PIC X.                                     00001830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001840
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001850
           02 FILLER    PIC X(4).                                       00001860
           02 MLIBERRI  PIC X(79).                                      00001870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001880
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001890
           02 FILLER    PIC X(4).                                       00001900
           02 MCODTRAI  PIC X(4).                                       00001910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001920
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001930
           02 FILLER    PIC X(4).                                       00001940
           02 MCICSI    PIC X(5).                                       00001950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001960
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001970
           02 FILLER    PIC X(4).                                       00001980
           02 MNETNAMI  PIC X(8).                                       00001990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00002000
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00002010
           02 FILLER    PIC X(4).                                       00002020
           02 MSCREENI  PIC X(4).                                       00002030
      ***************************************************************** 00002040
      * SDF: EGG40   EGG40                                              00002050
      ***************************************************************** 00002060
       01   EGG40O REDEFINES EGG40I.                                    00002070
           02 FILLER    PIC X(12).                                      00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MDATJOUA  PIC X.                                          00002100
           02 MDATJOUC  PIC X.                                          00002110
           02 MDATJOUP  PIC X.                                          00002120
           02 MDATJOUH  PIC X.                                          00002130
           02 MDATJOUV  PIC X.                                          00002140
           02 MDATJOUO  PIC X(10).                                      00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MTIMJOUA  PIC X.                                          00002170
           02 MTIMJOUC  PIC X.                                          00002180
           02 MTIMJOUP  PIC X.                                          00002190
           02 MTIMJOUH  PIC X.                                          00002200
           02 MTIMJOUV  PIC X.                                          00002210
           02 MTIMJOUO  PIC X(5).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MPAGEA    PIC X.                                          00002240
           02 MPAGEC    PIC X.                                          00002250
           02 MPAGEP    PIC X.                                          00002260
           02 MPAGEH    PIC X.                                          00002270
           02 MPAGEV    PIC X.                                          00002280
           02 MPAGEO    PIC X(3).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MPAGEMAXA      PIC X.                                     00002310
           02 MPAGEMAXC PIC X.                                          00002320
           02 MPAGEMAXP PIC X.                                          00002330
           02 MPAGEMAXH PIC X.                                          00002340
           02 MPAGEMAXV PIC X.                                          00002350
           02 MPAGEMAXO      PIC X(3).                                  00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MNSOCA    PIC X.                                          00002380
           02 MNSOCC    PIC X.                                          00002390
           02 MNSOCP    PIC X.                                          00002400
           02 MNSOCH    PIC X.                                          00002410
           02 MNSOCV    PIC X.                                          00002420
           02 MNSOCO    PIC X(3).                                       00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MNZPA     PIC X.                                          00002450
           02 MNZPC     PIC X.                                          00002460
           02 MNZPP     PIC X.                                          00002470
           02 MNZPH     PIC X.                                          00002480
           02 MNZPV     PIC X.                                          00002490
           02 MNZPO     PIC X(2).                                       00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MNCODICA  PIC X.                                          00002520
           02 MNCODICC  PIC X.                                          00002530
           02 MNCODICP  PIC X.                                          00002540
           02 MNCODICH  PIC X.                                          00002550
           02 MNCODICV  PIC X.                                          00002560
           02 MNCODICO  PIC X(7).                                       00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MLREFFOURNA    PIC X.                                     00002590
           02 MLREFFOURNC    PIC X.                                     00002600
           02 MLREFFOURNP    PIC X.                                     00002610
           02 MLREFFOURNH    PIC X.                                     00002620
           02 MLREFFOURNV    PIC X.                                     00002630
           02 MLREFFOURNO    PIC X(20).                                 00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MCFAMA    PIC X.                                          00002660
           02 MCFAMC    PIC X.                                          00002670
           02 MCFAMP    PIC X.                                          00002680
           02 MCFAMH    PIC X.                                          00002690
           02 MCFAMV    PIC X.                                          00002700
           02 MCFAMO    PIC X(5).                                       00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MCMARQA   PIC X.                                          00002730
           02 MCMARQC   PIC X.                                          00002740
           02 MCMARQP   PIC X.                                          00002750
           02 MCMARQH   PIC X.                                          00002760
           02 MCMARQV   PIC X.                                          00002770
           02 MCMARQO   PIC X(5).                                       00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MCAPPROA  PIC X.                                          00002800
           02 MCAPPROC  PIC X.                                          00002810
           02 MCAPPROP  PIC X.                                          00002820
           02 MCAPPROH  PIC X.                                          00002830
           02 MCAPPROV  PIC X.                                          00002840
           02 MCAPPROO  PIC X(3).                                       00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MOANATA   PIC X.                                          00002870
           02 MOANATC   PIC X.                                          00002880
           02 MOANATP   PIC X.                                          00002890
           02 MOANATH   PIC X.                                          00002900
           02 MOANATV   PIC X.                                          00002910
           02 MOANATO   PIC X.                                          00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MDOANATA  PIC X.                                          00002940
           02 MDOANATC  PIC X.                                          00002950
           02 MDOANATP  PIC X.                                          00002960
           02 MDOANATH  PIC X.                                          00002970
           02 MDOANATV  PIC X.                                          00002980
           02 MDOANATO  PIC X(8).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MCEXPOA   PIC X.                                          00003010
           02 MCEXPOC   PIC X.                                          00003020
           02 MCEXPOP   PIC X.                                          00003030
           02 MCEXPOH   PIC X.                                          00003040
           02 MCEXPOV   PIC X.                                          00003050
           02 MCEXPOO   PIC X(5).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MPRAA     PIC X.                                          00003080
           02 MPRAC     PIC X.                                          00003090
           02 MPRAP     PIC X.                                          00003100
           02 MPRAH     PIC X.                                          00003110
           02 MPRAV     PIC X.                                          00003120
           02 MPRAO     PIC X(9).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MSENSAPPA      PIC X.                                     00003150
           02 MSENSAPPC PIC X.                                          00003160
           02 MSENSAPPP PIC X.                                          00003170
           02 MSENSAPPH PIC X.                                          00003180
           02 MSENSAPPV PIC X.                                          00003190
           02 MSENSAPPO      PIC X.                                     00003200
           02 FILLER    PIC X(2).                                       00003210
           02 MOAZPA    PIC X.                                          00003220
           02 MOAZPC    PIC X.                                          00003230
           02 MOAZPP    PIC X.                                          00003240
           02 MOAZPH    PIC X.                                          00003250
           02 MOAZPV    PIC X.                                          00003260
           02 MOAZPO    PIC X.                                          00003270
           02 FILLER    PIC X(2).                                       00003280
           02 MDOAZPA   PIC X.                                          00003290
           02 MDOAZPC   PIC X.                                          00003300
           02 MDOAZPP   PIC X.                                          00003310
           02 MDOAZPH   PIC X.                                          00003320
           02 MDOAZPV   PIC X.                                          00003330
           02 MDOAZPO   PIC X(8).                                       00003340
           02 FILLER    PIC X(2).                                       00003350
           02 MSTATCA   PIC X.                                          00003360
           02 MSTATCC   PIC X.                                          00003370
           02 MSTATCP   PIC X.                                          00003380
           02 MSTATCH   PIC X.                                          00003390
           02 MSTATCV   PIC X.                                          00003400
           02 MSTATCO   PIC X(9).                                       00003410
           02 FILLER    PIC X(2).                                       00003420
           02 MSRPA     PIC X.                                          00003430
           02 MSRPC     PIC X.                                          00003440
           02 MSRPP     PIC X.                                          00003450
           02 MSRPH     PIC X.                                          00003460
           02 MSRPV     PIC X.                                          00003470
           02 MSRPO     PIC X(9).                                       00003480
           02 FILLER    PIC X(2).                                       00003490
           02 MLIB1A    PIC X.                                          00003500
           02 MLIB1C    PIC X.                                          00003510
           02 MLIB1P    PIC X.                                          00003520
           02 MLIB1H    PIC X.                                          00003530
           02 MLIB1V    PIC X.                                          00003540
           02 MLIB1O    PIC X(11).                                      00003550
           02 MTABLEO OCCURS   10 TIMES .                               00003560
             03 FILLER       PIC X(2).                                  00003570
             03 MCOL1A  PIC X.                                          00003580
             03 MCOL1C  PIC X.                                          00003590
             03 MCOL1P  PIC X.                                          00003600
             03 MCOL1H  PIC X.                                          00003610
             03 MCOL1V  PIC X.                                          00003620
             03 MCOL1O  PIC X(3).                                       00003630
             03 FILLER       PIC X(2).                                  00003640
             03 MPRIXA  PIC X.                                          00003650
             03 MPRIXC  PIC X.                                          00003660
             03 MPRIXP  PIC X.                                          00003670
             03 MPRIXH  PIC X.                                          00003680
             03 MPRIXV  PIC X.                                          00003690
             03 MPRIXO  PIC X(8).                                       00003700
             03 FILLER       PIC X(2).                                  00003710
             03 MCODEPRIXA   PIC X.                                     00003720
             03 MCODEPRIXC   PIC X.                                     00003730
             03 MCODEPRIXP   PIC X.                                     00003740
             03 MCODEPRIXH   PIC X.                                     00003750
             03 MCODEPRIXV   PIC X.                                     00003760
             03 MCODEPRIXO   PIC X(3).                                  00003770
             03 FILLER       PIC X(2).                                  00003780
             03 MDEFFETA     PIC X.                                     00003790
             03 MDEFFETC     PIC X.                                     00003800
             03 MDEFFETP     PIC X.                                     00003810
             03 MDEFFETH     PIC X.                                     00003820
             03 MDEFFETV     PIC X.                                     00003830
             03 MDEFFETO     PIC X(8).                                  00003840
             03 FILLER       PIC X(2).                                  00003850
             03 MCOMMENT1A   PIC X.                                     00003860
             03 MCOMMENT1C   PIC X.                                     00003870
             03 MCOMMENT1P   PIC X.                                     00003880
             03 MCOMMENT1H   PIC X.                                     00003890
             03 MCOMMENT1V   PIC X.                                     00003900
             03 MCOMMENT1O   PIC X(17).                                 00003910
             03 FILLER       PIC X(2).                                  00003920
             03 MPCOMMA      PIC X.                                     00003930
             03 MPCOMMC PIC X.                                          00003940
             03 MPCOMMP PIC X.                                          00003950
             03 MPCOMMH PIC X.                                          00003960
             03 MPCOMMV PIC X.                                          00003970
             03 MPCOMMO      PIC X(5).                                  00003980
             03 FILLER       PIC X(2).                                  00003990
             03 MCOMMENT2A   PIC X.                                     00004000
             03 MCOMMENT2C   PIC X.                                     00004010
             03 MCOMMENT2P   PIC X.                                     00004020
             03 MCOMMENT2H   PIC X.                                     00004030
             03 MCOMMENT2V   PIC X.                                     00004040
             03 MCOMMENT2O   PIC X(16).                                 00004050
             03 FILLER       PIC X(2).                                  00004060
             03 MDPCOMMA     PIC X.                                     00004070
             03 MDPCOMMC     PIC X.                                     00004080
             03 MDPCOMMP     PIC X.                                     00004090
             03 MDPCOMMH     PIC X.                                     00004100
             03 MDPCOMMV     PIC X.                                     00004110
             03 MDPCOMMO     PIC X(8).                                  00004120
           02 FILLER    PIC X(2).                                       00004130
           02 MNCONCA   PIC X.                                          00004140
           02 MNCONCC   PIC X.                                          00004150
           02 MNCONCP   PIC X.                                          00004160
           02 MNCONCH   PIC X.                                          00004170
           02 MNCONCV   PIC X.                                          00004180
           02 MNCONCO   PIC X(4).                                       00004190
           02 FILLER    PIC X(2).                                       00004200
           02 MLCONCA   PIC X.                                          00004210
           02 MLCONCC   PIC X.                                          00004220
           02 MLCONCP   PIC X.                                          00004230
           02 MLCONCH   PIC X.                                          00004240
           02 MLCONCV   PIC X.                                          00004250
           02 MLCONCO   PIC X(15).                                      00004260
           02 DFHMS1 OCCURS   20 TIMES .                                00004270
             03 FILLER       PIC X(2).                                  00004280
             03 MNLIEUA      PIC X.                                     00004290
             03 MNLIEUC PIC X.                                          00004300
             03 MNLIEUP PIC X.                                          00004310
             03 MNLIEUH PIC X.                                          00004320
             03 MNLIEUV PIC X.                                          00004330
             03 MNLIEUO      PIC X(3).                                  00004340
           02 FILLER    PIC X(2).                                       00004350
           02 MPRIEXA   PIC X.                                          00004360
           02 MPRIEXC   PIC X.                                          00004370
           02 MPRIEXP   PIC X.                                          00004380
           02 MPRIEXH   PIC X.                                          00004390
           02 MPRIEXV   PIC X.                                          00004400
           02 MPRIEXO   PIC X(8).                                       00004410
           02 FILLER    PIC X(2).                                       00004420
           02 MDDPRIEXA      PIC X.                                     00004430
           02 MDDPRIEXC PIC X.                                          00004440
           02 MDDPRIEXP PIC X.                                          00004450
           02 MDDPRIEXH PIC X.                                          00004460
           02 MDDPRIEXV PIC X.                                          00004470
           02 MDDPRIEXO      PIC X(8).                                  00004480
           02 FILLER    PIC X(2).                                       00004490
           02 MCOMEXA   PIC X.                                          00004500
           02 MCOMEXC   PIC X.                                          00004510
           02 MCOMEXP   PIC X.                                          00004520
           02 MCOMEXH   PIC X.                                          00004530
           02 MCOMEXV   PIC X.                                          00004540
           02 MCOMEXO   PIC X(5).                                       00004550
           02 FILLER    PIC X(2).                                       00004560
           02 MDDCOMEXA      PIC X.                                     00004570
           02 MDDCOMEXC PIC X.                                          00004580
           02 MDDCOMEXP PIC X.                                          00004590
           02 MDDCOMEXH PIC X.                                          00004600
           02 MDDCOMEXV PIC X.                                          00004610
           02 MDDCOMEXO      PIC X(8).                                  00004620
           02 FILLER    PIC X(2).                                       00004630
           02 MTMBLIBA  PIC X.                                          00004640
           02 MTMBLIBC  PIC X.                                          00004650
           02 MTMBLIBP  PIC X.                                          00004660
           02 MTMBLIBH  PIC X.                                          00004670
           02 MTMBLIBV  PIC X.                                          00004680
           02 MTMBLIBO  PIC X(7).                                       00004690
           02 FILLER    PIC X(2).                                       00004700
           02 MPMBUEXA  PIC X.                                          00004710
           02 MPMBUEXC  PIC X.                                          00004720
           02 MPMBUEXP  PIC X.                                          00004730
           02 MPMBUEXH  PIC X.                                          00004740
           02 MPMBUEXV  PIC X.                                          00004750
           02 MPMBUEXO  PIC X(8).                                       00004760
           02 FILLER    PIC X(2).                                       00004770
           02 MDFPRIEXA      PIC X.                                     00004780
           02 MDFPRIEXC PIC X.                                          00004790
           02 MDFPRIEXP PIC X.                                          00004800
           02 MDFPRIEXH PIC X.                                          00004810
           02 MDFPRIEXV PIC X.                                          00004820
           02 MDFPRIEXO      PIC X(8).                                  00004830
           02 FILLER    PIC X(2).                                       00004840
           02 MPCOMLIBA      PIC X.                                     00004850
           02 MPCOMLIBC PIC X.                                          00004860
           02 MPCOMLIBP PIC X.                                          00004870
           02 MPCOMLIBH PIC X.                                          00004880
           02 MPCOMLIBV PIC X.                                          00004890
           02 MPCOMLIBO      PIC X(16).                                 00004900
           02 FILLER    PIC X(2).                                       00004910
           02 MCOMADAPTA     PIC X.                                     00004920
           02 MCOMADAPTC     PIC X.                                     00004930
           02 MCOMADAPTP     PIC X.                                     00004940
           02 MCOMADAPTH     PIC X.                                     00004950
           02 MCOMADAPTV     PIC X.                                     00004960
           02 MCOMADAPTO     PIC X(5).                                  00004970
           02 FILLER    PIC X(2).                                       00004980
           02 MDFCOMEXA      PIC X.                                     00004990
           02 MDFCOMEXC PIC X.                                          00005000
           02 MDFCOMEXP PIC X.                                          00005010
           02 MDFCOMEXH PIC X.                                          00005020
           02 MDFCOMEXV PIC X.                                          00005030
           02 MDFCOMEXO      PIC X(8).                                  00005040
           02 FILLER    PIC X(2).                                       00005050
           02 MANNPRIEXA     PIC X.                                     00005060
           02 MANNPRIEXC     PIC X.                                     00005070
           02 MANNPRIEXP     PIC X.                                     00005080
           02 MANNPRIEXH     PIC X.                                     00005090
           02 MANNPRIEXV     PIC X.                                     00005100
           02 MANNPRIEXO     PIC X.                                     00005110
           02 FILLER    PIC X(2).                                       00005120
           02 MANNCOMEXA     PIC X.                                     00005130
           02 MANNCOMEXC     PIC X.                                     00005140
           02 MANNCOMEXP     PIC X.                                     00005150
           02 MANNCOMEXH     PIC X.                                     00005160
           02 MANNCOMEXV     PIC X.                                     00005170
           02 MANNCOMEXO     PIC X.                                     00005180
           02 FILLER    PIC X(2).                                       00005190
           02 MLIBERRA  PIC X.                                          00005200
           02 MLIBERRC  PIC X.                                          00005210
           02 MLIBERRP  PIC X.                                          00005220
           02 MLIBERRH  PIC X.                                          00005230
           02 MLIBERRV  PIC X.                                          00005240
           02 MLIBERRO  PIC X(79).                                      00005250
           02 FILLER    PIC X(2).                                       00005260
           02 MCODTRAA  PIC X.                                          00005270
           02 MCODTRAC  PIC X.                                          00005280
           02 MCODTRAP  PIC X.                                          00005290
           02 MCODTRAH  PIC X.                                          00005300
           02 MCODTRAV  PIC X.                                          00005310
           02 MCODTRAO  PIC X(4).                                       00005320
           02 FILLER    PIC X(2).                                       00005330
           02 MCICSA    PIC X.                                          00005340
           02 MCICSC    PIC X.                                          00005350
           02 MCICSP    PIC X.                                          00005360
           02 MCICSH    PIC X.                                          00005370
           02 MCICSV    PIC X.                                          00005380
           02 MCICSO    PIC X(5).                                       00005390
           02 FILLER    PIC X(2).                                       00005400
           02 MNETNAMA  PIC X.                                          00005410
           02 MNETNAMC  PIC X.                                          00005420
           02 MNETNAMP  PIC X.                                          00005430
           02 MNETNAMH  PIC X.                                          00005440
           02 MNETNAMV  PIC X.                                          00005450
           02 MNETNAMO  PIC X(8).                                       00005460
           02 FILLER    PIC X(2).                                       00005470
           02 MSCREENA  PIC X.                                          00005480
           02 MSCREENC  PIC X.                                          00005490
           02 MSCREENP  PIC X.                                          00005500
           02 MSCREENH  PIC X.                                          00005510
           02 MSCREENV  PIC X.                                          00005520
           02 MSCREENO  PIC X(4).                                       00005530
                                                                                
