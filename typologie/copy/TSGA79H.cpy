      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *===============================================================* 00010000
      *                                                               * 00020000
      *      TS ASSOCIE A LA TRANSACTION GA79                         * 00030004
      *            AJOUT DE L'ECOTAXE AU PRODUIT                      * 00040000
      *            AFFICHAGE HISTORIQUE                               * 00050000
      *===============================================================* 00060000
                                                                        00070000
       01 TS-GA79H.                                                     00080004
          05 TS-GA79H-LONG             PIC S9(5) COMP-3     VALUE +61.  00090004
          05 TS-GA79H-DONNEES.                                          00100004
             10 TS-GA79H-NCODIC        PIC  X(7).                       00120004
             10 TS-GA79H-NSEQ          PIC  X(1).                       00120104
             10 TS-GA79H-CTAXE         PIC  X(5).                       00121004
             10 TS-GA79H-CPAYS         PIC  X(2).                       00122004
             10 TS-GA79H-CODFOU        PIC  X(5).                       00123004
             10 TS-GA79H-DEBEFF        PIC  X(8).                       00130004
             10 TS-GA79H-FINEFF        PIC  X(8).                       00141004
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      10 TS-GA79H-MONTAN        PIC  S9(5)V99 COMP.              00141104
      *--                                                                       
             10 TS-GA79H-MONTAN        PIC  S9(5)V99 COMP-5.                    
      *}                                                                        
             10 TS-GA79H-WIMPORTE      PIC  X(1).                       00230004
             10 TS-GA79H-CODPRD        PIC  X(20).                      00240004
      *===============================================================* 00400000
                                                                                
