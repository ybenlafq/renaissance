      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
       01  Z-COMMAREA-MSE01.                                            00000020
           02 COMM-MSE01-ENTETE-RETOUR.                                 00000030
      *---- BOOLEEN TYPE D'ERREUR                                       00000040
              03 COMM-MSE01-TYPERR     PIC 9(01).                       00000050
                 88 MSE01-NO-ERROR       VALUE 0.                       00000060
                 88 MSE01-APPL-ERROR     VALUE 1.                       00000070
                 88 MSE01-DB2-ERROR      VALUE 2.                       00000080
              03 COMM-MSE01-FUNC-SQL   PIC X(08).                       00000090
              03 COMM-MSE01-TABLE-NAME PIC X(08).                       00000100
              03 COMM-MSE01-SQLCODE    PIC S9(04).                      00000110
              03 COMM-MSE01-POSITION   PIC  9(03).                      00000120
      *---- BOOLEEN POSITIONNEMENT CURSEUR                              00000130
           02 COMM-MSE01-CURSEUR   PIC 9(02).                           00000140
              88 NO-CURSOR           VALUE 0.                           00000150
              88 CSERV-CURS          VALUE 1.                           00000160
              88 CFAM-CURS           VALUE 2.                           00000170
              88 COPER-CURS          VALUE 3.                           00000180
              88 CASSOR-CURS         VALUE 4.                           00000190
      *---- BOOLEEN UTILISATION                                         00000200
           02 COMM-MSE01-UTILISATION  PIC 9.                            00000210
              88 AUTORISATION         VALUE 1.                          00000220
              88 CONTROLE             VALUE 2.                          00000230
                                                                        00000240
           02 COMM-MSE01-DROIT     PIC X(01).                           00000250
           02 COMM-MSE01-CICS      PIC X(05).                           00000260
                                                                        00000270
           02 COMM-MSE01-NCSERV    PIC X(05).                           00000280
           02 COMM-MSE01-LCSERV    PIC X(20).                           00000290
           02 COMM-MSE01-COPER     PIC X(05).                           00000300
           02 COMM-MSE01-LOPER     PIC X(20).                           00000310
           02 COMM-MSE01-CFAM      PIC X(05).                           00000320
           02 COMM-MSE01-LCFAM     PIC X(20).                           00000330
           02 COMM-MSE01-CASSOR    PIC X(05).                           00000340
           02 COMM-MSE01-LASSOR    PIC X(20).                           00000350
           02 COMM-MSE01-FILLER    PIC X(3900).                         00000360
                                                                                
