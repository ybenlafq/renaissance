      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HELII LIEUX DE TRI GHE                 *        
      *----------------------------------------------------------------*        
       01  RVGA01KJ.                                                            
           05  HELII-CTABLEG2    PIC X(15).                                     
           05  HELII-CTABLEG2-REDEF REDEFINES HELII-CTABLEG2.                   
               10  HELII-CLIEUHEI        PIC X(05).                             
           05  HELII-WTABLEG     PIC X(80).                                     
           05  HELII-WTABLEG-REDEF  REDEFINES HELII-WTABLEG.                    
               10  HELII-WACTIF          PIC X(01).                             
               10  HELII-LLIEUHEI        PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01KJ-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HELII-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HELII-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HELII-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HELII-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
