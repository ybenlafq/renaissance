      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA15   EGA15                                              00000020
      ***************************************************************** 00000030
       01   EGA15I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCFAMI    PIC X(5).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLFAMI    PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMULTIL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MMULTIL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MMULTIF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MMULTII   PIC X.                                          00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPENTL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MCTYPENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTYPENTF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCTYPENTI      PIC X(2).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPENTL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLTYPENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLTYPENTF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLTYPENTI      PIC X(20).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSEQL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNSEQL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSEQF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNSEQI    PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTVAL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCTVAL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCTVAF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCTVAI    PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTVAL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLTVAL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLTVAF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLTVAI    PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMODSTL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCMODSTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMODSTF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCMODSTI  PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBPVSL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MQNBPVSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQNBPVSF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MQNBPVSI  PIC X(3).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQBSOLL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MQBSOLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MQBSOLF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MQBSOLI   PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQBVRACL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MQBVRACL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQBVRACF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MQBVRACI  PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMPL   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCFAMPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAMPF   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCFAMPI   PIC X(5).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMSL   COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MCFAMSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAMSF   PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCFAMSI   PIC X(5).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGGRATL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCGGRATL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCGGRATF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCGGRATI  PIC X(5).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLGGRATL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLGGRATL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLGGRATF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLGGRATI  PIC X(20).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGMGDL   COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCGMGDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCGMGDF   PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCGMGDI   PIC X(5).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCGMGDL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MLCGMGDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCGMGDF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MLCGMGDI  PIC X(20).                                      00000890
           02 MCOD-LIBI OCCURS   5 TIMES .                              00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCGCPLTL     COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MCGCPLTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCGCPLTF     PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MCGCPLTI     PIC X(5).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLGCPLTL     COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MLGCPLTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLGCPLTF     PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MLGCPLTI     PIC X(20).                                 00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPL  COMP PIC S9(4).                                 00000990
      *--                                                                       
             03 MCTYPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCTYPF  PIC X.                                          00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MCTYPI  PIC X(5).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLTYPDCLL    COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MLTYPDCLL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLTYPDCLF    PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MLTYPDCLI    PIC X(20).                                 00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MZONCMDI  PIC X(15).                                      00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MLIBERRI  PIC X(58).                                      00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCODTRAI  PIC X(4).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MCICSI    PIC X(5).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MNETNAMI  PIC X(8).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MSCREENI  PIC X(4).                                       00001300
      ***************************************************************** 00001310
      * SDF: EGA15   EGA15                                              00001320
      ***************************************************************** 00001330
       01   EGA15O REDEFINES EGA15I.                                    00001340
           02 FILLER    PIC X(12).                                      00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MDATJOUA  PIC X.                                          00001370
           02 MDATJOUC  PIC X.                                          00001380
           02 MDATJOUP  PIC X.                                          00001390
           02 MDATJOUH  PIC X.                                          00001400
           02 MDATJOUV  PIC X.                                          00001410
           02 MDATJOUO  PIC X(10).                                      00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MTIMJOUA  PIC X.                                          00001440
           02 MTIMJOUC  PIC X.                                          00001450
           02 MTIMJOUP  PIC X.                                          00001460
           02 MTIMJOUH  PIC X.                                          00001470
           02 MTIMJOUV  PIC X.                                          00001480
           02 MTIMJOUO  PIC X(5).                                       00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MWFONCA   PIC X.                                          00001510
           02 MWFONCC   PIC X.                                          00001520
           02 MWFONCP   PIC X.                                          00001530
           02 MWFONCH   PIC X.                                          00001540
           02 MWFONCV   PIC X.                                          00001550
           02 MWFONCO   PIC X(3).                                       00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MCFAMA    PIC X.                                          00001580
           02 MCFAMC    PIC X.                                          00001590
           02 MCFAMP    PIC X.                                          00001600
           02 MCFAMH    PIC X.                                          00001610
           02 MCFAMV    PIC X.                                          00001620
           02 MCFAMO    PIC X(5).                                       00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MLFAMA    PIC X.                                          00001650
           02 MLFAMC    PIC X.                                          00001660
           02 MLFAMP    PIC X.                                          00001670
           02 MLFAMH    PIC X.                                          00001680
           02 MLFAMV    PIC X.                                          00001690
           02 MLFAMO    PIC X(20).                                      00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MMULTIA   PIC X.                                          00001720
           02 MMULTIC   PIC X.                                          00001730
           02 MMULTIP   PIC X.                                          00001740
           02 MMULTIH   PIC X.                                          00001750
           02 MMULTIV   PIC X.                                          00001760
           02 MMULTIO   PIC X.                                          00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MCTYPENTA      PIC X.                                     00001790
           02 MCTYPENTC PIC X.                                          00001800
           02 MCTYPENTP PIC X.                                          00001810
           02 MCTYPENTH PIC X.                                          00001820
           02 MCTYPENTV PIC X.                                          00001830
           02 MCTYPENTO      PIC X(2).                                  00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MLTYPENTA      PIC X.                                     00001860
           02 MLTYPENTC PIC X.                                          00001870
           02 MLTYPENTP PIC X.                                          00001880
           02 MLTYPENTH PIC X.                                          00001890
           02 MLTYPENTV PIC X.                                          00001900
           02 MLTYPENTO      PIC X(20).                                 00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MNSEQA    PIC X.                                          00001930
           02 MNSEQC    PIC X.                                          00001940
           02 MNSEQP    PIC X.                                          00001950
           02 MNSEQH    PIC X.                                          00001960
           02 MNSEQV    PIC X.                                          00001970
           02 MNSEQO    PIC X(5).                                       00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MCTVAA    PIC X.                                          00002000
           02 MCTVAC    PIC X.                                          00002010
           02 MCTVAP    PIC X.                                          00002020
           02 MCTVAH    PIC X.                                          00002030
           02 MCTVAV    PIC X.                                          00002040
           02 MCTVAO    PIC X(5).                                       00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MLTVAA    PIC X.                                          00002070
           02 MLTVAC    PIC X.                                          00002080
           02 MLTVAP    PIC X.                                          00002090
           02 MLTVAH    PIC X.                                          00002100
           02 MLTVAV    PIC X.                                          00002110
           02 MLTVAO    PIC X(20).                                      00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MCMODSTA  PIC X.                                          00002140
           02 MCMODSTC  PIC X.                                          00002150
           02 MCMODSTP  PIC X.                                          00002160
           02 MCMODSTH  PIC X.                                          00002170
           02 MCMODSTV  PIC X.                                          00002180
           02 MCMODSTO  PIC X(5).                                       00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MQNBPVSA  PIC X.                                          00002210
           02 MQNBPVSC  PIC X.                                          00002220
           02 MQNBPVSP  PIC X.                                          00002230
           02 MQNBPVSH  PIC X.                                          00002240
           02 MQNBPVSV  PIC X.                                          00002250
           02 MQNBPVSO  PIC X(3).                                       00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MQBSOLA   PIC X.                                          00002280
           02 MQBSOLC   PIC X.                                          00002290
           02 MQBSOLP   PIC X.                                          00002300
           02 MQBSOLH   PIC X.                                          00002310
           02 MQBSOLV   PIC X.                                          00002320
           02 MQBSOLO   PIC X(5).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MQBVRACA  PIC X.                                          00002350
           02 MQBVRACC  PIC X.                                          00002360
           02 MQBVRACP  PIC X.                                          00002370
           02 MQBVRACH  PIC X.                                          00002380
           02 MQBVRACV  PIC X.                                          00002390
           02 MQBVRACO  PIC X(5).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MCFAMPA   PIC X.                                          00002420
           02 MCFAMPC   PIC X.                                          00002430
           02 MCFAMPP   PIC X.                                          00002440
           02 MCFAMPH   PIC X.                                          00002450
           02 MCFAMPV   PIC X.                                          00002460
           02 MCFAMPO   PIC X(5).                                       00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MCFAMSA   PIC X.                                          00002490
           02 MCFAMSC   PIC X.                                          00002500
           02 MCFAMSP   PIC X.                                          00002510
           02 MCFAMSH   PIC X.                                          00002520
           02 MCFAMSV   PIC X.                                          00002530
           02 MCFAMSO   PIC X(5).                                       00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MCGGRATA  PIC X.                                          00002560
           02 MCGGRATC  PIC X.                                          00002570
           02 MCGGRATP  PIC X.                                          00002580
           02 MCGGRATH  PIC X.                                          00002590
           02 MCGGRATV  PIC X.                                          00002600
           02 MCGGRATO  PIC X(5).                                       00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MLGGRATA  PIC X.                                          00002630
           02 MLGGRATC  PIC X.                                          00002640
           02 MLGGRATP  PIC X.                                          00002650
           02 MLGGRATH  PIC X.                                          00002660
           02 MLGGRATV  PIC X.                                          00002670
           02 MLGGRATO  PIC X(20).                                      00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MCGMGDA   PIC X.                                          00002700
           02 MCGMGDC   PIC X.                                          00002710
           02 MCGMGDP   PIC X.                                          00002720
           02 MCGMGDH   PIC X.                                          00002730
           02 MCGMGDV   PIC X.                                          00002740
           02 MCGMGDO   PIC X(5).                                       00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MLCGMGDA  PIC X.                                          00002770
           02 MLCGMGDC  PIC X.                                          00002780
           02 MLCGMGDP  PIC X.                                          00002790
           02 MLCGMGDH  PIC X.                                          00002800
           02 MLCGMGDV  PIC X.                                          00002810
           02 MLCGMGDO  PIC X(20).                                      00002820
           02 MCOD-LIBO OCCURS   5 TIMES .                              00002830
             03 FILLER       PIC X(2).                                  00002840
             03 MCGCPLTA     PIC X.                                     00002850
             03 MCGCPLTC     PIC X.                                     00002860
             03 MCGCPLTP     PIC X.                                     00002870
             03 MCGCPLTH     PIC X.                                     00002880
             03 MCGCPLTV     PIC X.                                     00002890
             03 MCGCPLTO     PIC X(5).                                  00002900
             03 FILLER       PIC X(2).                                  00002910
             03 MLGCPLTA     PIC X.                                     00002920
             03 MLGCPLTC     PIC X.                                     00002930
             03 MLGCPLTP     PIC X.                                     00002940
             03 MLGCPLTH     PIC X.                                     00002950
             03 MLGCPLTV     PIC X.                                     00002960
             03 MLGCPLTO     PIC X(20).                                 00002970
             03 FILLER       PIC X(2).                                  00002980
             03 MCTYPA  PIC X.                                          00002990
             03 MCTYPC  PIC X.                                          00003000
             03 MCTYPP  PIC X.                                          00003010
             03 MCTYPH  PIC X.                                          00003020
             03 MCTYPV  PIC X.                                          00003030
             03 MCTYPO  PIC X(5).                                       00003040
             03 FILLER       PIC X(2).                                  00003050
             03 MLTYPDCLA    PIC X.                                     00003060
             03 MLTYPDCLC    PIC X.                                     00003070
             03 MLTYPDCLP    PIC X.                                     00003080
             03 MLTYPDCLH    PIC X.                                     00003090
             03 MLTYPDCLV    PIC X.                                     00003100
             03 MLTYPDCLO    PIC X(20).                                 00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MZONCMDA  PIC X.                                          00003130
           02 MZONCMDC  PIC X.                                          00003140
           02 MZONCMDP  PIC X.                                          00003150
           02 MZONCMDH  PIC X.                                          00003160
           02 MZONCMDV  PIC X.                                          00003170
           02 MZONCMDO  PIC X(15).                                      00003180
           02 FILLER    PIC X(2).                                       00003190
           02 MLIBERRA  PIC X.                                          00003200
           02 MLIBERRC  PIC X.                                          00003210
           02 MLIBERRP  PIC X.                                          00003220
           02 MLIBERRH  PIC X.                                          00003230
           02 MLIBERRV  PIC X.                                          00003240
           02 MLIBERRO  PIC X(58).                                      00003250
           02 FILLER    PIC X(2).                                       00003260
           02 MCODTRAA  PIC X.                                          00003270
           02 MCODTRAC  PIC X.                                          00003280
           02 MCODTRAP  PIC X.                                          00003290
           02 MCODTRAH  PIC X.                                          00003300
           02 MCODTRAV  PIC X.                                          00003310
           02 MCODTRAO  PIC X(4).                                       00003320
           02 FILLER    PIC X(2).                                       00003330
           02 MCICSA    PIC X.                                          00003340
           02 MCICSC    PIC X.                                          00003350
           02 MCICSP    PIC X.                                          00003360
           02 MCICSH    PIC X.                                          00003370
           02 MCICSV    PIC X.                                          00003380
           02 MCICSO    PIC X(5).                                       00003390
           02 FILLER    PIC X(2).                                       00003400
           02 MNETNAMA  PIC X.                                          00003410
           02 MNETNAMC  PIC X.                                          00003420
           02 MNETNAMP  PIC X.                                          00003430
           02 MNETNAMH  PIC X.                                          00003440
           02 MNETNAMV  PIC X.                                          00003450
           02 MNETNAMO  PIC X(8).                                       00003460
           02 FILLER    PIC X(2).                                       00003470
           02 MSCREENA  PIC X.                                          00003480
           02 MSCREENC  PIC X.                                          00003490
           02 MSCREENP  PIC X.                                          00003500
           02 MSCREENH  PIC X.                                          00003510
           02 MSCREENV  PIC X.                                          00003520
           02 MSCREENO  PIC X(4).                                       00003530
                                                                                
