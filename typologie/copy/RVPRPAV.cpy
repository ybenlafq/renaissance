      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PRPAV PRPAV PRIMES PRESTA VARIABLES    *        
      *----------------------------------------------------------------*        
       01  RVPRPAV.                                                             
           05  PRPAV-CTABLEG2    PIC X(15).                                     
           05  PRPAV-CTABLEG2-REDEF REDEFINES PRPAV-CTABLEG2.                   
               10  PRPAV-TYPRES          PIC X(05).                             
               10  PRPAV-ZONE            PIC X(02).                             
               10  PRPAV-DEFFET          PIC X(08).                             
               10  PRPAV-DEFFET-N       REDEFINES PRPAV-DEFFET                  
                                         PIC 9(08).                             
           05  PRPAV-WTABLEG     PIC X(80).                                     
           05  PRPAV-WTABLEG-REDEF  REDEFINES PRPAV-WTABLEG.                    
               10  PRPAV-CPRESTAT        PIC X(05).                             
               10  PRPAV-VALEUR          PIC X(06).                             
               10  PRPAV-VALEUR-N       REDEFINES PRPAV-VALEUR                  
                                         PIC 9(03)V9(03).                       
               10  PRPAV-TCALCUL         PIC X(30).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVPRPAV-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRPAV-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PRPAV-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRPAV-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PRPAV-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
