      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00010000
      *==> DARTY ****************************************************** 00020000
      *          * TRAITEMENT DE DATE                                 * 00021000
      *    BATCH * ZONE DE WORKING DU PROGRAMME APPELANT              * 00022000
      *          * ZONE DE LINKAGE DU PROGRAMME APPELE    BETDATC     * 00023000
      *          *                                                    * 00024000
      ************************************************* COPY WORKDATC * 00025000
      *                                                                 00026000
       01  WORK-BETDATC.                                                00027000
      *                                                                 00028000
          02 GFDATA                      PIC X.                         00029000
      *                                                                 00030000
          02 GFJJMMSSAA.                                                00040000
             05 GFJOUR                   PIC XX.                        00050000
             05 GFJJ REDEFINES GFJOUR    PIC 99.                        00060000
             05 GFMOIS                   PIC XX.                        00070000
             05 GFMM REDEFINES GFMOIS    PIC 99.                        00080000
             05 GFSIECLE                 PIC XX.                        00090000
             05 GFSS REDEFINES GFSIECLE  PIC 99.                        00100000
             05 GFANNEE                  PIC XX.                        00110000
             05 GFAA REDEFINES GFANNEE   PIC 99.                        00120000
      *                                                                 00130000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00140000
          02 GFQNTA                      PIC 999.                       00150000
          02 GFQNT0                      PIC 9(5).                      00160000
      *                                                                 00170000
      *   RESULTATS SEMAINE                                             00180000
          02 GFSMN                       PIC 9.                         00190000
          02 GFSMN-LIB-C                 PIC X(3).                      00200000
PTSUP *   02 GFSMN-LIB-L                 PIC X(8).                      00210000
PTADD     02 GFSMN-LIB-L                 PIC X(12).                     00220000
      *                                                                 00230000
      *   LIBELLE MOIS                                                  00240000
          02 GFMOI-LIB-C                 PIC X(3).                      00250000
          02 GFMOI-LIB-L                 PIC X(9).                      00260000
      *                                                                 00270000
      * DIFFERENTES EXPRESSIONS DES DATES                               00280000
      *   FORME SSAAMMJJ                                                00290000
          02 GFSAMJ-0                    PIC X(8).                      00300000
      *   FORME AAMMJJ                                                  00310000
          02 GFAMJ-1                     PIC X(6).                      00320000
      *   FORME JJMMSSAA                                                00330000
          02 GFJMSA-2                    PIC X(8).                      00340000
      *   FORME JJMMAA                                                  00350000
          02 GFJMA-3                     PIC X(6).                      00360000
      *   FORME JJ/MM/AA                                                00370000
          02 GFJMA-4                     PIC X(8).                      00380000
      *   FORME JJ/MM/SSAA                                              00390000
          02 GFJMSA-5                    PIC X(10).                     00400000
      *                                                                 00410000
          02 GFVDAT                      PIC X.                         00420000
          02 GFBISS                      PIC 9.                         00430000
      *                                                                 00440000
          02 GFWEEK.                                                    00450000
             05 GFSEMSS                  PIC 99.                        00460000
             05 GFSEMAA                  PIC 99.                        00470000
             05 GFSEMNU                  PIC 99.                        00480000
      *                                                                 00490000
          02 GFAJOUX                     PIC XX.                        00500000
          02 GFAJOUP REDEFINES GFAJOUX   PIC 99.                        00510000
          02 FILLER                      PIC X(6).                      00520000
      *                                                                 00530000
          02 GF-MESS-ERR                 PIC X(60).                     00540000
                                                                                
