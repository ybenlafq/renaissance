      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE LIVRL ASSOCIATION ZONE LIV/P�RIM�TRE   *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVGA01YE.                                                            
           05  LIVRL-CTABLEG2    PIC X(15).                                     
           05  LIVRL-CTABLEG2-REDEF REDEFINES LIVRL-CTABLEG2.                   
               10  LIVRL-CPERIM          PIC X(05).                             
               10  LIVRL-CZONLIV         PIC X(05).                             
           05  LIVRL-WTABLEG     PIC X(80).                                     
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01YE-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LIVRL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  LIVRL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LIVRL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  LIVRL-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
