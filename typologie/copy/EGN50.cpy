      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF:GESTION DES CONCURRENTS NAT.                                00000020
      ***************************************************************** 00000030
       01   EGN50I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(5).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(5).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(5).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(5).                                       00000200
           02 MPAGEMAXI      PIC X(4).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCONCNL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCONCNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCONCNF  PIC X.                                          00000230
           02 FILLER    PIC X(5).                                       00000240
           02 MNCONCNI  PIC X(4).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCONCNL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLCONCNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCONCNF  PIC X.                                          00000270
           02 FILLER    PIC X(5).                                       00000280
           02 MLCONCNI  PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIENCONL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MLIENCONL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIENCONF      PIC X.                                     00000310
           02 FILLER    PIC X(5).                                       00000320
           02 MLIENCONI      PIC X.                                     00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MICENSEL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MICENSEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MICENSEF  PIC X.                                          00000350
           02 FILLER    PIC X(5).                                       00000360
           02 MICENSEI  PIC X(4).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MILENSEL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MILENSEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MILENSEF  PIC X.                                          00000390
           02 FILLER    PIC X(5).                                       00000400
           02 MILENSEI  PIC X(20).                                      00000410
           02 MACTD OCCURS   14 TIMES .                                 00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTL   COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MACTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MACTF   PIC X.                                          00000440
             03 FILLER  PIC X(5).                                       00000450
             03 MACTI   PIC X.                                          00000460
           02 MNCONCD OCCURS   14 TIMES .                               00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCONCL      COMP PIC S9(4).                            00000480
      *--                                                                       
             03 MNCONCL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNCONCF      PIC X.                                     00000490
             03 FILLER  PIC X(5).                                       00000500
             03 MNCONCI      PIC X(4).                                  00000510
           02 MLCONCD OCCURS   14 TIMES .                               00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCONCL      COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MLCONCL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLCONCF      PIC X.                                     00000540
             03 FILLER  PIC X(5).                                       00000550
             03 MLCONCI      PIC X(20).                                 00000560
           02 MLIENCOD OCCURS   14 TIMES .                              00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIENCOL     COMP PIC S9(4).                            00000580
      *--                                                                       
             03 MLIENCOL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLIENCOF     PIC X.                                     00000590
             03 FILLER  PIC X(5).                                       00000600
             03 MLIENCOI     PIC X.                                     00000610
           02 MCENSED OCCURS   14 TIMES .                               00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCENSEL      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MCENSEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCENSEF      PIC X.                                     00000640
             03 FILLER  PIC X(5).                                       00000650
             03 MCENSEI      PIC X(4).                                  00000660
           02 MLENSED OCCURS   14 TIMES .                               00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLENSEL      COMP PIC S9(4).                            00000680
      *--                                                                       
             03 MLENSEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLENSEF      PIC X.                                     00000690
             03 FILLER  PIC X(5).                                       00000700
             03 MLENSEI      PIC X(20).                                 00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000730
           02 FILLER    PIC X(5).                                       00000740
           02 MLIBERRI  PIC X(78).                                      00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000770
           02 FILLER    PIC X(5).                                       00000780
           02 MCODTRAI  PIC X(4).                                       00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000810
           02 FILLER    PIC X(5).                                       00000820
           02 MCICSI    PIC X(5).                                       00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000850
           02 FILLER    PIC X(5).                                       00000860
           02 MNETNAMI  PIC X(8).                                       00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000890
           02 FILLER    PIC X(5).                                       00000900
           02 MSCREENI  PIC X(4).                                       00000910
      ***************************************************************** 00000920
      * SDF:GESTION DES CONCURRENTS NAT.                                00000930
      ***************************************************************** 00000940
       01   EGN50O REDEFINES EGN50I.                                    00000950
           02 FILLER    PIC X(12).                                      00000960
           02 FILLER    PIC X(2).                                       00000970
           02 MDATJOUA  PIC X.                                          00000980
           02 MDATJOUC  PIC X.                                          00000990
           02 MDATJOUP  PIC X.                                          00001000
           02 MDATJOUH  PIC X.                                          00001010
           02 MDATJOUV  PIC X.                                          00001020
           02 MDATJOUU  PIC X.                                          00001030
           02 MDATJOUO  PIC X(10).                                      00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MTIMJOUA  PIC X.                                          00001060
           02 MTIMJOUC  PIC X.                                          00001070
           02 MTIMJOUP  PIC X.                                          00001080
           02 MTIMJOUH  PIC X.                                          00001090
           02 MTIMJOUV  PIC X.                                          00001100
           02 MTIMJOUU  PIC X.                                          00001110
           02 MTIMJOUO  PIC X(5).                                       00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MPAGEA    PIC X.                                          00001140
           02 MPAGEC    PIC X.                                          00001150
           02 MPAGEP    PIC X.                                          00001160
           02 MPAGEH    PIC X.                                          00001170
           02 MPAGEV    PIC X.                                          00001180
           02 MPAGEU    PIC X.                                          00001190
           02 MPAGEO    PIC X(3).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MPAGEMAXA      PIC X.                                     00001220
           02 MPAGEMAXC PIC X.                                          00001230
           02 MPAGEMAXP PIC X.                                          00001240
           02 MPAGEMAXH PIC X.                                          00001250
           02 MPAGEMAXV PIC X.                                          00001260
           02 MPAGEMAXU PIC X.                                          00001270
           02 MPAGEMAXO      PIC X(4).                                  00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MNCONCNA  PIC X.                                          00001300
           02 MNCONCNC  PIC X.                                          00001310
           02 MNCONCNP  PIC X.                                          00001320
           02 MNCONCNH  PIC X.                                          00001330
           02 MNCONCNV  PIC X.                                          00001340
           02 MNCONCNU  PIC X.                                          00001350
           02 MNCONCNO  PIC X(4).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MLCONCNA  PIC X.                                          00001380
           02 MLCONCNC  PIC X.                                          00001390
           02 MLCONCNP  PIC X.                                          00001400
           02 MLCONCNH  PIC X.                                          00001410
           02 MLCONCNV  PIC X.                                          00001420
           02 MLCONCNU  PIC X.                                          00001430
           02 MLCONCNO  PIC X(20).                                      00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MLIENCONA      PIC X.                                     00001460
           02 MLIENCONC PIC X.                                          00001470
           02 MLIENCONP PIC X.                                          00001480
           02 MLIENCONH PIC X.                                          00001490
           02 MLIENCONV PIC X.                                          00001500
           02 MLIENCONU PIC X.                                          00001510
           02 MLIENCONO      PIC X.                                     00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MICENSEA  PIC X.                                          00001540
           02 MICENSEC  PIC X.                                          00001550
           02 MICENSEP  PIC X.                                          00001560
           02 MICENSEH  PIC X.                                          00001570
           02 MICENSEV  PIC X.                                          00001580
           02 MICENSEU  PIC X.                                          00001590
           02 MICENSEO  PIC X(4).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MILENSEA  PIC X.                                          00001620
           02 MILENSEC  PIC X.                                          00001630
           02 MILENSEP  PIC X.                                          00001640
           02 MILENSEH  PIC X.                                          00001650
           02 MILENSEV  PIC X.                                          00001660
           02 MILENSEU  PIC X.                                          00001670
           02 MILENSEO  PIC X(20).                                      00001680
           02 DFHMS1 OCCURS   14 TIMES .                                00001690
             03 FILLER       PIC X(2).                                  00001700
             03 MACTA   PIC X.                                          00001710
             03 MACTC   PIC X.                                          00001720
             03 MACTP   PIC X.                                          00001730
             03 MACTH   PIC X.                                          00001740
             03 MACTV   PIC X.                                          00001750
             03 MACTU   PIC X.                                          00001760
             03 MACTO   PIC X.                                          00001770
           02 DFHMS2 OCCURS   14 TIMES .                                00001780
             03 FILLER       PIC X(2).                                  00001790
             03 MNCONCA      PIC X.                                     00001800
             03 MNCONCC PIC X.                                          00001810
             03 MNCONCP PIC X.                                          00001820
             03 MNCONCH PIC X.                                          00001830
             03 MNCONCV PIC X.                                          00001840
             03 MNCONCU PIC X.                                          00001850
             03 MNCONCO      PIC X(4).                                  00001860
           02 DFHMS3 OCCURS   14 TIMES .                                00001870
             03 FILLER       PIC X(2).                                  00001880
             03 MLCONCA      PIC X.                                     00001890
             03 MLCONCC PIC X.                                          00001900
             03 MLCONCP PIC X.                                          00001910
             03 MLCONCH PIC X.                                          00001920
             03 MLCONCV PIC X.                                          00001930
             03 MLCONCU PIC X.                                          00001940
             03 MLCONCO      PIC X(20).                                 00001950
           02 DFHMS4 OCCURS   14 TIMES .                                00001960
             03 FILLER       PIC X(2).                                  00001970
             03 MLIENCOA     PIC X.                                     00001980
             03 MLIENCOC     PIC X.                                     00001990
             03 MLIENCOP     PIC X.                                     00002000
             03 MLIENCOH     PIC X.                                     00002010
             03 MLIENCOV     PIC X.                                     00002020
             03 MLIENCOU     PIC X.                                     00002030
             03 MLIENCOO     PIC X.                                     00002040
           02 DFHMS5 OCCURS   14 TIMES .                                00002050
             03 FILLER       PIC X(2).                                  00002060
             03 MCENSEA      PIC X.                                     00002070
             03 MCENSEC PIC X.                                          00002080
             03 MCENSEP PIC X.                                          00002090
             03 MCENSEH PIC X.                                          00002100
             03 MCENSEV PIC X.                                          00002110
             03 MCENSEU PIC X.                                          00002120
             03 MCENSEO      PIC X(4).                                  00002130
           02 DFHMS6 OCCURS   14 TIMES .                                00002140
             03 FILLER       PIC X(2).                                  00002150
             03 MLENSEA      PIC X.                                     00002160
             03 MLENSEC PIC X.                                          00002170
             03 MLENSEP PIC X.                                          00002180
             03 MLENSEH PIC X.                                          00002190
             03 MLENSEV PIC X.                                          00002200
             03 MLENSEU PIC X.                                          00002210
             03 MLENSEO      PIC X(20).                                 00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MLIBERRA  PIC X.                                          00002240
           02 MLIBERRC  PIC X.                                          00002250
           02 MLIBERRP  PIC X.                                          00002260
           02 MLIBERRH  PIC X.                                          00002270
           02 MLIBERRV  PIC X.                                          00002280
           02 MLIBERRU  PIC X.                                          00002290
           02 MLIBERRO  PIC X(78).                                      00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MCODTRAA  PIC X.                                          00002320
           02 MCODTRAC  PIC X.                                          00002330
           02 MCODTRAP  PIC X.                                          00002340
           02 MCODTRAH  PIC X.                                          00002350
           02 MCODTRAV  PIC X.                                          00002360
           02 MCODTRAU  PIC X.                                          00002370
           02 MCODTRAO  PIC X(4).                                       00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MCICSA    PIC X.                                          00002400
           02 MCICSC    PIC X.                                          00002410
           02 MCICSP    PIC X.                                          00002420
           02 MCICSH    PIC X.                                          00002430
           02 MCICSV    PIC X.                                          00002440
           02 MCICSU    PIC X.                                          00002450
           02 MCICSO    PIC X(5).                                       00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MNETNAMA  PIC X.                                          00002480
           02 MNETNAMC  PIC X.                                          00002490
           02 MNETNAMP  PIC X.                                          00002500
           02 MNETNAMH  PIC X.                                          00002510
           02 MNETNAMV  PIC X.                                          00002520
           02 MNETNAMU  PIC X.                                          00002530
           02 MNETNAMO  PIC X(8).                                       00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MSCREENA  PIC X.                                          00002560
           02 MSCREENC  PIC X.                                          00002570
           02 MSCREENP  PIC X.                                          00002580
           02 MSCREENH  PIC X.                                          00002590
           02 MSCREENV  PIC X.                                          00002600
           02 MSCREENU  PIC X.                                          00002610
           02 MSCREENO  PIC X(4).                                       00002620
                                                                                
