      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EXFAC EXCEPTIONS DE FACTURATION        *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01CZ.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01CZ.                                                            
      *}                                                                        
           05  EXFAC-CTABLEG2    PIC X(15).                                     
           05  EXFAC-CTABLEG2-REDEF REDEFINES EXFAC-CTABLEG2.                   
               10  EXFAC-SOC             PIC X(03).                             
               10  EXFAC-MARQUE          PIC X(05).                             
               10  EXFAC-FAMILLE         PIC X(05).                             
           05  EXFAC-WTABLEG     PIC X(80).                                     
           05  EXFAC-WTABLEG-REDEF  REDEFINES EXFAC-WTABLEG.                    
               10  EXFAC-REGLE           PIC X(04).                             
               10  EXFAC-FLAG            PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01CZ-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01CZ-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EXFAC-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EXFAC-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EXFAC-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EXFAC-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
