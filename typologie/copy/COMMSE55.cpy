      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
       01  Z-COMMAREA-MSE55.                                            00000020
           02 COMM-MSE55-ENTETE-RETOUR.                                 00000030
      *---- BOOLEEN TYPE D'ERREUR                                       00000040
              03 COMM-MSE55-TYPERR     PIC 9(01).                       00000050
                 88 MSE55-NO-ERROR       VALUE 0.                       00000060
                 88 MSE55-APPL-ERROR     VALUE 1.                       00000070
                 88 MSE55-DB2-ERROR      VALUE 2.                       00000080
              03 COMM-MSE55-FUNC-SQL   PIC X(08).                       00000090
              03 COMM-MSE55-TABLE-NAME PIC X(08).                       00000100
              03 COMM-MSE55-SQLCODE    PIC S9(04).                      00000110
              03 COMM-MSE55-POSITION   PIC  9(03).                      00000120
      *---- BOOLEEN CODE FONCTION                                       00000130
           02 COMM-MSE55-FONC      PIC 9(01).                           00000140
              88 MSE55-CTRL-DATA     VALUE 0.                           00000150
              88 MSE55-MAJ-DB2       VALUE 1.                           00000160
      *---- DONNEES EN ENTREE / SORTIE                                  00000170
           02 COMM-MSE55-ZINOUT.                                        00000180
              03 COMM-MSE55-CODESFONCTION   PIC X(12).                  00000190
              03 COMM-MSE55-WFONC           PIC X(03).                  00000200
              03 COMM-MSE55-NCSERV          PIC X(05).                  00000210
              03 COMM-MSE55-COPER           PIC X(05).                  00000220
              03 COMM-MSE55-NENTCDE-SAVE    PIC X(05).                  00000230
              03 COMM-MSE55-DMAJ            PIC X(08).                  00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-MSE55-NBR-OCCURS      PIC S9(04)   COMP.          00000250
      *--                                                                       
              03 COMM-MSE55-NBR-OCCURS      PIC S9(04) COMP-5.                  
      *}                                                                        
              03 COMM-MSE55-TABLEAU.                                    00000260
                 05 COMM-MSE55-LIGNE       OCCURS 050.                  00000270
                    10 COMM-MSE55-MNENTCDE  PIC X(05).                  00000280
                    10 COMM-MSE55-MLENTCDE  PIC X(20).                  00000290
                    10 COMM-MSE55-WENTCDE   PIC X(01).                  00000300
                    10 COMM-MSE55-SUPP      PIC X(01).                  00000310
      *------------ CODE RETOUR POSITIONNEMENT DES ERREURS              00000320
                    10 COMM-MSE55-CDE-ERR   PIC X(04).                  00000330
                                                                        00000340
           02 COMM-MSE55-FILLER             PIC X(2481).                00000350
                                                                                
