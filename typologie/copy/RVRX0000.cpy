      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVRX0000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRX0000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRX0000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRX0000.                                                            
      *}                                                                        
           02  RX00-NCONC                                                       
               PIC X(0004).                                                     
           02  RX00-NTYPCONC                                                    
               PIC X(0002).                                                     
           02  RX00-NZONPRIX                                                    
               PIC X(0002).                                                     
           02  RX00-LENSCONC                                                    
               PIC X(0015).                                                     
           02  RX00-LEMPCONC                                                    
               PIC X(0015).                                                     
           02  RX00-LADDR1CONC                                                  
               PIC X(0032).                                                     
           02  RX00-LADDR2CONC                                                  
               PIC X(0032).                                                     
           02  RX00-CPOSTCONC                                                   
               PIC X(0005).                                                     
           02  RX00-LVILLECONC                                                  
               PIC X(0018).                                                     
           02  RX00-DCRECONC                                                    
               PIC X(0008).                                                     
           02  RX00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRX0000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRX0000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRX0000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX00-NCONC-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX00-NCONC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX00-NTYPCONC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX00-NTYPCONC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX00-NZONPRIX-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX00-NZONPRIX-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX00-LENSCONC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX00-LENSCONC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX00-LEMPCONC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX00-LEMPCONC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX00-LADDR1CONC-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX00-LADDR1CONC-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX00-LADDR2CONC-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX00-LADDR2CONC-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX00-CPOSTCONC-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX00-CPOSTCONC-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX00-LVILLECONC-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX00-LVILLECONC-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX00-DCRECONC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX00-DCRECONC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RX00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RX00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
