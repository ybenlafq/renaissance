      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE RX1000                       
      ******************************************************************        
      *                                                                         
       CLEF-RX1000             SECTION.                                         
      *                                                                         
           MOVE 'RVRX1000          '       TO   TABLE-NAME.                     
           MOVE 'RX1000'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-RX1000. EXIT.                                                   
                EJECT                                                           
                                                                                
