      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA44   EGA44                                              00000020
      ***************************************************************** 00000030
       01   EGA44I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMMUNEL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MLCOMMUNEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLCOMMUNEF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLCOMMUNEI     PIC X(32).                                 00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVOIEL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNVOIEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNVOIEF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNVOIEI   PIC X(4).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTVOIEL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCTVOIEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTVOIEF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCTVOIEI  PIC X(4).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOMVOIEL     COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MLNOMVOIEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLNOMVOIEF     PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLNOMVOIEI     PIC X(21).                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOMPVOIEL    COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLNOMPVOIEL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLNOMPVOIEF    PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLNOMPVOIEI    PIC X(42).                                 00000370
           02 MTRONCONSI OCCURS   10 TIMES .                            00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCINSEEL     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MCINSEEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCINSEEF     PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MCINSEEI     PIC X(5).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPARITEL    COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MCPARITEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCPARITEF    PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCPARITEI    PIC X.                                     00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNDEBTRL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNDEBTRL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNDEBTRF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNDEBTRI     PIC X(4).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNFINTRL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MNFINTRL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNFINTRF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNFINTRI     PIC X(4).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCILOTL      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MCILOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCILOTF      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCILOTI      PIC X(8).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MZONCMDI  PIC X(15).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MLIBERRI  PIC X(58).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCODTRAI  PIC X(4).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCICSI    PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MNETNAMI  PIC X(8).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MSCREENI  PIC X(4).                                       00000820
      ***************************************************************** 00000830
      * SDF: EGA44   EGA44                                              00000840
      ***************************************************************** 00000850
       01   EGA44O REDEFINES EGA44I.                                    00000860
           02 FILLER    PIC X(12).                                      00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MDATJOUA  PIC X.                                          00000890
           02 MDATJOUC  PIC X.                                          00000900
           02 MDATJOUP  PIC X.                                          00000910
           02 MDATJOUH  PIC X.                                          00000920
           02 MDATJOUV  PIC X.                                          00000930
           02 MDATJOUO  PIC X(10).                                      00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MTIMJOUA  PIC X.                                          00000960
           02 MTIMJOUC  PIC X.                                          00000970
           02 MTIMJOUP  PIC X.                                          00000980
           02 MTIMJOUH  PIC X.                                          00000990
           02 MTIMJOUV  PIC X.                                          00001000
           02 MTIMJOUO  PIC X(5).                                       00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MPAGEA    PIC X.                                          00001030
           02 MPAGEC    PIC X.                                          00001040
           02 MPAGEP    PIC X.                                          00001050
           02 MPAGEH    PIC X.                                          00001060
           02 MPAGEV    PIC X.                                          00001070
           02 MPAGEO    PIC X(3).                                       00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MLCOMMUNEA     PIC X.                                     00001100
           02 MLCOMMUNEC     PIC X.                                     00001110
           02 MLCOMMUNEP     PIC X.                                     00001120
           02 MLCOMMUNEH     PIC X.                                     00001130
           02 MLCOMMUNEV     PIC X.                                     00001140
           02 MLCOMMUNEO     PIC X(32).                                 00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MNVOIEA   PIC X.                                          00001170
           02 MNVOIEC   PIC X.                                          00001180
           02 MNVOIEP   PIC X.                                          00001190
           02 MNVOIEH   PIC X.                                          00001200
           02 MNVOIEV   PIC X.                                          00001210
           02 MNVOIEO   PIC X(4).                                       00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MCTVOIEA  PIC X.                                          00001240
           02 MCTVOIEC  PIC X.                                          00001250
           02 MCTVOIEP  PIC X.                                          00001260
           02 MCTVOIEH  PIC X.                                          00001270
           02 MCTVOIEV  PIC X.                                          00001280
           02 MCTVOIEO  PIC X(4).                                       00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MLNOMVOIEA     PIC X.                                     00001310
           02 MLNOMVOIEC     PIC X.                                     00001320
           02 MLNOMVOIEP     PIC X.                                     00001330
           02 MLNOMVOIEH     PIC X.                                     00001340
           02 MLNOMVOIEV     PIC X.                                     00001350
           02 MLNOMVOIEO     PIC X(21).                                 00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MLNOMPVOIEA    PIC X.                                     00001380
           02 MLNOMPVOIEC    PIC X.                                     00001390
           02 MLNOMPVOIEP    PIC X.                                     00001400
           02 MLNOMPVOIEH    PIC X.                                     00001410
           02 MLNOMPVOIEV    PIC X.                                     00001420
           02 MLNOMPVOIEO    PIC X(42).                                 00001430
           02 MTRONCONSO OCCURS   10 TIMES .                            00001440
             03 FILLER       PIC X(2).                                  00001450
             03 MCINSEEA     PIC X.                                     00001460
             03 MCINSEEC     PIC X.                                     00001470
             03 MCINSEEP     PIC X.                                     00001480
             03 MCINSEEH     PIC X.                                     00001490
             03 MCINSEEV     PIC X.                                     00001500
             03 MCINSEEO     PIC X(5).                                  00001510
             03 FILLER       PIC X(2).                                  00001520
             03 MCPARITEA    PIC X.                                     00001530
             03 MCPARITEC    PIC X.                                     00001540
             03 MCPARITEP    PIC X.                                     00001550
             03 MCPARITEH    PIC X.                                     00001560
             03 MCPARITEV    PIC X.                                     00001570
             03 MCPARITEO    PIC X.                                     00001580
             03 FILLER       PIC X(2).                                  00001590
             03 MNDEBTRA     PIC X.                                     00001600
             03 MNDEBTRC     PIC X.                                     00001610
             03 MNDEBTRP     PIC X.                                     00001620
             03 MNDEBTRH     PIC X.                                     00001630
             03 MNDEBTRV     PIC X.                                     00001640
             03 MNDEBTRO     PIC X(4).                                  00001650
             03 FILLER       PIC X(2).                                  00001660
             03 MNFINTRA     PIC X.                                     00001670
             03 MNFINTRC     PIC X.                                     00001680
             03 MNFINTRP     PIC X.                                     00001690
             03 MNFINTRH     PIC X.                                     00001700
             03 MNFINTRV     PIC X.                                     00001710
             03 MNFINTRO     PIC X(4).                                  00001720
             03 FILLER       PIC X(2).                                  00001730
             03 MCILOTA      PIC X.                                     00001740
             03 MCILOTC PIC X.                                          00001750
             03 MCILOTP PIC X.                                          00001760
             03 MCILOTH PIC X.                                          00001770
             03 MCILOTV PIC X.                                          00001780
             03 MCILOTO      PIC X(8).                                  00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MZONCMDA  PIC X.                                          00001810
           02 MZONCMDC  PIC X.                                          00001820
           02 MZONCMDP  PIC X.                                          00001830
           02 MZONCMDH  PIC X.                                          00001840
           02 MZONCMDV  PIC X.                                          00001850
           02 MZONCMDO  PIC X(15).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MLIBERRA  PIC X.                                          00001880
           02 MLIBERRC  PIC X.                                          00001890
           02 MLIBERRP  PIC X.                                          00001900
           02 MLIBERRH  PIC X.                                          00001910
           02 MLIBERRV  PIC X.                                          00001920
           02 MLIBERRO  PIC X(58).                                      00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MCODTRAA  PIC X.                                          00001950
           02 MCODTRAC  PIC X.                                          00001960
           02 MCODTRAP  PIC X.                                          00001970
           02 MCODTRAH  PIC X.                                          00001980
           02 MCODTRAV  PIC X.                                          00001990
           02 MCODTRAO  PIC X(4).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MCICSA    PIC X.                                          00002020
           02 MCICSC    PIC X.                                          00002030
           02 MCICSP    PIC X.                                          00002040
           02 MCICSH    PIC X.                                          00002050
           02 MCICSV    PIC X.                                          00002060
           02 MCICSO    PIC X(5).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MNETNAMA  PIC X.                                          00002090
           02 MNETNAMC  PIC X.                                          00002100
           02 MNETNAMP  PIC X.                                          00002110
           02 MNETNAMH  PIC X.                                          00002120
           02 MNETNAMV  PIC X.                                          00002130
           02 MNETNAMO  PIC X(8).                                       00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MSCREENA  PIC X.                                          00002160
           02 MSCREENC  PIC X.                                          00002170
           02 MSCREENP  PIC X.                                          00002180
           02 MSCREENH  PIC X.                                          00002190
           02 MSCREENV  PIC X.                                          00002200
           02 MSCREENO  PIC X(4).                                       00002210
                                                                                
