      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGA300 AU 22/01/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,05,BI,A,                          *        
      *                           25,05,BI,A,                          *        
      *                           30,08,BI,A,                          *        
      *                           38,05,BI,A,                          *        
      *                           43,20,BI,A,                          *        
      *                           63,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGA300.                                                        
            05 NOMETAT-IGA300           PIC X(6) VALUE 'IGA300'.                
            05 RUPTURES-IGA300.                                                 
           10 IGA300-ELATLM             PIC X(03).                      007  003
           10 IGA300-BRUNBLANC          PIC X(05).                      010  005
           10 IGA300-CHEFPROD           PIC X(05).                      015  005
           10 IGA300-WSEQFAM            PIC 9(05).                      020  005
           10 IGA300-CFAM               PIC X(05).                      025  005
           10 IGA300-D1RECEPT           PIC X(08).                      030  008
           10 IGA300-CMARQ              PIC X(05).                      038  005
           10 IGA300-LREFFOURN          PIC X(20).                      043  020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGA300-SEQUENCE           PIC S9(04) COMP.                063  002
      *--                                                                       
           10 IGA300-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGA300.                                                   
           10 IGA300-CAPPRO             PIC X(05).                      065  005
           10 IGA300-ETOILE             PIC X(01).                      070  001
           10 IGA300-LSTATCOMP          PIC X(03).                      071  003
           10 IGA300-NCODIC             PIC X(07).                      074  007
           10 IGA300-DCREATION          PIC X(08).                      081  008
           10 IGA300-DEFSTATUT          PIC X(08).                      089  008
            05 FILLER                      PIC X(416).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGA300-LONG           PIC S9(4)   COMP  VALUE +096.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGA300-LONG           PIC S9(4) COMP-5  VALUE +096.           
                                                                                
      *}                                                                        
