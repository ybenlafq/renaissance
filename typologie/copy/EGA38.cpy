      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA38   EGA38                                              00000020
      ***************************************************************** 00000030
       01   EGA38I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MWFONCI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCODICI  PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLREFI    PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWHISTOL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MWHISTOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWHISTOF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MWHISTOI  PIC X.                                          00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFAML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNFAMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNFAMI    PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLFAMI    PIC X(20).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMARQL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNMARQF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNMARQI   PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLMARQI   PIC X(20).                                      00000530
           02 MWTABLEI OCCURS   12 TIMES .                              00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPAYSL      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MCPAYSL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCPAYSF      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCPAYSI      PIC X(2).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFACL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MDEFFACL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFACF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MDEFFACI     PIC X(8).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFVEL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MDEFFVEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFVEF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MDEFFVEI     PIC X(8).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPMNTACL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MPMNTACL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPMNTACF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MPMNTACI     PIC X(8).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPMNTVEL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MPMNTVEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPMNTVEF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MPMNTVEI     PIC X(8).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFOURNL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MCFOURNL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCFOURNF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCFOURNI     PIC X(5).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLFOURNL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MLFOURNL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLFOURNF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MLFOURNI     PIC X(20).                                 00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWTFOURL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MWTFOURL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWTFOURF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MWTFOURI     PIC X.                                     00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWIMPORL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MWIMPORL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWIMPORF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MWIMPORI     PIC X.                                     00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MZONCMDI  PIC X(15).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLIBERRI  PIC X(58).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCODTRAI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCICSI    PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MNETNAMI  PIC X(8).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MSCREENI  PIC X(4).                                       00001140
      ***************************************************************** 00001150
      * SDF: EGA38   EGA38                                              00001160
      ***************************************************************** 00001170
       01   EGA38O REDEFINES EGA38I.                                    00001180
           02 FILLER    PIC X(12).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTIMJOUA  PIC X.                                          00001280
           02 MTIMJOUC  PIC X.                                          00001290
           02 MTIMJOUP  PIC X.                                          00001300
           02 MTIMJOUH  PIC X.                                          00001310
           02 MTIMJOUV  PIC X.                                          00001320
           02 MTIMJOUO  PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MPAGEA    PIC X.                                          00001350
           02 MPAGEC    PIC X.                                          00001360
           02 MPAGEP    PIC X.                                          00001370
           02 MPAGEH    PIC X.                                          00001380
           02 MPAGEV    PIC X.                                          00001390
           02 MPAGEO    PIC X(3).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MPAGEMAXA      PIC X.                                     00001420
           02 MPAGEMAXC PIC X.                                          00001430
           02 MPAGEMAXP PIC X.                                          00001440
           02 MPAGEMAXH PIC X.                                          00001450
           02 MPAGEMAXV PIC X.                                          00001460
           02 MPAGEMAXO      PIC X(3).                                  00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MWFONCA   PIC X.                                          00001490
           02 MWFONCC   PIC X.                                          00001500
           02 MWFONCP   PIC X.                                          00001510
           02 MWFONCH   PIC X.                                          00001520
           02 MWFONCV   PIC X.                                          00001530
           02 MWFONCO   PIC X(3).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MNCODICA  PIC X.                                          00001560
           02 MNCODICC  PIC X.                                          00001570
           02 MNCODICP  PIC X.                                          00001580
           02 MNCODICH  PIC X.                                          00001590
           02 MNCODICV  PIC X.                                          00001600
           02 MNCODICO  PIC X(7).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MLREFA    PIC X.                                          00001630
           02 MLREFC    PIC X.                                          00001640
           02 MLREFP    PIC X.                                          00001650
           02 MLREFH    PIC X.                                          00001660
           02 MLREFV    PIC X.                                          00001670
           02 MLREFO    PIC X(20).                                      00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MWHISTOA  PIC X.                                          00001700
           02 MWHISTOC  PIC X.                                          00001710
           02 MWHISTOP  PIC X.                                          00001720
           02 MWHISTOH  PIC X.                                          00001730
           02 MWHISTOV  PIC X.                                          00001740
           02 MWHISTOO  PIC X.                                          00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MNFAMA    PIC X.                                          00001770
           02 MNFAMC    PIC X.                                          00001780
           02 MNFAMP    PIC X.                                          00001790
           02 MNFAMH    PIC X.                                          00001800
           02 MNFAMV    PIC X.                                          00001810
           02 MNFAMO    PIC X(5).                                       00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MLFAMA    PIC X.                                          00001840
           02 MLFAMC    PIC X.                                          00001850
           02 MLFAMP    PIC X.                                          00001860
           02 MLFAMH    PIC X.                                          00001870
           02 MLFAMV    PIC X.                                          00001880
           02 MLFAMO    PIC X(20).                                      00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MNMARQA   PIC X.                                          00001910
           02 MNMARQC   PIC X.                                          00001920
           02 MNMARQP   PIC X.                                          00001930
           02 MNMARQH   PIC X.                                          00001940
           02 MNMARQV   PIC X.                                          00001950
           02 MNMARQO   PIC X(5).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MLMARQA   PIC X.                                          00001980
           02 MLMARQC   PIC X.                                          00001990
           02 MLMARQP   PIC X.                                          00002000
           02 MLMARQH   PIC X.                                          00002010
           02 MLMARQV   PIC X.                                          00002020
           02 MLMARQO   PIC X(20).                                      00002030
           02 MWTABLEO OCCURS   12 TIMES .                              00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MCPAYSA      PIC X.                                     00002060
             03 MCPAYSC PIC X.                                          00002070
             03 MCPAYSP PIC X.                                          00002080
             03 MCPAYSH PIC X.                                          00002090
             03 MCPAYSV PIC X.                                          00002100
             03 MCPAYSO      PIC X(2).                                  00002110
             03 FILLER       PIC X(2).                                  00002120
             03 MDEFFACA     PIC X.                                     00002130
             03 MDEFFACC     PIC X.                                     00002140
             03 MDEFFACP     PIC X.                                     00002150
             03 MDEFFACH     PIC X.                                     00002160
             03 MDEFFACV     PIC X.                                     00002170
             03 MDEFFACO     PIC X(8).                                  00002180
             03 FILLER       PIC X(2).                                  00002190
             03 MDEFFVEA     PIC X.                                     00002200
             03 MDEFFVEC     PIC X.                                     00002210
             03 MDEFFVEP     PIC X.                                     00002220
             03 MDEFFVEH     PIC X.                                     00002230
             03 MDEFFVEV     PIC X.                                     00002240
             03 MDEFFVEO     PIC X(8).                                  00002250
             03 FILLER       PIC X(2).                                  00002260
             03 MPMNTACA     PIC X.                                     00002270
             03 MPMNTACC     PIC X.                                     00002280
             03 MPMNTACP     PIC X.                                     00002290
             03 MPMNTACH     PIC X.                                     00002300
             03 MPMNTACV     PIC X.                                     00002310
             03 MPMNTACO     PIC X(8).                                  00002320
             03 FILLER       PIC X(2).                                  00002330
             03 MPMNTVEA     PIC X.                                     00002340
             03 MPMNTVEC     PIC X.                                     00002350
             03 MPMNTVEP     PIC X.                                     00002360
             03 MPMNTVEH     PIC X.                                     00002370
             03 MPMNTVEV     PIC X.                                     00002380
             03 MPMNTVEO     PIC X(8).                                  00002390
             03 FILLER       PIC X(2).                                  00002400
             03 MCFOURNA     PIC X.                                     00002410
             03 MCFOURNC     PIC X.                                     00002420
             03 MCFOURNP     PIC X.                                     00002430
             03 MCFOURNH     PIC X.                                     00002440
             03 MCFOURNV     PIC X.                                     00002450
             03 MCFOURNO     PIC X(5).                                  00002460
             03 FILLER       PIC X(2).                                  00002470
             03 MLFOURNA     PIC X.                                     00002480
             03 MLFOURNC     PIC X.                                     00002490
             03 MLFOURNP     PIC X.                                     00002500
             03 MLFOURNH     PIC X.                                     00002510
             03 MLFOURNV     PIC X.                                     00002520
             03 MLFOURNO     PIC X(20).                                 00002530
             03 FILLER       PIC X(2).                                  00002540
             03 MWTFOURA     PIC X.                                     00002550
             03 MWTFOURC     PIC X.                                     00002560
             03 MWTFOURP     PIC X.                                     00002570
             03 MWTFOURH     PIC X.                                     00002580
             03 MWTFOURV     PIC X.                                     00002590
             03 MWTFOURO     PIC X.                                     00002600
             03 FILLER       PIC X(2).                                  00002610
             03 MWIMPORA     PIC X.                                     00002620
             03 MWIMPORC     PIC X.                                     00002630
             03 MWIMPORP     PIC X.                                     00002640
             03 MWIMPORH     PIC X.                                     00002650
             03 MWIMPORV     PIC X.                                     00002660
             03 MWIMPORO     PIC X.                                     00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MZONCMDA  PIC X.                                          00002690
           02 MZONCMDC  PIC X.                                          00002700
           02 MZONCMDP  PIC X.                                          00002710
           02 MZONCMDH  PIC X.                                          00002720
           02 MZONCMDV  PIC X.                                          00002730
           02 MZONCMDO  PIC X(15).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MLIBERRA  PIC X.                                          00002760
           02 MLIBERRC  PIC X.                                          00002770
           02 MLIBERRP  PIC X.                                          00002780
           02 MLIBERRH  PIC X.                                          00002790
           02 MLIBERRV  PIC X.                                          00002800
           02 MLIBERRO  PIC X(58).                                      00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MCODTRAA  PIC X.                                          00002830
           02 MCODTRAC  PIC X.                                          00002840
           02 MCODTRAP  PIC X.                                          00002850
           02 MCODTRAH  PIC X.                                          00002860
           02 MCODTRAV  PIC X.                                          00002870
           02 MCODTRAO  PIC X(4).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MCICSA    PIC X.                                          00002900
           02 MCICSC    PIC X.                                          00002910
           02 MCICSP    PIC X.                                          00002920
           02 MCICSH    PIC X.                                          00002930
           02 MCICSV    PIC X.                                          00002940
           02 MCICSO    PIC X(5).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MNETNAMA  PIC X.                                          00002970
           02 MNETNAMC  PIC X.                                          00002980
           02 MNETNAMP  PIC X.                                          00002990
           02 MNETNAMH  PIC X.                                          00003000
           02 MNETNAMV  PIC X.                                          00003010
           02 MNETNAMO  PIC X(8).                                       00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MSCREENA  PIC X.                                          00003040
           02 MSCREENC  PIC X.                                          00003050
           02 MSCREENP  PIC X.                                          00003060
           02 MSCREENH  PIC X.                                          00003070
           02 MSCREENV  PIC X.                                          00003080
           02 MSCREENO  PIC X(4).                                       00003090
                                                                                
