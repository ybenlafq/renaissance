      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA72   EGA72                                              00000020
      ***************************************************************** 00000030
       01   EGA72I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * N� DE PAGE                                                      00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MPAGEI    PIC X(3).                                       00000200
      * CODE FONCTIO                                                    00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBFCTL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLIBFCTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBFCTF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLIBFCTI  PIC X(14).                                      00000250
      * N� CONTROLE                                                     00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NNUCON1L  COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 NNUCON1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 NNUCON1F  PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 NNUCON1I  PIC X(3).                                       00000300
      * TYPE CONTR.                                                     00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYCON1L  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MTYCON1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYCON1F  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MTYCON1I  PIC X.                                          00000350
      * BORNE MINI                                                      00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBOMIN1L  COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MBOMIN1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MBOMIN1F  PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MBOMIN1I  PIC X(30).                                      00000400
      * BORNE MAXI                                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBOMAX1L  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MBOMAX1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MBOMAX1F  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MBOMAX1I  PIC X(30).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUCON2L  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNUCON2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNUCON2F  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNUCON2I  PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYCON2L  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MTYCON2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYCON2F  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MTYCON2I  PIC X.                                          00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBOMIN2L  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MBOMIN2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MBOMIN2F  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MBOMIN2I  PIC X(30).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBOMAX2L  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MBOMAX2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MBOMAX2F  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MBOMAX2I  PIC X(30).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUCON3L  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MNUCON3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNUCON3F  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNUCON3I  PIC X(3).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYCON3L  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MTYCON3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYCON3F  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MTYCON3I  PIC X.                                          00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBOMIN3L  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MBOMIN3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MBOMIN3F  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MBOMIN3I  PIC X(30).                                      00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBOMAX3L  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MBOMAX3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MBOMAX3F  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MBOMAX3I  PIC X(30).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUCON4L  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MNUCON4L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNUCON4F  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MNUCON4I  PIC X(3).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYCON4L  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MTYCON4L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYCON4F  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MTYCON4I  PIC X.                                          00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBOMIN4L  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MBOMIN4L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MBOMIN4F  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MBOMIN4I  PIC X(30).                                      00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBOMAX4L  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MBOMAX4L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MBOMAX4F  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MBOMAX4I  PIC X(30).                                      00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUCON5L  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MNUCON5L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNUCON5F  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MNUCON5I  PIC X(3).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYCON5L  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MTYCON5L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYCON5F  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MTYCON5I  PIC X.                                          00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBOMIN5L  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MBOMIN5L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MBOMIN5F  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MBOMIN5I  PIC X(30).                                      00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBOMAX5L  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MBOMAX5L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MBOMAX5F  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MBOMAX5I  PIC X(30).                                      00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUCON6L  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MNUCON6L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNUCON6F  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MNUCON6I  PIC X(3).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYCON6L  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MTYCON6L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYCON6F  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MTYCON6I  PIC X.                                          00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBOMIN6L  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MBOMIN6L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MBOMIN6F  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MBOMIN6I  PIC X(30).                                      00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBOMAX6L  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MBOMAX6L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MBOMAX6F  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MBOMAX6I  PIC X(30).                                      00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUCON7L  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MNUCON7L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNUCON7F  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MNUCON7I  PIC X(3).                                       00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYCON7L  COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MTYCON7L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYCON7F  PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MTYCON7I  PIC X.                                          00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBOMIN7L  COMP PIC S9(4).                                 00001340
      *--                                                                       
           02 MBOMIN7L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MBOMIN7F  PIC X.                                          00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MBOMIN7I  PIC X(30).                                      00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBOMAX7L  COMP PIC S9(4).                                 00001380
      *--                                                                       
           02 MBOMAX7L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MBOMAX7F  PIC X.                                          00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MBOMAX7I  PIC X(30).                                      00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUCON8L  COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MNUCON8L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNUCON8F  PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MNUCON8I  PIC X(3).                                       00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYCON8L  COMP PIC S9(4).                                 00001460
      *--                                                                       
           02 MTYCON8L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYCON8F  PIC X.                                          00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MTYCON8I  PIC X.                                          00001490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBOMIN8L  COMP PIC S9(4).                                 00001500
      *--                                                                       
           02 MBOMIN8L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MBOMIN8F  PIC X.                                          00001510
           02 FILLER    PIC X(4).                                       00001520
           02 MBOMIN8I  PIC X(30).                                      00001530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBOMAX8L  COMP PIC S9(4).                                 00001540
      *--                                                                       
           02 MBOMAX8L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MBOMAX8F  PIC X.                                          00001550
           02 FILLER    PIC X(4).                                       00001560
           02 MBOMAX8I  PIC X(30).                                      00001570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUCON9L  COMP PIC S9(4).                                 00001580
      *--                                                                       
           02 MNUCON9L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNUCON9F  PIC X.                                          00001590
           02 FILLER    PIC X(4).                                       00001600
           02 MNUCON9I  PIC X(3).                                       00001610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYCON9L  COMP PIC S9(4).                                 00001620
      *--                                                                       
           02 MTYCON9L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYCON9F  PIC X.                                          00001630
           02 FILLER    PIC X(4).                                       00001640
           02 MTYCON9I  PIC X.                                          00001650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBOMIN9L  COMP PIC S9(4).                                 00001660
      *--                                                                       
           02 MBOMIN9L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MBOMIN9F  PIC X.                                          00001670
           02 FILLER    PIC X(4).                                       00001680
           02 MBOMIN9I  PIC X(30).                                      00001690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBOMAX9L  COMP PIC S9(4).                                 00001700
      *--                                                                       
           02 MBOMAX9L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MBOMAX9F  PIC X.                                          00001710
           02 FILLER    PIC X(4).                                       00001720
           02 MBOMAX9I  PIC X(30).                                      00001730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUCO10L  COMP PIC S9(4).                                 00001740
      *--                                                                       
           02 MNUCO10L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNUCO10F  PIC X.                                          00001750
           02 FILLER    PIC X(4).                                       00001760
           02 MNUCO10I  PIC X(3).                                       00001770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYCO10L  COMP PIC S9(4).                                 00001780
      *--                                                                       
           02 MTYCO10L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYCO10F  PIC X.                                          00001790
           02 FILLER    PIC X(4).                                       00001800
           02 MTYCO10I  PIC X.                                          00001810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBOMI10L  COMP PIC S9(4).                                 00001820
      *--                                                                       
           02 MBOMI10L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MBOMI10F  PIC X.                                          00001830
           02 FILLER    PIC X(4).                                       00001840
           02 MBOMI10I  PIC X(30).                                      00001850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBOMA10L  COMP PIC S9(4).                                 00001860
      *--                                                                       
           02 MBOMA10L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MBOMA10F  PIC X.                                          00001870
           02 FILLER    PIC X(4).                                       00001880
           02 MBOMA10I  PIC X(30).                                      00001890
      * ZONE CMD AIDA                                                   00001900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001910
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001920
           02 FILLER    PIC X(4).                                       00001930
           02 MZONCMDI  PIC X(15).                                      00001940
      * MESSAGE ERREUR                                                  00001950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001960
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001970
           02 FILLER    PIC X(4).                                       00001980
           02 MLIBERRI  PIC X(58).                                      00001990
      * CODE TRANSACTION                                                00002000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00002010
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00002020
           02 FILLER    PIC X(4).                                       00002030
           02 MCODTRAI  PIC X(4).                                       00002040
      * CICS DE TRAVAIL                                                 00002050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00002060
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00002070
           02 FILLER    PIC X(4).                                       00002080
           02 MCICSI    PIC X(5).                                       00002090
      * NETNAME                                                         00002100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00002110
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00002120
           02 FILLER    PIC X(4).                                       00002130
           02 MNETNAMI  PIC X(8).                                       00002140
      * CODE TERMINAL                                                   00002150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00002160
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00002170
           02 FILLER    PIC X(4).                                       00002180
           02 MSCREENI  PIC X(5).                                       00002190
      ***************************************************************** 00002200
      * SDF: EGA72   EGA72                                              00002210
      ***************************************************************** 00002220
       01   EGA72O REDEFINES EGA72I.                                    00002230
           02 FILLER    PIC X(12).                                      00002240
      * DATE DU JOUR                                                    00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MDATJOUA  PIC X.                                          00002270
           02 MDATJOUC  PIC X.                                          00002280
           02 MDATJOUP  PIC X.                                          00002290
           02 MDATJOUH  PIC X.                                          00002300
           02 MDATJOUV  PIC X.                                          00002310
           02 MDATJOUO  PIC X(10).                                      00002320
      * HEURE                                                           00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MTIMJOUA  PIC X.                                          00002350
           02 MTIMJOUC  PIC X.                                          00002360
           02 MTIMJOUP  PIC X.                                          00002370
           02 MTIMJOUH  PIC X.                                          00002380
           02 MTIMJOUV  PIC X.                                          00002390
           02 MTIMJOUO  PIC X(5).                                       00002400
      * N� DE PAGE                                                      00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MPAGEA    PIC X.                                          00002430
           02 MPAGEC    PIC X.                                          00002440
           02 MPAGEP    PIC X.                                          00002450
           02 MPAGEH    PIC X.                                          00002460
           02 MPAGEV    PIC X.                                          00002470
           02 MPAGEO    PIC X(3).                                       00002480
      * CODE FONCTIO                                                    00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MLIBFCTA  PIC X.                                          00002510
           02 MLIBFCTC  PIC X.                                          00002520
           02 MLIBFCTP  PIC X.                                          00002530
           02 MLIBFCTH  PIC X.                                          00002540
           02 MLIBFCTV  PIC X.                                          00002550
           02 MLIBFCTO  PIC X(14).                                      00002560
      * N� CONTROLE                                                     00002570
           02 FILLER    PIC X(2).                                       00002580
           02 NNUCON1A  PIC X.                                          00002590
           02 NNUCON1C  PIC X.                                          00002600
           02 NNUCON1P  PIC X.                                          00002610
           02 NNUCON1H  PIC X.                                          00002620
           02 NNUCON1V  PIC X.                                          00002630
           02 NNUCON1O  PIC X(3).                                       00002640
      * TYPE CONTR.                                                     00002650
           02 FILLER    PIC X(2).                                       00002660
           02 MTYCON1A  PIC X.                                          00002670
           02 MTYCON1C  PIC X.                                          00002680
           02 MTYCON1P  PIC X.                                          00002690
           02 MTYCON1H  PIC X.                                          00002700
           02 MTYCON1V  PIC X.                                          00002710
           02 MTYCON1O  PIC X.                                          00002720
      * BORNE MINI                                                      00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MBOMIN1A  PIC X.                                          00002750
           02 MBOMIN1C  PIC X.                                          00002760
           02 MBOMIN1P  PIC X.                                          00002770
           02 MBOMIN1H  PIC X.                                          00002780
           02 MBOMIN1V  PIC X.                                          00002790
           02 MBOMIN1O  PIC X(30).                                      00002800
      * BORNE MAXI                                                      00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MBOMAX1A  PIC X.                                          00002830
           02 MBOMAX1C  PIC X.                                          00002840
           02 MBOMAX1P  PIC X.                                          00002850
           02 MBOMAX1H  PIC X.                                          00002860
           02 MBOMAX1V  PIC X.                                          00002870
           02 MBOMAX1O  PIC X(30).                                      00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MNUCON2A  PIC X.                                          00002900
           02 MNUCON2C  PIC X.                                          00002910
           02 MNUCON2P  PIC X.                                          00002920
           02 MNUCON2H  PIC X.                                          00002930
           02 MNUCON2V  PIC X.                                          00002940
           02 MNUCON2O  PIC X(3).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MTYCON2A  PIC X.                                          00002970
           02 MTYCON2C  PIC X.                                          00002980
           02 MTYCON2P  PIC X.                                          00002990
           02 MTYCON2H  PIC X.                                          00003000
           02 MTYCON2V  PIC X.                                          00003010
           02 MTYCON2O  PIC X.                                          00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MBOMIN2A  PIC X.                                          00003040
           02 MBOMIN2C  PIC X.                                          00003050
           02 MBOMIN2P  PIC X.                                          00003060
           02 MBOMIN2H  PIC X.                                          00003070
           02 MBOMIN2V  PIC X.                                          00003080
           02 MBOMIN2O  PIC X(30).                                      00003090
           02 FILLER    PIC X(2).                                       00003100
           02 MBOMAX2A  PIC X.                                          00003110
           02 MBOMAX2C  PIC X.                                          00003120
           02 MBOMAX2P  PIC X.                                          00003130
           02 MBOMAX2H  PIC X.                                          00003140
           02 MBOMAX2V  PIC X.                                          00003150
           02 MBOMAX2O  PIC X(30).                                      00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MNUCON3A  PIC X.                                          00003180
           02 MNUCON3C  PIC X.                                          00003190
           02 MNUCON3P  PIC X.                                          00003200
           02 MNUCON3H  PIC X.                                          00003210
           02 MNUCON3V  PIC X.                                          00003220
           02 MNUCON3O  PIC X(3).                                       00003230
           02 FILLER    PIC X(2).                                       00003240
           02 MTYCON3A  PIC X.                                          00003250
           02 MTYCON3C  PIC X.                                          00003260
           02 MTYCON3P  PIC X.                                          00003270
           02 MTYCON3H  PIC X.                                          00003280
           02 MTYCON3V  PIC X.                                          00003290
           02 MTYCON3O  PIC X.                                          00003300
           02 FILLER    PIC X(2).                                       00003310
           02 MBOMIN3A  PIC X.                                          00003320
           02 MBOMIN3C  PIC X.                                          00003330
           02 MBOMIN3P  PIC X.                                          00003340
           02 MBOMIN3H  PIC X.                                          00003350
           02 MBOMIN3V  PIC X.                                          00003360
           02 MBOMIN3O  PIC X(30).                                      00003370
           02 FILLER    PIC X(2).                                       00003380
           02 MBOMAX3A  PIC X.                                          00003390
           02 MBOMAX3C  PIC X.                                          00003400
           02 MBOMAX3P  PIC X.                                          00003410
           02 MBOMAX3H  PIC X.                                          00003420
           02 MBOMAX3V  PIC X.                                          00003430
           02 MBOMAX3O  PIC X(30).                                      00003440
           02 FILLER    PIC X(2).                                       00003450
           02 MNUCON4A  PIC X.                                          00003460
           02 MNUCON4C  PIC X.                                          00003470
           02 MNUCON4P  PIC X.                                          00003480
           02 MNUCON4H  PIC X.                                          00003490
           02 MNUCON4V  PIC X.                                          00003500
           02 MNUCON4O  PIC X(3).                                       00003510
           02 FILLER    PIC X(2).                                       00003520
           02 MTYCON4A  PIC X.                                          00003530
           02 MTYCON4C  PIC X.                                          00003540
           02 MTYCON4P  PIC X.                                          00003550
           02 MTYCON4H  PIC X.                                          00003560
           02 MTYCON4V  PIC X.                                          00003570
           02 MTYCON4O  PIC X.                                          00003580
           02 FILLER    PIC X(2).                                       00003590
           02 MBOMIN4A  PIC X.                                          00003600
           02 MBOMIN4C  PIC X.                                          00003610
           02 MBOMIN4P  PIC X.                                          00003620
           02 MBOMIN4H  PIC X.                                          00003630
           02 MBOMIN4V  PIC X.                                          00003640
           02 MBOMIN4O  PIC X(30).                                      00003650
           02 FILLER    PIC X(2).                                       00003660
           02 MBOMAX4A  PIC X.                                          00003670
           02 MBOMAX4C  PIC X.                                          00003680
           02 MBOMAX4P  PIC X.                                          00003690
           02 MBOMAX4H  PIC X.                                          00003700
           02 MBOMAX4V  PIC X.                                          00003710
           02 MBOMAX4O  PIC X(30).                                      00003720
           02 FILLER    PIC X(2).                                       00003730
           02 MNUCON5A  PIC X.                                          00003740
           02 MNUCON5C  PIC X.                                          00003750
           02 MNUCON5P  PIC X.                                          00003760
           02 MNUCON5H  PIC X.                                          00003770
           02 MNUCON5V  PIC X.                                          00003780
           02 MNUCON5O  PIC X(3).                                       00003790
           02 FILLER    PIC X(2).                                       00003800
           02 MTYCON5A  PIC X.                                          00003810
           02 MTYCON5C  PIC X.                                          00003820
           02 MTYCON5P  PIC X.                                          00003830
           02 MTYCON5H  PIC X.                                          00003840
           02 MTYCON5V  PIC X.                                          00003850
           02 MTYCON5O  PIC X.                                          00003860
           02 FILLER    PIC X(2).                                       00003870
           02 MBOMIN5A  PIC X.                                          00003880
           02 MBOMIN5C  PIC X.                                          00003890
           02 MBOMIN5P  PIC X.                                          00003900
           02 MBOMIN5H  PIC X.                                          00003910
           02 MBOMIN5V  PIC X.                                          00003920
           02 MBOMIN5O  PIC X(30).                                      00003930
           02 FILLER    PIC X(2).                                       00003940
           02 MBOMAX5A  PIC X.                                          00003950
           02 MBOMAX5C  PIC X.                                          00003960
           02 MBOMAX5P  PIC X.                                          00003970
           02 MBOMAX5H  PIC X.                                          00003980
           02 MBOMAX5V  PIC X.                                          00003990
           02 MBOMAX5O  PIC X(30).                                      00004000
           02 FILLER    PIC X(2).                                       00004010
           02 MNUCON6A  PIC X.                                          00004020
           02 MNUCON6C  PIC X.                                          00004030
           02 MNUCON6P  PIC X.                                          00004040
           02 MNUCON6H  PIC X.                                          00004050
           02 MNUCON6V  PIC X.                                          00004060
           02 MNUCON6O  PIC X(3).                                       00004070
           02 FILLER    PIC X(2).                                       00004080
           02 MTYCON6A  PIC X.                                          00004090
           02 MTYCON6C  PIC X.                                          00004100
           02 MTYCON6P  PIC X.                                          00004110
           02 MTYCON6H  PIC X.                                          00004120
           02 MTYCON6V  PIC X.                                          00004130
           02 MTYCON6O  PIC X.                                          00004140
           02 FILLER    PIC X(2).                                       00004150
           02 MBOMIN6A  PIC X.                                          00004160
           02 MBOMIN6C  PIC X.                                          00004170
           02 MBOMIN6P  PIC X.                                          00004180
           02 MBOMIN6H  PIC X.                                          00004190
           02 MBOMIN6V  PIC X.                                          00004200
           02 MBOMIN6O  PIC X(30).                                      00004210
           02 FILLER    PIC X(2).                                       00004220
           02 MBOMAX6A  PIC X.                                          00004230
           02 MBOMAX6C  PIC X.                                          00004240
           02 MBOMAX6P  PIC X.                                          00004250
           02 MBOMAX6H  PIC X.                                          00004260
           02 MBOMAX6V  PIC X.                                          00004270
           02 MBOMAX6O  PIC X(30).                                      00004280
           02 FILLER    PIC X(2).                                       00004290
           02 MNUCON7A  PIC X.                                          00004300
           02 MNUCON7C  PIC X.                                          00004310
           02 MNUCON7P  PIC X.                                          00004320
           02 MNUCON7H  PIC X.                                          00004330
           02 MNUCON7V  PIC X.                                          00004340
           02 MNUCON7O  PIC X(3).                                       00004350
           02 FILLER    PIC X(2).                                       00004360
           02 MTYCON7A  PIC X.                                          00004370
           02 MTYCON7C  PIC X.                                          00004380
           02 MTYCON7P  PIC X.                                          00004390
           02 MTYCON7H  PIC X.                                          00004400
           02 MTYCON7V  PIC X.                                          00004410
           02 MTYCON7O  PIC X.                                          00004420
           02 FILLER    PIC X(2).                                       00004430
           02 MBOMIN7A  PIC X.                                          00004440
           02 MBOMIN7C  PIC X.                                          00004450
           02 MBOMIN7P  PIC X.                                          00004460
           02 MBOMIN7H  PIC X.                                          00004470
           02 MBOMIN7V  PIC X.                                          00004480
           02 MBOMIN7O  PIC X(30).                                      00004490
           02 FILLER    PIC X(2).                                       00004500
           02 MBOMAX7A  PIC X.                                          00004510
           02 MBOMAX7C  PIC X.                                          00004520
           02 MBOMAX7P  PIC X.                                          00004530
           02 MBOMAX7H  PIC X.                                          00004540
           02 MBOMAX7V  PIC X.                                          00004550
           02 MBOMAX7O  PIC X(30).                                      00004560
           02 FILLER    PIC X(2).                                       00004570
           02 MNUCON8A  PIC X.                                          00004580
           02 MNUCON8C  PIC X.                                          00004590
           02 MNUCON8P  PIC X.                                          00004600
           02 MNUCON8H  PIC X.                                          00004610
           02 MNUCON8V  PIC X.                                          00004620
           02 MNUCON8O  PIC X(3).                                       00004630
           02 FILLER    PIC X(2).                                       00004640
           02 MTYCON8A  PIC X.                                          00004650
           02 MTYCON8C  PIC X.                                          00004660
           02 MTYCON8P  PIC X.                                          00004670
           02 MTYCON8H  PIC X.                                          00004680
           02 MTYCON8V  PIC X.                                          00004690
           02 MTYCON8O  PIC X.                                          00004700
           02 FILLER    PIC X(2).                                       00004710
           02 MBOMIN8A  PIC X.                                          00004720
           02 MBOMIN8C  PIC X.                                          00004730
           02 MBOMIN8P  PIC X.                                          00004740
           02 MBOMIN8H  PIC X.                                          00004750
           02 MBOMIN8V  PIC X.                                          00004760
           02 MBOMIN8O  PIC X(30).                                      00004770
           02 FILLER    PIC X(2).                                       00004780
           02 MBOMAX8A  PIC X.                                          00004790
           02 MBOMAX8C  PIC X.                                          00004800
           02 MBOMAX8P  PIC X.                                          00004810
           02 MBOMAX8H  PIC X.                                          00004820
           02 MBOMAX8V  PIC X.                                          00004830
           02 MBOMAX8O  PIC X(30).                                      00004840
           02 FILLER    PIC X(2).                                       00004850
           02 MNUCON9A  PIC X.                                          00004860
           02 MNUCON9C  PIC X.                                          00004870
           02 MNUCON9P  PIC X.                                          00004880
           02 MNUCON9H  PIC X.                                          00004890
           02 MNUCON9V  PIC X.                                          00004900
           02 MNUCON9O  PIC X(3).                                       00004910
           02 FILLER    PIC X(2).                                       00004920
           02 MTYCON9A  PIC X.                                          00004930
           02 MTYCON9C  PIC X.                                          00004940
           02 MTYCON9P  PIC X.                                          00004950
           02 MTYCON9H  PIC X.                                          00004960
           02 MTYCON9V  PIC X.                                          00004970
           02 MTYCON9O  PIC X.                                          00004980
           02 FILLER    PIC X(2).                                       00004990
           02 MBOMIN9A  PIC X.                                          00005000
           02 MBOMIN9C  PIC X.                                          00005010
           02 MBOMIN9P  PIC X.                                          00005020
           02 MBOMIN9H  PIC X.                                          00005030
           02 MBOMIN9V  PIC X.                                          00005040
           02 MBOMIN9O  PIC X(30).                                      00005050
           02 FILLER    PIC X(2).                                       00005060
           02 MBOMAX9A  PIC X.                                          00005070
           02 MBOMAX9C  PIC X.                                          00005080
           02 MBOMAX9P  PIC X.                                          00005090
           02 MBOMAX9H  PIC X.                                          00005100
           02 MBOMAX9V  PIC X.                                          00005110
           02 MBOMAX9O  PIC X(30).                                      00005120
           02 FILLER    PIC X(2).                                       00005130
           02 MNUCO10A  PIC X.                                          00005140
           02 MNUCO10C  PIC X.                                          00005150
           02 MNUCO10P  PIC X.                                          00005160
           02 MNUCO10H  PIC X.                                          00005170
           02 MNUCO10V  PIC X.                                          00005180
           02 MNUCO10O  PIC X(3).                                       00005190
           02 FILLER    PIC X(2).                                       00005200
           02 MTYCO10A  PIC X.                                          00005210
           02 MTYCO10C  PIC X.                                          00005220
           02 MTYCO10P  PIC X.                                          00005230
           02 MTYCO10H  PIC X.                                          00005240
           02 MTYCO10V  PIC X.                                          00005250
           02 MTYCO10O  PIC X.                                          00005260
           02 FILLER    PIC X(2).                                       00005270
           02 MBOMI10A  PIC X.                                          00005280
           02 MBOMI10C  PIC X.                                          00005290
           02 MBOMI10P  PIC X.                                          00005300
           02 MBOMI10H  PIC X.                                          00005310
           02 MBOMI10V  PIC X.                                          00005320
           02 MBOMI10O  PIC X(30).                                      00005330
           02 FILLER    PIC X(2).                                       00005340
           02 MBOMA10A  PIC X.                                          00005350
           02 MBOMA10C  PIC X.                                          00005360
           02 MBOMA10P  PIC X.                                          00005370
           02 MBOMA10H  PIC X.                                          00005380
           02 MBOMA10V  PIC X.                                          00005390
           02 MBOMA10O  PIC X(30).                                      00005400
      * ZONE CMD AIDA                                                   00005410
           02 FILLER    PIC X(2).                                       00005420
           02 MZONCMDA  PIC X.                                          00005430
           02 MZONCMDC  PIC X.                                          00005440
           02 MZONCMDP  PIC X.                                          00005450
           02 MZONCMDH  PIC X.                                          00005460
           02 MZONCMDV  PIC X.                                          00005470
           02 MZONCMDO  PIC X(15).                                      00005480
      * MESSAGE ERREUR                                                  00005490
           02 FILLER    PIC X(2).                                       00005500
           02 MLIBERRA  PIC X.                                          00005510
           02 MLIBERRC  PIC X.                                          00005520
           02 MLIBERRP  PIC X.                                          00005530
           02 MLIBERRH  PIC X.                                          00005540
           02 MLIBERRV  PIC X.                                          00005550
           02 MLIBERRO  PIC X(58).                                      00005560
      * CODE TRANSACTION                                                00005570
           02 FILLER    PIC X(2).                                       00005580
           02 MCODTRAA  PIC X.                                          00005590
           02 MCODTRAC  PIC X.                                          00005600
           02 MCODTRAP  PIC X.                                          00005610
           02 MCODTRAH  PIC X.                                          00005620
           02 MCODTRAV  PIC X.                                          00005630
           02 MCODTRAO  PIC X(4).                                       00005640
      * CICS DE TRAVAIL                                                 00005650
           02 FILLER    PIC X(2).                                       00005660
           02 MCICSA    PIC X.                                          00005670
           02 MCICSC    PIC X.                                          00005680
           02 MCICSP    PIC X.                                          00005690
           02 MCICSH    PIC X.                                          00005700
           02 MCICSV    PIC X.                                          00005710
           02 MCICSO    PIC X(5).                                       00005720
      * NETNAME                                                         00005730
           02 FILLER    PIC X(2).                                       00005740
           02 MNETNAMA  PIC X.                                          00005750
           02 MNETNAMC  PIC X.                                          00005760
           02 MNETNAMP  PIC X.                                          00005770
           02 MNETNAMH  PIC X.                                          00005780
           02 MNETNAMV  PIC X.                                          00005790
           02 MNETNAMO  PIC X(8).                                       00005800
      * CODE TERMINAL                                                   00005810
           02 FILLER    PIC X(2).                                       00005820
           02 MSCREENA  PIC X.                                          00005830
           02 MSCREENC  PIC X.                                          00005840
           02 MSCREENP  PIC X.                                          00005850
           02 MSCREENH  PIC X.                                          00005860
           02 MSCREENV  PIC X.                                          00005870
           02 MSCREENO  PIC X(5).                                       00005880
                                                                                
