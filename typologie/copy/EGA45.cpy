      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA45   EGA45                                              00000020
      ***************************************************************** 00000030
       01   EGA45I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGETL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGETF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGETI   PIC X(2).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGCPLTL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCGCPLTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCGCPLTF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCGCPLTI  PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFFETL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDEFFETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEFFETF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDEFFETI  PIC X(10).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCMARQI   PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDUGSTDL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MDUGSTDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDUGSTDF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDUGSTDI  PIC X(2).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDUGCPLTL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MDUGCPLTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDUGCPLTF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDUGCPLTI      PIC X(2).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPFRAISL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MPFRAISL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPFRAISF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MPFRAISI  PIC X(6).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTAUXTVL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MCTAUXTVL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTAUXTVF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCTAUXTVI      PIC X(5).                                  00000490
           02 MQMOISD OCCURS   72 TIMES .                               00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQMOISL      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MQMOISL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQMOISF      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MQMOISI      PIC X(2).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MLIBERRI  PIC X(74).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MCODTRAI  PIC X(4).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCICSI    PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MNETNAMI  PIC X(8).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MSCREENI  PIC X(4).                                       00000740
      ***************************************************************** 00000750
      * SDF: EGA45   EGA45                                              00000760
      ***************************************************************** 00000770
       01   EGA45O REDEFINES EGA45I.                                    00000780
           02 FILLER    PIC X(12).                                      00000790
           02 FILLER    PIC X(2).                                       00000800
           02 MDATJOUA  PIC X.                                          00000810
           02 MDATJOUC  PIC X.                                          00000820
           02 MDATJOUP  PIC X.                                          00000830
           02 MDATJOUH  PIC X.                                          00000840
           02 MDATJOUV  PIC X.                                          00000850
           02 MDATJOUO  PIC X(10).                                      00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MTIMJOUA  PIC X.                                          00000880
           02 MTIMJOUC  PIC X.                                          00000890
           02 MTIMJOUP  PIC X.                                          00000900
           02 MTIMJOUH  PIC X.                                          00000910
           02 MTIMJOUV  PIC X.                                          00000920
           02 MTIMJOUO  PIC X(5).                                       00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MPAGEA    PIC X.                                          00000950
           02 MPAGEC    PIC X.                                          00000960
           02 MPAGEP    PIC X.                                          00000970
           02 MPAGEH    PIC X.                                          00000980
           02 MPAGEV    PIC X.                                          00000990
           02 MPAGEO    PIC X(2).                                       00001000
           02 FILLER    PIC X(2).                                       00001010
           02 MPAGETA   PIC X.                                          00001020
           02 MPAGETC   PIC X.                                          00001030
           02 MPAGETP   PIC X.                                          00001040
           02 MPAGETH   PIC X.                                          00001050
           02 MPAGETV   PIC X.                                          00001060
           02 MPAGETO   PIC X(2).                                       00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MCGCPLTA  PIC X.                                          00001090
           02 MCGCPLTC  PIC X.                                          00001100
           02 MCGCPLTP  PIC X.                                          00001110
           02 MCGCPLTH  PIC X.                                          00001120
           02 MCGCPLTV  PIC X.                                          00001130
           02 MCGCPLTO  PIC X(5).                                       00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MDEFFETA  PIC X.                                          00001160
           02 MDEFFETC  PIC X.                                          00001170
           02 MDEFFETP  PIC X.                                          00001180
           02 MDEFFETH  PIC X.                                          00001190
           02 MDEFFETV  PIC X.                                          00001200
           02 MDEFFETO  PIC X(10).                                      00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MCMARQA   PIC X.                                          00001230
           02 MCMARQC   PIC X.                                          00001240
           02 MCMARQP   PIC X.                                          00001250
           02 MCMARQH   PIC X.                                          00001260
           02 MCMARQV   PIC X.                                          00001270
           02 MCMARQO   PIC X(5).                                       00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MDUGSTDA  PIC X.                                          00001300
           02 MDUGSTDC  PIC X.                                          00001310
           02 MDUGSTDP  PIC X.                                          00001320
           02 MDUGSTDH  PIC X.                                          00001330
           02 MDUGSTDV  PIC X.                                          00001340
           02 MDUGSTDO  PIC 99.                                         00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MDUGCPLTA      PIC X.                                     00001370
           02 MDUGCPLTC PIC X.                                          00001380
           02 MDUGCPLTP PIC X.                                          00001390
           02 MDUGCPLTH PIC X.                                          00001400
           02 MDUGCPLTV PIC X.                                          00001410
           02 MDUGCPLTO      PIC 99.                                    00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MPFRAISA  PIC X.                                          00001440
           02 MPFRAISC  PIC X.                                          00001450
           02 MPFRAISP  PIC X.                                          00001460
           02 MPFRAISH  PIC X.                                          00001470
           02 MPFRAISV  PIC X.                                          00001480
           02 MPFRAISO  PIC ZZ9,99.                                     00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MCTAUXTVA      PIC X.                                     00001510
           02 MCTAUXTVC PIC X.                                          00001520
           02 MCTAUXTVP PIC X.                                          00001530
           02 MCTAUXTVH PIC X.                                          00001540
           02 MCTAUXTVV PIC X.                                          00001550
           02 MCTAUXTVO      PIC X(5).                                  00001560
           02 DFHMS1 OCCURS   72 TIMES .                                00001570
             03 FILLER       PIC X(2).                                  00001580
             03 MQMOISA      PIC X.                                     00001590
             03 MQMOISC PIC X.                                          00001600
             03 MQMOISP PIC X.                                          00001610
             03 MQMOISH PIC X.                                          00001620
             03 MQMOISV PIC X.                                          00001630
             03 MQMOISO      PIC ZZ.                                    00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MLIBERRA  PIC X.                                          00001660
           02 MLIBERRC  PIC X.                                          00001670
           02 MLIBERRP  PIC X.                                          00001680
           02 MLIBERRH  PIC X.                                          00001690
           02 MLIBERRV  PIC X.                                          00001700
           02 MLIBERRO  PIC X(74).                                      00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MCODTRAA  PIC X.                                          00001730
           02 MCODTRAC  PIC X.                                          00001740
           02 MCODTRAP  PIC X.                                          00001750
           02 MCODTRAH  PIC X.                                          00001760
           02 MCODTRAV  PIC X.                                          00001770
           02 MCODTRAO  PIC X(4).                                       00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MCICSA    PIC X.                                          00001800
           02 MCICSC    PIC X.                                          00001810
           02 MCICSP    PIC X.                                          00001820
           02 MCICSH    PIC X.                                          00001830
           02 MCICSV    PIC X.                                          00001840
           02 MCICSO    PIC X(5).                                       00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MNETNAMA  PIC X.                                          00001870
           02 MNETNAMC  PIC X.                                          00001880
           02 MNETNAMP  PIC X.                                          00001890
           02 MNETNAMH  PIC X.                                          00001900
           02 MNETNAMV  PIC X.                                          00001910
           02 MNETNAMO  PIC X(8).                                       00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MSCREENA  PIC X.                                          00001940
           02 MSCREENC  PIC X.                                          00001950
           02 MSCREENP  PIC X.                                          00001960
           02 MSCREENH  PIC X.                                          00001970
           02 MSCREENV  PIC X.                                          00001980
           02 MSCREENO  PIC X(4).                                       00001990
                                                                                
