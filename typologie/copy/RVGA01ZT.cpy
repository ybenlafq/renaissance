      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SLMDL NMD SL : GESTION MODES DELIVR.   *        
      *----------------------------------------------------------------*        
       01  RVGA01ZT.                                                            
           05  SLMDL-CTABLEG2    PIC X(15).                                     
           05  SLMDL-CTABLEG2-REDEF REDEFINES SLMDL-CTABLEG2.                   
               10  SLMDL-CMOD            PIC X(03).                             
           05  SLMDL-WTABLEG     PIC X(80).                                     
           05  SLMDL-WTABLEG-REDEF  REDEFINES SLMDL-WTABLEG.                    
               10  SLMDL-WLIVR           PIC X(01).                             
               10  SLMDL-WSTMAG          PIC X(01).                             
               10  SLMDL-WBE             PIC X(01).                             
               10  SLMDL-WREPR           PIC X(01).                             
               10  SLMDL-CONCAT          PIC X(30).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01ZT-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLMDL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SLMDL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLMDL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SLMDL-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
