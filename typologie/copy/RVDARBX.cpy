      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE DARBX FOURNISSEURS RED BOX             *        
      *----------------------------------------------------------------*        
       01  RVDARBX.                                                             
           05  DARBX-CTABLEG2    PIC X(15).                                     
           05  DARBX-CTABLEG2-REDEF REDEFINES DARBX-CTABLEG2.                   
               10  DARBX-NENTCDE         PIC X(05).                             
               10  DARBX-CFAM            PIC X(05).                             
           05  DARBX-WTABLEG     PIC X(80).                                     
           05  DARBX-WTABLEG-REDEF  REDEFINES DARBX-WTABLEG.                    
               10  DARBX-LIBELLE         PIC X(25).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVDARBX-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  DARBX-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  DARBX-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  DARBX-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  DARBX-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
