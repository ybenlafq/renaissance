      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PRPRE PRESTAIRES INNOVENTE             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPRPRE .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPRPRE .                                                            
      *}                                                                        
           05  PRPRE-CTABLEG2    PIC X(15).                                     
           05  PRPRE-CTABLEG2-REDEF REDEFINES PRPRE-CTABLEG2.                   
               10  PRPRE-CPRESTAT        PIC X(05).                             
           05  PRPRE-WTABLEG     PIC X(80).                                     
           05  PRPRE-WTABLEG-REDEF  REDEFINES PRPRE-WTABLEG.                    
               10  PRPRE-WACTIF          PIC X(01).                             
               10  PRPRE-LPRESTAT        PIC X(30).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPRPRE-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPRPRE-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRPRE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PRPRE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRPRE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PRPRE-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
