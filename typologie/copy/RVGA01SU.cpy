      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE IFBQE CODES BANQUE DE FINANCEMENT      *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01SU.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01SU.                                                            
      *}                                                                        
           05  IFBQE-CTABLEG2    PIC X(15).                                     
           05  IFBQE-CTABLEG2-REDEF REDEFINES IFBQE-CTABLEG2.                   
               10  IFBQE-CBANQUE         PIC X(02).                             
           05  IFBQE-WTABLEG     PIC X(80).                                     
           05  IFBQE-WTABLEG-REDEF  REDEFINES IFBQE-WTABLEG.                    
               10  IFBQE-WACTIF          PIC X(01).                             
               10  IFBQE-LBANQUE         PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01SU-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01SU-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IFBQE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  IFBQE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IFBQE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  IFBQE-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
