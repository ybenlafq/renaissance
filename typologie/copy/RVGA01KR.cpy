      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HETYO CODES OPERATION GHE              *        
      *----------------------------------------------------------------*        
       01  RVGA01KR.                                                            
           05  HETYO-CTABLEG2    PIC X(15).                                     
           05  HETYO-CTABLEG2-REDEF REDEFINES HETYO-CTABLEG2.                   
               10  HETYO-NTYOPE          PIC X(03).                             
               10  HETYO-NTYOPE-N       REDEFINES HETYO-NTYOPE                  
                                         PIC 9(03).                             
           05  HETYO-WTABLEG     PIC X(80).                                     
           05  HETYO-WTABLEG-REDEF  REDEFINES HETYO-WTABLEG.                    
               10  HETYO-WACTIF          PIC X(01).                             
               10  HETYO-LTYOPE          PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01KR-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HETYO-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HETYO-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HETYO-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HETYO-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
