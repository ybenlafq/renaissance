      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPR102 AU 01/03/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,05,BI,A,                          *        
      *                           22,01,BI,A,                          *        
      *                           23,05,BI,A,                          *        
      *                           28,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPR102.                                                        
            05 NOMETAT-IPR102           PIC X(6) VALUE 'IPR102'.                
            05 RUPTURES-IPR102.                                                 
           10 IPR102-CHEFPROD           PIC X(05).                      007  005
           10 IPR102-CTYPPREST          PIC X(05).                      012  005
           10 IPR102-NENTCDE            PIC X(05).                      017  005
           10 IPR102-WMAJOPT            PIC X(01).                      022  001
           10 IPR102-CPRESTATION        PIC X(05).                      023  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPR102-SEQUENCE           PIC S9(04) COMP.                028  002
      *--                                                                       
           10 IPR102-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPR102.                                                   
           10 IPR102-CDEVISE            PIC X(06).                      030  006
           10 IPR102-LCHEFPROD          PIC X(20).                      036  020
           10 IPR102-LMAJOPT            PIC X(16).                      056  016
           10 IPR102-LPRESTATION        PIC X(20).                      072  020
           10 IPR102-NSOCIETE           PIC X(03).                      092  003
           10 IPR102-CA                 PIC S9(09)      COMP-3.         095  005
           10 IPR102-CA-MOIS            PIC S9(09)      COMP-3.         100  005
           10 IPR102-CA-7               PIC S9(09)      COMP-3.         105  005
           10 IPR102-NBPREST            PIC S9(06)      COMP-3.         110  004
           10 IPR102-NBPREST-MOIS       PIC S9(06)      COMP-3.         114  004
           10 IPR102-NBPREST-7          PIC S9(06)      COMP-3.         118  004
           10 IPR102-VOLNET             PIC S9(06)      COMP-3.         122  004
           10 IPR102-VOLNET-7           PIC S9(06)      COMP-3.         126  004
           10 IPR102-DVENTE             PIC X(08).                      130  008
            05 FILLER                      PIC X(375).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPR102-LONG           PIC S9(4)   COMP  VALUE +137.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPR102-LONG           PIC S9(4) COMP-5  VALUE +137.           
                                                                                
      *}                                                                        
