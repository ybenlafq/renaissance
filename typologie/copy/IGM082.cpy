      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGM082 AU 28/07/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,02,BI,A,                          *        
      *                           09,03,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,05,BI,A,                          *        
      *                           22,07,BI,A,                          *        
      *                           29,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGM082.                                                        
            05 NOMETAT-IGM082           PIC X(6) VALUE 'IGM082'.                
            05 RUPTURES-IGM082.                                                 
           10 IGM082-NZONPRIX           PIC X(02).                      007  002
           10 IGM082-NLIEU              PIC X(03).                      009  003
           10 IGM082-CHEFPROD           PIC X(05).                      012  005
           10 IGM082-CFAM               PIC X(05).                      017  005
           10 IGM082-NCODIC             PIC X(07).                      022  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGM082-SEQUENCE           PIC S9(04) COMP.                029  002
      *--                                                                       
           10 IGM082-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGM082.                                                   
           10 IGM082-CMARQ              PIC X(05).                      031  005
           10 IGM082-DDPRIME            PIC X(08).                      036  008
           10 IGM082-DDPRIME-REF        PIC X(08).                      044  008
           10 IGM082-DDPVMAG            PIC X(08).                      052  008
           10 IGM082-DDPVR              PIC X(08).                      060  008
           10 IGM082-DDPVR2             PIC X(08).                      068  008
           10 IGM082-DDWOA              PIC X(08).                      076  008
           10 IGM082-DEFSTATUT          PIC X(08).                      084  008
           10 IGM082-DFPRIME            PIC X(08).                      092  008
           10 IGM082-DFPRIME-REF        PIC X(08).                      100  008
           10 IGM082-DFPVMAG            PIC X(08).                      108  008
           10 IGM082-DFPVR              PIC X(08).                      116  008
           10 IGM082-DFPVR2             PIC X(08).                      124  008
           10 IGM082-LCHEFPROD          PIC X(20).                      132  020
           10 IGM082-LDATEDEB           PIC X(17).                      152  017
           10 IGM082-LDATEFIN           PIC X(14).                      169  014
           10 IGM082-LFAM               PIC X(20).                      183  020
           10 IGM082-LLIEU              PIC X(20).                      203  020
           10 IGM082-LREFFOURN          PIC X(20).                      223  020
           10 IGM082-LSTATCOMP          PIC X(03).                      243  003
           10 IGM082-LZONPRIX           PIC X(20).                      246  020
           10 IGM082-NSOCIETE           PIC X(03).                      266  003
           10 IGM082-TYPE               PIC X(04).                      269  004
           10 IGM082-TYPEP              PIC X(03).                      273  003
           10 IGM082-WBLOQUE            PIC X(03).                      276  003
           10 IGM082-WIMPER             PIC X(03).                      279  003
           10 IGM082-WOA                PIC X(01).                      282  001
           10 IGM082-WOAZP              PIC X(04).                      283  004
           10 IGM082-PRIME              PIC S9(03)V9(1) COMP-3.         287  003
           10 IGM082-PRIME-REF          PIC S9(03)V9(1) COMP-3.         290  003
           10 IGM082-PVMAG              PIC S9(05)V9(2) COMP-3.         293  004
           10 IGM082-PVR                PIC S9(05)V9(2) COMP-3.         297  004
           10 IGM082-PVR2               PIC S9(05)V9(2) COMP-3.         301  004
            05 FILLER                      PIC X(208).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGM082-LONG           PIC S9(4)   COMP  VALUE +304.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGM082-LONG           PIC S9(4) COMP-5  VALUE +304.           
                                                                                
      *}                                                                        
