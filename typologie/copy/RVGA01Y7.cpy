      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SLOPL SL / LIBELLE DES OPER. ARBITAB   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01Y.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01Y.                                                             
      *}                                                                        
           05  SLOPL-CTABLEG2    PIC X(15).                                     
           05  SLOPL-CTABLEG2-REDEF REDEFINES SLOPL-CTABLEG2.                   
               10  SLOPL-COPAR           PIC X(05).                             
           05  SLOPL-WTABLEG     PIC X(80).                                     
           05  SLOPL-WTABLEG-REDEF  REDEFINES SLOPL-WTABLEG.                    
               10  SLOPL-LOPAR           PIC X(20).                             
               10  SLOPL-WAUTR           PIC X(02).                             
               10  SLOPL-WAUTA           PIC X(02).                             
               10  SLOPL-WINT            PIC X(01).                             
               10  SLOPL-DELAI           PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  SLOPL-DELAI-N        REDEFINES SLOPL-DELAI                   
                                         PIC 9(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01Y-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01Y-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLOPL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SLOPL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLOPL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SLOPL-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
