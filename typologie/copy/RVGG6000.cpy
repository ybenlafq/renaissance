      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGG6000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGG6000                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGG6000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGG6000.                                                            
      *}                                                                        
           02  GG60-NCODIC                                                      
               PIC X(0007).                                                     
           02  GG60-DREC                                                        
               PIC X(0008).                                                     
           02  GG60-NREC                                                        
               PIC X(0007).                                                     
           02  GG60-NCDE                                                        
               PIC X(0007).                                                     
           02  GG60-PRARECYCL                                                   
               PIC S9(7)V9(0006) COMP-3.                                        
           02  GG60-DDEMANDE                                                    
               PIC X(0008).                                                     
           02  GG60-WTRAITE                                                     
               PIC X(0001).                                                     
           02  GG60-PRIST                                                       
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GG60-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGG6000                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGG6000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGG6000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG60-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG60-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG60-DREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG60-DREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG60-NREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG60-NREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG60-NCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG60-NCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG60-PRARECYCL-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG60-PRARECYCL-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG60-DDEMANDE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG60-DDEMANDE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG60-WTRAITE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG60-WTRAITE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG60-PRIST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GG60-PRIST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GG60-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GG60-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
