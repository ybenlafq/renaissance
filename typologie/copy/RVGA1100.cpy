      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGA1100                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA1100                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA1100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA1100.                                                            
      *}                                                                        
           02  GA11-CETAT                                                       
               PIC X(0010).                                                     
           02  GA11-CRAYONFAM                                                   
               PIC X(0005).                                                     
           02  GA11-WRAYONFAM                                                   
               PIC X(0001).                                                     
           02  GA11-WSEQED                                                      
               PIC S9(5) COMP-3.                                                
           02  GA11-WSAUT                                                       
               PIC X(0001).                                                     
           02  GA11-NAGREGATED1                                                 
               PIC X(0005).                                                     
           02  GA11-NAGREGATED2                                                 
               PIC X(0005).                                                     
           02  GA11-NAGREGATED3                                                 
               PIC X(0005).                                                     
           02  GA11-NAGREGATED4                                                 
               PIC X(0005).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGA1100                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA1100-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA1100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA11-CETAT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA11-CETAT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA11-CRAYONFAM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA11-CRAYONFAM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA11-WRAYONFAM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA11-WRAYONFAM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA11-WSEQED-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA11-WSEQED-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA11-WSAUT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA11-WSAUT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA11-NAGREGATED1-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA11-NAGREGATED1-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA11-NAGREGATED2-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA11-NAGREGATED2-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA11-NAGREGATED3-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA11-NAGREGATED3-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA11-NAGREGATED4-F                                               
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GA11-NAGREGATED4-F                                               
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
