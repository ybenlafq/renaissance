      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPR020 AU 26/10/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,12,BI,A,                          *        
      *                           32,05,BI,A,                          *        
      *                           37,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPR020.                                                        
            05 NOMETAT-IPR020           PIC X(6) VALUE 'IPR020'.                
            05 RUPTURES-IPR020.                                                 
           10 IPR020-NLIEU              PIC X(03).                      007  003
           10 IPR020-CTYPPREST          PIC X(05).                      010  005
           10 IPR020-NENTCDE            PIC X(05).                      015  005
           10 IPR020-PREMAJOP           PIC X(12).                      020  012
           10 IPR020-CPRESTATION        PIC X(05).                      032  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPR020-SEQUENCE           PIC S9(04) COMP.                037  002
      *--                                                                       
           10 IPR020-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPR020.                                                   
           10 IPR020-CPRIME             PIC X(03).                      039  003
           10 IPR020-LENTCDE            PIC X(20).                      042  020
           10 IPR020-LLIEU              PIC X(20).                      062  020
           10 IPR020-LPRESTATION        PIC X(20).                      082  020
           10 IPR020-LTPREST            PIC X(20).                      102  020
           10 IPR020-NSOCIETE           PIC X(03).                      122  003
           10 IPR020-NZONPRIX           PIC X(02).                      125  002
           10 IPR020-PVACT              PIC S9(06)V9(2) COMP-3.         127  005
            05 FILLER                      PIC X(381).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPR020-LONG           PIC S9(4)   COMP  VALUE +131.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPR020-LONG           PIC S9(4) COMP-5  VALUE +131.           
                                                                                
      *}                                                                        
