      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA20O  EGA20                                              00000020
      ***************************************************************** 00000030
       01   EGA20I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMILLL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MCFAMILLL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCFAMILLF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCFAMILLI      PIC X(5).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAMILLL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MLFAMILLL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLFAMILLF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLFAMILLI      PIC X(20).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSEQL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNSEQL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSEQF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNSEQI    PIC X(2).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARKE1L      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MLMARKE1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLMARKE1F      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLMARKE1I      PIC X(20).                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARKE2L      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLMARKE2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLMARKE2F      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLMARKE2I      PIC X(20).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARKE3L      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLMARKE3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLMARKE3F      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLMARKE3I      PIC X(20).                                 00000410
           02 M117I OCCURS   11 TIMES .                                 00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMARK1L     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MLMARK1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLMARK1F     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MLMARK1I     PIC X(20).                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMARK2L     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MLMARK2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLMARK2F     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MLMARK2I     PIC X(20).                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMARK3L     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MLMARK3L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLMARK3F     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MLMARK3I     PIC X(20).                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSELL  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MCSELL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCSELF  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCSELI  PIC X(2).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MZONCMDI  PIC X(15).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MLIBERRI  PIC X(58).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCODTRAI  PIC X(4).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCICSI    PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MNETNAMI  PIC X(8).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MSCREENI  PIC X(4).                                       00000820
      ***************************************************************** 00000830
      * SDF: EGA20O  EGA20                                              00000840
      ***************************************************************** 00000850
       01   EGA20O REDEFINES EGA20I.                                    00000860
           02 FILLER    PIC X(12).                                      00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MDATJOUA  PIC X.                                          00000890
           02 MDATJOUC  PIC X.                                          00000900
           02 MDATJOUP  PIC X.                                          00000910
           02 MDATJOUH  PIC X.                                          00000920
           02 MDATJOUV  PIC X.                                          00000930
           02 MDATJOUO  PIC X(10).                                      00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MTIMJOUA  PIC X.                                          00000960
           02 MTIMJOUC  PIC X.                                          00000970
           02 MTIMJOUP  PIC X.                                          00000980
           02 MTIMJOUH  PIC X.                                          00000990
           02 MTIMJOUV  PIC X.                                          00001000
           02 MTIMJOUO  PIC X(5).                                       00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MPAGEA    PIC X.                                          00001030
           02 MPAGEC    PIC X.                                          00001040
           02 MPAGEP    PIC X.                                          00001050
           02 MPAGEH    PIC X.                                          00001060
           02 MPAGEV    PIC X.                                          00001070
           02 MPAGEO    PIC X(3).                                       00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MCFAMILLA      PIC X.                                     00001100
           02 MCFAMILLC PIC X.                                          00001110
           02 MCFAMILLP PIC X.                                          00001120
           02 MCFAMILLH PIC X.                                          00001130
           02 MCFAMILLV PIC X.                                          00001140
           02 MCFAMILLO      PIC X(5).                                  00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MLFAMILLA      PIC X.                                     00001170
           02 MLFAMILLC PIC X.                                          00001180
           02 MLFAMILLP PIC X.                                          00001190
           02 MLFAMILLH PIC X.                                          00001200
           02 MLFAMILLV PIC X.                                          00001210
           02 MLFAMILLO      PIC X(20).                                 00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MNSEQA    PIC X.                                          00001240
           02 MNSEQC    PIC X.                                          00001250
           02 MNSEQP    PIC X.                                          00001260
           02 MNSEQH    PIC X.                                          00001270
           02 MNSEQV    PIC X.                                          00001280
           02 MNSEQO    PIC X(2).                                       00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MLMARKE1A      PIC X.                                     00001310
           02 MLMARKE1C PIC X.                                          00001320
           02 MLMARKE1P PIC X.                                          00001330
           02 MLMARKE1H PIC X.                                          00001340
           02 MLMARKE1V PIC X.                                          00001350
           02 MLMARKE1O      PIC X(20).                                 00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MLMARKE2A      PIC X.                                     00001380
           02 MLMARKE2C PIC X.                                          00001390
           02 MLMARKE2P PIC X.                                          00001400
           02 MLMARKE2H PIC X.                                          00001410
           02 MLMARKE2V PIC X.                                          00001420
           02 MLMARKE2O      PIC X(20).                                 00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MLMARKE3A      PIC X.                                     00001450
           02 MLMARKE3C PIC X.                                          00001460
           02 MLMARKE3P PIC X.                                          00001470
           02 MLMARKE3H PIC X.                                          00001480
           02 MLMARKE3V PIC X.                                          00001490
           02 MLMARKE3O      PIC X(20).                                 00001500
           02 M117O OCCURS   11 TIMES .                                 00001510
             03 FILLER       PIC X(2).                                  00001520
             03 MLMARK1A     PIC X.                                     00001530
             03 MLMARK1C     PIC X.                                     00001540
             03 MLMARK1P     PIC X.                                     00001550
             03 MLMARK1H     PIC X.                                     00001560
             03 MLMARK1V     PIC X.                                     00001570
             03 MLMARK1O     PIC X(20).                                 00001580
             03 FILLER       PIC X(2).                                  00001590
             03 MLMARK2A     PIC X.                                     00001600
             03 MLMARK2C     PIC X.                                     00001610
             03 MLMARK2P     PIC X.                                     00001620
             03 MLMARK2H     PIC X.                                     00001630
             03 MLMARK2V     PIC X.                                     00001640
             03 MLMARK2O     PIC X(20).                                 00001650
             03 FILLER       PIC X(2).                                  00001660
             03 MLMARK3A     PIC X.                                     00001670
             03 MLMARK3C     PIC X.                                     00001680
             03 MLMARK3P     PIC X.                                     00001690
             03 MLMARK3H     PIC X.                                     00001700
             03 MLMARK3V     PIC X.                                     00001710
             03 MLMARK3O     PIC X(20).                                 00001720
             03 FILLER       PIC X(2).                                  00001730
             03 MCSELA  PIC X.                                          00001740
             03 MCSELC  PIC X.                                          00001750
             03 MCSELP  PIC X.                                          00001760
             03 MCSELH  PIC X.                                          00001770
             03 MCSELV  PIC X.                                          00001780
             03 MCSELO  PIC X(2).                                       00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MZONCMDA  PIC X.                                          00001810
           02 MZONCMDC  PIC X.                                          00001820
           02 MZONCMDP  PIC X.                                          00001830
           02 MZONCMDH  PIC X.                                          00001840
           02 MZONCMDV  PIC X.                                          00001850
           02 MZONCMDO  PIC X(15).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MLIBERRA  PIC X.                                          00001880
           02 MLIBERRC  PIC X.                                          00001890
           02 MLIBERRP  PIC X.                                          00001900
           02 MLIBERRH  PIC X.                                          00001910
           02 MLIBERRV  PIC X.                                          00001920
           02 MLIBERRO  PIC X(58).                                      00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MCODTRAA  PIC X.                                          00001950
           02 MCODTRAC  PIC X.                                          00001960
           02 MCODTRAP  PIC X.                                          00001970
           02 MCODTRAH  PIC X.                                          00001980
           02 MCODTRAV  PIC X.                                          00001990
           02 MCODTRAO  PIC X(4).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MCICSA    PIC X.                                          00002020
           02 MCICSC    PIC X.                                          00002030
           02 MCICSP    PIC X.                                          00002040
           02 MCICSH    PIC X.                                          00002050
           02 MCICSV    PIC X.                                          00002060
           02 MCICSO    PIC X(5).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MNETNAMA  PIC X.                                          00002090
           02 MNETNAMC  PIC X.                                          00002100
           02 MNETNAMP  PIC X.                                          00002110
           02 MNETNAMH  PIC X.                                          00002120
           02 MNETNAMV  PIC X.                                          00002130
           02 MNETNAMO  PIC X(8).                                       00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MSCREENA  PIC X.                                          00002160
           02 MSCREENC  PIC X.                                          00002170
           02 MSCREENP  PIC X.                                          00002180
           02 MSCREENH  PIC X.                                          00002190
           02 MSCREENV  PIC X.                                          00002200
           02 MSCREENO  PIC X(4).                                       00002210
                                                                                
