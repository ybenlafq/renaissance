      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HSGES CODE GESTION DU HS/SOCIETE       *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01OT.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01OT.                                                            
      *}                                                                        
           05  HSGES-CTABLEG2    PIC X(15).                                     
           05  HSGES-CTABLEG2-REDEF REDEFINES HSGES-CTABLEG2.                   
               10  HSGES-NSOCIETE        PIC X(03).                             
           05  HSGES-WTABLEG     PIC X(80).                                     
           05  HSGES-WTABLEG-REDEF  REDEFINES HSGES-WTABLEG.                    
               10  HSGES-CGESTION        PIC X(01).                             
               10  HSGES-NLIEUINI        PIC X(03).                             
               10  HSGES-DEPOTGRF        PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01OT-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01OT-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HSGES-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HSGES-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HSGES-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HSGES-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
