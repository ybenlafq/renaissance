      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * COMMAREA SPECIFIQUE PROG TGA19                             *            
      *       TR : GA00  ADMISTRATION DES DONNEES                  *            
      *       PG : TGA19 DEF DES FAMILLES= CARACT SPECIFS ,MODELIV *            
      **************************************************************            
      * DATE   : 22/03/2012                                        *            
      * AUTEUR : L.ARMANT                                          *            
      * OBJET  : COPIE DES MODES DE DELIVRANCE PAR FAMILLE         *            
      * DE DIF VERS FILIALES                                       *            
      **************************************************************            
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724          
      *                                                                         
      *                                                                         
          02 COMM-GA19-APPLI REDEFINES COMM-GA00-APPLI.                         
      *------------------------------ CODE FONCTION                             
             03 COMM-GA19-WFONC          PIC X(3).                              
      *---------                         CHOIX                                  
             03 COMM-GA19-ZONCMD         PIC X(15).                             
      *---------                                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GA19-ITEM           PIC S9(4)   COMP.                      
      *--                                                                       
             03 COMM-GA19-ITEM           PIC S9(4) COMP-5.                      
      *}                                                                        
      *---------                                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GA19-MAXITEM        PIC S9(4)   COMP.                      
      *--                                                                       
             03 COMM-GA19-MAXITEM        PIC S9(4) COMP-5.                      
      *}                                                                        
      *---------                          LIGNE FAMILLE                         
             03 COMM-GA19-GA1400.                                               
      *                                                                         
                 05 COMM-GA19-CFAM           PIC X(5).                          
                 05 COMM-GA19-LFAM           PIC X(20).                         
                 05 COMM-GA19-CMODSTOCK      PIC X(5).                          
                 05 COMM-GA19-CTAUXTVA       PIC X(5).                          
                 05 COMM-GA19-CGARANTIE      PIC X(5).                          
                 05 COMM-GA19-QBORNESOL      PIC S99V999 COMP-3.                
                 05 COMM-GA19-QBORNVRAC      PIC S99V999 COMP-3.                
                 05 COMM-GA19-QNBPSCL        PIC S999    COMP-3.                
                 05 COMM-GA19-WMULTIFAM      PIC X.                             
                 05 COMM-GA19-WSEQFAM        PIC S9(5)   COMP-3.                
      *                                                                         
             03 COMM-GA19-OK                 PIC X.                             
      *                                                                         
             03 COMM-TAB-ACTION.                                                
               05 COMM-DECISION1      OCCURS 10.                                
                   07 COMM-GA19-ACTION1        PIC X.                           
               05 COMM-DECISION2      OCCURS 10.                                
                   07 COMM-GA19-ACTION2        PIC X.                           
      *      TABLE DES CODES CARACTERISTIK SPECIFS + LIBELLES                   
             03 COMM-TABCOD-CARACT.                                             
               05 COMM-TABCOD-ELEM    OCCURS 10.                                
                   07 COMM-GA19-CARACT         PIC X(5).                        
                   07 COMM-GA19-LCARAC         PIC X(20).                       
      *                                                                         
      *      TABLE DES CODES MODES DE DELIVRANCES + LIBELLES                    
             03 COMM-TABCOD-MODDEL.                                             
               05 COMM-TABCOD-ELEM    OCCURS 10.                                
                   07 COMM-GA19-MODELI         PIC X(3).                        
                   07 COMM-GA19-LMODEL         PIC X(20).                       
      *                                                                         
      *      SOCIETE APPARTENANT AU USER DE CONNECTION                          
             03 COMM-GA19-NSOC           PIC  X(3).                             
      *------------------------------ ZONE LIBRE                                
      *      03 COMM-GA19-LIBRE1         PIC X(3129).                           
             03 COMM-GA19-LIBRE1         PIC X(3126).                           
      *                                                                         
      *****************************************************************         
                                                                                
