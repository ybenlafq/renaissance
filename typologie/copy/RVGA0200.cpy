      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGA0200                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA0200                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA0200.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA0200.                                                            
      *}                                                                        
           02  GA02-CHEFPROD                                                    
               PIC X(0005).                                                     
           02  GA02-LCHEFPROD                                                   
               PIC X(0020).                                                     
           02  GA02-DCHEFPROD                                                   
               PIC X(0008).                                                     
           02  GA02-WCHEFPROD                                                   
               PIC X(0001).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGA0200                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA0200-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA0200-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA02-CHEFPROD-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA02-CHEFPROD-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA02-LCHEFPROD-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA02-LCHEFPROD-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA02-DCHEFPROD-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA02-DCHEFPROD-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA02-WCHEFPROD-F                                                 
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GA02-WCHEFPROD-F                                                 
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
