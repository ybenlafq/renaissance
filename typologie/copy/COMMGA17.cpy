      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * COMMAREA SPECIFIQUE PROG TGA17                             *            
      *       TR : GA00  ADMISTRATION DES DONNEES                  *            
      *       PG : TGA17 SOUS DEFINITION DES FAMILLES              *            
      **************************************************************            
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724          
      *                                                                         
      *                                                                         
          02 COMM-GA17-APPLI REDEFINES COMM-GA00-APPLI.                         
      *------------------------------ CODE FONCTION                             
             03 COMM-GA17-WFONC          PIC X(3).                              
      *------------------------------ CHOIX                                     
             03 COMM-GA17-ZONCMD         PIC X(15).                             
      *---------                                                                
             03 COMM-GA17-ITEM           PIC S9(2).                             
      *---------                                                                
             03 COMM-GA17-MAXITEM        PIC S9(2).                             
      *------------------------------ LIGNE FAMILLE                             
             03 COMM-GA17-GA1400.                                               
      *                                                                         
                 05 COMM-GA17-CFAM           PIC X(5).                          
                 05 COMM-GA17-LFAM           PIC X(20).                         
      *---------                                                                
             03 COMM-GA17-MAXCODES           PIC S9(3)   COMP-3.                
      *---------                                                                
             03 COMM-STAR                    PIC X(6) .                         
      *                                                                         
      *---------    TAB       CODES DESCRIPTS   (PAGE COURANTE)                 
      *                                                                         
             03 COMM-TABCOD-DESCR.                                              
               05 COMM-TABCOD-ELEM    OCCURS 14.                                
                   07 COMM-GA17-CDESCR         PIC X(5).                        
                   07 COMM-GA17-LDESCR         PIC X(20).                       
                   07 COMM-GA17-CVDESCR        PIC X(5).                        
                   07 COMM-GA17-LVDESCR        PIC X(20).                       
      *                                                                         
      *****************************************************************         
                                                                                
