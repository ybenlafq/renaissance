      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGA4200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA4200                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGA4200.                                                            
           02  GA42-CGCPLT                                                      
               PIC X(0005).                                                     
           02  GA42-DEFFET                                                      
               PIC X(0008).                                                     
           02  GA42-NMOIS                                                       
               PIC X(0002).                                                     
           02  GA42-QMOIS                                                       
               PIC S9(3) COMP-3.                                                
           02  GA42-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGA4200                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGA4200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA42-CGCPLT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA42-CGCPLT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA42-DEFFET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA42-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA42-NMOIS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA42-NMOIS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA42-QMOIS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA42-QMOIS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA42-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA42-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
