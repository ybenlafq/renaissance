      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HSGRP CODE REGROUPEMENTS LIEUX DE HS   *        
      *----------------------------------------------------------------*        
       01  RVGA01SA.                                                            
           05  HSGRP-CTABLEG2    PIC X(15).                                     
           05  HSGRP-CTABLEG2-REDEF REDEFINES HSGRP-CTABLEG2.                   
               10  HSGRP-CREGROUP        PIC X(01).                             
           05  HSGRP-WTABLEG     PIC X(80).                                     
           05  HSGRP-WTABLEG-REDEF  REDEFINES HSGRP-WTABLEG.                    
               10  HSGRP-LREGROUP        PIC X(25).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01SA-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HSGRP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HSGRP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HSGRP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HSGRP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
