      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: Erx24                                                      00000020
      ***************************************************************** 00000030
       01   ERX24I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(5).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCI    PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGTL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNMAGTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNMAGTF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNMAGTI   PIC X(3).                                       00000290
           02 MNDETRELI OCCURS   14 TIMES .                             00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNNUMEL      COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MNNUMEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNNUMEF      PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MNNUMEI      PIC X(7).                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPEL  COMP PIC S9(4).                                 00000350
      *--                                                                       
             03 MTYPEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTYPEF  PIC X.                                          00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MTYPEI  PIC X.                                          00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODEL      COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MNCODEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNCODEF      PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNCODEI      PIC X(4).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLENSCL      COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MLENSCL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLENSCF      PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MLENSCI      PIC X(15).                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNZPL   COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MNZPL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNZPF   PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNZPI   PIC X(2).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMAGRL      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MNMAGRL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNMAGRF      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNMAGRI      PIC X(3).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNNBREL      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNNBREL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNNBREF      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNNBREI      PIC X(4).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEDITL      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MDEDITL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDEDITF      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MDEDITI      PIC X(8).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDSAISIL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MDSAISIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDSAISIF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MDSAISII     PIC X(8).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCLOTL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MDCLOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDCLOTF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MDCLOTI      PIC X(8).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MLIBERRI  PIC X(78).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCODTRAI  PIC X(4).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCICSI    PIC X(5).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MNETNAMI  PIC X(8).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MSCREENI  PIC X(4).                                       00000900
      ***************************************************************** 00000910
      * SDF: Erx24                                                      00000920
      ***************************************************************** 00000930
       01   ERX24O REDEFINES ERX24I.                                    00000940
           02 FILLER    PIC X(12).                                      00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MDATJOUA  PIC X.                                          00000970
           02 MDATJOUC  PIC X.                                          00000980
           02 MDATJOUP  PIC X.                                          00000990
           02 MDATJOUH  PIC X.                                          00001000
           02 MDATJOUV  PIC X.                                          00001010
           02 MDATJOUO  PIC X(10).                                      00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MTIMJOUA  PIC X.                                          00001040
           02 MTIMJOUC  PIC X.                                          00001050
           02 MTIMJOUP  PIC X.                                          00001060
           02 MTIMJOUH  PIC X.                                          00001070
           02 MTIMJOUV  PIC X.                                          00001080
           02 MTIMJOUO  PIC X(5).                                       00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MPAGEA    PIC X.                                          00001110
           02 MPAGEC    PIC X.                                          00001120
           02 MPAGEP    PIC X.                                          00001130
           02 MPAGEH    PIC X.                                          00001140
           02 MPAGEV    PIC X.                                          00001150
           02 MPAGEO    PIC X(5).                                       00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MWFONCA   PIC X.                                          00001180
           02 MWFONCC   PIC X.                                          00001190
           02 MWFONCP   PIC X.                                          00001200
           02 MWFONCH   PIC X.                                          00001210
           02 MWFONCV   PIC X.                                          00001220
           02 MWFONCO   PIC X(3).                                       00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MNSOCA    PIC X.                                          00001250
           02 MNSOCC    PIC X.                                          00001260
           02 MNSOCP    PIC X.                                          00001270
           02 MNSOCH    PIC X.                                          00001280
           02 MNSOCV    PIC X.                                          00001290
           02 MNSOCO    PIC X(3).                                       00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MNMAGTA   PIC X.                                          00001320
           02 MNMAGTC   PIC X.                                          00001330
           02 MNMAGTP   PIC X.                                          00001340
           02 MNMAGTH   PIC X.                                          00001350
           02 MNMAGTV   PIC X.                                          00001360
           02 MNMAGTO   PIC X(3).                                       00001370
           02 MNDETRELO OCCURS   14 TIMES .                             00001380
             03 FILLER       PIC X(2).                                  00001390
             03 MNNUMEA      PIC X.                                     00001400
             03 MNNUMEC PIC X.                                          00001410
             03 MNNUMEP PIC X.                                          00001420
             03 MNNUMEH PIC X.                                          00001430
             03 MNNUMEV PIC X.                                          00001440
             03 MNNUMEO      PIC X(7).                                  00001450
             03 FILLER       PIC X(2).                                  00001460
             03 MTYPEA  PIC X.                                          00001470
             03 MTYPEC  PIC X.                                          00001480
             03 MTYPEP  PIC X.                                          00001490
             03 MTYPEH  PIC X.                                          00001500
             03 MTYPEV  PIC X.                                          00001510
             03 MTYPEO  PIC X.                                          00001520
             03 FILLER       PIC X(2).                                  00001530
             03 MNCODEA      PIC X.                                     00001540
             03 MNCODEC PIC X.                                          00001550
             03 MNCODEP PIC X.                                          00001560
             03 MNCODEH PIC X.                                          00001570
             03 MNCODEV PIC X.                                          00001580
             03 MNCODEO      PIC X(4).                                  00001590
             03 FILLER       PIC X(2).                                  00001600
             03 MLENSCA      PIC X.                                     00001610
             03 MLENSCC PIC X.                                          00001620
             03 MLENSCP PIC X.                                          00001630
             03 MLENSCH PIC X.                                          00001640
             03 MLENSCV PIC X.                                          00001650
             03 MLENSCO      PIC X(15).                                 00001660
             03 FILLER       PIC X(2).                                  00001670
             03 MNZPA   PIC X.                                          00001680
             03 MNZPC   PIC X.                                          00001690
             03 MNZPP   PIC X.                                          00001700
             03 MNZPH   PIC X.                                          00001710
             03 MNZPV   PIC X.                                          00001720
             03 MNZPO   PIC X(2).                                       00001730
             03 FILLER       PIC X(2).                                  00001740
             03 MNMAGRA      PIC X.                                     00001750
             03 MNMAGRC PIC X.                                          00001760
             03 MNMAGRP PIC X.                                          00001770
             03 MNMAGRH PIC X.                                          00001780
             03 MNMAGRV PIC X.                                          00001790
             03 MNMAGRO      PIC X(3).                                  00001800
             03 FILLER       PIC X(2).                                  00001810
             03 MNNBREA      PIC X.                                     00001820
             03 MNNBREC PIC X.                                          00001830
             03 MNNBREP PIC X.                                          00001840
             03 MNNBREH PIC X.                                          00001850
             03 MNNBREV PIC X.                                          00001860
             03 MNNBREO      PIC X(4).                                  00001870
             03 FILLER       PIC X(2).                                  00001880
             03 MDEDITA      PIC X.                                     00001890
             03 MDEDITC PIC X.                                          00001900
             03 MDEDITP PIC X.                                          00001910
             03 MDEDITH PIC X.                                          00001920
             03 MDEDITV PIC X.                                          00001930
             03 MDEDITO      PIC X(8).                                  00001940
             03 FILLER       PIC X(2).                                  00001950
             03 MDSAISIA     PIC X.                                     00001960
             03 MDSAISIC     PIC X.                                     00001970
             03 MDSAISIP     PIC X.                                     00001980
             03 MDSAISIH     PIC X.                                     00001990
             03 MDSAISIV     PIC X.                                     00002000
             03 MDSAISIO     PIC X(8).                                  00002010
             03 FILLER       PIC X(2).                                  00002020
             03 MDCLOTA      PIC X.                                     00002030
             03 MDCLOTC PIC X.                                          00002040
             03 MDCLOTP PIC X.                                          00002050
             03 MDCLOTH PIC X.                                          00002060
             03 MDCLOTV PIC X.                                          00002070
             03 MDCLOTO      PIC X(8).                                  00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MLIBERRA  PIC X.                                          00002100
           02 MLIBERRC  PIC X.                                          00002110
           02 MLIBERRP  PIC X.                                          00002120
           02 MLIBERRH  PIC X.                                          00002130
           02 MLIBERRV  PIC X.                                          00002140
           02 MLIBERRO  PIC X(78).                                      00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCODTRAA  PIC X.                                          00002170
           02 MCODTRAC  PIC X.                                          00002180
           02 MCODTRAP  PIC X.                                          00002190
           02 MCODTRAH  PIC X.                                          00002200
           02 MCODTRAV  PIC X.                                          00002210
           02 MCODTRAO  PIC X(4).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MCICSA    PIC X.                                          00002240
           02 MCICSC    PIC X.                                          00002250
           02 MCICSP    PIC X.                                          00002260
           02 MCICSH    PIC X.                                          00002270
           02 MCICSV    PIC X.                                          00002280
           02 MCICSO    PIC X(5).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MNETNAMA  PIC X.                                          00002310
           02 MNETNAMC  PIC X.                                          00002320
           02 MNETNAMP  PIC X.                                          00002330
           02 MNETNAMH  PIC X.                                          00002340
           02 MNETNAMV  PIC X.                                          00002350
           02 MNETNAMO  PIC X(8).                                       00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MSCREENA  PIC X.                                          00002380
           02 MSCREENC  PIC X.                                          00002390
           02 MSCREENP  PIC X.                                          00002400
           02 MSCREENH  PIC X.                                          00002410
           02 MSCREENV  PIC X.                                          00002420
           02 MSCREENO  PIC X(4).                                       00002430
                                                                                
