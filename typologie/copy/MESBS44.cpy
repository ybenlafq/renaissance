      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQHI                                                     
      *                                                                         
      *                                                                         
      *****************************************************************         
      *                                                                         
      *                                                                         
       01  WS-MQ10.                                                             
         02 WS-QUEUE.                                                           
               10   MQ10-MSGID         PIC    X(24).                            
               10   MQ10-CORRELID      PIC    X(24).                            
         02 WS-CODRET                  PIC    X(02).                            
         02 MESSAGE44.                                                          
           03 MESSAGE44-ENTETE.                                                 
               05   MES44-TYPE         PIC    X(03).                            
               05   MES44-NSOCMSG      PIC    X(03).                            
               05   MES44-NLIEUMSG     PIC    X(03).                            
               05   MES44-NSOCDST      PIC    X(03).                            
               05   MES44-NLIEUDST     PIC    X(03).                            
               05   MES44-NORD         PIC    9(08).                            
               05   MES44-LPROG        PIC    X(10).                            
               05   MES44-DJOUR        PIC    X(08).                            
               05   MES44-WSID         PIC    X(10).                            
               05   MES44-USER         PIC    X(10).                            
               05   MES44-CHRONO       PIC    9(07).                            
               05   MES44-NBRMSG       PIC    9(07).                            
               05   MES44-FILLER       PIC    X(30).                            
         02 MESSAGE44-DATA.                                                     
               05   MES44-NSOC         PIC    X(03).                            
               05   MES44-NLIEU        PIC    X(03).                            
               05   MES44-NORDV        PIC    X(08).                            
               05   MES44-WEDFAC       PIC    X(01).                            
               05   MES44-NBCDE        PIC    X(07).                            
               05   MES44-NORDRE       PIC    X(05).                            
               05   MES44-DFACT        PIC    X(08).                            
               05   MES44-NSOCF        PIC    X(03).                            
               05   MES44-LIEUF        PIC    X(03).                            
               05   MES44-NFACT        PIC    X(07).                            
               05   MES44-HFACT        PIC    9(06).                            
               05   MES44-NBRLG        PIC   S9(04).                            
               05   MES44-TYPVTE       PIC    X(01).                            
               05   MES44-DET.                                                  
                    07   MES44-DET-VTE      OCCURS 80.                          
                         10   MES44-NCODIC       PIC    X(07).                  
                         10   MES44-CPREST       PIC    X(05).                  
                         10   MES44-NLGVTH       PIC   S9(03).                  
                         10   MES44-NSEQFV       PIC   S9(02).                  
      *--TABLE RTGV02 : 1 OCCURENCE = 449 CAR-----------                        
                    07   MES44-ENR-GV02          PIC   X(449).                  
                                                                                
