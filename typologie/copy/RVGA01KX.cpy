      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE DTECH JOURS ECHEANCES DS LE MOIS       *        
      *----------------------------------------------------------------*        
       01  RVGA01KX.                                                            
           05  DTECH-CTABLEG2    PIC X(15).                                     
           05  DTECH-CTABLEG2-REDEF REDEFINES DTECH-CTABLEG2.                   
               10  DTECH-CPROG           PIC X(06).                             
               10  DTECH-NSEQ            PIC X(02).                             
               10  DTECH-NSEQ-N         REDEFINES DTECH-NSEQ                    
                                         PIC 9(02).                             
           05  DTECH-WTABLEG     PIC X(80).                                     
           05  DTECH-WTABLEG-REDEF  REDEFINES DTECH-WTABLEG.                    
               10  DTECH-JOUR            PIC X(02).                             
               10  DTECH-WACTIF          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01KX-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  DTECH-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  DTECH-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  DTECH-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  DTECH-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
