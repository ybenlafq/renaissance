      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA52   EGA52                                              00000020
      ***************************************************************** 00000030
       01   EGA52I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(7).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCODICI  PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFOL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLREFOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLREFOF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLREFOI   PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLFAMI    PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCMARQI   PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLMARQI   PIC X(20).                                      00000450
           02 MTABLEI OCCURS   11 TIMES .                               00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCDESL  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MCDESL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCDESF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MCDESI  PIC X(5).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLDESL  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MLDESL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLDESF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MLDESI  PIC X(20).                                      00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCVDESL      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MCVDESL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCVDESF      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCVDESI      PIC X(5).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLVDESL      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MLVDESL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLVDESF      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MLVDESI      PIC X(20).                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MZONCMDI  PIC X(15).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLIBERRI  PIC X(58).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCODTRAI  PIC X(4).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCICSI    PIC X(5).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MNETNAMI  PIC X(8).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MSCREENI  PIC X(4).                                       00000860
      ***************************************************************** 00000870
      * SDF: EGA52   EGA52                                              00000880
      ***************************************************************** 00000890
       01   EGA52O REDEFINES EGA52I.                                    00000900
           02 FILLER    PIC X(12).                                      00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MDATJOUA  PIC X.                                          00000930
           02 MDATJOUC  PIC X.                                          00000940
           02 MDATJOUP  PIC X.                                          00000950
           02 MDATJOUH  PIC X.                                          00000960
           02 MDATJOUV  PIC X.                                          00000970
           02 MDATJOUO  PIC X(10).                                      00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MTIMJOUA  PIC X.                                          00001000
           02 MTIMJOUC  PIC X.                                          00001010
           02 MTIMJOUP  PIC X.                                          00001020
           02 MTIMJOUH  PIC X.                                          00001030
           02 MTIMJOUV  PIC X.                                          00001040
           02 MTIMJOUO  PIC X(5).                                       00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MPAGEA    PIC X.                                          00001070
           02 MPAGEC    PIC X.                                          00001080
           02 MPAGEP    PIC X.                                          00001090
           02 MPAGEH    PIC X.                                          00001100
           02 MPAGEV    PIC X.                                          00001110
           02 MPAGEO    PIC X(7).                                       00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MCFONCA   PIC X.                                          00001140
           02 MCFONCC   PIC X.                                          00001150
           02 MCFONCP   PIC X.                                          00001160
           02 MCFONCH   PIC X.                                          00001170
           02 MCFONCV   PIC X.                                          00001180
           02 MCFONCO   PIC X(3).                                       00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MNCODICA  PIC X.                                          00001210
           02 MNCODICC  PIC X.                                          00001220
           02 MNCODICP  PIC X.                                          00001230
           02 MNCODICH  PIC X.                                          00001240
           02 MNCODICV  PIC X.                                          00001250
           02 MNCODICO  PIC X(7).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MLREFOA   PIC X.                                          00001280
           02 MLREFOC   PIC X.                                          00001290
           02 MLREFOP   PIC X.                                          00001300
           02 MLREFOH   PIC X.                                          00001310
           02 MLREFOV   PIC X.                                          00001320
           02 MLREFOO   PIC X(20).                                      00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MCFAMA    PIC X.                                          00001350
           02 MCFAMC    PIC X.                                          00001360
           02 MCFAMP    PIC X.                                          00001370
           02 MCFAMH    PIC X.                                          00001380
           02 MCFAMV    PIC X.                                          00001390
           02 MCFAMO    PIC X(5).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MLFAMA    PIC X.                                          00001420
           02 MLFAMC    PIC X.                                          00001430
           02 MLFAMP    PIC X.                                          00001440
           02 MLFAMH    PIC X.                                          00001450
           02 MLFAMV    PIC X.                                          00001460
           02 MLFAMO    PIC X(20).                                      00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MCMARQA   PIC X.                                          00001490
           02 MCMARQC   PIC X.                                          00001500
           02 MCMARQP   PIC X.                                          00001510
           02 MCMARQH   PIC X.                                          00001520
           02 MCMARQV   PIC X.                                          00001530
           02 MCMARQO   PIC X(5).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MLMARQA   PIC X.                                          00001560
           02 MLMARQC   PIC X.                                          00001570
           02 MLMARQP   PIC X.                                          00001580
           02 MLMARQH   PIC X.                                          00001590
           02 MLMARQV   PIC X.                                          00001600
           02 MLMARQO   PIC X(20).                                      00001610
           02 MTABLEO OCCURS   11 TIMES .                               00001620
             03 FILLER       PIC X(2).                                  00001630
             03 MCDESA  PIC X.                                          00001640
             03 MCDESC  PIC X.                                          00001650
             03 MCDESP  PIC X.                                          00001660
             03 MCDESH  PIC X.                                          00001670
             03 MCDESV  PIC X.                                          00001680
             03 MCDESO  PIC X(5).                                       00001690
             03 FILLER       PIC X(2).                                  00001700
             03 MLDESA  PIC X.                                          00001710
             03 MLDESC  PIC X.                                          00001720
             03 MLDESP  PIC X.                                          00001730
             03 MLDESH  PIC X.                                          00001740
             03 MLDESV  PIC X.                                          00001750
             03 MLDESO  PIC X(20).                                      00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MCVDESA      PIC X.                                     00001780
             03 MCVDESC PIC X.                                          00001790
             03 MCVDESP PIC X.                                          00001800
             03 MCVDESH PIC X.                                          00001810
             03 MCVDESV PIC X.                                          00001820
             03 MCVDESO      PIC X(5).                                  00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MLVDESA      PIC X.                                     00001850
             03 MLVDESC PIC X.                                          00001860
             03 MLVDESP PIC X.                                          00001870
             03 MLVDESH PIC X.                                          00001880
             03 MLVDESV PIC X.                                          00001890
             03 MLVDESO      PIC X(20).                                 00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MZONCMDA  PIC X.                                          00001920
           02 MZONCMDC  PIC X.                                          00001930
           02 MZONCMDP  PIC X.                                          00001940
           02 MZONCMDH  PIC X.                                          00001950
           02 MZONCMDV  PIC X.                                          00001960
           02 MZONCMDO  PIC X(15).                                      00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MLIBERRA  PIC X.                                          00001990
           02 MLIBERRC  PIC X.                                          00002000
           02 MLIBERRP  PIC X.                                          00002010
           02 MLIBERRH  PIC X.                                          00002020
           02 MLIBERRV  PIC X.                                          00002030
           02 MLIBERRO  PIC X(58).                                      00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCODTRAA  PIC X.                                          00002060
           02 MCODTRAC  PIC X.                                          00002070
           02 MCODTRAP  PIC X.                                          00002080
           02 MCODTRAH  PIC X.                                          00002090
           02 MCODTRAV  PIC X.                                          00002100
           02 MCODTRAO  PIC X(4).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MCICSA    PIC X.                                          00002130
           02 MCICSC    PIC X.                                          00002140
           02 MCICSP    PIC X.                                          00002150
           02 MCICSH    PIC X.                                          00002160
           02 MCICSV    PIC X.                                          00002170
           02 MCICSO    PIC X(5).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MNETNAMA  PIC X.                                          00002200
           02 MNETNAMC  PIC X.                                          00002210
           02 MNETNAMP  PIC X.                                          00002220
           02 MNETNAMH  PIC X.                                          00002230
           02 MNETNAMV  PIC X.                                          00002240
           02 MNETNAMO  PIC X(8).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MSCREENA  PIC X.                                          00002270
           02 MSCREENC  PIC X.                                          00002280
           02 MSCREENP  PIC X.                                          00002290
           02 MSCREENH  PIC X.                                          00002300
           02 MSCREENV  PIC X.                                          00002310
           02 MSCREENO  PIC X(4).                                       00002320
                                                                                
