      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TRX30 (MENU)             TR: RX30  *    00020000
      *                                                            *    00090000
      *           POUR L'ADMINISTATION DES DONNEES                 *    00100000
      **************************************************************    00110000
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00120000
      **************************************************************    00130000
      *                                                                 00140000
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00150000
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00160000
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00170000
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00180000
      *                                                                 00190000
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00200000
      * COMPRENANT :                                                    00210000
      * 1 - LES ZONES RESERVEES A AIDA                                  00220000
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00230000
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00240000
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00250000
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00260000
      *                                                                 00270000
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00280000
      * PAR AIDA                                                        00290000
      *                                                                 00300000
      *-------------------------------------------------------------    00310000
      *                                                                 00320000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-RX30-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00330000
      *--                                                                       
       01  COM-RX30-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00340000
       01  Z-COMMAREA.                                                  00350000
      *                                                                 00360000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00370000
          02 FILLER-COM-AIDA      PIC X(100).                           00380000
      *                                                                 00390000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00400000
          02 COMM-CICS-APPLID     PIC X(8).                             00410000
          02 COMM-CICS-NETNAM     PIC X(8).                             00420000
          02 COMM-CICS-TRANSA     PIC X(4).                             00430000
      *                                                                 00440000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00450000
          02 COMM-DATE-SIECLE     PIC XX.                               00460000
          02 COMM-DATE-ANNEE      PIC XX.                               00470000
          02 COMM-DATE-MOIS       PIC XX.                               00480000
          02 COMM-DATE-JOUR       PIC XX.                               00490000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00500000
          02 COMM-DATE-QNTA       PIC 999.                              00510000
          02 COMM-DATE-QNT0       PIC 99999.                            00520000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00530000
          02 COMM-DATE-BISX       PIC 9.                                00540000
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00550000
          02 COMM-DATE-JSM        PIC 9.                                00560000
      *   LIBELLES DU JOUR COURT - LONG                                 00570000
          02 COMM-DATE-JSM-LC     PIC XXX.                              00580000
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                         00590000
      *   LIBELLES DU MOIS COURT - LONG                                 00600000
          02 COMM-DATE-MOIS-LC    PIC XXX.                              00610000
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                         00620000
      *   DIFFERENTES FORMES DE DATE                                    00630000
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                             00640000
          02 COMM-DATE-AAMMJJ     PIC X(6).                             00650000
          02 COMM-DATE-JJMMSSAA   PIC X(8).                             00660000
          02 COMM-DATE-JJMMAA     PIC X(6).                             00670000
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                             00680000
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                            00690000
      *                                                                 00690100
          02 COMM-DATE-WEEK.                                            00690200
              05  COMM-DATE-SEMSS PIC 99.                               00690300
              05  COMM-DATE-SEMAA PIC 99.                               00690310
              05  COMM-DATE-SEMNU PIC 99.                               00690320
          02 COMM-DATE-FILLER     PIC X(8).                             00690330
      *                                                                 00720000
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00730000
      *                                                                 00740000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.              00750000
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                        00760000
      *                                                                 00770000
      * ZONES MESSAGE CREATION COMMANDE    OU EDITION CDE----------     00780000
      *                                                                 00790000
      *   02 COMM-MESSAGE              PIC X(78).                       00800000
      *                                                                 00810000
      * ZONES DONNEES COMMUNES AU RELEVE DE PRIX        ---------- 170  00820000
      *                                                                 00830000
          02 COMM-RX30-COMMUN.                                          00840000
      *                                                                 00850000
              03 COMM-RX30-NSOC              PIC X(3).                  00850100
              03 COMM-RX30-NMAG              PIC X(3).                  00850200
              03 COMM-RX30-LMAG              PIC X(20).                 00850201
              03 COMM-RX30-SIEGE             PIC X(1).                  00850202
              03 COMM-RX30-ZONPRIX           PIC X(2).                  00850210
              03 COMM-RX30-NCONC             PIC X(4).                  00850300
              03 COMM-RX30-LENSCONC          PIC X(15).                 00860100
              03 COMM-RX30-CPROFIL           PIC X(5).                  00860200
              03 COMM-RX30-WFONC             PIC X(3).                  00860300
              03 COMM-RX30-TRANS-SUP         PIC X(5).                  00860310
              03 COMM-RX30-TRANS-GEN         PIC X(5).                  00860320
              03 COMM-RX34-PAGE              PIC 9(2) COMP-3.           00860400
              03 COMM-RX34-NBP               PIC 9(2) COMP-3.           00860500
              03 COMM-RX30-CRAYON            PIC X(5).                  00860200
              03 COMM-RX30-LRAYON            PIC X(20).                 00860200
              03 COMM-RX30-MENU              PIC X.                             
              03 COMM-RX30-SUITE             PIC X(76).                 00870000
                                                                        00870100
      *                                                                 01000000
      * ZONES DONNEES SPECIFIQUES MENU RX30 RX40        ----------3474  01000020
      *                                                                 01000030
          02 COMM-RX30-APPLI.                                           01000031
              03 COMM-RX30-RELEVE            PIC X(7).                  01000040
              03 COMM-RX40-DATREL-SAMJ.                                 01000100
                  04 COMM-RX40-DATREL-SAMJ-S  PIC XX.                   01000200
                  04 COMM-RX40-DATREL-SAMJ-A  PIC XX.                   01000300
                  04 COMM-RX40-DATREL-SAMJ-M  PIC XX.                   01000400
                  04 COMM-RX40-DATREL-SAMJ-J  PIC XX.                   01000500
              03 COMM-RX40-DATREL-OUT.                                  01000600
                  04 COMM-RX40-DATREL-OUT-J   PIC XX.                   01000700
                  04 FILLER                   PIC X VALUE '/'.          01000800
                  04 COMM-RX40-DATREL-OUT-M   PIC XX.                   01000900
                  04 FILLER                   PIC X VALUE '/'.          01001000
                  04 COMM-RX40-DATREL-OUT-A   PIC XX.                   01001100
                  04 FILLER                   PIC X VALUE '/'.          01001200
                 04 FILLER                   PIC X(10).                 01060000
                                                                        01060100
             03 COMM-RX30-LPROFIL        PIC X(20).                     01260147
             03 COMM-RX30-ACID           PIC X(4).                      01260120
             03 COMM-RX30-NLIEU          PIC X(3).                      01260121
             03 COMM-RX30-TYPE-MAG       PIC X(5).                      01260121
             03 COMM-RX40-CVENDEUR       PIC X(6).                      01260122
             03 COMM-RX40-LVENDEUR       PIC X(15).                     01260123
             03 COMM-RX41-NCODIC         PIC X(7).                      01260146
             03 COMM-RX42-REFFOU         PIC X(20).                     01260147
             03 COMM-RX30-PRG            PIC X(5).                      01260200
             03 COMM-RX30-TOP            PIC X(1).                      01260300
             03 COMM-RX30-TBFAMILLE.                                    01260300
                05 COMM-RX30-CFAMILLE     OCCURS 12    PIC X(5).        00860200
                05 COMM-RX30-LFAMILLE     OCCURS 12    PIC X(20).       00860200
             03 COMM-RX30-CRAYON-SAIS.                                  01260300
                05 COMM-RX30-CRAYONS      OCCURS 4    PIC X(5).         00860200
             03 COMM-RX30-CRAYON-NCONC.                                 01260300
                05 COMM-RX30-CRAYCONC     OCCURS 15   PIC X(5).         00860200
             03 COMM-RX30-VALIDE         PIC X.                                 
             03 COMM-RX30-MEM-SAUT.                                             
                05 COMM-RX30-MEM-FAM     PIC X(05).                             
                05 COMM-RX30-MEM-MARK    PIC X(20).                             
             03 COMM-RX30-FILLER         PIC X(3011).                   01260400
      ***************************************************************** 01270000
                                                                                
