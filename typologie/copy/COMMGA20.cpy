      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
          02 COMM-GA20-APPLI REDEFINES COMM-GA00-APPLI.                         
             05 COMM-GA20-FONCT               PIC X(3).                         
             05 COMM-GA20-CETAT               PIC X(10).                        
             05 COMM-GA20-NO-PAGE             PIC S9(03) COMP-3.                
             05 COMM-GA20-CFAMILLE            PIC X(5).                         
             05 COMM-GA20-LFAMILLE            PIC X(20).                        
             05 COMM-GA20-NSEQ                PIC X(2).                         
             05 COMM-GA20-TAB.                                                  
                10 COMM-GA20-TABLE     OCCURS  3.                               
                   15 COMM-GA20-NIMB          PIC  X.                           
                   15 COMM-GA20-CMARK         PIC  X(5).                        
                   15 COMM-GA20-LMARK         PIC  X(20).                       
             05 COMM-GA20-PGM                 PIC  X(3604).                     
                                                                                
