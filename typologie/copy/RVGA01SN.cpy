      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE IFOPA MODES PAIEMENT PAR ORGANISME     *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01SN.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01SN.                                                            
      *}                                                                        
           05  IFOPA-CTABLEG2    PIC X(15).                                     
           05  IFOPA-CTABLEG2-REDEF REDEFINES IFOPA-CTABLEG2.                   
               10  IFOPA-CORGAN          PIC X(03).                             
               10  IFOPA-CTYPMVT         PIC X(05).                             
           05  IFOPA-WTABLEG     PIC X(80).                                     
           05  IFOPA-WTABLEG-REDEF  REDEFINES IFOPA-WTABLEG.                    
               10  IFOPA-CMODPAI         PIC X(03).                             
               10  IFOPA-NJRN            PIC X(03).                             
               10  IFOPA-IMPDET          PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01SN-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01SN-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IFOPA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  IFOPA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IFOPA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  IFOPA-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
