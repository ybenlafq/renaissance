      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RVTH1000                           *        
      ******************************************************************        
       01  RVTH1000.                                                            
      *    *************************************************************        
      *                       IDREGLE                                           
           10 TH10-IDREGLE         PIC X(10).                                   
      *    *************************************************************        
      *                       CFAM                                              
           10 TH10-CFAM            PIC X(05).                                   
      *    *************************************************************        
      *                       CMARQ                                             
           10 TH10-CMARQ           PIC X(05).                                   
      *    *************************************************************        
      *                       CMARQ                                             
           10 TH10-NCODIC          PIC X(07).                                   
      *    *************************************************************        
      *                       DEFFET                                            
           10 TH10-DEFFET          PIC X(08).                                   
      *    *************************************************************        
      *                       VALMTT                                            
           10 TH10-VALMTT          PIC S9(05)V9(02) COMP-3.                     
      *    *************************************************************        
      *                       VALPOURCENT                                       
           10 TH10-VALPOURCENT     PIC S9(03)V9(02) COMP-3.                     
      *    *************************************************************        
      *                       BORNEMTT                                          
           10 TH10-BORNEMTT        PIC S9(05)V9(02) COMP-3.                     
      *    *************************************************************        
      *                       BORNEPOURCENT                                     
           10 TH10-BORNEPOURCENT   PIC S9(03)V9(02) COMP-3.                     
      *    *************************************************************        
      *                       DCRESYST                                          
           10 TH10-DCRESYST        PIC X(26).                                   
      *    *************************************************************        
      *                       DMAJSYST                                          
           10 TH10-DMAJSYST        PIC X(26).                                   
      *                                                                         
      ******************************************************************        
      *   LISTE DES FLAGS DE LA TABLE RVNV1800                                  
      ******************************************************************        
       01  RVTH1000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH10-IDREGLE-F           PIC S9(4) COMP.                         
      *--                                                                       
           10  TH10-IDREGLE-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH10-CFAM-F              PIC S9(4) COMP.                         
      *--                                                                       
           10  TH10-CFAM-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH10-CMARQ-F             PIC S9(4) COMP.                         
      *--                                                                       
           10  TH10-CMARQ-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH10-NCODIC-F            PIC S9(4) COMP.                         
      *--                                                                       
           10  TH10-NCODIC-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH10-DEFFET-F            PIC S9(4) COMP.                         
      *--                                                                       
           10  TH10-DEFFET-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH10-VALMTT-F            PIC S9(4) COMP.                         
      *--                                                                       
           10  TH10-VALMTT-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH10-VALPOURCENT-F       PIC S9(4) COMP.                         
      *--                                                                       
           10  TH10-VALPOURCENT-F       PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH10-BORNEMTT-F          PIC S9(4) COMP.                         
      *--                                                                       
           10  TH10-BORNEMTT-F          PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH10-BORNEPOURCENT-F     PIC S9(4) COMP.                         
      *--                                                                       
           10  TH10-BORNEPOURCENT-F     PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH10-DCRESYST-F          PIC S9(4) COMP.                         
      *--                                                                       
           10  TH10-DCRESYST-F          PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10  TH10-DMAJSYST-F          PIC S9(4) COMP.                         
      *--                                                                       
           10  TH10-DMAJSYST-F          PIC S9(4) COMP-5.                       
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 11      *        
      ******************************************************************        
                                                                                
