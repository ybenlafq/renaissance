      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE ENAFA ENTREPOT D AFFIL./AFFECT. PROF   *        
      *----------------------------------------------------------------*        
       01  RVGA01CP.                                                            
           05  ENAFA-CTABLEG2    PIC X(15).                                     
           05  ENAFA-CTABLEG2-REDEF REDEFINES ENAFA-CTABLEG2.                   
               10  ENAFA-NSOC            PIC X(03).                             
               10  ENAFA-NSOC-N         REDEFINES ENAFA-NSOC                    
                                         PIC 9(03).                             
               10  ENAFA-NMAG            PIC X(03).                             
               10  ENAFA-NMAG-N         REDEFINES ENAFA-NMAG                    
                                         PIC 9(03).                             
               10  ENAFA-CMODDEL         PIC X(03).                             
           05  ENAFA-WTABLEG     PIC X(80).                                     
           05  ENAFA-WTABLEG-REDEF  REDEFINES ENAFA-WTABLEG.                    
               10  ENAFA-CPROAFF         PIC X(05).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01CP-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ENAFA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  ENAFA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ENAFA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  ENAFA-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
