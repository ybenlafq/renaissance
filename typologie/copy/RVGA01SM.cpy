      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE IFMOT MOTS CLES DE L APPLICATION       *        
      *----------------------------------------------------------------*        
       01  RVGA01SM.                                                            
           05  IFMOT-CTABLEG2    PIC X(15).                                     
           05  IFMOT-CTABLEG2-REDEF REDEFINES IFMOT-CTABLEG2.                   
               10  IFMOT-CMOTCLE         PIC X(10).                             
           05  IFMOT-WTABLEG     PIC X(80).                                     
           05  IFMOT-WTABLEG-REDEF  REDEFINES IFMOT-WTABLEG.                    
               10  IFMOT-CONTENU         PIC X(30).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01SM-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IFMOT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  IFMOT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IFMOT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  IFMOT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
