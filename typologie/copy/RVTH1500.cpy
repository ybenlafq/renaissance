      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVTH1500                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTH1500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTH1500.                                                            
      *}                                                                        
      *                       NCODIC                                            
           10 TH15-NCODIC          PIC X(7).                                    
      *                       COPCO                                             
           10 TH15-COPCO           PIC X(3).                                    
      *                       DEFFET                                            
           10 TH15-DEFFET          PIC X(8).                                    
      *                       CTAXE                                             
           10 TH15-CTAXE           PIC X(5).                                    
      *                       MONTANT                                           
           10 TH15-MONTANT         PIC S9(5)V9(2) USAGE COMP-3.                 
      *                       DCRESYST                                          
           10 TH15-DCRESYST        PIC X(26).                                   
      *                       DMAJSYST                                          
           10 TH15-DMAJSYST        PIC X(26).                                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 7       *        
      ******************************************************************        
                                                                                
