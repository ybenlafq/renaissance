      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SLRSL SL : REGROUPEMENTS SOUS-LIEUX    *        
      *----------------------------------------------------------------*        
       01  RVGA01ZD.                                                            
           05  SLRSL-CTABLEG2    PIC X(15).                                     
           05  SLRSL-CTABLEG2-REDEF REDEFINES SLRSL-CTABLEG2.                   
               10  SLRSL-CRSL            PIC X(05).                             
           05  SLRSL-WTABLEG     PIC X(80).                                     
           05  SLRSL-WTABLEG-REDEF  REDEFINES SLRSL-WTABLEG.                    
               10  SLRSL-LRSL            PIC X(20).                             
               10  SLRSL-INDETAT         PIC X(04).                             
               10  SLRSL-INDEMPL         PIC X(06).                             
               10  SLRSL-CEMPL           PIC X(05).                             
               10  SLRSL-NSEQ            PIC X(03).                             
               10  SLRSL-NSEQ-N         REDEFINES SLRSL-NSEQ                    
                                         PIC 9(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01ZD-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLRSL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SLRSL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLRSL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SLRSL-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
