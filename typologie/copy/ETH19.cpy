      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * MENU PARAMETRAGE DES REGLES                                     00000020
      ***************************************************************** 00000030
       01   ETH19I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTRIL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWTRIL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MWTRIF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWTRII    PIC X.                                          00000170
           02 MLIGNEI OCCURS   12 TIMES .                               00000180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELCL  COMP PIC S9(4).                                 00000190
      *--                                                                       
             03 MSELCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSELCF  PIC X.                                          00000200
             03 FILLER  PIC X(4).                                       00000210
             03 MSELCI  PIC X.                                          00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCCOMPOSL    COMP PIC S9(4).                            00000230
      *--                                                                       
             03 MCCOMPOSL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCCOMPOSF    PIC X.                                     00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MCCOMPOSI    PIC X(3).                                  00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELRL  COMP PIC S9(4).                                 00000270
      *--                                                                       
             03 MSELRL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSELRF  PIC X.                                          00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MSELRI  PIC X.                                          00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MIDRGL  COMP PIC S9(4).                                 00000310
      *--                                                                       
             03 MIDRGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MIDRGF  PIC X.                                          00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MIDRGI  PIC X(10).                                      00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOPCOL      COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MCOPCOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCOPCOF      PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MCOPCOI      PIC X(3).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLOPCOL      COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MLOPCOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLOPCOF      PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MLOPCOI      PIC X(20).                                 00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000430
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MLIBERRI  PIC X(78).                                      00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MCODTRAI  PIC X(4).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MCICSI    PIC X(5).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MNETNAMI  PIC X(8).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MSCREENI  PIC X(4).                                       00000620
      ***************************************************************** 00000630
      * MENU PARAMETRAGE DES REGLES                                     00000640
      ***************************************************************** 00000650
       01   ETH19O REDEFINES ETH19I.                                    00000660
           02 FILLER    PIC X(12).                                      00000670
           02 FILLER    PIC X(2).                                       00000680
           02 MDATJOUA  PIC X.                                          00000690
           02 MDATJOUC  PIC X.                                          00000700
           02 MDATJOUP  PIC X.                                          00000710
           02 MDATJOUH  PIC X.                                          00000720
           02 MDATJOUV  PIC X.                                          00000730
           02 MDATJOUO  PIC X(10).                                      00000740
           02 FILLER    PIC X(2).                                       00000750
           02 MTIMJOUA  PIC X.                                          00000760
           02 MTIMJOUC  PIC X.                                          00000770
           02 MTIMJOUP  PIC X.                                          00000780
           02 MTIMJOUH  PIC X.                                          00000790
           02 MTIMJOUV  PIC X.                                          00000800
           02 MTIMJOUO  PIC X(5).                                       00000810
           02 FILLER    PIC X(2).                                       00000820
           02 MWTRIA    PIC X.                                          00000830
           02 MWTRIC    PIC X.                                          00000840
           02 MWTRIP    PIC X.                                          00000850
           02 MWTRIH    PIC X.                                          00000860
           02 MWTRIV    PIC X.                                          00000870
           02 MWTRIO    PIC X.                                          00000880
           02 MLIGNEO OCCURS   12 TIMES .                               00000890
             03 FILLER       PIC X(2).                                  00000900
             03 MSELCA  PIC X.                                          00000910
             03 MSELCC  PIC X.                                          00000920
             03 MSELCP  PIC X.                                          00000930
             03 MSELCH  PIC X.                                          00000940
             03 MSELCV  PIC X.                                          00000950
             03 MSELCO  PIC X.                                          00000960
             03 FILLER       PIC X(2).                                  00000970
             03 MCCOMPOSA    PIC X.                                     00000980
             03 MCCOMPOSC    PIC X.                                     00000990
             03 MCCOMPOSP    PIC X.                                     00001000
             03 MCCOMPOSH    PIC X.                                     00001010
             03 MCCOMPOSV    PIC X.                                     00001020
             03 MCCOMPOSO    PIC X(3).                                  00001030
             03 FILLER       PIC X(2).                                  00001040
             03 MSELRA  PIC X.                                          00001050
             03 MSELRC  PIC X.                                          00001060
             03 MSELRP  PIC X.                                          00001070
             03 MSELRH  PIC X.                                          00001080
             03 MSELRV  PIC X.                                          00001090
             03 MSELRO  PIC X.                                          00001100
             03 FILLER       PIC X(2).                                  00001110
             03 MIDRGA  PIC X.                                          00001120
             03 MIDRGC  PIC X.                                          00001130
             03 MIDRGP  PIC X.                                          00001140
             03 MIDRGH  PIC X.                                          00001150
             03 MIDRGV  PIC X.                                          00001160
             03 MIDRGO  PIC X(10).                                      00001170
             03 FILLER       PIC X(2).                                  00001180
             03 MCOPCOA      PIC X.                                     00001190
             03 MCOPCOC PIC X.                                          00001200
             03 MCOPCOP PIC X.                                          00001210
             03 MCOPCOH PIC X.                                          00001220
             03 MCOPCOV PIC X.                                          00001230
             03 MCOPCOO      PIC X(3).                                  00001240
             03 FILLER       PIC X(2).                                  00001250
             03 MLOPCOA      PIC X.                                     00001260
             03 MLOPCOC PIC X.                                          00001270
             03 MLOPCOP PIC X.                                          00001280
             03 MLOPCOH PIC X.                                          00001290
             03 MLOPCOV PIC X.                                          00001300
             03 MLOPCOO      PIC X(20).                                 00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MLIBERRA  PIC X.                                          00001330
           02 MLIBERRC  PIC X.                                          00001340
           02 MLIBERRP  PIC X.                                          00001350
           02 MLIBERRH  PIC X.                                          00001360
           02 MLIBERRV  PIC X.                                          00001370
           02 MLIBERRO  PIC X(78).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MCODTRAA  PIC X.                                          00001400
           02 MCODTRAC  PIC X.                                          00001410
           02 MCODTRAP  PIC X.                                          00001420
           02 MCODTRAH  PIC X.                                          00001430
           02 MCODTRAV  PIC X.                                          00001440
           02 MCODTRAO  PIC X(4).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MCICSA    PIC X.                                          00001470
           02 MCICSC    PIC X.                                          00001480
           02 MCICSP    PIC X.                                          00001490
           02 MCICSH    PIC X.                                          00001500
           02 MCICSV    PIC X.                                          00001510
           02 MCICSO    PIC X(5).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MNETNAMA  PIC X.                                          00001540
           02 MNETNAMC  PIC X.                                          00001550
           02 MNETNAMP  PIC X.                                          00001560
           02 MNETNAMH  PIC X.                                          00001570
           02 MNETNAMV  PIC X.                                          00001580
           02 MNETNAMO  PIC X(8).                                       00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MSCREENA  PIC X.                                          00001610
           02 MSCREENC  PIC X.                                          00001620
           02 MSCREENP  PIC X.                                          00001630
           02 MSCREENH  PIC X.                                          00001640
           02 MSCREENV  PIC X.                                          00001650
           02 MSCREENO  PIC X(4).                                       00001660
                                                                                
