      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : INTERROGATION DU REFERENTIEL NATIONAL  (GN20)          *         
      *****************************************************************         
      *                                                               *         
       01  TS-GN20.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GN20-LONG                 PIC S9(03) COMP-3                    
                                           VALUE +142.                          
      *----------------------------------  DONNEES  TS                          
           02 TS-GN20-DONNEES.                                                  
      *----------------------------------  NUMERO CODIC                         
              03 TS-GN20-NCODIC            PIC X(7).                            
      *----------------------------------  CODE FAMILLE                         
              03 TS-GN20-CFAM              PIC X(05).                           
      *----------------------------------  CODE MARQUE                          
              03 TS-GN20-CMARQ             PIC X(5).                            
      *----------------------------------  LIBELLE REFERENCE FOURNISSEUR        
              03 TS-GN20-LREFFOU           PIC X(20).                           
      *----------------------------------  STATUT                               
              03 TS-GN20-STATUT            PIC X(03).                           
      *----------------------------------                                       
              03 TS-GN20-DONNEE.                                                
      *----------------------------------  DONNEES REF                          
                 05 TS-GN20-DONNEES1          PIC X(32).                        
      *----------------------------------  DONNEES REF2                         
                 05 TS-GN20-DONNEES2          PIC X(32).                        
      *----------------------------------  DONNEES OFFRE ACTIVE                 
                 05 TS-GN20-DONNEES3          PIC X(32).                        
                                                                                
