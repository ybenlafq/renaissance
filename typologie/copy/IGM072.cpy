      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGM072 AU 22/01/2002  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,03,PD,A,                          *        
      *                           20,20,BI,A,                          *        
      *                           40,20,BI,A,                          *        
      *                           60,20,BI,A,                          *        
      *                           80,07,BI,A,                          *        
      *                           87,07,BI,A,                          *        
      *                           94,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGM072.                                                        
            05 NOMETAT-IGM072           PIC X(6) VALUE 'IGM072'.                
            05 RUPTURES-IGM072.                                                 
           10 IGM072-NSOCIETE           PIC X(03).                      007  003
           10 IGM072-NZONE              PIC X(02).                      010  002
           10 IGM072-CHEFPROD           PIC X(05).                      012  005
           10 IGM072-WSEQFAM            PIC S9(05)      COMP-3.         017  003
           10 IGM072-LVMARKET1          PIC X(20).                      020  020
           10 IGM072-LVMARKET2          PIC X(20).                      040  020
           10 IGM072-LVMARKET3          PIC X(20).                      060  020
           10 IGM072-NCODIC2            PIC X(07).                      080  007
           10 IGM072-NCODIC             PIC X(07).                      087  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGM072-SEQUENCE           PIC S9(04) COMP.                094  002
      *--                                                                       
           10 IGM072-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGM072.                                                   
           10 IGM072-CAPPRO             PIC X(05).                      096  005
           10 IGM072-CFAM               PIC X(05).                      101  005
           10 IGM072-CMARQ              PIC X(05).                      106  005
           10 IGM072-CODLVM1            PIC X(01).                      111  001
           10 IGM072-CODLVM2            PIC X(01).                      112  001
           10 IGM072-CODLVM3            PIC X(01).                      113  001
           10 IGM072-DESCR1             PIC X(05).                      114  005
           10 IGM072-DESCR2             PIC X(05).                      119  005
           10 IGM072-DESCR3             PIC X(05).                      124  005
           10 IGM072-LCHEFPROD          PIC X(20).                      129  020
           10 IGM072-LFAM               PIC X(20).                      149  020
           10 IGM072-LREFFOURN          PIC X(20).                      169  020
           10 IGM072-LSTATCOMP          PIC X(03).                      189  003
           10 IGM072-LZONPRIX           PIC X(20).                      192  020
           10 IGM072-LZPCOMP            PIC X(20).                      212  020
           10 IGM072-NZONPRIX           PIC X(02).                      232  002
           10 IGM072-NZPCOMP            PIC X(02).                      234  002
           10 IGM072-WD15               PIC X(01).                      236  001
           10 IGM072-WEDIT              PIC X(01).                      237  001
           10 IGM072-WGROUPE            PIC X(01).                      238  001
           10 IGM072-WOA                PIC X(01).                      239  001
           10 IGM072-WSENSAPP           PIC X(01).                      240  001
           10 IGM072-PCA1S              PIC S9(11)V9(2) COMP-3.         241  007
           10 IGM072-PCA4S              PIC S9(11)V9(2) COMP-3.         248  007
           10 IGM072-PCHT               PIC S9(07)V9(6) COMP-3.         255  007
           10 IGM072-PCOMMART           PIC S9(05)V9(2) COMP-3.         262  004
           10 IGM072-PCOMMFOR           PIC S9(05)V9(2) COMP-3.         266  004
           10 IGM072-PMBU1S             PIC S9(11)V9(2) COMP-3.         270  007
           10 IGM072-PMBU4S             PIC S9(11)V9(2) COMP-3.         277  007
           10 IGM072-PMTPRIMES          PIC S9(11)V9(2) COMP-3.         284  007
           10 IGM072-POURCENT           PIC S9(05)V9(2) COMP-3.         291  004
           10 IGM072-PRAR               PIC S9(06)V9(2) COMP-3.         295  005
           10 IGM072-PRMP               PIC S9(07)V9(2) COMP-3.         300  005
           10 IGM072-PSTDMAG            PIC S9(06)V9(2) COMP-3.         305  005
           10 IGM072-PSTDTTC            PIC S9(06)V9(2) COMP-3.         310  005
           10 IGM072-QSTOCKDEP          PIC S9(05)      COMP-3.         315  003
           10 IGM072-QSTOCKMAG          PIC S9(05)      COMP-3.         318  003
           10 IGM072-QSTOCKMAGP         PIC S9(05)      COMP-3.         321  003
           10 IGM072-QSTOCKMAGS         PIC S9(05)      COMP-3.         324  003
           10 IGM072-QTAUXTVA           PIC S9(02)V9(3) COMP-3.         327  003
           10 IGM072-QV1S               PIC S9(07)      COMP-3.         330  004
           10 IGM072-QV2S               PIC S9(07)      COMP-3.         334  004
           10 IGM072-QV3S               PIC S9(07)      COMP-3.         338  004
           10 IGM072-QV4S               PIC S9(07)      COMP-3.         342  004
           10 IGM072-SETUP              PIC S9(06)V9(2) COMP-3.         346  005
           10 IGM072-ZTRI               PIC S9(07)V9(2) COMP-3.         351  005
           10 IGM072-ZTRI2              PIC S9(07)      COMP-3.         356  004
            05 FILLER                      PIC X(153).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGM072-LONG           PIC S9(4)   COMP  VALUE +359.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGM072-LONG           PIC S9(4) COMP-5  VALUE +359.           
                                                                                
      *}                                                                        
