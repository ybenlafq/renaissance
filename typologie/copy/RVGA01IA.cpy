      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE ZPRIX ZONE DE PRIX                     *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01IA.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01IA.                                                            
      *}                                                                        
           05  ZPRIX-CTABLEG2    PIC X(15).                                     
           05  ZPRIX-CTABLEG2-REDEF REDEFINES ZPRIX-CTABLEG2.                   
               10  ZPRIX-SOCIETE         PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  ZPRIX-SOCIETE-N      REDEFINES ZPRIX-SOCIETE                 
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  ZPRIX-ZONE            PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  ZPRIX-ZONE-N         REDEFINES ZPRIX-ZONE                    
                                         PIC 9(02).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  ZPRIX-WTABLEG     PIC X(80).                                     
           05  ZPRIX-WTABLEG-REDEF  REDEFINES ZPRIX-WTABLEG.                    
               10  ZPRIX-LIBELLE         PIC X(20).                             
               10  ZPRIX-WTYP            PIC X(01).                             
               10  ZPRIX-WMGI            PIC X(01).                             
               10  ZPRIX-EDITDACE        PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01IA-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01IA-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ZPRIX-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  ZPRIX-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ZPRIX-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  ZPRIX-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
