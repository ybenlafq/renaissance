      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGA8300                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGA8300                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA8300.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA8300.                                                            
      *}                                                                        
           02  GA83-NCODIC                                                      
               PIC X(0007).                                                     
           02  GA83-CFAM                                                        
               PIC X(0005).                                                     
           02  GA83-CDESCRIPTIF                                                 
               PIC X(0005).                                                     
           02  GA83-CVDESCRIPT                                                  
               PIC X(0005).                                                     
           02  GA83-CMARKETING                                                  
               PIC X(0005).                                                     
           02  GA83-CVMARKETING                                                 
               PIC X(0005).                                                     
           02  GA83-LVMARKETING                                                 
               PIC X(0020).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGA8300                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA8300-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA8300-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA83-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA83-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA83-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA83-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA83-CDESCRIPTIF-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA83-CDESCRIPTIF-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA83-CVDESCRIPT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA83-CVDESCRIPT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA83-CMARKETING-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA83-CMARKETING-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA83-CVMARKETING-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA83-CVMARKETING-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GA83-LVMARKETING-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GA83-LVMARKETING-F                                               
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
