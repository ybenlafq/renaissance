      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPR445 AU 23/11/1998  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,05,BI,A,                          *        
      *                           18,05,BI,A,                          *        
      *                           23,01,BI,A,                          *        
      *                           24,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPR445.                                                        
            05 NOMETAT-IPR445           PIC X(6) VALUE 'IPR445'.                
            05 RUPTURES-IPR445.                                                 
           10 IPR445-NSOCIETE           PIC X(03).                      007  003
           10 IPR445-NLIEU              PIC X(03).                      010  003
           10 IPR445-CTYPPREST          PIC X(05).                      013  005
           10 IPR445-COPER              PIC X(05).                      018  005
           10 IPR445-WPRODMAJ           PIC X(01).                      023  001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPR445-SEQUENCE           PIC S9(04) COMP.                024  002
      *--                                                                       
           10 IPR445-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPR445.                                                   
           10 IPR445-LLIEU              PIC X(20).                      026  020
           10 IPR445-TX-CONCRETISATION  PIC X(01).                      046  001
           10 IPR445-WMAJEUR            PIC X(01).                      047  001
           10 IPR445-WOPTIONS           PIC X(01).                      048  001
           10 IPR445-PPRIME             PIC S9(05)V9(2) COMP-3.         049  004
           10 IPR445-PVTOTAL            PIC S9(07)V9(2) COMP-3.         053  005
           10 IPR445-QVENDUES           PIC S9(06)      COMP-3.         058  004
           10 IPR445-DDEBUT             PIC X(08).                      062  008
           10 IPR445-DFIN               PIC X(08).                      070  008
            05 FILLER                      PIC X(435).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPR445-LONG           PIC S9(4)   COMP  VALUE +077.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPR445-LONG           PIC S9(4) COMP-5  VALUE +077.           
                                                                                
      *}                                                                        
