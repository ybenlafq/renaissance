      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE QCGRS LIEUX SAV CARTES T               *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01QJ.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01QJ.                                                            
      *}                                                                        
           05  QCGRS-CTABLEG2    PIC X(15).                                     
           05  QCGRS-CTABLEG2-REDEF REDEFINES QCGRS-CTABLEG2.                   
               10  QCGRS-NSOCLIEU        PIC X(06).                             
           05  QCGRS-WTABLEG     PIC X(80).                                     
           05  QCGRS-WTABLEG-REDEF  REDEFINES QCGRS-WTABLEG.                    
               10  QCGRS-WACTIF          PIC X(01).                             
               10  QCGRS-LSAV            PIC X(20).                             
               10  QCGRS-GRSAV           PIC X(03).                             
               10  QCGRS-FEXTACT         PIC X(02).                             
               10  QCGRS-SAVMGI          PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01QJ-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01QJ-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QCGRS-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  QCGRS-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QCGRS-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  QCGRS-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
