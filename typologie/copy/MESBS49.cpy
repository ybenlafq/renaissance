      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGES MQ INTERROGATION STATUT VENTE LOCAL                     
      *                                                                         
      *****************************************************************         
      *  POUR MBS49                                                             
      *                                                                         
           10  WS-MESSAGE REDEFINES COMM-MQ20-MESSAGE.                          
      *---                                                                      
           15  MES-ENTETE.                                                      
               20   MES-TYPE      PIC    X(3).                                  
               20   MES-NSOCMSG   PIC    X(3).                                  
               20   MES-NLIEUMSG  PIC    X(3).                                  
               20   MES-NSOCDST   PIC    X(3).                                  
               20   MES-NLIEUDST  PIC    X(3).                                  
               20   MES-NORD      PIC    9(8).                                  
               20   MES-LPROG     PIC    X(10).                                 
               20   MES-DJOUR     PIC    X(8).                                  
               20   MES-WSID      PIC    X(10).                                 
               20   MES-USER      PIC    X(10).                                 
               20   MES-CHRONO    PIC    9(7).                                  
               20   MES-NBRMSG    PIC    9(7).                                  
               20   MES-FILLER    PIC    X(30).                                 
           15  MES-INTER-STATUT.                                                
               20   MES-DVENTE               PIC X(8).                          
               20   MES-NORDRE               PIC X(5).                          
               20   MES-NVENTE               PIC X(7).                          
               20   MES-TYPVTE               PIC X(1).                          
           10   LONGUEUR-MESSAGE         PIC S9(03) VALUE +126.                 
                                                                                
