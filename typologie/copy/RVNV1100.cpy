      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVNV1100                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA VUE RVNV1100                           
      **********************************************************                
       01  RVNV1100.                                                            
      *---------------------  EMETTEUR                                          
           05  NV11-CEMETTEUR       PIC X(20).                                  
      *---------------------  DESTINATAIRE                                      
           05  NV11-CDESTI          PIC X(20).                                  
      *---------------------  DTD WEBSPHERE                                     
           05  NV11-CDTD            PIC X(20).                                  
      *---------------------  IDENTIFIANT DU MSG ENVOYE                         
           05  NV11-IDENTIFIANT     PIC X(12).                                  
      *---------------------  DATE D ENVOI DU MSG                               
           05  NV11-DENVOI          PIC X(08).                                  
      *---------------------  CODE RETOUR                                       
           05  NV11-CRETOUR         PIC X(02).                                  
      *---------------------  LIBELLE ANOMALIE                                  
           05  NV11-LIBANO          PIC X(20).                                  
      *---------------------  TIMESTAMP DE TRAITEMENT                           
           05  NV11-TIMESTAMP       PIC X(26).                                  
      *---------------------  NBRE DE MESSAGES REJETES                          
           05  NV11-QMSGREJET       PIC S9(7)V USAGE COMP-3.                    
      *---------------------  DSYST                                             
           05  NV11-DSYST           PIC S9(13)V USAGE COMP-3.                   
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVNV1100                                  
      **********************************************************                
       01  RVNV1100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV11-CEMETTEUR-F         PIC S9(4) COMP.                         
      *--                                                                       
           05  NV11-CEMETTEUR-F         PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV11-CDESTI-F            PIC S9(4) COMP.                         
      *--                                                                       
           05  NV11-CDESTI-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV11-CDTD-F              PIC S9(4) COMP.                         
      *--                                                                       
           05  NV11-CDTD-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV11-IDENTIFIANT-F       PIC S9(4) COMP.                         
      *--                                                                       
           05  NV11-IDENTIFIANT-F       PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV11-DENVOI-F            PIC S9(4) COMP.                         
      *--                                                                       
           05  NV11-DENVOI-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV11-CRETOUR-F           PIC S9(4) COMP.                         
      *--                                                                       
           05  NV11-CRETOUR-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV11-LIBANO-F            PIC S9(4) COMP.                         
      *--                                                                       
           05  NV11-LIBANO-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV11-TIMESTAMP-F         PIC S9(4) COMP.                         
      *--                                                                       
           05  NV11-TIMESTAMP-F         PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV11-QMSGREJET-F         PIC S9(4) COMP.                         
      *--                                                                       
           05  NV11-QMSGREJET-F         PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NV11-DSYST-F             PIC S9(4) COMP.                         
      *                                                                         
      *--                                                                       
           05  NV11-DSYST-F             PIC S9(4) COMP-5.                       
                                                                                
      *}                                                                        
