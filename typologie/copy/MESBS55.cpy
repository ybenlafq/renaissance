      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00010000
      *   COPY MESSAGE MQ C&C REMONTEE DU 400                           00020002
      ***************************************************************** 00040000
      *                                                                 00060000
       01  WS-MQ10-MBS55.                                               00070008
               10   FILLER                    PIC X(24).                00090005
               10   FILLER                    PIC X(24).                00100008
         02 WS-MQ-MBS55-CODRET                PIC XX.                   00110004
         02 MESBS55-ENTETE.                                             00130001
               05   MESBS55-TYPE              PIC X(3).                 00140002
               05   MESBS55-NSOCMSG           PIC X(3).                 00150002
               05   MESBS55-NLIEUMSG          PIC X(3).                 00160002
               05   MESBS55-NSOCDST           PIC X(3).                 00170002
               05   MESBS55-NLIEUDST          PIC X(3).                 00180002
               05   MESBS55-NORD              PIC 9(8).                 00190002
               05   MESBS55-LPROG             PIC X(10).                00200002
               05   MESBS55-DJOUR             PIC X(8).                 00210002
               05   MESBS55-WSID              PIC X(10).                00220002
               05   MESBS55-USER              PIC X(10).                00230002
               05   MESBS55-CHRONO            PIC 9(7).                 00240002
               05   MESBS55-NBRMSG            PIC 9(7).                 00250002
               05   MESBS55-FILLER            PIC X(30).                00260002
                                                                        00270000
         02 MESBS55-DATA.                                               00280001
               05   MESBS55-DUPVTE            PIC X(01).                00281002
               05   MESBS55-NB-LIGNES         PIC 9(02).                00282002
               05   MESBS55-DVENTE            PIC X(08).                00283007
               05   MESBS55-NORDV             PIC X(05).                00290007
               05   MESBS55-NSOCIETE          PIC X(03).                00300003
               05   MESBS55-NLIEU             PIC X(03).                00310002
               05   MESBS55-NVENTE            PIC X(07).                00320002
               05   MESBS55-POSTE-NCODIC OCCURS 40.                     00321001
                    10   MESBS55-NCODIC       PIC X(07).                00330001
                    10   MESBS55-QSTOCK       PIC S9(05).               00340001
                    10   MESBS55-NSEQNQ       PIC 9(05).                00350001
      *  VALEURS DU TYPE DE RETRAIT : COMPTOIR / CONSIGNE / � BLANC             
               05   MESBS55-CHOIX-RETRAIT     PIC X(10).                        
      *  FILLER D�DUIT DU TYPE DE RETRAIT                                       
      *  02 FILLER                            PIC X(49836).             00360002
         02 FILLER                            PIC X(49826).                     
                                                                                
