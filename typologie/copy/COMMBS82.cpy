      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES VENTES                               *        
      *  TRANSACTION: GV00                                             *        
      *  TITRE      : COMMAREA DE L'APPLICATION BS82 APELLEE PAR GV00  *        
      *  LONGUEUR   : 200                                              *        
      *                                                                *        
      ******************************************************************        
      *                                                                 00230000
      *                                                                 00230000
       01  COMM-BS82-APPLI.                                             00260000
           02 COMM-BS82-NSOCIETE        PIC X(03).                      00320000
           02 COMM-BS82-NLIEU           PIC X(03).                      00330000
           02 COMM-BS82-NVENTE          PIC X(07).                      00340000
           02 COMM-BS82-NVENTE-NEW      PIC X(07).                      00340000
           02 COMM-BS82-TYPVTE          PIC X(01).                      00340000
           02 COMM-BS82-CODE-RETOUR     PIC X(01).                      00340000
           02 COMM-BS82-MESSAGE.                                                
                10 COMM-BS82-CODRET          PIC X.                             
                   88  COMM-BS82-CODRET-OK   VALUE ' '.                         
                   88  COMM-BS82-CODRET-ERREUR  VALUE '1'.                      
                10 COMM-BS82-LIBERR          PIC X(57).                         
           02 FILLER                    PIC X(120).                     00340000
                                                                                
