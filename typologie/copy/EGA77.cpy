      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA77   EGA77                                              00000020
      ***************************************************************** 00000030
       01   EGA77I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCODICI  PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLREFI    PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCRECCL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDCRECCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDCRECCF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDCRECCI  PIC X(8).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNFAMI    PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLFAMI    PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRECEPL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDRECEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDRECEPF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDRECEPI  PIC X(8).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMARQL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNMARQF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNMARQI   PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLMARQI   PIC X(20).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMAJSTL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDMAJSTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDMAJSTF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDMAJSTI  PIC X(8).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLRESUML  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLRESUML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLRESUMF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLRESUMI  PIC X(20).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFCL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLREFCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLREFCF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLREFCI   PIC X(50).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTORREPL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MSTORREPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSTORREPF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MSTORREPI      PIC X.                                     00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMPTL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MLCOMPTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCOMPTF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLCOMPTI  PIC X(3).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPEL    COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MTYPEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTYPEF    PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MTYPEI    PIC X(2).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWSENVTL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MWSENVTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWSENVTF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MWSENVTI  PIC X.                                          00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSRECOUVL      COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MSRECOUVL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSRECOUVF      PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MSRECOUVI      PIC X(2).                                  00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWSAPPRL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MWSAPPRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWSAPPRF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MWSAPPRI  PIC X.                                          00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPROL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MCAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAPPROF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCAPPROI  PIC X(5).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAPPROL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MLAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLAPPROF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MLAPPROI  PIC X(20).                                      00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPOL   COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MCEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXPOF   PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MCEXPOI   PIC X(5).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLEXPOL   COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MLEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLEXPOF   PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MLEXPOI   PIC X(20).                                      00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MZONCMDI  PIC X(15).                                      00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MLIBERRI  PIC X(58).                                      00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MCODTRAI  PIC X(4).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MCICSI    PIC X(5).                                       00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MNETNAMI  PIC X(8).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MSCREENI  PIC X(4).                                       00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWWERL    COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MWWERL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MWWERF    PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MWWERI    PIC X.                                          00001330
      ***************************************************************** 00001340
      * SDF: EGA77   EGA77                                              00001350
      ***************************************************************** 00001360
       01   EGA77O REDEFINES EGA77I.                                    00001370
           02 FILLER    PIC X(12).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MDATJOUA  PIC X.                                          00001400
           02 MDATJOUC  PIC X.                                          00001410
           02 MDATJOUP  PIC X.                                          00001420
           02 MDATJOUH  PIC X.                                          00001430
           02 MDATJOUV  PIC X.                                          00001440
           02 MDATJOUO  PIC X(10).                                      00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MTIMJOUA  PIC X.                                          00001470
           02 MTIMJOUC  PIC X.                                          00001480
           02 MTIMJOUP  PIC X.                                          00001490
           02 MTIMJOUH  PIC X.                                          00001500
           02 MTIMJOUV  PIC X.                                          00001510
           02 MTIMJOUO  PIC X(5).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MPAGEA    PIC X.                                          00001540
           02 MPAGEC    PIC X.                                          00001550
           02 MPAGEP    PIC X.                                          00001560
           02 MPAGEH    PIC X.                                          00001570
           02 MPAGEV    PIC X.                                          00001580
           02 MPAGEO    PIC X(3).                                       00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MWFONCA   PIC X.                                          00001610
           02 MWFONCC   PIC X.                                          00001620
           02 MWFONCP   PIC X.                                          00001630
           02 MWFONCH   PIC X.                                          00001640
           02 MWFONCV   PIC X.                                          00001650
           02 MWFONCO   PIC X(3).                                       00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MNCODICA  PIC X.                                          00001680
           02 MNCODICC  PIC X.                                          00001690
           02 MNCODICP  PIC X.                                          00001700
           02 MNCODICH  PIC X.                                          00001710
           02 MNCODICV  PIC X.                                          00001720
           02 MNCODICO  PIC X(7).                                       00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MLREFA    PIC X.                                          00001750
           02 MLREFC    PIC X.                                          00001760
           02 MLREFP    PIC X.                                          00001770
           02 MLREFH    PIC X.                                          00001780
           02 MLREFV    PIC X.                                          00001790
           02 MLREFO    PIC X(20).                                      00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MDCRECCA  PIC X.                                          00001820
           02 MDCRECCC  PIC X.                                          00001830
           02 MDCRECCP  PIC X.                                          00001840
           02 MDCRECCH  PIC X.                                          00001850
           02 MDCRECCV  PIC X.                                          00001860
           02 MDCRECCO  PIC X(8).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MNFAMA    PIC X.                                          00001890
           02 MNFAMC    PIC X.                                          00001900
           02 MNFAMP    PIC X.                                          00001910
           02 MNFAMH    PIC X.                                          00001920
           02 MNFAMV    PIC X.                                          00001930
           02 MNFAMO    PIC X(5).                                       00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MLFAMA    PIC X.                                          00001960
           02 MLFAMC    PIC X.                                          00001970
           02 MLFAMP    PIC X.                                          00001980
           02 MLFAMH    PIC X.                                          00001990
           02 MLFAMV    PIC X.                                          00002000
           02 MLFAMO    PIC X(20).                                      00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MDRECEPA  PIC X.                                          00002030
           02 MDRECEPC  PIC X.                                          00002040
           02 MDRECEPP  PIC X.                                          00002050
           02 MDRECEPH  PIC X.                                          00002060
           02 MDRECEPV  PIC X.                                          00002070
           02 MDRECEPO  PIC X(8).                                       00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MNMARQA   PIC X.                                          00002100
           02 MNMARQC   PIC X.                                          00002110
           02 MNMARQP   PIC X.                                          00002120
           02 MNMARQH   PIC X.                                          00002130
           02 MNMARQV   PIC X.                                          00002140
           02 MNMARQO   PIC X(5).                                       00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MLMARQA   PIC X.                                          00002170
           02 MLMARQC   PIC X.                                          00002180
           02 MLMARQP   PIC X.                                          00002190
           02 MLMARQH   PIC X.                                          00002200
           02 MLMARQV   PIC X.                                          00002210
           02 MLMARQO   PIC X(20).                                      00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MDMAJSTA  PIC X.                                          00002240
           02 MDMAJSTC  PIC X.                                          00002250
           02 MDMAJSTP  PIC X.                                          00002260
           02 MDMAJSTH  PIC X.                                          00002270
           02 MDMAJSTV  PIC X.                                          00002280
           02 MDMAJSTO  PIC X(8).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MLRESUMA  PIC X.                                          00002310
           02 MLRESUMC  PIC X.                                          00002320
           02 MLRESUMP  PIC X.                                          00002330
           02 MLRESUMH  PIC X.                                          00002340
           02 MLRESUMV  PIC X.                                          00002350
           02 MLRESUMO  PIC X(20).                                      00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MLREFCA   PIC X.                                          00002380
           02 MLREFCC   PIC X.                                          00002390
           02 MLREFCP   PIC X.                                          00002400
           02 MLREFCH   PIC X.                                          00002410
           02 MLREFCV   PIC X.                                          00002420
           02 MLREFCO   PIC X(50).                                      00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MSTORREPA      PIC X.                                     00002450
           02 MSTORREPC PIC X.                                          00002460
           02 MSTORREPP PIC X.                                          00002470
           02 MSTORREPH PIC X.                                          00002480
           02 MSTORREPV PIC X.                                          00002490
           02 MSTORREPO      PIC X.                                     00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MLCOMPTA  PIC X.                                          00002520
           02 MLCOMPTC  PIC X.                                          00002530
           02 MLCOMPTP  PIC X.                                          00002540
           02 MLCOMPTH  PIC X.                                          00002550
           02 MLCOMPTV  PIC X.                                          00002560
           02 MLCOMPTO  PIC X(3).                                       00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MTYPEA    PIC X.                                          00002590
           02 MTYPEC    PIC X.                                          00002600
           02 MTYPEP    PIC X.                                          00002610
           02 MTYPEH    PIC X.                                          00002620
           02 MTYPEV    PIC X.                                          00002630
           02 MTYPEO    PIC X(2).                                       00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MWSENVTA  PIC X.                                          00002660
           02 MWSENVTC  PIC X.                                          00002670
           02 MWSENVTP  PIC X.                                          00002680
           02 MWSENVTH  PIC X.                                          00002690
           02 MWSENVTV  PIC X.                                          00002700
           02 MWSENVTO  PIC X.                                          00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MSRECOUVA      PIC X.                                     00002730
           02 MSRECOUVC PIC X.                                          00002740
           02 MSRECOUVP PIC X.                                          00002750
           02 MSRECOUVH PIC X.                                          00002760
           02 MSRECOUVV PIC X.                                          00002770
           02 MSRECOUVO      PIC X(2).                                  00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MWSAPPRA  PIC X.                                          00002800
           02 MWSAPPRC  PIC X.                                          00002810
           02 MWSAPPRP  PIC X.                                          00002820
           02 MWSAPPRH  PIC X.                                          00002830
           02 MWSAPPRV  PIC X.                                          00002840
           02 MWSAPPRO  PIC X.                                          00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MCAPPROA  PIC X.                                          00002870
           02 MCAPPROC  PIC X.                                          00002880
           02 MCAPPROP  PIC X.                                          00002890
           02 MCAPPROH  PIC X.                                          00002900
           02 MCAPPROV  PIC X.                                          00002910
           02 MCAPPROO  PIC X(5).                                       00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MLAPPROA  PIC X.                                          00002940
           02 MLAPPROC  PIC X.                                          00002950
           02 MLAPPROP  PIC X.                                          00002960
           02 MLAPPROH  PIC X.                                          00002970
           02 MLAPPROV  PIC X.                                          00002980
           02 MLAPPROO  PIC X(20).                                      00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MCEXPOA   PIC X.                                          00003010
           02 MCEXPOC   PIC X.                                          00003020
           02 MCEXPOP   PIC X.                                          00003030
           02 MCEXPOH   PIC X.                                          00003040
           02 MCEXPOV   PIC X.                                          00003050
           02 MCEXPOO   PIC X(5).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MLEXPOA   PIC X.                                          00003080
           02 MLEXPOC   PIC X.                                          00003090
           02 MLEXPOP   PIC X.                                          00003100
           02 MLEXPOH   PIC X.                                          00003110
           02 MLEXPOV   PIC X.                                          00003120
           02 MLEXPOO   PIC X(20).                                      00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MZONCMDA  PIC X.                                          00003150
           02 MZONCMDC  PIC X.                                          00003160
           02 MZONCMDP  PIC X.                                          00003170
           02 MZONCMDH  PIC X.                                          00003180
           02 MZONCMDV  PIC X.                                          00003190
           02 MZONCMDO  PIC X(15).                                      00003200
           02 FILLER    PIC X(2).                                       00003210
           02 MLIBERRA  PIC X.                                          00003220
           02 MLIBERRC  PIC X.                                          00003230
           02 MLIBERRP  PIC X.                                          00003240
           02 MLIBERRH  PIC X.                                          00003250
           02 MLIBERRV  PIC X.                                          00003260
           02 MLIBERRO  PIC X(58).                                      00003270
           02 FILLER    PIC X(2).                                       00003280
           02 MCODTRAA  PIC X.                                          00003290
           02 MCODTRAC  PIC X.                                          00003300
           02 MCODTRAP  PIC X.                                          00003310
           02 MCODTRAH  PIC X.                                          00003320
           02 MCODTRAV  PIC X.                                          00003330
           02 MCODTRAO  PIC X(4).                                       00003340
           02 FILLER    PIC X(2).                                       00003350
           02 MCICSA    PIC X.                                          00003360
           02 MCICSC    PIC X.                                          00003370
           02 MCICSP    PIC X.                                          00003380
           02 MCICSH    PIC X.                                          00003390
           02 MCICSV    PIC X.                                          00003400
           02 MCICSO    PIC X(5).                                       00003410
           02 FILLER    PIC X(2).                                       00003420
           02 MNETNAMA  PIC X.                                          00003430
           02 MNETNAMC  PIC X.                                          00003440
           02 MNETNAMP  PIC X.                                          00003450
           02 MNETNAMH  PIC X.                                          00003460
           02 MNETNAMV  PIC X.                                          00003470
           02 MNETNAMO  PIC X(8).                                       00003480
           02 FILLER    PIC X(2).                                       00003490
           02 MSCREENA  PIC X.                                          00003500
           02 MSCREENC  PIC X.                                          00003510
           02 MSCREENP  PIC X.                                          00003520
           02 MSCREENH  PIC X.                                          00003530
           02 MSCREENV  PIC X.                                          00003540
           02 MSCREENO  PIC X(4).                                       00003550
           02 FILLER    PIC X(2).                                       00003560
           02 MWWERA    PIC X.                                          00003570
           02 MWWERC    PIC X.                                          00003580
           02 MWWERP    PIC X.                                          00003590
           02 MWWERH    PIC X.                                          00003600
           02 MWWERV    PIC X.                                          00003610
           02 MWWERO    PIC X.                                          00003620
                                                                                
