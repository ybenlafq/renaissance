      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE ACCES ZONE D ACCESSIBILITE             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01AA.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01AA.                                                            
      *}                                                                        
           05  ACCES-CTABLEG2    PIC X(15).                                     
           05  ACCES-CTABLEG2-REDEF REDEFINES ACCES-CTABLEG2.                   
               10  ACCES-CACCES          PIC X(01).                             
           05  ACCES-WTABLEG     PIC X(80).                                     
           05  ACCES-WTABLEG-REDEF  REDEFINES ACCES-WTABLEG.                    
               10  ACCES-LACCES          PIC X(20).                             
               10  ACCES-WML             PIC X(01).                             
               10  ACCES-ZAA             PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGA01AA-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGA01AA-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ACCES-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  ACCES-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ACCES-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  ACCES-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
