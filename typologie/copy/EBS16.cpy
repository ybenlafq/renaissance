      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESE03   ESE03                                              00000020
      ***************************************************************** 00000030
       01   EBS16I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLISTL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNLISTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLISTF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNLISTI   PIC X(7).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLISTL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLLISTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLISTF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLLISTI   PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCREATIONL    COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MDCREATIONL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDCREATIONF    PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDCREATIONI    PIC X(10).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHEFPRODL     COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MCHEFPRODL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCHEFPRODF     PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCHEFPRODI     PIC X(5).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCHEFPRODL    COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLCHEFPRODL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLCHEFPRODF    PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLCHEFPRODI    PIC X(20).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPENTL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MCTYPENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTYPENTF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCTYPENTI      PIC X(2).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPENTL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MLTYPENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLTYPENTF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLTYPENTI      PIC X(20).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCRIT1L  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCCRIT1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCCRIT1F  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCCRIT1I  PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCRIT2L  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCCRIT2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCCRIT2F  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCCRIT2I  PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDETAILL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDETAILL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDETAILF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDETAILI  PIC X.                                          00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MPAGEI    PIC X(3).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MPAGEMAXI      PIC X(3).                                  00000650
           02 MTABLISTI OCCURS   10 TIMES .                             00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLFAML  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MLFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLFAMF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MLFAMI  PIC X(20).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLDESCRIPTL  COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MLDESCRIPTL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLDESCRIPTF  PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MLDESCRIPTI  PIC X(20).                                 00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLVDESCRIPTL      COMP PIC S9(4).                       00000750
      *--                                                                       
             03 MLVDESCRIPTL COMP-5 PIC S9(4).                                  
      *}                                                                        
             03 MLVDESCRIPTF      PIC X.                                00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MLVDESCRIPTI      PIC X(20).                            00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MSELI   PIC X.                                          00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNEWL     COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MNEWL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNEWF     PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MNEWI     PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPENEWL      COMP PIC S9(4).                            00000870
      *--                                                                       
           02 MTYPENEWL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPENEWF      PIC X.                                     00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MTYPENEWI      PIC X.                                     00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MZONCMDI  PIC X(15).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLIBERRI  PIC X(58).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCODTRAI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCICSI    PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MNETNAMI  PIC X(8).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MSCREENI  PIC X(4).                                       00001140
      ***************************************************************** 00001150
      * SDF: ESE03   ESE03                                              00001160
      ***************************************************************** 00001170
       01   EBS16O REDEFINES EBS16I.                                    00001180
           02 FILLER    PIC X(12).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTIMJOUA  PIC X.                                          00001280
           02 MTIMJOUC  PIC X.                                          00001290
           02 MTIMJOUP  PIC X.                                          00001300
           02 MTIMJOUH  PIC X.                                          00001310
           02 MTIMJOUV  PIC X.                                          00001320
           02 MTIMJOUO  PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MWFONCA   PIC X.                                          00001350
           02 MWFONCC   PIC X.                                          00001360
           02 MWFONCP   PIC X.                                          00001370
           02 MWFONCH   PIC X.                                          00001380
           02 MWFONCV   PIC X.                                          00001390
           02 MWFONCO   PIC X(3).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MNLISTA   PIC X.                                          00001420
           02 MNLISTC   PIC X.                                          00001430
           02 MNLISTP   PIC X.                                          00001440
           02 MNLISTH   PIC X.                                          00001450
           02 MNLISTV   PIC X.                                          00001460
           02 MNLISTO   PIC X(7).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MLLISTA   PIC X.                                          00001490
           02 MLLISTC   PIC X.                                          00001500
           02 MLLISTP   PIC X.                                          00001510
           02 MLLISTH   PIC X.                                          00001520
           02 MLLISTV   PIC X.                                          00001530
           02 MLLISTO   PIC X(20).                                      00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MDCREATIONA    PIC X.                                     00001560
           02 MDCREATIONC    PIC X.                                     00001570
           02 MDCREATIONP    PIC X.                                     00001580
           02 MDCREATIONH    PIC X.                                     00001590
           02 MDCREATIONV    PIC X.                                     00001600
           02 MDCREATIONO    PIC X(10).                                 00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MCHEFPRODA     PIC X.                                     00001630
           02 MCHEFPRODC     PIC X.                                     00001640
           02 MCHEFPRODP     PIC X.                                     00001650
           02 MCHEFPRODH     PIC X.                                     00001660
           02 MCHEFPRODV     PIC X.                                     00001670
           02 MCHEFPRODO     PIC X(5).                                  00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MLCHEFPRODA    PIC X.                                     00001700
           02 MLCHEFPRODC    PIC X.                                     00001710
           02 MLCHEFPRODP    PIC X.                                     00001720
           02 MLCHEFPRODH    PIC X.                                     00001730
           02 MLCHEFPRODV    PIC X.                                     00001740
           02 MLCHEFPRODO    PIC X(20).                                 00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MCTYPENTA      PIC X.                                     00001770
           02 MCTYPENTC PIC X.                                          00001780
           02 MCTYPENTP PIC X.                                          00001790
           02 MCTYPENTH PIC X.                                          00001800
           02 MCTYPENTV PIC X.                                          00001810
           02 MCTYPENTO      PIC X(2).                                  00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MLTYPENTA      PIC X.                                     00001840
           02 MLTYPENTC PIC X.                                          00001850
           02 MLTYPENTP PIC X.                                          00001860
           02 MLTYPENTH PIC X.                                          00001870
           02 MLTYPENTV PIC X.                                          00001880
           02 MLTYPENTO      PIC X(20).                                 00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MCCRIT1A  PIC X.                                          00001910
           02 MCCRIT1C  PIC X.                                          00001920
           02 MCCRIT1P  PIC X.                                          00001930
           02 MCCRIT1H  PIC X.                                          00001940
           02 MCCRIT1V  PIC X.                                          00001950
           02 MCCRIT1O  PIC X(5).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MCCRIT2A  PIC X.                                          00001980
           02 MCCRIT2C  PIC X.                                          00001990
           02 MCCRIT2P  PIC X.                                          00002000
           02 MCCRIT2H  PIC X.                                          00002010
           02 MCCRIT2V  PIC X.                                          00002020
           02 MCCRIT2O  PIC X(5).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MDETAILA  PIC X.                                          00002050
           02 MDETAILC  PIC X.                                          00002060
           02 MDETAILP  PIC X.                                          00002070
           02 MDETAILH  PIC X.                                          00002080
           02 MDETAILV  PIC X.                                          00002090
           02 MDETAILO  PIC X.                                          00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MPAGEA    PIC X.                                          00002120
           02 MPAGEC    PIC X.                                          00002130
           02 MPAGEP    PIC X.                                          00002140
           02 MPAGEH    PIC X.                                          00002150
           02 MPAGEV    PIC X.                                          00002160
           02 MPAGEO    PIC X(3).                                       00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MPAGEMAXA      PIC X.                                     00002190
           02 MPAGEMAXC PIC X.                                          00002200
           02 MPAGEMAXP PIC X.                                          00002210
           02 MPAGEMAXH PIC X.                                          00002220
           02 MPAGEMAXV PIC X.                                          00002230
           02 MPAGEMAXO      PIC X(3).                                  00002240
           02 MTABLISTO OCCURS   10 TIMES .                             00002250
             03 FILLER       PIC X(2).                                  00002260
             03 MLFAMA  PIC X.                                          00002270
             03 MLFAMC  PIC X.                                          00002280
             03 MLFAMP  PIC X.                                          00002290
             03 MLFAMH  PIC X.                                          00002300
             03 MLFAMV  PIC X.                                          00002310
             03 MLFAMO  PIC X(20).                                      00002320
             03 FILLER       PIC X(2).                                  00002330
             03 MLDESCRIPTA  PIC X.                                     00002340
             03 MLDESCRIPTC  PIC X.                                     00002350
             03 MLDESCRIPTP  PIC X.                                     00002360
             03 MLDESCRIPTH  PIC X.                                     00002370
             03 MLDESCRIPTV  PIC X.                                     00002380
             03 MLDESCRIPTO  PIC X(20).                                 00002390
             03 FILLER       PIC X(2).                                  00002400
             03 MLVDESCRIPTA      PIC X.                                00002410
             03 MLVDESCRIPTC PIC X.                                     00002420
             03 MLVDESCRIPTP PIC X.                                     00002430
             03 MLVDESCRIPTH PIC X.                                     00002440
             03 MLVDESCRIPTV PIC X.                                     00002450
             03 MLVDESCRIPTO      PIC X(20).                            00002460
             03 FILLER       PIC X(2).                                  00002470
             03 MSELA   PIC X.                                          00002480
             03 MSELC   PIC X.                                          00002490
             03 MSELP   PIC X.                                          00002500
             03 MSELH   PIC X.                                          00002510
             03 MSELV   PIC X.                                          00002520
             03 MSELO   PIC X.                                          00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MNEWA     PIC X.                                          00002550
           02 MNEWC     PIC X.                                          00002560
           02 MNEWP     PIC X.                                          00002570
           02 MNEWH     PIC X.                                          00002580
           02 MNEWV     PIC X.                                          00002590
           02 MNEWO     PIC X(5).                                       00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MTYPENEWA      PIC X.                                     00002620
           02 MTYPENEWC PIC X.                                          00002630
           02 MTYPENEWP PIC X.                                          00002640
           02 MTYPENEWH PIC X.                                          00002650
           02 MTYPENEWV PIC X.                                          00002660
           02 MTYPENEWO      PIC X.                                     00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MZONCMDA  PIC X.                                          00002690
           02 MZONCMDC  PIC X.                                          00002700
           02 MZONCMDP  PIC X.                                          00002710
           02 MZONCMDH  PIC X.                                          00002720
           02 MZONCMDV  PIC X.                                          00002730
           02 MZONCMDO  PIC X(15).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MLIBERRA  PIC X.                                          00002760
           02 MLIBERRC  PIC X.                                          00002770
           02 MLIBERRP  PIC X.                                          00002780
           02 MLIBERRH  PIC X.                                          00002790
           02 MLIBERRV  PIC X.                                          00002800
           02 MLIBERRO  PIC X(58).                                      00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MCODTRAA  PIC X.                                          00002830
           02 MCODTRAC  PIC X.                                          00002840
           02 MCODTRAP  PIC X.                                          00002850
           02 MCODTRAH  PIC X.                                          00002860
           02 MCODTRAV  PIC X.                                          00002870
           02 MCODTRAO  PIC X(4).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MCICSA    PIC X.                                          00002900
           02 MCICSC    PIC X.                                          00002910
           02 MCICSP    PIC X.                                          00002920
           02 MCICSH    PIC X.                                          00002930
           02 MCICSV    PIC X.                                          00002940
           02 MCICSO    PIC X(5).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MNETNAMA  PIC X.                                          00002970
           02 MNETNAMC  PIC X.                                          00002980
           02 MNETNAMP  PIC X.                                          00002990
           02 MNETNAMH  PIC X.                                          00003000
           02 MNETNAMV  PIC X.                                          00003010
           02 MNETNAMO  PIC X(8).                                       00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MSCREENA  PIC X.                                          00003040
           02 MSCREENC  PIC X.                                          00003050
           02 MSCREENP  PIC X.                                          00003060
           02 MSCREENH  PIC X.                                          00003070
           02 MSCREENV  PIC X.                                          00003080
           02 MSCREENO  PIC X(4).                                       00003090
                                                                                
