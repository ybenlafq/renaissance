      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PRPAR PARAMETRES TYPE DE PRESTATION    *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPRPAR .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPRPAR .                                                            
      *}                                                                        
           05  PRPAR-CTABLEG2    PIC X(15).                                     
           05  PRPAR-CTABLEG2-REDEF REDEFINES PRPAR-CTABLEG2.                   
               10  PRPAR-CTPREST         PIC X(05).                             
               10  PRPAR-CPARAM          PIC X(05).                             
               10  PRPAR-CLEPARAM        PIC X(05).                             
           05  PRPAR-WTABLEG     PIC X(80).                                     
           05  PRPAR-WTABLEG-REDEF  REDEFINES PRPAR-WTABLEG.                    
               10  PRPAR-LVPARAM         PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPRPAR-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPRPAR-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRPAR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PRPAR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PRPAR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PRPAR-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
