      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
       01  TS-GA60.                                                             
           03   TS-GA60-LONG            PIC  S9(3)    COMP-3  VALUE 88.         
           03   TS-GA60-DONNEES.                                                
                05   TS-GA60-CMAJ       PIC X.                                  
                05   TS-GA60-NCODIC     PIC X(7).                               
                05   TS-GA60-NMESSDEPOT PIC  S9(3)    COMP-3.                   
                05   TS-GA60-LMESSDEPOT PIC  X(78).                             
                                                                                
