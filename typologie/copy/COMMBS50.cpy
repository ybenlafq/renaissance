      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      *                                                                 00000010
       01  Z-COMMAREA-MBS50.                                            00000020
           02 COMM-MBS50-ENTETE-RETOUR.                                 00000030
      *---- BOOLEEN TYPE D'ERREUR                                       00000040
      *    ----         EN SORTIE                                       00000050
              03 COMM-MBS50-TYPERR     PIC 9(01).                       00000060
                 88 MBS50-NO-ERROR       VALUE 0.                       00000070
                 88 MBS50-APPL-ERROR     VALUE 1.                       00000080
                 88 MBS50-DB2-ERROR      VALUE 2.                       00000090
              03 COMM-MBS50-MESSAGE.                                    00000100
                 04 COMM-MBS50-FUNC-SQL   PIC X(08).                    00000110
                 04 COMM-MBS50-TABLE-NAME PIC X(08).                    00000120
                 04 COMM-MBS50-SQLCODE    PIC S9(04).                   00000130
                 04 COMM-MBS50-POSITION   PIC  9(03).                   00000140
      *    ----         EN ENTREE                                       00000150
           02 COMM-MBS50-MODE-CHGT       PIC 9(01).                     00000160
              88 CHGT-DATA-ALL        VALUE 0.                          00000170
              88 CHGT-DATA-GENE       VALUE 1.                          00000180
              88 CHGT-DATA-DESCR      VALUE 2.                          00000190
              88 CHGT-DATA-DOSS       VALUE 3.                          00000200
              88 CHGT-DATA-COMPO      VALUE 4.                          00000210
           02 COMM-MBS50-MODE-CHGT-FAM   PIC 9(01).                     00000220
              88 PAS-CHGT-CFAM        VALUE 0.                          00000230
              88 CHGT-CFAM            VALUE 1.                          00000240
           02 COMM-MBS50-MODE-CHGT-COMPO PIC 9(01).                     00000250
              88 PAS-CHGT-CPROFASS    VALUE 0.                          00000260
              88 CHGT-CPROFASS        VALUE 1.                          00000270
      *    ---- DONNEES EN ENTREE                                       00000280
           02 COMM-MBS50-CODESFONCTION   PIC X(12).                     00000290
           02 COMM-MBS50-DROIT           PIC X(01).                     00000300
           02 COMM-MBS50-DATEJ           PIC X(08).                     00000310
           02 COMM-MBS50-DATEJ1          PIC X(08).                     00000320
           02 COMM-MBS50-WFONC           PIC X(03).                     00000330
           02 COMM-MBS50-CICS            PIC X(05).                     00000340
      *   ---- DONNEES EN ENTREE / SORTIE                               00000350
           02 COMM-MBS50-NCOFFRE         PIC X(07).                     00000360
           02 COMM-MBS50-CFAM            PIC X(05).                     00000370
           02 COMM-MBS50-CPROFASS        PIC X(05).                     00000380
      *    ---- DONNEES EN SORTIE                                       00000390
           02 COMM-MBS50-CTYPENT         PIC X(02).                     00000400
           02 COMM-MBS50-LCOFFRE         PIC X(20).                     00000410
           02 COMM-MBS50-LCFAM           PIC X(20).                     00000420
           02 COMM-MBS50-LPROFASS        PIC X(20).                     00000430
           02 COMM-MBS50-DCREATION       PIC X(08).                     00000440
           02 COMM-MBS50-DMAJ            PIC X(08).                     00000450
           02 COMM-MBS50-CMARQ           PIC X(05).                     00000460
           02 COMM-MBS50-LMARQ           PIC X(20).                     00000470
           02 COMM-MBS50-CTXTVA          PIC X(01).                     00000480
           02 COMM-MBS50-NTXTVA          PIC S999V99.                   00000490
           02 COMM-MBS50-LTXTVA          PIC X(20).                     00000500
           02 COMM-MBS50-CASSORTC        PIC X(05).                     00000510
           02 COMM-MBS50-LASSORTC        PIC X(20).                     00000520
           02 COMM-MBS50-CASSORTF        PIC X(05).                     00000530
           02 COMM-MBS50-LASSORTF        PIC X(20).                     00000540
           02 COMM-MBS50-DATEF           PIC X(08).                     00000550
           02 COMM-MBS50-DATEC           PIC X(08).                     00000560
           02 COMM-MBS50-LREFDARTY       PIC X(20).                     00000570
           02 COMM-MBS50-LCOMMENT        PIC X(50).                     00000580
           02 COMM-MBS50-CHEF            PIC X(05).                     00000590
           02 COMM-MBS50-LCHEF           PIC X(20).                     00000600
           02 COMM-MBS50-CCOMPT          PIC X(03).                     00000610
           02 COMM-MBS50-LCOMPT          PIC X(20).                     00000620
           02 COMM-MBS50-PRIX            PIC S9(7)V99 COMP-3.           00000630
           02 COMM-MBS50-PRIX-F          PIC X.                         00000640
           02 COMM-MBS50-PRIME           PIC S9(7)V99 COMP-3.           00000650
           02 COMM-MBS50-PRIME-F         PIC X.                         00000660
           02 COMM-MBS50-CODPROF         PIC X(10).                     00000670
           02 COMM-MBS50-FAMCLT          PIC X(30).                     00000680
           02 COMM-MBS50-DATA-DESCR.                                    00000690
              03 COMM-MBS50-OBLIG2             PIC X.                   00000700
              03 COMM-MBS50-LIGNE-DESCR     OCCURS 30.                  00000710
                 05 COMM-MBS50-CCDESC          PIC X(05).               00000720
                 05 COMM-MBS50-LCDESC          PIC X(20).               00000730
                 05 COMM-MBS50-CVDESC          PIC X(05).               00000740
                 05 COMM-MBS50-LVDESC          PIC X(20).               00000750
           02 COMM-MBS50-AFF-COMPO.                                     00000760
              03 COMM-MBS50-AFF           OCCURS 30.                    00000770
                 05 COMM-MBS50-BLOCSEQ-F       PIC X(01).               00000780
                 05 COMM-MBS50-WCONTUNI-F      PIC X(01).               00000790
                 05 COMM-MBS50-WCONTFAM-F      PIC X(01).               00000800
                 05 COMM-MBS50-NENTITE2-F      PIC X(01).               00000810
                 05 COMM-MBS50-NSEQENTREF-F    PIC X(01).               00000820
                 05 COMM-MBS50-DATES-F         PIC X(01).               00000830
AL1905     02 COMM-MBS50-WFLAGQTEMULT          PIC X(01).               00000840
AL1905     02 COMM-MBS50-WFLAGREMISE           PIC X(01).               00000850
AL1905     02 COMM-MBS50-WFLAGTICKET           PIC X(01).               00000860
SR99       02 COMM-MBS50-WCLTREQ               PIC X(01).               00000870
SR99       02 COMM-MBS50-WCLIREQ               PIC X(01).               00000880
           02 COMM-MBS50-VAL-COMPO.                                     00000890
              03 COMM-MBS50-OBLIG3             PIC X.                   00000900
              03 COMM-MBS50-VAL          OCCURS 30.                     00000910
                 05 COMM-MBS50-VNSEQENT        PIC X(02).               00000920
                 05 COMM-MBS50-NSEQENTORIG     PIC X(02).               00000930
                 05 COMM-MBS50-CPROFLIEN       PIC X(05).               00000940
                 05 COMM-MBS50-LPROFLIEN       PIC X(34).               00000950
                 05 COMM-MBS50-CTYPENT2        PIC X(02).               00000960
                 05 COMM-MBS50-BLOCSEQ         PIC X(03).               00000970
                 05 COMM-MBS50-NENTITE2        PIC X(07).               00000980
                 05 COMM-MBS50-CONTROLE2       PIC X(07).               00000990
                 05 COMM-MBS50-LENTITE2.                                00001000
                    10 COMM-MBS50-FILLER       PIC X(02).               00001010
                    10 COMM-MBS50-LIB30.                                00001020
                       15 COMM-MBS50-LIB1         PIC X(05).            00001030
                       15 FILLER                  PIC X VALUE SPACES.   00001040
                       15 COMM-MBS50-LIB2         PIC X(05).            00001050
                       15 FILLER                  PIC X VALUE SPACES.   00001060
                       15 COMM-MBS50-LIB3         PIC X(20).            00001070
                 05 COMM-MBS50-NSEQENTREF      PIC X(02).               00001080
                 05 COMM-MBS50-QDELTACLI       PIC S999V99 COMP-3.      00001090
                 05 COMM-MBS50-DATES.                                   00001100
                    10 COMM-MBS50-DDEBUT       PIC X(08).               00001110
                    10 COMM-MBS50-DFIN         PIC X(08).               00001120
           02 COMM-MBS50-DATA-DOSS.                                     00001130
              03 COMM-MBS50-NBRSEQENT          PIC 999.                 00001140
              03 COMM-MBS50-LIGNE-DOSS      OCCURS 30.                  00001150
                 05 COMM-MBS50-NSEQENT         PIC XX.                  00001160
                 05 COMM-MBS50-LIEN            PIC X.                   00001170
                 05 COMM-MBS50-CDOSS           PIC X(05).               00001180
                 05 COMM-MBS50-LDOSS           PIC X(20).               00001190
                 05 COMM-MBS50-NBRENT          PIC 99.                  00001200
                 05 COMM-MBS50-DEFFET          PIC X(08).               00001210
                 05 COMM-MBS50-DFINEFFET       PIC X(08).               00001220
                 05 COMM-MBS50-NSEQDOS         PIC X(03).               00001230
                                                                        00001240
           02 COMM-MBS50-FLAG-AFF.                                      00001250
              03 COMM-MBS50-MLCOFFRE-AFF   PIC X.                       00001260
              03 COMM-MBS50-CMARQ-AFF      PIC X.                       00001270
              03 COMM-MBS50-CFAM-AFF       PIC X.                       00001280
              03 COMM-MBS50-MCPROFASS-AFF  PIC X.                       00001290
              03 COMM-MBS50-MCTXTVA-AFF    PIC X.                       00001300
              03 COMM-MBS50-MCASSOR-AFF    PIC X.                       00001310
              03 COMM-MBS50-MLREFDARTY-AFF PIC X.                       00001320
              03 COMM-MBS50-MLCOMMENT-AFF  PIC X.                       00001330
              03 COMM-MBS50-MCHEF-AFF      PIC X.                       00001340
              03 COMM-MBS50-MCCOMPT-AFF    PIC X.                       00001350
              03 COMM-MBS50-MPRIX-AFF      PIC X.                       00001360
              03 COMM-MBS50-MPRIME-AFF     PIC X.                       00001370
              03 COMM-MBS50-MCODPROF-AFF   PIC X.                       00001380
NV01          03 COMM-MBS50-MQTEMUL-AFF    PIC X.                       00001390
NV01          03 COMM-MBS50-MREMISE-AFF    PIC X.                       00001400
NV01          03 COMM-MBS50-MPRNTTCK-AFF   PIC X.                       00001410
                                                                                
