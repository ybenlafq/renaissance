      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HETRA TYPES DE TRANSPORT GHE           *        
      *----------------------------------------------------------------*        
       01  RVGA01KF.                                                            
           05  HETRA-CTABLEG2    PIC X(15).                                     
           05  HETRA-CTABLEG2-REDEF REDEFINES HETRA-CTABLEG2.                   
               10  HETRA-CTYPTRAN        PIC X(05).                             
           05  HETRA-WTABLEG     PIC X(80).                                     
           05  HETRA-WTABLEG-REDEF  REDEFINES HETRA-WTABLEG.                    
               10  HETRA-WACTIF          PIC X(01).                             
               10  HETRA-LTYPTRAN        PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGA01KF-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HETRA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HETRA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HETRA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HETRA-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
