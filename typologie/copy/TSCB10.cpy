      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * ITSSC                                                                   
      * TS D'AFFICHAGE                                                          
E0265 ******************************************************************        
      * DE02005 29/05/06 CODE INITIAL                                           
      ******************************************************************        
      * CONSTANTES RELATIVES AU TRAITEMENT                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB10-INIT                     PIC S9(4) COMP VALUE +000.           
      *--                                                                       
       77 TC-CB10-INIT                     PIC S9(4) COMP-5 VALUE +000.         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB10-ZONE                     PIC S9(4) COMP VALUE +010.           
      *--                                                                       
       77 TC-CB10-ZONE                     PIC S9(4) COMP-5 VALUE +010.         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB10-MAG                      PIC S9(4) COMP VALUE +100.           
      *--                                                                       
       77 TC-CB10-MAG                      PIC S9(4) COMP-5 VALUE +100.         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB10-GRP                      PIC S9(4) COMP VALUE +100.           
      *--                                                                       
       77 TC-CB10-GRP                      PIC S9(4) COMP-5 VALUE +100.         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 TC-CB10-DGRP                     PIC S9(4) COMP VALUE -100.           
      *--                                                                       
       77 TC-CB10-DGRP                     PIC S9(4) COMP-5 VALUE -100.         
      *}                                                                        
      * TS AFFICHAGE LIENS AVEC TS PP                                           
       01 TS-CB10-DATA.                                                         
           02 TS-CB10-GESTION.                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB10-POS                    PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB10-POS                    PIC S9(4) COMP-5.              
      *}                                                                        
      *    TS PRIX ET PRIMES                                                    
           02 TS-CB10-ENR.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB10-STATUT                 PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB10-STATUT                 PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB10-TYPE                   PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB10-TYPE                   PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TB-CB10-EXC                    PIC S9(4)      COMP.           
      *--                                                                       
               05 TB-CB10-EXC                    PIC S9(4) COMP-5.              
      *}                                                                        
      *        LIEN AVEC ELEMENTS DEPENDANTS                                    
               05 TS-CB10-LIEN.                                                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB10-LIE                PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB10-LIE                PIC S9(4) COMP-5.              
      *}                                                                        
      *        LIEN AVEC TS PP                                                  
               05 TS-CB10-PP-LIEN.                                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB10-LI-POS             PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB10-LI-POS             PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB10-PP-POS             PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB10-PP-POS             PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB10-PP-DEB             PIC S9(4)      COMP.           
      *--                                                                       
                   10 TB-CB10-PP-DEB             PIC S9(4) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            10 TB-CB10-PP-FIN             PIC S9(4)      COMP.           
      *                                                                         
      *--                                                                       
                   10 TB-CB10-PP-FIN             PIC S9(4) COMP-5.              
                                                                                
      *}                                                                        
