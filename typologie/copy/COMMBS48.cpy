      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES VENTES                               *        
      *  TRANSACTION: MBS48                                            *        
      *  TITRE      : COMMAREA DE L'APPLICATION DGL  APELLEE PAR TGV01 *        
      *  LONGUEUR   : 700                                              *        
      *                                                                *        
      ******************************************************************        
      *                                                                 00530000
      *                                                                 00230000
       01  COMM-MBS48-APPLI.                                            00260000
         02 COMM-MBS48-ENTREE.                                          00260000
      * CODE EVENEMENT EN C&C VERSION CONSIGNES                                 
      * SI VIENT DU MBS72 : 'CREATION'                                          
      * SI VIENT DU MEC16 : 'DEPOT CONSIGNE'                                    
      *                OU : 'RETRAIT CLIENT'                                    
      *                OU : 'SORTIE CONSIGNE'                                   
           05 COMM-MBS48-EVENT          PIC X(15).                              
      * IDENTIFIANT VENTE                                                       
           05 COMM-MBS48-NSOCIETE       PIC X(03).                      00320000
           05 COMM-MBS48-NLIEU          PIC X(03).                      00330000
           05 COMM-MBS48-NVENTE         PIC X(07).                      00340000
           05 COMM-MBS48-TYPVTE         PIC X(01).                      00340000
      * LIEU DE MODIFICATION                                                    
           05 COMM-MBS48-NSOCMODIF      PIC X(03).                      00320000
           05 COMM-MBS48-NLIEUMODIF     PIC X(03).                      00330000
      * LIEU DE PAIEMENT                                                        
           05 COMM-MBS48-NSOCP          PIC X(03).                      00320000
           05 COMM-MBS48-NLIEUP         PIC X(03).                      00330000
           05 COMM-MBS48-CIMPRIM        PIC X(10).                      00330000
         02 COMM-MBS48-SORTIE.                                          00260000
           05 COMM-MBS48-CODE-RETOUR    PIC X(01).                      00340000
           05 COMM-MBS48-MESSAGE.                                               
                10 COMM-MBS48-CODRET         PIC X.                             
                   88  COMM-MBS48-OK         VALUE ' '.                         
                   88  COMM-MBS48-ERR-BLQ    VALUE '1'.                         
                   88  COMM-MBS48-ERR-STO    VALUE '2'.                         
                   88  COMM-MBS48-ERR-DELAI  VALUE '3'.                         
BF                 88  COMM-MBS48-ERR-TOPE   VALUE '4'.                         
                10 COMM-MBS48-LIBERR         PIC X(58).                         
           05 COMM-MBS48-PBSTOCK OCCURS 40.                                     
              10 COMM-MBS48-NCODIC         PIC X(07).                   00340000
              10 COMM-MBS48-QSTOCK         PIC 9(05).                   00340000
              10 COMM-MBS48-NLIGNE         PIC X(02).                   00340000
           05 COMM-MBS48-MODIF-CTYPLIEU PIC X(01).                              
              88 COMM-MBS48-ENTREPOT        VALUE '2'.                          
              88 COMM-MBS48-MAGASIN         VALUE '3'.                          
      * DEDUCTION DU CODE EVENEMENT (EVENT)                                     
      *    05 FILLER                    PIC X(109).                     00340000
           05 FILLER                    PIC X(94).                              
                                                                                
