      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: Erx50   Erx50                                              00000020
      ***************************************************************** 00000030
       01   ERX50I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGETOTL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNPAGETOTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNPAGETOTF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGETOTI     PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCIETEI     PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRIXL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MPRIXL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPRIXF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MPRIXI    PIC X(9).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPROL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAPPROF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCAPPROI  PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNLIEUI   PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNZONPRIXL     COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MNZONPRIXL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNZONPRIXF     PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNZONPRIXI     PIC X(2).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCFAMI    PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLFAMI    PIC X(19).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPOL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXPOF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCEXPOI   PIC X.                                          00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNCODICI  PIC X(7).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCMARQI   PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLMARQI   PIC X(19).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMPACL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCOMPACL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCOMPACF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCOMPACI  PIC X(3).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEBL    COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MDDEBL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDDEBF    PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MDDEBI    PIC X(8).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLREFFOURNI    PIC X(20).                                 00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOACTL   COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MCOACTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOACTF   PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCOACTI   PIC X.                                          00000810
           02 LIGNEI OCCURS   12 TIMES .                                00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDTRTRELL    COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MDTRTRELL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDTRTRELF    PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MDTRTRELI    PIC X(6).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNZONPRILL   COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MNZONPRILL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNZONPRILF   PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MNZONPRILI   PIC X(2).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRELEVEL    COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MPRELEVEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MPRELEVEF    PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MPRELEVEI    PIC X(9).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MHCL    COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MHCL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MHCF    PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MHCI    PIC X.                                          00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLENSCONCL   COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MLENSCONCL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLENSCONCF   PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MLENSCONCI   PIC X(15).                                 00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDRELEVEL    COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MDRELEVEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDRELEVEF    PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MDRELEVEI    PIC X(4).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLEMPCONCL   COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MLEMPCONCL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLEMPCONCF   PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MLEMPCONCI   PIC X(15).                                 00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOMMENTL   COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MLCOMMENTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLCOMMENTF   PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MLCOMMENTI   PIC X(17).                                 00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MOAML   COMP PIC S9(4).                                 00001150
      *--                                                                       
             03 MOAML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MOAMF   PIC X.                                          00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MOAMI   PIC X.                                          00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCDMDL      COMP PIC S9(4).                            00001190
      *--                                                                       
           02 MZONCDMDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MZONCDMDF      PIC X.                                     00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MZONCDMDI      PIC X(15).                                 00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MLIBERRI  PIC X(58).                                      00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MCODTRAI  PIC X(4).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MCICSI    PIC X(5).                                       00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MNETNAMI  PIC X(8).                                       00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MSCREENI  PIC X(4).                                       00001420
      ***************************************************************** 00001430
      * SDF: Erx50   Erx50                                              00001440
      ***************************************************************** 00001450
       01   ERX50O REDEFINES ERX50I.                                    00001460
           02 FILLER    PIC X(12).                                      00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MDATJOUA  PIC X.                                          00001490
           02 MDATJOUC  PIC X.                                          00001500
           02 MDATJOUP  PIC X.                                          00001510
           02 MDATJOUH  PIC X.                                          00001520
           02 MDATJOUV  PIC X.                                          00001530
           02 MDATJOUO  PIC X(10).                                      00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MTIMJOUA  PIC X.                                          00001560
           02 MTIMJOUC  PIC X.                                          00001570
           02 MTIMJOUP  PIC X.                                          00001580
           02 MTIMJOUH  PIC X.                                          00001590
           02 MTIMJOUV  PIC X.                                          00001600
           02 MTIMJOUO  PIC X(5).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MNPAGEA   PIC X.                                          00001630
           02 MNPAGEC   PIC X.                                          00001640
           02 MNPAGEP   PIC X.                                          00001650
           02 MNPAGEH   PIC X.                                          00001660
           02 MNPAGEV   PIC X.                                          00001670
           02 MNPAGEO   PIC X(3).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MNPAGETOTA     PIC X.                                     00001700
           02 MNPAGETOTC     PIC X.                                     00001710
           02 MNPAGETOTP     PIC X.                                     00001720
           02 MNPAGETOTH     PIC X.                                     00001730
           02 MNPAGETOTV     PIC X.                                     00001740
           02 MNPAGETOTO     PIC X(3).                                  00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MNSOCIETEA     PIC X.                                     00001770
           02 MNSOCIETEC     PIC X.                                     00001780
           02 MNSOCIETEP     PIC X.                                     00001790
           02 MNSOCIETEH     PIC X.                                     00001800
           02 MNSOCIETEV     PIC X.                                     00001810
           02 MNSOCIETEO     PIC X(3).                                  00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MPRIXA    PIC X.                                          00001840
           02 MPRIXC    PIC X.                                          00001850
           02 MPRIXP    PIC X.                                          00001860
           02 MPRIXH    PIC X.                                          00001870
           02 MPRIXV    PIC X.                                          00001880
           02 MPRIXO    PIC X(9).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MCAPPROA  PIC X.                                          00001910
           02 MCAPPROC  PIC X.                                          00001920
           02 MCAPPROP  PIC X.                                          00001930
           02 MCAPPROH  PIC X.                                          00001940
           02 MCAPPROV  PIC X.                                          00001950
           02 MCAPPROO  PIC X(3).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MNLIEUA   PIC X.                                          00001980
           02 MNLIEUC   PIC X.                                          00001990
           02 MNLIEUP   PIC X.                                          00002000
           02 MNLIEUH   PIC X.                                          00002010
           02 MNLIEUV   PIC X.                                          00002020
           02 MNLIEUO   PIC X(3).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MNZONPRIXA     PIC X.                                     00002050
           02 MNZONPRIXC     PIC X.                                     00002060
           02 MNZONPRIXP     PIC X.                                     00002070
           02 MNZONPRIXH     PIC X.                                     00002080
           02 MNZONPRIXV     PIC X.                                     00002090
           02 MNZONPRIXO     PIC X(2).                                  00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MCFAMA    PIC X.                                          00002120
           02 MCFAMC    PIC X.                                          00002130
           02 MCFAMP    PIC X.                                          00002140
           02 MCFAMH    PIC X.                                          00002150
           02 MCFAMV    PIC X.                                          00002160
           02 MCFAMO    PIC X(5).                                       00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MLFAMA    PIC X.                                          00002190
           02 MLFAMC    PIC X.                                          00002200
           02 MLFAMP    PIC X.                                          00002210
           02 MLFAMH    PIC X.                                          00002220
           02 MLFAMV    PIC X.                                          00002230
           02 MLFAMO    PIC X(19).                                      00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MCEXPOA   PIC X.                                          00002260
           02 MCEXPOC   PIC X.                                          00002270
           02 MCEXPOP   PIC X.                                          00002280
           02 MCEXPOH   PIC X.                                          00002290
           02 MCEXPOV   PIC X.                                          00002300
           02 MCEXPOO   PIC X.                                          00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MNCODICA  PIC X.                                          00002330
           02 MNCODICC  PIC X.                                          00002340
           02 MNCODICP  PIC X.                                          00002350
           02 MNCODICH  PIC X.                                          00002360
           02 MNCODICV  PIC X.                                          00002370
           02 MNCODICO  PIC X(7).                                       00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MCMARQA   PIC X.                                          00002400
           02 MCMARQC   PIC X.                                          00002410
           02 MCMARQP   PIC X.                                          00002420
           02 MCMARQH   PIC X.                                          00002430
           02 MCMARQV   PIC X.                                          00002440
           02 MCMARQO   PIC X(5).                                       00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MLMARQA   PIC X.                                          00002470
           02 MLMARQC   PIC X.                                          00002480
           02 MLMARQP   PIC X.                                          00002490
           02 MLMARQH   PIC X.                                          00002500
           02 MLMARQV   PIC X.                                          00002510
           02 MLMARQO   PIC X(19).                                      00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MCOMPACA  PIC X.                                          00002540
           02 MCOMPACC  PIC X.                                          00002550
           02 MCOMPACP  PIC X.                                          00002560
           02 MCOMPACH  PIC X.                                          00002570
           02 MCOMPACV  PIC X.                                          00002580
           02 MCOMPACO  PIC X(3).                                       00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MDDEBA    PIC X.                                          00002610
           02 MDDEBC    PIC X.                                          00002620
           02 MDDEBP    PIC X.                                          00002630
           02 MDDEBH    PIC X.                                          00002640
           02 MDDEBV    PIC X.                                          00002650
           02 MDDEBO    PIC X(8).                                       00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MLREFFOURNA    PIC X.                                     00002680
           02 MLREFFOURNC    PIC X.                                     00002690
           02 MLREFFOURNP    PIC X.                                     00002700
           02 MLREFFOURNH    PIC X.                                     00002710
           02 MLREFFOURNV    PIC X.                                     00002720
           02 MLREFFOURNO    PIC X(20).                                 00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MCOACTA   PIC X.                                          00002750
           02 MCOACTC   PIC X.                                          00002760
           02 MCOACTP   PIC X.                                          00002770
           02 MCOACTH   PIC X.                                          00002780
           02 MCOACTV   PIC X.                                          00002790
           02 MCOACTO   PIC X.                                          00002800
           02 LIGNEO OCCURS   12 TIMES .                                00002810
             03 FILLER       PIC X(2).                                  00002820
             03 MDTRTRELA    PIC X.                                     00002830
             03 MDTRTRELC    PIC X.                                     00002840
             03 MDTRTRELP    PIC X.                                     00002850
             03 MDTRTRELH    PIC X.                                     00002860
             03 MDTRTRELV    PIC X.                                     00002870
             03 MDTRTRELO    PIC X(6).                                  00002880
             03 FILLER       PIC X(2).                                  00002890
             03 MNZONPRILA   PIC X.                                     00002900
             03 MNZONPRILC   PIC X.                                     00002910
             03 MNZONPRILP   PIC X.                                     00002920
             03 MNZONPRILH   PIC X.                                     00002930
             03 MNZONPRILV   PIC X.                                     00002940
             03 MNZONPRILO   PIC X(2).                                  00002950
             03 FILLER       PIC X(2).                                  00002960
             03 MPRELEVEA    PIC X.                                     00002970
             03 MPRELEVEC    PIC X.                                     00002980
             03 MPRELEVEP    PIC X.                                     00002990
             03 MPRELEVEH    PIC X.                                     00003000
             03 MPRELEVEV    PIC X.                                     00003010
             03 MPRELEVEO    PIC X(9).                                  00003020
             03 FILLER       PIC X(2).                                  00003030
             03 MHCA    PIC X.                                          00003040
             03 MHCC    PIC X.                                          00003050
             03 MHCP    PIC X.                                          00003060
             03 MHCH    PIC X.                                          00003070
             03 MHCV    PIC X.                                          00003080
             03 MHCO    PIC X.                                          00003090
             03 FILLER       PIC X(2).                                  00003100
             03 MLENSCONCA   PIC X.                                     00003110
             03 MLENSCONCC   PIC X.                                     00003120
             03 MLENSCONCP   PIC X.                                     00003130
             03 MLENSCONCH   PIC X.                                     00003140
             03 MLENSCONCV   PIC X.                                     00003150
             03 MLENSCONCO   PIC X(15).                                 00003160
             03 FILLER       PIC X(2).                                  00003170
             03 MDRELEVEA    PIC X.                                     00003180
             03 MDRELEVEC    PIC X.                                     00003190
             03 MDRELEVEP    PIC X.                                     00003200
             03 MDRELEVEH    PIC X.                                     00003210
             03 MDRELEVEV    PIC X.                                     00003220
             03 MDRELEVEO    PIC X(4).                                  00003230
             03 FILLER       PIC X(2).                                  00003240
             03 MLEMPCONCA   PIC X.                                     00003250
             03 MLEMPCONCC   PIC X.                                     00003260
             03 MLEMPCONCP   PIC X.                                     00003270
             03 MLEMPCONCH   PIC X.                                     00003280
             03 MLEMPCONCV   PIC X.                                     00003290
             03 MLEMPCONCO   PIC X(15).                                 00003300
             03 FILLER       PIC X(2).                                  00003310
             03 MLCOMMENTA   PIC X.                                     00003320
             03 MLCOMMENTC   PIC X.                                     00003330
             03 MLCOMMENTP   PIC X.                                     00003340
             03 MLCOMMENTH   PIC X.                                     00003350
             03 MLCOMMENTV   PIC X.                                     00003360
             03 MLCOMMENTO   PIC X(17).                                 00003370
             03 FILLER       PIC X(2).                                  00003380
             03 MOAMA   PIC X.                                          00003390
             03 MOAMC   PIC X.                                          00003400
             03 MOAMP   PIC X.                                          00003410
             03 MOAMH   PIC X.                                          00003420
             03 MOAMV   PIC X.                                          00003430
             03 MOAMO   PIC X.                                          00003440
           02 FILLER    PIC X(2).                                       00003450
           02 MZONCDMDA      PIC X.                                     00003460
           02 MZONCDMDC PIC X.                                          00003470
           02 MZONCDMDP PIC X.                                          00003480
           02 MZONCDMDH PIC X.                                          00003490
           02 MZONCDMDV PIC X.                                          00003500
           02 MZONCDMDO      PIC X(15).                                 00003510
           02 FILLER    PIC X(2).                                       00003520
           02 MLIBERRA  PIC X.                                          00003530
           02 MLIBERRC  PIC X.                                          00003540
           02 MLIBERRP  PIC X.                                          00003550
           02 MLIBERRH  PIC X.                                          00003560
           02 MLIBERRV  PIC X.                                          00003570
           02 MLIBERRO  PIC X(58).                                      00003580
           02 FILLER    PIC X(2).                                       00003590
           02 MCODTRAA  PIC X.                                          00003600
           02 MCODTRAC  PIC X.                                          00003610
           02 MCODTRAP  PIC X.                                          00003620
           02 MCODTRAH  PIC X.                                          00003630
           02 MCODTRAV  PIC X.                                          00003640
           02 MCODTRAO  PIC X(4).                                       00003650
           02 FILLER    PIC X(2).                                       00003660
           02 MCICSA    PIC X.                                          00003670
           02 MCICSC    PIC X.                                          00003680
           02 MCICSP    PIC X.                                          00003690
           02 MCICSH    PIC X.                                          00003700
           02 MCICSV    PIC X.                                          00003710
           02 MCICSO    PIC X(5).                                       00003720
           02 FILLER    PIC X(2).                                       00003730
           02 MNETNAMA  PIC X.                                          00003740
           02 MNETNAMC  PIC X.                                          00003750
           02 MNETNAMP  PIC X.                                          00003760
           02 MNETNAMH  PIC X.                                          00003770
           02 MNETNAMV  PIC X.                                          00003780
           02 MNETNAMO  PIC X(8).                                       00003790
           02 FILLER    PIC X(2).                                       00003800
           02 MSCREENA  PIC X.                                          00003810
           02 MSCREENC  PIC X.                                          00003820
           02 MSCREENP  PIC X.                                          00003830
           02 MSCREENH  PIC X.                                          00003840
           02 MSCREENV  PIC X.                                          00003850
           02 MSCREENO  PIC X(4).                                       00003860
                                                                                
