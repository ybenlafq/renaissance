#!/bin/ksh

export COBCPY=../COPY-P:../COPY:../COPY/DESC:/opt/ibm/db2/V10.5/include/cobol_mf:${CICSPATH}/include
export DB2INCLUDE=${COBCPY}
bndname=$(cat /tmp/bndname)
cp $1 ${1%.cob}.sqb
db2 connect to ${MT_DB_LOGIN}
db2 prep ${1%.cob}.sqb output $2 bindfile using ${1%.*}.bnd package using ${1%.*} action replace qualifier M907 deferred_prepare no degree 1 disconnect explicit dynamicrules run explain no isolation cs sqlrules db2 validate bind version ref call_resolution deferred datetime iso
db2 terminate
cp ${1%.*}.bnd $bndname
