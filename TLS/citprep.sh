#!/bin/ksh

flag1=$(awk '/ENVIRONMENT *DIVISION/ {print "true"}' $1)
flag2=$(awk '/CONFIGURATION *SECTION/ {print "true"}' $1)

if [[ $flag1 != "true" ]]
then
   awk '/DATA *DIVISION/ { 
                       print "       ENVIRONMENT DIVISION."
                       print "       CONFIGURATION SECTION."
                       print "       SOURCE-COMPUTER. UNIX-MF."
                       print "       OBJECT-COMPUTER."
                       print "            PROGRAM COLLATING SEQUENCE IS EBCDIC1."
                       print "       SPECIAL-NAMES."
                       print "            ALPHABET EBCDIC1 IS EBCDIC"
                       print "            DECIMAL-POINT IS COMMA."
                       print
                       next
       }
       {print}
' $1 
elif [[ $flag1 == "true" && $flag2 != "true" ]]
then
awk '/ENVIRONMENT *DIVISION/ {
                       print
                       print "       CONFIGURATION SECTION."
                       print "       SOURCE-COMPUTER. UNIX-MF."
                       print "       OBJECT-COMPUTER."
                       print "            PROGRAM COLLATING SEQUENCE IS EBCDIC1."
                       print "       SPECIAL-NAMES."
                       print "            ALPHABET EBCDIC1 IS EBCDIC"
                       print "            DECIMAL-POINT IS COMMA."
                       next
     }
     {print}
' $1
elif [[ $flag1 == "true" && $flag2 == "true" ]]
then
awk '/CONFIGURATION *SECTION/,/^...... [^C].*SECTION/ {
                       if ( $0 ~ /CONFIGURATION *SECTION/ ) {
                          print
                          print "       SOURCE-COMPUTER. UNIX-MF."
                          print "       OBJECT-COMPUTER."
                          print "            PROGRAM COLLATING SEQUENCE IS EBCDIC1."
                          print "       SPECIAL-NAMES."
                          print "            ALPHABET EBCDIC1 IS EBCDIC"
                          print "            DECIMAL-POINT IS COMMA." 
                       }   
                       if ( $0 ~ /^......\*/ ) print
                       if ( $0 ~ /^...... [^C].*SECTION/ ) print
                       if ( $0 ~ /IS *SAUT/ ) print
                       if ( $0 ~ /DATA *DIVISION/ ) print
                       next
   }
   {print}
' $1
fi
