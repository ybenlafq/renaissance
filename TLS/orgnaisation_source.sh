#!/bin/zsh


# la liste des domaines
domaines=(finance logistique typologie ventes achats service technique autres)
# dossier cible de reorganisation
cible=${1:-"reorganisation"}

#emplacement de la source
BATCH="target/CBL/BATCH"
CICS="target/CBL/CICS"
COPY="target/CBL/COPY"
BMS="target/BMS"
JOB="target/KSH"

# création des dossiers cibles
#echo "création des répertoires"
for dom in $domaines; do
  mkdir -p $cible/$dom/batch
  mkdir -p $cible/$dom/cics
  mkdir -p $cible/$dom/bms
  mkdir -p $cible/$dom/job
  mkdir -p $cible/$dom/copy
  mkdir -p $cible/$dom/copy/desc
done

# organisation des bms
echo "organisation des ecrans bms"
ls $BMS | while read i; do
  radical=${i:1:2}
  dom=$(awk '/^'$radical' / {print $2}' /tmp/affectations_radicaux_v2.csv | head -1)
  if [[ $dom == "" ]]; then
    dom="autres"
  fi
  echo "== cp $BMS/$i $cible/$dom/bms/$i =="
  cp $BMS/$i $cible/$dom/bms/$i
done


# organisation des job
echo "organisation des JOB"
ls $JOB | while read i; do
  radical=$(echo $i | sed 's:.*\([A-Z][A-Z]\)[0-9].*:\1:')
  if [[ $radical =~ ".ksh" ]]; then
    dom="autres"
  else
    dom=$(awk '/^'$radical' / {print $2}' /tmp/affectations_radicaux_v2.csv | head -1)
  fi
  if [[ $dom == "" ]]; then
    dom="autres"
  fi
  echo "== cp $JOB/$i $cible/$dom/job/$i =="
  cp $JOB/$i $cible/$dom/job/$i
done


# organisation des batch
ls $BATCH | while read i; do
  if [[ $i =~ "^[^BMT]" ]]; then
    dom="autres"
  else
    radical=${i:1:2}
    dom=$(awk '/^'$radical' / {print $2}' /tmp/affectations_radicaux_v2.csv | head -1)
  fi
  if [[ $dom == "" ]]; then
    dom="autres"
  fi
  echo "== cp $BATCH/$i $cible/$dom/batch/$i =="
  cp $BATCH/$i $cible/$dom/batch/$i
done

# organisation des cics
ls $CICS | while read i; do
  if [[ $i =~ "^[^BMT]" ]]; then
    dom="autres"
  else
    radical=${i:1:2}
    dom=$(awk '/^'$radical' / {print $2}' /tmp/affectations_radicaux_v2.csv | head -1)
  fi
  if [[ $dom == "" ]]; then
    dom="autres"
  fi
  echo "== cp $CICS/$i $cible/$dom/cics/$i =="
  cp $CICS/$i $cible/$dom/cics/$i
done


# organisation des copy
find $COPY -name "*.cpy"| sed 's:'$COPY'/::g' | while read i; do
 if [[ $i =~ "^(COMM|WORK)" || $i =~ "^DESC/(COMM|WORK)" ]]; then
   radical=${i:4:2}
   dom=$(awk '/^'$radical' / {print $2}' /tmp/affectations_radicaux_v2.csv | head -1)
 elif [[ $i =~ "^(TS|SW|PR|RV)" ||  $i =~ "^DESC/(TS|SW|PR|RV)" ]]; then
   radical=${i:2:2}
   dom=$(awk '/^'$radical' / {print $2}' /tmp/affectations_radicaux_v2.csv | head -1)
 elif [[ $i =~ "^[EFPiIJ]" ||$i =~ "^DESC/[EFPiIJ]" ]]; then
   radical=${i:1:2}
   dom=$(awk '/^'$radical' / {print $2}' /tmp/affectations_radicaux_v2.csv | head -1)
 elif [[ $i =~ "^MES" || $i =~ "^DESC/MES" ]]; then
   radical=${i:3:2}
   dom=$(awk '/^'$radical' / {print $2}' /tmp/affectations_radicaux_v2.csv | head -1)
 elif [[ $i =~ "^(D|SY|TB|ZW)" || $i =~ "^DESC/(D|SY|TB|ZW)" ]]; then
   dom="technique"
 elif [[ $i =~ "^L"  || $i =~ "^DESC/L" ]]; then
   dom="logistique"
 else
   dom="autres"
 fi
 if [[ $dom == "" ]]; then
   dom="autres"
 fi
 echo "== cp $COPY/$i $cible/$dom/copy/$i =="
 j=$(echo $i | sed 's:DESC/:desc/:')
 cp $COPY/$i $cible/$dom/copy/$j
done
