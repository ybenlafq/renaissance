#!/bin/ksh

pgm=$1
rm -rf ../COPY-P
mkdir -p ../COPY-P
if [[ ! -f $pgm.save ]] 
then 
   cp $pgm $pgm.save
fi
CobRc :Script=${PROJECT}/target/TLS/AddDeclareSql.rcs :ExpandIncludes=True :IncludeSearchPath="../COPY"  :IncludeExtension=".cpy" $pgm
find ${PROJECT}/target/CBL -name "*.p" | while read i
do
  awk '/EXEC SQL BEGIN/,/END.*END-EXE/ {
 if (((($0 ~ /[A-Z][a-z]*.* PIC  *9/ || $0 ~ /[A-Z][a-z]*.* PIC  *Z/ )  && $0 !~ / COMP/  && $0 !~ /\*/) || ($0 ~ /PIC S9\(08\) COMP\-5/)) || ($0 ~ /PIC  *9.*COMP-3/ ) || ($0 ~ /PIC  *\-999999/) || ($0 ~ /PIC  *99999/ ) || ($0 ~ /PIC  *\-Z/) || ($0 ~ /PIC.*\-9/) || ($0 ~ /PIC  *S9\(1\)  *COMP\-5/) || ($0 ~ /PIC  *S9\(05\) COMP-5/ ) || ($0 ~ /PIC  *S9\(07\) COMP-5/ ) || ($0 ~ /PIC  *S9\(7\) COMP-5/ ) || ($0 ~ /X\(400\)/ ) ) {

               print "        EXEC SQL END DECLARE SECTION END-EXEC."
               print $0
               print "        EXEC SQL BEGIN DECLARE SECTION END-EXEC."
               next 
           }
           if ($0 ~ / REDEFINES / && $0 !~ / PIC/ && $0 !~ /\*/) {
                  var = $0 ;  getline
                  if ((($0 ~ /^  *PIC  *9/ ) && ($0 !~ / COMP/ )) || ($0 ~ /^  *PIC  *S9/ ) && ($0 !~ / COMP/ )) {
                           print "        EXEC SQL END DECLARE SECTION END-EXEC."
                           print var
                           print $0
                           print "        EXEC SQL BEGIN DECLARE SECTION END-EXEC."
                           next 
                  }
                  else {
                           print var 
                  }
           }
       }
       {print}
      ' $i > $i.awk
      mv $i.awk $i
      j=$(echo $i | sed s':COPY:COPY-P:'| sed s':\.p$::')
      mv $i $j
done

if [[ -f ${pgm}.p ]]
then
   mv ${pgm}.p $pgm
fi
