#!/usr/bin/perl

use File::Basename;

if ( -f "envmake" )
{
	open(X1, "envmake");
	while ( <X1> ) 
	{
		if ( /^COBCPY/ ) 
		{ 
			($ENV{COBCPY}) = $_ =~ /COBCPY\=([^\n ]+)/g; 
                        $ENV{COBCPY} =~ s/\$\{DB2DIR\}/$ENV{DB2DIR}/g;
                        $ENV{COBCPY} =~ s/\$\{CICSPATH\}/$ENV{CICSPATH}/g;
			last;
		}
	}
	close(X1);
}

( @t ) = $ENV{COBCPY} =~ /([^:\n]+)/g; $ind=@t;

($NOM) = basename($ARGV[0]) =~ /(.*)\.[a-zA-Z]*/g;
open(W1, "| sort -u >$NOM.deps.tmp");

&copie("$ARGV[0]");

close( W1 );

open(X1, "$NOM.deps.tmp"); open(W2, ">$NOM.deps");

print W2 "$ARGV[0]\n";

while ( <X1> ) { if ( $. < 961 ) { print W2; } else { last; } }
close(W2); close(X1); unlink("$NOM.deps.tmp"); 
system("chmod 755 $NOM.deps");

sub verif 
{
	my $flag=0; my $subfile=$_[0]; #$subfile = lc($subfile);
	my $resul=""; my $name_copy="";
	for ( my $cont = 0; $cont < $ind; ++$cont ) {
		if ( -e "$t[$cont]/$subfile.cpy" || -l "$t[$cont]/$subfile.cpy" ) 
		{
			$resul=$t[$cont]."/".$subfile.".cpy";
			$flag=1; $name_copy=$subfile.".cpy"; print W1 "$resul\n";
			last; 
		} 
		elsif ( -e "$t[$cont]/$subfile.CPY" || -l "$t[$cont]/$subfile.CPY" ) 
		{
			$resul=$t[$cont]."/".$subfile.".CPY";
			$flag=1; $name_copy=$subfile.".CPY"; print W1 "$resul\n";
			last; 
		} 
		elsif ( -e "$t[$cont]/$subfile.hst" || -l "$t[$cont]/$subfile.hst" ) 
		{
			$resul=$t[$cont]."/".$subfile.".hst";
			$flag=1; $name_copy=$subfile.".hst"; print W1 "$resul\n";
			last; 
		} 
                elsif ( -e "$t[$cont]/$subfile" || -l "$t[$cont]/$subfile" )
                {
                        $resul=$t[$cont]."/".$subfile;
                        $flag=1; $name_copy=$subfile; print W1 "$resul\n";
                        last;
                }
		else { next; }
	}
	if ( !$flag ) { print STDERR "$subfile.cpy copie Not Found in $ARGV[0] .....\n"; } else { return $resul; }
}

sub copie
{
	local (*E1); 
	my $file=$_[0];
#	print STDERR "Load file < $file >\n";

	open(E1, "$file");
	while ( my $enr = <E1> )
	{
		if ( $enr =~ /^\*? *\*/ || $enr =~ /^......\*/ || $enr =~ / VALUE /i ) { next; }
		elsif ( $enr =~ / copy /i )	
		{
			my ($x) = $enr =~ / copy +\"?([^\"\. \n]+)/ig;
			#print STDERR "Copie found $x in $enr\n";
			&copie(&verif($x));
		}
		elsif ( $enr =~ /EXEC +SQL +INCLUDE +/i )
		{
			my ($x) = $enr =~ /EXEC +SQL +INCLUDE +\"?([^\"\. \n]+)/ig;
			if ( $x =~ /SQLCA/i ) { next; }
			&copie(&verif($x));
		}	
                elsif ( $enr =~ / +INCLUDE +/i )
                {
                        #print STDERR "found";
                        my ($x) = $enr =~ / +INCLUDE +\"?([^\"\. \n]+)/ig;
                        if ( $x =~ /SQLCA/i ) { next; }
                        &copie(&verif($x));
                }
	}
	close(E1);
}
