#!/bin/ksh



pilote=$1


if [[ ! -f ${pilote} ]]
then
   echo "fichier ${pilote} not found"
   exit
fi




cat ${pilote} | while read pgm
do
  if [[ -f $pgm ]]
  then
     dir=$(dirname $i)
     bas=$(basename $i)
     cd $dir
     if [[ ! -f makefile ]]
     then
        buildmake.sh
     fi
     mak=$(egrep -l ${bas%.*}.so makefile*)
     make -f $mak ${bas%.*}.so
     cd -
  fi
done
