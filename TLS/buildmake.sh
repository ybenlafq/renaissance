#! /bin/ksh

rm -f makefile[0-9]*
fic="makefile"
i=1
j=0

print "all:\n	-\$(MAKE) -j4 -kf ${PWD}/makefile${i}" >makefile
CLEAN="\nclean:\n\t-\$(MAKE) -j4 -skf \$(PWD)/makefile${i} clean\n"

printf 'list=\\\n' >>${fic}$i
for f in *.cbl *.pco ; do
    if test -f $f; then
       if [[ $j -eq 300 ]] ; then
          printf "\n\nMAKENUM=$i\n\ninclude \${MAK}/compile.mk\n" >>${fic}$i
          make -f ${fic}$i depend
          let i+=1
          print "	-\$(MAKE) -j4 -kf ${PWD}/makefile${i}" >>makefile
          CLEAN="${CLEAN}\t-\$(MAKE) -j4 -skf \$(PWD)/makefile${i} clean\n"
          j=0
          printf 'list=\\\n' >>${fic}$i
       fi
       let j+=1
       printf ' %s\\\n' $f >>${fic}$i
    fi
done

printf "${CLEAN}" >>makefile

printf "\n\nMAKENUM=$i\n\ninclude \${MAK}/compile.mk\n" >>${fic}$i
make -f ${fic}$i depend

chmod 644 ${fic}$i
