#SHELL += -x

.SUFFIXES: .cbl .so .gnt .deps .pco .cob

include envmake

VPATH=$(SRC):$(DEST)

.cbl.deps:
	-@echo "deps.pl $< suite modif $?"
	deps.pl $<
.pco.deps:
	-@echo "deps.pl $< suite modif $?"
	deps.pl $<

.cbl.so: $$*.deps
	-@echo "Compilation de $< suite modif $?"
	#citprep.sh $< > $<
ifeq ($(ISCICS),TRUE)
	echo "Precompilation cics de $<"
	export COBCPY=$(COBCPY) ;\
	cobc -g -fthread-safe -fnotrunc -conf=${MAK}/cit.cfg -t $*.lst -w -m -preprocess=citprocics.sh $< 2>&1 | tee $<.err
else
	export COBCPY=$(COBCPY) ;\
	cobc -fixed -g -fthread-safe -fnotrunc -conf=${MAK}/cit.cfg -t $*.lst -w -m $< 2>&1 | tee $<.err
endif
	@if test -f $*.so ; \
	then \
	(gzip --best $*.lst;rm -f $*.int $*.idy $*.o $*.ccp $<.err; mv $*.so $(DEST); mv $*.lst.gz $(LST); echo "Compilation <OK> de $<" ) ;\
	else \
	echo "Erreur de compilation de $<" ;\
	fi

.pco.so: $$*.deps
	-@echo "Compilation de $< suite modif $?"
	beginend.sh  $<
	#citprep.sh $< > $<
ifeq ($(ISCICS),TRUE)
	echo "Precompilation cics de $<"
	export DB2INCLUDE=$(DB2INCLUDE) ;\
	export COBCPY=$(COBCPY) ;\
	echo "$*.bnd" > /tmp/bndname ;\
	cobc -g -fthread-safe -fnotrunc ${TRACE} -sysout=SYSOUT,L,1,120  -conf=${MAK}/cit.cfg -L/opt/mqm/lib -lmqmcb_r -I/opt/mqm/inc -t $*.lst -w -m -preprocess=citproall.sh $< 2>&1 | tee $<.err
	rm /tmp/bndname
else
	export DB2INCLUDE=$(DB2INCLUDE) ;\
 	export COBCPY=$(COBCPY) ;\
	echo "$*.bnd" > /tmp/bndname ;\
	cobc -g -fixed -fthread-safe -fnotrunc  ${TRACE} -sysout=SYSOUT,L,1,120 -conf=${MAK}/cit.cfg -L/opt/mqm/lib -lmqmcb_r -I/opt/mqm/inc -t $*.lst -w -m -preprocess=citprodb2.sh $< 2>&1 | tee $<.err
	rm /tmp/bndname
endif
	@if test -f $*.so ; \
	then \
	(gzip --best $*.lst;rm -f $*.int $*.idy $*.o $*.cbl $*.ccp $<.err ; mv $*.so $(DEST); mv $*.lst.gz $(LST); echo "Compilation <OK> de $<" ; db2 connect to ${MT_DB_LOGIN} ; db2 bind $*.bnd ; db2 terminate) ;\
	else \
	echo "Erreur de compilation de $<" ;\
	fi
	
all: compile

create:
	-@(test ! -d $(DEST) && mkdir $(DEST) ; echo "Create directory $(DEST)")
	(test ! -d $(LST) && mkdir $(LST) ; echo "Create directory $(LST)")

compile: create ${list:.cbl=.deps} ${list:.pco=.deps} ${list:.cbl=.so} ${list:.pco=.so}

deps: ${list:.cbl=.deps} ${list:.pco=.deps}

make_depend:
	-@echo "make depend makefile$(MAKENUM)"
	-@makedepend.pl -o so -f makefile$(MAKENUM) ${list}

depend: deps make_depend

clean:
	$(shell find . -name "*.deps" -o -name  "*.err" -o -name "*.bnd" -o -name "*.CBL" | xargs rm -f)
	$(shell find . -name "*.pco" | sed 's:\.pco:.cbl:' | xargs rm -f )
