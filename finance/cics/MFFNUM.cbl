      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 20/09/2016 1        
      *{     Tr-Collating-Sequence-EBCDIC 1.0                                   
      * OBJECT-COMPUTER.                                                        
      *    PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
      *--                                                                       
       IDENTIFICATION DIVISION.                                         00000020
       PROGRAM-ID.                     MFFNUM.                          00000030
       AUTHOR. PROJET-DSA0-MAURICE-DUPREY-CEAFI.                        00000040
      *-------------------------------------------------------------*   00000050
      *    CONTROLE ET MISE EN FORME D'UNE ZONE DE SAISIE NUMERIQUE *   00000060
      *    OU MISE EN FORMAT D'EDITION D'UNE ZONE NUMERIQUE         *   00000070
      *-------------------------------------------------------------*   00000080
       ENVIRONMENT DIVISION.                                            00000090
       CONFIGURATION SECTION.                                           00000100
      *{ Tr-Collating-Sequence-EBCDIC 1.0                                       
      *SPECIAL-NAMES.                                                   00000110
      *    DECIMAL-POINT IS COMMA.                                      00000120
      *--                                                                       
        OBJECT-COMPUTER.                                                        
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
       DATA DIVISION.                                                   00000130
       WORKING-STORAGE SECTION.                                         00000140
                                                                        00000150
      *----------------------------------------------------------------*00000010
      *         COMMAREA POUR SOUS-PROGRAMME DE DECODAGE               *00000020
      *         ET AFFICHAGE DES ZONES NUMERIQUES                      *00000030
      *----------------------------------------------------------------*00000040
                                                                        00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-FNUM-LONG-COMMAREA PIC S9(4)   COMP VALUE +200.         00000060
      *                                                                         
      *--                                                                       
       01  COMM-FNUM-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +200.                 
                                                                        00000070
      *}                                                                        
       01  Z-COMMAREA-MFFNUM.                                           00000080
         02  FNUM-RECORD.                                               00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FNUM-LGIN           PIC S9(4)       COMP.                00000100
      *--                                                                       
           05  FNUM-LGIN           PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FNUM-LGOUT          PIC S9(4)       COMP.                00000110
      *--                                                                       
           05  FNUM-LGOUT          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FNUM-NBDEC          PIC S9(4)       COMP.                00000120
      *--                                                                       
           05  FNUM-NBDEC          PIC S9(4) COMP-5.                            
      *}                                                                        
           05  FNUM-TYPTRAIT       PIC X.                               00000130
           05  FNUM-CDRET          PIC X.                               00000140
               88  FNUM-OK                         VALUE '0'.           00000150
               88  FNUM-INVCAR                     VALUE '1'.           00000160
               88  FNUM-DBLEDECIM                  VALUE '2'.           00000170
               88  FNUM-TROPLONG                   VALUE '3'.           00000180
               88  FNUM-DBLESIGNE                  VALUE '4'.           00000190
               88  FNUM-TROPDECIM                  VALUE '5'.           00000200
               88  FNUM-PASNEGATIF                 VALUE '6'.           00000210
               88  FNUM-ERRIN                      VALUE 'P'.           00000220
           05  FNUM-TYPDECIMAL     PIC X      VALUE ','.                00000230
           05  FNUM-SIGNE          PIC X      VALUE ' '.                00000240
               88  FNUM-NOTMOINS                   VALUE 'N'.           00000250
           05  FNUM-ZEDIT          PIC X(18).                           00000260
           05  FNUM-ZNUM.                                               00000270
               10  FNUM-ZNUMN          PIC S9(18).                      00000280
           05  FNUM-MESS-ERREUR.                                        00000290
               10  FNUM-CODERREUR      PIC X(4).                        00000300
               10  FILLER              PIC X.                           00000310
               10  FNUM-LIBERREUR      PIC X(55).                       00000320
                                                                        00000330
                                                                        00000170
       01  FNUM-TRAVAIL.                                                00000180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FNUM-I1             PIC S9(4)   VALUE ZERO   COMP.       00000190
      *--                                                                       
           05  FNUM-I1             PIC S9(4)   VALUE ZERO COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FNUM-I2             PIC S9(4)   VALUE ZERO   COMP.       00000200
      *--                                                                       
           05  FNUM-I2             PIC S9(4)   VALUE ZERO COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FNUM-ENTIER         PIC S9(4)   VALUE ZERO   COMP.       00000210
      *--                                                                       
           05  FNUM-ENTIER         PIC S9(4)   VALUE ZERO COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FNUM-DECIM          PIC S9(4)   VALUE ZERO   COMP.       00000220
      *--                                                                       
           05  FNUM-DECIM          PIC S9(4)   VALUE ZERO COMP-5.               
      *}                                                                        
           05  FNUM-WORK           PIC X(18)   VALUE SPACES.            00000230
           05  FNUM-VIRGULE        PIC X       VALUE ','.               00000240
           05  FNUM-POINT          PIC X       VALUE '.'.               00000250
           05  FNUM-DECIMAL-FOUND  PIC X       VALUE 'N'.               00000260
               88  FNUM-DECIM-NON                  VALUE 'N'.           00000270
               88  FNUM-DECIM-OUI                  VALUE 'O'.           00000280
           05  FNUM-SIGNE-FOUND    PIC X       VALUE ' '.               00000290
               88  FNUM-NEUTRE                     VALUE ' '.           00000300
               88  FNUM-NEGATIF                    VALUE '-'.           00000310
               88  FNUM-POSITIF                    VALUE '+'.           00000320
           05  FNUM-ZNEGATIVE.                                          00000330
               10  FNUM-ZNEGATIVN      PIC S9      VALUE ZERO.          00000340
           05  FNUM-NONSIGNE       PIC  9      VALUE ZERO.              00000350
                                                                        00000360
      * -AIDA ********************************************************* 00000370
      *                                                               * 00000380
      *  LLL      IIIIIIII NN    NNN KKK    KKK AAAAAAAAA GGGGGGGGG   * 00000390
      *  LLL      IIIIIIII NNNN  NNN KKK   KKK  AAAAAAAAA GGGGGGGGG   * 00000400
      *  LLL         II    NNNNN NNN KKK  KKK   AA     AA GG          * 00000410
      *  LLL         II    NNN NNNNN KKK KKK    AA     AA GG          * 00000420
      *  LLL         II    NNN  NNNN KKKKKK     AAAAAAAAA GG          * 00000430
      *  LLL         II    NNN   NNN KKKKKK     AAAAAAAAA GG  GGGG    * 00000440
      *  LLL         II    NNN   NNN KKK KKK    AA     AA GG    GG    * 00000450
      *  LLL         II    NNN   NNN KKK  KKK   AA     AA GG    GG    * 00000460
      *  LLLLLLLL IIIIIIII NNN   NNN KKK   KKK  AA     AA GGGGGGGG    * 00000470
      *  LLLLLLLL IIIIIIII NNN   NNN KKK    KK  AA     AA GGGGGGGG    * 00000480
      *                                                               * 00000490
      ***************************************************************** 00000500
      *                                                                 00000510
       LINKAGE SECTION.                                                 00000520
      *                    DFHCOMMAREA                                  00000530
       01  DFHCOMMAREA.                                                 00000540
           05  FILLER PIC X OCCURS 4000 DEPENDING ON EIBCALEN.          00000550
                                                                        00000560
       PROCEDURE DIVISION.                                              00000570
                                                                        00000580
           MOVE DFHCOMMAREA         TO Z-COMMAREA-MFFNUM.               00000590
                                                                        00000600
           SET FNUM-OK              TO TRUE.                            00000610
                                                                        00000620
           IF FNUM-TYPDECIMAL NOT = FNUM-VIRGULE                        00000630
              IF FNUM-TYPDECIMAL = FNUM-POINT                           00000640
                 MOVE FNUM-VIRGULE         TO FNUM-POINT                00000650
                 MOVE FNUM-TYPDECIMAL      TO FNUM-VIRGULE              00000660
              ELSE                                                      00000670
                 SET FNUM-ERRIN            TO TRUE                      00000680
              END-IF                                                    00000690
           END-IF.                                                      00000700
                                                                        00000710
           IF FNUM-LGIN  > 18                                           00000720
           OR FNUM-LGOUT > 18                                           00000730
           OR FNUM-NBDEC > 18                                           00000740
              SET FNUM-ERRIN            TO TRUE                         00000750
           END-IF                                                       00000760
                                                                        00000770
           IF FNUM-TYPTRAIT = '1'                                       00000780
              PERFORM DECODE-NUMERIQUE                                  00000790
           END-IF.                                                      00000800
                                                                        00000810
           IF FNUM-OK                                                   00000820
              PERFORM EDIT-NUMERIQUE                                    00000830
           END-IF.                                                      00000840
                                                                        00000850
           IF NOT FNUM-OK                                               00000860
              PERFORM FNUM-LIB-ERREUR                                   00000870
           END-IF.                                                      00000880
                                                                        00000890
           MOVE Z-COMMAREA-MFFNUM   TO DFHCOMMAREA.                     00000900
                                                                        00000910
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS RETURN END-EXEC.                                   00000920
      *--                                                                       
           EXEC CICS RETURN                                                     
           END-EXEC.                                                            
      *}                                                                        
                                                                        00000930
       DECODE-NUMERIQUE        SECTION.                                 00000940
                                                                        00000950
           SET FNUM-DECIM-NON    TO TRUE.                               00000970
           SET FNUM-NEUTRE       TO TRUE.                               00000980
           MOVE ZERO             TO FNUM-I1.                            00000990
           MOVE ZERO             TO FNUM-I2.                            00001000
           MOVE FNUM-NBDEC       TO FNUM-DECIM.                         00001010
                                                                        00001020
           PERFORM VARYING FNUM-I1 FROM 1 BY 1                          00001030
                   UNTIL FNUM-I1 > FNUM-LGIN                            00001040
                   OR NOT FNUM-OK                                       00001050
              IF FNUM-ZEDIT(FNUM-I1:1) NOT < '0' AND NOT > '9'          00001060
                 PERFORM DECODE-NUM-MVCHIFFRE                           00001070
              ELSE                                                      00001080
                 IF FNUM-ZEDIT(FNUM-I1:1) = '+' OR '-'                  00001090
                    IF FNUM-POSITIF OR FNUM-NEGATIF                     00001100
                       SET FNUM-DBLESIGNE TO TRUE                       00001110
                    ELSE                                                00001120
                       IF FNUM-ZEDIT(FNUM-I1:1) = '+'                   00001130
                          SET FNUM-POSITIF    TO TRUE                   00001140
                       ELSE                                             00001150
                          IF NOT FNUM-NOTMOINS                          00001160
                             SET FNUM-NEGATIF    TO TRUE                00001170
                          ELSE                                          00001180
                             SET FNUM-PASNEGATIF TO TRUE                00001190
                          END-IF                                        00001200
                       END-IF                                           00001210
                    END-IF                                              00001220
                 ELSE                                                   00001230
                    IF FNUM-ZEDIT(FNUM-I1:1) = FNUM-VIRGULE             00001240
                       IF FNUM-DECIM = ZERO                             00001250
                          SET FNUM-TROPLONG   TO TRUE                   00001260
                       ELSE                                             00001270
                          IF FNUM-DECIM-OUI                             00001280
                             SET FNUM-DBLEDECIM  TO TRUE                00001290
                          ELSE                                          00001300
                             SET FNUM-DECIM-OUI  TO TRUE                00001310
                          END-IF                                        00001320
                       END-IF                                           00001330
                    ELSE                                                00001340
                       IF FNUM-ZEDIT(FNUM-I1:1) NOT = FNUM-POINT        00001350
                                              AND NOT = SPACE           00001360
      *{ Tr-Hexa-Map 1.5                                                        
      *                                       AND NOT = X'00'           00001370
      *--                                                                       
                                              AND NOT = X'00'                   
      *}                                                                        
                          SET FNUM-INVCAR     TO TRUE                   00001380
                       END-IF                                           00001390
                    END-IF                                              00001400
                 END-IF                                                 00001410
              END-IF                                                    00001420
           END-PERFORM.                                                 00001430
                                                                        00001440
           IF FNUM-OK                                                   00001450
              PERFORM DECODE-NUM-MVZERO                                 00001460
           END-IF.                                                      00001470
                                                                        00001480
       DECODE-NUM-MVCHIFFRE     SECTION.                                00001490
           IF FNUM-DECIM-OUI                                            00001500
              SUBTRACT 1           FROM FNUM-DECIM                      00001510
              IF FNUM-DECIM < ZERO                                      00001520
                 SET FNUM-TROPDECIM TO TRUE                             00001530
              END-IF                                                    00001540
           END-IF.                                                      00001550
           ADD  1                  TO FNUM-I2.                          00001560
           MOVE FNUM-ZEDIT(FNUM-I1:1) TO FNUM-WORK(FNUM-I2:1).          00001570
           IF (FNUM-I2 + FNUM-DECIM) > FNUM-LGOUT                       00001580
              SET FNUM-TROPLONG    TO TRUE                              00001590
           END-IF.                                                      00001600
                                                                        00001610
       DECODE-NUM-MVZERO        SECTION.                                00001620
                                                                        00001630
           IF FNUM-DECIM > ZERO                                         00001640
              PERFORM VARYING FNUM-DECIM FROM FNUM-DECIM BY -1          00001650
                      UNTIL FNUM-DECIM = ZERO                           00001660
                 ADD  1               TO FNUM-I2                        00001670
                 MOVE '0'             TO FNUM-WORK(FNUM-I2:1)           00001680
              END-PERFORM                                               00001690
           END-IF.                                                      00001700
                                                                        00001710
           PERFORM VARYING FNUM-I1 FROM 18 BY -1                        00001720
                   UNTIL FNUM-I1 = ZERO                                 00001730
              IF FNUM-I2 > ZERO                                         00001740
                 MOVE FNUM-WORK(FNUM-I2:1) TO FNUM-ZNUM(FNUM-I1:1)      00001750
                 SUBTRACT 1         FROM FNUM-I2                        00001760
              ELSE                                                      00001770
                 MOVE '0'             TO FNUM-ZNUM(FNUM-I1:1)           00001780
              END-IF                                                    00001790
           END-PERFORM.                                                 00001800
                                                                        00001810
           IF FNUM-NEGATIF                                              00001820
              COMPUTE FNUM-ZNUMN = FNUM-ZNUMN * -1                      00001830
           END-IF.                                                      00001840
                                                                        00001850
      *--------------------------------------------------------------   00001860
                                                                        00001870
       EDIT-NUMERIQUE           SECTION.                                00001880
                                                                        00001890
           MOVE LOW-VALUE                  TO FNUM-ZEDIT.               00001900
           MOVE ZERO                       TO FNUM-I2.                  00001910
           COMPUTE FNUM-ENTIER = 18 - FNUM-NBDEC.                       00001920
           COMPUTE FNUM-I1     = 18 - FNUM-LGIN + 1.                    00001930
           MOVE FNUM-ZNUM                  TO FNUM-WORK.                        
           MOVE FNUM-WORK(18:1)            TO FNUM-ZNEGATIVE.           00001940
           MOVE FNUM-ZNEGATIVN             TO FNUM-NONSIGNE.            00001940
           MOVE FNUM-NONSIGNE              TO FNUM-WORK(18:1).          00001960
           IF FNUM-ZNUMN < ZERO                                                 
              ADD  1                       TO FNUM-I2                   00001990
              MOVE '-'                     TO FNUM-ZEDIT(FNUM-I2:1)     00002000
           END-IF.                                                      00002020
                                                                        00002030
           PERFORM VARYING FNUM-I1 FROM FNUM-I1 BY 1                    00002040
                   UNTIL FNUM-I1 > FNUM-ENTIER                          00002050
              IF FNUM-WORK (FNUM-I1:1) NOT = '0'                        00002060
                 PERFORM EDIT-NUM-MVCHIFFRE                             00002070
              ELSE                                                      00002080
                 IF FNUM-I2 > ZERO AND                                  00002090
                    FNUM-ZEDIT(FNUM-I2:1) NOT < '0'                     00002100
                    PERFORM EDIT-NUM-MVCHIFFRE                          00002110
                 END-IF                                                 00002120
              END-IF                                                    00002130
           END-PERFORM.                                                 00002140
                                                                        00002150
           IF FNUM-NBDEC > ZERO                                         00002160
              IF FNUM-WORK(FNUM-I1:FNUM-NBDEC) NOT = ALL '0'            00002170
                 IF FNUM-I2 = ZERO                                      00002180
                    ADD  1                    TO FNUM-I2                00002190
                    MOVE '0'                  TO FNUM-ZEDIT(FNUM-I2:1)  00002200
                 END-IF                                                 00002210
              END-IF                                                    00002220
           END-IF.                                                      00002230
                                                                        00002240
           IF FNUM-NBDEC > ZERO                                         00002250
           AND FNUM-WORK(FNUM-I1:FNUM-NBDEC) NOT = ALL '0'              00002260
              ADD  1                    TO FNUM-I2                      00002270
              MOVE FNUM-VIRGULE         TO FNUM-ZEDIT(FNUM-I2:1)        00002280
              PERFORM VARYING FNUM-I1 FROM FNUM-I1 BY 1                 00002290
                      UNTIL FNUM-I1 > 18                                00002300
                 PERFORM EDIT-NUM-MVCHIFFRE                             00002310
              END-PERFORM                                               00002320
           END-IF.                                                      00002330
                                                                        00002340
       EDIT-NUM-MVCHIFFRE       SECTION.                                00002350
           IF FNUM-I2 NOT < FNUM-LGOUT AND                              00002360
              FNUM-TYPTRAIT NOT = '1'                                   00000780
              SET FNUM-TROPLONG      TO TRUE                            00002370
           ELSE                                                         00002380
              ADD  1                     TO FNUM-I2                     00002390
              MOVE FNUM-WORK(FNUM-I1:1) TO FNUM-ZEDIT(FNUM-I2:1)        00002400
           END-IF.                                                      00002410
                                                                        00002420
      *---------------------------------------------------------------  00002430
                                                                        00002440
       FNUM-LIB-ERREUR          SECTION.                                00002450
                                                                        00002460
           MOVE SPACES                    TO FNUM-MESS-ERREUR.          00002470
                                                                        00002480
           STRING 'NUM' FNUM-CDRET DELIMITED BY SIZE                    00002490
                  INTO FNUM-CODERREUR.                                  00002500
                                                                        00002510
           EVALUATE FNUM-CDRET                                          00002520
              WHEN '1'                                                  00002530
                   MOVE 'CARACTERES INVALIDES DANS CETTE ZONE    '      00002540
                                          TO FNUM-LIBERREUR             00002550
              WHEN '2'                                                  00002560
                   MOVE 'CARACTERE DECIMAL EN DOUBLE             '      00002570
                                          TO FNUM-LIBERREUR             00002580
              WHEN '3'                                                  00002590
                   MOVE 'CHIFFRE TROP LONG PAR RAPPORT AU PREVU  '      00002600
                                          TO FNUM-LIBERREUR             00002610
              WHEN '4'                                                  00002620
                   MOVE 'CARACTERE SIGNE SAISI EN DOUBLE         '      00002630
                                          TO FNUM-LIBERREUR             00002640
              WHEN '5'                                                  00002650
                   MOVE 'IL Y A TROP DE DECIMALES SAISIES        '      00002660
                                          TO FNUM-LIBERREUR             00002670
              WHEN '6'                                                  00002680
                   MOVE 'CETTE ZONE NE PEUT PAS ETRE NEGATIVE    '      00002690
                                          TO FNUM-LIBERREUR             00002700
              WHEN 'P'                                                  00002710
                   MOVE 'ERREUR DE PARAMETRAGE  DU SOUS-PROGRAMME'      00002720
                                          TO FNUM-LIBERREUR             00002730
           END-EVALUATE.                                                00002740
                                                                        00002750
