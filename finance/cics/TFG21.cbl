      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 20/09/2016 1        
      *{     Tr-Collating-Sequence-EBCDIC 1.0                                   
      * OBJECT-COMPUTER.                                                        
      *    PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
      *--                                                                       
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.                     TFG21.                                   
       AUTHOR. PROJET DSA0.                                                     
      *               GESTION DES PARAMETRES D'INVENTAIRE                       
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
      *{ Tr-Collating-Sequence-EBCDIC 1.0                                       
      *SPECIAL-NAMES.                                                           
      *    DECIMAL-POINT IS COMMA.                                              
      *--                                                                       
        OBJECT-COMPUTER.                                                        
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
       DATA DIVISION.                                                           
       WORKING-STORAGE SECTION.
      *{Post-translation Correct-Val-pointeur
         77  NULLV PIC S9(8) COMP-5 VALUE -16777216.
         77  NULLP REDEFINES NULLV USAGE IS POINTER.
      *}
                  COPY  SYKWDIV0.                                               
           EJECT                                                                
      *****************************************************************         
      *   DECRIRE   ICI   LES   ZONES   SPECIFIQUES   AU   PROGRAMME  *         
      *****************************************************************         
      *                                                               *         
      *                                                               *         
      *****************************************************************         
      *                                                                         
       77  ZONE-RECH PIC X(14).                                                 
       01  W-NBREXP3.                                                           
           05  W-NBREXP    PIC 9(3).                                            
       01  INIT-NB.                                                             
           05  INIT1     PIC X  VALUE SPACE.                                    
           05  INIT2     PIC XX VALUE LOW-VALUE.                                
       01  ZONE-PARAMETRE.                                                      
           05  ZONE-DATE PIC X(8).                                              
           05  ZONE-SOC  PIC XXX.                                               
           05  ZONE-TYPE PIC XXX.                                               
           05  FILLER    PIC X.                                                 
           05  PARA-ETIQ PIC X.                                                 
           05  PARA-ETAT PIC X.                                                 
           05  PARA-EDIT PIC X.                                                 
           05  PARA-DINV.                                                       
               15  PARA-SINV PIC XXXX.                                          
               15  PARA-MINV PIC XX.                                            
               15  PARA-JINV PIC XX.                                            
           05  FILLER PIC X(54).                                                
      *                                                                         
      * -AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * DESCRIPTION  DES ZONES D'INTERFACE DB2                                  
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
      *   COPY SYKWSQ10.                                                        
          COPY SYKWZCMD.                                                        
      *   COPY ZLIBERRG.                                                        
            EJECT                                                               
      *                                                                         
      * -AIDA *********************************************************         
      *   ZONES GENERALES OBLIGATOIRES                                *         
      *****************************************************************         
      *                                                                         
           COPY  SYKWEIB0.                                                      
           COPY  SYKWDATH.                                                      
           COPY  SYKWCWA0.                                                      
           COPY  SYKWTCTU.                                                      
           COPY  SYKWECRA.                                                      
           EJECT                                                                
      *                                                                         
      * -AIDA *********************************************************         
      *   ZONES DE CONTROLE ET DE TRAITEMENT                          *         
      *****************************************************************         
      *                                                                         
      *    COPY  SYKWAGE0.                                                      
      *    COPY  SYKWDATE.                                                      
      *    COPY  SYKWDECI.                                                      
      *    COPY  SYKWVALN.                                                      
      *    COPY  SYKWTIME.                                                      
      *    COPY  SYKWMONT.                                                      
           EJECT                                                                
      *                                                                         
      * -AIDA *********************************************************         
      *  ZONES DE LA MAP                                                        
      *****************************************************************         
      *                                                                         
       01  FILLER  PIC X(16) VALUE '*** MAP FG21 ***'.                          
           COPY EFG21 REPLACING EFG21I BY Z-MAP.                                
           EJECT                                                                
      *                                                                         
      * -AIDA *********************************************************         
      *  ZONES DE COMMAREA                                                      
      *****************************************************************         
           COPY  COMMFG20.                                                      
           COPY  SYKWCOMM.                                                      
      *    COPY  SYKWSTAR.                                                      
           EJECT                                                                
      *                                                                         
      * -AIDA *********************************************************         
      *  ZONE DE COMMAREA POUR PROGRAMME APPELE PAR LINK                        
      *****************************************************************         
      *                                                                         
       01  FILLER             PIC X(16)  VALUE 'Z-COMMAREA-LINK'.               
       01  Z-COMMAREA-LINK    PIC X(500).                                       
      *                                                                         
           COPY COMMDATC.                                                       
           EJECT                                                                
      *                                                                         
      * -AIDA *********************************************************         
      *  ZONE BUFFER D'ENTREE-SORTIE                                            
      *****************************************************************         
      *                                                                         
           COPY  SYKWZINO.                                                      
           EJECT                                                                
      *                                                                         
      * -AIDA *********************************************************         
      *  ZONE DE GESTION DES ERREURS                                            
      *****************************************************************         
      *                                                                         
           COPY  SYKWERRO.                                                      
           EJECT                                                                
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *         DESCRIPTION DES BASES DE DONNEES RELATIONNELLES                 
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
      *    EXEC SQL INCLUDE SQLCA                                               
      *    END-EXEC.                                                            
      *                                                                         
      *    EXEC SQL INCLUDE RVAN0000                                            
      *    END-EXEC.                                                            
      *    EXEC SQL INCLUDE RVGA9900                                            
      *    END-EXEC.                                                            
           EJECT                                                                
      *                                                                         
      * -AIDA *********************************************************         
      *                                                               *         
      *  LLL      IIIIIIII NN    NNN KKK    KKK AAAAAAAAA GGGGGGGGG   *         
      *  LLL      IIIIIIII NNNN  NNN KKK   KKK  AAAAAAAAA GGGGGGGGG   *         
      *  LLL         II    NNNNN NNN KKK  KKK   AA     AA GG          *         
      *  LLL         II    NNN NNNNN KKK KKK    AA     AA GG          *         
      *  LLL         II    NNN  NNNN KKKKKK     AAAAAAAAA GG          *         
      *  LLL         II    NNN   NNN KKKKKK     AAAAAAAAA GG  GGGG    *         
      *  LLL         II    NNN   NNN KKK KKK    AA     AA GG    GG    *         
      *  LLL         II    NNN   NNN KKK  KKK   AA     AA GG    GG    *         
      *  LLLLLLLL IIIIIIII NNN   NNN KKK   KKK  AA     AA GGGGGGGG    *         
      *  LLLLLLLL IIIIIIII NNN   NNN KKK    KK  AA     AA GGGGGGGG    *         
      *                                                               *         
      *****************************************************************         
      *                                                                         
       LINKAGE SECTION.                                                         
      *                    DFHCOMMAREA                                          
       01  DFHCOMMAREA.                                                         
           05  FILLER PIC X OCCURS 4000 DEPENDING ON EIBCALEN.                  
      *                                                                         
           COPY  SYKLINKA.                                                      
      *                    BLOCK UIB DL/I                                       
           COPY DLIUIB.                                                         
           EJECT                                                                
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *                                                                         
      *****************************************************************         
      *              T R A M E   DU   P R O G R A M M E               *         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *           -----------------------------------------           *         
      *           - MODULE DE BASE DE LA TRANSACTION FG21 -           *         
      *           -----------------------------------------           *         
      *                               I                               *         
      *           -----------------------------------------           *         
      *           I                   I                   I           *         
      *   -----------------   -----------------   -----------------   *         
      *   -    ENTREE     -   -  TRAITEMENT   -   -     SORTIE    -   *         
      *   -----------------   -----------------   -----------------   *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-FG21                    SECTION.                                  
      *                                                                         
           PERFORM MODULE-ENTREE.                                               
      *                                                                         
           IF TRAITEMENT                                                        
              PERFORM MODULE-TRAITEMENT                                         
           END-IF.                                                              
      *                                                                         
           PERFORM MODULE-SORTIE.                                               
      *                                                                         
       FIN-MODULE-FG21.    EXIT.                                                
                    EJECT                                                       
      * -AIDA *********************************************************         
      *                                                               *         
      *  EEEEEEEEE NN     NN TTTTTTTTT RRRRRRRR  EEEEEEEEE EEEEEEEEE  *         
      *  EEEEEEEEE NNN    NN TTTTTTTTT RRRRRRRRR EEEEEEEEE EEEEEEEEE  *         
      *  EEE       NNN    NN    TTT    RRR   RRR EEE       EEE        *         
      *  EEE       NNNN   NN    TTT    RRR   RRR EEE       EEE        *         
      *  EEEEEEEEE NN NN  NN    TTT    RRRRRRRRR EEEEEEEEE EEEEEEEEE  *         
      *  EEEEEEEEE NN  NN NN    TTT    RRRRRRRR  EEEEEEEEE EEEEEEEEE  *         
      *  EEE       NN   NNNN    TTT    RRR RRRR  EEE       EEE        *         
      *  EEE       NN    NNN    TTT    RRR   RR  EEE       EEE        *         
      *  EEEEEEEEE NN    NNN    TTT    RRR    RR EEEEEEEEE EEEEEEEEE  *         
      *  EEEEEEEEE NN     NN    TTT    RRR    RR EEEEEEEEE EEEEEEEEE  *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *                    -----------------------                    *         
      *                    -   MODULE D'ENTREE   -                    *         
      *                    -----------------------                    *         
      *                               -                               *         
      *           -----------------------------------------           *         
      *           -      -            -                   -           *         
      *  ----------  --------  ----------  -----------------------    *         
      *  - HANDLE -  - USER -  - ADRESS -  - RECEPTION MESSAGE   -    *         
      *  ----------  --------  ----------  -----------------------    *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-ENTREE              SECTION.                                      
      *                                                                         
           PERFORM INIT-HANDLE.                                                 
      *                                                                         
           PERFORM INIT-USER.                                                   
      *                                                                         
           PERFORM INIT-ADDRESS.                                                
      *                                                                         
           PERFORM RECEPTION-MESSAGE.                                           
      *                                                                         
       FIN-MODULE-ENTREE.   EXIT.                                               
                    EJECT                                                       
       E01-COPY SECTION. CONTINUE. COPY SYKCHAND.                               
      *                                                                         
      *****************************************************************         
      *    INITIALISATION DES ZONES QUI NE PEUVENT PAS ETRE EN VALUE  *         
      *****************************************************************         
      *                                                                         
       INIT-USER                  SECTION.                                      
      *                                                                         
           MOVE   LOW-VALUE   TO Z-MAP.                                         
      *                                                                         
           MOVE   'EFG21'  TO NOM-MAP.                                          
           MOVE   'EFG21' TO NOM-MAPSET.                                        
           MOVE   'TFG21' TO NOM-PROG.                                          
           MOVE   'FG21'   TO NOM-TACHE.                                        
           MOVE   'FG21'   TO NOM-LEVEL-MAX.                                    
           MOVE   'NON'    TO DEBUGGIN.                                         
      *                                  GESTION DE LA COMMAREA                 
           IF  EIBCALEN = 0                                                     
               MOVE SPACES   TO Z-COMMAREA                                      
               MOVE 'TFG20'  TO NOM-PROG-XCTL                                   
               PERFORM XCTL-NO-COMMAREA                                         
           END-IF.                                                              
      *                                            AUTRE TACHE                  
           MOVE COM-FG20-LONG-COMMAREA TO LONG-COMMAREA.                        
      *                                      LONG-START.                        
      *                                                                         
           IF  LONG-COMMAREA LESS THAN 220                                      
               MOVE 'COMMAREA PLUS PETITE QUE 220' TO MESS                      
               GO TO ABANDON-TACHE                                              
           END-IF.                                                              
      *                                                                         
           IF  LONG-COMMAREA GREATER THAN 4096                                  
               MOVE 'COMMAREA PLUS GRANDE QUE 4096' TO MESS                     
               GO TO ABANDON-TACHE                                              
           END-IF.                                                              
      *                                                                         
           IF  EIBCALEN NOT = 0                                                 
               THEN MOVE DFHCOMMAREA  TO  Z-COMMAREA                            
               ELSE MOVE SPACES       TO  Z-COMMAREA                            
           END-IF.                                                              
      *                                                                         
       FIN-INIT-USER.  EXIT.                                                    
                    EJECT                                                       
      *                                                                         
       E02-COPY SECTION. CONTINUE. COPY SYKCADDR.                               
      *                                                                         
      *                                                                         
       RECEPTION-MESSAGE          SECTION.                                      
      *                                                                         
           PERFORM POSIT-ATTR-INITIAL.                                          
      *                                                                         
           IF  EIBTRNID NOT = NOM-TACHE                                         
               MOVE CODE-TRAITEMENT-AUTOMATIQUE TO FONCTION                     
               GO TO FIN-RECEPTION-MESSAGE                                      
           END-IF.                                                              
      *                                            FAST PATH                    
           IF  EIBTRNID  = Z-COMMAREA-TACHE-JUMP                                
               MOVE SPACES TO Z-COMMAREA-TACHE-JUMP                             
               MOVE CODE-TRAITEMENT-AUTOMATIQUE TO FONCTION                     
               GO TO FIN-RECEPTION-MESSAGE                                      
           END-IF.                                                              
      *                                                                         
           PERFORM RECEIVE-MAP.                                                 
      *                                                                         
           MOVE  EIBAID       TO WORKAID.                                       
      *                                            SWAP KS                      
      *    IF  TOUCHE-PF9                                                       
      *        MOVE CODE-SWAP TO FONCTION                                       
      *    END-IF.                                                              
      *                                            LEVEL-MAX KA                 
           IF  TOUCHE-PF4                                                       
               MOVE CODE-LEVEL-MAX TO FONCTION                                  
           END-IF.                                                              
      *                                            HELP KB                      
      *    IF  TOUCHE-PF1                                                       
      *        MOVE CODE-HELP TO FONCTION                                       
      *    END-IF.                                                              
      *                                            LEVEL-SUP KC                 
           IF  TOUCHE-PF3                                                       
               MOVE CODE-LEVEL-SUP TO FONCTION                                  
           END-IF.                                                              
      *                                            SUITE 01                     
           IF  TOUCHE-ENTER                                                     
               MOVE CODE-TRAITEMENT-NORMAL TO FONCTION                          
           END-IF.                                                              
      *                                            ZONCMD                       
           IF  MZONCMDI NOT = LOW-VALUE AND SPACES                              
               MOVE 1 TO DISPATCH-LEVEL                                         
               PERFORM RECEPTION-ZONE-COMMANDE                                  
           END-IF.                                                              
           IF  ECRAN-MAPFAIL                                                    
               MOVE LOW-VALUE TO Z-MAP                                          
               MOVE CODE-TRAITEMENT-AUTOMATIQUE TO FONCTION                     
           END-IF.                                                              
      *                                                                         
           PERFORM POSIT-ATTR-INITIAL.                                          
      *                                                                         
       FIN-RECEPTION-MESSAGE. EXIT.                                             
                    EJECT                                                       
       E03-COPY SECTION. CONTINUE. COPY SYKCRECV.                               
       E04-COPY SECTION. CONTINUE. COPY SYKCZCMD.                               
            EJECT                                                               
      *=== DARTY *****************************************************          
      *          * POSITIONNEMENT ATTRIBUTS INITIAUX DE LA MAP BMS   *          
      ****************************************************************          
      *                                                                         
       POSIT-ATTR-INITIAL         SECTION.                                      
      *                                                                         
      * PENSEZ QU'IL Y A TROIS ATTRIBUTS PAR VARIABLES                          
      *        A = ATTRIBUT STANDARD                                            
      *        C = COULEUR                                                      
      *        H = ATTRIBUT ETENDUS                                             
      *                                                                         
      * ON TROUVERA DANS LE COPY SYKWECRA LES NOMS ET LES VALEURS               
      * ACCORDES A CHACUN. ON POURRA EGALEMENT EN CREER DE NOUVEAUX.            
      *                                                                         
           MOVE NOR-ALP-FSET  TO  MZONCMDA.                                     
           MOVE DFH-YELLO     TO  MZONCMDC.                                     
           MOVE DFH-UNDLN     TO  MZONCMDH.                                     
      *                                                                         
           MOVE NOR-PRO-FSET  TO  MCODTRAA.                                     
           MOVE DFH-TURQ      TO  MCODTRAC.                                     
           MOVE DFH-BASE      TO  MCODTRAH.                                     
      *                                                                         
           MOVE NOR-PRO-FSET  TO  MCICSA.                                       
           MOVE DFH-TURQ      TO  MCICSC.                                       
           MOVE DFH-BASE      TO  MCICSH.                                       
      *                                                                         
           MOVE NOR-PRO-FSET  TO  MNETNAMA.                                     
           MOVE DFH-TURQ      TO  MNETNAMC.                                     
           MOVE DFH-BASE      TO  MNETNAMH.                                     
      *                                                                         
           MOVE NOR-PRO-FSET  TO  MSCREENA.                                     
           MOVE DFH-TURQ      TO  MSCREENC.                                     
           MOVE DFH-BASE      TO  MSCREENH.                                     
      *                                                                         
           MOVE NOR-PRO-FSET  TO  MTIMJOUA.                                     
           MOVE DFH-TURQ      TO  MTIMJOUC.                                     
           MOVE DFH-BASE      TO  MTIMJOUH.                                     
      *                                                                         
           MOVE NOR-PRO-FSET  TO  MDATJOUA.                                     
           MOVE DFH-TURQ      TO  MDATJOUC.                                     
           MOVE DFH-BASE      TO  MDATJOUH.                                     
      *                                                                         
      *                                                                         
       FIN-POSIT-ATTR-INITIAL.    EXIT.                                         
                   EJECT                                                        
      * -AIDA *********************************************************         
      *                                                               *         
      *  TTTTTTTTT RRRRRRRR  AAAAAAAAA IIIIIIIII TTTTTTTTT EEEEEEEEE  *         
      *  TTTTTTTTT RRRRRRRR  AAAAAAAAA IIIIIIIII TTTTTTTTT EEEEEEEEE  *         
      *     TTT    RRR   RRR AAA   AAA    III       TTT    EEE        *         
      *     TTT    RRR   RRR AAA   AAA    III       TTT    EEE        *         
      *     TTT    RRRRRRRR  AAAAAAAAA    III       TTT    EEEEEEEEE  *         
      *     TTT    RRRRRRRR  AAAAAAAAA    III       TTT    EEEEEEEEE  *         
      *     TTT    RRR RRRR  AAA   AAA    III       TTT    EEE        *         
      *     TTT    RRR   RR  AAA   AAA    III       TTT    EEE        *         
      *     TTT    RRR   RRR AAA   AAA IIIIIIIII    TTT    EEEEEEEEE  *         
      *     TTT    RRR   RRR AAA   AAA IIIIIIIII    TTT    EEEEEEEEE  *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * MODULE DE TRAITEMENT                                          *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-TRAITEMENT          SECTION.                                      
      *                                                                         
           IF  TRAITEMENT-NORMAL                                                
               PERFORM MODULE-TRAITEMENT-NORMAL                                 
           END-IF.                                                              
      *                                                                         
           IF  TRAITEMENT-AUTOMATIQUE                                           
               PERFORM MODULE-TRAITEMENT-AUTOMATIQUE                            
           END-IF.                                                              
      *                                                                         
       FIN-MODULE-TRAITEMENT. EXIT.                                             
                   EJECT                                                        
      *****************************************************************         
      *****************************************************************         
      ***************** TRAITEMENT AUTOMATIQUE ************************         
      *****************************************************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * MODULE DE BASE DU CONTAINER ......  * TRAITEMENT AUTOMATIQUE  *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *                -------------------------------                *         
      *                -   TRAITEMENT AUTOMATIQUE    -                *         
      *                -------------------------------                *         
      *                               -                               *         
      *                               -                               *         
      *                               -                               *         
      *                    ---------------------                      *         
      *                    - REMPLISSAGE ECRAN -                      *         
      *                    ---------------------                      *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-TRAITEMENT-AUTOMATIQUE  SECTION.                                  
      *                                                                         
           PERFORM REMPLISSAGE-FORMAT-ECRAN.                                    
      *                                                                         
       FIN-MODULE-TRAITEMENT-AUTOMAT. EXIT.                                     
       EJECT                                                                    
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *                -------------------------------                *         
      *                -   REMPLISSAGE DE L'ECRAN    -                *         
      *                -------------------------------                *         
      *                               -                               *         
      *           -----------------------------------------           *         
      *           -                   -                   -           *         
      * ----------------------   -------------   -----------------    *         
      * - ZONES OBLIGATOIRES -   - PROTEGEES -   - NON PROTEGEES -    *         
      * ----------------------   -------------   -----------------    *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * REMPLISSAGE DE LA MAP     *  FG21    * TRAITEMENT AUTOMATIQUE  *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       REMPLISSAGE-FORMAT-ECRAN      SECTION.                                   
      *                                                                         
            PERFORM REMPLISSAGE-ZONES-OBLIGATOIRES.                             
      *                                                                         
      *     PERFORM REMPLISSAGE-ZONES-PROTEGEES.                                
      *                                                                         
      *     PERFORM REMPLISSAGE-ZONES-NO-PROTEGEES.                             
      *                                                                         
       FIN-REMPLISSAGE-FORMAT-ECRAN. EXIT.                                      
       EJECT                                                                    
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * ZONES OBLIGATOIRES        *  FG21    * TRAITEMENT AUTOMATIQUE  *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       REMPLISSAGE-ZONES-OBLIGATOIRES SECTION.                                  
      *                                                                         
           MOVE COMM-CICS-APPLID      TO   MCICSI.                              
           MOVE COMM-CICS-NETNAM      TO   MNETNAMI.                            
           MOVE COMM-CICS-TRANSA      TO   MCODTRAI.                            
           MOVE EIBTRMID              TO   MSCREENI.                            
           MOVE COMM-DATE-JJ-MM-SSAA  TO   MDATJOUI.                            
           MOVE Z-TIMER-TIMJOU        TO   MTIMJOUI.                            
      *                                                                         
       FIN-REMP-ZONES-OBL.            EXIT.                                     
       EJECT                                                                    
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * ZONES PROTEGEES           *  FG21    * TRAITEMENT AUTOMATIQUE  *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
      *REMPLISSAGE-ZONES-PROTEGEES    SECTION.                                  
      *                                                                         
      *                                                                         
      *FIN-REMP-ZONES-PROT.           EXIT.                                     
       EJECT                                                                    
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * ZONES NON PROTEGEES       *  FG21    * TRAITEMENT AUTOMATIQUE  *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
      *REMPLISSAGE-ZONES-NO-PROTEGEES SECTION.                                  
      *                                                                         
      *                                                                         
      *FIN-REMP-ZONES-NO-PROT.        EXIT.                                     
       EJECT                                                                    
      *****************************************************************         
      *****************************************************************         
      ***************                                ******************         
      *************** T R A I T E M T    N O R M A L ******************         
      ***************                                ******************         
      *****************************************************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * MODULE DE BASE DU CONTAINER ....... * TRAITEMENT NORMAL       *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *                -------------------------------                *         
      *                -      TRAITEMENT NORMAL      -                *         
      *                -------------------------------                *         
      *                               -                               *         
      *           -----------------------------------------           *         
      *           -                   -                   -           *         
      * -------------------- -------------------- ------------------- *         
      * - CONTROLE SYNTAXE - - CONTROLE LOGIQUE - - TRAITEMENT TACHE- *         
      * -------------------- -------------------- ------------------- *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-TRAITEMENT-NORMAL     SECTION.                                    
      *                                                                         
           PERFORM CONTROLE-SYNTAXE.                                            
      *                                                                         
      *    IF OK                                                                
      *       PERFORM CONTROLE-LOGIQUE                                          
      *       END-IF                                                            
           IF OK                                                                
                 PERFORM TRAITEMENT-TACHE                                       
           END-IF.                                                              
      *                                                                         
       FIN-MODULE-TRAITEMENT-NORM.  EXIT.                                       
       EJECT                                                                    
      *****************************************************************         
      *****************************************************************         
      *************** CONTROLES SYNYAXIQUES ***************************         
      *****************************************************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *  CONTROLES  SYNTAXIQUES   *  &V2   * TRAITEMENT NORMAL       *          
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       CONTROLE-SYNTAXE           SECTION.                                      
      *     IF (MZONCMDI = '1' OR '2' OR '3' OR '4')                            
            IF MZONCMDI NOT > SPACES                                            
                 MOVE BRT-ALP-FSET    TO   MZONCMDA                             
                 IF  OK MOVE                                                    
                    'K010 : SELECTION OBLIGATOIRE'                              
                                     TO   MLIBERRI                              
                    MOVE 'K020'      TO   COM-CODERR                            
                    MOVE CURSEUR     TO   MZONCMDL                              
                    MOVE DFH-RED     TO   MZONCMDC                              
                    MOVE DFH-REVRS   TO   MZONCMDH                              
                    MOVE  '1'        TO   KONTROL                               
                 END-IF                                                         
            ELSE                                                                
                EVALUATE MZONCMDI                                               
                   WHEN '1'                                                     
                      MOVE '1' TO COMM-FG20-SELEC                               
                   WHEN '2'                                                     
                      MOVE '2' TO COMM-FG20-SELEC                               
                   WHEN OTHER                                                   
      *--------VALEURS INCONNUES                                                
                     MOVE BRT-ALP-FSET TO  MZONCMDA                             
                     IF OK MOVE                                                 
                        'K020 : VOTRE SELECTION EST INVALIDE'                   
                                         TO MLIBERRI                            
                        MOVE 'K020'  TO   COM-CODERR                            
                        MOVE CURSEUR TO   MZONCMDL                              
                        MOVE DFH-RED TO   MZONCMDC                              
                        MOVE DFH-REVRS TO MZONCMDH                              
                        MOVE '1'     TO   KONTROL                               
                     END-IF                                                     
      *                                                                         
                END-EVALUATE                                                    
            END-IF.                                                             
       FIN-CONTROLE-SYNTAXE.      EXIT.                                         
       EJECT                                                                    
      *****************************************************************         
      *****************************************************************         
      *************** CONTROLES LOGIQUES ******************************         
      *****************************************************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *  CONTROLES  LOGIQUES      *  FG21    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
      *CONTROLE-LOGIQUE           SECTION.                                      
      *                                                                         
      *                                                                         
      *FIN-CONTROL-LOGIQUE.       EXIT.                                         
       EJECT                                                                    
      *****************************************************************         
      *****************************************************************         
      *************** TRAITEMENT TACHE ********************************         
      *****************************************************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *  TRAITEMENTS DE LA TACHE  *  FG21    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *                -------------------------------                *         
      *                -  TRAITEMENT DE LA TACHE     -                *         
      *                -------------------------------                *         
      *                               -                               *         
      *           -----------------------------------------           *         
      *           -                   -                   -           *         
      * -------------------  ----------------  ---------------------  *         
      * - GESTION FICHIER -  - GESTION  MAP -  - GESTION COMMAREA  -  *         
      * -------------------  ----------------  ---------------------  *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       TRAITEMENT-TACHE               SECTION.                                  
      *                                                                         
           PERFORM TRAITEMENT-FICHIER.                                          
      *                                                                         
      *    PERFORM TRAITEMENT-COMMAREA.                                         
      *                                                                         
           PERFORM TRAITEMENT-MAP.                                              
      *                                                                         
       FIN-TRAITEMENT-TACHE.          EXIT.                                     
       EJECT                                                                    
      *****************************************************************         
      *************** GESTION DES FICHIERS ****************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *  GESTION DES FICHIERS     *  FG21    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *                -------------------------------                *         
      *                -    GESTION DES FICHIERS     -                *         
      *                -------------------------------                *         
      *                               -                               *         
      *           -----------------------------------------           *         
      *           -            -             -            -           *         
      * ---------------- ------------ ---------------- -------------- *         
      * - DETERMINATION- - CREATION - - MODIFICATION - - SUPRESSION - *         
      * ---------------- ------------ ---------------- -------------- *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       TRAITEMENT-FICHIER             SECTION.                                  
      *                                                                         
           PERFORM DETERMINATION-TRAITEMENT.                                    
      *    IF CREATION                                                          
      *       PERFORM CREATION-RECORD                                           
      *    END-IF.                                                              
      *                                                                         
      *    IF MODIFICATION                                                      
      *       PERFORM MODIFICATION-RECORD                                       
      *    END-IF.                                                              
       FIN-TRAITEMENT-FICHIER.        EXIT.                                     
       EJECT                                                                    
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * DETERMINATION DE L'ACTION *  FG21    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       DETERMINATION-TRAITEMENT       SECTION.                                  
      *                                                                         
           MOVE COM-FG20-LONG-COMMAREA TO LONG-COMMAREA-LINK.           00000600
           MOVE Z-COMMAREA             TO Z-COMMAREA-LINK.              00000600
           MOVE 'MFG20' TO NOM-PROG-LINK.                               00000700
           PERFORM LINK-PROG.                                           00000800
           MOVE Z-COMMAREA-LINK TO Z-COMMAREA.                                  
           MOVE COMM-FG20-LIBERR   TO MLIBERRI.                                 
           MOVE SPACES    TO   COMM-FG20-LIBERR.                                
           SET TRAITEMENT-AUTOMATIQUE TO TRUE.                                  
      *                                                                 00000900
      *          ETC ETC                                                        
      *                                                                         
       FIN-DETER-TRAITEMENT.          EXIT.                                     
       EJECT                                                                    
   *                                                                    70001   
      *****************************************************************         
      *************** GESTION DE LA COMMAREA **************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * GESTION DE LA COMMAREA    *  FG21    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
      *TRAITEMENT-COMMAREA            SECTION.                                  
      *                                                                         
      *    MOVE XX01-XXXXXX       TO COM-FG20-XXXXXX.                           
      *                                                                         
      *FIN-TRAITEMENT-COMMAREA.       EXIT.                                     
       EJECT                                                                    
      *****************************************************************         
      *************** GESTION DE LA MAP *******************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * GESTION DE LA MAP         *  FG21    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       TRAITEMENT-MAP                 SECTION.                                  
      *                                                                         
           MOVE -1 TO MZONCMDL.                                                 
           MOVE SPACE TO     MZONCMDI.                                          
      *                                                                         
       FIN-TRAITEMENT-MAP.            EXIT.                                     
       EJECT                                                                    
      *                                                                         
      * -AIDA *********************************************************         
      *        MODULES DE CONTROLES ET DE TRAITEMENTS STANDARDISES              
      *****************************************************************         
      *                                                                         
      *T01-COPY SECTION. CONTINUE. COPY SYKPAGE0.                               
      *T02-COPY SECTION. CONTINUE. COPY SYKPDATE.                               
      *T03-COPY SECTION. CONTINUE. COPY SYKPDECI.                               
      *T04-COPY SECTION. CONTINUE. COPY SYKPVALN.                               
      *T05-COPY SECTION. CONTINUE. COPY SYKPTIME.                               
      *T06-COPY SECTION. CONTINUE. COPY SYKPMONT.                               
                   EJECT                                                        
      * -AIDA *********************************************************         
      *                                                               *         
      *  SSSSSSSS   OOOOOOO  RRRRRRRR  TTTTTTTTT IIIIIIIII EEEEEEEEE  *         
      *  SSSSSSSS  OOOOOOOOO RRRRRRRRR TTTTTTTTT IIIIIIIII EEEEEEEEE  *         
      *  SSSS      OOO   OOO RRR   RRR    TTT       III    EEE        *         
      *   SSSS     OOO   OOO RRR   RRR    TTT       III    EEE        *         
      *    SSSS    OOO   OOO RRR   RRR    TTT       III    EEEEEEEEE  *         
      *     SSSS   OOO   OOO RRRRRRRRR    TTT       III    EEEEEEEEE  *         
      *      SSSS  OOO   OOO RRRRRRRR     TTT       III    EEE        *         
      *      SSSS  OOO   OOO RRR   RR     TTT       III    EEE        *         
      *  SSSSSSSS  OOOOOOOOO RRR   RRR    TTT    IIIIIIIII EEEEEEEEE  *         
      *  SSSSSSSS   OOOOOOO  RRR   RRR    TTT    IIIIIIIII EEEEEEEEE  *         
      *                                                               *         
      *****************************************************************         
                   EJECT                                                        
      * -AIDA *********************************************************         
      *    S O R T I E   M A Q U E T T E   S A I S I E   S I M P L E  *         
      *****************************************************************         
                    EJECT                                                       
      *****************************************************************         
      *                 MODULE DE SORTIE GENERALISE                   *         
      *****************************************************************         
      *                                                                         
       MODULE-SORTIE              SECTION.                                      
      *                                                                         
           IF  TRAITEMENT-AUTOMATIQUE                                           
                       PERFORM          SAVE-SWAP-ATTR                          
                       PERFORM          SORTIE-AFFICHAGE-FORMAT                 
           END-IF.                                                              
      *                                                                         
           IF  NOT OK                                                           
                       PERFORM          SAVE-SWAP-ATTR                          
                       PERFORM          SORTIE-ERREUR                           
           END-IF.                                                              
      *                                                                         
           IF  SWAP                                                             
                       PERFORM          REST-SWAP-ATTR                          
                       PERFORM          SORTIE-SWAP                             
           END-IF.                                                              
      *                                                                         
           IF  TRAITEMENT-NORMAL                                                
                       PERFORM          SORTIE-SUITE                            
           END-IF.                                                              
      *                                                                         
           IF  LEVEL-SUP                                                        
                       PERFORM          SORTIE-LEVEL-SUPERIEUR                  
           END-IF.                                                              
      *                                                                         
           IF  LEVEL-MAX OR JUMP                                                
                       PERFORM          SORTIE-LEVEL-MAX                        
           END-IF.                                                              
      *                                                                         
           IF  ERREUR-MANIPULATION                                              
                       PERFORM          REST-SWAP-ATTR                          
                       PERFORM          SORTIE-ERREUR-MANIP                     
           END-IF.                                                              
      *                                                                         
      *    IF  HELP                                                             
      *                PERFORM          REST-SWAP-ATTR                          
      *                PERFORM          SORTIE-HELP                             
      *    END-IF.                                                              
      *                                                                         
      * ABANDON * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
           MOVE 'ERREUR CODE FONCTION DANS MODULE SORTIE' TO MESS               
           GO TO ABANDON-TACHE.                                                 
      *                                                                         
       FIN-MODULE-SORTIE. EXIT.                                                 
                    EJECT                                                       
      *****************************************************************         
      *                  SORTIE AFFICHAGE FORMAT                      *         
      *****************************************************************         
      *                                                                         
       SORTIE-AFFICHAGE-FORMAT    SECTION.                                      
      *                                                                         
      *    POSITIONNEMENT OBLIGATOIRE DU CURSEUR.                               
           MOVE CURSEUR  TO  MZONCMDL.                                          
      *                                                                         
           PERFORM SEND-MAP.                                                    
      *                                                                         
           MOVE SPACES    TO Z-COMMAREA-TACHE-JUMP.                             
           MOVE NOM-TACHE TO NOM-TACHE-RETOUR.                                  
      *                                                                         
           PERFORM RETOUR-COMMAREA.                                             
      *                                                                         
       FIN-SORTIE-AFFICHAGE-FORMAT. EXIT.                                       
                    EJECT                                                       
      *****************************************************************         
      *  AFFICHAGE DE LA MAP EN ERREUR                                *         
      *                      EN CONFIRMATION                          *         
      *  ET RETURN AU MEME PROGRAMME                                  *         
      *****************************************************************         
      *                                                                         
       SORTIE-ERREUR              SECTION.                                      
      *                                                                         
           PERFORM SEND-MAP-SORTIE-ERREUR.                                      
      *                                                                         
           MOVE NOM-TACHE TO NOM-TACHE-RETOUR.                                  
      *                                                                         
           PERFORM RETOUR-COMMAREA.                                             
      *                                                                         
       FIN-SORTIE-ERREUR. EXIT.                                                 
                    EJECT                                                       
      *****************************************************************         
      *  AFFICHAGE DE LA MAP SUIVANTE ET RETURN AU PROGRAMME SUIVANT  *         
      *****************************************************************         
      *                                                                         
       SORTIE-SUITE               SECTION.                                      
      *                                                                         
           PERFORM SAVE-SWAP-ATTR.                                              
      *                                                                         
           PERFORM SEND-MAP-DATA-ONLY.                                          
      *                                                                         
           MOVE EIBTRNID TO NOM-TACHE-RETOUR.                                   
      *                                                                         
           PERFORM RETOUR-COMMAREA.                                             
      *                                                                         
       FIN-SORTIE-SUITE. EXIT.                                                  
                    EJECT                                                       
      *****************************************************************         
      *    RETOUR APRES PF4 AU LEVEL SUPERIEUR                      *           
      *****************************************************************         
      *                                                                         
       SORTIE-LEVEL-SUPERIEUR     SECTION.                                      
      *                                                                         
      *                                                                         
           MOVE 'TFG20'     TO NOM-PROG-XCTL.                                   
           MOVE  NOM-PROG   TO COM-PGMPRC.                                      
      *                                                                         
           PERFORM XCTL-PROG-COMMAREA.                                          
      *                                                                         
      *                                                                         
       FIN-SORTIE-LEVEL-SUPERIEUR. EXIT.                                        
                    EJECT                                                       
      *****************************************************************         
      *    RETOUR APRES PF3 AU MENU                                 *           
      *****************************************************************         
      *                                                                         
       SORTIE-LEVEL-MAX           SECTION.                                      
      *                                                                         
           MOVE 'TFG20'     TO NOM-PROG-XCTL.                                   
           MOVE  NOM-PROG   TO COM-PGMPRC.                                      
      *                                                                         
           PERFORM XCTL-PROG-COMMAREA.                                          
      *                                                                         
       FIN-SORTIE-LEVEL-MAX. EXIT.                                              
                    EJECT                                                       
      *****************************************************************         
      *         SORTIE ERREUR MANIPULATION                            *         
      *****************************************************************         
      *                                                                         
       SORTIE-ERREUR-MANIP        SECTION.                                      
      *                                                                         
           MOVE 'K017 : ERREUR MANIPULATION' TO MLIBERRI.                       
           MOVE 'K017'                       TO COM-CODERR.                     
      *                                                                         
           MOVE CURSEUR                      TO MZONCMDL.                       
      *                                                                         
           PERFORM SEND-MAP-ERREUR-MANIP.                                       
      *                                                                         
           MOVE NOM-TACHE TO NOM-TACHE-RETOUR.                                  
      *                                                                         
           PERFORM RETOUR-COMMAREA.                                             
      *                                                                         
       FIN-SORTIE-ERREUR-MANIP. EXIT.                                           
                    EJECT                                                       
      *=== DARTY ******************************************************         
      *         SAUVEGARDE DES ATTRIBUTS BMS EN CAS DE SWAP           *         
      *****************************************************************         
      *                                                                         
       SAVE-SWAP-ATTR             SECTION.                                      
      *                                                                         
      *                                                                         
      *    MOVE MXXXXXA     TO COMM-SWAP-ATTR (001).                            
      *    MOVE MXXXXXC     TO COMM-SWAP-ATTR (002).                            
      *    MOVE MXXXXXH     TO COMM-SWAP-ATTR (003).                            
      *                                                                         
      *    MOVE MZZZZZA     TO COMM-SWAP-ATTR (148).                            
      *    MOVE MZZZZZC     TO COMM-SWAP-ATTR (149).                            
      *    MOVE MZZZZZH     TO COMM-SWAP-ATTR (150).                            
      *                                                                         
      *                                                                         
       FIN-SAVE-SWAP-ATTR     . EXIT.                                           
                    EJECT                                                       
      *=== DARTY ******************************************************         
      *         RESTAURATION DES ATTRIBUTS BMS EN CAS DE SWAP         *         
      *****************************************************************         
      *                                                                         
       REST-SWAP-ATTR             SECTION.                                      
      *                                                                         
      *                                                                         
           MOVE EIBCPOSN             TO COMM-SWAP-CURS.                         
      *                                                                         
      *    MOVE COMM-SWAP-ATTR (001) TO MXXXXXA.                                
      *    MOVE COMM-SWAP-ATTR (002) TO MXXXXXC.                                
      *    MOVE COMM-SWAP-ATTR (003) TO MXXXXXH.                                
      *                                                                         
      *    MOVE COMM-SWAP-ATTR (148) TO MZZZZZA.                                
      *    MOVE COMM-SWAP-ATTR (149) TO MZZZZZC.                                
      *    MOVE COMM-SWAP-ATTR (150) TO MZZZZZH.                                
      *                                                                         
      *                                                                         
       FIN-REST-SWAP-ATTR     . EXIT.                                           
                    EJECT                                                       
      * AIDA  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *        DESCRIPTION DES BRIQUES AIDA CICS          * SYKC....  *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
      *S01-COPY SECTION. CONTINUE. COPY SYKCHELP.                               
       S02-COPY SECTION. CONTINUE. COPY SYKCRTCO.                               
      *S03-COPY SECTION. CONTINUE. COPY SYKCRTNO.                               
       S04-COPY SECTION. CONTINUE. COPY SYKCRT00.                               
       S05-COPY SECTION. CONTINUE. COPY SYKCSMDO.                               
       S06-COPY SECTION. CONTINUE. COPY SYKCSMEM.                               
      *S07-COPY SECTION. CONTINUE. COPY SYKCSMER.                               
      *S08-COPY SECTION. CONTINUE. COPY SYKCSMNO.                               
       S09-COPY SECTION. CONTINUE. COPY SYKCSMSE.                               
       S10-COPY SECTION. CONTINUE. COPY SYKCSM00.                               
      *S11-COPY SECTION. CONTINUE. COPY SYKCSPAG.                               
      *S12-COPY SECTION. CONTINUE. COPY SYKCST00.                               
      *S13-COPY SECTION. CONTINUE. COPY SYKCSTRT.                               
       S14-COPY SECTION. CONTINUE. COPY SYKCXCTL.                               
       S15-COPY SECTION. CONTINUE. COPY SYKCLINK.                               
       S16-COPY SECTION. CONTINUE. COPY SYKCSWAP.                               
       S17-COPY SECTION. CONTINUE. COPY SYKXSWAP.                               
      *S18-COPY SECTION. CONTINUE. COPY SYKCRETR.                               
      *                                                                         
      *                                                                         
             EJECT                                                              
      *                                                                         
      * -AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *      DESCRIPTION DES MODULES DE PREPARATION DE CLEFS SQL      *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
      *USER-SQL-1 SECTION. CONTINUE. COPY PRAN0000.                             
      *USER-SQL-2 SECTION. CONTINUE. COPY PRGA0100.                             
      *USER-SQL-99 SECTION. CONTINUE. COPY PRGA9900.                            
      *                                                                         
      ** DARTY ********************************************************         
      *        * PROCEDURE DE TRAITEMENT DES MESSAGES GENERALISES     *         
      *****************************************************************         
      *                                                                         
      *MLIBERRGEN SECTION.                                                      
      *                                                                         
      *    MOVE CODE-RETOUR    TO W-CODE-RETOUR-MLIBERRG.                       
      *    MOVE NOM-PROG       TO GA99-CNOMPGRM.                                
      *    MOVE  FUNC-SELECT   TO  TRACE-SQL-FUNCTION.                          
      *                                                                         
      *    PERFORM CLEF-GA9900.                                                 
      *                                                                         
      *    IF DEBUGGIN = 'OUI'                                                  
      *       PERFORM TRACE-SQL                                                 
      *    END-IF.                                                              
      *                                                                         
      *    EXEC SQL SELECT                                                      
      *                   CERRUT,                                               
      *                   LIBERR                                                
      *             INTO                                                        
      *                  :GA99-CERRUT   :GA99-CERRUT-F,                         
      *                  :GA99-LIBERR   :GA99-LIBERR-F                          
      *             FROM     RVGA9900                                           
      *             WHERE                                                       
      *                   CNOMPGRM = :GA99-CNOMPGRM                             
      *             AND   NSEQERR = :GA99-NSEQERR                               
      *             END-EXEC.                                                   
      *                                                                         
      *    IF DEBUGGIN = 'OUI'                                                  
      *       PERFORM TRACE-SQL                                                 
      *    END-IF.                                                              
      *                                                                         
      *    PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
      *    IF NON-TROUVE                                                        
      *       THEN MOVE 'MESSAGE NON REFERENCE' TO LIBERR-MESSAGE               
      *            MOVE GA99-NSEQERR TO ERREUR-UTILISATEUR                      
      *       ELSE MOVE GA99-CERRUT TO ERREUR-UTILISATEUR                       
      *            MOVE GA99-LIBERR TO LIBERR-MESSAGE                           
      *    END-IF.                                                              
      *                                                                         
      *    IF MESSAGE-VARIABLE NOT = SPACES                                     
      *       THEN MOVE MESSAGE-VARIABLE TO LIBERR-MESSAGE                      
      *    END-IF.                                                              
      *                                                                         
      *    MOVE MESSAGE-ERREUR TO MLIBERRI.                                     
      *    MOVE ERREUR-UTILISATEUR TO COM-CODERR.                               
      *    MOVE W-CODE-RETOUR-MLIBERRG TO CODE-RETOUR.                          
      *                                                                         
      *FIN-MLIBERRGEN.       EXIT.                                              
                EJECT                                                           
      *                                                                         
      * -AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *        DESCRIPTION DES BRIQUES AIDA SQL           * SYKS....  *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
      *AIDA-SQL-01 SECTION. CONTINUE. COPY  SYKSTRAC.                           
      *AIDA-SQL-02 SECTION. CONTINUE. COPY  SYKSRETC.                           
      *                                                                         
       99-COPY SECTION. CONTINUE. COPY SYKCERRO.                                
