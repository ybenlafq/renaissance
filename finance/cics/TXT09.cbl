      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 18/10/2016 0        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.                     TXT09.                                   
       AUTHOR. PROJET DSA0.                                                     
      *               CONTROLE AVANT GENERATION DE RTGA91                       
       ENVIRONMENT DIVISION.                                                    
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                                   
      *{ Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                      
      *SPECIAL-NAMES.                                                           
      *    DECIMAL-POINT IS COMMA.                                              
      *--                                                                       
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
      *}                                                                        
       DATA DIVISION.                                                           
       WORKING-STORAGE SECTION.
      *{Post-translation Correct-Val-pointeur
         77  NULLV PIC S9(8) COMP-5 VALUE -16777216.
         77  NULLP REDEFINES NULLV USAGE IS POINTER.
      *}
                  COPY  SYKWDIV0.                                               
       EJECT                                                                    
      *****************************************************************         
      *   DECRIRE   ICI   LES   ZONES   SPECIFIQUES   AU   PROGRAMME  *         
      *****************************************************************         
      *                                                                         
      *                                                                         
       EJECT                                                                    
           COPY SYKWZCMD.                                                       
      * -AIDA *********************************************************         
      *   ZONES GENERALES OBLIGATOIRES                                *         
      *****************************************************************         
           COPY  SYKWEIB0.                                                      
           COPY  SYKWDATH.                                                      
           COPY  SYKWCWA0.                                                      
           COPY  SYKWTCTU.                                                      
           COPY  SYKWECRA.                                                      
      * -AIDA *********************************************************         
      *   ZONES DE CONTROLE ET DE TRAITEMENT                          *         
      *****************************************************************         
           COPY  SYKWAGE0.                                                      
           COPY  SYKWDATE.                                                      
           COPY  SYKWDECI.                                                      
           COPY  SYKWVALN.                                                      
           COPY  SYKWTIME.                                                      
           COPY  SYKWMONT.                                                      
       EJECT                                                                    
      * -AIDA *********************************************************         
      *  ZONES DE LA MAP                                                        
      *****************************************************************         
      *                                                                         
       01  FILLER  PIC X(16) VALUE '*** MAP XT09 ***'.                          
           COPY EXT09 REPLACING EXT09I BY Z-MAP.                                
       EJECT                                                                    
      * -AIDA *********************************************************         
      *  ZONES DE COMMAREA                                                      
      *****************************************************************         
           COPY  COMMXT00.                                                      
           COPY  SYKWCOMM.                                                      
           COPY  SYKWSTAR.                                                      
       EJECT                                                                    
      * -AIDA *********************************************************         
      *  ZONE DE COMMAREA POUR PROGRAMME APPELE PAR LINK                        
      *****************************************************************         
      *                                                                         
       01  FILLER             PIC X(16)  VALUE 'Z-COMMAREA-LINK'.               
       01  Z-COMMAREA-LINK    PIC X(200).                                       
      *                                                                         
           COPY COMMDATC.                                                       
       EJECT                                                                    
      * -AIDA *********************************************************         
      *       * ZONE BUFFER D'ENTREE-SORTIE                                     
      *****************************************************************         
           COPY  SYKWZINO.                                                      
       EJECT                                                                    
      * -AIDA *********************************************************         
      *       * ZONE DE GESTION DES ERREURS                                     
      *****************************************************************         
           COPY  SYKWERRO.                                                      
       EJECT                                                                    
      * -AIDA *********************************************************         
      *       * ZONES DE GESTION DES FICHIERS                                   
      *****************************************************************         
       EJECT                                                                    
      * -AIDA *********************************************************         
      *                                                               *         
      *  LLL      IIIIIIII NN    NNN KKK    KKK AAAAAAAAA GGGGGGGGG   *         
      *  LLL      IIIIIIII NNNN  NNN KKK   KKK  AAAAAAAAA GGGGGGGGG   *         
      *  LLL         II    NNNNN NNN KKK  KKK   AA     AA GG          *         
      *  LLL         II    NNN NNNNN KKK KKK    AA     AA GG          *         
      *  LLL         II    NNN  NNNN KKKKKK     AAAAAAAAA GG          *         
      *  LLL         II    NNN   NNN KKKKKK     AAAAAAAAA GG  GGGG    *         
      *  LLL         II    NNN   NNN KKK KKK    AA     AA GG    GG    *         
      *  LLL         II    NNN   NNN KKK  KKK   AA     AA GG    GG    *         
      *  LLLLLLLL IIIIIIII NNN   NNN KKK   KKK  AA     AA GGGGGGGG    *         
      *  LLLLLLLL IIIIIIII NNN   NNN KKK    KK  AA     AA GGGGGGGG    *         
      *                                                               *         
      *****************************************************************         
      *                                                                         
       LINKAGE SECTION.                                                         
      *                    DFHCOMMAREA                                          
       01  DFHCOMMAREA.                                                         
           05  FILLER PIC X OCCURS 4000 DEPENDING ON EIBCALEN.                  
           COPY  SYKLINKB.                                                      
       EJECT                                                                    
       PROCEDURE DIVISION.                                                      
      *                                                                         
      *****************************************************************         
      *              T R A M E   DU   P R O G R A M M E               *         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *           -----------------------------------------           *         
      *           - MODULE DE BASE DE LA TRANSACTION XT09 -           *         
      *           -----------------------------------------           *         
      *                               I                               *         
      *           -----------------------------------------           *         
      *           I                   I                   I           *         
      *   -----------------   -----------------   -----------------   *         
      *   -    ENTREE     -   -  TRAITEMENT   -   -     SORTIE    -   *         
      *   -----------------   -----------------   -----------------   *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-XT09                    SECTION.                                  
      *                                                                         
           PERFORM MODULE-ENTREE.                                               
      *                                                                         
           IF TRAITEMENT                                                        
              PERFORM MODULE-TRAITEMENT                                         
           END-IF.                                                              
      *                                                                         
           PERFORM MODULE-SORTIE.                                               
      *                                                                         
       FIN-MODULE-XT09.    EXIT.                                                
       EJECT                                                                    
      * -AIDA *********************************************************         
      *                                                               *         
      *  EEEEEEEEE NN     NN TTTTTTTTT RRRRRRRR  EEEEEEEEE EEEEEEEEE  *         
      *  EEEEEEEEE NNN    NN TTTTTTTTT RRRRRRRRR EEEEEEEEE EEEEEEEEE  *         
      *  EEE       NNN    NN    TTT    RRR   RRR EEE       EEE        *         
      *  EEE       NNNN   NN    TTT    RRR   RRR EEE       EEE        *         
      *  EEEEEEEEE NN NN  NN    TTT    RRRRRRRRR EEEEEEEEE EEEEEEEEE  *         
      *  EEEEEEEEE NN  NN NN    TTT    RRRRRRRR  EEEEEEEEE EEEEEEEEE  *         
      *  EEE       NN   NNNN    TTT    RRR RRRR  EEE       EEE        *         
      *  EEE       NN    NNN    TTT    RRR   RR  EEE       EEE        *         
      *  EEEEEEEEE NN    NNN    TTT    RRR    RR EEEEEEEEE EEEEEEEEE  *         
      *  EEEEEEEEE NN     NN    TTT    RRR    RR EEEEEEEEE EEEEEEEEE  *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
       EJECT                                                                    
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *                    -----------------------                    *         
      *                    -   MODULE D'ENTREE   -                    *         
      *                    -----------------------                    *         
      *                               -                               *         
      *           -----------------------------------------           *         
      *           -      -            -                   -           *         
      *  ----------  --------  ----------  -----------------------    *         
      *  - HANDLE -  - USER -  - ADRESS -  - RECEPTION MESSAGE   -    *         
      *  ----------  --------  ----------  -----------------------    *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-ENTREE              SECTION.                                      
      *                                                                         
           PERFORM INIT-HANDLE.                                                 
      *                                                                         
           PERFORM INIT-USER.                                                   
      *                                                                         
           PERFORM INIT-ADDRESS.                                                
      *                                                                         
           PERFORM RECEPTION-MESSAGE.                                           
      *                                                                         
       FIN-MODULE-ENTREE.         EXIT.                                         
       EJECT                                                                    
       E01-COPY SECTION. CONTINUE. COPY SYKCHAND.                               
      *****************************************************************         
      *    INITIALISATION DES ZONES QUI NE PEUVENT PAS ETRE EN VALUE  *         
      *****************************************************************         
      *                                                                         
       INIT-USER                  SECTION.                                      
      *                                                                         
           MOVE   LOW-VALUE   TO  Z-MAP.                                        
      *                                                                         
           MOVE   'EXT09'     TO  NOM-MAP.                                      
           MOVE   'EXT09'    TO  NOM-MAPSET.                                    
           MOVE   'TXT09'    TO  NOM-PROG.                                      
           MOVE   'XT09'      TO  NOM-TACHE.                                    
           MOVE   'XT00'    TO  NOM-LEVEL-MAX.                                  
      *                                                                         
           MOVE   'NON'       TO  DEBUGGIN.                                     
      *                                                                         
      *----ON NE RENTRE PAS DIRECTEMENT DANS CE PROGRAMME                       
           IF  EIBCALEN  =  0                                                   
               MOVE SPACES       TO  Z-COMMAREA                                 
               MOVE 'TXT00'      TO  NOM-PROG-XCTL                              
               PERFORM XCTL-NO-COMMAREA                                         
           END-IF.                                                              
      *                                                                         
      *----RECEPTION COMMAREA                                                   
           MOVE COM-XT00-LONG-COMMAREA TO LONG-COMMAREA                         
                                             LONG-START.                        
      *                                                                         
      *                                                                         
           IF  LONG-COMMAREA  LESS  220   OR  GREATER  4096                     
               MOVE 'LONG-COMMAREA HORS-LIMITES' TO MESS                        
               GO TO ABANDON-TACHE                                              
           END-IF.                                                              
      *                                                                         
           IF EIBCALEN  LESS  220   OR  GREATER  4096                           
              MOVE 'EIBCALEN HORS LIMITES' TO MESS                              
              GO TO ABANDON-TACHE                                               
           END-IF.                                                              
      *                                                                         
           IF EIBCALEN  NOT =   LONG-COMMAREA                                   
              MOVE 'EIBCALEN NOT = LONG-COMMAREA' TO MESS                       
              GO TO ABANDON-TACHE                                               
           END-IF.                                                              
      *                                                                         
           MOVE DFHCOMMAREA  TO  Z-COMMAREA.                                    
      *                                                                         
           PERFORM COMMAREA-XT00-INPUT.                                         
      *                                                                         
       FIN-INIT-USER.             EXIT.                                         
       EJECT                                                                    
      * DARTY ********************************************************          
      *       * CONTROLES INFORMATIONS APPLICATIVES COMMAREA         *          
      ****************************************************************          
      *                                                                         
       COMMAREA-XT00-INPUT       SECTION.                                       
      *                                                                         
      *    IF COM-XT00-XXXX  NOT =  VALEUR-VOULUE                               
      *       MOVE 'COMMAREA : COM-XT00-XXXX HORS-LIMITES' TO MESS              
      *       GO TO ABANDON-TACHE                                               
      *    END-IF.                                                              
      *                                                                         
       FIN-COMMAREA-XT00-INPUT.      EXIT.                                      
       EJECT                                                                    
       E02-COPY SECTION. CONTINUE. COPY SYKCADDR.                               
      *****************************************************************         
      * DETERMINATION DU TRAITEMENT EN FONCTION DE L'ENVIRONNEMENT    *         
      *****************************************************************         
      *                                                                         
       RECEPTION-MESSAGE          SECTION.                                      
      *                                                                         
      *----AUTRE TACHE                                                          
           IF  EIBTRNID NOT = NOM-TACHE                                         
               PERFORM POSIT-ATTR-INITIAL                                       
               MOVE CODE-TRAITEMENT-AUTOMATIQUE TO FONCTION                     
               GO TO FIN-RECEPTION-MESSAGE                                      
           END-IF.                                                              
      *                                                                         
      *----FAST PATH OR JUMP                                                    
           IF  EIBTRNID  = Z-COMMAREA-TACHE-JUMP                                
               PERFORM POSIT-ATTR-INITIAL                                       
               MOVE SPACES TO Z-COMMAREA-TACHE-JUMP                             
               MOVE CODE-TRAITEMENT-AUTOMATIQUE TO FONCTION                     
               GO TO FIN-RECEPTION-MESSAGE                                      
           END-IF.                                                              
      *                                                                         
           PERFORM RECEIVE-MAP.                                                 
      *                                                                         
           MOVE  EIBAID       TO WORKAID.                                       
       EJECT                                                                    
      *----SWAP KS                                                              
           IF  TOUCHE-PF9                                                       
               MOVE CODE-SWAP TO FONCTION                                       
           END-IF.                                                              
      *                                                                         
      *----LEVEL-MAX KA                                                         
           IF  TOUCHE-PF4                                                       
               MOVE CODE-LEVEL-MAX TO FONCTION                                  
           END-IF.                                                              
      *                                                                         
      *----HELP KB                                                              
           IF  TOUCHE-PF1                                                       
               MOVE CODE-HELP TO FONCTION                                       
           END-IF.                                                              
      *                                                                         
      *----LEVEL-SUP KC                                                         
           IF  TOUCHE-PF3                                                       
               MOVE CODE-LEVEL-SUP TO FONCTION                                  
           END-IF.                                                              
      *                                                                         
      *----SUITE 01                                                             
      *----ON NE GENERE PAS DE RTGA91 SI MNOI DIFFERENT DE OUI                  
           IF  TOUCHE-ENTER                                                     
             IF  MONI NOT = 'OUI'                                               
                 MOVE CODE-LEVEL-SUP TO FONCTION                                
             ELSE                                                               
                 MOVE CODE-TRAITEMENT-NORMAL TO FONCTION                        
             END-IF                                                             
           END-IF.                                                              
      *                                                                         
      *                                                                         
      *----ZONE COMMANDE                                                        
           IF  MZONCMDI NOT = LOW-VALUE AND SPACES                              
               MOVE 1 TO DISPATCH-LEVEL                                         
               PERFORM RECEPTION-ZONE-COMMANDE                                  
           END-IF.                                                              
      *                                                                         
      *----MAPFAIL                                                              
           IF  ECRAN-MAPFAIL                                                    
               MOVE LOW-VALUE TO Z-MAP                                          
               MOVE CODE-TRAITEMENT-AUTOMATIQUE TO FONCTION                     
           END-IF.                                                              
      *                                                                         
           PERFORM POSIT-ATTR-INITIAL.                                          
      *                                                                         
       FIN-RECEPTION-MESSAGE.      EXIT.                                        
       EJECT                                                                    
       E03-COPY SECTION. CONTINUE. COPY SYKCRECV.                               
       E04-COPY SECTION. CONTINUE. COPY SYKCZCMD.                               
      * DARTY ********************************************************          
      *       * POSITIONNEMENT ATTRIBUTS INITIAUX DE LA MAP BMS      *          
      ****************************************************************          
      *                                                                         
       POSIT-ATTR-INITIAL         SECTION.                                      
      *                                                                         
      *                                                                         
           MOVE BRT-PRO-FSET  TO  MDATJOUA.                             04940000
           MOVE DFH-TURQ      TO  MDATJOUC.                             04950000
           MOVE DFH-BASE      TO  MDATJOUH.                             04960000
      *                                                                 04970000
           MOVE BRT-PRO-FSET  TO  MTIMJOUA.                             04980000
           MOVE DFH-TURQ      TO  MTIMJOUC.                             04990000
           MOVE DFH-BASE      TO  MTIMJOUH.                             05000000
      *                                                                 04970000
           MOVE BRT-PRO-FSET  TO  MLIBERRA.                             06860000
           MOVE DFH-RED       TO  MLIBERRC.                             06870000
           MOVE DFH-REVRS     TO  MLIBERRH.                             06880000
      *                                                                 06890000
           MOVE BRT-ALP-FSET  TO  MZONCMDA.                             06900000
           MOVE DFH-YELLO     TO  MZONCMDC.                             06910000
           MOVE DFH-UNDLN     TO  MZONCMDH.                             06920000
      *                                                                 06930000
           MOVE BRT-PRO-FSET  TO  MCODTRAA.                             06940000
           MOVE DFH-TURQ      TO  MCODTRAC.                             06950000
           MOVE DFH-BASE      TO  MCODTRAH.                             06960000
      *                                                                 06970000
           MOVE BRT-PRO-FSET  TO  MCICSA.                               06980000
           MOVE DFH-TURQ      TO  MCICSC.                               06990000
           MOVE DFH-BASE      TO  MCICSH.                               07000000
      *                                                                 07010000
           MOVE BRT-PRO-FSET  TO  MNETNAMA.                             07020000
           MOVE DFH-TURQ      TO  MNETNAMC.                             07030000
           MOVE DFH-BASE      TO  MNETNAMH.                             07040000
      *                                                                 07050000
           MOVE BRT-PRO-FSET  TO  MSCREENA.                             07060000
           MOVE DFH-TURQ      TO  MSCREENC.                             07070000
           MOVE DFH-BASE      TO  MSCREENH.                             07080000
      *                                                                         
           MOVE BRT-PRO-FSET  TO  MCETATA.                              07060000
           MOVE DFH-TURQ      TO  MCETATC.                              07070000
           MOVE DFH-BASE      TO  MCETATH.                              07080000
      *                                                                         
           MOVE BRT-ALP-FSET  TO  MONA.                                 06900000
           MOVE DFH-YELLO     TO  MONC.                                 06910000
           MOVE DFH-UNDLN     TO  MONH.                                 06920000
      *                                                                         
       FIN-POSIT-ATTR-INITIAL.    EXIT.                                         
       EJECT                                                                    
      * -AIDA *********************************************************         
      *                                                               *         
      *  TTTTTTTTT RRRRRRRR  AAAAAAAAA IIIIIIIII TTTTTTTTT EEEEEEEEE  *         
      *  TTTTTTTTT RRRRRRRR  AAAAAAAAA IIIIIIIII TTTTTTTTT EEEEEEEEE  *         
      *     TTT    RRR   RRR AAA   AAA    III       TTT    EEE        *         
      *     TTT    RRR   RRR AAA   AAA    III       TTT    EEE        *         
      *     TTT    RRRRRRRR  AAAAAAAAA    III       TTT    EEEEEEEEE  *         
      *     TTT    RRRRRRRR  AAAAAAAAA    III       TTT    EEEEEEEEE  *         
      *     TTT    RRR RRRR  AAA   AAA    III       TTT    EEE        *         
      *     TTT    RRR   RR  AAA   AAA    III       TTT    EEE        *         
      *     TTT    RRR   RRR AAA   AAA IIIIIIIII    TTT    EEEEEEEEE  *         
      *     TTT    RRR   RRR AAA   AAA IIIIIIIII    TTT    EEEEEEEEE  *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
       EJECT                                                                    
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * MODULE DE TRAITEMENT                                          *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-TRAITEMENT          SECTION.                                      
      *                                                                         
           IF  TRAITEMENT-NORMAL                                                
               PERFORM MODULE-TRAITEMENT-NORMAL                                 
           END-IF.                                                              
      *                                                                         
           IF  TRAITEMENT-AUTOMATIQUE                                           
               PERFORM MODULE-TRAITEMENT-AUTOMATIQUE                            
           END-IF.                                                              
      *                                                                         
       FIN-MODULE-TRAITEMENT.      EXIT.                                        
       EJECT                                                                    
      *****************************************************************         
      *****************************************************************         
      ***************** TRAITEMENT AUTOMATIQUE ************************         
      *****************************************************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * MODULE DE BASE DU CONTAINER ......  * TRAITEMENT AUTOMATIQUE  *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *                -------------------------------                *         
      *                -   TRAITEMENT AUTOMATIQUE    -                *         
      *                -------------------------------                *         
      *                               -                               *         
      *                               -                               *         
      *                               -                               *         
      *                    ---------------------                      *         
      *                    - REMPLISSAGE ECRAN -                      *         
      *                    ---------------------                      *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-TRAITEMENT-AUTOMATIQUE  SECTION.                                  
      *                                                                         
           PERFORM REMPLISSAGE-FORMAT-ECRAN.                                    
      *                                                                         
       FIN-MODULE-TRAITEMENT-AUTOMAT. EXIT.                                     
       EJECT                                                                    
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *                -------------------------------                *         
      *                -   REMPLISSAGE DE L'ECRAN    -                *         
      *                -------------------------------                *         
      *                               -                               *         
      *           -----------------------------------------           *         
      *           -                   -                   -           *         
      * ----------------------   -------------   -----------------    *         
      * - ZONES OBLIGATOIRES -   - PROTEGEES -   - NON PROTEGEES -    *         
      * ----------------------   -------------   -----------------    *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * REMPLISSAGE DE LA MAP     *  XT09    * TRAITEMENT AUTOMATIQUE  *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       REMPLISSAGE-FORMAT-ECRAN      SECTION.                                   
      *                                                                         
            PERFORM REMPLISSAGE-ZONES-OBLIGATOIRES.                             
      *                                                                         
            PERFORM REMPLISSAGE-ZONES-PROTEGEES.                                
      *                                                                         
            PERFORM REMPLISSAGE-ZONES-NO-PROTEGEES.                             
      *                                                                         
       FIN-REMPLISSAGE-FORMAT-ECRAN. EXIT.                                      
       EJECT                                                                    
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * ZONES OBLIGATOIRES        *  XT09    * TRAITEMENT AUTOMATIQUE  *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       REMPLISSAGE-ZONES-OBLIGATOIRES SECTION.                                  
      *                                                                         
           MOVE COMM-CICS-APPLID      TO   MCICSI.                              
           MOVE COMM-CICS-NETNAM      TO   MNETNAMI.                            
           MOVE COMM-CICS-TRANSA      TO   MCODTRAI.                            
           MOVE EIBTRMID              TO   MSCREENI.                            
           MOVE COMM-DATE-JJ-MM-SSAA  TO   MDATJOUI.                            
           MOVE Z-TIMER-TIMJOU        TO   MTIMJOUI.                            
      *                                                                         
       FIN-REMP-ZONES-OBL.            EXIT.                                     
       EJECT                                                                    
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * ZONES PROTEGEES           *  XT09    * TRAITEMENT AUTOMATIQUE  *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       REMPLISSAGE-ZONES-PROTEGEES    SECTION.                                  
      *                                                                         
           MOVE COMM-XT00-CETAT TO MCETATI.                                     
      *                                                                         
       FIN-REMP-ZONES-PROT.           EXIT.                                     
       EJECT                                                                    
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * ZONES NON PROTEGEES       *  XT09    * TRAITEMENT AUTOMATIQUE  *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       REMPLISSAGE-ZONES-NO-PROTEGEES SECTION.                                  
      *                                                                         
           MOVE 'NON' TO MONI.                                                  
      *                                                                         
       FIN-REMP-ZONES-NO-PROT.        EXIT.                                     
       EJECT                                                                    
      *****************************************************************         
      *****************************************************************         
      ***************                                ******************         
      *************** T R A I T E M T    N O R M A L ******************         
      ***************                                ******************         
      *****************************************************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * MODULE DE BASE DU CONTAINER ....... * TRAITEMENT NORMAL       *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *                -------------------------------                *         
      *                -      TRAITEMENT NORMAL      -                *         
      *                -------------------------------                *         
      *                               -                               *         
      *           -----------------------------------------           *         
      *           -                   -                   -           *         
      * -------------------- -------------------- ------------------- *         
      * - CONTROLE SYNTAXE - - CONTROLE LOGIQUE - - TRAITEMENT TACHE- *         
      * -------------------- -------------------- ------------------- *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-TRAITEMENT-NORMAL     SECTION.                                    
      *                                                                         
           PERFORM CONTROLE-SYNTAXE.                                            
      *                                                                         
           IF OK                                                                
              PERFORM CONTROLE-LOGIQUE                                          
              IF OK                                                             
                 PERFORM TRAITEMENT-TACHE                                       
              END-IF                                                            
            END-IF.                                                             
      *                                                                         
       FIN-MODULE-TRAITEMENT-NORM.  EXIT.                                       
       EJECT                                                                    
      *****************************************************************         
      *****************************************************************         
      *************** CONTROLES SYNYAXIQUES ***************************         
      *****************************************************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *  CONTROLES  SYNTAXIQUES   *  &V2   * TRAITEMENT NORMAL       *          
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       CONTROLE-SYNTAXE           SECTION.                                      
      *                                                                         
      *                                                                         
       FIN-CONTROLE-SYNTAXE.      EXIT.                                         
       EJECT                                                                    
      *****************************************************************         
      *****************************************************************         
      *************** CONTROLES LOGIQUES ******************************         
      *****************************************************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *  CONTROLES  LOGIQUES      *  XT09    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       CONTROLE-LOGIQUE           SECTION.                                      
      *                                                                         
      *                                                                         
       FIN-CONTROL-LOGIQUE.       EXIT.                                         
       EJECT                                                                    
      *****************************************************************         
      *****************************************************************         
      *************** TRAITEMENT TACHE ********************************         
      *****************************************************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *  TRAITEMENTS DE LA TACHE  *  XT09    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *                -------------------------------                *         
      *                -  TRAITEMENT DE LA TACHE     -                *         
      *                -------------------------------                *         
      *                               -                               *         
      *           -----------------------------------------           *         
      *           -                   -                   -           *         
      * -------------------  ----------------  ---------------------  *         
      * - GESTION FICHIER -  - GESTION  MAP -  - GESTION COMMAREA  -  *         
      * -------------------  ----------------  ---------------------  *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       TRAITEMENT-TACHE               SECTION.                                  
      *                                                                         
           PERFORM TRAITEMENT-FICHIER.                                          
      *                                                                         
           PERFORM TRAITEMENT-COMMAREA.                                         
      *                                                                         
           PERFORM TRAITEMENT-MAP.                                              
      *                                                                         
       FIN-TRAITEMENT-TACHE.          EXIT.                                     
       EJECT                                                                    
      *****************************************************************         
      *************** GESTION DES FICHIERS ****************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *  GESTION DES FICHIERS     *  XT09    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *                -------------------------------                *         
      *                -    GESTION DES FICHIERS     -                *         
      *                -------------------------------                *         
      *                               -                               *         
      *           -----------------------------------------           *         
      *           -            -             -            -           *         
      * ---------------- ------------ ---------------- -------------- *         
      * - DETERMINATION- - CREATION - - MODIFICATION - - SUPRESSION - *         
      * ---------------- ------------ ---------------- -------------- *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       TRAITEMENT-FICHIER             SECTION.                                  
      *                                                                         
           PERFORM DETERMINATION-TRAITEMENT.                                    
      *                                                                         
           IF CREATION                                                          
              PERFORM CREATION-RECORD                                           
           END-IF.                                                              
      *                                                                         
           IF MODIFICATION                                                      
              PERFORM MODIFICATION-RECORD                                       
           END-IF.                                                              
      *                                                                         
           IF SUPPRESSION                                                       
              PERFORM SUPPRESSION-RECORD                                        
           END-IF.                                                              
      *                                                                         
       FIN-TRAITEMENT-FICHIER.        EXIT.                                     
       EJECT                                                                    
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * DETERMINATION DE L'ACTION *  XT09    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       DETERMINATION-TRAITEMENT       SECTION.                                  
      *                                                                         
      *    IF                                                                   
      *        MOVE CODE-CREATION TO ACTION.                                    
      *                                                                         
      *          ETC ETC                                                        
      *                                                                         
       FIN-DETER-TRAITEMENT.          EXIT.                                     
       EJECT                                                                    
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *  CREATION                 *  XT09    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       CREATION-RECORD                SECTION.                                  
      *                                                                         
           PERFORM INITIALISATION-FICHIER.                                      
      *                                                                         
           PERFORM RENSEIGNEMENT-FICHIER.                                       
      *                                                                         
      * VOUS DEVEZ INSERER PAR GAME LES ORDRES :                                
      *            INSERT POUR DL1                                              
      *            WRITE POUR VSAM                                              
      *                                                                         
           IF  EXISTE-DEJA                                                      
               MOVE ' XT09 CREATION RECORD EXISTE DEJA ' TO MESS                
               GO TO ABANDON-TACHE                                              
           END-IF.                                                              
      *                                                                         
       FIN-CREATION-RECORD.           EXIT.                                     
       EJECT                                                                    
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * INITIALISATION DU RECORD  *  XT09    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * SPACES DANS LES ZONES EN X  * ZERO DANS LES ZONES NUMERIQUES  *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       INITIALISATION-FICHIER         SECTION.                                  
      *                                                                         
      *                                                                         
       FIN-INITIALISATION-FICHIER.    EXIT.                                     
       EJECT                                                                    
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * SUPPRESSION               *  XT09    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       SUPPRESSION-RECORD              SECTION.                                 
      *                                                                         
           PERFORM RECUPERATION-FICHIER.                                        
      *                                                                         
      * VOUS DEVEZ INSERER PAR GAME LES ORDRES :                                
      *            DELETE VSAM OU DL1                                           
      *                                                                         
       FIN-SUPPRESSION-RECORD.         EXIT.                                    
       EJECT                                                                    
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * MODIFICATION              *  XT09    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODIFICATION-RECORD             SECTION.                                 
      *                                                                         
           PERFORM RECUPERATION-FICHIER.                                        
      *                                                                         
           PERFORM REECRITURE-FICHIER.                                          
      *                                                                         
       FIN-MODIFICATION-RECORD.        EXIT.                                    
       EJECT                                                                    
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * READ FOR UPDATE           *  XT09    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       RECUPERATION-FICHIER       SECTION.                                      
      *                                                                         
      * VOUS DEVEZ INSERER PAR GAME LES ORDRES :                                
      *            GHU POUR CALL DLI                                            
      *            GU  POUR EXEC DLI                                            
      *            READ UPDATE POUR VSAM                                        
      *                                                                         
           IF NON-TROUVE                                                        
              MOVE ' BIZARRE BIZARRE                  ' TO MESS                 
              GO TO ABANDON-TACHE                                               
           END-IF.                                                              
      *                                                                         
      *    IF TROUVE                                                            
      *       MOVE Z-INOUT TO XX01                                              
      *    END-IF.                                                              
      *                                                                         
       FIN-RECUPERATION-FICHIER.  EXIT.                                         
       EJECT                                                                    
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * REWRITE                   *  XT09    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       REECRITURE-FICHIER         SECTION.                                      
      *                                                                         
           PERFORM RENSEIGNEMENT-FICHIER.                                       
      *                                                                         
      * VOUS DEVEZ INSERER PAR GAME LES ORDRES :                                
      *            REPLACE POUR DLI OU VSAM                                     
      *                                                                         
       FIN-REECRITURE-FICHIER.    EXIT.                                         
       EJECT                                                                    
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * RENSEIGNEMENT RECORD      *  XT09    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       RENSEIGNEMENT-FICHIER      SECTION.                                      
      *                                                                         
      *    MOVE MXXXXXXI TO XX01-XXXXXX.                                        
      *                                                                         
       FIN-RENSEIGNEMENT-FICHIER. EXIT.                                         
       EJECT                                                                    
      *****************************************************************         
      *************** GESTION DE LA COMMAREA **************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * GESTION DE LA COMMAREA    *  XT09    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       TRAITEMENT-COMMAREA            SECTION.                                  
      *                                                                         
      *    MOVE XX01-XXXXXX       TO COM-XT00-XXXXXX.                           
      *                                                                         
       FIN-TRAITEMENT-COMMAREA.       EXIT.                                     
       EJECT                                                                    
      *****************************************************************         
      *************** GESTION DE LA MAP *******************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * GESTION DE LA MAP         *  XT09    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       TRAITEMENT-MAP                 SECTION.                                  
      *                                                                         
      *                                                                         
       FIN-TRAITEMENT-MAP.            EXIT.                                     
       EJECT                                                                    
      * -AIDA *********************************************************         
      *                                                               *         
      *  SSSSSSSS   OOOOOOO  RRRRRRRR  TTTTTTTTT IIIIIIIII EEEEEEEEE  *         
      *  SSSSSSSS  OOOOOOOOO RRRRRRRRR TTTTTTTTT IIIIIIIII EEEEEEEEE  *         
      *  SSSS      OOO   OOO RRR   RRR    TTT       III    EEE        *         
      *   SSSS     OOO   OOO RRR   RRR    TTT       III    EEE        *         
      *    SSSS    OOO   OOO RRR   RRR    TTT       III    EEEEEEEEE  *         
      *     SSSS   OOO   OOO RRRRRRRRR    TTT       III    EEEEEEEEE  *         
      *      SSSS  OOO   OOO RRRRRRRR     TTT       III    EEE        *         
      *      SSSS  OOO   OOO RRR   RR     TTT       III    EEE        *         
      *  SSSSSSSS  OOOOOOOOO RRR   RRR    TTT    IIIIIIIII EEEEEEEEE  *         
      *  SSSSSSSS   OOOOOOO  RRR   RRR    TTT    IIIIIIIII EEEEEEEEE  *         
      *                                                               *         
      *****************************************************************         
       EJECT                                                                    
      *****************************************************************         
      *                 MODULE DE SORTIE GENERALISE                   *         
      *****************************************************************         
      *                                                                         
       MODULE-SORTIE              SECTION.                                      
      *                                                                         
           IF  TRAITEMENT-AUTOMATIQUE                                           
                       PERFORM          SAVE-SWAP-ATTR                          
                       PERFORM          SORTIE-AFFICHAGE-FORMAT                 
           END-IF.                                                              
      *                                                                         
           IF  NOT OK                                                           
                       PERFORM          SAVE-SWAP-ATTR                          
                       PERFORM          SORTIE-ERREUR                           
           END-IF.                                                              
      *                                                                         
           IF  SWAP                                                             
                       PERFORM          REST-SWAP-ATTR                          
                       PERFORM          SORTIE-SWAP                             
           END-IF.                                                              
      *                                                                         
           IF  TRAITEMENT-NORMAL                                                
                       PERFORM          SORTIE-SUITE                            
           END-IF.                                                              
      *                                                                         
           IF  LEVEL-SUP                                                        
                       PERFORM          SORTIE-LEVEL-SUPERIEUR                  
           END-IF.                                                              
      *                                                                         
           IF  LEVEL-MAX OR JUMP                                                
                       PERFORM          SORTIE-LEVEL-MAX                        
           END-IF.                                                              
      *                                                                         
           IF  ERREUR-MANIPULATION                                              
                       PERFORM          REST-SWAP-ATTR                          
                       PERFORM          SORTIE-ERREUR-MANIP                     
           END-IF.                                                              
      *                                                                         
           IF  HELP                                                             
                       PERFORM          REST-SWAP-ATTR                          
                       PERFORM          SORTIE-HELP                             
           END-IF.                                                              
      *                                                                         
      * ABANDON * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
           MOVE 'ERREUR CODE FONCTION DANS MODULE SORTIE' TO MESS.              
           GO TO ABANDON-TACHE.                                                 
      *                                                                         
       FIN-MODULE-SORTIE.        EXIT.                                          
       EJECT                                                                    
      ****************************************************************          
      * SORTIE DU TRAITEMENT AUTOMATIQUE                             *          
      ****************************************************************          
      *                                                                         
       SORTIE-AFFICHAGE-FORMAT    SECTION.                                      
      *                                                                         
      *    POSITIONNEMENT OBLIGATOIRE DU CURSEUR.                               
           MOVE CURSEUR    TO  MONL.                                            
      *                                                                         
           PERFORM SEND-MAP.                                                    
      *                                                                         
           MOVE SPACES     TO  Z-COMMAREA-TACHE-JUMP.                           
           MOVE NOM-TACHE  TO  NOM-TACHE-RETOUR.                                
      *                                                                         
           PERFORM RETOUR-COMMAREA.                                             
      *                                                                         
       FIN-SORTIE-AFFICHAGE-FORMAT. EXIT.                                       
       EJECT                                                                    
      *****************************************************************         
      *  AFFICHAGE DE LA MAP EN ERREUR                                *         
      *                      EN CONFIRMATION                          *         
      *  ET RETURN AU MEME PROGRAMME                                  *         
      *****************************************************************         
      *                                                                         
       SORTIE-ERREUR              SECTION.                                      
      *                                                                         
           PERFORM SEND-MAP-SORTIE-ERREUR.                                      
      *                                                                         
           MOVE NOM-TACHE TO NOM-TACHE-RETOUR.                                  
      *                                                                         
           PERFORM RETOUR-COMMAREA.                                             
      *                                                                         
       FIN-SORTIE-ERREUR. EXIT.                                                 
       EJECT                                                                    
      *****************************************************************         
      *  AFFICHAGE DE LA MAP SUIVANTE ET RETURN AU PROGRAMME SUIVANT  *         
      *****************************************************************         
      *                                                                         
       SORTIE-SUITE               SECTION.                                      
      *                                                                         
           MOVE 'TXT01'   TO  NOM-PROG-XCTL.                                    
           MOVE NOM-PROG  TO  COM-PGMPRC.                                       
      *                                                                         
           PERFORM XCTL-PROG-COMMAREA.                                          
      *                                                                         
       FIN-SORTIE-SUITE. EXIT.                                                  
       EJECT                                                                    
      *****************************************************************         
      *    RETOUR APRES PF3 AU LEVEL SUPERIEUR                      *           
      *****************************************************************         
      *                                                                         
       SORTIE-LEVEL-SUPERIEUR     SECTION.                                      
      *                                                                         
           MOVE 'TXT00'   TO  NOM-PROG-XCTL.                                    
           MOVE NOM-PROG  TO  COM-PGMPRC.                                       
      *                                                                         
           PERFORM XCTL-PROG-COMMAREA.                                          
      *                                                                         
       FIN-SORTIE-LEVEL-SUPERIEUR. EXIT.                                        
       EJECT                                                                    
      *****************************************************************         
      *    RETOUR APRES PF4 AU MENU                                 *           
      *****************************************************************         
      *                                                                         
       SORTIE-LEVEL-MAX           SECTION.                                      
      *                                                                         
           MOVE 'TXT00'   TO  NOM-PROG-XCTL.                                    
           MOVE NOM-PROG  TO  COM-PGMPRC.                                       
      *                                                                         
           PERFORM XCTL-PROG-COMMAREA.                                          
      *                                                                         
       FIN-SORTIE-LEVEL-MAX. EXIT.                                              
       EJECT                                                                    
      *****************************************************************         
      *         SORTIE ERREUR MANIPULATION                            *         
      *****************************************************************         
      *                                                                         
       SORTIE-ERREUR-MANIP        SECTION.                                      
      *                                                                         
           MOVE 'K017 : ERREUR MANIPULATION' TO MLIBERRI.                       
           MOVE 'K017'                       TO COM-CODERR.                     
      *                                                                         
           MOVE CURSEUR                      TO MONL.                           
      *                                                                         
           PERFORM SEND-MAP-ERREUR-MANIP.                                       
      *                                                                         
           MOVE NOM-TACHE TO NOM-TACHE-RETOUR.                                  
      *                                                                         
           PERFORM RETOUR-COMMAREA.                                             
      *                                                                         
       FIN-SORTIE-ERREUR-MANIP. EXIT.                                           
       EJECT                                                                    
      * DARTY ********************************************************          
      *       * SAUVEGARDE DES ATTRIBUTS BMS EN CAS DE SWAP          *          
      ****************************************************************          
      *                                                                         
       SAVE-SWAP-ATTR             SECTION.                                      
      *                                                                         
      *    MOVE MXXXXXA     TO COMM-SWAP-ATTR (001).                            
      *    MOVE MXXXXXC     TO COMM-SWAP-ATTR (002).                            
      *    MOVE MXXXXXH     TO COMM-SWAP-ATTR (003).                            
      *                                                                         
      *    MOVE MZZZZZA     TO COMM-SWAP-ATTR (148).                            
      *    MOVE MZZZZZC     TO COMM-SWAP-ATTR (149).                            
      *    MOVE MZZZZZH     TO COMM-SWAP-ATTR (150).                            
      *                                                                         
       FIN-SAVE-SWAP-ATTR.      EXIT.                                           
       EJECT                                                                    
      * DARTY ********************************************************          
      *       * RESTAURATION DES ATTRIBUTS BMS EN CAS DE SWAP        *          
      ****************************************************************          
      *                                                                         
       REST-SWAP-ATTR             SECTION.                                      
      *                                                                         
      *                                                                         
           MOVE EIBCPOSN             TO COMM-SWAP-CURS.                         
      *                                                                         
      *    MOVE COMM-SWAP-ATTR (001) TO MXXXXXA.                                
      *    MOVE COMM-SWAP-ATTR (002) TO MXXXXXC.                                
      *    MOVE COMM-SWAP-ATTR (003) TO MXXXXXH.                                
      *                                                                         
      *    MOVE COMM-SWAP-ATTR (148) TO MZZZZZA.                                
      *    MOVE COMM-SWAP-ATTR (149) TO MZZZZZC.                                
      *    MOVE COMM-SWAP-ATTR (150) TO MZZZZZH.                                
      *                                                                         
       FIN-REST-SWAP-ATTR.      EXIT.                                           
       EJECT                                                                    
      * AIDA  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *        DESCRIPTION DES BRIQUES AIDA CICS          * SYKC....  *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
       S01-COPY SECTION. CONTINUE. COPY SYKCHELP.                               
       S02-COPY SECTION. CONTINUE. COPY SYKCRTCO.                               
       S03-COPY SECTION. CONTINUE. COPY SYKCRTNO.                               
       S04-COPY SECTION. CONTINUE. COPY SYKCRT00.                               
       S05-COPY SECTION. CONTINUE. COPY SYKCSMDO.                               
       S06-COPY SECTION. CONTINUE. COPY SYKCSMEM.                               
       S07-COPY SECTION. CONTINUE. COPY SYKCSMER.                               
       S08-COPY SECTION. CONTINUE. COPY SYKCSMNO.                               
       S09-COPY SECTION. CONTINUE. COPY SYKCSMSE.                               
       S10-COPY SECTION. CONTINUE. COPY SYKCSM00.                               
       S11-COPY SECTION. CONTINUE. COPY SYKCSPAG.                               
       S12-COPY SECTION. CONTINUE. COPY SYKCST00.                               
       S13-COPY SECTION. CONTINUE. COPY SYKCSTRT.                               
       S14-COPY SECTION. CONTINUE. COPY SYKCXCTL.                               
       S15-COPY SECTION. CONTINUE. COPY SYKCLINK.                               
       S16-COPY SECTION. CONTINUE. COPY SYKCSWAP.                               
       S17-COPY SECTION. CONTINUE. COPY SYKXSWAP.                               
       S18-COPY SECTION. CONTINUE. COPY SYKCRETR.                               
       EJECT                                                                    
      * -AIDA *********************************************************         
      *        MODULES DE CONTROLES ET DE TRAITEMENTS STANDARDISES              
      *****************************************************************         
       T01-COPY SECTION. CONTINUE. COPY SYKPAGE0.                               
       T02-COPY SECTION. CONTINUE. COPY SYKPDATE.                               
       T03-COPY SECTION. CONTINUE. COPY SYKPDECI.                               
       T04-COPY SECTION. CONTINUE. COPY SYKPVALN.                               
       T05-COPY SECTION. CONTINUE. COPY SYKPTIME.                               
       T06-COPY SECTION. CONTINUE. COPY SYKPMONT.                               
       99-COPY SECTION. CONTINUE. COPY SYKCERRO.                                
