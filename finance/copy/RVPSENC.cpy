      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PSENC GESTION AUTO PSE EXCLSN CODICS   *        
      *----------------------------------------------------------------*        
       01  RVPSENC.                                                             
           05  PSENC-CTABLEG2    PIC X(15).                                     
           05  PSENC-CTABLEG2-REDEF REDEFINES PSENC-CTABLEG2.                   
               10  PSENC-NCODIC          PIC X(07).                             
           05  PSENC-WTABLEG     PIC X(80).                                     
           05  PSENC-WTABLEG-REDEF  REDEFINES PSENC-WTABLEG.                    
               10  PSENC-WACTIF          PIC X(01).                             
               10  PSENC-LCOMMENT        PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVPSENC-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PSENC-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PSENC-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PSENC-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PSENC-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
