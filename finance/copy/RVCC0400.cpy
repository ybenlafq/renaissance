      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVCC0400                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVCC0400                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVCC0400.                                                            
           02  CC04-NSOCIETE             PIC X(0003).                           
           02  CC04-NLIEU                PIC X(0003).                           
           02  CC04-DCAISSE              PIC X(0008).                           
           02  CC04-NCAISSE              PIC X(0003).                           
           02  CC04-CPAICPT              PIC X(0003).                           
           02  CC04-MTLOG                PIC S9(13)V9(0002) COMP-3.             
           02  CC04-MTSAISI              PIC S9(13)V9(0002) COMP-3.             
           02  CC04-MTOD                 PIC S9(13)V9(0002) COMP-3.             
           02  CC04-LCOMMENT             PIC X(0020).                           
           02  CC04-DVALID               PIC X(0008).                           
           02  CC04-DSYST                PIC S9(13)         COMP-3.             
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVCC0400                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVCC0400-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC04-NSOCIETE-F           PIC S9(4) COMP.                        
      *--                                                                       
           02  CC04-NSOCIETE-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC04-NLIEU-F              PIC S9(4) COMP.                        
      *--                                                                       
           02  CC04-NLIEU-F              PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC04-DCAISSE-F            PIC S9(4) COMP.                        
      *--                                                                       
           02  CC04-DCAISSE-F            PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC04-NCAISSE-F            PIC S9(4) COMP.                        
      *--                                                                       
           02  CC04-NCAISSE-F            PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC04-CPAICPT-F            PIC S9(4) COMP.                        
      *--                                                                       
           02  CC04-CPAICPT-F            PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC04-MTLOG-F              PIC S9(4) COMP.                        
      *--                                                                       
           02  CC04-MTLOG-F              PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC04-MTSAISI-F            PIC S9(4) COMP.                        
      *--                                                                       
           02  CC04-MTSAISI-F            PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC04-MTOD-F               PIC S9(4) COMP.                        
      *--                                                                       
           02  CC04-MTOD-F               PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC04-LCOMMENT-F           PIC S9(4) COMP.                        
      *--                                                                       
           02  CC04-LCOMMENT-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC04-DVALID-F             PIC S9(4) COMP.                        
      *--                                                                       
           02  CC04-DVALID-F             PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC04-DSYST-F              PIC S9(4) COMP.                        
      *                                                                         
      *--                                                                       
           02  CC04-DSYST-F              PIC S9(4) COMP-5.                      
                                                                                
      *}                                                                        
