      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SAP - REGLES D'ETALEMENT                                        00000020
      ***************************************************************** 00000030
       01   EFM93I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(2).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINTERFACEL   COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MCINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MCINTERFACEF   PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MCINTERFACEI   PIC X(5).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINTERFACEL   COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MLINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLINTERFACEF   PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLINTERFACEI   PIC X(30).                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MCTYPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCTYPF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MCTYPI    PIC X(5).                                       00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCNATL    COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MCNATL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCNATF    PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MCNATI    PIC X(5).                                       00000390
           02 MTABI OCCURS   11 TIMES .                                 00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000410
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MSELI   PIC X.                                          00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPOPERL   COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MCTYPOPERL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCTYPOPERF   PIC X.                                     00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MCTYPOPERI   PIC X(5).                                  00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCNATOPERL   COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MCNATOPERL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCNATOPERF   PIC X.                                     00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MCNATOPERI   PIC X(5).                                  00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPOIDS1L     COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MPOIDS1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPOIDS1F     PIC X.                                     00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MPOIDS1I     PIC X(3).                                  00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPOIDS2L     COMP PIC S9(4).                            00000570
      *--                                                                       
             03 MPOIDS2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPOIDS2F     PIC X.                                     00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MPOIDS2I     PIC X(3).                                  00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPOIDS3L     COMP PIC S9(4).                            00000610
      *--                                                                       
             03 MPOIDS3L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPOIDS3F     PIC X.                                     00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MPOIDS3I     PIC X(3).                                  00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPOIDS4L     COMP PIC S9(4).                            00000650
      *--                                                                       
             03 MPOIDS4L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPOIDS4F     PIC X.                                     00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MPOIDS4I     PIC X(3).                                  00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPOIDS5L     COMP PIC S9(4).                            00000690
      *--                                                                       
             03 MPOIDS5L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPOIDS5F     PIC X.                                     00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MPOIDS5I     PIC X(3).                                  00000720
      * ZONE CMD AIDA                                                   00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLIBERRI  PIC X(79).                                      00000770
      * CODE TRANSACTION                                                00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      * CICS DE TRAVAIL                                                 00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MCICSI    PIC X(5).                                       00000870
      * NETNAME                                                         00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000890
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000900
           02 FILLER    PIC X(4).                                       00000910
           02 MNETNAMI  PIC X(8).                                       00000920
      * CODE TERMINAL                                                   00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MSCREENI  PIC X(4).                                       00000970
      ***************************************************************** 00000980
      * SAP - REGLES D'ETALEMENT                                        00000990
      ***************************************************************** 00001000
       01   EFM93O REDEFINES EFM93I.                                    00001010
           02 FILLER    PIC X(12).                                      00001020
      * DATE DU JOUR                                                    00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MDATJOUA  PIC X.                                          00001050
           02 MDATJOUC  PIC X.                                          00001060
           02 MDATJOUP  PIC X.                                          00001070
           02 MDATJOUH  PIC X.                                          00001080
           02 MDATJOUV  PIC X.                                          00001090
           02 MDATJOUO  PIC X(10).                                      00001100
      * HEURE                                                           00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MTIMJOUA  PIC X.                                          00001130
           02 MTIMJOUC  PIC X.                                          00001140
           02 MTIMJOUP  PIC X.                                          00001150
           02 MTIMJOUH  PIC X.                                          00001160
           02 MTIMJOUV  PIC X.                                          00001170
           02 MTIMJOUO  PIC X(5).                                       00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MPAGEA    PIC X.                                          00001200
           02 MPAGEC    PIC X.                                          00001210
           02 MPAGEP    PIC X.                                          00001220
           02 MPAGEH    PIC X.                                          00001230
           02 MPAGEV    PIC X.                                          00001240
           02 MPAGEO    PIC Z9.                                         00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MNBPA     PIC X.                                          00001270
           02 MNBPC     PIC X.                                          00001280
           02 MNBPP     PIC X.                                          00001290
           02 MNBPH     PIC X.                                          00001300
           02 MNBPV     PIC X.                                          00001310
           02 MNBPO     PIC Z9.                                         00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MCINTERFACEA   PIC X.                                     00001340
           02 MCINTERFACEC   PIC X.                                     00001350
           02 MCINTERFACEP   PIC X.                                     00001360
           02 MCINTERFACEH   PIC X.                                     00001370
           02 MCINTERFACEV   PIC X.                                     00001380
           02 MCINTERFACEO   PIC X(5).                                  00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MLINTERFACEA   PIC X.                                     00001410
           02 MLINTERFACEC   PIC X.                                     00001420
           02 MLINTERFACEP   PIC X.                                     00001430
           02 MLINTERFACEH   PIC X.                                     00001440
           02 MLINTERFACEV   PIC X.                                     00001450
           02 MLINTERFACEO   PIC X(30).                                 00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MCTYPA    PIC X.                                          00001480
           02 MCTYPC    PIC X.                                          00001490
           02 MCTYPP    PIC X.                                          00001500
           02 MCTYPH    PIC X.                                          00001510
           02 MCTYPV    PIC X.                                          00001520
           02 MCTYPO    PIC X(5).                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MCNATA    PIC X.                                          00001550
           02 MCNATC    PIC X.                                          00001560
           02 MCNATP    PIC X.                                          00001570
           02 MCNATH    PIC X.                                          00001580
           02 MCNATV    PIC X.                                          00001590
           02 MCNATO    PIC X(5).                                       00001600
           02 MTABO OCCURS   11 TIMES .                                 00001610
             03 FILLER       PIC X(2).                                  00001620
             03 MSELA   PIC X.                                          00001630
             03 MSELC   PIC X.                                          00001640
             03 MSELP   PIC X.                                          00001650
             03 MSELH   PIC X.                                          00001660
             03 MSELV   PIC X.                                          00001670
             03 MSELO   PIC X.                                          00001680
             03 FILLER       PIC X(2).                                  00001690
             03 MCTYPOPERA   PIC X.                                     00001700
             03 MCTYPOPERC   PIC X.                                     00001710
             03 MCTYPOPERP   PIC X.                                     00001720
             03 MCTYPOPERH   PIC X.                                     00001730
             03 MCTYPOPERV   PIC X.                                     00001740
             03 MCTYPOPERO   PIC X(5).                                  00001750
             03 FILLER       PIC X(2).                                  00001760
             03 MCNATOPERA   PIC X.                                     00001770
             03 MCNATOPERC   PIC X.                                     00001780
             03 MCNATOPERP   PIC X.                                     00001790
             03 MCNATOPERH   PIC X.                                     00001800
             03 MCNATOPERV   PIC X.                                     00001810
             03 MCNATOPERO   PIC X(5).                                  00001820
             03 FILLER       PIC X(2).                                  00001830
             03 MPOIDS1A     PIC X.                                     00001840
             03 MPOIDS1C     PIC X.                                     00001850
             03 MPOIDS1P     PIC X.                                     00001860
             03 MPOIDS1H     PIC X.                                     00001870
             03 MPOIDS1V     PIC X.                                     00001880
             03 MPOIDS1O     PIC X(3).                                  00001890
             03 FILLER       PIC X(2).                                  00001900
             03 MPOIDS2A     PIC X.                                     00001910
             03 MPOIDS2C     PIC X.                                     00001920
             03 MPOIDS2P     PIC X.                                     00001930
             03 MPOIDS2H     PIC X.                                     00001940
             03 MPOIDS2V     PIC X.                                     00001950
             03 MPOIDS2O     PIC X(3).                                  00001960
             03 FILLER       PIC X(2).                                  00001970
             03 MPOIDS3A     PIC X.                                     00001980
             03 MPOIDS3C     PIC X.                                     00001990
             03 MPOIDS3P     PIC X.                                     00002000
             03 MPOIDS3H     PIC X.                                     00002010
             03 MPOIDS3V     PIC X.                                     00002020
             03 MPOIDS3O     PIC X(3).                                  00002030
             03 FILLER       PIC X(2).                                  00002040
             03 MPOIDS4A     PIC X.                                     00002050
             03 MPOIDS4C     PIC X.                                     00002060
             03 MPOIDS4P     PIC X.                                     00002070
             03 MPOIDS4H     PIC X.                                     00002080
             03 MPOIDS4V     PIC X.                                     00002090
             03 MPOIDS4O     PIC X(3).                                  00002100
             03 FILLER       PIC X(2).                                  00002110
             03 MPOIDS5A     PIC X.                                     00002120
             03 MPOIDS5C     PIC X.                                     00002130
             03 MPOIDS5P     PIC X.                                     00002140
             03 MPOIDS5H     PIC X.                                     00002150
             03 MPOIDS5V     PIC X.                                     00002160
             03 MPOIDS5O     PIC X(3).                                  00002170
      * ZONE CMD AIDA                                                   00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MLIBERRA  PIC X.                                          00002200
           02 MLIBERRC  PIC X.                                          00002210
           02 MLIBERRP  PIC X.                                          00002220
           02 MLIBERRH  PIC X.                                          00002230
           02 MLIBERRV  PIC X.                                          00002240
           02 MLIBERRO  PIC X(79).                                      00002250
      * CODE TRANSACTION                                                00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCODTRAA  PIC X.                                          00002280
           02 MCODTRAC  PIC X.                                          00002290
           02 MCODTRAP  PIC X.                                          00002300
           02 MCODTRAH  PIC X.                                          00002310
           02 MCODTRAV  PIC X.                                          00002320
           02 MCODTRAO  PIC X(4).                                       00002330
      * CICS DE TRAVAIL                                                 00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MCICSA    PIC X.                                          00002360
           02 MCICSC    PIC X.                                          00002370
           02 MCICSP    PIC X.                                          00002380
           02 MCICSH    PIC X.                                          00002390
           02 MCICSV    PIC X.                                          00002400
           02 MCICSO    PIC X(5).                                       00002410
      * NETNAME                                                         00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MNETNAMA  PIC X.                                          00002440
           02 MNETNAMC  PIC X.                                          00002450
           02 MNETNAMP  PIC X.                                          00002460
           02 MNETNAMH  PIC X.                                          00002470
           02 MNETNAMV  PIC X.                                          00002480
           02 MNETNAMO  PIC X(8).                                       00002490
      * CODE TERMINAL                                                   00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MSCREENA  PIC X.                                          00002520
           02 MSCREENC  PIC X.                                          00002530
           02 MSCREENP  PIC X.                                          00002540
           02 MSCREENH  PIC X.                                          00002550
           02 MSCREENV  PIC X.                                          00002560
           02 MSCREENO  PIC X(4).                                       00002570
                                                                                
