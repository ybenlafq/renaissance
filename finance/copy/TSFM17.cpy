      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
AM     01  TS-FM17-LONG             PIC S9(4) COMP-3 VALUE +138.                
       01  TS-FM17-RECORD.                                                      
           10 TS-FM17-NDEMANDE      PIC X(2).                                   
           10 TS-FM17-DCREATION     PIC X(10).                                  
           10 TS-FM17-CACID         PIC X(8).                                   
           10 TS-FM17-USER          PIC X(25).                                  
           10 TS-FM17-WMFICHE       PIC X.                                      
           10 TS-FM17-WPAPIER       PIC X.                                      
           10 TS-FM17-NETAB         PIC X(3).                                   
AM         10 TS-FM17-NAUXMIN       PIC X(3).                                   
AM         10 TS-FM17-NAUXMAX       PIC X(3).                                   
AM         10 TS-FM17-WTYPAUX       PIC X.                                      
           10 TS-FM17-NCOMPTEMIN    PIC X(6).                                   
           10 TS-FM17-NCOMPTEMAX    PIC X(6).                                   
           10 TS-FM17-WTOTAL        PIC X    OCCURS 3.                          
           10 TS-FM17-WNTRI         PIC X    OCCURS 3.                          
           10 TS-FM17-CNAT          PIC X(3) OCCURS 10.                         
           10 TS-FM17-CNATE         PIC X(3) OCCURS 10.                         
           10 TS-FM17-CDEVISE       PIC X(3).                                   
                                                                                
