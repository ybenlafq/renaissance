      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IFF039 AU 01/06/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,01,BI,A,                          *        
      *                           14,09,BI,A,                          *        
      *                           23,06,BI,A,                          *        
      *                           29,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IFF039.                                                        
            05 NOMETAT-IFF039           PIC X(6) VALUE 'IFF039'.                
            05 RUPTURES-IFF039.                                                 
           10 IFF039-SOCDEST            PIC X(03).                      007  003
           10 IFF039-DEVISE             PIC X(03).                      010  003
           10 IFF039-NAUX               PIC X(01).                      013  001
           10 IFF039-NTIERS             PIC X(09).                      014  009
           10 IFF039-NPIECE             PIC X(06).                      023  006
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IFF039-SEQUENCE           PIC S9(04) COMP.                029  002
      *--                                                                       
           10 IFF039-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IFF039.                                                   
           10 IFF039-DCPT               PIC X(10).                      031  010
           10 IFF039-DEVISE-TOT         PIC X(03).                      041  003
           10 IFF039-DFINPER            PIC X(10).                      044  010
           10 IFF039-LAUX               PIC X(03).                      054  003
           10 IFF039-LAUX-TOT           PIC X(03).                      057  003
           10 IFF039-LIBELLE            PIC X(30).                      060  030
           10 IFF039-LIBFOUR            PIC X(30).                      090  030
           10 IFF039-LIBFOUR-TOT        PIC X(30).                      120  030
           10 IFF039-MCIVIL             PIC X(14).                      150  014
           10 IFF039-NFACT              PIC X(16).                      164  016
           10 IFF039-NSOC               PIC X(03).                      180  003
           10 IFF039-WTYPEDOC           PIC X(02).                      183  002
           10 IFF039-TXREMISE           PIC 9(03)V9(2).                 185  005
           10 IFF039-MONTANT-NRIS       PIC S9(11)V9(2) COMP-3.         190  007
           10 IFF039-MONTANT-RIS        PIC S9(11)V9(2) COMP-3.         197  007
            05 FILLER                      PIC X(309).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IFF039-LONG           PIC S9(4)   COMP  VALUE +203.           
      *                                                                         
      *--                                                                       
        01  DSECT-IFF039-LONG           PIC S9(4) COMP-5  VALUE +203.           
                                                                                
      *}                                                                        
