      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TFM21                                          *  00020000
      *       TR : FM21  GESTION PARAMETRAGE MODELE GCT              *  00030000
      *       PG : TFM21 CREATION/MODIFICATION DES ENTITE MODELES    *  00040000
      ****************************************************************  00050000
      *                                                                 00060000
      *------------------------------ LONGUEUR                          00070000
       01  TS-FM21-LONG         PIC S9(5) COMP-3 VALUE 1056.            00080006
       01  TS-FM21-DONNEES.                                             00090000
           05 TS-FM21-LIGNE OCCURS 12.                                          
              10 TS-FM21-CPLAN      PIC X(04).                          00150000
              10 TS-FM21-LPLAN      PIC X(20).                          00150000
              10 TS-FM21-OCC OCCURS 12.                                         
                 15 TS-FM21-NENTITE  PIC X(05).                         00150000
              10 TS-FM21-NBENTITE    PIC 9(02).                         00150000
      *-------FLAG -----------------------------------                  00070000
              10 TS-FM21-FLAG      PIC X.                               00150000
              10 TS-FM21-SEL       PIC X.                               00150000
                                                                                
