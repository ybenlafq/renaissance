      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SAP - REGLES D'ETALEMENT                                        00000020
      ***************************************************************** 00000030
       01   EFM92I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(2).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINTERFACEL   COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MCINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MCINTERFACEF   PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MCINTERFACEI   PIC X(5).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINTERFACEL   COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MLINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLINTERFACEF   PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLINTERFACEI   PIC X(30).                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MCTYPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCTYPF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MCTYPI    PIC X(5).                                       00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCNATL    COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MCNATL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCNATF    PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MCNATI    PIC X(5).                                       00000390
           02 MTABI OCCURS   11 TIMES .                                 00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000410
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MSELI   PIC X.                                          00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPOPERL   COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MCTYPOPERL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCTYPOPERF   PIC X.                                     00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MCTYPOPERI   PIC X(5).                                  00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCNATOPERL   COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MCNATOPERL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCNATOPERF   PIC X.                                     00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MCNATOPERI   PIC X(5).                                  00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMONTANTL    COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MMONTANTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MMONTANTF    PIC X.                                     00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MMONTANTI    PIC X(3).                                  00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDUREEL      COMP PIC S9(4).                            00000570
      *--                                                                       
             03 MDUREEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDUREEF      PIC X.                                     00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MDUREEI      PIC X(2).                                  00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDELAIL      COMP PIC S9(4).                            00000610
      *--                                                                       
             03 MDELAIL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDELAIF      PIC X.                                     00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MDELAII      PIC X(2).                                  00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWPROL  COMP PIC S9(4).                                 00000650
      *--                                                                       
             03 MWPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MWPROF  PIC X.                                          00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MWPROI  PIC X.                                          00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWPOIDSL     COMP PIC S9(4).                            00000690
      *--                                                                       
             03 MWPOIDSL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWPOIDSF     PIC X.                                     00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MWPOIDSI     PIC X.                                     00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MINFO1L      COMP PIC S9(4).                            00000730
      *--                                                                       
             03 MINFO1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MINFO1F      PIC X.                                     00000740
             03 FILLER  PIC X(4).                                       00000750
             03 MINFO1I      PIC X(2).                                  00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MINFO2L      COMP PIC S9(4).                            00000770
      *--                                                                       
             03 MINFO2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MINFO2F      PIC X.                                     00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MINFO2I      PIC X(2).                                  00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MINFO3L      COMP PIC S9(4).                            00000810
      *--                                                                       
             03 MINFO3L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MINFO3F      PIC X.                                     00000820
             03 FILLER  PIC X(4).                                       00000830
             03 MINFO3I      PIC X(2).                                  00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00000850
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00000860
             03 FILLER  PIC X(4).                                       00000870
             03 MDEFFETI     PIC X(10).                                 00000880
      * ZONE CMD AIDA                                                   00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MLIBERRI  PIC X(79).                                      00000930
      * CODE TRANSACTION                                                00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCODTRAI  PIC X(4).                                       00000980
      * CICS DE TRAVAIL                                                 00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MCICSI    PIC X(5).                                       00001030
      * NETNAME                                                         00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001050
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001060
           02 FILLER    PIC X(4).                                       00001070
           02 MNETNAMI  PIC X(8).                                       00001080
      * CODE TERMINAL                                                   00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MSCREENI  PIC X(4).                                       00001130
      ***************************************************************** 00001140
      * SAP - REGLES D'ETALEMENT                                        00001150
      ***************************************************************** 00001160
       01   EFM92O REDEFINES EFM92I.                                    00001170
           02 FILLER    PIC X(12).                                      00001180
      * DATE DU JOUR                                                    00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
      * HEURE                                                           00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MTIMJOUA  PIC X.                                          00001290
           02 MTIMJOUC  PIC X.                                          00001300
           02 MTIMJOUP  PIC X.                                          00001310
           02 MTIMJOUH  PIC X.                                          00001320
           02 MTIMJOUV  PIC X.                                          00001330
           02 MTIMJOUO  PIC X(5).                                       00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MPAGEA    PIC X.                                          00001360
           02 MPAGEC    PIC X.                                          00001370
           02 MPAGEP    PIC X.                                          00001380
           02 MPAGEH    PIC X.                                          00001390
           02 MPAGEV    PIC X.                                          00001400
           02 MPAGEO    PIC Z9.                                         00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MNBPA     PIC X.                                          00001430
           02 MNBPC     PIC X.                                          00001440
           02 MNBPP     PIC X.                                          00001450
           02 MNBPH     PIC X.                                          00001460
           02 MNBPV     PIC X.                                          00001470
           02 MNBPO     PIC Z9.                                         00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MCINTERFACEA   PIC X.                                     00001500
           02 MCINTERFACEC   PIC X.                                     00001510
           02 MCINTERFACEP   PIC X.                                     00001520
           02 MCINTERFACEH   PIC X.                                     00001530
           02 MCINTERFACEV   PIC X.                                     00001540
           02 MCINTERFACEO   PIC X(5).                                  00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MLINTERFACEA   PIC X.                                     00001570
           02 MLINTERFACEC   PIC X.                                     00001580
           02 MLINTERFACEP   PIC X.                                     00001590
           02 MLINTERFACEH   PIC X.                                     00001600
           02 MLINTERFACEV   PIC X.                                     00001610
           02 MLINTERFACEO   PIC X(30).                                 00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MCTYPA    PIC X.                                          00001640
           02 MCTYPC    PIC X.                                          00001650
           02 MCTYPP    PIC X.                                          00001660
           02 MCTYPH    PIC X.                                          00001670
           02 MCTYPV    PIC X.                                          00001680
           02 MCTYPO    PIC X(5).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MCNATA    PIC X.                                          00001710
           02 MCNATC    PIC X.                                          00001720
           02 MCNATP    PIC X.                                          00001730
           02 MCNATH    PIC X.                                          00001740
           02 MCNATV    PIC X.                                          00001750
           02 MCNATO    PIC X(5).                                       00001760
           02 MTABO OCCURS   11 TIMES .                                 00001770
             03 FILLER       PIC X(2).                                  00001780
             03 MSELA   PIC X.                                          00001790
             03 MSELC   PIC X.                                          00001800
             03 MSELP   PIC X.                                          00001810
             03 MSELH   PIC X.                                          00001820
             03 MSELV   PIC X.                                          00001830
             03 MSELO   PIC X.                                          00001840
             03 FILLER       PIC X(2).                                  00001850
             03 MCTYPOPERA   PIC X.                                     00001860
             03 MCTYPOPERC   PIC X.                                     00001870
             03 MCTYPOPERP   PIC X.                                     00001880
             03 MCTYPOPERH   PIC X.                                     00001890
             03 MCTYPOPERV   PIC X.                                     00001900
             03 MCTYPOPERO   PIC X(5).                                  00001910
             03 FILLER       PIC X(2).                                  00001920
             03 MCNATOPERA   PIC X.                                     00001930
             03 MCNATOPERC   PIC X.                                     00001940
             03 MCNATOPERP   PIC X.                                     00001950
             03 MCNATOPERH   PIC X.                                     00001960
             03 MCNATOPERV   PIC X.                                     00001970
             03 MCNATOPERO   PIC X(5).                                  00001980
             03 FILLER       PIC X(2).                                  00001990
             03 MMONTANTA    PIC X.                                     00002000
             03 MMONTANTC    PIC X.                                     00002010
             03 MMONTANTP    PIC X.                                     00002020
             03 MMONTANTH    PIC X.                                     00002030
             03 MMONTANTV    PIC X.                                     00002040
             03 MMONTANTO    PIC X(3).                                  00002050
             03 FILLER       PIC X(2).                                  00002060
             03 MDUREEA      PIC X.                                     00002070
             03 MDUREEC PIC X.                                          00002080
             03 MDUREEP PIC X.                                          00002090
             03 MDUREEH PIC X.                                          00002100
             03 MDUREEV PIC X.                                          00002110
             03 MDUREEO      PIC X(2).                                  00002120
             03 FILLER       PIC X(2).                                  00002130
             03 MDELAIA      PIC X.                                     00002140
             03 MDELAIC PIC X.                                          00002150
             03 MDELAIP PIC X.                                          00002160
             03 MDELAIH PIC X.                                          00002170
             03 MDELAIV PIC X.                                          00002180
             03 MDELAIO      PIC X(2).                                  00002190
             03 FILLER       PIC X(2).                                  00002200
             03 MWPROA  PIC X.                                          00002210
             03 MWPROC  PIC X.                                          00002220
             03 MWPROP  PIC X.                                          00002230
             03 MWPROH  PIC X.                                          00002240
             03 MWPROV  PIC X.                                          00002250
             03 MWPROO  PIC X.                                          00002260
             03 FILLER       PIC X(2).                                  00002270
             03 MWPOIDSA     PIC X.                                     00002280
             03 MWPOIDSC     PIC X.                                     00002290
             03 MWPOIDSP     PIC X.                                     00002300
             03 MWPOIDSH     PIC X.                                     00002310
             03 MWPOIDSV     PIC X.                                     00002320
             03 MWPOIDSO     PIC X.                                     00002330
             03 FILLER       PIC X(2).                                  00002340
             03 MINFO1A      PIC X.                                     00002350
             03 MINFO1C PIC X.                                          00002360
             03 MINFO1P PIC X.                                          00002370
             03 MINFO1H PIC X.                                          00002380
             03 MINFO1V PIC X.                                          00002390
             03 MINFO1O      PIC X(2).                                  00002400
             03 FILLER       PIC X(2).                                  00002410
             03 MINFO2A      PIC X.                                     00002420
             03 MINFO2C PIC X.                                          00002430
             03 MINFO2P PIC X.                                          00002440
             03 MINFO2H PIC X.                                          00002450
             03 MINFO2V PIC X.                                          00002460
             03 MINFO2O      PIC X(2).                                  00002470
             03 FILLER       PIC X(2).                                  00002480
             03 MINFO3A      PIC X.                                     00002490
             03 MINFO3C PIC X.                                          00002500
             03 MINFO3P PIC X.                                          00002510
             03 MINFO3H PIC X.                                          00002520
             03 MINFO3V PIC X.                                          00002530
             03 MINFO3O      PIC X(2).                                  00002540
             03 FILLER       PIC X(2).                                  00002550
             03 MDEFFETA     PIC X.                                     00002560
             03 MDEFFETC     PIC X.                                     00002570
             03 MDEFFETP     PIC X.                                     00002580
             03 MDEFFETH     PIC X.                                     00002590
             03 MDEFFETV     PIC X.                                     00002600
             03 MDEFFETO     PIC X(10).                                 00002610
      * ZONE CMD AIDA                                                   00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MLIBERRA  PIC X.                                          00002640
           02 MLIBERRC  PIC X.                                          00002650
           02 MLIBERRP  PIC X.                                          00002660
           02 MLIBERRH  PIC X.                                          00002670
           02 MLIBERRV  PIC X.                                          00002680
           02 MLIBERRO  PIC X(79).                                      00002690
      * CODE TRANSACTION                                                00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MCODTRAA  PIC X.                                          00002720
           02 MCODTRAC  PIC X.                                          00002730
           02 MCODTRAP  PIC X.                                          00002740
           02 MCODTRAH  PIC X.                                          00002750
           02 MCODTRAV  PIC X.                                          00002760
           02 MCODTRAO  PIC X(4).                                       00002770
      * CICS DE TRAVAIL                                                 00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MCICSA    PIC X.                                          00002800
           02 MCICSC    PIC X.                                          00002810
           02 MCICSP    PIC X.                                          00002820
           02 MCICSH    PIC X.                                          00002830
           02 MCICSV    PIC X.                                          00002840
           02 MCICSO    PIC X(5).                                       00002850
      * NETNAME                                                         00002860
           02 FILLER    PIC X(2).                                       00002870
           02 MNETNAMA  PIC X.                                          00002880
           02 MNETNAMC  PIC X.                                          00002890
           02 MNETNAMP  PIC X.                                          00002900
           02 MNETNAMH  PIC X.                                          00002910
           02 MNETNAMV  PIC X.                                          00002920
           02 MNETNAMO  PIC X(8).                                       00002930
      * CODE TERMINAL                                                   00002940
           02 FILLER    PIC X(2).                                       00002950
           02 MSCREENA  PIC X.                                          00002960
           02 MSCREENC  PIC X.                                          00002970
           02 MSCREENP  PIC X.                                          00002980
           02 MSCREENH  PIC X.                                          00002990
           02 MSCREENV  PIC X.                                          00003000
           02 MSCREENO  PIC X(4).                                       00003010
                                                                                
