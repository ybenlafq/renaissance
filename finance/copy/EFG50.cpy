      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFG10   EFG10                                              00000020
      ***************************************************************** 00000030
       01   EFG50I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCCL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MSOCCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSOCCF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MSOCCI    PIC X(3).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCCL   COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MLSOCCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLSOCCF   PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MLSOCCI   PIC X(24).                                      00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNSOCI    PIC X(3).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MNLIEUI   PIC X(3).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MLLIEUI   PIC X(24).                                      00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPEL   COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MCTYPEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCTYPEF   PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MCTYPEI   PIC X(2).                                       00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPEL   COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MLTYPEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLTYPEF   PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MLTYPEI   PIC X(30).                                      00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNATUREL  COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MNATUREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNATUREF  PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MNATUREI  PIC X(5).                                       00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNATUREL      COMP PIC S9(4).                            00000480
      *--                                                                       
           02 MLNATUREL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLNATUREF      PIC X.                                     00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MLNATUREI      PIC X(30).                                 00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMFACTL      COMP PIC S9(4).                            00000520
      *--                                                                       
           02 MNUMFACTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMFACTF      PIC X.                                     00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MNUMFACTI      PIC X(7).                                  00000550
      * MESSAGE ERREUR                                                  00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MLIBERRI  PIC X(78).                                      00000600
      * CODE TRANSACTION                                                00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCODTRAI  PIC X(4).                                       00000650
      * CICS DE TRAVAIL                                                 00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCICSI    PIC X(5).                                       00000700
      * NETNAME                                                         00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MNETNAMI  PIC X(8).                                       00000750
      * CODE TERMINAL                                                   00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MSCREENI  PIC X(5).                                       00000800
      ***************************************************************** 00000810
      * SDF: EFG10   EFG10                                              00000820
      ***************************************************************** 00000830
       01   EFG50O REDEFINES EFG50I.                                    00000840
           02 FILLER    PIC X(12).                                      00000850
      * DATE DU JOUR                                                    00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MDATJOUA  PIC X.                                          00000880
           02 MDATJOUC  PIC X.                                          00000890
           02 MDATJOUP  PIC X.                                          00000900
           02 MDATJOUH  PIC X.                                          00000910
           02 MDATJOUV  PIC X.                                          00000920
           02 MDATJOUO  PIC X(10).                                      00000930
      * HEURE                                                           00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MTIMJOUA  PIC X.                                          00000960
           02 MTIMJOUC  PIC X.                                          00000970
           02 MTIMJOUP  PIC X.                                          00000980
           02 MTIMJOUH  PIC X.                                          00000990
           02 MTIMJOUV  PIC X.                                          00001000
           02 MTIMJOUO  PIC X(5).                                       00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MSOCCA    PIC X.                                          00001030
           02 MSOCCC    PIC X.                                          00001040
           02 MSOCCP    PIC X.                                          00001050
           02 MSOCCH    PIC X.                                          00001060
           02 MSOCCV    PIC X.                                          00001070
           02 MSOCCO    PIC X(3).                                       00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MLSOCCA   PIC X.                                          00001100
           02 MLSOCCC   PIC X.                                          00001110
           02 MLSOCCP   PIC X.                                          00001120
           02 MLSOCCH   PIC X.                                          00001130
           02 MLSOCCV   PIC X.                                          00001140
           02 MLSOCCO   PIC X(24).                                      00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MNSOCA    PIC X.                                          00001170
           02 MNSOCC    PIC X.                                          00001180
           02 MNSOCP    PIC X.                                          00001190
           02 MNSOCH    PIC X.                                          00001200
           02 MNSOCV    PIC X.                                          00001210
           02 MNSOCO    PIC X(3).                                       00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MNLIEUA   PIC X.                                          00001240
           02 MNLIEUC   PIC X.                                          00001250
           02 MNLIEUP   PIC X.                                          00001260
           02 MNLIEUH   PIC X.                                          00001270
           02 MNLIEUV   PIC X.                                          00001280
           02 MNLIEUO   PIC X(3).                                       00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MLLIEUA   PIC X.                                          00001310
           02 MLLIEUC   PIC X.                                          00001320
           02 MLLIEUP   PIC X.                                          00001330
           02 MLLIEUH   PIC X.                                          00001340
           02 MLLIEUV   PIC X.                                          00001350
           02 MLLIEUO   PIC X(24).                                      00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MCTYPEA   PIC X.                                          00001380
           02 MCTYPEC   PIC X.                                          00001390
           02 MCTYPEP   PIC X.                                          00001400
           02 MCTYPEH   PIC X.                                          00001410
           02 MCTYPEV   PIC X.                                          00001420
           02 MCTYPEO   PIC X(2).                                       00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MLTYPEA   PIC X.                                          00001450
           02 MLTYPEC   PIC X.                                          00001460
           02 MLTYPEP   PIC X.                                          00001470
           02 MLTYPEH   PIC X.                                          00001480
           02 MLTYPEV   PIC X.                                          00001490
           02 MLTYPEO   PIC X(30).                                      00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MNATUREA  PIC X.                                          00001520
           02 MNATUREC  PIC X.                                          00001530
           02 MNATUREP  PIC X.                                          00001540
           02 MNATUREH  PIC X.                                          00001550
           02 MNATUREV  PIC X.                                          00001560
           02 MNATUREO  PIC X(5).                                       00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MLNATUREA      PIC X.                                     00001590
           02 MLNATUREC PIC X.                                          00001600
           02 MLNATUREP PIC X.                                          00001610
           02 MLNATUREH PIC X.                                          00001620
           02 MLNATUREV PIC X.                                          00001630
           02 MLNATUREO      PIC X(30).                                 00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MNUMFACTA      PIC X.                                     00001660
           02 MNUMFACTC PIC X.                                          00001670
           02 MNUMFACTP PIC X.                                          00001680
           02 MNUMFACTH PIC X.                                          00001690
           02 MNUMFACTV PIC X.                                          00001700
           02 MNUMFACTO      PIC X(7).                                  00001710
      * MESSAGE ERREUR                                                  00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MLIBERRA  PIC X.                                          00001740
           02 MLIBERRC  PIC X.                                          00001750
           02 MLIBERRP  PIC X.                                          00001760
           02 MLIBERRH  PIC X.                                          00001770
           02 MLIBERRV  PIC X.                                          00001780
           02 MLIBERRO  PIC X(78).                                      00001790
      * CODE TRANSACTION                                                00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MCODTRAA  PIC X.                                          00001820
           02 MCODTRAC  PIC X.                                          00001830
           02 MCODTRAP  PIC X.                                          00001840
           02 MCODTRAH  PIC X.                                          00001850
           02 MCODTRAV  PIC X.                                          00001860
           02 MCODTRAO  PIC X(4).                                       00001870
      * CICS DE TRAVAIL                                                 00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MCICSA    PIC X.                                          00001900
           02 MCICSC    PIC X.                                          00001910
           02 MCICSP    PIC X.                                          00001920
           02 MCICSH    PIC X.                                          00001930
           02 MCICSV    PIC X.                                          00001940
           02 MCICSO    PIC X(5).                                       00001950
      * NETNAME                                                         00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MNETNAMA  PIC X.                                          00001980
           02 MNETNAMC  PIC X.                                          00001990
           02 MNETNAMP  PIC X.                                          00002000
           02 MNETNAMH  PIC X.                                          00002010
           02 MNETNAMV  PIC X.                                          00002020
           02 MNETNAMO  PIC X(8).                                       00002030
      * CODE TERMINAL                                                   00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MSCREENA  PIC X.                                          00002060
           02 MSCREENC  PIC X.                                          00002070
           02 MSCREENP  PIC X.                                          00002080
           02 MSCREENH  PIC X.                                          00002090
           02 MSCREENV  PIC X.                                          00002100
           02 MSCREENO  PIC X(5).                                       00002110
                                                                                
