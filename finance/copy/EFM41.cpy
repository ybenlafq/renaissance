      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - GESTION DEVISES                                           00000020
      ***************************************************************** 00000030
       01   EFM41I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(2).                                       00000230
           02 LIGNEI OCCURS   14 TIMES .                                00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000250
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000260
             03 FILLER  PIC X(4).                                       00000270
             03 MSELI   PIC X.                                          00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODEL  COMP PIC S9(4).                                 00000290
      *--                                                                       
             03 MCODEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCODEF  PIC X.                                          00000300
             03 FILLER  PIC X(4).                                       00000310
             03 MCODEI  PIC X(2).                                       00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELLEL    COMP PIC S9(4).                            00000330
      *--                                                                       
             03 MLIBELLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBELLEF    PIC X.                                     00000340
             03 FILLER  PIC X(4).                                       00000350
             03 MLIBELLEI    PIC X(30).                                 00000360
      * ZONE CMD AIDA                                                   00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLIBERRI  PIC X(79).                                      00000410
      * CODE TRANSACTION                                                00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000430
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MCODTRAI  PIC X(4).                                       00000460
      * CICS DE TRAVAIL                                                 00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MCICSI    PIC X(5).                                       00000510
      * NETNAME                                                         00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000530
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000540
           02 FILLER    PIC X(4).                                       00000550
           02 MNETNAMI  PIC X(8).                                       00000560
      * CODE TERMINAL                                                   00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MSCREENI  PIC X(5).                                       00000610
      ***************************************************************** 00000620
      * GCT - GESTION DEVISES                                           00000630
      ***************************************************************** 00000640
       01   EFM41O REDEFINES EFM41I.                                    00000650
           02 FILLER    PIC X(12).                                      00000660
      * DATE DU JOUR                                                    00000670
           02 FILLER    PIC X(2).                                       00000680
           02 MDATJOUA  PIC X.                                          00000690
           02 MDATJOUC  PIC X.                                          00000700
           02 MDATJOUP  PIC X.                                          00000710
           02 MDATJOUH  PIC X.                                          00000720
           02 MDATJOUV  PIC X.                                          00000730
           02 MDATJOUO  PIC X(10).                                      00000740
      * HEURE                                                           00000750
           02 FILLER    PIC X(2).                                       00000760
           02 MTIMJOUA  PIC X.                                          00000770
           02 MTIMJOUC  PIC X.                                          00000780
           02 MTIMJOUP  PIC X.                                          00000790
           02 MTIMJOUH  PIC X.                                          00000800
           02 MTIMJOUV  PIC X.                                          00000810
           02 MTIMJOUO  PIC X(5).                                       00000820
           02 FILLER    PIC X(2).                                       00000830
           02 MPAGEA    PIC X.                                          00000840
           02 MPAGEC    PIC X.                                          00000850
           02 MPAGEP    PIC X.                                          00000860
           02 MPAGEH    PIC X.                                          00000870
           02 MPAGEV    PIC X.                                          00000880
           02 MPAGEO    PIC Z9.                                         00000890
           02 FILLER    PIC X(2).                                       00000900
           02 MNBPA     PIC X.                                          00000910
           02 MNBPC     PIC X.                                          00000920
           02 MNBPP     PIC X.                                          00000930
           02 MNBPH     PIC X.                                          00000940
           02 MNBPV     PIC X.                                          00000950
           02 MNBPO     PIC Z9.                                         00000960
           02 LIGNEO OCCURS   14 TIMES .                                00000970
             03 FILLER       PIC X(2).                                  00000980
             03 MSELA   PIC X.                                          00000990
             03 MSELC   PIC X.                                          00001000
             03 MSELP   PIC X.                                          00001010
             03 MSELH   PIC X.                                          00001020
             03 MSELV   PIC X.                                          00001030
             03 MSELO   PIC X.                                          00001040
             03 FILLER       PIC X(2).                                  00001050
             03 MCODEA  PIC X.                                          00001060
             03 MCODEC  PIC X.                                          00001070
             03 MCODEP  PIC X.                                          00001080
             03 MCODEH  PIC X.                                          00001090
             03 MCODEV  PIC X.                                          00001100
             03 MCODEO  PIC X(2).                                       00001110
             03 FILLER       PIC X(2).                                  00001120
             03 MLIBELLEA    PIC X.                                     00001130
             03 MLIBELLEC    PIC X.                                     00001140
             03 MLIBELLEP    PIC X.                                     00001150
             03 MLIBELLEH    PIC X.                                     00001160
             03 MLIBELLEV    PIC X.                                     00001170
             03 MLIBELLEO    PIC X(30).                                 00001180
      * ZONE CMD AIDA                                                   00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MLIBERRA  PIC X.                                          00001210
           02 MLIBERRC  PIC X.                                          00001220
           02 MLIBERRP  PIC X.                                          00001230
           02 MLIBERRH  PIC X.                                          00001240
           02 MLIBERRV  PIC X.                                          00001250
           02 MLIBERRO  PIC X(79).                                      00001260
      * CODE TRANSACTION                                                00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MCODTRAA  PIC X.                                          00001290
           02 MCODTRAC  PIC X.                                          00001300
           02 MCODTRAP  PIC X.                                          00001310
           02 MCODTRAH  PIC X.                                          00001320
           02 MCODTRAV  PIC X.                                          00001330
           02 MCODTRAO  PIC X(4).                                       00001340
      * CICS DE TRAVAIL                                                 00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MCICSA    PIC X.                                          00001370
           02 MCICSC    PIC X.                                          00001380
           02 MCICSP    PIC X.                                          00001390
           02 MCICSH    PIC X.                                          00001400
           02 MCICSV    PIC X.                                          00001410
           02 MCICSO    PIC X(5).                                       00001420
      * NETNAME                                                         00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MNETNAMA  PIC X.                                          00001450
           02 MNETNAMC  PIC X.                                          00001460
           02 MNETNAMP  PIC X.                                          00001470
           02 MNETNAMH  PIC X.                                          00001480
           02 MNETNAMV  PIC X.                                          00001490
           02 MNETNAMO  PIC X(8).                                       00001500
      * CODE TERMINAL                                                   00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MSCREENA  PIC X.                                          00001530
           02 MSCREENC  PIC X.                                          00001540
           02 MSCREENP  PIC X.                                          00001550
           02 MSCREENH  PIC X.                                          00001560
           02 MSCREENV  PIC X.                                          00001570
           02 MSCREENO  PIC X(5).                                       00001580
                                                                                
