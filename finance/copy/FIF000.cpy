      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  DSECT-IF000.                                                         
      ******************************************************************        
      *                                                                         
      *   DESCRIPTION DU FICHIER STANDARD DES MOUVEMENTS DES ORGANISMES         
      *                                                                         
      ******************************************************************        
      *  CODE ORGANISME                                                         
           05  IF000-CORGANISME PIC X(3).                               001 003 
      *  DATE DE RECEPTION                                                      
           05  IF000-DRECEPT    PIC X(8).                               004 008 
      *  HEURE DE RECEPTION (HHMM)                                              
           05  IF000-HRECEPT    PIC X(4).                               012 004 
      *  MODE DE PAIEMENT                                                       
           05  IF000-CMODPAI    PIC X(3).                               016 003 
      *  SOCIETE COMPTABLE                                                      
           05  IF000-NSOCCOMPT  PIC X(3).                               019 003 
      *  CODE ETABLISSEMENT                                                     
           05  IF000-NETAB      PIC X(3).                               022 003 
      *  SOCIETE DE GESTION                                                     
           05  IF000-NSOCIETE   PIC X(3).                               025 003 
      *  LIEU DE GESTION (MAGASIN)                                              
           05  IF000-NLIEU      PIC X(3).                               028 003 
      *  IDENTIFIANT ORGANISME                                                  
           05  IF000-NIDENT     PIC X(20).                              031 020 
      *  DATE DE MOUVEMENT (OPERATION)                                          
           05  IF000-DOPER      PIC X(8).                               051 008 
      *  DATE DE FINANCEMENT                                                    
           05  IF000-DFINANCE   PIC X(8).                               059 008 
      *  DATE DE REMISE                                                         
           05  IF000-DREMISE    PIC X(8).                               067 008 
      *  NUMERO DE REMISE                                                       
           05  IF000-NREMISE    PIC X(6).                               075 006 
      *  NUMERO DE DOSSIER                                                      
           05  IF000-NDOSSIER   PIC X(8).                               081 008 
      *  NOM DU CLIENT                                                          
           05  IF000-NOMCLIENT  PIC X(15).                              089 015 
      *  REFERENCE INTERNE (BON DE COMMANDE)                                    
      *  (LIEU + NUMERO)                                                        
           05  IF000-REFINTERNE PIC X(9).                               104 009 
      *  NOMBRE DE FACTURETTES                                                  
           05  IF000-NFACTURES  PIC S9(7)        COMP-3.                113 004 
      *  NUMERO DE CARTE DU PORTEUR                                             
           05  IF000-NPORTEUR   PIC X(19).                              117 019 
      *  TYPE DE MOUVEMENT/CREDIT                                               
           05  IF000-CTYPMVT    PIC X(1).                               136 001 
      *  MONTANT BRUT                                                           
           05  IF000-MTBRUT     PIC S9(9)V9(2)   COMP-3.                137 006 
      *  MONTANT DES COMMISSIONS                                                
           05  IF000-MTCOM      PIC S9(7)V9(2)   COMP-3.                143 005 
      *  MONTANT NET FINANCE                                                    
           05  IF000-MTNET      PIC S9(9)V9(2)   COMP-3.                148 006 
      *  TYPE D'ANOMALIE (A/R)                                                  
           05  IF000-TYPANO     PIC X(1).                               154 001 
      *  CODE ANOMALIE                                                          
           05  IF000-CODANO     PIC X(2).                               155 002 
      *  CTYPE DE COMPTABILITE                                                  
           05  IF000-CTYPCTA    PIC X(3).                               157 003 
      *  TYPE DE MONTANT                                                        
           05  IF000-CTYPMONT   PIC X(5).                               160 005 
      *  CODE BANQUE                                                            
           05  IF000-CBANQUE    PIC X(2).                               165 002 
      *  SEQUENCE DU FICHIER D ENTREE                                           
           05  IF000-WNSEQ      PIC S9(5)        COMP-3.                167 003 
      *  DEVISE ( 3 CARACTERES )                                                
           05  IF000-CDEVISE    PIC X(03).                              170 023 
           05  IF000-FILLER     PIC X(20).                              170 023 
      *******************************************************************       
      * DESCRIPTION DYNAMIQUE DU FICHIER IF000                                  
      *******************************************************************       
       01  IF000-CHAMPS.                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FILLER      PIC S9(4)    VALUE +28    COMP.                      
      *--                                                                       
           05  FILLER      PIC S9(4)    VALUE +28 COMP-5.                       
      *}                                                                        
           05  FILLER      PIC X(10)   VALUE 'CORGANISME'.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +3   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'DRECEPT   '.                      
           05  FILLER      PIC X       VALUE 'S'.                               
           05  FILLER      PIC S9(3)   VALUE +8   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'HRECEPT   '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +4   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'CMODPAI   '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +3   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'NSOCCOMPT '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +3   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'NETAB     '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +3   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'NSOCIETE  '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +3   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'NLIEU     '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +3   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'NIDENT    '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +20  COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'DOPER     '.                      
           05  FILLER      PIC X       VALUE 'S'.                               
           05  FILLER      PIC S9(3)   VALUE +8   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'DFINANCE  '.                      
           05  FILLER      PIC X       VALUE 'S'.                               
           05  FILLER      PIC S9(3)   VALUE +8   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'DREMISE   '.                      
           05  FILLER      PIC X       VALUE 'S'.                               
           05  FILLER      PIC S9(3)   VALUE +8   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'NREMISE   '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +6   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'NDOSSIER  '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +8   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'NOMCLIENT '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +15  COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'REFINTERNE'.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +9   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'NFACTURES '.                      
           05  FILLER      PIC X       VALUE 'P'.                               
           05  FILLER      PIC S9(3)   VALUE +7   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'NPORTEUR  '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +19  COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'CTYPMVT   '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +1   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'MTBRUT    '.                      
           05  FILLER      PIC X       VALUE 'P'.                               
           05  FILLER      PIC S9(3)   VALUE +11  COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +2   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'MTCOM     '.                      
           05  FILLER      PIC X       VALUE 'P'.                               
           05  FILLER      PIC S9(3)   VALUE +9   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +2   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'MTNET     '.                      
           05  FILLER      PIC X       VALUE 'P'.                               
           05  FILLER      PIC S9(3)   VALUE +11  COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +2   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'TYPANO    '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +1   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'CODANO    '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +2   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'CTYPCTA   '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +3   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'CTYPMONT  '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +5   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'CBANQUE   '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +2   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'WNSEQ     '.                      
           05  FILLER      PIC X       VALUE 'P'.                               
           05  FILLER      PIC S9(3)   VALUE +5   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'CDEVISE   '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +3   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
                                                                                
