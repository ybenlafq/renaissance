      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSARO1.RTFM90                      *        
      ******************************************************************        
       01  RVFM9000.                                                            
      *                       CINTERFACE                                        
           10 FM90-CINTERFACE      PIC X(5).                                    
      *                       CNATOPER                                          
           10 FM90-CNATOPER        PIC X(5).                                    
      *                       CTYPOPER                                          
           10 FM90-CTYPOPER        PIC X(5).                                    
      *                       NZ1                                               
           10 FM90-NZ1             PIC S9(2)V USAGE COMP-3.                     
      *                       NZ2                                               
           10 FM90-NZ2             PIC S9(2)V USAGE COMP-3.                     
      *                       NZ3                                               
           10 FM90-NZ3             PIC S9(2)V USAGE COMP-3.                     
      *                       NZ4                                               
           10 FM90-NZ4             PIC S9(2)V USAGE COMP-3.                     
      *                       NZ5                                               
           10 FM90-NZ5             PIC S9(2)V USAGE COMP-3.                     
      *                       NZ6                                               
           10 FM90-NZ6             PIC S9(2)V USAGE COMP-3.                     
      *                       NZ7                                               
           10 FM90-NZ7             PIC S9(2)V USAGE COMP-3.                     
      *                       NZ8                                               
           10 FM90-NZ8             PIC S9(2)V USAGE COMP-3.                     
      *                       NZ9                                               
           10 FM90-NZ9             PIC S9(2)V USAGE COMP-3.                     
      *                       NZ10                                              
           10 FM90-NZ10            PIC S9(2)V USAGE COMP-3.                     
      *                       LIBELLE                                           
           10 FM90-LIBELLE1        PIC X(30).                                   
      *                       LIBELLE                                           
           10 FM90-LIBELLE2        PIC X(30).                                   
      *                       DMAJ                                              
           10 FM90-DMAJ            PIC X(8).                                    
      *                       DSYST                                             
           10 FM90-DSYST           PIC S9(13)V USAGE COMP-3.                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 16      *        
      ******************************************************************        
                                                                                
