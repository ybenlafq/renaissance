      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFF8800                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFF8800                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFF8800.                                                            
           02  FF88-PREFT1                                                      
               PIC X(0001).                                                     
           02  FF88-DEXCPT                                                      
               PIC X(0004).                                                     
           02  FF88-NPIECE                                                      
               PIC X(0006).                                                     
           02  FF88-WTYPEPIECE                                                  
               PIC X(0001).                                                     
           02  FF88-CFAMCPT                                                     
               PIC X(0008).                                                     
           02  FF88-PMONTANT                                                    
               PIC S9(13)V99 COMP-3.                                            
           02  FF88-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFF8800                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFF8800-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF88-PREFT1-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF88-PREFT1-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF88-DEXCPT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF88-DEXCPT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF88-NPIECE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF88-NPIECE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF88-WTYPEPIECE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF88-WTYPEPIECE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF88-CFAMCPT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF88-CFAMCPT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF88-PMONTANT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF88-PMONTANT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF88-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  FF88-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
