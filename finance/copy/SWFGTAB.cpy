      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *    FICHIER DU TABLEAU DE REMUNEARTION DE COMPTE COURANT                 
      *                                                                         
      *----17/01/96 : AM (DSA005) : MODIF LONGUEUR ZONE INTERAGIO:              
      *----           PASSE DE 2 A 4 DECIMALES                                  
      *                                                                         
       01       FFGTAB-DSECT.                                                   
           05   FFGTAB-IDENT.                                                   
001          10 FFGTAB-NSOCCOMPT         PIC X(03).                             
004          10 FFGTAB-DVALEUR           PIC X(08).                             
           05   FFGTAB-SUITE.                                                   
012          10 FFGTAB-INTERAGIO         PIC S9(11)V9(4) COMP-3.                
020          10 FFGTAB-VIRPLUS           PIC S9(11)V9(2) COMP-3.                
027          10 FFGTAB-VIRMOIN           PIC S9(11)V9(2) COMP-3.                
034          10 FFGTAB-FACTPLUS          PIC S9(11)V9(2) COMP-3.                
041          10 FFGTAB-FACTMOIN          PIC S9(11)V9(2) COMP-3.                
048          10 FFGTAB-SOLDE             PIC S9(13)V9(2) COMP-3.                
056          10 FFGTAB-TAUX              PIC S9(03)V9(4) COMP-3.                
060          10 FFGTAB-TYPE              PIC X(1).                              
061          10 FFGTAB-DELTA             PIC S9(11)V9(2) COMP-3.                
068          10 FFGTAB-SOLDE-CTA         PIC S9(13)V9(2) COMP-3.                
             10 FFGTAB-INFOFACT.                                                
076             12 FFGTAB-CTYPFACT       PIC X(02).                             
078             12 FFGTAB-NUMFACT        PIC X(07).                             
085             12 FFGTAB-NSOCORIG       PIC X(03).                             
088             12 FFGTAB-NLIEUORIG      PIC X(03).                             
091             12 FFGTAB-NSOCDEST       PIC X(03).                             
094             12 FFGTAB-NLIEUDEST      PIC X(03).                             
097          10 FILLER                   PIC X(04).                             
->0100*                                                                         
                                                                                
