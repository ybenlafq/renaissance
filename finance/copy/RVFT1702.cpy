      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   TABLE RTFT17                                                          
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA VUE RVFT1702                           
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT1702.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT1702.                                                            
      *}                                                                        
           02  FT17-NSOC                                                        
               PIC X(0005).                                                     
           02  FT17-NETAB                                                       
               PIC X(0003).                                                     
           02  FT17-NAUX                                                        
               PIC X(0003).                                                     
           02  FT17-NTIERSCV                                                    
               PIC X(0006).                                                     
           02  FT17-CTITRENOM                                                   
               PIC X(0005).                                                     
           02  FT17-LNOM                                                        
               PIC X(0025).                                                     
           02  FT17-LPRENOM                                                     
               PIC X(0015).                                                     
           02  FT17-CVOIE                                                       
               PIC X(0005).                                                     
           02  FT17-CTVOIE                                                      
               PIC X(0004).                                                     
           02  FT17-LNOMVOIE                                                    
               PIC X(0021).                                                     
           02  FT17-LCOMMUNE                                                    
               PIC X(0032).                                                     
           02  FT17-CPOSTAL                                                     
               PIC X(0005).                                                     
           02  FT17-LBUREAU                                                     
               PIC X(0025).                                                     
           02  FT17-DCREAT                                                      
               PIC X(0008).                                                     
           02  FT17-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  FT17-TRANSPO                                                     
               PIC X(0006).                                                     
           02  FT17-METHREG                                                     
               PIC X(0003).                                                     
           02  FT17-NBJREG                                                      
               PIC S9(3) COMP-3.                                                
           02  FT17-INDICE                                                      
               PIC X(0001).                                                     
           02  FT17-JPAIE                                                       
               PIC X(0002).                                                     
           02  FT17-WRELANCE                                                    
               PIC X(0001).                                                     
           02  FT17-NBJRELANCE                                                  
               PIC S9(3) COMP-3.                                                
           02  FT17-TYPEPAI                                                     
               PIC X(0001).                                                     
           02  FT17-COMPTE                                                      
               PIC X(0006).                                                     
           02  FT17-SECTION                                                     
               PIC X(0006).                                                     
           02  FT17-RUBRIQUE                                                    
               PIC X(0006).                                                     
           02  FT17-CBANQUE                                                     
               PIC X(0005).                                                     
           02  FT17-DEVISEREG                                                   
               PIC X(0003).                                                     
           02  FT17-NTBENEFCV                                                   
               PIC X(0006).                                                     
           02  FT17-MTDECOUV                                                    
               PIC S9(9)V99 COMP-3.                                             
           02  FT17-ACIDCRE                                                     
               PIC X(0008).                                                     
           02  FT17-DMAJ                                                        
               PIC X(0008).                                                     
           02  FT17-ACIDMAJ                                                     
               PIC X(0008).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFT1702                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT1702-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT1702-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-NETAB-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-NETAB-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-NAUX-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-NAUX-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-NTIERSCV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-NTIERSCV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-CTITRENOM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-CTITRENOM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-LNOM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-LNOM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-LPRENOM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-LPRENOM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-CVOIE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-CVOIE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-CTVOIE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-CTVOIE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-LNOMVOIE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-LNOMVOIE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-LCOMMUNE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-LCOMMUNE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-CPOSTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-LBUREAU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-LBUREAU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-DCREAT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-DCREAT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-TRANSPO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-TRANSPO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-METHREG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-METHREG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-NBJREG-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-NBJREG-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-INDICE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-INDICE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-JPAIE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-JPAIE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-WRELANCE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-WRELANCE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-NBJRELANCE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-NBJRELANCE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-TYPEPAI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-TYPEPAI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-COMPTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-COMPTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-SECTION-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-SECTION-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-RUBRIQUE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-RUBRIQUE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-CBANQUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-CBANQUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-DEVISEREG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-DEVISEREG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-NTBENEFCV-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-NTBENEFCV-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-MTDECOUV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-MTDECOUV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-ACIDCRE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-ACIDCRE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT17-ACIDMAJ-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT17-ACIDMAJ-F                                                   
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
