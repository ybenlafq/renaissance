      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVIF0400                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVIF0400                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIF0400.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIF0400.                                                            
      *}                                                                        
           02  IF04-CTYPCTA                                                     
               PIC X(0003).                                                     
           02  IF04-CTYPMONT                                                    
               PIC X(0005).                                                     
           02  IF04-NOECS                                                       
               PIC X(0005).                                                     
           02  IF04-CMOTCLE1                                                    
               PIC X(0010).                                                     
           02  IF04-CMOTCLE2                                                    
               PIC X(0010).                                                     
           02  IF04-CMOTCLE3                                                    
               PIC X(0010).                                                     
           02  IF04-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVIF0400                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIF0400-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIF0400-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF04-CTYPCTA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF04-CTYPCTA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF04-CTYPMONT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF04-CTYPMONT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF04-NOECS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF04-NOECS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF04-CMOTCLE1-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF04-CMOTCLE1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF04-CMOTCLE2-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF04-CMOTCLE2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF04-CMOTCLE3-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF04-CMOTCLE3-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF04-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF04-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
