      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                                 00000010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-GC00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.          00000020
      *--                                                                       
       01  COMM-GC00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                
      *}                                                                        
      *                                                                 00000030
       01  Z-COMMAREA.                                                  00000040
                                                                        00000050
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00000060
           02 FILLER-COM-AIDA      PIC X(100).                          00000070
                                                                        00000080
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00000090
           02 COMM-CICS-APPLID     PIC X(08).                           00000100
           02 COMM-CICS-NETNAM     PIC X(08).                           00000110
           02 COMM-CICS-TRANSA     PIC X(04).                           00000120
                                                                        00000130
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00000140
           02 COMM-DATE-SIECLE     PIC X(02).                           00000150
           02 COMM-DATE-ANNEE      PIC X(02).                           00000160
           02 COMM-DATE-MOIS       PIC X(02).                           00000170
           02 COMM-DATE-JOUR       PIC 99.                              00000180
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00000190
           02 COMM-DATE-QNTA       PIC 999.                             00000200
           02 COMM-DATE-QNT0       PIC 99999.                           00000210
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00000220
           02 COMM-DATE-BISX       PIC 9.                               00000230
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00000240
           02 COMM-DATE-JSM        PIC 9.                               00000250
      *   LIBELLES DU JOUR COURT - LONG                                 00000260
           02 COMM-DATE-JSM-LC     PIC X(03).                           00000270
           02 COMM-DATE-JSM-LL     PIC X(08).                           00000280
      *   LIBELLES DU MOIS COURT - LONG                                 00000290
           02 COMM-DATE-MOIS-LC    PIC X(03).                           00000300
           02 COMM-DATE-MOIS-LL    PIC X(08).                           00000310
      *   DIFFERENTES FORMES DE DATE                                    00000320
           02 COMM-DATE-SSAAMMJJ   PIC X(08).                           00000330
           02 COMM-DATE-AAMMJJ     PIC X(06).                           00000340
           02 COMM-DATE-JJMMSSAA   PIC X(08).                           00000350
           02 COMM-DATE-JJMMAA     PIC X(06).                           00000360
           02 COMM-DATE-JJ-MM-AA   PIC X(08).                           00000370
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00000380
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00000390
           02 COMM-DATE-WEEK.                                           00000400
              05 COMM-DATE-SEMSS   PIC 99.                              00000410
              05 COMM-DATE-SEMAA   PIC 99.                              00000420
              05 COMM-DATE-SEMNU   PIC 99.                              00000430
           02 COMM-DATE-FILLER     PIC X(08).                           00000440
      *   ZONES RESERVEES TRAITEMENT DU SWAP                            00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00000460
      *                                                                         
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
                                                                        00000470
      *}                                                                        
      *--  ZONES RESERVEES APPLICATIVES COMMUNES ----------------- 3874 00000480
                                                                        00000490
           02 COMM-GC00-APPLI.                                          00000500
      *--PROGRAMME TGC00-------------------------------------------27   00000510
              03 COMM-ENTETE.                                           00000520
                 05 COMM-CODLANG            PIC X(02).                  00000530
                 05 COMM-CODPIC             PIC X(02).                  00000540
                 05 COMM-CODESFONCTION      PIC X(12).                  00000550
              03 COMM-00-ENTETE.                                        00000560
                 05 COMM-00-WFONC           PIC X(03).                  00000570
                 05 COMM-00-PGMAPL          PIC X(08).                  00000580
GA0803        03 COMM-00-DATE-SAVE.                                     00000590
                 05 COMM-00-DATE8           PIC X(08).                  00000600
                 05 COMM-00-DATE10          PIC X(10).                  00000610
                                                                        00000620
      *--  ZONES RESERVEES APPLICATIVES OPTION 1 ----------------3847   00000630
                                                                        00000640
           02 COMM-GC10-APPLI.                                          00000650
                                                                        00000660
              03 COMM-10.                                               00000670
      *-- PROGRAMME TGC10-----------------------------------------   83 00000680
                                                                        00000690
                 05 COMM-10-INDTS           PIC S9(5) COMP-3.           00000700
                 05 COMM-10-INDMAX          PIC S9(5) COMP-3.           00000710
                 05 COMM-10-CPRESTATION     PIC X(05).                  00000720
                 05 COMM-10-CPREST          PIC X(05).                  00000730
                 05 COMM-10-LPRESTATION     PIC X(20).                  00000740
                 05 COMM-10-CTYPPREST       PIC X(05).                  00000750
                 05 COMM-10-LTPREST         PIC X(20).                  00000760
                 05 COMM-10-CMARQ           PIC X(05).                  00000770
                 05 COMM-10-LMARQ           PIC X(20).                  00000780
                 05 COMM-10-MENTETE         PIC X(78).                  00000790
                 05 COMM-10-CASCRS          PIC 9(1).                   00000800
                    88  COM-10-CASCRS1        VALUE 1.                  00000810
                    88  COM-10-CASCRS2        VALUE 2.                  00000820
                    88  COM-10-CASCRS3        VALUE 3.                  00000830
                 05 COMM-10-CRITERE         PIC 9(1).                   00000840
                    88  COMM-10-CRITERE-SERVICE      VALUE 1.           00000850
                    88  COMM-10-CRITERE-TYPE         VALUE 2.           00000860
                    88  COMM-10-CRITERE-MARQUE       VALUE 3.           00000870
                    88  COMM-10-CRITERE-MARQUE-TYPE  VALUE 4.           00000880
                 05 COMM-10-CHGT-CRITERE    PIC 9(1).                   00000890
                    88  COMM-10-UN-CHGT-CRITERE      VALUE 1.           00000900
                    88  COMM-10-AUCUN-CHGT-CRITERE   VALUE 2.           00000910
                                                                        00000920
                                                                        00000930
      *--  ZONES RESERVEES APPLICATIVES OPTION 2 ----------------3764   00000940
                                                                        00000950
           02 COMM-GC20-APPLI.                                          00000960
                                                                        00000970
              03 COMM-20.                                               00000980
      *-- PROGRAMME TGC20------------------------------------------24   00000990
                                                                        00001000
                 05 COMM-20-INDTS           PIC S9(5) COMP-3.           00001010
                 05 COMM-20-INDMAX          PIC S9(5) COMP-3.           00001020
                 05 COMM-20-NCODIC          PIC X(07).                  00001030
                 05 COMM-20-CTYPPREST       PIC X(05).                  00001040
                 05 COMM-20-LTPREST         PIC X(20).                  00001050
                 05 COMM-20-CMARQ           PIC X(05).                  00001060
                 05 COMM-20-LMARQ           PIC X(20).                  00001070
                 05 COMM-20-CFAM            PIC X(05).                  00001080
                 05 COMM-20-MENTETE         PIC X(78).                  00001090
                 05 COMM-20-CRITERE         PIC 9(1).                   00001100
                    88  COMM-20-CRITERE-TYPE    VALUE 1.                00001110
                    88  COMM-20-CRITERE-MARQUE  VALUE 2.                00001120
                    88  COMM-20-CRITERE-TRIPLE  VALUE 3.                00001130
                    88  COMM-20-CRITERE-FAM     VALUE 4.                00001140
                    88  COMM-20-CRITERE-MARQUE-TYPE      VALUE 5.       00001150
                 05 COMM-20-CASCRS          PIC 9(1).                   00001160
                    88  COM-20-CASCRS1        VALUE 1.                  00001170
                    88  COM-20-CASCRS2        VALUE 2.                  00001180
                    88  COM-20-CASCRS3        VALUE 3.                  00001190
                 05 COMM-20-CHGT-CRITERE    PIC 9(1).                   00001200
                    88  COMM-20-UN-CHGT-CRITERE      VALUE 1.           00001210
                    88  COMM-20-AUCUN-CHGT-CRITERE   VALUE 2.           00001220
                                                                        00001230
                                                                        00001240
      *--  ZONES RESERVEES APPLICATIVES OPTION 3 ----------------3740   00001250
                                                                        00001260
           02 COMM-GC30-APPLI.                                          00001270
                                                                        00001280
              03 COMM-30.                                               00001290
      *-- PROGRAMME TGC30------------------------------------------21   00001300
                                                                        00001310
                 05 COMM-30-INDTS           PIC S9(5) COMP-3.           00001320
                 05 COMM-30-INDMAX          PIC S9(5) COMP-3.           00001330
                 05 COMM-30-NCODIC          PIC X(07).                  00001340
                 05 COMM-30-COFFRE          PIC X(07).                  00001350
                 05 COMM-30-MENTETE         PIC X(78).                  00001360
                 05 COMM-30-CRITERE         PIC 9(1).                   00001370
                    88  COMM-30-PAS-CRITERE-OFFRE  VALUE 1.             00001380
                    88  COMM-30-CRITERE-OFFRE      VALUE 2.             00001390
                 05 COMM-30-CASCRS          PIC 9(1).                   00001400
                    88  COM-30-CASCRS1        VALUE 1.                  00001410
                 05 COMM-30-CHGT-CRITERE    PIC 9(1).                   00001420
                    88  COMM-30-UN-CHGT-CRITERE      VALUE 1.           00001430
                    88  COMM-30-AUCUN-CHGT-CRITERE   VALUE 2.           00001440
                                                                        00001450
      *--  ZONES RESERVEES APPLICATIVES OPTION 5 ----------------       00001460
                                                                        00001470
           02 COMM-GC50-APPLI.                                          00001480
                                                                        00001490
              03 COMM-50.                                               00001500
      *-- PROGRAMME TGC50------------------------------------------     00001510
                                                                        00001520
                 05 COMM-50-INDTS           PIC S9(5) COMP-3.           00001530
                 05 COMM-50-INDMAX          PIC S9(5) COMP-3.           00001540
                 05 COMM-50-CTYPPREST       PIC X(05).                  00001550
                 05 COMM-50-LTPREST         PIC X(20).                  00001560
                 05 COMM-50-CTYPTXP         PIC X(05).                  00001570
                 05 COMM-50-LCTYPTXP        PIC X(20).                  00001580
                 05 COMM-50-CMARQ           PIC X(05).                  00001590
                 05 COMM-50-LMARQ           PIC X(20).                  00001600
                 05 COMM-50-CTYPCND         PIC X(05).                  00001610
                 05 COMM-50-LIBELLE         PIC X(26).                  00001620
                 05 COMM-50-NENTCDE         PIC X(05).                  00001630
                 05 COMM-50-MENTETE         PIC X(78).                  00001640
                 05 COMM-50-CRITERE         PIC 9(1).                   00001650
                    88  COMM-50-CRITERE-TYPE           VALUE 1.         00001660
                    88  COMM-50-CRITERE-CONDITION      VALUE 2.         00001670
                    88  COMM-50-CRITERE-CONDITION-TYPE VALUE 3.         00001680
                 05 COMM-50-CASCRS          PIC 9(1).                   00001690
                    88  COM-50-CASCRS1        VALUE 1.                  00001700
                    88  COM-50-CASCRS2        VALUE 2.                  00001710
                 05 COMM-50-CHGT-CRITERE    PIC 9(1).                   00001720
                    88  COMM-50-UN-CHGT-CRITERE      VALUE 1.           00001730
                    88  COMM-50-AUCUN-CHGT-CRITERE   VALUE 2.           00001740
                                                                        00001750
      *--  ZONES RESERVEES APPLICATIVES OPTION 5 ----------------       00001760
                                                                        00001770
           02 COMM-GC51-APPLI.                                          00001780
                                                                        00001790
              03 COMM-51.                                               00001800
      *-- PROGRAMME TGC51------------------------------------------     00001810
                                                                        00001820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-51-PAGEA           PIC S9(4) BINARY.           00001830
      *--                                                                       
                 05 COMM-51-PAGEA           PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-51-NBP             PIC S9(4) BINARY.           00001840
      *--                                                                       
                 05 COMM-51-NBP             PIC S9(4) COMP-5.                   
      *}                                                                        
                 05 COMM-51-CMARQ           PIC X(05).                  00001850
                 05 COMM-51-LMARQ           PIC X(20).                  00001860
                 05 COMM-51-CTYPTXP         PIC X(05).                  00001870
                 05 COMM-51-LCTYPTXP        PIC X(20).                  00001880
                 05 COMM-51-CTYPPREST       PIC X(05).                  00001890
                 05 COMM-51-LTPREST         PIC X(20).                  00001900
                 05 COMM-51-CTYPCND2        PIC X(05).                  00001910
                 05 COMM-51-LIBELLE2        PIC X(26).                  00001920
                                                                        00001930
      *--  ZONES RESERVEES APPLICATIVES OPTION 1 ----------------3719   00001940
                                                                        00001950
           02 COMM-GC11-APPLI.                                          00001960
                                                                        00001970
              03 COMM-11.                                               00001980
      *-- PROGRAMME TGC11------------------------------------------32   00001990
                                                                        00002000
                 05 COMM-11-INDTS           PIC S9(5) COMP-3.           00002010
                 05 COMM-11-INDMAX          PIC S9(5) COMP-3.           00002020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-11-PAGEA           PIC S9(4) BINARY.           00002030
      *--                                                                       
                 05 COMM-11-PAGEA           PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 COMM-11-NBP             PIC S9(4) BINARY.           00002040
      *--                                                                       
                 05 COMM-11-NBP             PIC S9(4) COMP-5.                   
      *}                                                                        
                 05 COMM-11-LIBFICHE1       PIC X(20).                  00002050
                 05 COMM-11-LIBFICHE2       PIC X(20).                  00002060
                 05 COMM-11-LIBFICHE3       PIC X(20).                  00002070
                 05 COMM-11-CTYPCND2        PIC X(05).                  00002080
                 05 COMM-11-LIBELLE2        PIC X(30).                  00002090
                 05 COMM-11-LCOMMENT2       PIC X(30).                  00002100
                 05 COMM-11-MONTANT2        PIC X(09).                  00002110
                 05 COMM-11-DEFFET2         PIC X(08).                  00002120
                 05 COMM-11-DFINEFFET2      PIC X(08).                  00002130
                 05 COMM-11-WINCCOND        PIC X(01).                  00002140
                 05 COMM-11-CRGLT           PIC X(03).                  00002150
                 05 COMM-11-CFACT           PIC X(03).                  00002160
                 05 COMM-11-WMTVENTE        PIC X(01).                  00002170
                 05 COMM-11-CTYPE           PIC X(01).                  00002180
                 05 COMM-11-NENTCDE         PIC X(05).                  00002190
                 05 COMM-11-CPREST          PIC X(05).                  00002200
                 05 COMM-11-CTYPPREST       PIC X(05).                  00002210
                 05 COMM-11-NCODIC          PIC X(07).                  00002220
                 05 COMM-11-COFFRE          PIC X(07).                  00002230
                 05 COMM-11-CMARQ           PIC X(07).                  00002240
                 05 COMM-11-CASLIB          PIC 9(01).                  00002250
                    88  COM-11-CASLIB1        VALUE 1.                  00002260
                    88  COM-11-CASLIB2        VALUE 2.                  00002270
                    88  COM-11-CASLIB3        VALUE 3.                  00002280
                                                                        00002290
                                                                        00002300
      *--  ZONES RESERVEES APPLICATIVES OPTION 4 ----------------       00002310
                                                                        00002320
           02 COMM-GC40-GC41-APPLI.                                     00002330
      *-- PROGRAMME TGC40------------------------------------------29   00002340
                                                                        00002350
              03 COMM-40.                                               00002360
                                                                        00002370
                 05 COMM-40-INDTS           PIC S9(5) COMP-3.           00002380
                 05 COMM-40-INDMAX          PIC S9(5) COMP-3.           00002390
                 05 COMM-40-NOM-TS          PIC X(08).                  00002400
                 05 COMM-40-PGMAPL          PIC X(08).                  00002410
                 05 COMM-40-WMTVTE-SAV      PIC X(01).                  00002420
                 05 COMM-40-WCOND-SAV       PIC X(01).                  00002430
                 05 COMM-40-WCMARQ-SAV      PIC X(05).                  00002440
                                                                        00002450
      *------------------------------------------------------------64   00002460
              03 COMM-41.                                               00002470
                                                                        00002480
                 05 COMM-41-DONNEES-SEL.                                00002490
                    10 COMM-41-CTYPCND      PIC X(05).                  00002500
                    10 COMM-41-LIBELLE      PIC X(20).                  00002510
                    10 COMM-41-WINCCOND     PIC X(01).                  00002520
                    10 COMM-41-CRGLT        PIC X(03).                  00002530
                    10 COMM-41-CFACT        PIC X(03).                  00002540
                    10 COMM-41-WMTVENTE     PIC X(01).                  00002550
                    10 COMM-41-NSEQ         PIC X(03).                  00002560
                    10 COMM-41-CMARQ        PIC X(05).                  00002570
                    10 COMM-41-LMARQ        PIC X(20).                  00002580
                                                                                
