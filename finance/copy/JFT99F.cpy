      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT JFT99F AU 25/06/1999  *        
      *                                                                *        
      *          CRITERES DE TRI  07,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-JFT99F.                                                        
            05 NOMETAT-JFT99F           PIC X(6) VALUE 'JFT99F'.                
            05 RUPTURES-JFT99F.                                                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 JFT99F-SEQUENCE           PIC S9(04) COMP.                007  002
      *--                                                                       
           10 JFT99F-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-JFT99F.                                                   
           10 JFT99F-CODE               PIC X(05).                      009  005
           10 JFT99F-LIBELLE            PIC X(25).                      014  025
            05 FILLER                      PIC X(474).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-JFT99F-LONG           PIC S9(4)   COMP  VALUE +038.           
      *                                                                         
      *--                                                                       
        01  DSECT-JFT99F-LONG           PIC S9(4) COMP-5  VALUE +038.           
                                                                                
      *}                                                                        
