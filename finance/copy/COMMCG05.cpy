      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      * COMMAREA SPECIFIQUE MODULE D'ENVOI DONN�ES DE NCG VERS SAP *            
      **************************************************************            
      *                                                                         
      *   PROGRAMME MCG05                                                       
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 COMM-MCG05-LONG-COMMAREA       PIC S9(4) COMP VALUE +609.             
      *--                                                                       
       01 COMM-MCG05-LONG-COMMAREA       PIC S9(4) COMP-5 VALUE +609.           
      *}                                                                        
       01 COMM-MCG05-APPLI.                                                     
      *                                                                         
          02 COMM-MCG05-ENTREE.                                                 
      *------------------------------------------------------------09           
             05 COMM-MCG05-PGRM         PIC X(5).                               
             05 COMM-MCG05-NTERMID      PIC X(4).                               
      *      DONNEES GENERALES ----------------------------- 400                
             05 COMM-MCG05-CFONC        PIC X(03).                              
             05 FLAG-CDE-MERE           PIC X VALUE SPACE.                      
                88 CDE-MERE             VALUE '0'.                              
                88 CDE-FILLE            VALUE '1'.                              
             05 COMM-MCG05-DON-ENT      PIC X(300).                             
             05 COMM-MCG05-CRE  REDEFINES COMM-MCG05-DON-ENT.                   
                10 COMM-MCG05-NSURCDE-CRE  PIC X(07).                           
                10 COMM-MCG05-NCDE-CRE     PIC X(07).                           
                10 FILLER                  PIC X(286).                          
             05 COMM-MCG05-MAJ  REDEFINES COMM-MCG05-DON-ENT.                   
      *      DONNEES ALIMENTEES LORS DE LA MAJ D'UNE CDE                        
                10 COMM-MCG05-NSURCDE-MAJ  PIC X(07).                           
                10 COMM-MCG05-NCDE-MAJ     PIC X(07).                           
                10 COMM-MCG05-SUP-CODIC    PIC X(01).                           
                   88 PAS-SUPPRESSION-CODIC   VALUE '0'.                        
                   88 SUPPRESSION-CODIC       VALUE '1'.                        
      *      TABLEAU DES CODICS SUPPRIMES DANS LA COMMANDE                      
                10 COMM-MCG05-DETAIL-CODIC  OCCURS 20.                          
                   15 COMM-MCG05-NCODIC-SUP   PIC X(07).                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         10 COMM-MCG05-ICODIC-MAX   PIC 9(4) COMP.                       
      *--                                                                       
                10 COMM-MCG05-ICODIC-MAX   PIC 9(4) COMP-5.                     
      *}                                                                        
                10 FILLER                  PIC X(143).                          
      *      DONNEES ALIMENTEES LORS DE LA SUPPRESSION D'UNE CDE                
             05 COMM-MCG05-SUP    REDEFINES COMM-MCG05-DON-ENT.                 
                10 COMM-MCG05-NSURCDE-S    PIC X(07).                           
                10 COMM-MCG05-DETAIL-SUP OCCURS 5.                              
                   15 COMM-MCG05-NCDE-S       PIC X(07).                        
                   15 COMM-MCG05-NENTCDE-S    PIC X(05).                        
                   15 COMM-MCG05-CTYPCDE-S    PIC X(05).                        
                   15 COMM-MCG05-NSOCLIVR-S   PIC X(03).                        
                   15 COMM-MCG05-NDEPOT-S     PIC X(03).                        
                   15 COMM-MCG05-DSAISIE-S    PIC X(08).                        
                   15 COMM-MCG05-CCOMPTE-S    PIC X(06).                        
                10 FILLER                  PIC X(93).                           
             05 FILLER                  PIC X(96).                              
      *                                                                         
          02 COMM-MCG05-SORTIE.                                                 
      *            MESSAGE DE SORTIE ----------------------- 100                
             05 COMM-MCG05-MESSAGE.                                             
                  10 COMM-MCG05-CODRET              PIC X(1).                   
                     88 COMM-MCG05-OK              VALUE ' '.                   
                     88 COMM-MCG05-ERR             VALUE '1'.                   
                  10 COMM-MCG05-LIBERR             PIC X(58).                   
                  10 FILLER                        PIC X(41).                   
          02  FILLER                               PIC X(100).                  
      *                                                                         
                                                                                
