      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SAP - REGLES COMPTABLES                                         00000020
      ***************************************************************** 00000030
       01   EFM82I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(2).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINTERFACEL   COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MCINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MCINTERFACEF   PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MCINTERFACEI   PIC X(5).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINTERFACEL   COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MLINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLINTERFACEF   PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLINTERFACEI   PIC X(30).                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MCTYPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCTYPF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MCTYPI    PIC X(5).                                       00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCNATL    COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MCNATL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCNATF    PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MCNATI    PIC X(5).                                       00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPTL     COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MCPTL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MCPTF     PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MCPTI     PIC X(8).                                       00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRPL    COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MCTRPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCTRPF    PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MCTRPI    PIC X(8).                                       00000470
           02 MTABI OCCURS   11 TIMES .                                 00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000490
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MSELI   PIC X.                                          00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPOPERL   COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MCTYPOPERL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCTYPOPERF   PIC X.                                     00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MCTYPOPERI   PIC X(5).                                  00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCNATOPERL   COMP PIC S9(4).                            00000570
      *--                                                                       
             03 MCNATOPERL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCNATOPERF   PIC X.                                     00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MCNATOPERI   PIC X(5).                                  00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTVAL  COMP PIC S9(4).                                 00000610
      *--                                                                       
             03 MCTVAL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCTVAF  PIC X.                                          00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MCTVAI  PIC X.                                          00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCGEOL  COMP PIC S9(4).                                 00000650
      *--                                                                       
             03 MCGEOL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCGEOF  PIC X.                                          00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MCGEOI  PIC X.                                          00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWGROUPEL    COMP PIC S9(4).                            00000690
      *--                                                                       
             03 MWGROUPEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWGROUPEF    PIC X.                                     00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MWGROUPEI    PIC X.                                     00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMPTEL     COMP PIC S9(4).                            00000730
      *--                                                                       
             03 MCOMPTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCOMPTEF     PIC X.                                     00000740
             03 FILLER  PIC X(4).                                       00000750
             03 MCOMPTEI     PIC X(8).                                  00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSENSL      COMP PIC S9(4).                            00000770
      *--                                                                       
             03 MCSENSL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCSENSF      PIC X.                                     00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MCSENSI      PIC X.                                     00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWCUMULL     COMP PIC S9(4).                            00000810
      *--                                                                       
             03 MWCUMULL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWCUMULF     PIC X.                                     00000820
             03 FILLER  PIC X(4).                                       00000830
             03 MWCUMULI     PIC X.                                     00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNTIERSL     COMP PIC S9(4).                            00000850
      *--                                                                       
             03 MNTIERSL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNTIERSF     PIC X.                                     00000860
             03 FILLER  PIC X(4).                                       00000870
             03 MNTIERSI     PIC X.                                     00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLETTRAGEL  COMP PIC S9(4).                            00000890
      *--                                                                       
             03 MNLETTRAGEL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNLETTRAGEF  PIC X.                                     00000900
             03 FILLER  PIC X(4).                                       00000910
             03 MNLETTRAGEI  PIC X.                                     00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCCGSL  COMP PIC S9(4).                                 00000930
      *--                                                                       
             03 MCCGSL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCCGSF  PIC X.                                          00000940
             03 FILLER  PIC X(4).                                       00000950
             03 MCCGSI  PIC X.                                          00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWANALL      COMP PIC S9(4).                            00000970
      *--                                                                       
             03 MWANALL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MWANALF      PIC X.                                     00000980
             03 FILLER  PIC X(4).                                       00000990
             03 MWANALI      PIC X.                                     00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPMTL      COMP PIC S9(4).                            00001010
      *--                                                                       
             03 MTYPMTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MTYPMTF      PIC X.                                     00001020
             03 FILLER  PIC X(4).                                       00001030
             03 MTYPMTI      PIC X(2).                                  00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCONTREPL    COMP PIC S9(4).                            00001050
      *--                                                                       
             03 MCONTREPL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCONTREPF    PIC X.                                     00001060
             03 FILLER  PIC X(4).                                       00001070
             03 MCONTREPI    PIC X(8).                                  00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWCUMULCL    COMP PIC S9(4).                            00001090
      *--                                                                       
             03 MWCUMULCL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWCUMULCF    PIC X.                                     00001100
             03 FILLER  PIC X(4).                                       00001110
             03 MWCUMULCI    PIC X.                                     00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNTIERSCL    COMP PIC S9(4).                            00001130
      *--                                                                       
             03 MNTIERSCL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNTIERSCF    PIC X.                                     00001140
             03 FILLER  PIC X(4).                                       00001150
             03 MNTIERSCI    PIC X.                                     00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLETTRAGECL      COMP PIC S9(4).                       00001170
      *--                                                                       
             03 MNLETTRAGECL COMP-5 PIC S9(4).                                  
      *}                                                                        
             03 MNLETTRAGECF      PIC X.                                00001180
             03 FILLER  PIC X(4).                                       00001190
             03 MNLETTRAGECI      PIC X.                                00001200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCCGSCL      COMP PIC S9(4).                            00001210
      *--                                                                       
             03 MCCGSCL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCCGSCF      PIC X.                                     00001220
             03 FILLER  PIC X(4).                                       00001230
             03 MCCGSCI      PIC X.                                     00001240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWANALCL     COMP PIC S9(4).                            00001250
      *--                                                                       
             03 MWANALCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWANALCF     PIC X.                                     00001260
             03 FILLER  PIC X(4).                                       00001270
             03 MWANALCI     PIC X.                                     00001280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPMTCL     COMP PIC S9(4).                            00001290
      *--                                                                       
             03 MTYPMTCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTYPMTCF     PIC X.                                     00001300
             03 FILLER  PIC X(4).                                       00001310
             03 MTYPMTCI     PIC X(2).                                  00001320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWECHEANCL   COMP PIC S9(4).                            00001330
      *--                                                                       
             03 MWECHEANCL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MWECHEANCF   PIC X.                                     00001340
             03 FILLER  PIC X(4).                                       00001350
             03 MWECHEANCI   PIC X.                                     00001360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00001370
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00001380
             03 FILLER  PIC X(4).                                       00001390
             03 MDEFFETI     PIC X(10).                                 00001400
      * ZONE CMD AIDA                                                   00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MLIBERRI  PIC X(79).                                      00001450
      * CODE TRANSACTION                                                00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MCODTRAI  PIC X(4).                                       00001500
      * CICS DE TRAVAIL                                                 00001510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001520
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001530
           02 FILLER    PIC X(4).                                       00001540
           02 MCICSI    PIC X(5).                                       00001550
      * NETNAME                                                         00001560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001570
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001580
           02 FILLER    PIC X(4).                                       00001590
           02 MNETNAMI  PIC X(8).                                       00001600
      * CODE TERMINAL                                                   00001610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001620
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001630
           02 FILLER    PIC X(4).                                       00001640
           02 MSCREENI  PIC X(4).                                       00001650
      ***************************************************************** 00001660
      * SAP - REGLES COMPTABLES                                         00001670
      ***************************************************************** 00001680
       01   EFM82O REDEFINES EFM82I.                                    00001690
           02 FILLER    PIC X(12).                                      00001700
      * DATE DU JOUR                                                    00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MDATJOUA  PIC X.                                          00001730
           02 MDATJOUC  PIC X.                                          00001740
           02 MDATJOUP  PIC X.                                          00001750
           02 MDATJOUH  PIC X.                                          00001760
           02 MDATJOUV  PIC X.                                          00001770
           02 MDATJOUO  PIC X(10).                                      00001780
      * HEURE                                                           00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MTIMJOUA  PIC X.                                          00001810
           02 MTIMJOUC  PIC X.                                          00001820
           02 MTIMJOUP  PIC X.                                          00001830
           02 MTIMJOUH  PIC X.                                          00001840
           02 MTIMJOUV  PIC X.                                          00001850
           02 MTIMJOUO  PIC X(5).                                       00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MPAGEA    PIC X.                                          00001880
           02 MPAGEC    PIC X.                                          00001890
           02 MPAGEP    PIC X.                                          00001900
           02 MPAGEH    PIC X.                                          00001910
           02 MPAGEV    PIC X.                                          00001920
           02 MPAGEO    PIC Z9.                                         00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MNBPA     PIC X.                                          00001950
           02 MNBPC     PIC X.                                          00001960
           02 MNBPP     PIC X.                                          00001970
           02 MNBPH     PIC X.                                          00001980
           02 MNBPV     PIC X.                                          00001990
           02 MNBPO     PIC Z9.                                         00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MCINTERFACEA   PIC X.                                     00002020
           02 MCINTERFACEC   PIC X.                                     00002030
           02 MCINTERFACEP   PIC X.                                     00002040
           02 MCINTERFACEH   PIC X.                                     00002050
           02 MCINTERFACEV   PIC X.                                     00002060
           02 MCINTERFACEO   PIC X(5).                                  00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MLINTERFACEA   PIC X.                                     00002090
           02 MLINTERFACEC   PIC X.                                     00002100
           02 MLINTERFACEP   PIC X.                                     00002110
           02 MLINTERFACEH   PIC X.                                     00002120
           02 MLINTERFACEV   PIC X.                                     00002130
           02 MLINTERFACEO   PIC X(30).                                 00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MCTYPA    PIC X.                                          00002160
           02 MCTYPC    PIC X.                                          00002170
           02 MCTYPP    PIC X.                                          00002180
           02 MCTYPH    PIC X.                                          00002190
           02 MCTYPV    PIC X.                                          00002200
           02 MCTYPO    PIC X(5).                                       00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MCNATA    PIC X.                                          00002230
           02 MCNATC    PIC X.                                          00002240
           02 MCNATP    PIC X.                                          00002250
           02 MCNATH    PIC X.                                          00002260
           02 MCNATV    PIC X.                                          00002270
           02 MCNATO    PIC X(5).                                       00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MCPTA     PIC X.                                          00002300
           02 MCPTC     PIC X.                                          00002310
           02 MCPTP     PIC X.                                          00002320
           02 MCPTH     PIC X.                                          00002330
           02 MCPTV     PIC X.                                          00002340
           02 MCPTO     PIC X(8).                                       00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MCTRPA    PIC X.                                          00002370
           02 MCTRPC    PIC X.                                          00002380
           02 MCTRPP    PIC X.                                          00002390
           02 MCTRPH    PIC X.                                          00002400
           02 MCTRPV    PIC X.                                          00002410
           02 MCTRPO    PIC X(8).                                       00002420
           02 MTABO OCCURS   11 TIMES .                                 00002430
             03 FILLER       PIC X(2).                                  00002440
             03 MSELA   PIC X.                                          00002450
             03 MSELC   PIC X.                                          00002460
             03 MSELP   PIC X.                                          00002470
             03 MSELH   PIC X.                                          00002480
             03 MSELV   PIC X.                                          00002490
             03 MSELO   PIC X.                                          00002500
             03 FILLER       PIC X(2).                                  00002510
             03 MCTYPOPERA   PIC X.                                     00002520
             03 MCTYPOPERC   PIC X.                                     00002530
             03 MCTYPOPERP   PIC X.                                     00002540
             03 MCTYPOPERH   PIC X.                                     00002550
             03 MCTYPOPERV   PIC X.                                     00002560
             03 MCTYPOPERO   PIC X(5).                                  00002570
             03 FILLER       PIC X(2).                                  00002580
             03 MCNATOPERA   PIC X.                                     00002590
             03 MCNATOPERC   PIC X.                                     00002600
             03 MCNATOPERP   PIC X.                                     00002610
             03 MCNATOPERH   PIC X.                                     00002620
             03 MCNATOPERV   PIC X.                                     00002630
             03 MCNATOPERO   PIC X(5).                                  00002640
             03 FILLER       PIC X(2).                                  00002650
             03 MCTVAA  PIC X.                                          00002660
             03 MCTVAC  PIC X.                                          00002670
             03 MCTVAP  PIC X.                                          00002680
             03 MCTVAH  PIC X.                                          00002690
             03 MCTVAV  PIC X.                                          00002700
             03 MCTVAO  PIC X.                                          00002710
             03 FILLER       PIC X(2).                                  00002720
             03 MCGEOA  PIC X.                                          00002730
             03 MCGEOC  PIC X.                                          00002740
             03 MCGEOP  PIC X.                                          00002750
             03 MCGEOH  PIC X.                                          00002760
             03 MCGEOV  PIC X.                                          00002770
             03 MCGEOO  PIC X.                                          00002780
             03 FILLER       PIC X(2).                                  00002790
             03 MWGROUPEA    PIC X.                                     00002800
             03 MWGROUPEC    PIC X.                                     00002810
             03 MWGROUPEP    PIC X.                                     00002820
             03 MWGROUPEH    PIC X.                                     00002830
             03 MWGROUPEV    PIC X.                                     00002840
             03 MWGROUPEO    PIC X.                                     00002850
             03 FILLER       PIC X(2).                                  00002860
             03 MCOMPTEA     PIC X.                                     00002870
             03 MCOMPTEC     PIC X.                                     00002880
             03 MCOMPTEP     PIC X.                                     00002890
             03 MCOMPTEH     PIC X.                                     00002900
             03 MCOMPTEV     PIC X.                                     00002910
             03 MCOMPTEO     PIC X(8).                                  00002920
             03 FILLER       PIC X(2).                                  00002930
             03 MCSENSA      PIC X.                                     00002940
             03 MCSENSC PIC X.                                          00002950
             03 MCSENSP PIC X.                                          00002960
             03 MCSENSH PIC X.                                          00002970
             03 MCSENSV PIC X.                                          00002980
             03 MCSENSO      PIC X.                                     00002990
             03 FILLER       PIC X(2).                                  00003000
             03 MWCUMULA     PIC X.                                     00003010
             03 MWCUMULC     PIC X.                                     00003020
             03 MWCUMULP     PIC X.                                     00003030
             03 MWCUMULH     PIC X.                                     00003040
             03 MWCUMULV     PIC X.                                     00003050
             03 MWCUMULO     PIC X.                                     00003060
             03 FILLER       PIC X(2).                                  00003070
             03 MNTIERSA     PIC X.                                     00003080
             03 MNTIERSC     PIC X.                                     00003090
             03 MNTIERSP     PIC X.                                     00003100
             03 MNTIERSH     PIC X.                                     00003110
             03 MNTIERSV     PIC X.                                     00003120
             03 MNTIERSO     PIC X.                                     00003130
             03 FILLER       PIC X(2).                                  00003140
             03 MNLETTRAGEA  PIC X.                                     00003150
             03 MNLETTRAGEC  PIC X.                                     00003160
             03 MNLETTRAGEP  PIC X.                                     00003170
             03 MNLETTRAGEH  PIC X.                                     00003180
             03 MNLETTRAGEV  PIC X.                                     00003190
             03 MNLETTRAGEO  PIC X.                                     00003200
             03 FILLER       PIC X(2).                                  00003210
             03 MCCGSA  PIC X.                                          00003220
             03 MCCGSC  PIC X.                                          00003230
             03 MCCGSP  PIC X.                                          00003240
             03 MCCGSH  PIC X.                                          00003250
             03 MCCGSV  PIC X.                                          00003260
             03 MCCGSO  PIC X.                                          00003270
             03 FILLER       PIC X(2).                                  00003280
             03 MWANALA      PIC X.                                     00003290
             03 MWANALC PIC X.                                          00003300
             03 MWANALP PIC X.                                          00003310
             03 MWANALH PIC X.                                          00003320
             03 MWANALV PIC X.                                          00003330
             03 MWANALO      PIC X.                                     00003340
             03 FILLER       PIC X(2).                                  00003350
             03 MTYPMTA      PIC X.                                     00003360
             03 MTYPMTC PIC X.                                          00003370
             03 MTYPMTP PIC X.                                          00003380
             03 MTYPMTH PIC X.                                          00003390
             03 MTYPMTV PIC X.                                          00003400
             03 MTYPMTO      PIC X(2).                                  00003410
             03 FILLER       PIC X(2).                                  00003420
             03 MCONTREPA    PIC X.                                     00003430
             03 MCONTREPC    PIC X.                                     00003440
             03 MCONTREPP    PIC X.                                     00003450
             03 MCONTREPH    PIC X.                                     00003460
             03 MCONTREPV    PIC X.                                     00003470
             03 MCONTREPO    PIC X(8).                                  00003480
             03 FILLER       PIC X(2).                                  00003490
             03 MWCUMULCA    PIC X.                                     00003500
             03 MWCUMULCC    PIC X.                                     00003510
             03 MWCUMULCP    PIC X.                                     00003520
             03 MWCUMULCH    PIC X.                                     00003530
             03 MWCUMULCV    PIC X.                                     00003540
             03 MWCUMULCO    PIC X.                                     00003550
             03 FILLER       PIC X(2).                                  00003560
             03 MNTIERSCA    PIC X.                                     00003570
             03 MNTIERSCC    PIC X.                                     00003580
             03 MNTIERSCP    PIC X.                                     00003590
             03 MNTIERSCH    PIC X.                                     00003600
             03 MNTIERSCV    PIC X.                                     00003610
             03 MNTIERSCO    PIC X.                                     00003620
             03 FILLER       PIC X(2).                                  00003630
             03 MNLETTRAGECA      PIC X.                                00003640
             03 MNLETTRAGECC PIC X.                                     00003650
             03 MNLETTRAGECP PIC X.                                     00003660
             03 MNLETTRAGECH PIC X.                                     00003670
             03 MNLETTRAGECV PIC X.                                     00003680
             03 MNLETTRAGECO      PIC X.                                00003690
             03 FILLER       PIC X(2).                                  00003700
             03 MCCGSCA      PIC X.                                     00003710
             03 MCCGSCC PIC X.                                          00003720
             03 MCCGSCP PIC X.                                          00003730
             03 MCCGSCH PIC X.                                          00003740
             03 MCCGSCV PIC X.                                          00003750
             03 MCCGSCO      PIC X.                                     00003760
             03 FILLER       PIC X(2).                                  00003770
             03 MWANALCA     PIC X.                                     00003780
             03 MWANALCC     PIC X.                                     00003790
             03 MWANALCP     PIC X.                                     00003800
             03 MWANALCH     PIC X.                                     00003810
             03 MWANALCV     PIC X.                                     00003820
             03 MWANALCO     PIC X.                                     00003830
             03 FILLER       PIC X(2).                                  00003840
             03 MTYPMTCA     PIC X.                                     00003850
             03 MTYPMTCC     PIC X.                                     00003860
             03 MTYPMTCP     PIC X.                                     00003870
             03 MTYPMTCH     PIC X.                                     00003880
             03 MTYPMTCV     PIC X.                                     00003890
             03 MTYPMTCO     PIC X(2).                                  00003900
             03 FILLER       PIC X(2).                                  00003910
             03 MWECHEANCA   PIC X.                                     00003920
             03 MWECHEANCC   PIC X.                                     00003930
             03 MWECHEANCP   PIC X.                                     00003940
             03 MWECHEANCH   PIC X.                                     00003950
             03 MWECHEANCV   PIC X.                                     00003960
             03 MWECHEANCO   PIC X.                                     00003970
             03 FILLER       PIC X(2).                                  00003980
             03 MDEFFETA     PIC X.                                     00003990
             03 MDEFFETC     PIC X.                                     00004000
             03 MDEFFETP     PIC X.                                     00004010
             03 MDEFFETH     PIC X.                                     00004020
             03 MDEFFETV     PIC X.                                     00004030
             03 MDEFFETO     PIC X(10).                                 00004040
      * ZONE CMD AIDA                                                   00004050
           02 FILLER    PIC X(2).                                       00004060
           02 MLIBERRA  PIC X.                                          00004070
           02 MLIBERRC  PIC X.                                          00004080
           02 MLIBERRP  PIC X.                                          00004090
           02 MLIBERRH  PIC X.                                          00004100
           02 MLIBERRV  PIC X.                                          00004110
           02 MLIBERRO  PIC X(79).                                      00004120
      * CODE TRANSACTION                                                00004130
           02 FILLER    PIC X(2).                                       00004140
           02 MCODTRAA  PIC X.                                          00004150
           02 MCODTRAC  PIC X.                                          00004160
           02 MCODTRAP  PIC X.                                          00004170
           02 MCODTRAH  PIC X.                                          00004180
           02 MCODTRAV  PIC X.                                          00004190
           02 MCODTRAO  PIC X(4).                                       00004200
      * CICS DE TRAVAIL                                                 00004210
           02 FILLER    PIC X(2).                                       00004220
           02 MCICSA    PIC X.                                          00004230
           02 MCICSC    PIC X.                                          00004240
           02 MCICSP    PIC X.                                          00004250
           02 MCICSH    PIC X.                                          00004260
           02 MCICSV    PIC X.                                          00004270
           02 MCICSO    PIC X(5).                                       00004280
      * NETNAME                                                         00004290
           02 FILLER    PIC X(2).                                       00004300
           02 MNETNAMA  PIC X.                                          00004310
           02 MNETNAMC  PIC X.                                          00004320
           02 MNETNAMP  PIC X.                                          00004330
           02 MNETNAMH  PIC X.                                          00004340
           02 MNETNAMV  PIC X.                                          00004350
           02 MNETNAMO  PIC X(8).                                       00004360
      * CODE TERMINAL                                                   00004370
           02 FILLER    PIC X(2).                                       00004380
           02 MSCREENA  PIC X.                                          00004390
           02 MSCREENC  PIC X.                                          00004400
           02 MSCREENP  PIC X.                                          00004410
           02 MSCREENH  PIC X.                                          00004420
           02 MSCREENV  PIC X.                                          00004430
           02 MSCREENO  PIC X(4).                                       00004440
                                                                                
