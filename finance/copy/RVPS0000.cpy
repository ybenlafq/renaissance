      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVPS0000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVPS0000                         
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVPS0000.                                                            
           02  PS00-NSOC                                                        
               PIC X(0003).                                                     
           02  PS00-CGCPLT                                                      
               PIC X(0005).                                                     
           02  PS00-NGCPLT                                                      
               PIC X(0008).                                                     
           02  PS00-NLIEU                                                       
               PIC X(0003).                                                     
           02  PS00-NVENTE                                                      
               PIC X(0007).                                                     
           02  PS00-DDELIV                                                      
               PIC X(0008).                                                     
           02  PS00-DCOMPTA                                                     
               PIC X(0008).                                                     
           02  PS00-CINSEE                                                      
               PIC X(0005).                                                     
           02  PS00-NCODIC                                                      
               PIC X(0007).                                                     
           02  PS00-MTGCPLT                                                     
               PIC S9(5)V9(0002) COMP-3.                                        
           02  PS00-DANNUL                                                      
               PIC X(0008).                                                     
           02  PS00-MTREMB                                                      
               PIC S9(5)V9(0002) COMP-3.                                        
           02  PS00-DREMB                                                       
               PIC X(0008).                                                     
           02  PS00-MTRACHAT                                                    
               PIC S9(5)V9(0002) COMP-3.                                        
           02  PS00-DRACHAT                                                     
               PIC X(0008).                                                     
           02  PS00-NOMCLI                                                      
               PIC X(0015).                                                     
           02  PS00-DTRAIT                                                      
               PIC X(0008).                                                     
           02  PS00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVPS0000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVPS0000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS00-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS00-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS00-CGCPLT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS00-CGCPLT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS00-NGCPLT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS00-NGCPLT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS00-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS00-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS00-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS00-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS00-DDELIV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS00-DDELIV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS00-DCOMPTA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS00-DCOMPTA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS00-CINSEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS00-CINSEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS00-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS00-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS00-MTGCPLT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS00-MTGCPLT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS00-DANNUL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS00-DANNUL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS00-MTREMB-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS00-MTREMB-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS00-DREMB-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS00-DREMB-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS00-MTRACHAT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS00-MTRACHAT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS00-DRACHAT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS00-DRACHAT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS00-NOMCLI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS00-NOMCLI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS00-DTRAIT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS00-DTRAIT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL END DECLARE SECTION END-EXEC.
      *}                                                                        
       EJECT                                                                    
                                                                                
