      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFM0101                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM0101                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM0101.                                                            
           02  FM01-NENTITE                                                     
               PIC X(0005).                                                     
           02  FM01-NAUX                                                        
               PIC X(0003).                                                     
           02  FM01-LAUX                                                        
               PIC X(0030).                                                     
           02  FM01-WTYPAUX                                                     
               PIC X(0001).                                                     
           02  FM01-WSAISIE                                                     
               PIC X(0001).                                                     
           02  FM01-WAUXNCG                                                     
               PIC X(0001).                                                     
           02  FM01-WLETTRE                                                     
               PIC X(0001).                                                     
           02  FM01-WRELANCE                                                    
               PIC X(0001).                                                     
           02  FM01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  FM01-WAUXARF                                                     
               PIC X(0001).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFM0101                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM0101-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM01-NENTITE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM01-NENTITE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM01-NAUX-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM01-NAUX-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM01-LAUX-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM01-LAUX-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM01-WTYPAUX-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM01-WTYPAUX-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM01-WSAISIE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM01-WSAISIE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM01-WAUXNCG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM01-WAUXNCG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM01-WLETTRE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM01-WLETTRE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM01-WRELANCE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM01-WRELANCE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM01-WAUXARF-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM01-WAUXARF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
