      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00010000
      *   COPY DE LA TABLE RVFM8900                                     00020000
      **********************************************************        00030000
      *                                                                 00040000
      *---------------------------------------------------------        00050000
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM8900                 00060000
      *---------------------------------------------------------        00070000
      *                                                                 00080000
       01  RVFM8900.                                                    00090000
           02  FM89-CINTERFACE                                          00100000
               PIC X(0005).                                             00110000
           02  FM89-CZONE                                               00111000
               PIC X(0002).                                             00130000
           02  FM89-LIBELLE                                             00141401
               PIC X(0030).                                             00141501
           02  FM89-NPOS                                                00141601
               PIC S9(3) COMP-3.                                        00143003
           02  FM89-NLONG                                               00143100
               PIC S9(3) COMP-3.                                        00145003
           02  FM89-DMAJ                                                00146000
               PIC X(0008).                                             00147000
           02  FM89-DSYST                                               00148000
               PIC S9(13) COMP-3.                                       00149000
      *                                                                 00149100
      *---------------------------------------------------------        00149200
      *   LISTE DES FLAGS DE LA TABLE RVFM8900                          00149300
      *---------------------------------------------------------        00149400
      *                                                                 00149500
       01  RVFM8900-FLAGS.                                              00149600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM89-CINTERFACE-F                                        00149700
      *        PIC S9(4) COMP.                                          00149800
      *--                                                                       
           02  FM89-CINTERFACE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM89-CZONE-F                                             00149900
      *        PIC S9(4) COMP.                                          00150000
      *--                                                                       
           02  FM89-CZONE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM89-LIBELLE-F                                           00150300
      *        PIC S9(4) COMP.                                          00150400
      *--                                                                       
           02  FM89-LIBELLE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM89-NPOS-F                                              00150500
      *        PIC S9(4) COMP.                                          00150600
      *--                                                                       
           02  FM89-NPOS-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM89-NLONG-F                                             00150700
      *        PIC S9(4) COMP.                                          00150800
      *--                                                                       
           02  FM89-NLONG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM89-DMAJ-F                                              00150900
      *        PIC S9(4) COMP.                                          00151000
      *--                                                                       
           02  FM89-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM89-DSYST-F                                             00152000
      *        PIC S9(4) COMP.                                          00152400
      *--                                                                       
           02  FM89-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00153000
                                                                                
