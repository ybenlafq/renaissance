      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVFI1001                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFI1001                         
      **********************************************************                
       01  RVFI1001.                                                            
           02  FI10-DMVT                                                        
               PIC X(0008).                                                     
           02  FI10-DCOMPTA                                                     
               PIC X(0008).                                                     
           02  FI10-NFACTURE                                                    
               PIC X(0007).                                                     
           02  FI10-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  FI10-NLIEU                                                       
               PIC X(0003).                                                     
           02  FI10-CTYPLIEU                                                    
               PIC X(0001).                                                     
           02  FI10-CSENS                                                       
               PIC X(0001).                                                     
           02  FI10-CTYPE                                                       
               PIC X(0005).                                                     
           02  FI10-NMUTATION                                                   
               PIC X(0007).                                                     
           02  FI10-NCODIC                                                      
               PIC X(0007).                                                     
           02  FI10-QMUTEE                                                      
               PIC S9(5) COMP-3.                                                
           02  FI10-PRMP                                                        
               PIC S9(7)V9(0006) COMP-3.                                        
           02  FI10-PCF                                                         
               PIC S9(7)V9(0006) COMP-3.                                        
           02  FI10-CTAUXTVA                                                    
               PIC X(0005).                                                     
           02  FI10-QFRAISGEST                                                  
               PIC S9(3)V9(0002) COMP-3.                                        
           02  FI10-QTXESCPT                                                    
               PIC S9(3)V9(0002) COMP-3.                                        
           02  FI10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  FI10-CTYPSOC                                                     
               PIC X(0003).                                                     
           02  FI10-PRAK                                                        
               PIC S9(7)V9(0006) COMP-3.                                        
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVFI1001                                  
      **********************************************************                
       01  RVFI1001-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FI10-DMVT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FI10-DMVT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FI10-DCOMPTA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FI10-DCOMPTA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FI10-NFACTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FI10-NFACTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FI10-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FI10-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FI10-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FI10-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FI10-CTYPLIEU-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FI10-CTYPLIEU-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FI10-CSENS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FI10-CSENS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FI10-CTYPE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FI10-CTYPE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FI10-NMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FI10-NMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FI10-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FI10-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FI10-QMUTEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FI10-QMUTEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FI10-PRMP-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FI10-PRMP-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FI10-PCF-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FI10-PCF-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FI10-CTAUXTVA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FI10-CTAUXTVA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FI10-QFRAISGEST-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FI10-QFRAISGEST-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FI10-QTXESCPT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FI10-QTXESCPT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FI10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FI10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FI10-CTYPSOC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FI10-CTYPSOC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FI10-PRAK-F                                                      
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  FI10-PRAK-F                                                      
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
