      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00010000
      *   COPY DE LA TABLE RVFM9800                                     00020001
      **********************************************************        00030000
      *                                                                 00040000
      *---------------------------------------------------------        00050000
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM9800                 00060001
      *---------------------------------------------------------        00070000
      *                                                                 00080000
       01  RVFM9800.                                                    00090001
           02  FM98-SOCREF                                              00100001
               PIC X(0005).                                             00110000
           02  FM98-CINTERFACE                                          00120001
               PIC X(0005).                                             00130001
           02  FM98-CNATOPER                                            00140001
               PIC X(0005).                                             00150000
           02  FM98-CTYPOPER                                            00160001
               PIC X(0005).                                             00170000
           02  FM98-CRITERE1                                            00180001
               PIC X(0005).                                             00190001
           02  FM98-CRITERE2                                            00200001
               PIC X(0005).                                             00210001
           02  FM98-CRITERE3                                            00220001
               PIC X(0005).                                             00230001
           02  FM98-CPTSAP                                              00240001
               PIC X(0008).                                             00250002
           02  FM98-CONTREPSAP                                          00260001
               PIC X(0008).                                             00270002
           02  FM98-DEFFET                                              00280001
               PIC X(0008).                                             00290001
           02  FM98-DMAJ                                                00300001
               PIC X(0008).                                             00310000
           02  FM98-DSYST                                               00320001
               PIC S9(13) COMP-3.                                       00330000
      *                                                                 00340000
      *---------------------------------------------------------        00350000
      *   LISTE DES FLAGS DE LA TABLE RVFM9800                          00360001
      *---------------------------------------------------------        00370000
      *                                                                 00380000
       01  RVFM9800-FLAGS.                                              00390001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM98-SOCREF-F                                            00400001
      *        PIC S9(4) COMP.                                          00410000
      *--                                                                       
           02  FM98-SOCREF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM98-CINTERFACE-F                                        00420001
      *        PIC S9(4) COMP.                                          00430001
      *--                                                                       
           02  FM98-CINTERFACE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM98-CNATOPER-F                                          00440001
      *        PIC S9(4) COMP.                                          00450001
      *--                                                                       
           02  FM98-CNATOPER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM98-CTYPOPER-F                                          00460001
      *        PIC S9(4) COMP.                                          00470001
      *--                                                                       
           02  FM98-CTYPOPER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM98-CRITERE1-F                                          00480001
      *        PIC S9(4) COMP.                                          00490001
      *--                                                                       
           02  FM98-CRITERE1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM98-CRITERE2-F                                          00500001
      *        PIC S9(4) COMP.                                          00510001
      *--                                                                       
           02  FM98-CRITERE2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM98-CRITERE3-F                                          00520001
      *        PIC S9(4) COMP.                                          00530001
      *--                                                                       
           02  FM98-CRITERE3-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM98-CPTSAP-F                                            00540001
      *        PIC S9(4) COMP.                                          00550001
      *--                                                                       
           02  FM98-CPTSAP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM98-CONTREPSAP-F                                        00560001
      *        PIC S9(4) COMP.                                          00570001
      *--                                                                       
           02  FM98-CONTREPSAP-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM98-DEFFET-F                                            00580001
      *        PIC S9(4) COMP.                                          00590000
      *--                                                                       
           02  FM98-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM98-DMAJ-F                                              00600001
      *        PIC S9(4) COMP.                                          00610000
      *--                                                                       
           02  FM98-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM98-DSYST-F                                             00620001
      *        PIC S9(4) COMP.                                          00630000
      *--                                                                       
           02  FM98-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00640000
                                                                                
