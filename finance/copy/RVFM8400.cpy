      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************        00010000
      *   COPY DE LA TABLE RVFM8400                                     00020000
      **********************************************************        00030000
      *                                                                 00040000
      *---------------------------------------------------------        00050000
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM8400                 00060000
      *---------------------------------------------------------        00070000
      *                                                                 00080000
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVFM8400.                                                    00090000
           02  FM84-CINTERFACE                                          00100001
               PIC X(0005).                                             00110000
           02  FM84-CNATOPER                                            00120000
               PIC X(0005).                                             00130000
           02  FM84-CTYPOPER                                            00140000
               PIC X(0005).                                             00150000
           02  FM84-CTVA                                                00160000
               PIC X(0005).                                             00170000
           02  FM84-CGEO                                                00180000
               PIC X(0001).                                             00190000
           02  FM84-WGROUPE                                             00200002
               PIC X(0001).                                             00210000
           02  FM84-COMPTE                                              00220000
               PIC X(0006).                                             00230000
           02  FM84-CSENS                                               00240000
               PIC X(0001).                                             00250000
           02  FM84-WCUMUL                                              00260000
               PIC X(0001).                                             00270000
           02  FM84-NLETTRAGE                                           00280000
               PIC X(0001).                                             00290000
           02  FM84-NTIERS                                              00300000
               PIC X(0001).                                             00310000
           02  FM84-WANAL                                               00320003
               PIC X(0001).                                             00330003
           02  FM84-CONTREP                                             00340000
               PIC X(0006).                                             00350000
           02  FM84-WCUMULC                                             00360000
               PIC X(0001).                                             00370000
           02  FM84-NLETTRAGEC                                          00380000
               PIC X(0001).                                             00390000
           02  FM84-NTIERSC                                             00400000
               PIC X(0001).                                             00410000
           02  FM84-WANALC                                              00420003
               PIC X(0001).                                             00430000
           02  FM84-DEFFET                                              00440000
               PIC X(0008).                                             00450000
           02  FM84-DMAJ                                                00460000
               PIC X(0008).                                             00470000
           02  FM84-DSYST                                               00480000
               PIC S9(13) COMP-3.                                       00490000
      *                                                                 00500000
      *---------------------------------------------------------        00510000
      *   LISTE DES FLAGS DE LA TABLE RVFM8400                          00520000
      *---------------------------------------------------------        00530000
      *                                                                 00540000
       01  RVFM8400-FLAGS.                                              00550000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM84-CINTERFACE-F                                        00560001
      *        PIC S9(4) COMP.                                          00570000
      *--                                                                       
           02  FM84-CINTERFACE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM84-CNATOPER-F                                          00580000
      *        PIC S9(4) COMP.                                          00590000
      *--                                                                       
           02  FM84-CNATOPER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM84-CTYPOPER-F                                          00600000
      *        PIC S9(4) COMP.                                          00610000
      *--                                                                       
           02  FM84-CTYPOPER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM84-CTVA-F                                              00620000
      *        PIC S9(4) COMP.                                          00630000
      *--                                                                       
           02  FM84-CTVA-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM84-CGEO-F                                              00640000
      *        PIC S9(4) COMP.                                          00650000
      *--                                                                       
           02  FM84-CGEO-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM84-WGROUPE-F                                           00660002
      *        PIC S9(4) COMP.                                          00670000
      *--                                                                       
           02  FM84-WGROUPE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM84-COMPTE-F                                            00680000
      *        PIC S9(4) COMP.                                          00690000
      *--                                                                       
           02  FM84-COMPTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM84-CSENS-F                                             00700000
      *        PIC S9(4) COMP.                                          00710000
      *--                                                                       
           02  FM84-CSENS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM84-WCUMUL-F                                            00720000
      *        PIC S9(4) COMP.                                          00730000
      *--                                                                       
           02  FM84-WCUMUL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM84-NLETTRAGE-F                                         00740000
      *        PIC S9(4) COMP.                                          00750000
      *--                                                                       
           02  FM84-NLETTRAGE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM84-NTIERS-F                                            00760000
      *        PIC S9(4) COMP.                                          00770000
      *--                                                                       
           02  FM84-NTIERS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM84-WANAL-F                                             00780003
      *        PIC S9(4) COMP.                                          00790003
      *--                                                                       
           02  FM84-WANAL-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM84-CONTREP-F                                           00800000
      *        PIC S9(4) COMP.                                          00810000
      *--                                                                       
           02  FM84-CONTREP-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM84-WCUMULC-F                                           00820000
      *        PIC S9(4) COMP.                                          00830000
      *--                                                                       
           02  FM84-WCUMULC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM84-NLETTRAGEC-F                                        00840000
      *        PIC S9(4) COMP.                                          00850000
      *--                                                                       
           02  FM84-NLETTRAGEC-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM84-NTIERSC-F                                           00860000
      *        PIC S9(4) COMP.                                          00870000
      *--                                                                       
           02  FM84-NTIERSC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM84-WANALC-F                                            00880003
      *        PIC S9(4) COMP.                                          00890000
      *--                                                                       
           02  FM84-WANALC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM84-DEFFET-F                                            00900000
      *        PIC S9(4) COMP.                                          00910000
      *--                                                                       
           02  FM84-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM84-DMAJ-F                                              00920000
      *        PIC S9(4) COMP.                                          00930000
      *--                                                                       
           02  FM84-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM84-DSYST-F                                             00940000
      *        PIC S9(4) COMP.                                          00950000
      *--                                                                       
           02  FM84-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL END DECLARE SECTION END-EXEC.
      *}                                                                        
       EJECT                                                            00960000
                                                                                
