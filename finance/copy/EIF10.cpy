      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * MENU MODES DE COMPTABILISATION                                  00000020
      ***************************************************************** 00000030
       01   EIF10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(2).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(2).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * ZONE CMD AIDA                                                   00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000180
           02 FILLER    PIC X(2).                                       00000190
           02 MZONCMDI  PIC X(15).                                      00000200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMODPAIL  COMP PIC S9(4).                                 00000210
      *--                                                                       
           02 MMODPAIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMODPAIF  PIC X.                                          00000220
           02 FILLER    PIC X(2).                                       00000230
           02 MMODPAII  PIC X(3).                                       00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPCTAL  COMP PIC S9(4).                                 00000250
      *--                                                                       
           02 MTYPCTAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPCTAF  PIC X.                                          00000260
           02 FILLER    PIC X(2).                                       00000270
           02 MTYPCTAI  PIC X(3).                                       00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPMTL  COMP PIC S9(4).                                 00000290
      *--                                                                       
           02 MCTYPMTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTYPMTF  PIC X.                                          00000300
           02 FILLER    PIC X(2).                                       00000310
           02 MCTYPMTI  PIC X(5).                                       00000320
      * MESSAGE ERREUR                                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000350
           02 FILLER    PIC X(2).                                       00000360
           02 MLIBERRI  PIC X(79).                                      00000370
      * CODE TRANSACTION                                                00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000390
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000400
           02 FILLER    PIC X(2).                                       00000410
           02 MCODTRAI  PIC X(4).                                       00000420
      * CICS DE TRAVAIL                                                 00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000450
           02 FILLER    PIC X(2).                                       00000460
           02 MCICSI    PIC X(5).                                       00000470
      * NETNAME                                                         00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000490
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000500
           02 FILLER    PIC X(2).                                       00000510
           02 MNETNAMI  PIC X(8).                                       00000520
      * CODE TERMINAL                                                   00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000550
           02 FILLER    PIC X(2).                                       00000560
           02 MSCREENI  PIC X(5).                                       00000570
      ***************************************************************** 00000580
      * MENU MODES DE COMPTABILISATION                                  00000590
      ***************************************************************** 00000600
       01   EIF10O REDEFINES EIF10I.                                    00000610
           02 FILLER    PIC X(12).                                      00000620
      * DATE DU JOUR                                                    00000630
           02 FILLER    PIC X(2).                                       00000640
           02 MDATJOUA  PIC X.                                          00000650
           02 MDATJOUC  PIC X.                                          00000660
           02 MDATJOUH  PIC X.                                          00000670
           02 MDATJOUO  PIC X(10).                                      00000680
      * HEURE                                                           00000690
           02 FILLER    PIC X(2).                                       00000700
           02 MTIMJOUA  PIC X.                                          00000710
           02 MTIMJOUC  PIC X.                                          00000720
           02 MTIMJOUH  PIC X.                                          00000730
           02 MTIMJOUO  PIC X(5).                                       00000740
      * ZONE CMD AIDA                                                   00000750
           02 FILLER    PIC X(2).                                       00000760
           02 MZONCMDA  PIC X.                                          00000770
           02 MZONCMDC  PIC X.                                          00000780
           02 MZONCMDH  PIC X.                                          00000790
           02 MZONCMDO  PIC X(15).                                      00000800
           02 FILLER    PIC X(2).                                       00000810
           02 MMODPAIA  PIC X.                                          00000820
           02 MMODPAIC  PIC X.                                          00000830
           02 MMODPAIH  PIC X.                                          00000840
           02 MMODPAIO  PIC X(3).                                       00000850
           02 FILLER    PIC X(2).                                       00000860
           02 MTYPCTAA  PIC X.                                          00000870
           02 MTYPCTAC  PIC X.                                          00000880
           02 MTYPCTAH  PIC X.                                          00000890
           02 MTYPCTAO  PIC X(3).                                       00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MCTYPMTA  PIC X.                                          00000920
           02 MCTYPMTC  PIC X.                                          00000930
           02 MCTYPMTH  PIC X.                                          00000940
           02 MCTYPMTO  PIC X(5).                                       00000950
      * MESSAGE ERREUR                                                  00000960
           02 FILLER    PIC X(2).                                       00000970
           02 MLIBERRA  PIC X.                                          00000980
           02 MLIBERRC  PIC X.                                          00000990
           02 MLIBERRH  PIC X.                                          00001000
           02 MLIBERRO  PIC X(79).                                      00001010
      * CODE TRANSACTION                                                00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MCODTRAA  PIC X.                                          00001040
           02 MCODTRAC  PIC X.                                          00001050
           02 MCODTRAH  PIC X.                                          00001060
           02 MCODTRAO  PIC X(4).                                       00001070
      * CICS DE TRAVAIL                                                 00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MCICSA    PIC X.                                          00001100
           02 MCICSC    PIC X.                                          00001110
           02 MCICSH    PIC X.                                          00001120
           02 MCICSO    PIC X(5).                                       00001130
      * NETNAME                                                         00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MNETNAMA  PIC X.                                          00001160
           02 MNETNAMC  PIC X.                                          00001170
           02 MNETNAMH  PIC X.                                          00001180
           02 MNETNAMO  PIC X(8).                                       00001190
      * CODE TERMINAL                                                   00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MSCREENA  PIC X.                                          00001220
           02 MSCREENC  PIC X.                                          00001230
           02 MSCREENH  PIC X.                                          00001240
           02 MSCREENO  PIC X(5).                                       00001250
                                                                                
