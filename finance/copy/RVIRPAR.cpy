      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE IRPAR PARAMETRAGE RACHAT               *        
      *----------------------------------------------------------------*        
       01  RVIRPAR .                                                            
           05  IRPAR-CTABLEG2    PIC X(15).                                     
           05  IRPAR-CTABLEG2-REDEF REDEFINES IRPAR-CTABLEG2.                   
               10  IRPAR-CODE            PIC X(06).                             
               10  IRPAR-WANNEE          PIC X(04).                             
               10  IRPAR-WMOIS           PIC X(02).                             
           05  IRPAR-WTABLEG     PIC X(80).                                     
           05  IRPAR-WTABLEG-REDEF  REDEFINES IRPAR-WTABLEG.                    
               10  IRPAR-REEL            PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVIRPAR-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IRPAR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  IRPAR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IRPAR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  IRPAR-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
