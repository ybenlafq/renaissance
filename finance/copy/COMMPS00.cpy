      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      *         MAQUETTE COMMAREA STANDARD AIDA COBOL2             *            
      **************************************************************            
      *                                                                         
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET                    
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR          
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU                  
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                            
      *                                                                         
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                              
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES               
      * PAR AIDA                                                                
      *                                                                         
      *-----------------------------------------------------------------        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-PS00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-PS00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA -----------------------------------------        
           02 FILLER-COM-AIDA      PIC X(100).                                  
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS --------------------------        
           02 COMM-CICS-APPLID     PIC X(8).                                    
           02 COMM-CICS-NETNAM     PIC X(8).                                    
           02 COMM-CICS-TRANSA     PIC X(4).                                    
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------------        
           02 COMM-DATE-SIECLE     PIC XX.                                      
           02 COMM-DATE-ANNEE      PIC XX.                                      
           02 COMM-DATE-MOIS       PIC XX.                                      
           02 COMM-DATE-JOUR       PIC 99.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA       PIC 999.                                     
           02 COMM-DATE-QNT0       PIC 99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX       PIC 9.                                       
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM        PIC 9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC     PIC XXX.                                     
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC    PIC XXX.                                     
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                                    
           02 COMM-DATE-AAMMJJ     PIC X(6).                                    
           02 COMM-DATE-JJMMSSAA   PIC X(8).                                    
           02 COMM-DATE-JJMMAA     PIC X(6).                                    
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                                    
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS   PIC 99.                                      
              05 COMM-DATE-SEMAA   PIC 99.                                      
              05 COMM-DATE-SEMNU   PIC 99.                                      
      *                                                                         
           02 COMM-DATE-FILLER     PIC X(08).                                   
      *                                                                         
      * ZONES RESERVEES TRAITEMENT DU SWAP -----------------------------        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                     
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 180 PIC X(1).                               
      *                                                                         
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*        
      *>>>>>>>>>>>>>>>>> ZONES RESERVEES APPLICATIVES <<<<<<<<<<<<<<<<<*        
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*        
      *                                                                         
      *----------------------------------------------------------------*        
      * MENU GENERAL DE L'APPLICATION (PROGRAMME TPS00)                *        
      *----------------------------------------------------------------*        
           02 COMM-PS00-APPLI.                                                  
      *----------------------------------------------------------------*        
              03 COMM-PS00-FIC.                                                 
                 04 COMM-PS00-NSOC           PIC X(3).                          
                 04 COMM-PS00-CGCPLT         PIC X(5).                          
                 04 COMM-PS00-NGCPLT         PIC X(8).                          
                 04 COMM-PS00-NLIEU          PIC X(3).                          
                 04 COMM-PS00-NVENTE         PIC X(7).                          
                 04 COMM-PS00-DCOMPTA        PIC X(8).                          
                 04 COMM-PS00-NCODIC         PIC X(7).                          
                 04 COMM-PS00-MTGCPLT        PIC S9(5)V99 COMP-3.               
                 04 COMM-PS00-DANNUL         PIC X(8).                          
                 04 COMM-PS00-MTREMB         PIC S9(5)V99 COMP-3.               
                 04 COMM-PS00-DREMB          PIC X(8).                          
                 04 COMM-PS00-MTRACHAT       PIC S9(5)V99 COMP-3.               
                 04 COMM-PS00-DRACHAT        PIC X(8).                          
                 04 COMM-PS00-NOMCLI         PIC X(15).                         
                 04 COMM-PS00-DTRAITR        PIC X(8).                          
                 04 COMM-PS00-LREF           PIC X(20).                         
                 04 COMM-PS00-CFAM           PIC X(5).                          
                 04 COMM-PS00-CMARQ          PIC X(5).                          
      *----------------------------------------------------------------*        
                 04 COMM-PS00-FONC           PIC X(3).                          
                 04 COMM-PS00-PAGE           PIC 9(2).                          
                 04 COMM-PS00-PAGET          PIC 9(2).                          
                 04 COMM-PS00-LLIEU          PIC X(20).                         
                 04 COMM-PS00-LGCPLT         PIC X(20).                         
                 04 COMM-PS00-CINSEE         PIC X(05).                         
                 04 COMM-PS00-MAG            PIC    X VALUE '0'.                
                    88 COMM-PS00-MAG-N       VALUE '0'.                         
                    88 COMM-PS00-MAG-O       VALUE '1'.                         
              03 COMM-PS00-PROG              PIC X(3500).                       
      *----------------------------------------------------------------*        
      * POUR LA TRANSACTION PS02  PRIVEE-------------------------------*        
      *..............................................................           
              03 COMM-PS02-PROG    REDEFINES COMM-PS00-PROG.                    
      *..............................................TOTAL DES PAGES            
                 04 COMM-PS02-DJR            PIC X.                             
      *............................................DATE FIN GARANTIE            
                 04 COMM-PS02-DFGCPLT        PIC X(8).                          
      *............................................LIBELLE COMMUNE              
                 04 COMM-PS02-LCOMMUNE       PIC X(32).                         
      *............................................DATE DE TRAITEMENT           
                 04 COMM-PS02-DTRAITR        PIC X(08).                         
      *                                                                         
                 04 COMM-PS02-FILLER         PIC X(3434).                       
      *----------------------------------------------------------------*        
      * POUR LA TRANSACTION PS03  PRIVEE-------------------------------*        
      *..............................................................           
              03 COMM-PS03-PROG    REDEFINES COMM-PS00-PROG.                    
      *..............................................DATE DEBUT PER             
                 04 COMM-PS03-DATEDEB        PIC X(8).                          
      *............................................DATE FIN PER                 
                 04 COMM-PS03-DATEFIN        PIC X(8).                          
      *                                                                         
                 04 COMM-PS03-FILLER         PIC X(3484).                       
                                                                                
