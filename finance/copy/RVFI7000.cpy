      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RVFI7000                    *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFI7000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFI7000.                                                            
      *}                                                                        
      *                       ID                                                
           10 FI70-ID              PIC X(15).                                   
      *                       CODAPP                                            
           10 FI70-CODAPP          PIC X(10).                                   
      *                       COPCO                                             
           10 FI70-COPCO           PIC X(6).                                    
      *                       TYPMVT                                            
           10 FI70-TYPMVT          PIC X(15).                                   
      *                       IDMVT                                             
           10 FI70-IDMVT           PIC X(20).                                   
      *                       NUMORIG                                           
           10 FI70-NUMORIG         PIC X(20).                                   
      *                       DMVT                                              
           10 FI70-DMVT            PIC X(8).                                    
      *                       HMVT                                              
           10 FI70-HMVT            PIC X(6).                                    
      *                       NSOCORIG                                          
           10 FI70-NSOCORIG        PIC X(3).                                    
      *                       NLIEUORIG                                         
           10 FI70-NLIEUORIG       PIC X(3).                                    
      *                       NSOCDEST                                          
           10 FI70-NSOCDEST        PIC X(3).                                    
      *                       NLIEUDEST                                         
           10 FI70-NLIEUDEST       PIC X(3).                                    
      *                       NSOCVTE                                           
           10 FI70-NSOCVTE         PIC X(3).                                    
      *                       NLIEUVTE                                          
           10 FI70-NLIEUVTE        PIC X(3).                                    
      *                       NSEQ                                              
           10 FI70-NSEQ            PIC X(2).                                    
      *                       NARTICLE                                          
           10 FI70-NARTICLE        PIC X(7).                                    
      *                       REFERENCE                                         
           10 FI70-REFERENCE       PIC X(40).                                   
      *                       LIBELLE                                           
           10 FI70-LIBELLE         PIC X(40).                                   
      *                       QTE                                               
           10 FI70-QTE             PIC S9(7)V USAGE COMP-3.                     
      *                       CMODDEL                                           
           10 FI70-CMODDEL         PIC X(5).                                    
      *                       IDLOG                                             
           10 FI70-IDLOG           PIC X(15).                                   
      *                       PTTC                                              
           10 FI70-PTTC            PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       PHT                                               
           10 FI70-PHT             PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       TXTVA                                             
           10 FI70-TXTVA           PIC S9(2)V9(2) USAGE COMP-3.                 
      *                       LANO                                              
           10 FI70-LANO            PIC X(5).                                    
      *                       DCREATION                                         
           10 FI70-DCREATION       PIC X(8).                                    
      *                       DTRAIT                                            
           10 FI70-DTRAIT          PIC X(8).                                    
      *                       DSYST                                             
           10 FI70-DSYST           PIC S9(13)V USAGE COMP-3.                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 28      *        
      ******************************************************************        
                                                                                
