      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFM6001                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM6001                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM6001.                                                            
           02  FM60-CPCG                                                        
               PIC X(0004).                                                     
           02  FM60-COMPTE                                                      
               PIC X(0006).                                                     
           02  FM60-LCOMPTEC                                                    
               PIC X(0015).                                                     
           02  FM60-LCOMPTEL                                                    
               PIC X(0030).                                                     
           02  FM60-CMASQUE                                                     
               PIC X(0006).                                                     
           02  FM60-CCONSOD                                                     
               PIC X(0006).                                                     
           02  FM60-CCONSOC                                                     
               PIC X(0006).                                                     
           02  FM60-NAUX                                                        
               PIC X(0003).                                                     
           02  FM60-WTYPCOMPTE                                                  
               PIC X(0001).                                                     
           02  FM60-WAUXILIARISE                                                
               PIC X(0001).                                                     
           02  FM60-WCOLLECTIF                                                  
               PIC X(0001).                                                     
           02  FM60-WABONNE                                                     
               PIC X(0001).                                                     
           02  FM60-WREGAUTO                                                    
               PIC X(0001).                                                     
           02  FM60-WCATNATCH                                                   
               PIC X(0001).                                                     
           02  FM60-WLETTRAGE                                                   
               PIC X(0001).                                                     
           02  FM60-WPOINTAGE                                                   
               PIC X(0001).                                                     
           02  FM60-WEDITION                                                    
               PIC X(0001).                                                     
           02  FM60-WCLOTURE                                                    
               PIC X(0001).                                                     
           02  FM60-WINTERFACE                                                  
               PIC X(0001).                                                     
           02  FM60-CEPURATION                                                  
               PIC X(0001).                                                     
           02  FM60-DCLOTURE                                                    
               PIC X(0008).                                                     
           02  FM60-DMAJ                                                        
               PIC X(0008).                                                     
           02  FM60-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  FM60-WREGAUTOC                                                   
               PIC X(0001).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFM6001                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM6001-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM60-CPCG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM60-CPCG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM60-COMPTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM60-COMPTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM60-LCOMPTEC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM60-LCOMPTEC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM60-LCOMPTEL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM60-LCOMPTEL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM60-CMASQUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM60-CMASQUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM60-CCONSOD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM60-CCONSOD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM60-CCONSOC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM60-CCONSOC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM60-NAUX-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM60-NAUX-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM60-WTYPCOMPTE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM60-WTYPCOMPTE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM60-WAUXILIARISE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM60-WAUXILIARISE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM60-WCOLLECTIF-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM60-WCOLLECTIF-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM60-WABONNE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM60-WABONNE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM60-WREGAUTO-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM60-WREGAUTO-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM60-WCATNATCH-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM60-WCATNATCH-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM60-WLETTRAGE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM60-WLETTRAGE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM60-WPOINTAGE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM60-WPOINTAGE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM60-WEDITION-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM60-WEDITION-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM60-WCLOTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM60-WCLOTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM60-WINTERFACE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM60-WINTERFACE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM60-CEPURATION-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM60-CEPURATION-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM60-DCLOTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM60-DCLOTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM60-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM60-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM60-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM60-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM60-WREGAUTOC-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM60-WREGAUTOC-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
