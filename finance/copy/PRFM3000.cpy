      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE FM3000                       
      ******************************************************************        
      *                                                                         
       CLEF-FM3000             SECTION.                                         
      *                                                                         
           MOVE 'RVFM3000          '       TO   TABLE-NAME.                     
           MOVE 'FM3000'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-FM3000. EXIT.                                                   
                EJECT                                                           
                                                                                
