      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFG20   EFG20                                              00000020
      ***************************************************************** 00000030
       01   EFG20I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MNSOCI    PIC X(3).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNLIEUI   PIC X(3).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MLLIEUI   PIC X(19).                                      00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCOMPTL  COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MSCOMPTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCOMPTF  PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MSCOMPTI  PIC X(3).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MECOMPTL  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MECOMPTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MECOMPTF  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MECOMPTI  PIC X(3).                                       00000350
      * OPTION                                                          00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MZONCMDI  PIC X.                                          00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBTTESL      COMP PIC S9(4).                            00000410
      *--                                                                       
           02 MLIBTTESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBTTESF      PIC X.                                     00000420
           02 FILLER    PIC X(4).                                       00000430
           02 MLIBTTESI      PIC X(22).                                 00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONTTESL      COMP PIC S9(4).                            00000450
      *--                                                                       
           02 MZONTTESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MZONTTESF      PIC X.                                     00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MZONTTESI      PIC X.                                     00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSECTIONL      COMP PIC S9(4).                            00000490
      *--                                                                       
           02 MSECTIONL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSECTIONF      PIC X.                                     00000500
           02 FILLER    PIC X(4).                                       00000510
           02 MSECTIONI      PIC X(6).                                  00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCIMPL    COMP PIC S9(4).                                 00000530
      *--                                                                       
           02 MCIMPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCIMPF    PIC X.                                          00000540
           02 FILLER    PIC X(4).                                       00000550
           02 MCIMPI    PIC X(4).                                       00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFRAISL  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MNFRAISL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNFRAISF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MNFRAISI  PIC X(6).                                       00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNEXERCL  COMP PIC S9(4).                                 00000610
      *--                                                                       
           02 MNEXERCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNEXERCF  PIC X.                                          00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MNEXERCI  PIC X(4).                                       00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPERIODL      COMP PIC S9(4).                            00000650
      *--                                                                       
           02 MNPERIODL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNPERIODF      PIC X.                                     00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MNPERIODI      PIC X(3).                                  00000680
      * correspondance compta                                           00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIB5L    COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MLIB5L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIB5F    PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLIB5I    PIC X(55).                                      00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMSOC4L      COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MNUMSOC4L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMSOC4F      PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MNUMSOC4I      PIC X(3).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBLIEUL      COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MLIBLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBLIEUF      PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLIBLIEUI      PIC X(23).                                 00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSGESTL   COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MSGESTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSGESTF   PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MSGESTI   PIC X(3).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLGESTL   COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MLGESTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLGESTF   PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MLGESTI   PIC X(3).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIB6L    COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MLIB6L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIB6F    PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MLIB6I    PIC X(60).                                      00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIB7L    COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MLIB7L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIB7F    PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MLIB7I    PIC X(55).                                      00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVALEURL      COMP PIC S9(4).                            00000980
      *--                                                                       
           02 MDVALEURL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDVALEURF      PIC X.                                     00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MDVALEURI      PIC X(10).                                 00001010
      * MESSAGE ERREUR                                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MLIBERRI  PIC X(78).                                      00001060
      * CODE TRANSACTION                                                00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001080
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001090
           02 FILLER    PIC X(4).                                       00001100
           02 MCODTRAI  PIC X(4).                                       00001110
      * CICS DE TRAVAIL                                                 00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001130
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001140
           02 FILLER    PIC X(4).                                       00001150
           02 MCICSI    PIC X(5).                                       00001160
      * NETNAME                                                         00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MNETNAMI  PIC X(8).                                       00001210
      * CODE TERMINAL                                                   00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MSCREENI  PIC X(5).                                       00001260
      ***************************************************************** 00001270
      * SDF: EFG20   EFG20                                              00001280
      ***************************************************************** 00001290
       01   EFG20O REDEFINES EFG20I.                                    00001300
           02 FILLER    PIC X(12).                                      00001310
      * DATE DU JOUR                                                    00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MDATJOUA  PIC X.                                          00001340
           02 MDATJOUC  PIC X.                                          00001350
           02 MDATJOUP  PIC X.                                          00001360
           02 MDATJOUH  PIC X.                                          00001370
           02 MDATJOUV  PIC X.                                          00001380
           02 MDATJOUO  PIC X(10).                                      00001390
      * HEURE                                                           00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MTIMJOUA  PIC X.                                          00001420
           02 MTIMJOUC  PIC X.                                          00001430
           02 MTIMJOUP  PIC X.                                          00001440
           02 MTIMJOUH  PIC X.                                          00001450
           02 MTIMJOUV  PIC X.                                          00001460
           02 MTIMJOUO  PIC X(5).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MNSOCA    PIC X.                                          00001490
           02 MNSOCC    PIC X.                                          00001500
           02 MNSOCP    PIC X.                                          00001510
           02 MNSOCH    PIC X.                                          00001520
           02 MNSOCV    PIC X.                                          00001530
           02 MNSOCO    PIC X(3).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MNLIEUA   PIC X.                                          00001560
           02 MNLIEUC   PIC X.                                          00001570
           02 MNLIEUP   PIC X.                                          00001580
           02 MNLIEUH   PIC X.                                          00001590
           02 MNLIEUV   PIC X.                                          00001600
           02 MNLIEUO   PIC X(3).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MLLIEUA   PIC X.                                          00001630
           02 MLLIEUC   PIC X.                                          00001640
           02 MLLIEUP   PIC X.                                          00001650
           02 MLLIEUH   PIC X.                                          00001660
           02 MLLIEUV   PIC X.                                          00001670
           02 MLLIEUO   PIC X(19).                                      00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MSCOMPTA  PIC X.                                          00001700
           02 MSCOMPTC  PIC X.                                          00001710
           02 MSCOMPTP  PIC X.                                          00001720
           02 MSCOMPTH  PIC X.                                          00001730
           02 MSCOMPTV  PIC X.                                          00001740
           02 MSCOMPTO  PIC X(3).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MECOMPTA  PIC X.                                          00001770
           02 MECOMPTC  PIC X.                                          00001780
           02 MECOMPTP  PIC X.                                          00001790
           02 MECOMPTH  PIC X.                                          00001800
           02 MECOMPTV  PIC X.                                          00001810
           02 MECOMPTO  PIC X(3).                                       00001820
      * OPTION                                                          00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MZONCMDA  PIC X.                                          00001850
           02 MZONCMDC  PIC X.                                          00001860
           02 MZONCMDP  PIC X.                                          00001870
           02 MZONCMDH  PIC X.                                          00001880
           02 MZONCMDV  PIC X.                                          00001890
           02 MZONCMDO  PIC X.                                          00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MLIBTTESA      PIC X.                                     00001920
           02 MLIBTTESC PIC X.                                          00001930
           02 MLIBTTESP PIC X.                                          00001940
           02 MLIBTTESH PIC X.                                          00001950
           02 MLIBTTESV PIC X.                                          00001960
           02 MLIBTTESO      PIC X(22).                                 00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MZONTTESA      PIC X.                                     00001990
           02 MZONTTESC PIC X.                                          00002000
           02 MZONTTESP PIC X.                                          00002010
           02 MZONTTESH PIC X.                                          00002020
           02 MZONTTESV PIC X.                                          00002030
           02 MZONTTESO      PIC X.                                     00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MSECTIONA      PIC X.                                     00002060
           02 MSECTIONC PIC X.                                          00002070
           02 MSECTIONP PIC X.                                          00002080
           02 MSECTIONH PIC X.                                          00002090
           02 MSECTIONV PIC X.                                          00002100
           02 MSECTIONO      PIC X(6).                                  00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MCIMPA    PIC X.                                          00002130
           02 MCIMPC    PIC X.                                          00002140
           02 MCIMPP    PIC X.                                          00002150
           02 MCIMPH    PIC X.                                          00002160
           02 MCIMPV    PIC X.                                          00002170
           02 MCIMPO    PIC X(4).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MNFRAISA  PIC X.                                          00002200
           02 MNFRAISC  PIC X.                                          00002210
           02 MNFRAISP  PIC X.                                          00002220
           02 MNFRAISH  PIC X.                                          00002230
           02 MNFRAISV  PIC X.                                          00002240
           02 MNFRAISO  PIC 9(6).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MNEXERCA  PIC X.                                          00002270
           02 MNEXERCC  PIC X.                                          00002280
           02 MNEXERCP  PIC X.                                          00002290
           02 MNEXERCH  PIC X.                                          00002300
           02 MNEXERCV  PIC X.                                          00002310
           02 MNEXERCO  PIC X(4).                                       00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MNPERIODA      PIC X.                                     00002340
           02 MNPERIODC PIC X.                                          00002350
           02 MNPERIODP PIC X.                                          00002360
           02 MNPERIODH PIC X.                                          00002370
           02 MNPERIODV PIC X.                                          00002380
           02 MNPERIODO      PIC X(3).                                  00002390
      * correspondance compta                                           00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MLIB5A    PIC X.                                          00002420
           02 MLIB5C    PIC X.                                          00002430
           02 MLIB5P    PIC X.                                          00002440
           02 MLIB5H    PIC X.                                          00002450
           02 MLIB5V    PIC X.                                          00002460
           02 MLIB5O    PIC X(55).                                      00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MNUMSOC4A      PIC X.                                     00002490
           02 MNUMSOC4C PIC X.                                          00002500
           02 MNUMSOC4P PIC X.                                          00002510
           02 MNUMSOC4H PIC X.                                          00002520
           02 MNUMSOC4V PIC X.                                          00002530
           02 MNUMSOC4O      PIC X(3).                                  00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MLIBLIEUA      PIC X.                                     00002560
           02 MLIBLIEUC PIC X.                                          00002570
           02 MLIBLIEUP PIC X.                                          00002580
           02 MLIBLIEUH PIC X.                                          00002590
           02 MLIBLIEUV PIC X.                                          00002600
           02 MLIBLIEUO      PIC X(23).                                 00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MSGESTA   PIC X.                                          00002630
           02 MSGESTC   PIC X.                                          00002640
           02 MSGESTP   PIC X.                                          00002650
           02 MSGESTH   PIC X.                                          00002660
           02 MSGESTV   PIC X.                                          00002670
           02 MSGESTO   PIC X(3).                                       00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MLGESTA   PIC X.                                          00002700
           02 MLGESTC   PIC X.                                          00002710
           02 MLGESTP   PIC X.                                          00002720
           02 MLGESTH   PIC X.                                          00002730
           02 MLGESTV   PIC X.                                          00002740
           02 MLGESTO   PIC X(3).                                       00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MLIB6A    PIC X.                                          00002770
           02 MLIB6C    PIC X.                                          00002780
           02 MLIB6P    PIC X.                                          00002790
           02 MLIB6H    PIC X.                                          00002800
           02 MLIB6V    PIC X.                                          00002810
           02 MLIB6O    PIC X(60).                                      00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MLIB7A    PIC X.                                          00002840
           02 MLIB7C    PIC X.                                          00002850
           02 MLIB7P    PIC X.                                          00002860
           02 MLIB7H    PIC X.                                          00002870
           02 MLIB7V    PIC X.                                          00002880
           02 MLIB7O    PIC X(55).                                      00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MDVALEURA      PIC X.                                     00002910
           02 MDVALEURC PIC X.                                          00002920
           02 MDVALEURP PIC X.                                          00002930
           02 MDVALEURH PIC X.                                          00002940
           02 MDVALEURV PIC X.                                          00002950
           02 MDVALEURO      PIC X(10).                                 00002960
      * MESSAGE ERREUR                                                  00002970
           02 FILLER    PIC X(2).                                       00002980
           02 MLIBERRA  PIC X.                                          00002990
           02 MLIBERRC  PIC X.                                          00003000
           02 MLIBERRP  PIC X.                                          00003010
           02 MLIBERRH  PIC X.                                          00003020
           02 MLIBERRV  PIC X.                                          00003030
           02 MLIBERRO  PIC X(78).                                      00003040
      * CODE TRANSACTION                                                00003050
           02 FILLER    PIC X(2).                                       00003060
           02 MCODTRAA  PIC X.                                          00003070
           02 MCODTRAC  PIC X.                                          00003080
           02 MCODTRAP  PIC X.                                          00003090
           02 MCODTRAH  PIC X.                                          00003100
           02 MCODTRAV  PIC X.                                          00003110
           02 MCODTRAO  PIC X(4).                                       00003120
      * CICS DE TRAVAIL                                                 00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MCICSA    PIC X.                                          00003150
           02 MCICSC    PIC X.                                          00003160
           02 MCICSP    PIC X.                                          00003170
           02 MCICSH    PIC X.                                          00003180
           02 MCICSV    PIC X.                                          00003190
           02 MCICSO    PIC X(5).                                       00003200
      * NETNAME                                                         00003210
           02 FILLER    PIC X(2).                                       00003220
           02 MNETNAMA  PIC X.                                          00003230
           02 MNETNAMC  PIC X.                                          00003240
           02 MNETNAMP  PIC X.                                          00003250
           02 MNETNAMH  PIC X.                                          00003260
           02 MNETNAMV  PIC X.                                          00003270
           02 MNETNAMO  PIC X(8).                                       00003280
      * CODE TERMINAL                                                   00003290
           02 FILLER    PIC X(2).                                       00003300
           02 MSCREENA  PIC X.                                          00003310
           02 MSCREENC  PIC X.                                          00003320
           02 MSCREENP  PIC X.                                          00003330
           02 MSCREENH  PIC X.                                          00003340
           02 MSCREENV  PIC X.                                          00003350
           02 MSCREENO  PIC X(5).                                       00003360
                                                                                
