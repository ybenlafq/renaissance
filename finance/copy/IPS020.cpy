      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPS020 AU 20/05/1996  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPS020.                                                        
            05 NOMETAT-IPS020           PIC X(6) VALUE 'IPS020'.                
            05 RUPTURES-IPS020.                                                 
           10 IPS020-CGRP               PIC X(05).                      007  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPS020-SEQUENCE           PIC S9(04) COMP.                012  002
      *--                                                                       
           10 IPS020-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPS020.                                                   
           10 IPS020-CGCPLT             PIC X(05).                      014  005
           10 IPS020-LGRP               PIC X(21).                      019  021
           10 IPS020-NBCMD              PIC X(10).                      040  010
           10 IPS020-NCHEQUE            PIC X(07).                      050  007
           10 IPS020-NGCPLT             PIC X(08).                      057  008
           10 IPS020-NOMCLI             PIC X(15).                      065  015
           10 IPS020-NSOC               PIC X(03).                      080  003
           10 IPS020-MTGCPLT            PIC S9(05)V9(2) COMP-3.         083  004
           10 IPS020-MTREMB             PIC S9(05)V9(2) COMP-3.         087  004
           10 IPS020-DATEDEB            PIC X(06).                      091  006
           10 IPS020-DATEFIN            PIC X(06).                      097  006
            05 FILLER                      PIC X(410).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPS020-LONG           PIC S9(4)   COMP  VALUE +102.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPS020-LONG           PIC S9(4) COMP-5  VALUE +102.           
                                                                                
      *}                                                                        
