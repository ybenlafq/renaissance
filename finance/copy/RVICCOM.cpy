      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE ICCOM DRIVE INT. COMPTA. COMMISSIONS   *        
      *----------------------------------------------------------------*        
       01  RVICCOM.                                                             
           05  ICCOM-CTABLEG2    PIC X(15).                                     
           05  ICCOM-CTABLEG2-REDEF REDEFINES ICCOM-CTABLEG2.                   
               10  ICCOM-CODE            PIC X(08).                             
               10  ICCOM-WANNEE          PIC X(04).                             
               10  ICCOM-WMOIS           PIC X(02).                             
           05  ICCOM-WTABLEG     PIC X(80).                                     
           05  ICCOM-WTABLEG-REDEF  REDEFINES ICCOM-WTABLEG.                    
               10  ICCOM-CHAINE          PIC X(01).                             
               10  ICCOM-COMPTA          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVICCOM-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ICCOM-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  ICCOM-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ICCOM-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  ICCOM-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
