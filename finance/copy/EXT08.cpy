      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EXT08   EXT08                                              00000020
      ***************************************************************** 00000030
       01   EXT08I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEAFFL      COMP PIC S9(4).                            00000160
      *--                                                                       
           02 MPAGEAFFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEAFFF      PIC X.                                     00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEAFFI      PIC X(2).                                  00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPAGESI      PIC X(2).                                  00000230
           02 M86I OCCURS   15 TIMES .                                  00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAGR-1L     COMP PIC S9(4).                            00000250
      *--                                                                       
             03 MCAGR-1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCAGR-1F     PIC X.                                     00000260
             03 FILLER  PIC X(4).                                       00000270
             03 MCAGR-1I     PIC X(5).                                  00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLAGR-1L     COMP PIC S9(4).                            00000290
      *--                                                                       
             03 MLAGR-1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLAGR-1F     PIC X.                                     00000300
             03 FILLER  PIC X(4).                                       00000310
             03 MLAGR-1I     PIC X(20).                                 00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAGR-2L     COMP PIC S9(4).                            00000330
      *--                                                                       
             03 MCAGR-2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCAGR-2F     PIC X.                                     00000340
             03 FILLER  PIC X(4).                                       00000350
             03 MCAGR-2I     PIC X(5).                                  00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLAGR-2L     COMP PIC S9(4).                            00000370
      *--                                                                       
             03 MLAGR-2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLAGR-2F     PIC X.                                     00000380
             03 FILLER  PIC X(4).                                       00000390
             03 MLAGR-2I     PIC X(20).                                 00000400
      * MESSAGE ERREUR                                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLIBERRI  PIC X(78).                                      00000450
      * CODE TRANSACTION                                                00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MCODTRAI  PIC X(4).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MZONCMDI  PIC X(15).                                      00000540
      * CICS DE TRAVAIL                                                 00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MCICSI    PIC X(5).                                       00000590
      * NETNAME                                                         00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000610
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MNETNAMI  PIC X(8).                                       00000640
      * CODE TERMINAL                                                   00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MSCREENI  PIC X(5).                                       00000690
      ***************************************************************** 00000700
      * SDF: EXT08   EXT08                                              00000710
      ***************************************************************** 00000720
       01   EXT08O REDEFINES EXT08I.                                    00000730
           02 FILLER    PIC X(12).                                      00000740
      * DATE DU JOUR                                                    00000750
           02 FILLER    PIC X(2).                                       00000760
           02 MDATJOUA  PIC X.                                          00000770
           02 MDATJOUC  PIC X.                                          00000780
           02 MDATJOUP  PIC X.                                          00000790
           02 MDATJOUH  PIC X.                                          00000800
           02 MDATJOUV  PIC X.                                          00000810
           02 MDATJOUO  PIC X(10).                                      00000820
      * HEURE                                                           00000830
           02 FILLER    PIC X(2).                                       00000840
           02 MTIMJOUA  PIC X.                                          00000850
           02 MTIMJOUC  PIC X.                                          00000860
           02 MTIMJOUP  PIC X.                                          00000870
           02 MTIMJOUH  PIC X.                                          00000880
           02 MTIMJOUV  PIC X.                                          00000890
           02 MTIMJOUO  PIC X(5).                                       00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MPAGEAFFA      PIC X.                                     00000920
           02 MPAGEAFFC PIC X.                                          00000930
           02 MPAGEAFFP PIC X.                                          00000940
           02 MPAGEAFFH PIC X.                                          00000950
           02 MPAGEAFFV PIC X.                                          00000960
           02 MPAGEAFFO      PIC 99.                                    00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MNBPAGESA      PIC X.                                     00000990
           02 MNBPAGESC PIC X.                                          00001000
           02 MNBPAGESP PIC X.                                          00001010
           02 MNBPAGESH PIC X.                                          00001020
           02 MNBPAGESV PIC X.                                          00001030
           02 MNBPAGESO      PIC 99.                                    00001040
           02 M86O OCCURS   15 TIMES .                                  00001050
             03 FILLER       PIC X(2).                                  00001060
             03 MCAGR-1A     PIC X.                                     00001070
             03 MCAGR-1C     PIC X.                                     00001080
             03 MCAGR-1P     PIC X.                                     00001090
             03 MCAGR-1H     PIC X.                                     00001100
             03 MCAGR-1V     PIC X.                                     00001110
             03 MCAGR-1O     PIC X(5).                                  00001120
             03 FILLER       PIC X(2).                                  00001130
             03 MLAGR-1A     PIC X.                                     00001140
             03 MLAGR-1C     PIC X.                                     00001150
             03 MLAGR-1P     PIC X.                                     00001160
             03 MLAGR-1H     PIC X.                                     00001170
             03 MLAGR-1V     PIC X.                                     00001180
             03 MLAGR-1O     PIC X(20).                                 00001190
             03 FILLER       PIC X(2).                                  00001200
             03 MCAGR-2A     PIC X.                                     00001210
             03 MCAGR-2C     PIC X.                                     00001220
             03 MCAGR-2P     PIC X.                                     00001230
             03 MCAGR-2H     PIC X.                                     00001240
             03 MCAGR-2V     PIC X.                                     00001250
             03 MCAGR-2O     PIC X(5).                                  00001260
             03 FILLER       PIC X(2).                                  00001270
             03 MLAGR-2A     PIC X.                                     00001280
             03 MLAGR-2C     PIC X.                                     00001290
             03 MLAGR-2P     PIC X.                                     00001300
             03 MLAGR-2H     PIC X.                                     00001310
             03 MLAGR-2V     PIC X.                                     00001320
             03 MLAGR-2O     PIC X(20).                                 00001330
      * MESSAGE ERREUR                                                  00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MLIBERRA  PIC X.                                          00001360
           02 MLIBERRC  PIC X.                                          00001370
           02 MLIBERRP  PIC X.                                          00001380
           02 MLIBERRH  PIC X.                                          00001390
           02 MLIBERRV  PIC X.                                          00001400
           02 MLIBERRO  PIC X(78).                                      00001410
      * CODE TRANSACTION                                                00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MCODTRAA  PIC X.                                          00001440
           02 MCODTRAC  PIC X.                                          00001450
           02 MCODTRAP  PIC X.                                          00001460
           02 MCODTRAH  PIC X.                                          00001470
           02 MCODTRAV  PIC X.                                          00001480
           02 MCODTRAO  PIC X(4).                                       00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MZONCMDA  PIC X.                                          00001510
           02 MZONCMDC  PIC X.                                          00001520
           02 MZONCMDP  PIC X.                                          00001530
           02 MZONCMDH  PIC X.                                          00001540
           02 MZONCMDV  PIC X.                                          00001550
           02 MZONCMDO  PIC X(15).                                      00001560
      * CICS DE TRAVAIL                                                 00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MCICSA    PIC X.                                          00001590
           02 MCICSC    PIC X.                                          00001600
           02 MCICSP    PIC X.                                          00001610
           02 MCICSH    PIC X.                                          00001620
           02 MCICSV    PIC X.                                          00001630
           02 MCICSO    PIC X(5).                                       00001640
      * NETNAME                                                         00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MNETNAMA  PIC X.                                          00001670
           02 MNETNAMC  PIC X.                                          00001680
           02 MNETNAMP  PIC X.                                          00001690
           02 MNETNAMH  PIC X.                                          00001700
           02 MNETNAMV  PIC X.                                          00001710
           02 MNETNAMO  PIC X(8).                                       00001720
      * CODE TERMINAL                                                   00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MSCREENA  PIC X.                                          00001750
           02 MSCREENC  PIC X.                                          00001760
           02 MSCREENP  PIC X.                                          00001770
           02 MSCREENH  PIC X.                                          00001780
           02 MSCREENV  PIC X.                                          00001790
           02 MSCREENO  PIC X(5).                                       00001800
                                                                                
