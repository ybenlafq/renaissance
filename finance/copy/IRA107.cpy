      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRA107 AU 26/06/1995  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,12,BI,A,                          *        
      *                           22,05,BI,A,                          *        
      *                           27,07,BI,A,                          *        
      *                           34,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRA107.                                                        
            05 NOMETAT-IRA107           PIC X(6) VALUE 'IRA107'.                
            05 RUPTURES-IRA107.                                                 
           10 IRA107-NSOCIETE           PIC X(03).                      007  003
           10 IRA107-TYPREGL            PIC X(12).                      010  012
           10 IRA107-CTYPDEMANDE        PIC X(05).                      022  005
           10 IRA107-NREGL              PIC X(07).                      027  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRA107-SEQUENCE           PIC S9(04) COMP.                034  002
      *--                                                                       
           10 IRA107-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRA107.                                                   
           10 IRA107-BANQUE             PIC X(20).                      036  020
           10 IRA107-NDEMANDE           PIC X(05).                      056  005
           10 IRA107-MTREMBTOT          PIC 9(09)V9(2).                 061  011
           10 IRA107-QTE                PIC 9(01)     .                 072  001
           10 IRA107-DATEPARAM          PIC X(08).                      073  008
           10 IRA107-DCOMPTA            PIC X(08).                      081  008
           10 IRA107-DEDITLET           PIC X(08).                      089  008
           10 IRA107-DREGL              PIC X(08).                      097  008
            05 FILLER                      PIC X(408).                          
                                                                                
