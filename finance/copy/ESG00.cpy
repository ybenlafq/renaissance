      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESG00   ESG00                                              00000020
      ***************************************************************** 00000030
       01   ESG00I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPSOCL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MTYPSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPSOCF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MTYPSOCI  PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLSOCI    PIC X(24).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETABL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNETABL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNETABF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNETABI   PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLETABL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLETABL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLETABF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLETABI   PIC X(24).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MZONCMDI  PIC X.                                          00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRUBRL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MRUBRL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MRUBRF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MRUBRI    PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPEL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCTYPEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCTYPEF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCTYPEI   PIC X(2).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGRPL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCGRPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCGRPF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCGRPI    PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSECTIONL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MSECTIONL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSECTIONF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MSECTIONI      PIC X(6).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTRI2L    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MTRI2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTRI2F    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MTRI2I    PIC X.                                          00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCOMPTEL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MNCOMPTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCOMPTEF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNCOMPTEI      PIC X(8).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 METBADML  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 METBADML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 METBADMF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 METBADMI  PIC X(3).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTRI3L    COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MTRI3L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTRI3F    PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MTRI3I    PIC X.                                          00000690
           02 MLIGNE1D OCCURS   5 TIMES .                               00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNE1L     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MLIGNE1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLIGNE1F     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MLIGNE1I     PIC X(8).                                  00000740
           02 MLIGNE2D OCCURS   5 TIMES .                               00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNE2L     COMP PIC S9(4).                            00000760
      *--                                                                       
             03 MLIGNE2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLIGNE2F     PIC X.                                     00000770
             03 FILLER  PIC X(4).                                       00000780
             03 MLIGNE2I     PIC X(8).                                  00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCONTRATL      COMP PIC S9(4).                            00000800
      *--                                                                       
           02 MCONTRATL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCONTRATF      PIC X.                                     00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MCONTRATI      PIC X(2).                                  00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCIMPL    COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MCIMPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCIMPF    PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MCIMPI    PIC X(4).                                       00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRUB1L    COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MRUB1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MRUB1F    PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MRUB1I    PIC X(3).                                       00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRUB2L    COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MRUB2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MRUB2F    PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MRUB2I    PIC X(3).                                       00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MLIBERRI  PIC X(78).                                      00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MCODTRAI  PIC X(4).                                       00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001040
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MCICSI    PIC X(5).                                       00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001080
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001090
           02 FILLER    PIC X(4).                                       00001100
           02 MNETNAMI  PIC X(8).                                       00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001120
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001130
           02 FILLER    PIC X(4).                                       00001140
           02 MSCREENI  PIC X(4).                                       00001150
      ***************************************************************** 00001160
      * SDF: ESG00   ESG00                                              00001170
      ***************************************************************** 00001180
       01   ESG00O REDEFINES ESG00I.                                    00001190
           02 FILLER    PIC X(12).                                      00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MDATJOUA  PIC X.                                          00001220
           02 MDATJOUC  PIC X.                                          00001230
           02 MDATJOUP  PIC X.                                          00001240
           02 MDATJOUH  PIC X.                                          00001250
           02 MDATJOUV  PIC X.                                          00001260
           02 MDATJOUO  PIC X(10).                                      00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MTIMJOUA  PIC X.                                          00001290
           02 MTIMJOUC  PIC X.                                          00001300
           02 MTIMJOUP  PIC X.                                          00001310
           02 MTIMJOUH  PIC X.                                          00001320
           02 MTIMJOUV  PIC X.                                          00001330
           02 MTIMJOUO  PIC X(5).                                       00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MTYPSOCA  PIC X.                                          00001360
           02 MTYPSOCC  PIC X.                                          00001370
           02 MTYPSOCP  PIC X.                                          00001380
           02 MTYPSOCH  PIC X.                                          00001390
           02 MTYPSOCV  PIC X.                                          00001400
           02 MTYPSOCO  PIC X(3).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MNSOCA    PIC X.                                          00001430
           02 MNSOCC    PIC X.                                          00001440
           02 MNSOCP    PIC X.                                          00001450
           02 MNSOCH    PIC X.                                          00001460
           02 MNSOCV    PIC X.                                          00001470
           02 MNSOCO    PIC X(3).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MLSOCA    PIC X.                                          00001500
           02 MLSOCC    PIC X.                                          00001510
           02 MLSOCP    PIC X.                                          00001520
           02 MLSOCH    PIC X.                                          00001530
           02 MLSOCV    PIC X.                                          00001540
           02 MLSOCO    PIC X(24).                                      00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MNETABA   PIC X.                                          00001570
           02 MNETABC   PIC X.                                          00001580
           02 MNETABP   PIC X.                                          00001590
           02 MNETABH   PIC X.                                          00001600
           02 MNETABV   PIC X.                                          00001610
           02 MNETABO   PIC X(3).                                       00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MLETABA   PIC X.                                          00001640
           02 MLETABC   PIC X.                                          00001650
           02 MLETABP   PIC X.                                          00001660
           02 MLETABH   PIC X.                                          00001670
           02 MLETABV   PIC X.                                          00001680
           02 MLETABO   PIC X(24).                                      00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MZONCMDA  PIC X.                                          00001710
           02 MZONCMDC  PIC X.                                          00001720
           02 MZONCMDP  PIC X.                                          00001730
           02 MZONCMDH  PIC X.                                          00001740
           02 MZONCMDV  PIC X.                                          00001750
           02 MZONCMDO  PIC X.                                          00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MRUBRA    PIC X.                                          00001780
           02 MRUBRC    PIC X.                                          00001790
           02 MRUBRP    PIC X.                                          00001800
           02 MRUBRH    PIC X.                                          00001810
           02 MRUBRV    PIC X.                                          00001820
           02 MRUBRO    PIC X(3).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MCTYPEA   PIC X.                                          00001850
           02 MCTYPEC   PIC X.                                          00001860
           02 MCTYPEP   PIC X.                                          00001870
           02 MCTYPEH   PIC X.                                          00001880
           02 MCTYPEV   PIC X.                                          00001890
           02 MCTYPEO   PIC X(2).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MCGRPA    PIC X.                                          00001920
           02 MCGRPC    PIC X.                                          00001930
           02 MCGRPP    PIC X.                                          00001940
           02 MCGRPH    PIC X.                                          00001950
           02 MCGRPV    PIC X.                                          00001960
           02 MCGRPO    PIC X(3).                                       00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MSECTIONA      PIC X.                                     00001990
           02 MSECTIONC PIC X.                                          00002000
           02 MSECTIONP PIC X.                                          00002010
           02 MSECTIONH PIC X.                                          00002020
           02 MSECTIONV PIC X.                                          00002030
           02 MSECTIONO      PIC X(6).                                  00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MTRI2A    PIC X.                                          00002060
           02 MTRI2C    PIC X.                                          00002070
           02 MTRI2P    PIC X.                                          00002080
           02 MTRI2H    PIC X.                                          00002090
           02 MTRI2V    PIC X.                                          00002100
           02 MTRI2O    PIC X.                                          00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MNCOMPTEA      PIC X.                                     00002130
           02 MNCOMPTEC PIC X.                                          00002140
           02 MNCOMPTEP PIC X.                                          00002150
           02 MNCOMPTEH PIC X.                                          00002160
           02 MNCOMPTEV PIC X.                                          00002170
           02 MNCOMPTEO      PIC X(8).                                  00002180
           02 FILLER    PIC X(2).                                       00002190
           02 METBADMA  PIC X.                                          00002200
           02 METBADMC  PIC X.                                          00002210
           02 METBADMP  PIC X.                                          00002220
           02 METBADMH  PIC X.                                          00002230
           02 METBADMV  PIC X.                                          00002240
           02 METBADMO  PIC X(3).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MTRI3A    PIC X.                                          00002270
           02 MTRI3C    PIC X.                                          00002280
           02 MTRI3P    PIC X.                                          00002290
           02 MTRI3H    PIC X.                                          00002300
           02 MTRI3V    PIC X.                                          00002310
           02 MTRI3O    PIC X.                                          00002320
           02 DFHMS1 OCCURS   5 TIMES .                                 00002330
             03 FILLER       PIC X(2).                                  00002340
             03 MLIGNE1A     PIC X.                                     00002350
             03 MLIGNE1C     PIC X.                                     00002360
             03 MLIGNE1P     PIC X.                                     00002370
             03 MLIGNE1H     PIC X.                                     00002380
             03 MLIGNE1V     PIC X.                                     00002390
             03 MLIGNE1O     PIC X(8).                                  00002400
           02 DFHMS2 OCCURS   5 TIMES .                                 00002410
             03 FILLER       PIC X(2).                                  00002420
             03 MLIGNE2A     PIC X.                                     00002430
             03 MLIGNE2C     PIC X.                                     00002440
             03 MLIGNE2P     PIC X.                                     00002450
             03 MLIGNE2H     PIC X.                                     00002460
             03 MLIGNE2V     PIC X.                                     00002470
             03 MLIGNE2O     PIC X(8).                                  00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MCONTRATA      PIC X.                                     00002500
           02 MCONTRATC PIC X.                                          00002510
           02 MCONTRATP PIC X.                                          00002520
           02 MCONTRATH PIC X.                                          00002530
           02 MCONTRATV PIC X.                                          00002540
           02 MCONTRATO      PIC X(2).                                  00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MCIMPA    PIC X.                                          00002570
           02 MCIMPC    PIC X.                                          00002580
           02 MCIMPP    PIC X.                                          00002590
           02 MCIMPH    PIC X.                                          00002600
           02 MCIMPV    PIC X.                                          00002610
           02 MCIMPO    PIC X(4).                                       00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MRUB1A    PIC X.                                          00002640
           02 MRUB1C    PIC X.                                          00002650
           02 MRUB1P    PIC X.                                          00002660
           02 MRUB1H    PIC X.                                          00002670
           02 MRUB1V    PIC X.                                          00002680
           02 MRUB1O    PIC X(3).                                       00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MRUB2A    PIC X.                                          00002710
           02 MRUB2C    PIC X.                                          00002720
           02 MRUB2P    PIC X.                                          00002730
           02 MRUB2H    PIC X.                                          00002740
           02 MRUB2V    PIC X.                                          00002750
           02 MRUB2O    PIC X(3).                                       00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MLIBERRA  PIC X.                                          00002780
           02 MLIBERRC  PIC X.                                          00002790
           02 MLIBERRP  PIC X.                                          00002800
           02 MLIBERRH  PIC X.                                          00002810
           02 MLIBERRV  PIC X.                                          00002820
           02 MLIBERRO  PIC X(78).                                      00002830
           02 FILLER    PIC X(2).                                       00002840
           02 MCODTRAA  PIC X.                                          00002850
           02 MCODTRAC  PIC X.                                          00002860
           02 MCODTRAP  PIC X.                                          00002870
           02 MCODTRAH  PIC X.                                          00002880
           02 MCODTRAV  PIC X.                                          00002890
           02 MCODTRAO  PIC X(4).                                       00002900
           02 FILLER    PIC X(2).                                       00002910
           02 MCICSA    PIC X.                                          00002920
           02 MCICSC    PIC X.                                          00002930
           02 MCICSP    PIC X.                                          00002940
           02 MCICSH    PIC X.                                          00002950
           02 MCICSV    PIC X.                                          00002960
           02 MCICSO    PIC X(5).                                       00002970
           02 FILLER    PIC X(2).                                       00002980
           02 MNETNAMA  PIC X.                                          00002990
           02 MNETNAMC  PIC X.                                          00003000
           02 MNETNAMP  PIC X.                                          00003010
           02 MNETNAMH  PIC X.                                          00003020
           02 MNETNAMV  PIC X.                                          00003030
           02 MNETNAMO  PIC X(8).                                       00003040
           02 FILLER    PIC X(2).                                       00003050
           02 MSCREENA  PIC X.                                          00003060
           02 MSCREENC  PIC X.                                          00003070
           02 MSCREENP  PIC X.                                          00003080
           02 MSCREENH  PIC X.                                          00003090
           02 MSCREENV  PIC X.                                          00003100
           02 MSCREENO  PIC X(4).                                       00003110
                                                                                
