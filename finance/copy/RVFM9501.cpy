      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RVFM9501                    *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFM9501.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFM9501.                                                            
      *}                                                                        
      *                       CINTERFACE                                        
           10 FM95-CINTERFACE      PIC X(5).                                    
      *                       CTYPOPER                                          
           10 FM95-CTYPOPER        PIC X(5).                                    
      *                       CNATOPER                                          
           10 FM95-CNATOPER        PIC X(5).                                    
      *                       CINTERFACE_CORR                                   
           10 FM95-CINTERFACE-CORR                                              
              PIC X(5).                                                         
      *                       CTYPOPER_CORR                                     
           10 FM95-CTYPOPER-CORR   PIC X(5).                                    
      *                       CNATOPER_CORR                                     
           10 FM95-CNATOPER-CORR   PIC X(5).                                    
      *                       CDTIERS                                           
           10 FM95-CDTIERS         PIC X(4).                                    
      *                       NSOCCPTA                                          
           10 FM95-NSOCCPTA        PIC X(3).                                    
      *                       NLIEUCPTA                                         
           10 FM95-NLIEUCPTA       PIC X(3).                                    
      *                       NPIECE                                            
           10 FM95-NPIECE          PIC X(2).                                    
      *                       DMAJ                                              
           10 FM95-DMAJ            PIC X(8).                                    
      *                       DSYST                                             
           10 FM95-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       WINTER                                            
           10 FM95-WINTER          PIC X(1).                                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 13      *        
      ******************************************************************        
                                                                                
