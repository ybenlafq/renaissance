      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * CRE/MAJ D'UN MODELE DE PROVISION                                00000020
      ***************************************************************** 00000030
       01   EFM80I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPI     PIC X(2).                                       00000210
           02 TABLEAUI OCCURS   12 TIMES .                              00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSL     COMP PIC S9(4).                                 00000230
      *--                                                                       
             03 MSL COMP-5 PIC S9(4).                                           
      *}                                                                        
             03 MSF     PIC X.                                          00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MSI     PIC X.                                          00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCNOMPROGL   COMP PIC S9(4).                            00000270
      *--                                                                       
             03 MCNOMPROGL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCNOMPROGF   PIC X.                                     00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MCNOMPROGI   PIC X(8).                                  00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLNOMPROGL   COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MLNOMPROGL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLNOMPROGF   PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MLNOMPROGI   PIC X(30).                                 00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPEL      COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MCTYPEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCTYPEF      PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MCTYPEI      PIC X.                                     00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNJRNL  COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 MNJRNL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNJRNF  PIC X.                                          00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNJRNI  PIC X(3).                                       00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNATUREL     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MNATUREL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNATUREF     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNATUREI     PIC X(3).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCETATL      COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MCETATL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCETATF      PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MCETATI      PIC X(8).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MLIBERRI  PIC X(79).                                      00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MCODTRAI  PIC X(4).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MCICSI    PIC X(5).                                       00000620
      * NETNAME                                                         00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MNETNAMI  PIC X(8).                                       00000670
      * CODE TERMINAL                                                   00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MSCREENI  PIC X(4).                                       00000720
      ***************************************************************** 00000730
      * CRE/MAJ D'UN MODELE DE PROVISION                                00000740
      ***************************************************************** 00000750
       01   EFM80O REDEFINES EFM80I.                                    00000760
           02 FILLER    PIC X(12).                                      00000770
           02 FILLER    PIC X(2).                                       00000780
           02 MDATJOUA  PIC X.                                          00000790
           02 MDATJOUC  PIC X.                                          00000800
           02 MDATJOUP  PIC X.                                          00000810
           02 MDATJOUH  PIC X.                                          00000820
           02 MDATJOUV  PIC X.                                          00000830
           02 MDATJOUO  PIC X(10).                                      00000840
           02 FILLER    PIC X(2).                                       00000850
           02 MTIMJOUA  PIC X.                                          00000860
           02 MTIMJOUC  PIC X.                                          00000870
           02 MTIMJOUP  PIC X.                                          00000880
           02 MTIMJOUH  PIC X.                                          00000890
           02 MTIMJOUV  PIC X.                                          00000900
           02 MTIMJOUO  PIC X(5).                                       00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MPAGEA    PIC X.                                          00000930
           02 MPAGEC    PIC X.                                          00000940
           02 MPAGEP    PIC X.                                          00000950
           02 MPAGEH    PIC X.                                          00000960
           02 MPAGEV    PIC X.                                          00000970
           02 MPAGEO    PIC Z9.                                         00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MNBPA     PIC X.                                          00001000
           02 MNBPC     PIC X.                                          00001010
           02 MNBPP     PIC X.                                          00001020
           02 MNBPH     PIC X.                                          00001030
           02 MNBPV     PIC X.                                          00001040
           02 MNBPO     PIC Z9.                                         00001050
           02 TABLEAUO OCCURS   12 TIMES .                              00001060
             03 FILLER       PIC X(2).                                  00001070
             03 MSA     PIC X.                                          00001080
             03 MSC     PIC X.                                          00001090
             03 MSP     PIC X.                                          00001100
             03 MSH     PIC X.                                          00001110
             03 MSV     PIC X.                                          00001120
             03 MSO     PIC X.                                          00001130
             03 FILLER       PIC X(2).                                  00001140
             03 MCNOMPROGA   PIC X.                                     00001150
             03 MCNOMPROGC   PIC X.                                     00001160
             03 MCNOMPROGP   PIC X.                                     00001170
             03 MCNOMPROGH   PIC X.                                     00001180
             03 MCNOMPROGV   PIC X.                                     00001190
             03 MCNOMPROGO   PIC X(8).                                  00001200
             03 FILLER       PIC X(2).                                  00001210
             03 MLNOMPROGA   PIC X.                                     00001220
             03 MLNOMPROGC   PIC X.                                     00001230
             03 MLNOMPROGP   PIC X.                                     00001240
             03 MLNOMPROGH   PIC X.                                     00001250
             03 MLNOMPROGV   PIC X.                                     00001260
             03 MLNOMPROGO   PIC X(30).                                 00001270
             03 FILLER       PIC X(2).                                  00001280
             03 MCTYPEA      PIC X.                                     00001290
             03 MCTYPEC PIC X.                                          00001300
             03 MCTYPEP PIC X.                                          00001310
             03 MCTYPEH PIC X.                                          00001320
             03 MCTYPEV PIC X.                                          00001330
             03 MCTYPEO      PIC X.                                     00001340
             03 FILLER       PIC X(2).                                  00001350
             03 MNJRNA  PIC X.                                          00001360
             03 MNJRNC  PIC X.                                          00001370
             03 MNJRNP  PIC X.                                          00001380
             03 MNJRNH  PIC X.                                          00001390
             03 MNJRNV  PIC X.                                          00001400
             03 MNJRNO  PIC X(3).                                       00001410
             03 FILLER       PIC X(2).                                  00001420
             03 MNATUREA     PIC X.                                     00001430
             03 MNATUREC     PIC X.                                     00001440
             03 MNATUREP     PIC X.                                     00001450
             03 MNATUREH     PIC X.                                     00001460
             03 MNATUREV     PIC X.                                     00001470
             03 MNATUREO     PIC X(3).                                  00001480
             03 FILLER       PIC X(2).                                  00001490
             03 MCETATA      PIC X.                                     00001500
             03 MCETATC PIC X.                                          00001510
             03 MCETATP PIC X.                                          00001520
             03 MCETATH PIC X.                                          00001530
             03 MCETATV PIC X.                                          00001540
             03 MCETATO      PIC X(8).                                  00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MLIBERRA  PIC X.                                          00001570
           02 MLIBERRC  PIC X.                                          00001580
           02 MLIBERRP  PIC X.                                          00001590
           02 MLIBERRH  PIC X.                                          00001600
           02 MLIBERRV  PIC X.                                          00001610
           02 MLIBERRO  PIC X(79).                                      00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MCODTRAA  PIC X.                                          00001640
           02 MCODTRAC  PIC X.                                          00001650
           02 MCODTRAP  PIC X.                                          00001660
           02 MCODTRAH  PIC X.                                          00001670
           02 MCODTRAV  PIC X.                                          00001680
           02 MCODTRAO  PIC X(4).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MCICSA    PIC X.                                          00001710
           02 MCICSC    PIC X.                                          00001720
           02 MCICSP    PIC X.                                          00001730
           02 MCICSH    PIC X.                                          00001740
           02 MCICSV    PIC X.                                          00001750
           02 MCICSO    PIC X(5).                                       00001760
      * NETNAME                                                         00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MNETNAMA  PIC X.                                          00001790
           02 MNETNAMC  PIC X.                                          00001800
           02 MNETNAMP  PIC X.                                          00001810
           02 MNETNAMH  PIC X.                                          00001820
           02 MNETNAMV  PIC X.                                          00001830
           02 MNETNAMO  PIC X(8).                                       00001840
      * CODE TERMINAL                                                   00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MSCREENA  PIC X.                                          00001870
           02 MSCREENC  PIC X.                                          00001880
           02 MSCREENP  PIC X.                                          00001890
           02 MSCREENH  PIC X.                                          00001900
           02 MSCREENV  PIC X.                                          00001910
           02 MSCREENO  PIC X(4).                                       00001920
                                                                                
