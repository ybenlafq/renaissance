      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
AM     01  TS-FM19-LONG             PIC S9(4) COMP-3 VALUE +94.                 
       01  TS-FM19-RECORD.                                                      
           10 TS-FM19-NDEMANDE      PIC X(2).                                   
           10 TS-FM19-DCREATION     PIC X(10).                                  
           10 TS-FM19-CACID         PIC X(8).                                   
           10 TS-FM19-USER          PIC X(25).                                  
           10 TS-FM19-WMFICHE       PIC X.                                      
           10 TS-FM19-WPAPIER       PIC X.                                      
           10 TS-FM19-NETAB         PIC X(3).                                   
AM         10 TS-FM19-NAUXMIN       PIC X(3).                                   
AM         10 TS-FM19-NAUXMAX       PIC X(3).                                   
AM         10 TS-FM19-WTYPAUX       PIC X.                                      
           10 TS-FM19-NTIERSMIN     PIC X(8).                                   
           10 TS-FM19-NTIERSMAX     PIC X(8).                                   
           10 TS-FM19-NBANQUEMIN    PIC X(5).                                   
           10 TS-FM19-NBANQUEMAX    PIC X(5).                                   
           10 TS-FM19-DECHEANCE     PIC X.                                      
           10 TS-FM19-WTOTAL        PIC X OCCURS 5.                             
           10 TS-FM19-WNTRI         PIC X OCCURS 5.                             
                                                                                
