      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EPS02   EPS02                                              00000020
      ***************************************************************** 00000030
       01   EPS02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNSOCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNLIEUI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLLIEUI   PIC X(25).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCODICI  PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCMARQI   PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLREFI    PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGCPLTL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCGCPLTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCGCPLTF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCGCPLTI  PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLGCPLTL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLGCPLTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLGCPLTF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLGCPLTI  PIC X(33).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNGCPLTL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNGCPLTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNGCPLTF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNGCPLTI  PIC X(8).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVENTEL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDVENTEF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDVENTEI  PIC X(10).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFGCPLTL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MDFGCPLTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDFGCPLTF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MDFGCPLTI      PIC X(10).                                 00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOMCLIL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MNOMCLIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNOMCLIF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNOMCLII  PIC X(15).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMTGCPLTL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MMTGCPLTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MMTGCPLTF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MMTGCPLTI      PIC X(8).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMMUNEL      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MCOMMUNEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCOMMUNEF      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCOMMUNEI      PIC X(32).                                 00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDREMBL   COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MDREMBL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDREMBF   PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MDREMBI   PIC X(10).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRACHATL      COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MDRACHATL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDRACHATF      PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MDRACHATI      PIC X(10).                                 00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMTREMBL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MMTREMBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMTREMBF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MMTREMBI  PIC X(8).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMTRACHAL      COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MMTRACHAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MMTRACHAF      PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MMTRACHAI      PIC X(8).                                  00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MLIBERRI  PIC X(78).                                      00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MCODTRAI  PIC X(4).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MCICSI    PIC X(5).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MNETNAMI  PIC X(8).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MSCREENI  PIC X(4).                                       00001090
      ***************************************************************** 00001100
      * SDF: EPS02   EPS02                                              00001110
      ***************************************************************** 00001120
       01   EPS02O REDEFINES EPS02I.                                    00001130
           02 FILLER    PIC X(12).                                      00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MDATJOUA  PIC X.                                          00001160
           02 MDATJOUC  PIC X.                                          00001170
           02 MDATJOUP  PIC X.                                          00001180
           02 MDATJOUH  PIC X.                                          00001190
           02 MDATJOUV  PIC X.                                          00001200
           02 MDATJOUO  PIC X(10).                                      00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MTIMJOUA  PIC X.                                          00001230
           02 MTIMJOUC  PIC X.                                          00001240
           02 MTIMJOUP  PIC X.                                          00001250
           02 MTIMJOUH  PIC X.                                          00001260
           02 MTIMJOUV  PIC X.                                          00001270
           02 MTIMJOUO  PIC X(5).                                       00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MNSOCA    PIC X.                                          00001300
           02 MNSOCC    PIC X.                                          00001310
           02 MNSOCP    PIC X.                                          00001320
           02 MNSOCH    PIC X.                                          00001330
           02 MNSOCV    PIC X.                                          00001340
           02 MNSOCO    PIC X(3).                                       00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MNLIEUA   PIC X.                                          00001370
           02 MNLIEUC   PIC X.                                          00001380
           02 MNLIEUP   PIC X.                                          00001390
           02 MNLIEUH   PIC X.                                          00001400
           02 MNLIEUV   PIC X.                                          00001410
           02 MNLIEUO   PIC X(3).                                       00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MLLIEUA   PIC X.                                          00001440
           02 MLLIEUC   PIC X.                                          00001450
           02 MLLIEUP   PIC X.                                          00001460
           02 MLLIEUH   PIC X.                                          00001470
           02 MLLIEUV   PIC X.                                          00001480
           02 MLLIEUO   PIC X(25).                                      00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MNCODICA  PIC X.                                          00001510
           02 MNCODICC  PIC X.                                          00001520
           02 MNCODICP  PIC X.                                          00001530
           02 MNCODICH  PIC X.                                          00001540
           02 MNCODICV  PIC X.                                          00001550
           02 MNCODICO  PIC X(7).                                       00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MCFAMA    PIC X.                                          00001580
           02 MCFAMC    PIC X.                                          00001590
           02 MCFAMP    PIC X.                                          00001600
           02 MCFAMH    PIC X.                                          00001610
           02 MCFAMV    PIC X.                                          00001620
           02 MCFAMO    PIC X(5).                                       00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MCMARQA   PIC X.                                          00001650
           02 MCMARQC   PIC X.                                          00001660
           02 MCMARQP   PIC X.                                          00001670
           02 MCMARQH   PIC X.                                          00001680
           02 MCMARQV   PIC X.                                          00001690
           02 MCMARQO   PIC X(5).                                       00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MLREFA    PIC X.                                          00001720
           02 MLREFC    PIC X.                                          00001730
           02 MLREFP    PIC X.                                          00001740
           02 MLREFH    PIC X.                                          00001750
           02 MLREFV    PIC X.                                          00001760
           02 MLREFO    PIC X(20).                                      00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MCGCPLTA  PIC X.                                          00001790
           02 MCGCPLTC  PIC X.                                          00001800
           02 MCGCPLTP  PIC X.                                          00001810
           02 MCGCPLTH  PIC X.                                          00001820
           02 MCGCPLTV  PIC X.                                          00001830
           02 MCGCPLTO  PIC X(5).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MLGCPLTA  PIC X.                                          00001860
           02 MLGCPLTC  PIC X.                                          00001870
           02 MLGCPLTP  PIC X.                                          00001880
           02 MLGCPLTH  PIC X.                                          00001890
           02 MLGCPLTV  PIC X.                                          00001900
           02 MLGCPLTO  PIC X(33).                                      00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MNGCPLTA  PIC X.                                          00001930
           02 MNGCPLTC  PIC X.                                          00001940
           02 MNGCPLTP  PIC X.                                          00001950
           02 MNGCPLTH  PIC X.                                          00001960
           02 MNGCPLTV  PIC X.                                          00001970
           02 MNGCPLTO  PIC X(8).                                       00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MDVENTEA  PIC X.                                          00002000
           02 MDVENTEC  PIC X.                                          00002010
           02 MDVENTEP  PIC X.                                          00002020
           02 MDVENTEH  PIC X.                                          00002030
           02 MDVENTEV  PIC X.                                          00002040
           02 MDVENTEO  PIC X(10).                                      00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MDFGCPLTA      PIC X.                                     00002070
           02 MDFGCPLTC PIC X.                                          00002080
           02 MDFGCPLTP PIC X.                                          00002090
           02 MDFGCPLTH PIC X.                                          00002100
           02 MDFGCPLTV PIC X.                                          00002110
           02 MDFGCPLTO      PIC X(10).                                 00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MNOMCLIA  PIC X.                                          00002140
           02 MNOMCLIC  PIC X.                                          00002150
           02 MNOMCLIP  PIC X.                                          00002160
           02 MNOMCLIH  PIC X.                                          00002170
           02 MNOMCLIV  PIC X.                                          00002180
           02 MNOMCLIO  PIC X(15).                                      00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MMTGCPLTA      PIC X.                                     00002210
           02 MMTGCPLTC PIC X.                                          00002220
           02 MMTGCPLTP PIC X.                                          00002230
           02 MMTGCPLTH PIC X.                                          00002240
           02 MMTGCPLTV PIC X.                                          00002250
           02 MMTGCPLTO      PIC ZZZZ9,99.                              00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCOMMUNEA      PIC X.                                     00002280
           02 MCOMMUNEC PIC X.                                          00002290
           02 MCOMMUNEP PIC X.                                          00002300
           02 MCOMMUNEH PIC X.                                          00002310
           02 MCOMMUNEV PIC X.                                          00002320
           02 MCOMMUNEO      PIC X(32).                                 00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MDREMBA   PIC X.                                          00002350
           02 MDREMBC   PIC X.                                          00002360
           02 MDREMBP   PIC X.                                          00002370
           02 MDREMBH   PIC X.                                          00002380
           02 MDREMBV   PIC X.                                          00002390
           02 MDREMBO   PIC X(10).                                      00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MDRACHATA      PIC X.                                     00002420
           02 MDRACHATC PIC X.                                          00002430
           02 MDRACHATP PIC X.                                          00002440
           02 MDRACHATH PIC X.                                          00002450
           02 MDRACHATV PIC X.                                          00002460
           02 MDRACHATO      PIC X(10).                                 00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MMTREMBA  PIC X.                                          00002490
           02 MMTREMBC  PIC X.                                          00002500
           02 MMTREMBP  PIC X.                                          00002510
           02 MMTREMBH  PIC X.                                          00002520
           02 MMTREMBV  PIC X.                                          00002530
           02 MMTREMBO  PIC ZZZZ9,99.                                   00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MMTRACHAA      PIC X.                                     00002560
           02 MMTRACHAC PIC X.                                          00002570
           02 MMTRACHAP PIC X.                                          00002580
           02 MMTRACHAH PIC X.                                          00002590
           02 MMTRACHAV PIC X.                                          00002600
           02 MMTRACHAO      PIC ZZZZ9,99.                              00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MLIBERRA  PIC X.                                          00002630
           02 MLIBERRC  PIC X.                                          00002640
           02 MLIBERRP  PIC X.                                          00002650
           02 MLIBERRH  PIC X.                                          00002660
           02 MLIBERRV  PIC X.                                          00002670
           02 MLIBERRO  PIC X(78).                                      00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MCODTRAA  PIC X.                                          00002700
           02 MCODTRAC  PIC X.                                          00002710
           02 MCODTRAP  PIC X.                                          00002720
           02 MCODTRAH  PIC X.                                          00002730
           02 MCODTRAV  PIC X.                                          00002740
           02 MCODTRAO  PIC X(4).                                       00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MCICSA    PIC X.                                          00002770
           02 MCICSC    PIC X.                                          00002780
           02 MCICSP    PIC X.                                          00002790
           02 MCICSH    PIC X.                                          00002800
           02 MCICSV    PIC X.                                          00002810
           02 MCICSO    PIC X(5).                                       00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MNETNAMA  PIC X.                                          00002840
           02 MNETNAMC  PIC X.                                          00002850
           02 MNETNAMP  PIC X.                                          00002860
           02 MNETNAMH  PIC X.                                          00002870
           02 MNETNAMV  PIC X.                                          00002880
           02 MNETNAMO  PIC X(8).                                       00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MSCREENA  PIC X.                                          00002910
           02 MSCREENC  PIC X.                                          00002920
           02 MSCREENP  PIC X.                                          00002930
           02 MSCREENH  PIC X.                                          00002940
           02 MSCREENV  PIC X.                                          00002950
           02 MSCREENO  PIC X(4).                                       00002960
                                                                                
