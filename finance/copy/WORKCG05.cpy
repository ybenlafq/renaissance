      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01 WORK-MCG05-APPLI.                                                     
      *                                                                         
          02 WORK-MCG05-ENTREE.                                                 
      *------------------------------------------------------------5            
             05 WORK-MCG05-PGRM         PIC X(5).                               
      *      DONNEES GENERALES ----------------------------- 200                
             05 WORK-MCG05-CFONC        PIC X(03).                              
             05 WORK-MCG05-NSURCDE      PIC X(07).                              
             05 WORK-MCG05-NCDE         PIC X(07).                              
             05 WORK-FLAG-TYPCDE        PIC X(01).                              
                88 CDE-MERE             VALUE '0'.                              
                88 CDE-FILLE            VALUE '1'.                              
             05 FILLER                  PIC X(55).                              
      *                                                                         
          02 WORK-MCG05-SORTIE.                                                 
      *            MESSAGE DE SORTIE ----------------------- 100                
             05 WORK-MCG05-MESSAGE.                                             
                  10 WORK-MCG05-CODRET              PIC X(1).                   
                     88 WORK-MCG05-OK              VALUE '0'.                   
                     88 WORK-MCG05-ERR             VALUE '1'.                   
                     88 WORK-MCG05-ERR-DB2         VALUE '2'.                   
                     88 WORK-MCG05-ERR-MQ          VALUE '3'.                   
                  10 WORK-MCG05-LIBERR             PIC X(58).                   
                  10 FILLER                        PIC X(41).                   
          02  FILLER                               PIC X(100).                  
      *                                                                         
                                                                                
