      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : DEFINITION DES REGLES DETERMINATION PCF                *         
      *        POUR MISE A JOUR VUE RVFI0500     (PGR : TFI02)        *         
      *****************************************************************         
      *                                                               *         
       01  TS-FI02.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-FI02-LONG                 PIC S9(3) COMP-3                     
                                           VALUE +25.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-FI02-DONNEES.                                                  
      *----------------------------------  MODE DE MAJ                          
      *                                         ' ' : PAS DE MAJ                
      *                                         'M' : MODIFICATION              
      *                                         'S' : SUPPRESSION               
              03 TS-FI02-CMVT              PIC X(1).                            
      *----------------------------------  CODE ENTITE COMMANDE                 
              03 TS-FI02-NENTCDE           PIC X(5).                            
      *----------------------------------  CODE FAMILLE                         
              03 TS-FI02-CFAM              PIC X(5).                            
      *----------------------------------  CODE TYPE REGLE                      
              03 TS-FI02-CTYPE             PIC X(4).                            
      *----------------------------------  DATE EFFET REGLE                     
              03 TS-FI02-DEFFET            PIC X(8).                            
                                                                                
