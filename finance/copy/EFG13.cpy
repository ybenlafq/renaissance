      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFG13   EFG13                                              00000020
      ***************************************************************** 00000030
       01   EFG13I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITREL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MTITREL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTITREF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MTITREI   PIC X(6).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MSOCCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSOCCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MSOCCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSERVL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MSERVL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSERVF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSERVI    PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCCL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLSOCCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLSOCCF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLSOCCI   PIC X(22).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MPAGEI    PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MPAGEMAXI      PIC X(3).                                  00000370
           02 MTABFACI OCCURS   16 TIMES .                              00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNUMFACTL    COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MNUMFACTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNUMFACTF    PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNUMFACTI    PIC X(7).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPEL      COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MCTYPEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCTYPEF      PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCTYPEI      PIC X(2).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNATUREL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNATUREL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNATUREF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNATUREI     PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSORIGL      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MSORIGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MSORIGF      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MSORIGI      PIC X(3).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLORIGL      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MLORIGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLORIGF      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MLORIGI      PIC X(3).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSECORIGL    COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MSECORIGL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSECORIGF    PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MSECORIGI    PIC X(6).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSDESTL      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MSDESTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MSDESTF      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MSDESTI      PIC X(3).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLDESTL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MLDESTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLDESTF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MLDESTI      PIC X(3).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSECDESTL    COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MSECDESTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSECDESTF    PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MSECDESTI    PIC X(6).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMONTTTCL    COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MMONTTTCL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MMONTTTCF    PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MMONTTTCI    PIC X(15).                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDPIECEL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MDPIECEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDPIECEF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MDPIECEI     PIC X(8).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDECHEANL    COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MDECHEANL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDECHEANF    PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MDECHEANI    PIC X(5).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCONSULTL    COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MCONSULTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCONSULTF    PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MCONSULTI    PIC X.                                     00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MZONCMDI  PIC X(14).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLIBERRI  PIC X(58).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCODTRAI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCICSI    PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MNETNAMI  PIC X(8).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MSCREENI  PIC X(4).                                       00001140
      ***************************************************************** 00001150
      * SDF: EFG13   EFG13                                              00001160
      ***************************************************************** 00001170
       01   EFG13O REDEFINES EFG13I.                                    00001180
           02 FILLER    PIC X(12).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTIMJOUA  PIC X.                                          00001280
           02 MTIMJOUC  PIC X.                                          00001290
           02 MTIMJOUP  PIC X.                                          00001300
           02 MTIMJOUH  PIC X.                                          00001310
           02 MTIMJOUV  PIC X.                                          00001320
           02 MTIMJOUO  PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MTITREA   PIC X.                                          00001350
           02 MTITREC   PIC X.                                          00001360
           02 MTITREP   PIC X.                                          00001370
           02 MTITREH   PIC X.                                          00001380
           02 MTITREV   PIC X.                                          00001390
           02 MTITREO   PIC X(6).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MSOCCA    PIC X.                                          00001420
           02 MSOCCC    PIC X.                                          00001430
           02 MSOCCP    PIC X.                                          00001440
           02 MSOCCH    PIC X.                                          00001450
           02 MSOCCV    PIC X.                                          00001460
           02 MSOCCO    PIC X(3).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MSERVA    PIC X.                                          00001490
           02 MSERVC    PIC X.                                          00001500
           02 MSERVP    PIC X.                                          00001510
           02 MSERVH    PIC X.                                          00001520
           02 MSERVV    PIC X.                                          00001530
           02 MSERVO    PIC X(5).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MLSOCCA   PIC X.                                          00001560
           02 MLSOCCC   PIC X.                                          00001570
           02 MLSOCCP   PIC X.                                          00001580
           02 MLSOCCH   PIC X.                                          00001590
           02 MLSOCCV   PIC X.                                          00001600
           02 MLSOCCO   PIC X(22).                                      00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MPAGEA    PIC X.                                          00001630
           02 MPAGEC    PIC X.                                          00001640
           02 MPAGEP    PIC X.                                          00001650
           02 MPAGEH    PIC X.                                          00001660
           02 MPAGEV    PIC X.                                          00001670
           02 MPAGEO    PIC 9(3).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MPAGEMAXA      PIC X.                                     00001700
           02 MPAGEMAXC PIC X.                                          00001710
           02 MPAGEMAXP PIC X.                                          00001720
           02 MPAGEMAXH PIC X.                                          00001730
           02 MPAGEMAXV PIC X.                                          00001740
           02 MPAGEMAXO      PIC 9(3).                                  00001750
           02 MTABFACO OCCURS   16 TIMES .                              00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MNUMFACTA    PIC X.                                     00001780
             03 MNUMFACTC    PIC X.                                     00001790
             03 MNUMFACTP    PIC X.                                     00001800
             03 MNUMFACTH    PIC X.                                     00001810
             03 MNUMFACTV    PIC X.                                     00001820
             03 MNUMFACTO    PIC X(7).                                  00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MCTYPEA      PIC X.                                     00001850
             03 MCTYPEC PIC X.                                          00001860
             03 MCTYPEP PIC X.                                          00001870
             03 MCTYPEH PIC X.                                          00001880
             03 MCTYPEV PIC X.                                          00001890
             03 MCTYPEO      PIC X(2).                                  00001900
             03 FILLER       PIC X(2).                                  00001910
             03 MNATUREA     PIC X.                                     00001920
             03 MNATUREC     PIC X.                                     00001930
             03 MNATUREP     PIC X.                                     00001940
             03 MNATUREH     PIC X.                                     00001950
             03 MNATUREV     PIC X.                                     00001960
             03 MNATUREO     PIC X(5).                                  00001970
             03 FILLER       PIC X(2).                                  00001980
             03 MSORIGA      PIC X.                                     00001990
             03 MSORIGC PIC X.                                          00002000
             03 MSORIGP PIC X.                                          00002010
             03 MSORIGH PIC X.                                          00002020
             03 MSORIGV PIC X.                                          00002030
             03 MSORIGO      PIC X(3).                                  00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MLORIGA      PIC X.                                     00002060
             03 MLORIGC PIC X.                                          00002070
             03 MLORIGP PIC X.                                          00002080
             03 MLORIGH PIC X.                                          00002090
             03 MLORIGV PIC X.                                          00002100
             03 MLORIGO      PIC X(3).                                  00002110
             03 FILLER       PIC X(2).                                  00002120
             03 MSECORIGA    PIC X.                                     00002130
             03 MSECORIGC    PIC X.                                     00002140
             03 MSECORIGP    PIC X.                                     00002150
             03 MSECORIGH    PIC X.                                     00002160
             03 MSECORIGV    PIC X.                                     00002170
             03 MSECORIGO    PIC X(6).                                  00002180
             03 FILLER       PIC X(2).                                  00002190
             03 MSDESTA      PIC X.                                     00002200
             03 MSDESTC PIC X.                                          00002210
             03 MSDESTP PIC X.                                          00002220
             03 MSDESTH PIC X.                                          00002230
             03 MSDESTV PIC X.                                          00002240
             03 MSDESTO      PIC X(3).                                  00002250
             03 FILLER       PIC X(2).                                  00002260
             03 MLDESTA      PIC X.                                     00002270
             03 MLDESTC PIC X.                                          00002280
             03 MLDESTP PIC X.                                          00002290
             03 MLDESTH PIC X.                                          00002300
             03 MLDESTV PIC X.                                          00002310
             03 MLDESTO      PIC X(3).                                  00002320
             03 FILLER       PIC X(2).                                  00002330
             03 MSECDESTA    PIC X.                                     00002340
             03 MSECDESTC    PIC X.                                     00002350
             03 MSECDESTP    PIC X.                                     00002360
             03 MSECDESTH    PIC X.                                     00002370
             03 MSECDESTV    PIC X.                                     00002380
             03 MSECDESTO    PIC X(6).                                  00002390
             03 FILLER       PIC X(2).                                  00002400
             03 MMONTTTCA    PIC X.                                     00002410
             03 MMONTTTCC    PIC X.                                     00002420
             03 MMONTTTCP    PIC X.                                     00002430
             03 MMONTTTCH    PIC X.                                     00002440
             03 MMONTTTCV    PIC X.                                     00002450
             03 MMONTTTCO    PIC Z(11)9,99.                             00002460
             03 FILLER       PIC X(2).                                  00002470
             03 MDPIECEA     PIC X.                                     00002480
             03 MDPIECEC     PIC X.                                     00002490
             03 MDPIECEP     PIC X.                                     00002500
             03 MDPIECEH     PIC X.                                     00002510
             03 MDPIECEV     PIC X.                                     00002520
             03 MDPIECEO     PIC X(8).                                  00002530
             03 FILLER       PIC X(2).                                  00002540
             03 MDECHEANA    PIC X.                                     00002550
             03 MDECHEANC    PIC X.                                     00002560
             03 MDECHEANP    PIC X.                                     00002570
             03 MDECHEANH    PIC X.                                     00002580
             03 MDECHEANV    PIC X.                                     00002590
             03 MDECHEANO    PIC X(5).                                  00002600
             03 FILLER       PIC X(2).                                  00002610
             03 MCONSULTA    PIC X.                                     00002620
             03 MCONSULTC    PIC X.                                     00002630
             03 MCONSULTP    PIC X.                                     00002640
             03 MCONSULTH    PIC X.                                     00002650
             03 MCONSULTV    PIC X.                                     00002660
             03 MCONSULTO    PIC X.                                     00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MZONCMDA  PIC X.                                          00002690
           02 MZONCMDC  PIC X.                                          00002700
           02 MZONCMDP  PIC X.                                          00002710
           02 MZONCMDH  PIC X.                                          00002720
           02 MZONCMDV  PIC X.                                          00002730
           02 MZONCMDO  PIC X(14).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MLIBERRA  PIC X.                                          00002760
           02 MLIBERRC  PIC X.                                          00002770
           02 MLIBERRP  PIC X.                                          00002780
           02 MLIBERRH  PIC X.                                          00002790
           02 MLIBERRV  PIC X.                                          00002800
           02 MLIBERRO  PIC X(58).                                      00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MCODTRAA  PIC X.                                          00002830
           02 MCODTRAC  PIC X.                                          00002840
           02 MCODTRAP  PIC X.                                          00002850
           02 MCODTRAH  PIC X.                                          00002860
           02 MCODTRAV  PIC X.                                          00002870
           02 MCODTRAO  PIC X(4).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MCICSA    PIC X.                                          00002900
           02 MCICSC    PIC X.                                          00002910
           02 MCICSP    PIC X.                                          00002920
           02 MCICSH    PIC X.                                          00002930
           02 MCICSV    PIC X.                                          00002940
           02 MCICSO    PIC X(5).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MNETNAMA  PIC X.                                          00002970
           02 MNETNAMC  PIC X.                                          00002980
           02 MNETNAMP  PIC X.                                          00002990
           02 MNETNAMH  PIC X.                                          00003000
           02 MNETNAMV  PIC X.                                          00003010
           02 MNETNAMO  PIC X(8).                                       00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MSCREENA  PIC X.                                          00003040
           02 MSCREENC  PIC X.                                          00003050
           02 MSCREENP  PIC X.                                          00003060
           02 MSCREENH  PIC X.                                          00003070
           02 MSCREENV  PIC X.                                          00003080
           02 MSCREENO  PIC X(4).                                       00003090
                                                                                
