      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - JOURNAL GENERAL MODELE                                    00000020
      ***************************************************************** 00000030
       01   EFM16I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTITEL      COMP PIC S9(4).                            00000160
      *--                                                                       
           02 MNENTITEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNENTITEF      PIC X.                                     00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MNENTITEI      PIC X(5).                                  00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTITEL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MLENTITEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLENTITEF      PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MLENTITEI      PIC X(34).                                 00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEMANDEL     COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MNDEMANDEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNDEMANDEF     PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNDEMANDEI     PIC X(2).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCREATIONL    COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MDCREATIONL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDCREATIONF    PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MDCREATIONI    PIC X(10).                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MUSERL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MUSERL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MUSERF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MUSERI    PIC X(25).                                      00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWMFICHEL      COMP PIC S9(4).                            00000360
      *--                                                                       
           02 MWMFICHEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWMFICHEF      PIC X.                                     00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MWMFICHEI      PIC X.                                     00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAPIERL      COMP PIC S9(4).                            00000400
      *--                                                                       
           02 MWPAPIERL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWPAPIERF      PIC X.                                     00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MWPAPIERI      PIC X.                                     00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETABL   COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MNETABL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNETABF   PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MNETABI   PIC X(3).                                       00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNJOURNALMINL  COMP PIC S9(4).                            00000480
      *--                                                                       
           02 MNJOURNALMINL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MNJOURNALMINF  PIC X.                                     00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MNJOURNALMINI  PIC X(3).                                  00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNJOURNALMAXL  COMP PIC S9(4).                            00000520
      *--                                                                       
           02 MNJOURNALMAXL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MNJOURNALMAXF  PIC X.                                     00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MNJOURNALMAXI  PIC X(3).                                  00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWJOURNALGALL  COMP PIC S9(4).                            00000560
      *--                                                                       
           02 MWJOURNALGALL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MWJOURNALGALF  PIC X.                                     00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MWJOURNALGALI  PIC X.                                     00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTTRIECRITL   COMP PIC S9(4).                            00000600
      *--                                                                       
           02 MWTTRIECRITL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MWTTRIECRITF   PIC X.                                     00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MWTTRIECRITI   PIC X.                                     00000630
      * ZONE CMD AIDA                                                   00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MLIBERRI  PIC X(78).                                      00000680
      * CODE TRANSACTION                                                00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCODTRAI  PIC X(4).                                       00000730
      * CICS DE TRAVAIL                                                 00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCICSI    PIC X(5).                                       00000780
      * NETNAME                                                         00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MNETNAMI  PIC X(8).                                       00000830
      * CODE TERMINAL                                                   00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000850
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000860
           02 FILLER    PIC X(4).                                       00000870
           02 MSCREENI  PIC X(5).                                       00000880
      ***************************************************************** 00000890
      * GCT - JOURNAL GENERAL MODELE                                    00000900
      ***************************************************************** 00000910
       01   EFM16O REDEFINES EFM16I.                                    00000920
           02 FILLER    PIC X(12).                                      00000930
      * DATE DU JOUR                                                    00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MDATJOUA  PIC X.                                          00000960
           02 MDATJOUC  PIC X.                                          00000970
           02 MDATJOUP  PIC X.                                          00000980
           02 MDATJOUH  PIC X.                                          00000990
           02 MDATJOUV  PIC X.                                          00001000
           02 MDATJOUO  PIC X(10).                                      00001010
      * HEURE                                                           00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MTIMJOUA  PIC X.                                          00001040
           02 MTIMJOUC  PIC X.                                          00001050
           02 MTIMJOUP  PIC X.                                          00001060
           02 MTIMJOUH  PIC X.                                          00001070
           02 MTIMJOUV  PIC X.                                          00001080
           02 MTIMJOUO  PIC X(5).                                       00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MNENTITEA      PIC X.                                     00001110
           02 MNENTITEC PIC X.                                          00001120
           02 MNENTITEP PIC X.                                          00001130
           02 MNENTITEH PIC X.                                          00001140
           02 MNENTITEV PIC X.                                          00001150
           02 MNENTITEO      PIC X(5).                                  00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MLENTITEA      PIC X.                                     00001180
           02 MLENTITEC PIC X.                                          00001190
           02 MLENTITEP PIC X.                                          00001200
           02 MLENTITEH PIC X.                                          00001210
           02 MLENTITEV PIC X.                                          00001220
           02 MLENTITEO      PIC X(34).                                 00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MNDEMANDEA     PIC X.                                     00001250
           02 MNDEMANDEC     PIC X.                                     00001260
           02 MNDEMANDEP     PIC X.                                     00001270
           02 MNDEMANDEH     PIC X.                                     00001280
           02 MNDEMANDEV     PIC X.                                     00001290
           02 MNDEMANDEO     PIC X(2).                                  00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MDCREATIONA    PIC X.                                     00001320
           02 MDCREATIONC    PIC X.                                     00001330
           02 MDCREATIONP    PIC X.                                     00001340
           02 MDCREATIONH    PIC X.                                     00001350
           02 MDCREATIONV    PIC X.                                     00001360
           02 MDCREATIONO    PIC X(10).                                 00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MUSERA    PIC X.                                          00001390
           02 MUSERC    PIC X.                                          00001400
           02 MUSERP    PIC X.                                          00001410
           02 MUSERH    PIC X.                                          00001420
           02 MUSERV    PIC X.                                          00001430
           02 MUSERO    PIC X(25).                                      00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MWMFICHEA      PIC X.                                     00001460
           02 MWMFICHEC PIC X.                                          00001470
           02 MWMFICHEP PIC X.                                          00001480
           02 MWMFICHEH PIC X.                                          00001490
           02 MWMFICHEV PIC X.                                          00001500
           02 MWMFICHEO      PIC X.                                     00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MWPAPIERA      PIC X.                                     00001530
           02 MWPAPIERC PIC X.                                          00001540
           02 MWPAPIERP PIC X.                                          00001550
           02 MWPAPIERH PIC X.                                          00001560
           02 MWPAPIERV PIC X.                                          00001570
           02 MWPAPIERO      PIC X.                                     00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MNETABA   PIC X.                                          00001600
           02 MNETABC   PIC X.                                          00001610
           02 MNETABP   PIC X.                                          00001620
           02 MNETABH   PIC X.                                          00001630
           02 MNETABV   PIC X.                                          00001640
           02 MNETABO   PIC X(3).                                       00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MNJOURNALMINA  PIC X.                                     00001670
           02 MNJOURNALMINC  PIC X.                                     00001680
           02 MNJOURNALMINP  PIC X.                                     00001690
           02 MNJOURNALMINH  PIC X.                                     00001700
           02 MNJOURNALMINV  PIC X.                                     00001710
           02 MNJOURNALMINO  PIC X(3).                                  00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MNJOURNALMAXA  PIC X.                                     00001740
           02 MNJOURNALMAXC  PIC X.                                     00001750
           02 MNJOURNALMAXP  PIC X.                                     00001760
           02 MNJOURNALMAXH  PIC X.                                     00001770
           02 MNJOURNALMAXV  PIC X.                                     00001780
           02 MNJOURNALMAXO  PIC X(3).                                  00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MWJOURNALGALA  PIC X.                                     00001810
           02 MWJOURNALGALC  PIC X.                                     00001820
           02 MWJOURNALGALP  PIC X.                                     00001830
           02 MWJOURNALGALH  PIC X.                                     00001840
           02 MWJOURNALGALV  PIC X.                                     00001850
           02 MWJOURNALGALO  PIC X.                                     00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MWTTRIECRITA   PIC X.                                     00001880
           02 MWTTRIECRITC   PIC X.                                     00001890
           02 MWTTRIECRITP   PIC X.                                     00001900
           02 MWTTRIECRITH   PIC X.                                     00001910
           02 MWTTRIECRITV   PIC X.                                     00001920
           02 MWTTRIECRITO   PIC X.                                     00001930
      * ZONE CMD AIDA                                                   00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MLIBERRA  PIC X.                                          00001960
           02 MLIBERRC  PIC X.                                          00001970
           02 MLIBERRP  PIC X.                                          00001980
           02 MLIBERRH  PIC X.                                          00001990
           02 MLIBERRV  PIC X.                                          00002000
           02 MLIBERRO  PIC X(78).                                      00002010
      * CODE TRANSACTION                                                00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MCODTRAA  PIC X.                                          00002040
           02 MCODTRAC  PIC X.                                          00002050
           02 MCODTRAP  PIC X.                                          00002060
           02 MCODTRAH  PIC X.                                          00002070
           02 MCODTRAV  PIC X.                                          00002080
           02 MCODTRAO  PIC X(4).                                       00002090
      * CICS DE TRAVAIL                                                 00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MCICSA    PIC X.                                          00002120
           02 MCICSC    PIC X.                                          00002130
           02 MCICSP    PIC X.                                          00002140
           02 MCICSH    PIC X.                                          00002150
           02 MCICSV    PIC X.                                          00002160
           02 MCICSO    PIC X(5).                                       00002170
      * NETNAME                                                         00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MNETNAMA  PIC X.                                          00002200
           02 MNETNAMC  PIC X.                                          00002210
           02 MNETNAMP  PIC X.                                          00002220
           02 MNETNAMH  PIC X.                                          00002230
           02 MNETNAMV  PIC X.                                          00002240
           02 MNETNAMO  PIC X(8).                                       00002250
      * CODE TERMINAL                                                   00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MSCREENA  PIC X.                                          00002280
           02 MSCREENC  PIC X.                                          00002290
           02 MSCREENP  PIC X.                                          00002300
           02 MSCREENH  PIC X.                                          00002310
           02 MSCREENV  PIC X.                                          00002320
           02 MSCREENO  PIC X(5).                                       00002330
                                                                                
