      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFT4300                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFT4300                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT4301.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT4301.                                                            
      *}                                                                        
           02  FT43-NSOC                                                        
               PIC X(0005).                                                     
           02  FT43-NETAB                                                       
               PIC X(0003).                                                     
           02  FT43-NAUX                                                        
               PIC X(0003).                                                     
           02  FT43-NFOUR                                                       
               PIC X(0008).                                                     
           02  FT43-NJRN                                                        
               PIC X(0003).                                                     
           02  FT43-NEXER                                                       
               PIC X(0004).                                                     
           02  FT43-NPIECE                                                      
               PIC X(0010).                                                     
           02  FT43-NLIGNE                                                      
               PIC X(0003).                                                     
           02  FT43-CDEVISE                                                     
               PIC X(0003).                                                     
           02  FT43-NPER                                                        
               PIC X(0002).                                                     
           02  FT43-NATURE                                                      
               PIC X(0003).                                                     
           02  FT43-COMPTE                                                      
               PIC X(0006).                                                     
           02  FT43-SSCOMPTE                                                    
               PIC X(0006).                                                     
           02  FT43-SECTION                                                     
               PIC X(0006).                                                     
           02  FT43-RUBRIQUE                                                    
               PIC X(0006).                                                     
           02  FT43-STEAPP                                                      
               PIC X(0005).                                                     
           02  FT43-LIBELLE                                                     
               PIC X(0025).                                                     
           02  FT43-SLID                                                        
               PIC X(0001).                                                     
           02  FT43-SLAC                                                        
               PIC X(0006).                                                     
           02  FT43-MONTANT                                                     
               PIC S9(13)V9(0002) COMP-3.                                       
           02  FT43-WSENS                                                       
               PIC X(0001).                                                     
           02  FT43-WTVA                                                        
               PIC X(0001).                                                     
           02  FT43-LETTRAGE                                                    
               PIC X(0010).                                                     
           02  FT43-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  FT43-DECHEANCE                                                   
               PIC X(0008).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFT4300                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT4300-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT4300-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-NETAB-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-NETAB-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-NAUX-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-NAUX-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-NFOUR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-NFOUR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-NJRN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-NJRN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-NEXER-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-NEXER-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-NPIECE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-NPIECE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-NLIGNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-CDEVISE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-CDEVISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-NPER-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-NPER-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-NATURE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-NATURE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-COMPTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-COMPTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-SSCOMPTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-SSCOMPTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-SECTION-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-SECTION-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-RUBRIQUE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-RUBRIQUE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-STEAPP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-STEAPP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-LIBELLE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-LIBELLE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-SLID-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-SLID-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-SLAC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-SLAC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-MONTANT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-MONTANT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-WSENS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-WSENS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-WTVA-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-WTVA-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-LETTRAGE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-LETTRAGE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT43-DECHEANCE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT43-DECHEANCE-F                                                 
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
