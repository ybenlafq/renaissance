      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SIGA - SECTIONS DE REGROUPEMENT                                 00000020
      ***************************************************************** 00000030
       01   ESG02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MPAGEMAXI      PIC X(2).                                  00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNSOCI    PIC X(3).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCL    COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MLSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSOCF    PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLSOCI    PIC X(24).                                      00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETABL   COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MNETABL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNETABF   PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MNETABI   PIC X(3).                                       00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLETABL   COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MLETABL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLETABF   PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MLETABI   PIC X(22).                                      00000390
           02 MLIGNEI OCCURS   12 TIMES .                               00000400
             03 MSELECTD OCCURS   2 TIMES .                             00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MSELECTL   COMP PIC S9(4).                            00000420
      *--                                                                       
               04 MSELECTL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MSELECTF   PIC X.                                     00000430
               04 FILLER     PIC X(4).                                  00000440
               04 MSELECTI   PIC X.                                     00000450
             03 MSECPAIED OCCURS   2 TIMES .                            00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MSECPAIEL  COMP PIC S9(4).                            00000470
      *--                                                                       
               04 MSECPAIEL COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 MSECPAIEF  PIC X.                                     00000480
               04 FILLER     PIC X(4).                                  00000490
               04 MSECPAIEI  PIC X(6).                                  00000500
             03 MCODED OCCURS   2 TIMES .                               00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MCODEL     COMP PIC S9(4).                            00000520
      *--                                                                       
               04 MCODEL COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MCODEF     PIC X.                                     00000530
               04 FILLER     PIC X(4).                                  00000540
               04 MCODEI     PIC X(3).                                  00000550
             03 MSECGRPD OCCURS   2 TIMES .                             00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MSECGRPL   COMP PIC S9(4).                            00000570
      *--                                                                       
               04 MSECGRPL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MSECGRPF   PIC X.                                     00000580
               04 FILLER     PIC X(4).                                  00000590
               04 MSECGRPI   PIC X(6).                                  00000600
      * ZONE CMD AIDA                                                   00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLIBERRI  PIC X(79).                                      00000650
      * CODE TRANSACTION                                                00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCODTRAI  PIC X(4).                                       00000700
      * CICS DE TRAVAIL                                                 00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MCICSI    PIC X(5).                                       00000750
      * NETNAME                                                         00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MNETNAMI  PIC X(8).                                       00000800
      * CODE TERMINAL                                                   00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MSCREENI  PIC X(4).                                       00000850
      ***************************************************************** 00000860
      * SIGA - SECTIONS DE REGROUPEMENT                                 00000870
      ***************************************************************** 00000880
       01   ESG02O REDEFINES ESG02I.                                    00000890
           02 FILLER    PIC X(12).                                      00000900
      * DATE DU JOUR                                                    00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MDATJOUA  PIC X.                                          00000930
           02 MDATJOUC  PIC X.                                          00000940
           02 MDATJOUP  PIC X.                                          00000950
           02 MDATJOUH  PIC X.                                          00000960
           02 MDATJOUV  PIC X.                                          00000970
           02 MDATJOUO  PIC X(10).                                      00000980
      * HEURE                                                           00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MTIMJOUA  PIC X.                                          00001010
           02 MTIMJOUC  PIC X.                                          00001020
           02 MTIMJOUP  PIC X.                                          00001030
           02 MTIMJOUH  PIC X.                                          00001040
           02 MTIMJOUV  PIC X.                                          00001050
           02 MTIMJOUO  PIC X(5).                                       00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MPAGEA    PIC X.                                          00001080
           02 MPAGEC    PIC X.                                          00001090
           02 MPAGEP    PIC X.                                          00001100
           02 MPAGEH    PIC X.                                          00001110
           02 MPAGEV    PIC X.                                          00001120
           02 MPAGEO    PIC Z9.                                         00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MPAGEMAXA      PIC X.                                     00001150
           02 MPAGEMAXC PIC X.                                          00001160
           02 MPAGEMAXP PIC X.                                          00001170
           02 MPAGEMAXH PIC X.                                          00001180
           02 MPAGEMAXV PIC X.                                          00001190
           02 MPAGEMAXO      PIC Z9.                                    00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MNSOCA    PIC X.                                          00001220
           02 MNSOCC    PIC X.                                          00001230
           02 MNSOCP    PIC X.                                          00001240
           02 MNSOCH    PIC X.                                          00001250
           02 MNSOCV    PIC X.                                          00001260
           02 MNSOCO    PIC X(3).                                       00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MLSOCA    PIC X.                                          00001290
           02 MLSOCC    PIC X.                                          00001300
           02 MLSOCP    PIC X.                                          00001310
           02 MLSOCH    PIC X.                                          00001320
           02 MLSOCV    PIC X.                                          00001330
           02 MLSOCO    PIC X(24).                                      00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MNETABA   PIC X.                                          00001360
           02 MNETABC   PIC X.                                          00001370
           02 MNETABP   PIC X.                                          00001380
           02 MNETABH   PIC X.                                          00001390
           02 MNETABV   PIC X.                                          00001400
           02 MNETABO   PIC X(3).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MLETABA   PIC X.                                          00001430
           02 MLETABC   PIC X.                                          00001440
           02 MLETABP   PIC X.                                          00001450
           02 MLETABH   PIC X.                                          00001460
           02 MLETABV   PIC X.                                          00001470
           02 MLETABO   PIC X(22).                                      00001480
           02 MLIGNEO OCCURS   12 TIMES .                               00001490
             03 DFHMS1 OCCURS   2 TIMES .                               00001500
               04 FILLER     PIC X(2).                                  00001510
               04 MSELECTA   PIC X.                                     00001520
               04 MSELECTC   PIC X.                                     00001530
               04 MSELECTP   PIC X.                                     00001540
               04 MSELECTH   PIC X.                                     00001550
               04 MSELECTV   PIC X.                                     00001560
               04 MSELECTO   PIC X.                                     00001570
             03 DFHMS2 OCCURS   2 TIMES .                               00001580
               04 FILLER     PIC X(2).                                  00001590
               04 MSECPAIEA  PIC X.                                     00001600
               04 MSECPAIEC  PIC X.                                     00001610
               04 MSECPAIEP  PIC X.                                     00001620
               04 MSECPAIEH  PIC X.                                     00001630
               04 MSECPAIEV  PIC X.                                     00001640
               04 MSECPAIEO  PIC X(6).                                  00001650
             03 DFHMS3 OCCURS   2 TIMES .                               00001660
               04 FILLER     PIC X(2).                                  00001670
               04 MCODEA     PIC X.                                     00001680
               04 MCODEC     PIC X.                                     00001690
               04 MCODEP     PIC X.                                     00001700
               04 MCODEH     PIC X.                                     00001710
               04 MCODEV     PIC X.                                     00001720
               04 MCODEO     PIC X(3).                                  00001730
             03 DFHMS4 OCCURS   2 TIMES .                               00001740
               04 FILLER     PIC X(2).                                  00001750
               04 MSECGRPA   PIC X.                                     00001760
               04 MSECGRPC   PIC X.                                     00001770
               04 MSECGRPP   PIC X.                                     00001780
               04 MSECGRPH   PIC X.                                     00001790
               04 MSECGRPV   PIC X.                                     00001800
               04 MSECGRPO   PIC X(6).                                  00001810
      * ZONE CMD AIDA                                                   00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MLIBERRA  PIC X.                                          00001840
           02 MLIBERRC  PIC X.                                          00001850
           02 MLIBERRP  PIC X.                                          00001860
           02 MLIBERRH  PIC X.                                          00001870
           02 MLIBERRV  PIC X.                                          00001880
           02 MLIBERRO  PIC X(79).                                      00001890
      * CODE TRANSACTION                                                00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MCODTRAA  PIC X.                                          00001920
           02 MCODTRAC  PIC X.                                          00001930
           02 MCODTRAP  PIC X.                                          00001940
           02 MCODTRAH  PIC X.                                          00001950
           02 MCODTRAV  PIC X.                                          00001960
           02 MCODTRAO  PIC X(4).                                       00001970
      * CICS DE TRAVAIL                                                 00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MCICSA    PIC X.                                          00002000
           02 MCICSC    PIC X.                                          00002010
           02 MCICSP    PIC X.                                          00002020
           02 MCICSH    PIC X.                                          00002030
           02 MCICSV    PIC X.                                          00002040
           02 MCICSO    PIC X(5).                                       00002050
      * NETNAME                                                         00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MNETNAMA  PIC X.                                          00002080
           02 MNETNAMC  PIC X.                                          00002090
           02 MNETNAMP  PIC X.                                          00002100
           02 MNETNAMH  PIC X.                                          00002110
           02 MNETNAMV  PIC X.                                          00002120
           02 MNETNAMO  PIC X(8).                                       00002130
      * CODE TERMINAL                                                   00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MSCREENA  PIC X.                                          00002160
           02 MSCREENC  PIC X.                                          00002170
           02 MSCREENP  PIC X.                                          00002180
           02 MSCREENH  PIC X.                                          00002190
           02 MSCREENV  PIC X.                                          00002200
           02 MSCREENO  PIC X(4).                                       00002210
                                                                                
