      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CGMGD CONTITIONS GARANTIE MGD          *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCGMGD .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCGMGD .                                                            
      *}                                                                        
           05  CGMGD-CTABLEG2    PIC X(15).                                     
           05  CGMGD-CTABLEG2-REDEF REDEFINES CGMGD-CTABLEG2.                   
               10  CGMGD-CGARMGD         PIC X(05).                             
           05  CGMGD-WTABLEG     PIC X(80).                                     
           05  CGMGD-WTABLEG-REDEF  REDEFINES CGMGD-WTABLEG.                    
               10  CGMGD-LGARMGD         PIC X(20).                             
               10  CGMGD-CTYPGAR         PIC X(05).                             
               10  CGMGD-NBMOIS          PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  CGMGD-NBMOIS-N       REDEFINES CGMGD-NBMOIS                  
                                         PIC 9(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCGMGD-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCGMGD-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CGMGD-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CGMGD-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CGMGD-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CGMGD-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
