      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT/SAP - sections                                              00000020
      ***************************************************************** 00000030
       01   EFM63I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(2).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINTERFACEL   COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MCINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MCINTERFACEF   PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MCINTERFACEI   PIC X(5).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINTERFACEL   COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MLINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLINTERFACEF   PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLINTERFACEI   PIC X(30).                                 00000310
           02 MCRITERED OCCURS   3 TIMES .                              00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCRITEREL    COMP PIC S9(4).                            00000330
      *--                                                                       
             03 MCRITEREL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCRITEREF    PIC X.                                     00000340
             03 FILLER  PIC X(4).                                       00000350
             03 MCRITEREI    PIC X(8).                                  00000360
           02 MTABRI OCCURS   10 TIMES .                                00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPOPERL   COMP PIC S9(4).                            00000380
      *--                                                                       
             03 MCTYPOPERL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCTYPOPERF   PIC X.                                     00000390
             03 FILLER  PIC X(4).                                       00000400
             03 MCTYPOPERI   PIC X(5).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCNATOPERL   COMP PIC S9(4).                            00000420
      *--                                                                       
             03 MCNATOPERL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCNATOPERF   PIC X.                                     00000430
             03 FILLER  PIC X(4).                                       00000440
             03 MCNATOPERI   PIC X(5).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSEC1L      COMP PIC S9(4).                            00000460
      *--                                                                       
             03 MCSEC1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCSEC1F      PIC X.                                     00000470
             03 FILLER  PIC X(4).                                       00000480
             03 MCSEC1I      PIC X.                                     00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSEC2L      COMP PIC S9(4).                            00000500
      *--                                                                       
             03 MCSEC2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCSEC2F      PIC X.                                     00000510
             03 FILLER  PIC X(4).                                       00000520
             03 MCSEC2I      PIC X.                                     00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSEC3L      COMP PIC S9(4).                            00000540
      *--                                                                       
             03 MCSEC3L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCSEC3F      PIC X.                                     00000550
             03 FILLER  PIC X(4).                                       00000560
             03 MCSEC3I      PIC X.                                     00000570
      * ZONE CMD AIDA                                                   00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MLIBERRI  PIC X(79).                                      00000620
      * CODE TRANSACTION                                                00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MCODTRAI  PIC X(4).                                       00000670
      * CICS DE TRAVAIL                                                 00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MCICSI    PIC X(5).                                       00000720
      * NETNAME                                                         00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MNETNAMI  PIC X(8).                                       00000770
      * CODE TERMINAL                                                   00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MSCREENI  PIC X(4).                                       00000820
      ***************************************************************** 00000830
      * GCT/SAP - sections                                              00000840
      ***************************************************************** 00000850
       01   EFM63O REDEFINES EFM63I.                                    00000860
           02 FILLER    PIC X(12).                                      00000870
      * DATE DU JOUR                                                    00000880
           02 FILLER    PIC X(2).                                       00000890
           02 MDATJOUA  PIC X.                                          00000900
           02 MDATJOUC  PIC X.                                          00000910
           02 MDATJOUP  PIC X.                                          00000920
           02 MDATJOUH  PIC X.                                          00000930
           02 MDATJOUV  PIC X.                                          00000940
           02 MDATJOUO  PIC X(10).                                      00000950
      * HEURE                                                           00000960
           02 FILLER    PIC X(2).                                       00000970
           02 MTIMJOUA  PIC X.                                          00000980
           02 MTIMJOUC  PIC X.                                          00000990
           02 MTIMJOUP  PIC X.                                          00001000
           02 MTIMJOUH  PIC X.                                          00001010
           02 MTIMJOUV  PIC X.                                          00001020
           02 MTIMJOUO  PIC X(5).                                       00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MPAGEA    PIC X.                                          00001050
           02 MPAGEC    PIC X.                                          00001060
           02 MPAGEP    PIC X.                                          00001070
           02 MPAGEH    PIC X.                                          00001080
           02 MPAGEV    PIC X.                                          00001090
           02 MPAGEO    PIC X(2).                                       00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MNBPA     PIC X.                                          00001120
           02 MNBPC     PIC X.                                          00001130
           02 MNBPP     PIC X.                                          00001140
           02 MNBPH     PIC X.                                          00001150
           02 MNBPV     PIC X.                                          00001160
           02 MNBPO     PIC X(2).                                       00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MCINTERFACEA   PIC X.                                     00001190
           02 MCINTERFACEC   PIC X.                                     00001200
           02 MCINTERFACEP   PIC X.                                     00001210
           02 MCINTERFACEH   PIC X.                                     00001220
           02 MCINTERFACEV   PIC X.                                     00001230
           02 MCINTERFACEO   PIC X(5).                                  00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MLINTERFACEA   PIC X.                                     00001260
           02 MLINTERFACEC   PIC X.                                     00001270
           02 MLINTERFACEP   PIC X.                                     00001280
           02 MLINTERFACEH   PIC X.                                     00001290
           02 MLINTERFACEV   PIC X.                                     00001300
           02 MLINTERFACEO   PIC X(30).                                 00001310
           02 DFHMS1 OCCURS   3 TIMES .                                 00001320
             03 FILLER       PIC X(2).                                  00001330
             03 MCRITEREA    PIC X.                                     00001340
             03 MCRITEREC    PIC X.                                     00001350
             03 MCRITEREP    PIC X.                                     00001360
             03 MCRITEREH    PIC X.                                     00001370
             03 MCRITEREV    PIC X.                                     00001380
             03 MCRITEREO    PIC X(8).                                  00001390
           02 MTABRO OCCURS   10 TIMES .                                00001400
             03 FILLER       PIC X(2).                                  00001410
             03 MCTYPOPERA   PIC X.                                     00001420
             03 MCTYPOPERC   PIC X.                                     00001430
             03 MCTYPOPERP   PIC X.                                     00001440
             03 MCTYPOPERH   PIC X.                                     00001450
             03 MCTYPOPERV   PIC X.                                     00001460
             03 MCTYPOPERO   PIC X(5).                                  00001470
             03 FILLER       PIC X(2).                                  00001480
             03 MCNATOPERA   PIC X.                                     00001490
             03 MCNATOPERC   PIC X.                                     00001500
             03 MCNATOPERP   PIC X.                                     00001510
             03 MCNATOPERH   PIC X.                                     00001520
             03 MCNATOPERV   PIC X.                                     00001530
             03 MCNATOPERO   PIC X(5).                                  00001540
             03 FILLER       PIC X(2).                                  00001550
             03 MCSEC1A      PIC X.                                     00001560
             03 MCSEC1C PIC X.                                          00001570
             03 MCSEC1P PIC X.                                          00001580
             03 MCSEC1H PIC X.                                          00001590
             03 MCSEC1V PIC X.                                          00001600
             03 MCSEC1O      PIC X.                                     00001610
             03 FILLER       PIC X(2).                                  00001620
             03 MCSEC2A      PIC X.                                     00001630
             03 MCSEC2C PIC X.                                          00001640
             03 MCSEC2P PIC X.                                          00001650
             03 MCSEC2H PIC X.                                          00001660
             03 MCSEC2V PIC X.                                          00001670
             03 MCSEC2O      PIC X.                                     00001680
             03 FILLER       PIC X(2).                                  00001690
             03 MCSEC3A      PIC X.                                     00001700
             03 MCSEC3C PIC X.                                          00001710
             03 MCSEC3P PIC X.                                          00001720
             03 MCSEC3H PIC X.                                          00001730
             03 MCSEC3V PIC X.                                          00001740
             03 MCSEC3O      PIC X.                                     00001750
      * ZONE CMD AIDA                                                   00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MLIBERRA  PIC X.                                          00001780
           02 MLIBERRC  PIC X.                                          00001790
           02 MLIBERRP  PIC X.                                          00001800
           02 MLIBERRH  PIC X.                                          00001810
           02 MLIBERRV  PIC X.                                          00001820
           02 MLIBERRO  PIC X(79).                                      00001830
      * CODE TRANSACTION                                                00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MCODTRAA  PIC X.                                          00001860
           02 MCODTRAC  PIC X.                                          00001870
           02 MCODTRAP  PIC X.                                          00001880
           02 MCODTRAH  PIC X.                                          00001890
           02 MCODTRAV  PIC X.                                          00001900
           02 MCODTRAO  PIC X(4).                                       00001910
      * CICS DE TRAVAIL                                                 00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MCICSA    PIC X.                                          00001940
           02 MCICSC    PIC X.                                          00001950
           02 MCICSP    PIC X.                                          00001960
           02 MCICSH    PIC X.                                          00001970
           02 MCICSV    PIC X.                                          00001980
           02 MCICSO    PIC X(5).                                       00001990
      * NETNAME                                                         00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MNETNAMA  PIC X.                                          00002020
           02 MNETNAMC  PIC X.                                          00002030
           02 MNETNAMP  PIC X.                                          00002040
           02 MNETNAMH  PIC X.                                          00002050
           02 MNETNAMV  PIC X.                                          00002060
           02 MNETNAMO  PIC X(8).                                       00002070
      * CODE TERMINAL                                                   00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MSCREENA  PIC X.                                          00002100
           02 MSCREENC  PIC X.                                          00002110
           02 MSCREENP  PIC X.                                          00002120
           02 MSCREENH  PIC X.                                          00002130
           02 MSCREENV  PIC X.                                          00002140
           02 MSCREENO  PIC X(4).                                       00002150
                                                                                
