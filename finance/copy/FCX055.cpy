      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  FCX055-DSECT.                                                00000100
           02 FCX055-TYPENREG             PIC X(02).                    00000200
           02 FCX055-NSOCIETE             PIC X(03).                    00000210
           02 FCX055-NLIEU                PIC X(03).                    00000220
           02 FCX055-NVENTE               PIC X(07).                    00000230
           02 FCX055-DATA-01.                                           00002300
      * LIGNES ARTICLES                                                 00002310
              05 FCX055-DDELIV            PIC X(08).                    00002400
              05 FCX055-DCREATION         PIC X(08).                    00002500
              05 FCX055-HCREATION         PIC X(04).                    00002600
              05 FCX055-NCODIC            PIC X(07).                    00002700
              05 FCX055-QVENDUE           PIC S9(05)    COMP-3.         00002800
              05 FCX055-CTAUXTVA          PIC X(01).                    00002900
              05 FCX055-PVTOTAL           PIC S9(07)V99 COMP-3.         00003000
              05 FCX055-PRA               PIC S9(07)V99 COMP-3.         00003100
              05 FCX055-PRMP              PIC S9(07)V99 COMP-3.         00003200
              05 FCX055-CVENDEUR          PIC X(06).                    00003300
              05 FCX055-PCOMM             PIC S9(05)    COMP-3.         00003400
              05 FCX055-CPOSTAL           PIC X(05).                    00003500
              05 FCX055-CMODDEL           PIC X(05).                    00003600
              05 FCX055-PREMISE           PIC S9(07)V99 COMP-3.         00004000
              05 FCX055-PPSE              PIC S9(07)V99 COMP-3.         00004400
              05 FCX055-NPSE              PIC X(08).                    00004500
              05 FCX055-WPSESEULE         PIC X(01).                    00004600
      * VAUT 'P' SI PSE SEULE                                           00004700
              05 FCX055-CPROTOUR          PIC X(05).                    00004610
              05 FCX055-CTOURNEE          PIC X(03).                    00004610
              05 FCX055-CLIVR             PIC X(10).                    00004610
              05 FCX055-NSEQNQ            PIC 9(05).                    00004610
              05 FCX055-NSEQNQX REDEFINES FCX055-NSEQNQ PIC X(05).      00004610
              05 FCX055-LIBRE             PIC X(78).                    00004610
           02 FCX055-DATA-00              REDEFINES FCX055-DATA-01.     00004800
              05 FCX055-CTITRENOM         PIC X(05).                    00004900
              05 FCX055-LNOM              PIC X(25).                    00005000
              05 FCX055-LPRENOM           PIC X(15).                    00005100
              05 FCX055-CVOIE             PIC X(05).                    00005200
              05 FCX055-CTVOIE            PIC X(04).                    00005300
              05 FCX055-LNOMVOIE          PIC X(21).                    00005400
              05 FCX055-LBATIMENT         PIC X(03).                    00005500
              05 FCX055-LESCALIER         PIC X(03).                    00005600
              05 FCX055-LETAGE            PIC X(03).                    00005700
              05 FCX055-LPORTE            PIC X(03).                    00005800
              05 FCX055-LCOMMUNE          PIC X(32).                    00005900
              05 FCX055-CPOSTAL0          PIC X(05).                    00006000
              05 FCX055-TELDOM            PIC X(10).                    00006100
              05 FCX055-TELBUR            PIC X(10).                    00006200
              05 FCX055-RECEPT-FACT       PIC X(01).                    00006300
      * VAUT 'R' SI PAIEMENT A RECEPTION DE FACTURE                     00006400
              05 FCX055-CREDIT            OCCURS 5.                     00006500
                 10 FCX055-CORG           PIC X(02).                    00006600
                 10 FCX055-MONTANT        PIC S9(07)V99 COMP-3.         00006700
              05 FCX055-WEXPORT           PIC X(01).                            
              05 FILLER                   PIC X(04).                    00006800
           02 FCX055-DATA-02              REDEFINES FCX055-DATA-01.     00004800
              05 FCX055-CPRESTATION       PIC X(05).                            
              05 FCX055-PPRESTATION       PIC S9(7)V99 COMP-3.                  
                                                                                
