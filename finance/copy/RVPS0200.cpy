      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVPS0200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVPS0200                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVPS0200.                                                            
           02  PS02-NSOC                                                        
               PIC X(0003).                                                     
           02  PS02-CGCPLT                                                      
               PIC X(0005).                                                     
           02  PS02-NOSAV                                                       
               PIC X(0003).                                                     
           02  PS02-CSECTEUR                                                    
               PIC X(0005).                                                     
           02  PS02-DEFFET                                                      
               PIC X(0006).                                                     
           02  PS02-NBPSE                                                       
               PIC S9(7) COMP-3.                                                
           02  PS02-NBPSER                                                      
               PIC S9(7) COMP-3.                                                
           02  PS02-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVPS0200                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVPS0200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS02-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS02-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS02-CGCPLT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS02-CGCPLT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS02-NOSAV-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS02-NOSAV-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS02-CSECTEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS02-CSECTEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS02-DEFFET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS02-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS02-NBPSE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS02-NBPSE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS02-NBPSER-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS02-NBPSER-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS02-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS02-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
