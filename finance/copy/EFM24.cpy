      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - D�tail du compte                                          00000020
      ***************************************************************** 00000030
       01   EFM24I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(2).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(2).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPLANL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MCPLANL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCPLANF   PIC X.                                          00000150
           02 FILLER    PIC X(2).                                       00000160
           02 MCPLANI   PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPLANL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MLPLANL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLPLANF   PIC X.                                          00000190
           02 FILLER    PIC X(2).                                       00000200
           02 MLPLANI   PIC X(20).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMAJL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDMAJL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDMAJF    PIC X.                                          00000230
           02 FILLER    PIC X(2).                                       00000240
           02 MDMAJI    PIC X(10).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTRANSML  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MTRANSML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTRANSMF  PIC X.                                          00000270
           02 FILLER    PIC X(2).                                       00000280
           02 MTRANSMI  PIC X.                                          00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMPTEL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCOMPTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCOMPTEF  PIC X.                                          00000310
           02 FILLER    PIC X(2).                                       00000320
           02 MCOMPTEI  PIC X(6).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMPTECL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLCOMPTECL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLCOMPTECF     PIC X.                                     00000350
           02 FILLER    PIC X(2).                                       00000360
           02 MLCOMPTECI     PIC X(15).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMPTELL     COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLCOMPTELL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLCOMPTELF     PIC X.                                     00000390
           02 FILLER    PIC X(2).                                       00000400
           02 MLCOMPTELI     PIC X(30).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMASQUEL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MCMASQUEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCMASQUEF      PIC X.                                     00000430
           02 FILLER    PIC X(2).                                       00000440
           02 MCMASQUEI      PIC X(6).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNAUXL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNAUXL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNAUXF    PIC X.                                          00000470
           02 FILLER    PIC X(2).                                       00000480
           02 MNAUXI    PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTYPCOMPTEL   COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MWTYPCOMPTEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MWTYPCOMPTEF   PIC X.                                     00000510
           02 FILLER    PIC X(2).                                       00000520
           02 MWTYPCOMPTEI   PIC X.                                     00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTYPCOMPTELL  COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MWTYPCOMPTELL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MWTYPCOMPTELF  PIC X.                                     00000550
           02 FILLER    PIC X(2).                                       00000560
           02 MWTYPCOMPTELI  PIC X(20).                                 00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCONSODL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MCCONSODL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCCONSODF      PIC X.                                     00000590
           02 FILLER    PIC X(2).                                       00000600
           02 MCCONSODI      PIC X(6).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCONSOCL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MCCONSOCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCCONSOCF      PIC X.                                     00000630
           02 FILLER    PIC X(2).                                       00000640
           02 MCCONSOCI      PIC X(6).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCONSODL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MLCONSODL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLCONSODF      PIC X.                                     00000670
           02 FILLER    PIC X(2).                                       00000680
           02 MLCONSODI      PIC X(29).                                 00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCONSOCL      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MLCONSOCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLCONSOCF      PIC X.                                     00000710
           02 FILLER    PIC X(2).                                       00000720
           02 MLCONSOCI      PIC X(29).                                 00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWINTERFACEL   COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MWINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MWINTERFACEF   PIC X.                                     00000750
           02 FILLER    PIC X(2).                                       00000760
           02 MWINTERFACEI   PIC X.                                     00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWLETTRAGEL    COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MWLETTRAGEL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MWLETTRAGEF    PIC X.                                     00000790
           02 FILLER    PIC X(2).                                       00000800
           02 MWLETTRAGEI    PIC X.                                     00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWAUXILIARISEL      COMP PIC S9(4).                       00000820
      *--                                                                       
           02 MWAUXILIARISEL COMP-5 PIC S9(4).                                  
      *}                                                                        
           02 MWAUXILIARISEF      PIC X.                                00000830
           02 FILLER    PIC X(2).                                       00000840
           02 MWAUXILIARISEI      PIC X.                                00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWREGAUTOL     COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MWREGAUTOL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MWREGAUTOF     PIC X.                                     00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MWREGAUTOI     PIC X.                                     00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWCOLLECTIFL   COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MWCOLLECTIFL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MWCOLLECTIFF   PIC X.                                     00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MWCOLLECTIFI   PIC X.                                     00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWREGAUTOCL    COMP PIC S9(4).                            00000940
      *--                                                                       
           02 MWREGAUTOCL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MWREGAUTOCF    PIC X.                                     00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MWREGAUTOCI    PIC X.                                     00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWABONNEL      COMP PIC S9(4).                            00000980
      *--                                                                       
           02 MWABONNEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWABONNEF      PIC X.                                     00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MWABONNEI      PIC X.                                     00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEPURATIONL   COMP PIC S9(4).                            00001020
      *--                                                                       
           02 MCEPURATIONL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MCEPURATIONF   PIC X.                                     00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MCEPURATIONI   PIC X.                                     00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLEPURATIONL   COMP PIC S9(4).                            00001060
      *--                                                                       
           02 MLEPURATIONL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLEPURATIONF   PIC X.                                     00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MLEPURATIONI   PIC X(22).                                 00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPOINTAGEL    COMP PIC S9(4).                            00001100
      *--                                                                       
           02 MWPOINTAGEL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MWPOINTAGEF    PIC X.                                     00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MWPOINTAGEI    PIC X.                                     00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWEDITIONL     COMP PIC S9(4).                            00001140
      *--                                                                       
           02 MWEDITIONL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MWEDITIONF     PIC X.                                     00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MWEDITIONI     PIC X.                                     00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWCLOTUREL     COMP PIC S9(4).                            00001180
      *--                                                                       
           02 MWCLOTUREL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MWCLOTUREF     PIC X.                                     00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MWCLOTUREI     PIC X.                                     00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCLOTUREL     COMP PIC S9(4).                            00001220
      *--                                                                       
           02 MDCLOTUREL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDCLOTUREF     PIC X.                                     00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MDCLOTUREI     PIC X(10).                                 00001250
           02 MCODECATD OCCURS   6 TIMES .                              00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODECATL    COMP PIC S9(4).                            00001270
      *--                                                                       
             03 MCODECATL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODECATF    PIC X.                                     00001280
             03 FILLER  PIC X(2).                                       00001290
             03 MCODECATI    PIC X(6).                                  00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MLIBERRI  PIC X(79).                                      00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MCODTRAI  PIC X(4).                                       00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MCICSI    PIC X(5).                                       00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MSCREENI  PIC X(4).                                       00001460
      ***************************************************************** 00001470
      * GCT - D�tail du compte                                          00001480
      ***************************************************************** 00001490
       01   EFM24O REDEFINES EFM24I.                                    00001500
           02 FILLER    PIC X(12).                                      00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MDATJOUA  PIC X.                                          00001530
           02 MDATJOUC  PIC X.                                          00001540
           02 MDATJOUH  PIC X.                                          00001550
           02 MDATJOUO  PIC X(10).                                      00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MTIMJOUA  PIC X.                                          00001580
           02 MTIMJOUC  PIC X.                                          00001590
           02 MTIMJOUH  PIC X.                                          00001600
           02 MTIMJOUO  PIC X(5).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MCPLANA   PIC X.                                          00001630
           02 MCPLANC   PIC X.                                          00001640
           02 MCPLANH   PIC X.                                          00001650
           02 MCPLANO   PIC X(4).                                       00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MLPLANA   PIC X.                                          00001680
           02 MLPLANC   PIC X.                                          00001690
           02 MLPLANH   PIC X.                                          00001700
           02 MLPLANO   PIC X(20).                                      00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MDMAJA    PIC X.                                          00001730
           02 MDMAJC    PIC X.                                          00001740
           02 MDMAJH    PIC X.                                          00001750
           02 MDMAJO    PIC X(10).                                      00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MTRANSMA  PIC X.                                          00001780
           02 MTRANSMC  PIC X.                                          00001790
           02 MTRANSMH  PIC X.                                          00001800
           02 MTRANSMO  PIC X.                                          00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MCOMPTEA  PIC X.                                          00001830
           02 MCOMPTEC  PIC X.                                          00001840
           02 MCOMPTEH  PIC X.                                          00001850
           02 MCOMPTEO  PIC X(6).                                       00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MLCOMPTECA     PIC X.                                     00001880
           02 MLCOMPTECC     PIC X.                                     00001890
           02 MLCOMPTECH     PIC X.                                     00001900
           02 MLCOMPTECO     PIC X(15).                                 00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MLCOMPTELA     PIC X.                                     00001930
           02 MLCOMPTELC     PIC X.                                     00001940
           02 MLCOMPTELH     PIC X.                                     00001950
           02 MLCOMPTELO     PIC X(30).                                 00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MCMASQUEA      PIC X.                                     00001980
           02 MCMASQUEC PIC X.                                          00001990
           02 MCMASQUEH PIC X.                                          00002000
           02 MCMASQUEO      PIC X(6).                                  00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MNAUXA    PIC X.                                          00002030
           02 MNAUXC    PIC X.                                          00002040
           02 MNAUXH    PIC X.                                          00002050
           02 MNAUXO    PIC X(3).                                       00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MWTYPCOMPTEA   PIC X.                                     00002080
           02 MWTYPCOMPTEC   PIC X.                                     00002090
           02 MWTYPCOMPTEH   PIC X.                                     00002100
           02 MWTYPCOMPTEO   PIC X.                                     00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MWTYPCOMPTELA  PIC X.                                     00002130
           02 MWTYPCOMPTELC  PIC X.                                     00002140
           02 MWTYPCOMPTELH  PIC X.                                     00002150
           02 MWTYPCOMPTELO  PIC X(20).                                 00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MCCONSODA      PIC X.                                     00002180
           02 MCCONSODC PIC X.                                          00002190
           02 MCCONSODH PIC X.                                          00002200
           02 MCCONSODO      PIC X(6).                                  00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MCCONSOCA      PIC X.                                     00002230
           02 MCCONSOCC PIC X.                                          00002240
           02 MCCONSOCH PIC X.                                          00002250
           02 MCCONSOCO      PIC X(6).                                  00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MLCONSODA      PIC X.                                     00002280
           02 MLCONSODC PIC X.                                          00002290
           02 MLCONSODH PIC X.                                          00002300
           02 MLCONSODO      PIC X(29).                                 00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MLCONSOCA      PIC X.                                     00002330
           02 MLCONSOCC PIC X.                                          00002340
           02 MLCONSOCH PIC X.                                          00002350
           02 MLCONSOCO      PIC X(29).                                 00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MWINTERFACEA   PIC X.                                     00002380
           02 MWINTERFACEC   PIC X.                                     00002390
           02 MWINTERFACEH   PIC X.                                     00002400
           02 MWINTERFACEO   PIC X.                                     00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MWLETTRAGEA    PIC X.                                     00002430
           02 MWLETTRAGEC    PIC X.                                     00002440
           02 MWLETTRAGEH    PIC X.                                     00002450
           02 MWLETTRAGEO    PIC X.                                     00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MWAUXILIARISEA      PIC X.                                00002480
           02 MWAUXILIARISEC PIC X.                                     00002490
           02 MWAUXILIARISEH PIC X.                                     00002500
           02 MWAUXILIARISEO      PIC X.                                00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MWREGAUTOA     PIC X.                                     00002530
           02 MWREGAUTOC     PIC X.                                     00002540
           02 MWREGAUTOH     PIC X.                                     00002550
           02 MWREGAUTOO     PIC X.                                     00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MWCOLLECTIFA   PIC X.                                     00002580
           02 MWCOLLECTIFC   PIC X.                                     00002590
           02 MWCOLLECTIFH   PIC X.                                     00002600
           02 MWCOLLECTIFO   PIC X.                                     00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MWREGAUTOCA    PIC X.                                     00002630
           02 MWREGAUTOCC    PIC X.                                     00002640
           02 MWREGAUTOCH    PIC X.                                     00002650
           02 MWREGAUTOCO    PIC X.                                     00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MWABONNEA      PIC X.                                     00002680
           02 MWABONNEC PIC X.                                          00002690
           02 MWABONNEH PIC X.                                          00002700
           02 MWABONNEO      PIC X.                                     00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MCEPURATIONA   PIC X.                                     00002730
           02 MCEPURATIONC   PIC X.                                     00002740
           02 MCEPURATIONH   PIC X.                                     00002750
           02 MCEPURATIONO   PIC X.                                     00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MLEPURATIONA   PIC X.                                     00002780
           02 MLEPURATIONC   PIC X.                                     00002790
           02 MLEPURATIONH   PIC X.                                     00002800
           02 MLEPURATIONO   PIC X(22).                                 00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MWPOINTAGEA    PIC X.                                     00002830
           02 MWPOINTAGEC    PIC X.                                     00002840
           02 MWPOINTAGEH    PIC X.                                     00002850
           02 MWPOINTAGEO    PIC X.                                     00002860
           02 FILLER    PIC X(2).                                       00002870
           02 MWEDITIONA     PIC X.                                     00002880
           02 MWEDITIONC     PIC X.                                     00002890
           02 MWEDITIONH     PIC X.                                     00002900
           02 MWEDITIONO     PIC X.                                     00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MWCLOTUREA     PIC X.                                     00002930
           02 MWCLOTUREC     PIC X.                                     00002940
           02 MWCLOTUREH     PIC X.                                     00002950
           02 MWCLOTUREO     PIC X.                                     00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MDCLOTUREA     PIC X.                                     00002980
           02 MDCLOTUREC     PIC X.                                     00002990
           02 MDCLOTUREH     PIC X.                                     00003000
           02 MDCLOTUREO     PIC X(10).                                 00003010
           02 DFHMS1 OCCURS   6 TIMES .                                 00003020
             03 FILLER       PIC X(2).                                  00003030
             03 MCODECATA    PIC X.                                     00003040
             03 MCODECATC    PIC X.                                     00003050
             03 MCODECATH    PIC X.                                     00003060
             03 MCODECATO    PIC X(6).                                  00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MLIBERRA  PIC X.                                          00003090
           02 MLIBERRC  PIC X.                                          00003100
           02 MLIBERRH  PIC X.                                          00003110
           02 MLIBERRO  PIC X(79).                                      00003120
           02 FILLER    PIC X(2).                                       00003130
           02 MCODTRAA  PIC X.                                          00003140
           02 MCODTRAC  PIC X.                                          00003150
           02 MCODTRAH  PIC X.                                          00003160
           02 MCODTRAO  PIC X(4).                                       00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MCICSA    PIC X.                                          00003190
           02 MCICSC    PIC X.                                          00003200
           02 MCICSH    PIC X.                                          00003210
           02 MCICSO    PIC X(5).                                       00003220
           02 FILLER    PIC X(2).                                       00003230
           02 MSCREENA  PIC X.                                          00003240
           02 MSCREENC  PIC X.                                          00003250
           02 MSCREENH  PIC X.                                          00003260
           02 MSCREENO  PIC X(4).                                       00003270
                                                                                
