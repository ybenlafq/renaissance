      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * DCLGEN TABLE(DSA000.RVPS0401)                                  *        
      *        LIBRARY(DSA016.DEVL.RVPS0401.COBOL)                     *        
      *        ACTION(REPLACE)                                         *        
      *        LANGUAGE(COBOL)                                         *        
      *        NAMES(PS04-)                                            *        
      *        STRUCTURE(RVPS0401)                                     *        
      *        APOST                                                   *        
      *        COLSUFFIX(YES)                                          *        
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *        
      ******************************************************************        
           EXEC SQL DECLARE DSA000.RVPS0401 TABLE                               
           ( NSOC                           CHAR(3) NOT NULL,                   
             NLIEU                          CHAR(3) NOT NULL,                   
             TYPE                           CHAR(5) NOT NULL,                   
             DINIT                          CHAR(8) NOT NULL,                   
             INFO1                          CHAR(10) NOT NULL,                  
             INFO2                          CHAR(10) NOT NULL,                  
             INFO3                          CHAR(10) NOT NULL,                  
             MONTTOT                        DECIMAL(9, 2) NOT NULL,             
             DUREE                          DECIMAL(3, 0) NOT NULL,             
             DECHEANC                       CHAR(8) NOT NULL,                   
             NBMOIS                         DECIMAL(3, 0) NOT NULL,             
             MTMENSUEL1                     DECIMAL(9, 2) NOT NULL,             
             MTMENSUEL2                     DECIMAL(9, 2) NOT NULL,             
             MTMENSUEL3                     DECIMAL(9, 2) NOT NULL,             
             MTMENSUEL4                     DECIMAL(9, 2) NOT NULL,             
             MTMENSUEL5                     DECIMAL(9, 2) NOT NULL,             
             MTDEVERSE                      DECIMAL(9, 2) NOT NULL,             
             PRORATA                        DECIMAL(9, 2) NOT NULL,             
             DCALCUL                        CHAR(8) NOT NULL,                   
             DANNUL                         CHAR(8) NOT NULL,                   
             DSYST                          DECIMAL(13, 0) NOT NULL,            
             MTANNUL                        DECIMAL(9, 2) NOT NULL              
           ) END-EXEC.                                                          
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RVPS0401                    *        
      ******************************************************************        
       01  RVPS0401.                                                            
      *                       NSOC                                              
           10 PS04-NSOC            PIC X(3).                                    
      *                       NLIEU                                             
           10 PS04-NLIEU           PIC X(3).                                    
      *                       TYPE                                              
           10 PS04-TYPE            PIC X(5).                                    
      *                       DINIT                                             
           10 PS04-DINIT           PIC X(8).                                    
      *                       INFO1                                             
           10 PS04-INFO1           PIC X(10).                                   
      *                       INFO2                                             
           10 PS04-INFO2           PIC X(10).                                   
      *                       INFO3                                             
           10 PS04-INFO3           PIC X(10).                                   
      *                       MONTTOT                                           
           10 PS04-MONTTOT         PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       DUREE                                             
           10 PS04-DUREE           PIC S9(3)V USAGE COMP-3.                     
      *                       DECHEANC                                          
           10 PS04-DECHEANC        PIC X(8).                                    
      *                       NBMOIS                                            
           10 PS04-NBMOIS          PIC S9(3)V USAGE COMP-3.                     
      *                       MTMENSUEL1                                        
           10 PS04-MTMENSUEL1      PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       MTMENSUEL2                                        
           10 PS04-MTMENSUEL2      PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       MTMENSUEL3                                        
           10 PS04-MTMENSUEL3      PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       MTMENSUEL4                                        
           10 PS04-MTMENSUEL4      PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       MTMENSUEL5                                        
           10 PS04-MTMENSUEL5      PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       MTDEVERSE                                         
           10 PS04-MTDEVERSE       PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       PRORATA                                           
           10 PS04-PRORATA         PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       DCALCUL                                           
           10 PS04-DCALCUL         PIC X(8).                                    
      *                       DANNUL                                            
           10 PS04-DANNUL          PIC X(8).                                    
      *                       DSYST                                             
           10 PS04-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       MTANNUL                                           
           10 PS04-MTANNUL         PIC S9(7)V9(2) USAGE COMP-3.                 
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 22      *        
      ******************************************************************        
                                                                                
