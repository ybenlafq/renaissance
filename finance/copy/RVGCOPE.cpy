      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GCOPE CORRESPONDANCE CMARQ NENTCDE     *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVGCOPE.                                                             
           05  GCOPE-CTABLEG2    PIC X(15).                                     
           05  GCOPE-CTABLEG2-REDEF REDEFINES GCOPE-CTABLEG2.                   
               10  GCOPE-CMARQ           PIC X(05).                             
               10  GCOPE-NENTCDE         PIC X(05).                             
           05  GCOPE-WTABLEG     PIC X(80).                                     
           05  GCOPE-WTABLEG-REDEF  REDEFINES GCOPE-WTABLEG.                    
               10  GCOPE-WENTCDE         PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGCOPE-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GCOPE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GCOPE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GCOPE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GCOPE-WTABLEG-F   PIC S9(4) COMP-5.                              

       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
