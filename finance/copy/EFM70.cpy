      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - MENU PARAMETRES ECS                                       00000020
      ***************************************************************** 00000030
       01   EFM70I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MZONCMDI  PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUECSL   COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNUECSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNUECSF   PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNUECSI   PIC X(5).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUECSMINL     COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MNUECSMINL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNUECSMINF     PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNUECSMINI     PIC X(5).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUECSMAXL     COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MNUECSMAXL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNUECSMAXF     PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MNUECSMAXI     PIC X(5).                                  00000310
      * ZONE CMD AIDA                                                   00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000330
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000340
           02 FILLER    PIC X(4).                                       00000350
           02 MLIBERRI  PIC X(79).                                      00000360
      * CODE TRANSACTION                                                00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCODTRAI  PIC X(4).                                       00000410
      * CICS DE TRAVAIL                                                 00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000430
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MCICSI    PIC X(5).                                       00000460
      * CODE TERMINAL                                                   00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MSCREENI  PIC X(4).                                       00000510
      ***************************************************************** 00000520
      * GCT - MENU PARAMETRES ECS                                       00000530
      ***************************************************************** 00000540
       01   EFM70O REDEFINES EFM70I.                                    00000550
           02 FILLER    PIC X(12).                                      00000560
      * DATE DU JOUR                                                    00000570
           02 FILLER    PIC X(2).                                       00000580
           02 MDATJOUA  PIC X.                                          00000590
           02 MDATJOUC  PIC X.                                          00000600
           02 MDATJOUP  PIC X.                                          00000610
           02 MDATJOUH  PIC X.                                          00000620
           02 MDATJOUV  PIC X.                                          00000630
           02 MDATJOUO  PIC X(10).                                      00000640
      * HEURE                                                           00000650
           02 FILLER    PIC X(2).                                       00000660
           02 MTIMJOUA  PIC X.                                          00000670
           02 MTIMJOUC  PIC X.                                          00000680
           02 MTIMJOUP  PIC X.                                          00000690
           02 MTIMJOUH  PIC X.                                          00000700
           02 MTIMJOUV  PIC X.                                          00000710
           02 MTIMJOUO  PIC X(5).                                       00000720
           02 FILLER    PIC X(2).                                       00000730
           02 MZONCMDA  PIC X.                                          00000740
           02 MZONCMDC  PIC X.                                          00000750
           02 MZONCMDP  PIC X.                                          00000760
           02 MZONCMDH  PIC X.                                          00000770
           02 MZONCMDV  PIC X.                                          00000780
           02 MZONCMDO  PIC X(2).                                       00000790
           02 FILLER    PIC X(2).                                       00000800
           02 MNUECSA   PIC X.                                          00000810
           02 MNUECSC   PIC X.                                          00000820
           02 MNUECSP   PIC X.                                          00000830
           02 MNUECSH   PIC X.                                          00000840
           02 MNUECSV   PIC X.                                          00000850
           02 MNUECSO   PIC X(5).                                       00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MNUECSMINA     PIC X.                                     00000880
           02 MNUECSMINC     PIC X.                                     00000890
           02 MNUECSMINP     PIC X.                                     00000900
           02 MNUECSMINH     PIC X.                                     00000910
           02 MNUECSMINV     PIC X.                                     00000920
           02 MNUECSMINO     PIC X(5).                                  00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MNUECSMAXA     PIC X.                                     00000950
           02 MNUECSMAXC     PIC X.                                     00000960
           02 MNUECSMAXP     PIC X.                                     00000970
           02 MNUECSMAXH     PIC X.                                     00000980
           02 MNUECSMAXV     PIC X.                                     00000990
           02 MNUECSMAXO     PIC X(5).                                  00001000
      * ZONE CMD AIDA                                                   00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MLIBERRA  PIC X.                                          00001030
           02 MLIBERRC  PIC X.                                          00001040
           02 MLIBERRP  PIC X.                                          00001050
           02 MLIBERRH  PIC X.                                          00001060
           02 MLIBERRV  PIC X.                                          00001070
           02 MLIBERRO  PIC X(79).                                      00001080
      * CODE TRANSACTION                                                00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MCODTRAA  PIC X.                                          00001110
           02 MCODTRAC  PIC X.                                          00001120
           02 MCODTRAP  PIC X.                                          00001130
           02 MCODTRAH  PIC X.                                          00001140
           02 MCODTRAV  PIC X.                                          00001150
           02 MCODTRAO  PIC X(4).                                       00001160
      * CICS DE TRAVAIL                                                 00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MCICSA    PIC X.                                          00001190
           02 MCICSC    PIC X.                                          00001200
           02 MCICSP    PIC X.                                          00001210
           02 MCICSH    PIC X.                                          00001220
           02 MCICSV    PIC X.                                          00001230
           02 MCICSO    PIC X(5).                                       00001240
      * CODE TERMINAL                                                   00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MSCREENA  PIC X.                                          00001270
           02 MSCREENC  PIC X.                                          00001280
           02 MSCREENP  PIC X.                                          00001290
           02 MSCREENH  PIC X.                                          00001300
           02 MSCREENV  PIC X.                                          00001310
           02 MSCREENO  PIC X(4).                                       00001320
                                                                                
