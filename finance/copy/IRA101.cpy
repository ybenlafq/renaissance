      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRA101 AU 15/06/1995  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,25,BI,A,                          *        
      *                           35,05,BI,A,                          *        
      *                           40,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRA101.                                                        
            05 NOMETAT-IRA101           PIC X(6) VALUE 'IRA101'.                
            05 RUPTURES-IRA101.                                                 
           10 IRA101-NSOCIETE           PIC X(03).                      007  003
           10 IRA101-LNOM               PIC X(25).                      010  025
           10 IRA101-NDEMANDE           PIC X(05).                      035  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRA101-SEQUENCE           PIC S9(04) COMP.                040  002
      *--                                                                       
           10 IRA101-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRA101.                                                   
           10 IRA101-BANQUE             PIC X(20).                      042  020
           10 IRA101-CTYPDEMANDE        PIC X(05).                      062  005
           10 IRA101-LPRENOM            PIC X(15).                      067  015
           10 IRA101-NLIEU              PIC X(03).                      082  003
           10 IRA101-TYPREGL            PIC X(20).                      085  020
           10 IRA101-MTREMBTOT          PIC 9(09)V9(2).                 105  011
           10 IRA101-DATEPARAM          PIC X(08).                      116  008
           10 IRA101-DECHEANCE          PIC X(08).                      124  008
            05 FILLER                      PIC X(381).                          
                                                                                
