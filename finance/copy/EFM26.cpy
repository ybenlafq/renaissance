      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - d�tail des sections                                       00000020
      ***************************************************************** 00000030
       01   EFM26I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(2).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(2).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITREL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MTITREL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTITREF   PIC X.                                          00000150
           02 FILLER    PIC X(2).                                       00000160
           02 MTITREI   PIC X(8).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPLANL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCPLANL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCPLANF   PIC X.                                          00000190
           02 FILLER    PIC X(2).                                       00000200
           02 MCPLANI   PIC X(4).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPLANL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLPLANL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLPLANF   PIC X.                                          00000230
           02 FILLER    PIC X(2).                                       00000240
           02 MLPLANI   PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODEL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCODEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCODEF    PIC X.                                          00000270
           02 FILLER    PIC X(2).                                       00000280
           02 MCODEI    PIC X(6).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCODECL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLCODECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCODECF  PIC X.                                          00000310
           02 FILLER    PIC X(2).                                       00000320
           02 MLCODECI  PIC X(15).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCODELL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLCODELL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCODELF  PIC X.                                          00000350
           02 FILLER    PIC X(2).                                       00000360
           02 MLCODELI  PIC X(30).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMASQUEL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MMASQUEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMASQUEF  PIC X.                                          00000390
           02 FILLER    PIC X(2).                                       00000400
           02 MMASQUEI  PIC X(6).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBFIRL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLIBFIRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBFIRF  PIC X.                                          00000430
           02 FILLER    PIC X(2).                                       00000440
           02 MLIBFIRI  PIC X(8).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFIRL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MWFIRL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MWFIRF    PIC X.                                          00000470
           02 FILLER    PIC X(2).                                       00000480
           02 MWFIRI    PIC X.                                          00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVALFIRL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MVALFIRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MVALFIRF  PIC X.                                          00000510
           02 FILLER    PIC X(2).                                       00000520
           02 MVALFIRI  PIC X(25).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMAJL    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDMAJL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDMAJF    PIC X.                                          00000550
           02 FILLER    PIC X(2).                                       00000560
           02 MDMAJI    PIC X(10).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCSECL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MLSOCSECL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLSOCSECF      PIC X.                                     00000590
           02 FILLER    PIC X(2).                                       00000600
           02 MLSOCSECI      PIC X(9).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCSECL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MNSOCSECL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCSECF      PIC X.                                     00000630
           02 FILLER    PIC X(2).                                       00000640
           02 MNSOCSECI      PIC X(3).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCSECLIBL   COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MNSOCSECLIBL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNSOCSECLIBF   PIC X.                                     00000670
           02 FILLER    PIC X(2).                                       00000680
           02 MNSOCSECLIBI   PIC X(23).                                 00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCLOTUREL     COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MDCLOTUREL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDCLOTUREF     PIC X.                                     00000710
           02 FILLER    PIC X(2).                                       00000720
           02 MDCLOTUREI     PIC X(10).                                 00000730
           02 MSEQCODECD OCCURS   18 TIMES .                            00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSEQCODECL   COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MSEQCODECL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MSEQCODECF   PIC X.                                     00000760
             03 FILLER  PIC X(2).                                       00000770
             03 MSEQCODECI   PIC X(2).                                  00000780
           02 MCODECD OCCURS   18 TIMES .                               00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODECL      COMP PIC S9(4).                            00000800
      *--                                                                       
             03 MCODECL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCODECF      PIC X.                                     00000810
             03 FILLER  PIC X(2).                                       00000820
             03 MCODECI      PIC X(6).                                  00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTDUPLICL      COMP PIC S9(4).                            00000840
      *--                                                                       
           02 MTDUPLICL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTDUPLICF      PIC X.                                     00000850
           02 FILLER    PIC X(2).                                       00000860
           02 MTDUPLICI      PIC X(19).                                 00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSECTCREL     COMP PIC S9(4).                            00000880
      *--                                                                       
           02 MDSECTCREL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDSECTCREF     PIC X.                                     00000890
           02 FILLER    PIC X(2).                                       00000900
           02 MDSECTCREI     PIC X(3).                                  00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFSECTCREL     COMP PIC S9(4).                            00000920
      *--                                                                       
           02 MFSECTCREL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MFSECTCREF     PIC X.                                     00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MFSECTCREI     PIC X(3).                                  00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTAPARTIRL     COMP PIC S9(4).                            00000960
      *--                                                                       
           02 MTAPARTIRL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MTAPARTIRF     PIC X.                                     00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MTAPARTIRI     PIC X(11).                                 00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSECTORIGL    COMP PIC S9(4).                            00001000
      *--                                                                       
           02 MDSECTORIGL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDSECTORIGF    PIC X.                                     00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MDSECTORIGI    PIC X(3).                                  00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFSECTORIGL    COMP PIC S9(4).                            00001040
      *--                                                                       
           02 MFSECTORIGL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MFSECTORIGF    PIC X.                                     00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MFSECTORIGI    PIC X(3).                                  00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTSOCL    COMP PIC S9(4).                                 00001080
      *--                                                                       
           02 MTSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTSOCF    PIC X.                                          00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MTSOCI    PIC X(13).                                      00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDUPLSOCL      COMP PIC S9(4).                            00001120
      *--                                                                       
           02 MDUPLSOCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDUPLSOCF      PIC X.                                     00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MDUPLSOCI      PIC X(3).                                  00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001160
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MLIBERRI  PIC X(79).                                      00001190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001200
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MCODTRAI  PIC X(4).                                       00001230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001240
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MCICSI    PIC X(5).                                       00001270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001280
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MSCREENI  PIC X(4).                                       00001310
      ***************************************************************** 00001320
      * GCT - d�tail des sections                                       00001330
      ***************************************************************** 00001340
       01   EFM26O REDEFINES EFM26I.                                    00001350
           02 FILLER    PIC X(12).                                      00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MDATJOUA  PIC X.                                          00001380
           02 MDATJOUC  PIC X.                                          00001390
           02 MDATJOUH  PIC X.                                          00001400
           02 MDATJOUO  PIC X(10).                                      00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MTIMJOUA  PIC X.                                          00001430
           02 MTIMJOUC  PIC X.                                          00001440
           02 MTIMJOUH  PIC X.                                          00001450
           02 MTIMJOUO  PIC X(5).                                       00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MTITREA   PIC X.                                          00001480
           02 MTITREC   PIC X.                                          00001490
           02 MTITREH   PIC X.                                          00001500
           02 MTITREO   PIC X(8).                                       00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MCPLANA   PIC X.                                          00001530
           02 MCPLANC   PIC X.                                          00001540
           02 MCPLANH   PIC X.                                          00001550
           02 MCPLANO   PIC X(4).                                       00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MLPLANA   PIC X.                                          00001580
           02 MLPLANC   PIC X.                                          00001590
           02 MLPLANH   PIC X.                                          00001600
           02 MLPLANO   PIC X(20).                                      00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MCODEA    PIC X.                                          00001630
           02 MCODEC    PIC X.                                          00001640
           02 MCODEH    PIC X.                                          00001650
           02 MCODEO    PIC X(6).                                       00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MLCODECA  PIC X.                                          00001680
           02 MLCODECC  PIC X.                                          00001690
           02 MLCODECH  PIC X.                                          00001700
           02 MLCODECO  PIC X(15).                                      00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MLCODELA  PIC X.                                          00001730
           02 MLCODELC  PIC X.                                          00001740
           02 MLCODELH  PIC X.                                          00001750
           02 MLCODELO  PIC X(30).                                      00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MMASQUEA  PIC X.                                          00001780
           02 MMASQUEC  PIC X.                                          00001790
           02 MMASQUEH  PIC X.                                          00001800
           02 MMASQUEO  PIC X(6).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MLIBFIRA  PIC X.                                          00001830
           02 MLIBFIRC  PIC X.                                          00001840
           02 MLIBFIRH  PIC X.                                          00001850
           02 MLIBFIRO  PIC X(8).                                       00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MWFIRA    PIC X.                                          00001880
           02 MWFIRC    PIC X.                                          00001890
           02 MWFIRH    PIC X.                                          00001900
           02 MWFIRO    PIC X.                                          00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MVALFIRA  PIC X.                                          00001930
           02 MVALFIRC  PIC X.                                          00001940
           02 MVALFIRH  PIC X.                                          00001950
           02 MVALFIRO  PIC X(25).                                      00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MDMAJA    PIC X.                                          00001980
           02 MDMAJC    PIC X.                                          00001990
           02 MDMAJH    PIC X.                                          00002000
           02 MDMAJO    PIC X(10).                                      00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MLSOCSECA      PIC X.                                     00002030
           02 MLSOCSECC PIC X.                                          00002040
           02 MLSOCSECH PIC X.                                          00002050
           02 MLSOCSECO      PIC X(9).                                  00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MNSOCSECA      PIC X.                                     00002080
           02 MNSOCSECC PIC X.                                          00002090
           02 MNSOCSECH PIC X.                                          00002100
           02 MNSOCSECO      PIC X(3).                                  00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MNSOCSECLIBA   PIC X.                                     00002130
           02 MNSOCSECLIBC   PIC X.                                     00002140
           02 MNSOCSECLIBH   PIC X.                                     00002150
           02 MNSOCSECLIBO   PIC X(23).                                 00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MDCLOTUREA     PIC X.                                     00002180
           02 MDCLOTUREC     PIC X.                                     00002190
           02 MDCLOTUREH     PIC X.                                     00002200
           02 MDCLOTUREO     PIC X(10).                                 00002210
           02 DFHMS1 OCCURS   18 TIMES .                                00002220
             03 FILLER       PIC X(2).                                  00002230
             03 MSEQCODECA   PIC X.                                     00002240
             03 MSEQCODECC   PIC X.                                     00002250
             03 MSEQCODECH   PIC X.                                     00002260
             03 MSEQCODECO   PIC X(2).                                  00002270
           02 DFHMS2 OCCURS   18 TIMES .                                00002280
             03 FILLER       PIC X(2).                                  00002290
             03 MCODECA      PIC X.                                     00002300
             03 MCODECC PIC X.                                          00002310
             03 MCODECH PIC X.                                          00002320
             03 MCODECO      PIC X(6).                                  00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MTDUPLICA      PIC X.                                     00002350
           02 MTDUPLICC PIC X.                                          00002360
           02 MTDUPLICH PIC X.                                          00002370
           02 MTDUPLICO      PIC X(19).                                 00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MDSECTCREA     PIC X.                                     00002400
           02 MDSECTCREC     PIC X.                                     00002410
           02 MDSECTCREH     PIC X.                                     00002420
           02 MDSECTCREO     PIC X(3).                                  00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MFSECTCREA     PIC X.                                     00002450
           02 MFSECTCREC     PIC X.                                     00002460
           02 MFSECTCREH     PIC X.                                     00002470
           02 MFSECTCREO     PIC X(3).                                  00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MTAPARTIRA     PIC X.                                     00002500
           02 MTAPARTIRC     PIC X.                                     00002510
           02 MTAPARTIRH     PIC X.                                     00002520
           02 MTAPARTIRO     PIC X(11).                                 00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MDSECTORIGA    PIC X.                                     00002550
           02 MDSECTORIGC    PIC X.                                     00002560
           02 MDSECTORIGH    PIC X.                                     00002570
           02 MDSECTORIGO    PIC X(3).                                  00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MFSECTORIGA    PIC X.                                     00002600
           02 MFSECTORIGC    PIC X.                                     00002610
           02 MFSECTORIGH    PIC X.                                     00002620
           02 MFSECTORIGO    PIC X(3).                                  00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MTSOCA    PIC X.                                          00002650
           02 MTSOCC    PIC X.                                          00002660
           02 MTSOCH    PIC X.                                          00002670
           02 MTSOCO    PIC X(13).                                      00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MDUPLSOCA      PIC X.                                     00002700
           02 MDUPLSOCC PIC X.                                          00002710
           02 MDUPLSOCH PIC X.                                          00002720
           02 MDUPLSOCO      PIC X(3).                                  00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MLIBERRA  PIC X.                                          00002750
           02 MLIBERRC  PIC X.                                          00002760
           02 MLIBERRH  PIC X.                                          00002770
           02 MLIBERRO  PIC X(79).                                      00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MCODTRAA  PIC X.                                          00002800
           02 MCODTRAC  PIC X.                                          00002810
           02 MCODTRAH  PIC X.                                          00002820
           02 MCODTRAO  PIC X(4).                                       00002830
           02 FILLER    PIC X(2).                                       00002840
           02 MCICSA    PIC X.                                          00002850
           02 MCICSC    PIC X.                                          00002860
           02 MCICSH    PIC X.                                          00002870
           02 MCICSO    PIC X(5).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MSCREENA  PIC X.                                          00002900
           02 MSCREENC  PIC X.                                          00002910
           02 MSCREENH  PIC X.                                          00002920
           02 MSCREENO  PIC X(4).                                       00002930
                                                                                
